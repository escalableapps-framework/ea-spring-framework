# EA-SPRING-STARTER-REST

Provides configurations to be able to receive different types of data through the query-parameters, body, url and headers of a Rest endpoint.

The source and destination data types with their respective basic constraints are:

| http type      | java type      | constraints                                                                                                                                                           |
|----------------|----------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| string         | String         | [@Size]() [@TextPlain](../ea-spring-core#51-constraints) [@TextHtml](../ea-spring-core#51-constraints)                                                                |
| number         | Integer        | [@Min](https://docs.oracle.com/javaee/7/api/javax/validation/constraints/Min.html) [@Max](https://docs.oracle.com/javaee/7/api/javax/validation/constraints/Max.html) |
| number         | Long           | [@Min](https://docs.oracle.com/javaee/7/api/javax/validation/constraints/Min.html) [@Max](https://docs.oracle.com/javaee/7/api/javax/validation/constraints/Max.html) |
| number         | BigInteger     | [@Min](https://docs.oracle.com/javaee/7/api/javax/validation/constraints/Min.html) [@Max](https://docs.oracle.com/javaee/7/api/javax/validation/constraints/Max.html) |
| number         | Float          |                                                                                                                                                                       |
| number         | Double         |                                                                                                                                                                       |
| number         | BigDecimal     | [@DecimalMax](https://docs.oracle.com/javaee/7/api/javax/validation/constraints/DecimalMax.html)                                                                      |
| boolean        | Boolean        |                                                                                                                                                                       |
| ISO-8601       | OffsetDateTime | [@DatetimeRange](../ea-spring-core#51-constraints)                                                                                                                    |
| ISO-8601       | LocalDateTime  | [@DatetimeRange](../ea-spring-core#51-constraints)                                                                                                                    |
| ISO-8601       | LocalDate      | [@DateRange](../ea-spring-core#51-constraints)                                                                                                                        |
| ISO-8601       | LocalTime      |                                                                                                                                                                       |
| ISO-8601       | Year           | [@YearRange](../ea-spring-core#51-constraints)                                                                                                                        |
| string         | ENUM           |                                                                                                                                                                       |

## 1. Licence
[The MIT License (MIT)](LICENSE)

## 2. Model

### 2.1 ErrorResponse
DTO encargado de llevar la respuesta de una solicitud que ha resultado en un error. 

### 2.2 RequestException
Excepción que se produce cuando no se pudo acceder a la información de la solicitud. 


## 3. Request Features

Legend:
- ✅ The field sent, the values ​​received and/or the messages delivered by the service show consistency
- ❌ The field sent, the values ​​received and/or the messages delivered by the service do NOT show consistency
- ⭕ The field sent, the values ​​received and/or the messages delivered by the service show PARTIAL consistency

### 3.1 Handle of query parameters (request-params)

3.1.1 [Request parameter with valid parameter value](src/test/resources/rest/request_param/request_parameter_with_valid_parameter_value.feature)
- ✅ The endpoint receives the request parameter in a method parameter, both having the same name
- ✅ The endpoint receives the request parameter in a method parameter, both having the diferent names, a custom request-parameter name is defined by the endpoint 
- ✅ The endpoint receives the request parameter in an optional method parameter, both having the same name
- ✅ The endpoint receives the request parameter in an optional method parameter, both having the diferent names, a custom request-parameter name is defined by the endpoint
- ✅ The endpoint receives the request parameter in a dto attribute, both having the same name
- ✅ The endpoint receives the request parameter in an optional dto attribute, both having the same name
- ✅ The endpoint receives the request parameter in a nested dto attribute, both having the same name
- ✅ The endpoint receives the request parameter in a nested optional dto attribute, both having the same name
- ✅ The endpoint receives the request parameter in a nested dto attribute from an array, both having the same name
- ✅ The endpoint receives the request parameter in a nested optional dto attribute from an array, both having the same name

3.1.2 [Request parameter with empty parameter value](src/test/resources/rest/request_param/request_parameter_with_empty_parameter_value.feature)
- ✅ The endpoint receives the request parameter in a method parameter, both having the same name
- ✅ The endpoint receives the request parameter in a method parameter, both having the diferent names, a custom request-parameter name is defined by the endpoint 
- ✅ The endpoint receives the request parameter in an optional method parameter, both having the same name
- ✅ The endpoint receives the request parameter in an optional method parameter, both having the diferent names, a custom request-parameter name is defined by the endpoint
- ✅ The endpoint receives the request parameter in a dto attribute, both having the same name
- ✅ The endpoint receives the request parameter in an optional dto attribute, both having the same name
- ✅ The endpoint receives the request parameter in a nested dto attribute, both having the same name
- ✅ The endpoint receives the request parameter in a nested optional dto attribute, both having the same name
- ✅ The endpoint receives the request parameter in a nested dto attribute from an array, both having the same name
- ✅ The endpoint receives the request parameter in a nested optional dto attribute from an array, both having the same name

3.1.3 [Request parameter with lesser parameter value for type](src/test/resources/rest/request_param/request_parameter_with_lesser_parameter_value_for_type.feature)
- ✅ The endpoint receives the request parameter in a method parameter, both having the same name
- ✅ The endpoint receives the request parameter in a method parameter, both having the diferent names, a custom request-parameter name is defined by the endpoint 
- ✅ The endpoint receives the request parameter in an optional method parameter, both having the same name
- ✅ The endpoint receives the request parameter in an optional method parameter, both having the diferent names, a custom request-parameter name is defined by the endpoint
- ✅ The endpoint receives the request parameter in a dto attribute, both having the same name
- ✅ The endpoint receives the request parameter in an optional dto attribute, both having the same name
- ✅ The endpoint receives the request parameter in a nested dto attribute, both having the same name
- ✅ The endpoint receives the request parameter in a nested optional dto attribute, both having the same name
- ✅ The endpoint receives the request parameter in a nested dto attribute from an array, both having the same name
- ✅ The endpoint receives the request parameter in a nested optional dto attribute from an array, both having the same name

3.1.4 [Request parameter with largest parameter value for type](src/test/resources/rest/request_param/request_parameter_with_largest_parameter_value_for_type.feature)
- ✅ The endpoint receives the request parameter in a method parameter, both having the same name
- ✅ The endpoint receives the request parameter in a method parameter, both having the diferent names, a custom request-parameter name is defined by the endpoint 
- ✅ The endpoint receives the request parameter in an optional method parameter, both having the same name
- ✅ The endpoint receives the request parameter in an optional method parameter, both having the diferent names, a custom request-parameter name is defined by the endpoint
- ✅ The endpoint receives the request parameter in a dto attribute, both having the same name
- ✅ The endpoint receives the request parameter in an optional dto attribute, both having the same name
- ✅ The endpoint receives the request parameter in a nested dto attribute, both having the same name
- ✅ The endpoint receives the request parameter in a nested optional dto attribute, both having the same name
- ✅ The endpoint receives the request parameter in a nested dto attribute from an array, both having the same name
- ✅ The endpoint receives the request parameter in a nested optional dto attribute from an array, both having the same name

3.1.5 [Request parameter with parameter value of different type](src/test/resources/rest/request_param/request_parameter_with_parameter_value_of_different_type.feature)
- ✅ The endpoint receives the request parameter in a method parameter, both having the same name
- ✅ The endpoint receives the request parameter in a method parameter, both having the diferent names, a custom request-parameter name is defined by the endpoint 
- ✅ The endpoint receives the request parameter in an optional method parameter, both having the same name
- ✅ The endpoint receives the request parameter in an optional method parameter, both having the diferent names, a custom request-parameter name is defined by the endpoint
- ✅ The endpoint receives the request parameter in a dto attribute, both having the same name
- ✅ The endpoint receives the request parameter in an optional dto attribute, both having the same name
- ✅ The endpoint receives the request parameter in a nested dto attribute, both having the same name
- ✅ The endpoint receives the request parameter in a nested optional dto attribute, both having the same name
- ✅ The endpoint receives the request parameter in a nested dto attribute from an array, both having the same name
- ✅ The endpoint receives the request parameter in a nested optional dto attribute from an array, both having the same name

3.1.6 [Request parameter with required parameter value](src/test/resources/rest/request_param/request_parameter_with_required_parameter_value.feature)
- ✅ The endpoint receives the request parameter in a required method parameter, both having the same name
- ✅ The endpoint receives the request parameter in a required method parameter, both having the diferent names, a custom request-parameter name is defined by the endpointendpoint 
- ❌ The endpoint receives the request parameter in a required but optional method parameter, both having the same name
- ❌ The endpoint receives the request parameter in a required but optional method parameter, both having the diferent names, a custom request-parameter name is defined by the endpoint

3.1.7 [Request parameter with non-null parameter value](src/test/resources/rest/request_param/request_parameter_with_non-null_parameter_value.feature)
- ✅ The endpoint receives the request parameter in a method parameter, both having the same name
- ✅ The endpoint receives the request parameter in a method parameter, both having the diferent names, a custom request-parameter name is defined by the endpoint 
- ✅ The endpoint receives the request parameter in an optional method parameter, both having the same name
- ❌ The endpoint receives the request parameter in an optional method parameter, both having the diferent names, a custom request-parameter name is defined by the endpoint
- ✅ The endpoint receives the request parameter in a dto attribute, both having the same name
- ✅ The endpoint receives the request parameter in an optional dto attribute, both having the same name
- ✅ The endpoint receives the request parameter in a nested dto attribute, both having the same name
- ✅ The endpoint receives the request parameter in a nested optional dto attribute, both having the same name
- ✅ The endpoint receives the request parameter in a nested dto attribute from an array, both having the same name
- ✅ The endpoint receives the request parameter in a nested optional dto attribute from an array, both having the same name

3.1.8 [Request parameter with default-validation for parameter value](src/test/resources/rest/request_param/request_parameter_with_default-validation_parameter_value.feature)
- ✅ The endpoint receives the request parameter in a method parameter, both having the same name
- ❌ The endpoint receives the request parameter in a method parameter, both having the diferent names, a custom request-parameter name is defined by the endpoint 
- ✅ The endpoint receives the request parameter in an optional method parameter, both having the same name
- ❌ The endpoint receives the request parameter in an optional method parameter, both having the diferent names, a custom request-parameter name is defined by the endpoint
- ✅ The endpoint receives the request parameter in a dto attribute, both having the same name
- ✅ The endpoint receives the request parameter in an optional dto attribute, both having the same name
- ✅ The endpoint receives the request parameter in a nested dto attribute, both having the same name
- ✅ The endpoint receives the request parameter in a nested optional dto attribute, both having the same name
- ✅ The endpoint receives the request parameter in a nested dto attribute from an array, both having the same name
- ✅ The endpoint receives the request parameter in a nested optional dto attribute from an array, both having the same name

3.1.9 [Request parameter with custom-validation for parameter value](src/test/resources/rest/request_param/request_parameter_with_custom-validation_parameter_value.feature)
- ✅ The endpoint receives the request parameter in a method parameter, both having the same name
- ❌ The endpoint receives the request parameter in a method parameter, both having the diferent names, a custom request-parameter name is defined by the endpoint 
- ✅ The endpoint receives the request parameter in an optional method parameter, both having the same name
- ❌ The endpoint receives the request parameter in an optional method parameter, both having the diferent names, a custom request-parameter name is defined by the endpoint
- ✅ The endpoint receives the request parameter in a dto attribute, both having the same name
- ✅ The endpoint receives the request parameter in an optional dto attribute, both having the same name
- ✅ The endpoint receives the request parameter in a nested dto attribute, both having the same name
- ✅ The endpoint receives the request parameter in a nested optional dto attribute, both having the same name
- ✅ The endpoint receives the request parameter in a nested dto attribute from an array, both having the same name
- ✅ The endpoint receives the request parameter in a nested optional dto attribute from an array, both having the same name

3.1.10 [Request parameter with i18n-validation for parameter value](src/test/resources/rest/request_param/request_parameter_with_i18n-validation_parameter_value.feature)
- ✅ The endpoint receives the request parameter in a method parameter, both having the same name
- ❌ The endpoint receives the request parameter in a method parameter, both having the diferent names, a custom request-parameter name is defined by the endpoint 
- ✅ The endpoint receives the request parameter in an optional method parameter, both having the same name
- ❌ The endpoint receives the request parameter in an optional method parameter, both having the diferent names, a custom request-parameter name is defined by the endpoint
- ✅ The endpoint receives the request parameter in a dto attribute, both having the same name
- ✅ The endpoint receives the request parameter in an optional dto attribute, both having the same name
- ✅ The endpoint receives the request parameter in a nested dto attribute, both having the same name
- ✅ The endpoint receives the request parameter in a nested optional dto attribute, both having the same name
- ✅ The endpoint receives the request parameter in a nested dto attribute from an array, both having the same name
- ✅ The endpoint receives the request parameter in a nested optional dto attribute from an array, both having the same name

### 3.2 Handle of request body

3.2.1 [Request body with valid parameter value](src/test/resources/rest/request_body/request_body_with_valid_parameter_value.feature)
- ✅ The endpoint receives the request body-attibute in a dto attribute, both having the same name
- ✅ The endpoint receives the request body-attibute in a dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
- ✅ The endpoint receives the request body-attibute in an optional dto attribute, both having the same name
- ✅ The endpoint receives the request body-attibute in an optional dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
- ✅ The endpoint receives the request body-attibute in a nested dto attribute, both having the same name
- ✅ The endpoint receives the request body-attibute in a nested dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
- ✅ The endpoint receives the request body-attibute in a nested optional dto attribute, both having the same name
- ✅ The endpoint receives the request body-attibute in a nested optional dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
- ✅ The endpoint receives the request body-attibute in a nested dto attribute from an array, both having the same name
- ✅ The endpoint receives the request body-attibute in a nested dto attribute from an array, both having the diferent names, a custom body-attibute name is defined by the endpoint
- ✅ The endpoint receives the request body-attibute in a nested optional dto attribute, both having the same name
- ✅ The endpoint receives the request body-attibute in a nested optional dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint

3.2.2 [Request body with empty parameter value](src/test/resources/rest/request_body/request_body_with_empty_parameter_value.feature)
- ⭕ The endpoint receives the request body-attibute in a dto attribute, both having the same name
- ⭕ The endpoint receives the request body-attibute in a dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
- ⭕ The endpoint receives the request body-attibute in an optional dto attribute, both having the same name
- ⭕ The endpoint receives the request body-attibute in an optional dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
- ⭕ The endpoint receives the request body-attibute in a nested dto attribute, both having the same name
- ⭕ The endpoint receives the request body-attibute in a nested dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
- ⭕ The endpoint receives the request body-attibute in a nested optional dto attribute, both having the same name
- ⭕ The endpoint receives the request body-attibute in a nested optional dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
- ⭕ The endpoint receives the request body-attibute in a nested dto attribute from an array, both having the same name
- ⭕ The endpoint receives the request body-attibute in a nested dto attribute from an array, both having the diferent names, a custom body-attibute name is defined by the endpoint
- ⭕ The endpoint receives the request body-attibute in a nested optional dto attribute from an array, both having the same name
- ⭕ The endpoint receives the request body-attibute in a nested optional dto attribute from an array, both having the diferent names, a custom body-attibute name is defined by the endpoint

3.2.3 [Request body with lesser parameter value for type](src/test/resources/rest/request_body/request_body_with_lesser_parameter_value_for_type.feature)
- ✅ The endpoint receives the request body-attibute in a dto attribute, both having the same name
- ✅ The endpoint receives the request body-attibute in a dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
- ✅ The endpoint receives the request body-attibute in an optional dto attribute, both having the same name
- ✅ The endpoint receives the request body-attibute in an optional dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
- ✅ The endpoint receives the request body-attibute in a nested dto attribute, both having the same name
- ✅ The endpoint receives the request body-attibute in a nested dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
- ✅ The endpoint receives the request body-attibute in a nested optional dto attribute, both having the same name
- ✅ The endpoint receives the request body-attibute in a nested optional dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
- ✅ The endpoint receives the request body-attibute in a nested dto attribute from an array, both having the same name
- ✅ The endpoint receives the request body-attibute in a nested dto attribute from an array, both having the diferent names, a custom body-attibute name is defined by the endpoint
- ✅ The endpoint receives the request body-attibute in a nested optional dto attribute from an array, both having the same name
- ✅ The endpoint receives the request body-attibute in a nested optional dto attribute from an array, both having the diferent names, a custom body-attibute name is defined by the endpoint

3.2.4 [Request body with largest parameter value for type](src/test/resources/rest/request_body/request_body_with_largest_parameter_value_for_type.feature)
- ✅ The endpoint receives the request body-attibute in a dto attribute, both having the same name
- ✅ The endpoint receives the request body-attibute in a dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
- ✅ The endpoint receives the request body-attibute in an optional dto attribute, both having the same name
- ✅ The endpoint receives the request body-attibute in an optional dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
- ✅ The endpoint receives the request body-attibute in a nested dto attribute, both having the same name
- ✅ The endpoint receives the request body-attibute in a nested dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
- ✅ The endpoint receives the request body-attibute in a nested optional dto attribute, both having the same name
- ✅ The endpoint receives the request body-attibute in a nested optional dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
- ✅ The endpoint receives the request body-attibute in a nested dto attribute from an array, both having the same name
- ✅ The endpoint receives the request body-attibute in a nested dto attribute from an array, both having the diferent names, a custom body-attibute name is defined by the endpoint
- ✅ The endpoint receives the request body-attibute in a nested optional dto attribute from an array, both having the same name
- ✅ The endpoint receives the request body-attibute in a nested optional dto attribute from an array, both having the diferent names, a custom body-attibute name is defined by the endpoint

3.2.5 [Request body with parameter value of different type](src/test/resources/rest/request_body/request_body_with_parameter_value_of_different_type.feature)
- ✅ The endpoint receives the request body-attibute in a dto attribute, both having the same name
- ✅ The endpoint receives the request body-attibute in a dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
- ✅ The endpoint receives the request body-attibute in an optional dto attribute, both having the same name
- ✅ The endpoint receives the request body-attibute in an optional dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
- ✅ The endpoint receives the request body-attibute in a nested dto attribute, both having the same name
- ✅ The endpoint receives the request body-attibute in a nested dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
- ✅ The endpoint receives the request body-attibute in a nested optional dto attribute, both having the same name
- ✅ The endpoint receives the request body-attibute in a nested optional dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
- ✅ The endpoint receives the request body-attibute in a nested dto attribute from an array, both having the same name
- ✅ The endpoint receives the request body-attibute in a nested dto attribute from an array, both having the diferent names, a custom body-attibute name is defined by the endpoint
- ✅ The endpoint receives the request body-attibute in a nested optional dto attribute from an array, both having the same name
- ✅ The endpoint receives the request body-attibute in a nested optional dto attribute from an array, both having the diferent names, a custom body-attibute name is defined by the endpoint

3.2.6 [Request body with non-null parameter value](src/test/resources/rest/request_body/request_body_with_non-null_parameter_value.feature)
- ✅ The endpoint receives the request body-attibute in a dto attribute, both having the same name
- ❌ The endpoint receives the request body-attibute in a dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
- ✅ The endpoint receives the request body-attibute in an optional dto attribute, both having the same name
- ❌ The endpoint receives the request body-attibute in an optional dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
- ✅ The endpoint receives the request body-attibute in a nested dto attribute, both having the same name
- ❌ The endpoint receives the request body-attibute in a nested dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
- ✅ The endpoint receives the request body-attibute in a nested optional dto attribute, both having the same name
- ❌ The endpoint receives the request body-attibute in a nested optional dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
- ✅ The endpoint receives the request body-attibute in a nested dto attribute from an array, both having the same name
- ❌ The endpoint receives the request body-attibute in a nested dto attribute from an array, both having the diferent names, a custom body-attibute name is defined by the endpoint
- ✅ The endpoint receives the request body-attibute in a nested optional dto attribute from an array, both having the same name
- ❌ The endpoint receives the request body-attibute in a nested optional dto attribute from an array, both having the diferent names, a custom body-attibute name is defined by the endpoint

3.2.7 [Request body with default-validation parameter value](src/test/resources/rest/request_body/request_body_with_default-validation_parameter_value.feature)
- ✅ The endpoint receives the request body-attibute in a dto attribute, both having the same name
- ❌ The endpoint receives the request body-attibute in a dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
- ✅ The endpoint receives the request body-attibute in an optional dto attribute, both having the same name
- ❌ The endpoint receives the request body-attibute in an optional dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
- ✅ The endpoint receives the request body-attibute in a nested dto attribute, both having the same name
- ❌ The endpoint receives the request body-attibute in a nested dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
- ✅ The endpoint receives the request body-attibute in a nested optional dto attribute, both having the same name
- ❌ The endpoint receives the request body-attibute in a nested optional dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
- ✅ The endpoint receives the request body-attibute in a nested dto attribute from an array, both having the same name
- ❌ The endpoint receives the request body-attibute in a nested dto attribute from an array, both having the diferent names, a custom body-attibute name is defined by the endpoint
- ✅ The endpoint receives the request body-attibute in a nested optional dto attribute from an array, both having the same name
- ❌ The endpoint receives the request body-attibute in a nested optional dto attribute from an array, both having the diferent names, a custom body-attibute name is defined by the endpoint

3.2.8 [Request body with custom-validation parameter value](src/test/resources/rest/request_body/request_body_with_custom-validation_parameter_value.feature)
- ✅ The endpoint receives the request body-attibute in a dto attribute, both having the same name
- ❌ The endpoint receives the request body-attibute in a dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
- ✅ The endpoint receives the request body-attibute in an optional dto attribute, both having the same name
- ❌ The endpoint receives the request body-attibute in an optional dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
- ✅ The endpoint receives the request body-attibute in a nested dto attribute, both having the same name
- ❌ The endpoint receives the request body-attibute in a nested dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
- ✅ The endpoint receives the request body-attibute in a nested optional dto attribute, both having the same name
- ❌ The endpoint receives the request body-attibute in a nested optional dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
- ✅ The endpoint receives the request body-attibute in a nested dto attribute from an array, both having the same name
- ❌ The endpoint receives the request body-attibute in a nested dto attribute from an array, both having the diferent names, a custom body-attibute name is defined by the endpoint
- ✅ The endpoint receives the request body-attibute in a nested optional dto attribute from an array, both having the same name
- ❌ The endpoint receives the request body-attibute in a nested optional dto attribute from an array, both having the diferent names, a custom body-attibute name is defined by the endpoint

3.2.9 [Request body with i18n-validation parameter value](src/test/resources/rest/request_body/request_body_with_i18n-validation_parameter_value.feature)
- ✅ The endpoint receives the request body-attibute in a dto attribute, both having the same name
- ❌ The endpoint receives the request body-attibute in a dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
- ✅ The endpoint receives the request body-attibute in an optional dto attribute, both having the same name
- ❌ The endpoint receives the request body-attibute in an optional dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
- ✅ The endpoint receives the request body-attibute in a nested dto attribute, both having the same name
- ❌ The endpoint receives the request body-attibute in a nested dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
- ✅ The endpoint receives the request body-attibute in a nested optional dto attribute, both having the same name
- ❌ The endpoint receives the request body-attibute in a nested optional dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
- ✅ The endpoint receives the request body-attibute in a nested dto attribute from an array, both having the same name
- ❌ The endpoint receives the request body-attibute in a nested dto attribute from an array, both having the diferent names, a custom body-attibute name is defined by the endpoint
- ✅ The endpoint receives the request body-attibute in a nested optional dto attribute from an array, both having the same name
- ❌ The endpoint receives the request body-attibute in a nested optional dto attribute from an array, both having the diferent names, a custom body-attibute name is defined by the endpoint


3.2.10 [Request body malformed](src/test/resources/rest/request_body/request_body_malformed.feature)
- ✅ The endpoint endpoint receives and malformed request body.

### 3.3 Handle of path variable

