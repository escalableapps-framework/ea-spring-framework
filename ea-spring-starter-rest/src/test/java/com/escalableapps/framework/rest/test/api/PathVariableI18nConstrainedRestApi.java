package com.escalableapps.framework.rest.test.api;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.HEAD;
import static org.springframework.web.bind.annotation.RequestMethod.PATCH;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.Year;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.Max;
import javax.validation.constraints.Size;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.escalableapps.framework.core.validation.constraints.DateRange;
import com.escalableapps.framework.core.validation.constraints.DatetimeRange;
import com.escalableapps.framework.core.validation.constraints.YearRange;

@RestController
@RequestMapping("/api/v1/path-variable/i18n-constrained")
@Validated
public class PathVariableI18nConstrainedRestApi {

  @RequestMapping(path = "/method-parameter/paramString/{paramString}", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamString(
      @PathVariable("paramString") @Size(max = 1, message = "is not a valid string") String paramString) {
    return null;
  }

  @RequestMapping(path = "/method-parameter/paramInteger/{paramInteger}", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> methodParameterParamInteger(
      @PathVariable("paramInteger") @Max(value = 1, message = "is not a valid integer") Integer paramInteger) {
    return null;
  }

  @RequestMapping(path = "/method-parameter/paramLong/{paramLong}", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamLong(
      @PathVariable("paramLong") @Max(value = 1, message = "is not a valid long") Long paramLong) {
    return null;
  }

  @RequestMapping(path = "/method-parameter/paramBigInteger/{paramBigInteger}", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> methodParameterParamBigInteger(
      @PathVariable("paramBigInteger") @DecimalMax(value = "1", message = "is not a valid big-integer") BigInteger paramBigInteger) {
    return null;
  }

  @RequestMapping(path = "/method-parameter/paramBigDecimal/{paramBigDecimal}", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> methodParameterParamBigDecimal(
      @PathVariable("paramBigDecimal") @DecimalMax(value = "9.9", message = "is not a valid big-decimal") BigDecimal paramBigDecimal) {
    return null;
  }

  @RequestMapping(path = "/method-parameter/paramOffsetDateTime/{paramOffsetDateTime}", method = {GET, HEAD, POST, PUT,
      PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamOffsetDateTime(
      @PathVariable("paramOffsetDateTime") @DatetimeRange(max = "2000-01-01T00:00:00Z", message = "is not a valid offset-datetime") OffsetDateTime paramOffsetDateTime) {
    return null;
  }

  @RequestMapping(path = "/method-parameter/paramLocalDateTime/{paramLocalDateTime}", method = {GET, HEAD, POST, PUT,
      PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamLocalDateTime(
      @PathVariable("paramLocalDateTime") @DatetimeRange(max = "2000-01-01T00:00:00Z", message = "is not a valid local-datetime") LocalDateTime paramLocalDateTime) {
    return null;
  }

  @RequestMapping(path = "/method-parameter/paramLocalDate/{paramLocalDate}", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> methodParameterParamLocalDate(
      @PathVariable("paramLocalDate") @DateRange(max = "2000-01-01", message = "is not a valid date") LocalDate paramLocalDate) {
    return null;
  }

  @RequestMapping(path = "/method-parameter/paramYear/{paramYear}", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamYear(
      @PathVariable("paramYear") @YearRange(max = "2000", message = "is not a valid year") Year paramYear) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/param-string/{param-string}", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> namedMethodParameterParamString(
      @PathVariable("param-string") @Size(max = 1, message = "is not a valid string") String paramString) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/param-integer/{param-integer}", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> namedMethodParameterParamInteger(
      @PathVariable(name = "param-integer") @Max(value = 1, message = "is not a valid integer") Integer paramInteger) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/param-long/{param-long}", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> namedMethodParameterParamLong(
      @PathVariable("param-long") @Max(value = 1, message = "is not a valid long") Long paramLong) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/param-big-integer/{param-big-integer}", method = {GET, HEAD, POST,
      PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamBigInteger(
      @PathVariable("param-big-integer") @DecimalMax(value = "1", message = "is not a valid big-integer") BigInteger paramBigInteger) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/param-big-decimal/{param-big-decimal}", method = {GET, HEAD, POST,
      PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamBigDecimal(
      @PathVariable("param-big-decimal") @DecimalMax(value = "9.9", message = "is not a valid big-decimal") BigDecimal paramBigDecimal) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/param-offset-date-time/{param-offset-date-time}", method = {GET, HEAD,
      POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamOffsetDateTime(
      @PathVariable("param-offset-date-time") @DatetimeRange(max = "2000-01-01T00:00:00Z", message = "is not a valid offset-datetime") OffsetDateTime paramOffsetDateTime) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/param-local-date-time/{param-local-date-time}", method = {GET, HEAD,
      POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamLocalDateTime(
      @PathVariable("param-local-date-time") @DatetimeRange(max = "2000-01-01T00:00:00Z", message = "is not a valid local-datetime") LocalDateTime paramLocalDateTime) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/param-local-date/{param-local-date}", method = {GET, HEAD, POST, PUT,
      PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamLocalDate(
      @PathVariable("param-local-date") @DateRange(max = "2000-01-01", message = "is not a valid date") LocalDate paramLocalDate) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/param-year/{param-year}", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> namedMethodParameterParamYear(
      @PathVariable("param-year") @YearRange(max = "2000", message = "is not a valid year") Year paramYear) {
    return null;
  }
}
