package com.escalableapps.framework.rest.test.api;

import static com.escalableapps.framework.rest.RestConfigurationTest.RestConfigurationTestContext.RECEIVED_VALUES;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.HEAD;
import static org.springframework.web.bind.annotation.RequestMethod.PATCH;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.Year;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.escalableapps.framework.core.util.JsonUtils;
import com.escalableapps.framework.rest.RestConfigurationTest.EnumAB;
import com.escalableapps.framework.rest.RestConfigurationTest.EnumLowerAB;
import com.escalableapps.framework.rest.RestConfigurationTest.UnamedNotNullDto;

@RestController
@RequestMapping("/api/v1/path-variable")
@Validated
public class PathVariableUnconstrainedRestApi {

  @RequestMapping(path = "/method-parameter/paramString/{paramString}", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamString(@PathVariable(name = "paramString") String paramString) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramString(paramString) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/method-parameter/paramInteger/{paramInteger}", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> methodParameterParamInteger(@PathVariable(name = "paramInteger") Integer paramInteger) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramInteger(paramInteger) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/method-parameter/paramLong/{paramLong}", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamLong(@PathVariable(name = "paramLong") Long paramLong) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramLong(paramLong) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/method-parameter/paramBigInteger/{paramBigInteger}", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> methodParameterParamBigInteger(
      @PathVariable(name = "paramBigInteger") BigInteger paramBigInteger) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramBigInteger(paramBigInteger) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/method-parameter/paramFloat/{paramFloat}", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamFloat(@PathVariable(name = "paramFloat") Float paramFloat) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramFloat(paramFloat) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/method-parameter/paramDouble/{paramDouble}", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamDouble(@PathVariable(name = "paramDouble") Double paramDouble) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramDouble(paramDouble) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/method-parameter/paramBigDecimal/{paramBigDecimal}", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> methodParameterParamBigDecimal(
      @PathVariable(name = "paramBigDecimal") BigDecimal paramBigDecimal) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramBigDecimal(paramBigDecimal) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/method-parameter/paramBoolean/{paramBoolean}", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> methodParameterParamBoolean(@PathVariable(name = "paramBoolean") Boolean paramBoolean) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramBoolean(paramBoolean) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/method-parameter/paramOffsetDateTime/{paramOffsetDateTime}", method = {GET, HEAD, POST, PUT,
      PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamOffsetDateTime(
      @PathVariable(name = "paramOffsetDateTime") OffsetDateTime paramOffsetDateTime) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramOffsetDateTime(paramOffsetDateTime) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/method-parameter/paramLocalDateTime/{paramLocalDateTime}", method = {GET, HEAD, POST, PUT,
      PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamLocalDateTime(
      @PathVariable(name = "paramLocalDateTime") LocalDateTime paramLocalDateTime) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramLocalDateTime(paramLocalDateTime) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/method-parameter/paramLocalDate/{paramLocalDate}", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> methodParameterParamLocalDate(
      @PathVariable(name = "paramLocalDate") LocalDate paramLocalDate) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramLocalDate(paramLocalDate) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/method-parameter/paramLocalTime/{paramLocalTime}", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> methodParameterParamLocalTime(
      @PathVariable(name = "paramLocalTime") LocalTime paramLocalTime) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramLocalTime(paramLocalTime) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/method-parameter/paramYear/{paramYear}", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamYear(@PathVariable(name = "paramYear") Year paramYear) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramYear(paramYear) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/method-parameter/paramEnumAB/{paramEnumAB}", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamEnumAB(@PathVariable(name = "paramEnumAB") EnumAB paramEnumAB) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramEnumAB(paramEnumAB) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/method-parameter/paramEnumLowerAB/{paramEnumLowerAB}", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> methodParameterParamEnumLowerAB(
      @PathVariable(name = "paramEnumLowerAB") EnumLowerAB paramEnumLowerAB) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramEnumLowerAB(paramEnumLowerAB) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/named-method-parameter/param-string/{param-string}", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> namedMethodParameterParamString(@PathVariable(name = "param-string") String paramString) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramString(paramString) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/named-method-parameter/param-integer/{param-integer}", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> namedMethodParameterParamInteger(
      @PathVariable(name = "param-integer") Integer paramInteger) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramInteger(paramInteger) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/named-method-parameter/param-long/{param-long}", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> namedMethodParameterParamLong(@PathVariable(name = "param-long") Long paramLong) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramLong(paramLong) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/named-method-parameter/param-big-integer/{param-big-integer}", method = {GET, HEAD, POST,
      PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamBigInteger(
      @PathVariable(name = "param-big-integer") BigInteger paramBigInteger) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramBigInteger(paramBigInteger) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/named-method-parameter/param-float/{param-float}", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> namedMethodParameterParamFloat(@PathVariable(name = "param-float") Float paramFloat) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramFloat(paramFloat) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/named-method-parameter/param-double/{param-double}", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> namedMethodParameterParamDouble(@PathVariable(name = "param-double") Double paramDouble) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramDouble(paramDouble) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/named-method-parameter/param-big-decimal/{param-big-decimal}", method = {GET, HEAD, POST,
      PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamBigDecimal(
      @PathVariable(name = "param-big-decimal") BigDecimal paramBigDecimal) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramBigDecimal(paramBigDecimal) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/named-method-parameter/param-boolean/{param-boolean}", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> namedMethodParameterParamBoolean(
      @PathVariable(name = "param-boolean") Boolean paramBoolean) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramBoolean(paramBoolean) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/named-method-parameter/param-offset-date-time/{param-offset-date-time}", method = {GET, HEAD,
      POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamOffsetDateTime(
      @PathVariable(name = "param-offset-date-time") OffsetDateTime paramOffsetDateTime) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramOffsetDateTime(paramOffsetDateTime) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/named-method-parameter/param-local-date-time/{param-local-date-time}", method = {GET, HEAD,
      POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamLocalDateTime(
      @PathVariable(name = "param-local-date-time") LocalDateTime paramLocalDateTime) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramLocalDateTime(paramLocalDateTime) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/named-method-parameter/param-local-date/{param-local-date}", method = {GET, HEAD, POST, PUT,
      PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamLocalDate(
      @PathVariable(name = "param-local-date") LocalDate paramLocalDate) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramLocalDate(paramLocalDate) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/named-method-parameter/param-local-time/{param-local-time}", method = {GET, HEAD, POST, PUT,
      PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamLocalTime(
      @PathVariable(name = "param-local-time") LocalTime paramLocalTime) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramLocalTime(paramLocalTime) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/named-method-parameter/param-year/{param-year}", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> namedMethodParameterParamYear(@PathVariable(name = "param-year") Year paramYear) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramYear(paramYear) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/named-method-parameter/param-enum-ab/{param-enum-ab}", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> namedMethodParameterParamEnumAB(
      @PathVariable(name = "param-enum-ab") EnumAB paramEnumAB) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramEnumAB(paramEnumAB) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/named-method-parameter/param-enum-lower-ab/{param-enum-lower-ab}", method = {GET, HEAD, POST,
      PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamEnumLowerAB(
      @PathVariable(name = "param-enum-lower-ab") EnumLowerAB paramEnumLowerAB) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramEnumLowerAB(paramEnumLowerAB) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }
}
