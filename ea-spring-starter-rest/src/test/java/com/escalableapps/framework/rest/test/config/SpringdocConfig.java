package com.escalableapps.framework.rest.test.config;

import java.time.LocalTime;
import java.time.Year;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.springdoc.core.SpringDocUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.media.Schema;

@Configuration
public class SpringdocConfig {

  public class LocalTimeSchema extends Schema<LocalTime> {

    public LocalTimeSchema() {
      super("string", "full-time");
      example = LocalTime.now();
    }

    @Override
    public LocalTimeSchema type(String type) {
      super.setType(type);
      return this;
    }

    @Override
    public LocalTimeSchema format(String format) {
      super.setFormat(format);
      return this;
    }

    public LocalTimeSchema _default(Date _default) {
      super.setDefault(_default);
      return this;
    }

    @Override
    protected LocalTime cast(Object value) {
      if (value != null) {
        try {
          if (value instanceof String) {
            return LocalTime.parse((String) value);
          } else if (value instanceof LocalTime) {
            return (LocalTime) value;
          }
        } catch (Exception e) {
        }
      }
      return null;
    }

    public LocalTimeSchema _enum(List<LocalTime> _enum) {
      super.setEnum(_enum);
      return this;
    }

    public LocalTimeSchema addEnumItem(LocalTime _enumItem) {
      super.addEnumItemObject(_enumItem);
      return this;
    }

    @Override
    public boolean equals(java.lang.Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      return super.equals(o);
    }

    @Override
    public int hashCode() {
      return Objects.hash(super.hashCode());
    }

    @Override
    public String toString() {
      StringBuilder sb = new StringBuilder();
      sb.append("class LocalTimeSchema {\n");
      sb.append("    ").append(toIndentedString(super.toString())).append("\n");
      sb.append("}");
      return sb.toString();
    }
  }

  public class YearSchema extends Schema<Year> {

    public YearSchema() {
      super("string", "date-fullyear");
      example = Year.now();
    }

    @Override
    public YearSchema type(String type) {
      super.setType(type);
      return this;
    }

    @Override
    public YearSchema format(String format) {
      super.setFormat(format);
      return this;
    }

    public YearSchema _default(Date _default) {
      super.setDefault(_default);
      return this;
    }

    @Override
    protected Year cast(Object value) {
      if (value != null) {
        try {
          if (value instanceof String) {
            return Year.parse((String) value);
          } else if (value instanceof Year) {
            return (Year) value;
          }
        } catch (Exception e) {
        }
      }
      return null;
    }

    public YearSchema _enum(List<Year> _enum) {
      super.setEnum(_enum);
      return this;
    }

    public YearSchema addEnumItem(Year _enumItem) {
      super.addEnumItemObject(_enumItem);
      return this;
    }

    @Override
    public boolean equals(java.lang.Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      return super.equals(o);
    }

    @Override
    public int hashCode() {
      return Objects.hash(super.hashCode());
    }

    @Override
    public String toString() {
      StringBuilder sb = new StringBuilder();
      sb.append("class YearSchema {\n");
      sb.append("    ").append(toIndentedString(super.toString())).append("\n");
      sb.append("}");
      return sb.toString();
    }
  }

  @Bean
  public OpenAPI springShopOpenAPI( //
      @Value("${info.app.name}") String name, //
      @Value("${info.app.description}") String description, //
      @Value("${info.app.version}") String version //
  ) {
    SpringDocUtils.getConfig().replaceWithSchema(LocalTime.class, new LocalTimeSchema());
    SpringDocUtils.getConfig().replaceWithSchema(Year.class, new YearSchema());

    return new OpenAPI().info(new Info().title(name) //
        .description(description) //
        .version(version) //
        .license(new License() //
            .name("The MIT License (MIT)") //
            .url("https://gitlab.com/escalableapps-framework/crud-base-app/-/raw/master/LICENSE")) //
    );
  }
}
