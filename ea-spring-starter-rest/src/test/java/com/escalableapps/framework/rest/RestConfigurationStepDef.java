package com.escalableapps.framework.rest;

import static com.escalableapps.framework.rest.RestConfigurationTest.RestConfigurationTestContext.ENDPOINT;
import static com.escalableapps.framework.rest.RestConfigurationTest.RestConfigurationTestContext.EXCEPTION;
import static com.escalableapps.framework.rest.RestConfigurationTest.RestConfigurationTestContext.METHOD;
import static com.escalableapps.framework.rest.RestConfigurationTest.RestConfigurationTestContext.PARAM;
import static com.escalableapps.framework.rest.RestConfigurationTest.RestConfigurationTestContext.PARAM_VALUE;
import static com.escalableapps.framework.rest.RestConfigurationTest.RestConfigurationTestContext.RECEIVED_VALUES;
import static com.escalableapps.framework.rest.RestConfigurationTest.RestConfigurationTestContext.REQUEST_BODY;
import static com.escalableapps.framework.rest.RestConfigurationTest.RestConfigurationTestContext.RESPONSE_BODY;
import static com.escalableapps.framework.rest.RestConfigurationTest.RestConfigurationTestContext.STATUS;
import static org.apache.commons.lang3.StringUtils.join;
import static org.junit.Assert.assertEquals;

import java.nio.charset.StandardCharsets;
import java.time.OffsetDateTime;
import java.util.Map;
import java.util.TimeZone;

import org.apache.http.client.config.RequestConfig;
import org.json.JSONException;
import org.junit.Assert;
import org.skyscreamer.jsonassert.Customization;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.skyscreamer.jsonassert.comparator.CustomComparator;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;
import org.springframework.web.util.DefaultUriBuilderFactory.EncodingMode;
import org.springframework.web.util.UriTemplateHandler;
import org.springframework.web.util.UriUtils;

import com.escalableapps.framework.core.util.JsonUtils;
import com.escalableapps.framework.core.util.ResourceUtils;
import com.escalableapps.framework.cucumber.model.TestContext;
import com.escalableapps.framework.rest.RestConfigurationTest.RestConfigurationTestContext;
import com.escalableapps.framework.rest.test.RestConfigurationTestApp;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.spring.CucumberContextConfiguration;
import lombok.extern.slf4j.Slf4j;

@CucumberContextConfiguration
@SpringBootTest(classes = RestConfigurationTestApp.class, webEnvironment = WebEnvironment.RANDOM_PORT)
@Slf4j
public class RestConfigurationStepDef {

  @LocalServerPort
  private int port;

  private final TestContext<RestConfigurationTestContext> context;

  public RestConfigurationStepDef(TestContext<RestConfigurationTestContext> context) {
    this.context = context;
  }

  @Given("the default timezone {string}")
  public void theDefaultTimezone(String timezone) {
    System.setProperty("user.timezone", timezone);
    TimeZone.setDefault(TimeZone.getTimeZone(timezone));
  }

  @Given("the endpoint {string}")
  public void theEndpoint(String endpoint) {
    context.put(ENDPOINT, String.format("http://localhost:%d%s", port, endpoint));
  }

  @Given("the endpoint {string}{string}")
  public void theEndpoint(String endpoint, String path) {
    theEndpoint(join(endpoint, path));
  }

  @Given("the request with the {string} method")
  public void theRequestWithTheMethod(String method) {
    context.put(METHOD, method);
  }

  @Given("the request body is {string}")
  public void theRequestBodyIs(String body) {
    context.put(REQUEST_BODY, body);
  }

  @Given("the request parameter {string} has the value {string}")
  public void theRequestParameterHasTheValue(String param, String value) {
    context.put(PARAM, param);
    context.put(PARAM_VALUE, value);
  }

  @When("I execute the request")
  public void iExecuteTheRequest() {
    try {
      String endpoint = context.get(ENDPOINT);
      String method = context.get(METHOD);
      String param = context.get(PARAM);
      String paramValue = context.get(PARAM_VALUE);

      String uri = param == null ? context.get(ENDPOINT)
          : String.format("%s?%s=%s", endpoint, UriUtils.encode(param, StandardCharsets.UTF_8),
              UriUtils.encode(paramValue, StandardCharsets.UTF_8));
      String body = context.get(REQUEST_BODY);

      ResponseEntity<String> response = doRestRequest(method, uri, body);

      log.debug("response={}", response);

      context.put(STATUS, response.getStatusCodeValue());
      context.put(RESPONSE_BODY, response.getBody());
      context.put(RECEIVED_VALUES, response.getHeaders().getFirst(RECEIVED_VALUES.name()));

    } catch (Exception e) {
      context.put(EXCEPTION, e);
    }
  }

  private ResponseEntity<String> doRestRequest(String method, String uri, String body) {
    HttpEntity<String> requestEntity = null;
    HttpHeaders headers = new HttpHeaders();
    headers.add("Content-Type", "application/json;charset=UTF-8");
    headers.add("Accept-Language", "en-US,en");
    if (body == null) {
      requestEntity = new HttpEntity<>(headers);
    } else {
      requestEntity = new HttpEntity<>(body, headers);
    }
    RestTemplate restTemplate = new RestTemplate(requestFactory());
    restTemplate.setUriTemplateHandler(handler());
    try {
      return restTemplate.exchange(uri, HttpMethod.valueOf(method.toUpperCase()), requestEntity, String.class);
    } catch (RestClientResponseException e) {
      return new ResponseEntity<>(e.getResponseBodyAsString(), HttpStatus.valueOf(e.getRawStatusCode()));
    } catch (Exception e) {
      e.printStackTrace();
      return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  private static HttpComponentsClientHttpRequestFactory requestFactory() {
    HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory() {

      @Override
      protected RequestConfig createRequestConfig(Object client) {
        RequestConfig.Builder builder = RequestConfig.copy(super.createRequestConfig(client));
        builder.setNormalizeUri(false);
        return builder.build();
      }
    };
    return requestFactory;
  }

  private static UriTemplateHandler handler() {
    DefaultUriBuilderFactory uriFactory = new DefaultUriBuilderFactory();
    uriFactory.setEncodingMode(EncodingMode.NONE);
    return uriFactory;
  }

  @Then("I get the status code {int}")
  public void iGetTheStatusCode(Integer expectedStatusCode) {
    Integer actualStatusCode = context.get(STATUS);
    assertEquals(expectedStatusCode, actualStatusCode);
  }

  @Then("the response body contains a {string} property with the value {string}")
  public void theResponseBodyContainsAPropertyWithTheValue(String property, String expectedMessage) {
    if (!"HEAD".equals(context.get(METHOD))) {
      String json = context.get(RESPONSE_BODY);
      Map<String, String> body = JsonUtils.parseAsMap(json, String.class, String.class);
      String actual = body.get(property);

      Assert.assertEquals(expectedMessage, actual);
    }
  }

  @Then("the response body contains a {string} property with the value ")
  public void theResponseBodyContainsAPropertyWithTheValue(String property) {
    if (!"HEAD".equals(context.get(METHOD))) {
      Assert.fail();
    }
  }

  @Then("the received value is {string}")
  public void theReceivedValueIs(String expectedReceivedValue) throws JSONException {
    String actualReceivedValue = context.get(RECEIVED_VALUES);
    JSONAssert.assertEquals(expectedReceivedValue, actualReceivedValue, true);
  }

  @Then("the response body conforms to the format of the {string} template")
  public void theResponseBodyConformsToTheFormatOfTheTemplate(String templateName) throws JSONException {
    if (!"HEAD".equals(context.get(METHOD))) {
      String expectedBody = ResourceUtils.readResource("classpath:rest/data", templateName);
      String actualBody = context.get(RESPONSE_BODY);

      JSONAssert.assertEquals(expectedBody, actualBody, new CustomComparator(JSONCompareMode.NON_EXTENSIBLE, //
          new Customization("timestamp", (o1, o2) -> {

            try {
              OffsetDateTime.parse((String) o1);
              return true;
            } catch (Exception e) {
              return false;
            }

          }), //
          new Customization("message", (o1, o2) -> true) //
      ));
    }
  }
}
