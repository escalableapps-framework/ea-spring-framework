package com.escalableapps.framework.rest.test.api;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.HEAD;
import static org.springframework.web.bind.annotation.RequestMethod.PATCH;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.Year;
import java.util.Optional;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.escalableapps.framework.rest.RestConfigurationTest.BigDecimalValidationGroup;
import com.escalableapps.framework.rest.RestConfigurationTest.BigIntegerValidationGroup;
import com.escalableapps.framework.rest.RestConfigurationTest.BooleanValidationGroup;
import com.escalableapps.framework.rest.RestConfigurationTest.DoubleValidationGroup;
import com.escalableapps.framework.rest.RestConfigurationTest.EnumAB;
import com.escalableapps.framework.rest.RestConfigurationTest.EnumABGroup;
import com.escalableapps.framework.rest.RestConfigurationTest.EnumLowerAB;
import com.escalableapps.framework.rest.RestConfigurationTest.EnumLowerABGroup;
import com.escalableapps.framework.rest.RestConfigurationTest.FloatValidationGroup;
import com.escalableapps.framework.rest.RestConfigurationTest.IntegerValidationGroup;
import com.escalableapps.framework.rest.RestConfigurationTest.LocalDateTimeValidationGroup;
import com.escalableapps.framework.rest.RestConfigurationTest.LocalDateValidationGroup;
import com.escalableapps.framework.rest.RestConfigurationTest.LocalTimeValidationGroup;
import com.escalableapps.framework.rest.RestConfigurationTest.LongValidationGroup;
import com.escalableapps.framework.rest.RestConfigurationTest.OffsetDateTimeValidationGroup;
import com.escalableapps.framework.rest.RestConfigurationTest.ParentUnamedNotNullDto;
import com.escalableapps.framework.rest.RestConfigurationTest.ParentUnamedNotNullOptionalDto;
import com.escalableapps.framework.rest.RestConfigurationTest.StringValidationGroup;
import com.escalableapps.framework.rest.RestConfigurationTest.UnamedNotNullDto;
import com.escalableapps.framework.rest.RestConfigurationTest.UnamedNotNullOptionalDto;
import com.escalableapps.framework.rest.RestConfigurationTest.YearValidationGroup;

@RestController
@RequestMapping("/api/v1/request-parameter/not-null")
@Validated
public class RequestParamConstrainedNotNullRestApi {

  @RequestMapping(path = "/method-parameter/paramString", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamString(@RequestParam(required = false) @NotNull String paramString) {
    return null;
  }

  @RequestMapping(path = "/method-parameter/paramInteger", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamInteger(
      @RequestParam(required = false) @NotNull Integer paramInteger) {
    return null;
  }

  @RequestMapping(path = "/method-parameter/paramLong", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamLong(@RequestParam(required = false) @NotNull Long paramLong) {
    return null;
  }

  @RequestMapping(path = "/method-parameter/paramBigInteger", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamBigInteger(
      @RequestParam(required = false) @NotNull BigInteger paramBigInteger) {
    return null;
  }

  @RequestMapping(path = "/method-parameter/paramFloat", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamFloat(@RequestParam(required = false) @NotNull Float paramFloat) {
    return null;
  }

  @RequestMapping(path = "/method-parameter/paramDouble", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamDouble(@RequestParam(required = false) @NotNull Double paramDouble) {
    return null;
  }

  @RequestMapping(path = "/method-parameter/paramBigDecimal", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamBigDecimal(
      @RequestParam(required = false) @NotNull BigDecimal paramBigDecimal) {
    return null;
  }

  @RequestMapping(path = "/method-parameter/paramBoolean", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamBoolean(
      @RequestParam(required = false) @NotNull Boolean paramBoolean) {
    return null;
  }

  @RequestMapping(path = "/method-parameter/paramOffsetDateTime", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamOffsetDateTime(
      @RequestParam(required = false) @NotNull OffsetDateTime paramOffsetDateTime) {
    return null;
  }

  @RequestMapping(path = "/method-parameter/paramLocalDateTime", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamLocalDateTime(
      @RequestParam(required = false) @NotNull LocalDateTime paramLocalDateTime) {
    return null;
  }

  @RequestMapping(path = "/method-parameter/paramLocalDate", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamLocalDate(
      @RequestParam(required = false) @NotNull LocalDate paramLocalDate) {
    return null;
  }

  @RequestMapping(path = "/method-parameter/paramLocalTime", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamLocalTime(
      @RequestParam(required = false) @NotNull LocalTime paramLocalTime) {
    return null;
  }

  @RequestMapping(path = "/method-parameter/paramYear", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamYear(@RequestParam(required = false) @NotNull Year paramYear) {
    return null;
  }

  @RequestMapping(path = "/method-parameter/paramEnumAB", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamEnumAB(@RequestParam(required = false) @NotNull EnumAB paramEnumAB) {
    return null;
  }

  @RequestMapping(path = "/method-parameter/paramEnumLowerAB", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamEnumLowerAB(
      @RequestParam(required = false) @NotNull EnumLowerAB paramEnumLowerAB) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/paramString", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamString(
      @RequestParam(required = true, name = "param-string") @NotNull String paramString) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/paramInteger", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamInteger(
      @RequestParam(required = true, name = "param-integer") @NotNull Integer paramInteger) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/paramLong", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamLong(
      @RequestParam(required = true, name = "param-long") @NotNull Long paramLong) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/paramBigInteger", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamBigInteger(
      @RequestParam(required = true, name = "param-big-integer") @NotNull BigInteger paramBigInteger) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/paramFloat", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamFloat(
      @RequestParam(required = true, name = "param-float") @NotNull Float paramFloat) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/paramDouble", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamDouble(
      @RequestParam(required = true, name = "param-double") @NotNull Double paramDouble) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/paramBigDecimal", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamBigDecimal(
      @RequestParam(required = true, name = "param-big-decimal") @NotNull BigDecimal paramBigDecimal) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/paramBoolean", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamBoolean(
      @RequestParam(required = true, name = "param-boolean") @NotNull Boolean paramBoolean) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/paramOffsetDateTime", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamOffsetDateTime(
      @RequestParam(required = true, name = "param-offset-date-time") @NotNull OffsetDateTime paramOffsetDateTime) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/paramLocalDateTime", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamLocalDateTime(
      @RequestParam(required = true, name = "param-local-date-time") @NotNull LocalDateTime paramLocalDateTime) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/paramLocalDate", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamLocalDate(
      @RequestParam(required = true, name = "param-local-date") @NotNull LocalDate paramLocalDate) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/paramLocalTime", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamLocalTime(
      @RequestParam(required = true, name = "param-local-time") @NotNull LocalTime paramLocalTime) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/paramYear", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamYear(
      @RequestParam(required = true, name = "param-year") @NotNull Year paramYear) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/paramEnumAB", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamEnumAB(
      @RequestParam(required = true, name = "param-enum-ab") @NotNull EnumAB paramEnumAB) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/paramEnumLowerAB", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamEnumLowerAB(
      @RequestParam(required = true, name = "param-enum-lower-ab") @NotNull EnumLowerAB paramEnumLowerAB) {
    return null;
  }

  @RequestMapping(path = "/optional-method-parameter/paramString", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> optionalMethodParameterParamString(
      @RequestParam(required = false) Optional<@NotNull String> paramString) {
    return null;
  }

  @RequestMapping(path = "/optional-method-parameter/paramInteger", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> optionalMethodParameterParamInteger(
      @RequestParam(required = false) Optional<@NotNull Integer> paramInteger) {
    return null;
  }

  @RequestMapping(path = "/optional-method-parameter/paramLong", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> optionalMethodParameterParamLong(
      @RequestParam(required = false) Optional<@NotNull Long> paramLong) {
    return null;
  }

  @RequestMapping(path = "/optional-method-parameter/paramBigInteger", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> optionalMethodParameterParamBigInteger(
      @RequestParam(required = false) Optional<@NotNull BigInteger> paramBigInteger) {
    return null;
  }

  @RequestMapping(path = "/optional-method-parameter/paramFloat", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> optionalMethodParameterParamFloat(
      @RequestParam(required = false) Optional<@NotNull Float> paramFloat) {
    return null;
  }

  @RequestMapping(path = "/optional-method-parameter/paramDouble", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> optionalMethodParameterParamDouble(
      @RequestParam(required = false) Optional<@NotNull Double> paramDouble) {
    return null;
  }

  @RequestMapping(path = "/optional-method-parameter/paramBigDecimal", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> optionalMethodParameterParamBigDecimal(
      @RequestParam(required = false) Optional<@NotNull BigDecimal> paramBigDecimal) {
    return null;
  }

  @RequestMapping(path = "/optional-method-parameter/paramBoolean", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> optionalMethodParameterParamBoolean(
      @RequestParam(required = false) Optional<@NotNull Boolean> paramBoolean) {
    return null;
  }

  @RequestMapping(path = "/optional-method-parameter/paramOffsetDateTime", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> optionalMethodParameterParamOffsetDateTime(
      @RequestParam(required = false) Optional<@NotNull OffsetDateTime> paramOffsetDateTime) {
    return null;
  }

  @RequestMapping(path = "/optional-method-parameter/paramLocalDateTime", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> optionalMethodParameterParamLocalDateTime(
      @RequestParam(required = false) Optional<@NotNull LocalDateTime> paramLocalDateTime) {
    return null;
  }

  @RequestMapping(path = "/optional-method-parameter/paramLocalDate", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> optionalMethodParameterParamLocalDate(
      @RequestParam(required = false) Optional<@NotNull LocalDate> paramLocalDate) {
    return null;
  }

  @RequestMapping(path = "/optional-method-parameter/paramLocalTime", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> optionalMethodParameterParamLocalTime(
      @RequestParam(required = false) Optional<@NotNull LocalTime> paramLocalTime) {
    return null;
  }

  @RequestMapping(path = "/optional-method-parameter/paramYear", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> optionalMethodParameterParamYear(
      @RequestParam(required = false) Optional<@NotNull Year> paramYear) {
    return null;
  }

  @RequestMapping(path = "/optional-method-parameter/paramEnumAB", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> optionalMethodParameterParamEnumAB(
      @RequestParam(required = false) Optional<@NotNull EnumAB> paramEnumAB) {
    return null;
  }

  @RequestMapping(path = "/optional-method-parameter/paramEnumLowerAB", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> optionalMethodParameterParamEnumLowerAB(
      @RequestParam(required = false) Optional<@NotNull EnumLowerAB> paramEnumLowerAB) {
    return null;
  }

  @RequestMapping(path = "/named-optional-method-parameter/paramString", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedOptionalMethodParameterParamString(
      @RequestParam(required = true, name = "param-string") Optional<@NotNull String> paramString) {
    return null;
  }

  @RequestMapping(path = "/named-optional-method-parameter/paramInteger", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> namedOptionalMethodParameterParamInteger(
      @RequestParam(required = true, name = "param-integer") Optional<@NotNull Integer> paramInteger) {
    return null;
  }

  @RequestMapping(path = "/named-optional-method-parameter/paramLong", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedOptionalMethodParameterParamLong(
      @RequestParam(required = true, name = "param-long") Optional<@NotNull Long> paramLong) {
    return null;
  }

  @RequestMapping(path = "/named-optional-method-parameter/paramBigInteger", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> namedOptionalMethodParameterParamBigInteger(
      @RequestParam(required = true, name = "param-big-integer") Optional<@NotNull BigInteger> paramBigInteger) {
    return null;
  }

  @RequestMapping(path = "/named-optional-method-parameter/paramFloat", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedOptionalMethodParameterParamFloat(
      @RequestParam(required = true, name = "param-float") Optional<@NotNull Float> paramFloat) {
    return null;
  }

  @RequestMapping(path = "/named-optional-method-parameter/paramDouble", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedOptionalMethodParameterParamDouble(
      @RequestParam(required = true, name = "param-double") Optional<@NotNull Double> paramDouble) {
    return null;
  }

  @RequestMapping(path = "/named-optional-method-parameter/paramBigDecimal", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> namedOptionalMethodParameterParamBigDecimal(
      @RequestParam(required = true, name = "param-big-decimal") Optional<@NotNull BigDecimal> paramBigDecimal) {
    return null;
  }

  @RequestMapping(path = "/named-optional-method-parameter/paramBoolean", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> namedOptionalMethodParameterParamBoolean(
      @RequestParam(required = true, name = "param-boolean") Optional<@NotNull Boolean> paramBoolean) {
    return null;
  }

  @RequestMapping(path = "/named-optional-method-parameter/paramOffsetDateTime", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> namedOptionalMethodParameterParamOffsetDateTime(
      @RequestParam(required = true, name = "param-offset-date-time") Optional<@NotNull OffsetDateTime> paramOffsetDateTime) {
    return null;
  }

  @RequestMapping(path = "/named-optional-method-parameter/paramLocalDateTime", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> namedOptionalMethodParameterParamLocalDateTime(
      @RequestParam(required = true, name = "param-local-date-time") Optional<@NotNull LocalDateTime> paramLocalDateTime) {
    return null;
  }

  @RequestMapping(path = "/named-optional-method-parameter/paramLocalDate", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> namedOptionalMethodParameterParamLocalDate(
      @RequestParam(required = true, name = "param-local-date") Optional<@NotNull LocalDate> paramLocalDate) {
    return null;
  }

  @RequestMapping(path = "/named-optional-method-parameter/paramLocalTime", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> namedOptionalMethodParameterParamLocalTime(
      @RequestParam(required = true, name = "param-local-time") Optional<@NotNull LocalTime> paramLocalTime) {
    return null;
  }

  @RequestMapping(path = "/named-optional-method-parameter/paramYear", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedOptionalMethodParameterParamYear(
      @RequestParam(required = true, name = "param-year") Optional<@NotNull Year> paramYear) {
    return null;
  }

  @RequestMapping(path = "/named-optional-method-parameter/paramEnumAB", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedOptionalMethodParameterParamEnumAB(
      @RequestParam(required = true, name = "param-enum-ab") Optional<@NotNull EnumAB> paramEnumAB) {
    return null;
  }

  @RequestMapping(path = "/named-optional-method-parameter/paramEnumLowerAB", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> namedOptionalMethodParameterParamEnumLowerAB(
      @RequestParam(required = true, name = "param-enum-lower-ab") Optional<@NotNull EnumLowerAB> paramEnumLowerAB) {
    return null;
  }

  @RequestMapping(path = "/dto-attribute/paramString", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated({StringValidationGroup.class})
  public ResponseEntity<Void> dtoAttributeParamString(@Valid UnamedNotNullDto params) {
    return null;
  }

  @RequestMapping(path = "/dto-attribute/paramInteger", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(IntegerValidationGroup.class)
  public ResponseEntity<Void> dtoAttributeParamInteger(@Valid UnamedNotNullDto params) {
    return null;
  }

  @RequestMapping(path = "/dto-attribute/paramLong", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(LongValidationGroup.class)
  public ResponseEntity<Void> dtoAttributeParamLong(@Valid UnamedNotNullDto params) {
    return null;
  }

  @RequestMapping(path = "/dto-attribute/paramBigInteger", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(BigIntegerValidationGroup.class)
  public ResponseEntity<Void> dtoAttributeParamBigInteger(@Valid UnamedNotNullDto params) {
    return null;
  }

  @RequestMapping(path = "/dto-attribute/paramFloat", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(FloatValidationGroup.class)
  public ResponseEntity<Void> dtoAttributeParamFloat(@Valid UnamedNotNullDto params) {
    return null;
  }

  @RequestMapping(path = "/dto-attribute/paramDouble", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(DoubleValidationGroup.class)
  public ResponseEntity<Void> dtoAttributeParamDouble(@Valid UnamedNotNullDto params) {
    return null;
  }

  @RequestMapping(path = "/dto-attribute/paramBigDecimal", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(BigDecimalValidationGroup.class)
  public ResponseEntity<Void> dtoAttributeParamBigDecimal(@Valid UnamedNotNullDto params) {
    return null;
  }

  @RequestMapping(path = "/dto-attribute/paramBoolean", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(BooleanValidationGroup.class)
  public ResponseEntity<Void> dtoAttributeParamBoolean(@Valid UnamedNotNullDto params) {
    return null;
  }

  @RequestMapping(path = "/dto-attribute/paramOffsetDateTime", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(OffsetDateTimeValidationGroup.class)
  public ResponseEntity<Void> dtoAttributeParamOffsetDateTime(@Valid UnamedNotNullDto params) {
    return null;
  }

  @RequestMapping(path = "/dto-attribute/paramLocalDateTime", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(LocalDateTimeValidationGroup.class)
  public ResponseEntity<Void> dtoAttributeParamLocalDateTime(@Valid UnamedNotNullDto params) {
    return null;
  }

  @RequestMapping(path = "/dto-attribute/paramLocalDate", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(LocalDateValidationGroup.class)
  public ResponseEntity<Void> dtoAttributeParamLocalDate(@Valid UnamedNotNullDto params) {
    return null;
  }

  @RequestMapping(path = "/dto-attribute/paramLocalTime", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(LocalTimeValidationGroup.class)
  public ResponseEntity<Void> dtoAttributeParamLocalTime(@Valid UnamedNotNullDto params) {
    return null;
  }

  @RequestMapping(path = "/dto-attribute/paramYear", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(YearValidationGroup.class)
  public ResponseEntity<Void> dtoAttributeParamYear(@Valid UnamedNotNullDto params) {
    return null;
  }

  @RequestMapping(path = "/dto-attribute/paramEnumAB", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(EnumABGroup.class)
  public ResponseEntity<Void> dtoAttributeParamEnumAB(@Valid UnamedNotNullDto params) {
    return null;
  }

  @RequestMapping(path = "/dto-attribute/paramEnumLowerAB", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(EnumLowerABGroup.class)
  public ResponseEntity<Void> dtoAttributeParamEnumLowerAB(@Valid UnamedNotNullDto params) {
    return null;
  }

  @RequestMapping(path = "/optional-dto-attribute/paramString", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated({StringValidationGroup.class})
  public ResponseEntity<Void> optionalDtoAttributeParamString(@Valid UnamedNotNullOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/optional-dto-attribute/paramInteger", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(IntegerValidationGroup.class)
  public ResponseEntity<Void> optionalDtoAttributeParamInteger(@Valid UnamedNotNullOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/optional-dto-attribute/paramLong", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(LongValidationGroup.class)
  public ResponseEntity<Void> optionalDtoAttributeParamLong(@Valid UnamedNotNullOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/optional-dto-attribute/paramBigInteger", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(BigIntegerValidationGroup.class)
  public ResponseEntity<Void> optionalDtoAttributeParamBigInteger(@Valid UnamedNotNullOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/optional-dto-attribute/paramFloat", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(FloatValidationGroup.class)
  public ResponseEntity<Void> optionalDtoAttributeParamFloat(@Valid UnamedNotNullOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/optional-dto-attribute/paramDouble", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(DoubleValidationGroup.class)
  public ResponseEntity<Void> optionalDtoAttributeParamDouble(@Valid UnamedNotNullOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/optional-dto-attribute/paramBigDecimal", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(BigDecimalValidationGroup.class)
  public ResponseEntity<Void> optionalDtoAttributeParamBigDecimal(@Valid UnamedNotNullOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/optional-dto-attribute/paramBoolean", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(BooleanValidationGroup.class)
  public ResponseEntity<Void> optionalDtoAttributeParamBoolean(@Valid UnamedNotNullOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/optional-dto-attribute/paramOffsetDateTime", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(OffsetDateTimeValidationGroup.class)
  public ResponseEntity<Void> optionalDtoAttributeParamOffsetDateTime(@Valid UnamedNotNullOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/optional-dto-attribute/paramLocalDateTime", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(LocalDateTimeValidationGroup.class)
  public ResponseEntity<Void> optionalDtoAttributeParamLocalDateTime(@Valid UnamedNotNullOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/optional-dto-attribute/paramLocalDate", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(LocalDateValidationGroup.class)
  public ResponseEntity<Void> optionalDtoAttributeParamLocalDate(@Valid UnamedNotNullOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/optional-dto-attribute/paramLocalTime", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(LocalTimeValidationGroup.class)
  public ResponseEntity<Void> optionalDtoAttributeParamLocalTime(@Valid UnamedNotNullOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/optional-dto-attribute/paramYear", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(YearValidationGroup.class)
  public ResponseEntity<Void> optionalDtoAttributeParamYear(@Valid UnamedNotNullOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/optional-dto-attribute/paramEnumAB", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(EnumABGroup.class)
  public ResponseEntity<Void> optionalDtoAttributeParamEnumAB(@Valid UnamedNotNullOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/optional-dto-attribute/paramEnumLowerAB", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(EnumLowerABGroup.class)
  public ResponseEntity<Void> optionalDtoAttributeParamEnumLowerAB(@Valid UnamedNotNullOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-dto-attribute/paramString", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated({StringValidationGroup.class})
  public ResponseEntity<Void> nestedDtoAttributeParamString(@Valid ParentUnamedNotNullDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-dto-attribute/paramInteger", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(IntegerValidationGroup.class)
  public ResponseEntity<Void> nestedDtoAttributeParamInteger(@Valid ParentUnamedNotNullDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-dto-attribute/paramLong", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(LongValidationGroup.class)
  public ResponseEntity<Void> nestedDtoAttributeParamLong(@Valid ParentUnamedNotNullDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-dto-attribute/paramBigInteger", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(BigIntegerValidationGroup.class)
  public ResponseEntity<Void> nestedDtoAttributeParamBigInteger(@Valid ParentUnamedNotNullDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-dto-attribute/paramFloat", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(FloatValidationGroup.class)
  public ResponseEntity<Void> nestedDtoAttributeParamFloat(@Valid ParentUnamedNotNullDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-dto-attribute/paramDouble", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(DoubleValidationGroup.class)
  public ResponseEntity<Void> nestedDtoAttributeParamDouble(@Valid ParentUnamedNotNullDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-dto-attribute/paramBigDecimal", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(BigDecimalValidationGroup.class)
  public ResponseEntity<Void> nestedDtoAttributeParamBigDecimal(@Valid ParentUnamedNotNullDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-dto-attribute/paramBoolean", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(BooleanValidationGroup.class)
  public ResponseEntity<Void> nestedDtoAttributeParamBoolean(@Valid ParentUnamedNotNullDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-dto-attribute/paramOffsetDateTime", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(OffsetDateTimeValidationGroup.class)
  public ResponseEntity<Void> nestedDtoAttributeParamOffsetDateTime(@Valid ParentUnamedNotNullDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-dto-attribute/paramLocalDateTime", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(LocalDateTimeValidationGroup.class)
  public ResponseEntity<Void> nestedDtoAttributeParamLocalDateTime(@Valid ParentUnamedNotNullDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-dto-attribute/paramLocalDate", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(LocalDateValidationGroup.class)
  public ResponseEntity<Void> nestedDtoAttributeParamLocalDate(@Valid ParentUnamedNotNullDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-dto-attribute/paramLocalTime", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(LocalTimeValidationGroup.class)
  public ResponseEntity<Void> nestedDtoAttributeParamLocalTime(@Valid ParentUnamedNotNullDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-dto-attribute/paramYear", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(YearValidationGroup.class)
  public ResponseEntity<Void> nestedDtoAttributeParamYear(@Valid ParentUnamedNotNullDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-dto-attribute/paramEnumAB", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(EnumABGroup.class)
  public ResponseEntity<Void> nestedDtoAttributeParamEnumAB(@Valid ParentUnamedNotNullDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-dto-attribute/paramEnumLowerAB", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(EnumLowerABGroup.class)
  public ResponseEntity<Void> nestedDtoAttributeParamEnumLowerAB(@Valid ParentUnamedNotNullDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-optional-dto-attribute/paramString", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated({StringValidationGroup.class})
  public ResponseEntity<Void> nestedOptionalDtoAttributeParamString(@Valid ParentUnamedNotNullOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-optional-dto-attribute/paramInteger", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(IntegerValidationGroup.class)
  public ResponseEntity<Void> nestedOptionalDtoAttributeParamInteger(@Valid ParentUnamedNotNullOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-optional-dto-attribute/paramLong", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(LongValidationGroup.class)
  public ResponseEntity<Void> nestedOptionalDtoAttributeParamLong(@Valid ParentUnamedNotNullOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-optional-dto-attribute/paramBigInteger", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  @Validated(BigIntegerValidationGroup.class)
  public ResponseEntity<Void> nestedOptionalDtoAttributeParamBigInteger(@Valid ParentUnamedNotNullOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-optional-dto-attribute/paramFloat", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(FloatValidationGroup.class)
  public ResponseEntity<Void> nestedOptionalDtoAttributeParamFloat(@Valid ParentUnamedNotNullOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-optional-dto-attribute/paramDouble", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(DoubleValidationGroup.class)
  public ResponseEntity<Void> nestedOptionalDtoAttributeParamDouble(@Valid ParentUnamedNotNullOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-optional-dto-attribute/paramBigDecimal", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  @Validated(BigDecimalValidationGroup.class)
  public ResponseEntity<Void> nestedOptionalDtoAttributeParamBigDecimal(@Valid ParentUnamedNotNullOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-optional-dto-attribute/paramBoolean", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(BooleanValidationGroup.class)
  public ResponseEntity<Void> nestedOptionalDtoAttributeParamBoolean(@Valid ParentUnamedNotNullOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-optional-dto-attribute/paramOffsetDateTime", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  @Validated(OffsetDateTimeValidationGroup.class)
  public ResponseEntity<Void> nestedOptionalDtoAttributeParamOffsetDateTime(
      @Valid ParentUnamedNotNullOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-optional-dto-attribute/paramLocalDateTime", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  @Validated(LocalDateTimeValidationGroup.class)
  public ResponseEntity<Void> nestedOptionalDtoAttributeParamLocalDateTime(
      @Valid ParentUnamedNotNullOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-optional-dto-attribute/paramLocalDate", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  @Validated(LocalDateValidationGroup.class)
  public ResponseEntity<Void> nestedOptionalDtoAttributeParamLocalDate(@Valid ParentUnamedNotNullOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-optional-dto-attribute/paramLocalTime", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  @Validated(LocalTimeValidationGroup.class)
  public ResponseEntity<Void> nestedOptionalDtoAttributeParamLocalTime(@Valid ParentUnamedNotNullOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-optional-dto-attribute/paramYear", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(YearValidationGroup.class)
  public ResponseEntity<Void> nestedOptionalDtoAttributeParamYear(@Valid ParentUnamedNotNullOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-optional-dto-attribute/paramEnumAB", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(EnumABGroup.class)
  public ResponseEntity<Void> nestedOptionalDtoAttributeParamEnumAB(@Valid ParentUnamedNotNullOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-optional-dto-attribute/paramEnumLowerAB", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  @Validated(EnumLowerABGroup.class)
  public ResponseEntity<Void> nestedOptionalDtoAttributeParamEnumLowerAB(@Valid ParentUnamedNotNullOptionalDto params) {
    return null;
  }
}
