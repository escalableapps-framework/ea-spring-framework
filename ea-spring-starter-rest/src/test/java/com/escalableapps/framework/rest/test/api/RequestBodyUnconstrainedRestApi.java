package com.escalableapps.framework.rest.test.api;

import static com.escalableapps.framework.rest.RestConfigurationTest.RestConfigurationTestContext.RECEIVED_VALUES;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.PATCH;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.escalableapps.framework.core.util.JsonUtils;
import com.escalableapps.framework.rest.RestConfigurationTest.NamedNotNullDto;
import com.escalableapps.framework.rest.RestConfigurationTest.NamedNotNullOptionalDto;
import com.escalableapps.framework.rest.RestConfigurationTest.ParentNamedNotNullDto;
import com.escalableapps.framework.rest.RestConfigurationTest.ParentNamedNotNullOptionalDto;
import com.escalableapps.framework.rest.RestConfigurationTest.ParentUnamedNotNullDto;
import com.escalableapps.framework.rest.RestConfigurationTest.ParentUnamedNotNullOptionalDto;
import com.escalableapps.framework.rest.RestConfigurationTest.UnamedNotNullDto;
import com.escalableapps.framework.rest.RestConfigurationTest.UnamedNotNullOptionalDto;

@RestController
@RequestMapping("/api/v1/request-body")
public class RequestBodyUnconstrainedRestApi {

  @RequestMapping(path = "/dto-attribute", method = {POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> dtoAttribute(@RequestBody UnamedNotNullDto body) {
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(body)).build();
  }

  @RequestMapping(path = "/named-dto-attribute", method = {POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedDtoAttribute(@RequestBody NamedNotNullDto body) {
    UnamedNotNullDto head = new UnamedNotNullDto();
    BeanUtils.copyProperties(body, head);
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(head)).build();
  }

  @RequestMapping(path = "/optional-dto-attribute", method = {POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> optionalDtoAttribute(@RequestBody UnamedNotNullOptionalDto body) {
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(body)).build();
  }

  @RequestMapping(path = "/named-optional-dto-attribute", method = {POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedOptionalDtoAttribute(@RequestBody NamedNotNullOptionalDto body) {
    UnamedNotNullOptionalDto head = new UnamedNotNullOptionalDto();
    BeanUtils.copyProperties(body, head);
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(head)).build();
  }

  @RequestMapping(path = "/nested-dto-attribute", method = {POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> nestedDtoAttribute(@RequestBody ParentUnamedNotNullDto body) {
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(body)).build();
  }

  @RequestMapping(path = "/nested-optional-dto-attribute", method = {POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> nestedOptionalDtoAttribute(@RequestBody ParentUnamedNotNullOptionalDto body) {
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(body)).build();
  }

  @RequestMapping(path = "/named-nested-dto-attribute", method = {POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedNestedDtoAttribute(@RequestBody ParentNamedNotNullDto body) {
    UnamedNotNullDto nested = null;
    if (body.getNested() != null) {
      nested = new UnamedNotNullDto();
      BeanUtils.copyProperties(body.getNested(), nested);
    }
    List<UnamedNotNullDto> array = null;
    if (body.getArray() != null) {
      array = body.getArray().stream().map(named -> {
        UnamedNotNullDto dto = new UnamedNotNullDto();
        BeanUtils.copyProperties(named, dto);
        return dto;
      }).collect(Collectors.toList());
    }

    ParentUnamedNotNullDto head = ParentUnamedNotNullDto.builder().nested(nested).array(array).build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(head)).build();
  }

  @RequestMapping(path = "/named-nested-optional-dto-attribute", method = {POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedNestedOptionalDtoAttribute(@RequestBody ParentNamedNotNullOptionalDto body) {
    UnamedNotNullOptionalDto nested = null;
    if (body.getNested() != null) {
      nested = new UnamedNotNullOptionalDto();
      BeanUtils.copyProperties(body.getNested(), nested);
    }
    List<UnamedNotNullOptionalDto> array = null;
    if (body.getArray() != null) {
      array = body.getArray().stream().map(named -> {
        UnamedNotNullOptionalDto dto = new UnamedNotNullOptionalDto();
        BeanUtils.copyProperties(named, dto);
        return dto;
      }).collect(Collectors.toList());
    }

    ParentUnamedNotNullOptionalDto head = ParentUnamedNotNullOptionalDto.builder().nested(nested).array(array).build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(head)).build();
  }
}
