package com.escalableapps.framework.rest.test;

import static org.apache.commons.lang3.StringUtils.isEmpty;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.escalableapps.framework.core.model.jackson.JacksonDeserializer;
import com.escalableapps.framework.core.model.jackson.JacksonSerializer;
import com.escalableapps.framework.rest.RestConfigurationTest.EnumLowerAB;
import com.escalableapps.framework.rest.model.RequestParamConverter;

@SpringBootApplication
public class RestConfigurationTestApp {

  private static class EnumLowerABRequestParamConverter implements RequestParamConverter<EnumLowerAB> {

    @Override
    public EnumLowerAB convert(String source) {
      return isEmpty(source) ? null : EnumLowerAB.getFromProperty(source);
    }
  }

  private static class EnumLowerABJacksonDeserializer extends JacksonDeserializer<EnumLowerAB> {

    protected EnumLowerABJacksonDeserializer() {
      super(EnumLowerAB.class);
    }

    @Override
    protected EnumLowerAB deserialize(String source) {
      return EnumLowerAB.getFromProperty(source);
    }
  }

  private static class EnumLowerABJacksonSerializer extends JacksonSerializer<EnumLowerAB> {

    private static final long serialVersionUID = 503295831637849387L;

    protected EnumLowerABJacksonSerializer() {
      super(EnumLowerAB.class);
    }

    @Override
    protected String serialize(EnumLowerAB object) {
      return object.getProperty();
    }

  }

  @Bean
  public RequestParamConverter<EnumLowerAB> enumLowerABRequestParamConverter() {
    return new EnumLowerABRequestParamConverter();
  }

  @Bean
  public JacksonDeserializer<EnumLowerAB> enumLowerABJacksonDeserializer() {
    return new EnumLowerABJacksonDeserializer();
  }

  @Bean
  public JacksonSerializer<EnumLowerAB> enumLowerABJacksonSerializer() {
    return new EnumLowerABJacksonSerializer();
  }
}
