package com.escalableapps.framework.rest;

import static java.util.stream.Stream.of;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.Year;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.junit.runner.RunWith;

import com.escalableapps.framework.core.validation.constraints.DateRange;
import com.escalableapps.framework.core.validation.constraints.DatetimeRange;
import com.escalableapps.framework.core.validation.constraints.YearRange;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@RunWith(Cucumber.class)
@CucumberOptions( //
    features = "classpath:rest", //
    glue = "classpath:com.escalableapps.framework.rest" //
)
public class RestConfigurationTest {
  public static enum RestConfigurationTestContext {
    ENDPOINT, METHOD, PARAM, PARAM_VALUE, STATUS, REQUEST_BODY, RESPONSE_BODY, EXCEPTION, RECEIVED_VALUES
  }

  public enum EnumAB {
    A, B;
  }

  @Getter
  @RequiredArgsConstructor
  @Schema(type = "string", format = "query", allowableValues = {"a", "b"})
  public enum EnumLowerAB {

    A("a"), B("b");

    private final String property;

    public static EnumLowerAB getFromProperty(String property) {
      return of(values()).filter(enumValue -> enumValue.getProperty().equals(property)).findFirst().get();
    }
  }

  public interface StringValidationGroup {
  }

  public interface IntegerValidationGroup {
  }

  public interface LongValidationGroup {
  }

  public interface BigIntegerValidationGroup {
  }

  public interface FloatValidationGroup {
  }

  public interface DoubleValidationGroup {
  }

  public interface BigDecimalValidationGroup {
  }

  public interface BooleanValidationGroup {
  }

  public interface OffsetDateTimeValidationGroup {
  }

  public interface LocalDateTimeValidationGroup {
  }

  public interface LocalDateValidationGroup {
  }

  public interface LocalTimeValidationGroup {
  }

  public interface YearValidationGroup {
  }

  public interface EnumABGroup {
  }

  public interface EnumLowerABGroup {
  }

  @AllArgsConstructor
  @Builder
  @Data
  @NoArgsConstructor
  public static class NamedNotNullDto {

    @NotNull(groups = BigDecimalValidationGroup.class)
    @JsonProperty("param-big-decimal")
    private BigDecimal paramBigDecimal;

    @NotNull(groups = BigIntegerValidationGroup.class)
    @JsonProperty("param-big-integer")
    private BigInteger paramBigInteger;

    @NotNull(groups = BooleanValidationGroup.class)
    @JsonProperty("param-boolean")
    private Boolean paramBoolean;

    @NotNull(groups = DoubleValidationGroup.class)
    @JsonProperty("param-double")
    private Double paramDouble;

    @NotNull(groups = EnumABGroup.class)
    @JsonProperty("param-enum-ab")
    private EnumAB paramEnumAB;

    @NotNull(groups = EnumLowerABGroup.class)
    @JsonProperty("param-enum-lower-ab")
    private EnumLowerAB paramEnumLowerAB;

    @NotNull(groups = FloatValidationGroup.class)
    @JsonProperty("param-float")
    private Float paramFloat;

    @NotNull(groups = IntegerValidationGroup.class)
    @JsonProperty("param-integer")
    private Integer paramInteger;

    @NotNull(groups = LocalDateValidationGroup.class)
    @JsonProperty("param-local-date")
    private LocalDate paramLocalDate;

    @NotNull(groups = LocalDateTimeValidationGroup.class)
    @JsonProperty("param-local-date-time")
    private LocalDateTime paramLocalDateTime;

    @NotNull(groups = LocalTimeValidationGroup.class)
    @JsonProperty("param-local-time")
    private LocalTime paramLocalTime;

    @NotNull(groups = LongValidationGroup.class)
    @JsonProperty("param-long")
    private Long paramLong;

    @NotNull(groups = OffsetDateTimeValidationGroup.class)
    @JsonProperty("param-offset-date-time")
    private OffsetDateTime paramOffsetDateTime;

    @NotNull(groups = StringValidationGroup.class)
    @JsonProperty("param-string")
    private String paramString;

    @NotNull(groups = YearValidationGroup.class)
    @JsonProperty("param-year")
    private Year paramYear;
  }

  @AllArgsConstructor
  @Builder
  @Data
  @NoArgsConstructor
  public static class NamedConstrainedDto {

    @DecimalMax(value = "9.9", groups = BigDecimalValidationGroup.class)
    @JsonProperty("param-big-decimal")
    private BigDecimal paramBigDecimal;

    @DecimalMax(value = "1", groups = BigIntegerValidationGroup.class)
    @JsonProperty("param-big-integer")
    private BigInteger paramBigInteger;

    @Max(value = 1, groups = IntegerValidationGroup.class)
    @JsonProperty("param-integer")
    private Integer paramInteger;

    @DateRange(max = "2000-01-01", groups = LocalDateValidationGroup.class)
    @JsonProperty("param-local-date")
    private LocalDate paramLocalDate;

    @DatetimeRange(max = "2000-01-01T00:00:00Z", groups = LocalDateTimeValidationGroup.class)
    @JsonProperty("param-local-date-time")
    private LocalDateTime paramLocalDateTime;

    @Max(value = 1, groups = LongValidationGroup.class)
    @JsonProperty("param-long")
    private Long paramLong;

    @DatetimeRange(max = "2000-01-01T00:00:00Z", groups = OffsetDateTimeValidationGroup.class)
    @JsonProperty("param-offset-date-time")
    private OffsetDateTime paramOffsetDateTime;

    @Size(max = 1, groups = StringValidationGroup.class)
    @JsonProperty("param-string")
    private String paramString;

    @YearRange(max = "2000", groups = YearValidationGroup.class)
    @JsonProperty("param-year")
    private Year paramYear;

  }

  @AllArgsConstructor
  @Builder
  @Data
  @NoArgsConstructor
  public static class NamedCustomConstrainedDto {

    @DecimalMax(value = "9.9", groups = BigDecimalValidationGroup.class, message = "is not a valid big-decimal")
    @JsonProperty("param-big-decimal")
    private BigDecimal paramBigDecimal;

    @DecimalMax(value = "1", groups = BigIntegerValidationGroup.class, message = "is not a valid big-integer")
    @JsonProperty("param-big-integer")
    private BigInteger paramBigInteger;

    @Max(value = 1, groups = IntegerValidationGroup.class, message = "is not a valid integer")
    @JsonProperty("param-integer")
    private Integer paramInteger;

    @DateRange(max = "2000-01-01", groups = LocalDateValidationGroup.class, message = "is not a valid date")
    @JsonProperty("param-local-date")
    private LocalDate paramLocalDate;

    @DatetimeRange(max = "2000-01-01T00:00:00Z", groups = LocalDateTimeValidationGroup.class, message = "is not a valid local-datetime")
    @JsonProperty("param-local-date-time")
    private LocalDateTime paramLocalDateTime;

    @Max(value = 1, groups = LongValidationGroup.class, message = "is not a valid long")
    @JsonProperty("param-long")
    private Long paramLong;

    @DatetimeRange(max = "2000-01-01T00:00:00Z", groups = OffsetDateTimeValidationGroup.class, message = "is not a valid offset-datetime")
    @JsonProperty("param-offset-date-time")
    private OffsetDateTime paramOffsetDateTime;

    @Size(max = 1, groups = StringValidationGroup.class, message = "is not a valid string")
    @JsonProperty("param-string")
    private String paramString;

    @YearRange(max = "2000", groups = YearValidationGroup.class, message = "is not a valid year")
    @JsonProperty("param-year")
    private Year paramYear;
  }

  @AllArgsConstructor
  @Builder
  @Data
  @NoArgsConstructor
  public static class NamedNotNullOptionalDto {

    @JsonProperty("param-big-decimal")
    private Optional<@NotNull(groups = BigDecimalValidationGroup.class) BigDecimal> paramBigDecimal;

    @JsonProperty("param-big-integer")
    private Optional<@NotNull(groups = BigIntegerValidationGroup.class) BigInteger> paramBigInteger;

    @JsonProperty("param-boolean")
    private Optional<@NotNull(groups = BooleanValidationGroup.class) Boolean> paramBoolean;

    @JsonProperty("param-double")
    private Optional<@NotNull(groups = DoubleValidationGroup.class) Double> paramDouble;

    @JsonProperty("param-enum-ab")
    private Optional<@NotNull(groups = EnumABGroup.class) EnumAB> paramEnumAB;

    @JsonProperty("param-enum-lower-ab")
    private Optional<@NotNull(groups = EnumLowerABGroup.class) EnumLowerAB> paramEnumLowerAB;

    @JsonProperty("param-float")
    private Optional<@NotNull(groups = FloatValidationGroup.class) Float> paramFloat;

    @JsonProperty("param-integer")
    private Optional<@NotNull(groups = IntegerValidationGroup.class) Integer> paramInteger;

    @JsonProperty("param-local-date")
    private Optional<@NotNull(groups = LocalDateValidationGroup.class) LocalDate> paramLocalDate;

    @JsonProperty("param-local-date-time")
    private Optional<@NotNull(groups = LocalDateTimeValidationGroup.class) LocalDateTime> paramLocalDateTime;

    @JsonProperty("param-local-time")
    private Optional<@NotNull(groups = LocalTimeValidationGroup.class) LocalTime> paramLocalTime;

    @JsonProperty("param-long")
    private Optional<@NotNull(groups = LongValidationGroup.class) Long> paramLong;

    @JsonProperty("param-offset-date-time")
    private Optional<@NotNull(groups = OffsetDateTimeValidationGroup.class) OffsetDateTime> paramOffsetDateTime;

    @JsonProperty("param-string")
    private Optional<@NotNull(groups = StringValidationGroup.class) String> paramString;

    @JsonProperty("param-year")
    private Optional<@NotNull(groups = YearValidationGroup.class) Year> paramYear;
  }

  @AllArgsConstructor
  @Builder
  @Data
  @NoArgsConstructor
  public static class NamedConstrainedOptionalDto {

    @JsonProperty("param-big-decimal")
    private Optional<@DecimalMax(value = "9.9", groups = BigDecimalValidationGroup.class) BigDecimal> paramBigDecimal;

    @JsonProperty("param-big-integer")
    private Optional<@DecimalMax(value = "1", groups = BigIntegerValidationGroup.class) BigInteger> paramBigInteger;

    @JsonProperty("param-integer")
    private Optional<@Max(value = 1, groups = IntegerValidationGroup.class) Integer> paramInteger;

    @JsonProperty("param-local-date")
    private Optional<@DateRange(max = "2000-01-01", groups = LocalDateValidationGroup.class) LocalDate> paramLocalDate;

    @JsonProperty("param-local-date-time")
    private Optional<@DatetimeRange(max = "2000-01-01T00:00:00Z", groups = LocalDateTimeValidationGroup.class) LocalDateTime> paramLocalDateTime;

    @JsonProperty("param-long")
    private Optional<@Max(value = 1, groups = LongValidationGroup.class) Long> paramLong;

    @JsonProperty("param-offset-date-time")
    private Optional<@DatetimeRange(max = "2000-01-01T00:00:00Z", groups = OffsetDateTimeValidationGroup.class) OffsetDateTime> paramOffsetDateTime;

    @JsonProperty("param-string")
    private Optional<@Size(max = 1, groups = StringValidationGroup.class) String> paramString;

    @JsonProperty("param-year")
    private Optional<@YearRange(max = "2000", groups = YearValidationGroup.class) Year> paramYear;

  }

  @AllArgsConstructor
  @Builder
  @Data
  @NoArgsConstructor
  public static class NamedCustomConstrainedOptionalDto {

    @JsonProperty("param-big-decimal")
    private Optional<@DecimalMax(value = "9.9", groups = BigDecimalValidationGroup.class, message = "is not a valid big-decimal") BigDecimal> paramBigDecimal;

    @JsonProperty("param-big-integer")
    private Optional<@DecimalMax(value = "1", groups = BigIntegerValidationGroup.class, message = "is not a valid big-integer") BigInteger> paramBigInteger;

    @JsonProperty("param-integer")
    private Optional<@Max(value = 1, groups = IntegerValidationGroup.class, message = "is not a valid integer") Integer> paramInteger;

    @JsonProperty("param-local-date")
    private Optional<@DateRange(max = "2000-01-01", groups = LocalDateValidationGroup.class, message = "is not a valid date") LocalDate> paramLocalDate;

    @JsonProperty("param-local-date-time")
    private Optional<@DatetimeRange(max = "2000-01-01T00:00:00Z", groups = LocalDateTimeValidationGroup.class, message = "is not a valid local-datetime") LocalDateTime> paramLocalDateTime;

    @JsonProperty("param-long")
    private Optional<@Max(value = 1, groups = LongValidationGroup.class, message = "is not a valid long") Long> paramLong;

    @JsonProperty("param-offset-date-time")
    private Optional<@DatetimeRange(max = "2000-01-01T00:00:00Z", groups = OffsetDateTimeValidationGroup.class, message = "is not a valid offset-datetime") OffsetDateTime> paramOffsetDateTime;

    @JsonProperty("param-string")
    private Optional<@Size(max = 1, groups = StringValidationGroup.class, message = "is not a valid string") String> paramString;

    @JsonProperty("param-year")
    private Optional<@YearRange(max = "2000", groups = YearValidationGroup.class, message = "is not a valid year") Year> paramYear;

  }

  @AllArgsConstructor
  @Builder
  @Data
  @NoArgsConstructor
  public static class NamedI18nConstrainedOptionalDto {
    @JsonProperty("param-big-decimal")
    private Optional<@DecimalMax(value = "9.9", groups = BigDecimalValidationGroup.class, message = "{test.dto.BigDecimal}") BigDecimal> paramBigDecimal;

    @JsonProperty("param-big-integer")
    private Optional<@DecimalMax(value = "1", groups = BigIntegerValidationGroup.class, message = "{test.dto.BigInteger}") BigInteger> paramBigInteger;

    @JsonProperty("param-integer")
    private Optional<@Max(value = 1, groups = IntegerValidationGroup.class, message = "{test.dto.Integer}") Integer> paramInteger;

    @JsonProperty("param-local-date")
    private Optional<@DateRange(max = "2000-01-01", groups = LocalDateValidationGroup.class, message = "{test.dto.LocalDate}") LocalDate> paramLocalDate;

    @JsonProperty("param-local-date-time")
    private Optional<@DatetimeRange(max = "2000-01-01T00:00:00Z", groups = LocalDateTimeValidationGroup.class, message = "{test.dto.LocalDateTime}") LocalDateTime> paramLocalDateTime;

    @JsonProperty("param-long")
    private Optional<@Max(value = 1, groups = LongValidationGroup.class, message = "{test.dto.Long}") Long> paramLong;

    @JsonProperty("param-offset-date-time")
    private Optional<@DatetimeRange(max = "2000-01-01T00:00:00Z", groups = OffsetDateTimeValidationGroup.class, message = "{test.dto.OffsetDateTime}") OffsetDateTime> paramOffsetDateTime;

    @JsonProperty("param-string")
    private Optional<@Size(max = 1, groups = StringValidationGroup.class, message = "{test.dto.String}") String> paramString;

    @JsonProperty("param-year")
    private Optional<@YearRange(max = "2000", groups = YearValidationGroup.class, message = "{test.dto.Year}") Year> paramYear;

  }

  @AllArgsConstructor
  @Builder
  @Data
  @NoArgsConstructor
  public static class NamedI18nConstrainedDto {

    @DecimalMax(value = "9.9", groups = BigDecimalValidationGroup.class, message = "{test.dto.BigDecimal}")
    @JsonProperty("param-big-decimal")
    private BigDecimal paramBigDecimal;

    @DecimalMax(value = "1", groups = BigIntegerValidationGroup.class, message = "{test.dto.BigInteger}")
    @JsonProperty("param-big-integer")
    private BigInteger paramBigInteger;

    @Max(value = 1, groups = IntegerValidationGroup.class, message = "{test.dto.Integer}")
    @JsonProperty("param-integer")
    private Integer paramInteger;

    @DateRange(max = "2000-01-01", groups = LocalDateValidationGroup.class, message = "{test.dto.LocalDate}")
    @JsonProperty("param-local-date")
    private LocalDate paramLocalDate;

    @DatetimeRange(max = "2000-01-01T00:00:00Z", groups = LocalDateTimeValidationGroup.class, message = "{test.dto.LocalDateTime}")
    @JsonProperty("param-local-date-time")
    private LocalDateTime paramLocalDateTime;

    @Max(value = 1, groups = LongValidationGroup.class, message = "{test.dto.Long}")
    @JsonProperty("param-long")
    private Long paramLong;

    @DatetimeRange(max = "2000-01-01T00:00:00Z", groups = OffsetDateTimeValidationGroup.class, message = "{test.dto.OffsetDateTime}")
    @JsonProperty("param-offset-date-time")
    private OffsetDateTime paramOffsetDateTime;

    @Size(max = 1, groups = StringValidationGroup.class, message = "{test.dto.String}")
    @JsonProperty("param-string")
    private String paramString;

    @YearRange(max = "2000", groups = YearValidationGroup.class, message = "{test.dto.Year}")
    @JsonProperty("param-year")
    private Year paramYear;
  }

  @AllArgsConstructor
  @Builder
  @Data
  @NoArgsConstructor
  public static class UnamedConstrainedDto {

    @DecimalMax(value = "9.9", groups = BigDecimalValidationGroup.class)
    private BigDecimal paramBigDecimal;

    @DecimalMax(value = "1", groups = BigIntegerValidationGroup.class)
    private BigInteger paramBigInteger;

    @Max(value = 1, groups = IntegerValidationGroup.class)
    private Integer paramInteger;

    @DateRange(max = "2000-01-01", groups = LocalDateValidationGroup.class)
    private LocalDate paramLocalDate;

    @DatetimeRange(max = "2000-01-01T00:00:00Z", groups = LocalDateTimeValidationGroup.class)
    private LocalDateTime paramLocalDateTime;

    @Max(value = 1, groups = LongValidationGroup.class)
    private Long paramLong;

    @DatetimeRange(max = "2000-01-01T00:00:00Z", groups = OffsetDateTimeValidationGroup.class)
    private OffsetDateTime paramOffsetDateTime;

    @Size(max = 1, groups = StringValidationGroup.class)
    private String paramString;

    @YearRange(max = "2000", groups = YearValidationGroup.class)
    private Year paramYear;
  }

  @AllArgsConstructor
  @Builder
  @Data
  @NoArgsConstructor
  public static class UnamedConstrainedOptionalDto {

    private Optional<@DecimalMax(value = "9.9", groups = BigDecimalValidationGroup.class) BigDecimal> paramBigDecimal;

    private Optional<@DecimalMax(value = "1", groups = BigIntegerValidationGroup.class) BigInteger> paramBigInteger;

    private Optional<@Max(value = 1, groups = IntegerValidationGroup.class) Integer> paramInteger;

    private Optional<@DateRange(max = "2000-01-01", groups = LocalDateValidationGroup.class) LocalDate> paramLocalDate;

    private Optional<@DatetimeRange(max = "2000-01-01T00:00:00Z", groups = LocalDateTimeValidationGroup.class) LocalDateTime> paramLocalDateTime;

    private Optional<@Max(value = 1, groups = LongValidationGroup.class) Long> paramLong;

    private Optional<@DatetimeRange(max = "2000-01-01T00:00:00Z", groups = OffsetDateTimeValidationGroup.class) OffsetDateTime> paramOffsetDateTime;

    private Optional<@Size(max = 1, groups = StringValidationGroup.class) String> paramString;

    private Optional<@YearRange(max = "2000", groups = YearValidationGroup.class) Year> paramYear;
  }

  @AllArgsConstructor
  @Builder
  @Data
  @NoArgsConstructor
  public static class UnamedCustomConstrainedDto {

    @DecimalMax(value = "9.9", groups = BigDecimalValidationGroup.class, message = "is not a valid big-decimal")
    private BigDecimal paramBigDecimal;

    @DecimalMax(value = "1", groups = BigIntegerValidationGroup.class, message = "is not a valid big-integer")
    private BigInteger paramBigInteger;

    @Max(value = 1, groups = IntegerValidationGroup.class, message = "is not a valid integer")
    private Integer paramInteger;

    @DateRange(max = "2000-01-01", groups = LocalDateValidationGroup.class, message = "is not a valid date")
    private LocalDate paramLocalDate;

    @DatetimeRange(max = "2000-01-01T00:00:00Z", groups = LocalDateTimeValidationGroup.class, message = "is not a valid local-datetime")
    private LocalDateTime paramLocalDateTime;

    @Max(value = 1, groups = LongValidationGroup.class, message = "is not a valid long")
    private Long paramLong;

    @DatetimeRange(max = "2000-01-01T00:00:00Z", groups = OffsetDateTimeValidationGroup.class, message = "is not a valid offset-datetime")
    private OffsetDateTime paramOffsetDateTime;

    @Size(max = 1, groups = StringValidationGroup.class, message = "is not a valid string")
    private String paramString;

    @YearRange(max = "2000", groups = YearValidationGroup.class, message = "is not a valid year")
    private Year paramYear;
  }

  @AllArgsConstructor
  @Builder
  @Data
  @NoArgsConstructor
  public static class UnamedCustomConstrainedOptionalDto {

    private Optional<@DecimalMax(value = "9.9", groups = BigDecimalValidationGroup.class, message = "is not a valid big-decimal") BigDecimal> paramBigDecimal;

    private Optional<@DecimalMax(value = "1", groups = BigIntegerValidationGroup.class, message = "is not a valid big-integer") BigInteger> paramBigInteger;

    private Optional<@Max(value = 1, groups = IntegerValidationGroup.class, message = "is not a valid integer") Integer> paramInteger;

    private Optional<@DateRange(max = "2000-01-01", groups = LocalDateValidationGroup.class, message = "is not a valid date") LocalDate> paramLocalDate;

    private Optional<@DatetimeRange(max = "2000-01-01T00:00:00Z", groups = LocalDateTimeValidationGroup.class, message = "is not a valid local-datetime") LocalDateTime> paramLocalDateTime;

    private Optional<@Max(value = 1, groups = LongValidationGroup.class, message = "is not a valid long") Long> paramLong;

    private Optional<@DatetimeRange(max = "2000-01-01T00:00:00Z", groups = OffsetDateTimeValidationGroup.class, message = "is not a valid offset-datetime") OffsetDateTime> paramOffsetDateTime;

    private Optional<@Size(max = 1, groups = StringValidationGroup.class, message = "is not a valid string") String> paramString;

    private Optional<@YearRange(max = "2000", groups = YearValidationGroup.class, message = "is not a valid year") Year> paramYear;

  }

  @AllArgsConstructor
  @Builder
  @Data
  @NoArgsConstructor
  public static class UnamedI18nConstrainedDto {

    @DecimalMax(value = "9.9", groups = BigDecimalValidationGroup.class, message = "{test.dto.BigDecimal}")
    private BigDecimal paramBigDecimal;

    @DecimalMax(value = "1", groups = BigIntegerValidationGroup.class, message = "{test.dto.BigInteger}")
    private BigInteger paramBigInteger;

    @Max(value = 1, groups = IntegerValidationGroup.class, message = "{test.dto.Integer}")
    private Integer paramInteger;

    @DateRange(max = "2000-01-01", groups = LocalDateValidationGroup.class, message = "{test.dto.LocalDate}")
    private LocalDate paramLocalDate;

    @DatetimeRange(max = "2000-01-01T00:00:00Z", groups = LocalDateTimeValidationGroup.class, message = "{test.dto.LocalDateTime}")
    private LocalDateTime paramLocalDateTime;

    @Max(value = 1, groups = LongValidationGroup.class, message = "{test.dto.Long}")
    private Long paramLong;

    @DatetimeRange(max = "2000-01-01T00:00:00Z", groups = OffsetDateTimeValidationGroup.class, message = "{test.dto.OffsetDateTime}")
    private OffsetDateTime paramOffsetDateTime;

    @Size(max = 1, groups = StringValidationGroup.class, message = "{test.dto.String}")
    private String paramString;

    @YearRange(max = "2000", groups = YearValidationGroup.class, message = "{test.dto.Year}")
    private Year paramYear;
  }

  @AllArgsConstructor
  @Builder
  @Data
  @NoArgsConstructor
  public static class UnamedI18nConstrainedOptionalDto {
    private Optional<@DecimalMax(value = "9.9", groups = BigDecimalValidationGroup.class, message = "{test.dto.BigDecimal}") BigDecimal> paramBigDecimal;

    private Optional<@DecimalMax(value = "1", groups = BigIntegerValidationGroup.class, message = "{test.dto.BigInteger}") BigInteger> paramBigInteger;

    private Optional<@Max(value = 1, groups = IntegerValidationGroup.class, message = "{test.dto.Integer}") Integer> paramInteger;

    private Optional<@DateRange(max = "2000-01-01", groups = LocalDateValidationGroup.class, message = "{test.dto.LocalDate}") LocalDate> paramLocalDate;

    private Optional<@DatetimeRange(max = "2000-01-01T00:00:00Z", groups = LocalDateTimeValidationGroup.class, message = "{test.dto.LocalDateTime}") LocalDateTime> paramLocalDateTime;

    private Optional<@Max(value = 1, groups = LongValidationGroup.class, message = "{test.dto.Long}") Long> paramLong;

    private Optional<@DatetimeRange(max = "2000-01-01T00:00:00Z", groups = OffsetDateTimeValidationGroup.class, message = "{test.dto.OffsetDateTime}") OffsetDateTime> paramOffsetDateTime;

    private Optional<@Size(max = 1, groups = StringValidationGroup.class, message = "{test.dto.String}") String> paramString;

    private Optional<@YearRange(max = "2000", groups = YearValidationGroup.class, message = "{test.dto.Year}") Year> paramYear;

  }

  @AllArgsConstructor
  @Builder
  @Data
  @NoArgsConstructor
  public static class UnamedNotNullDto {

    @NotNull(groups = BigDecimalValidationGroup.class)
    private BigDecimal paramBigDecimal;

    @NotNull(groups = BigIntegerValidationGroup.class)
    private BigInteger paramBigInteger;

    @NotNull(groups = BooleanValidationGroup.class)
    private Boolean paramBoolean;

    @NotNull(groups = DoubleValidationGroup.class)
    private Double paramDouble;

    @NotNull(groups = EnumABGroup.class)
    private EnumAB paramEnumAB;

    @NotNull(groups = EnumLowerABGroup.class)
    private EnumLowerAB paramEnumLowerAB;

    @NotNull(groups = FloatValidationGroup.class)
    private Float paramFloat;

    @NotNull(groups = IntegerValidationGroup.class)
    private Integer paramInteger;

    @NotNull(groups = LocalDateValidationGroup.class)
    private LocalDate paramLocalDate;

    @NotNull(groups = LocalDateTimeValidationGroup.class)
    private LocalDateTime paramLocalDateTime;

    @NotNull(groups = LocalTimeValidationGroup.class)
    private LocalTime paramLocalTime;

    @NotNull(groups = LongValidationGroup.class)
    private Long paramLong;

    @NotNull(groups = OffsetDateTimeValidationGroup.class)
    private OffsetDateTime paramOffsetDateTime;

    @NotNull(groups = StringValidationGroup.class)
    private String paramString;

    @NotNull(groups = YearValidationGroup.class)
    private Year paramYear;
  }

  @AllArgsConstructor
  @Builder
  @Data
  @NoArgsConstructor
  public static class UnamedNotNullOptionalDto {

    private Optional<@NotNull(groups = BigDecimalValidationGroup.class) BigDecimal> paramBigDecimal;

    private Optional<@NotNull(groups = BigIntegerValidationGroup.class) BigInteger> paramBigInteger;

    private Optional<@NotNull(groups = BooleanValidationGroup.class) Boolean> paramBoolean;

    private Optional<@NotNull(groups = DoubleValidationGroup.class) Double> paramDouble;

    private Optional<@NotNull(groups = EnumABGroup.class) EnumAB> paramEnumAB;

    private Optional<@NotNull(groups = EnumLowerABGroup.class) EnumLowerAB> paramEnumLowerAB;

    private Optional<@NotNull(groups = FloatValidationGroup.class) Float> paramFloat;

    private Optional<@NotNull(groups = IntegerValidationGroup.class) Integer> paramInteger;

    private Optional<@NotNull(groups = LocalDateValidationGroup.class) LocalDate> paramLocalDate;

    private Optional<@NotNull(groups = LocalDateTimeValidationGroup.class) LocalDateTime> paramLocalDateTime;

    private Optional<@NotNull(groups = LocalTimeValidationGroup.class) LocalTime> paramLocalTime;

    private Optional<@NotNull(groups = LongValidationGroup.class) Long> paramLong;

    private Optional<@NotNull(groups = OffsetDateTimeValidationGroup.class) OffsetDateTime> paramOffsetDateTime;

    private Optional<@NotNull(groups = StringValidationGroup.class) String> paramString;

    private Optional<@NotNull(groups = YearValidationGroup.class) Year> paramYear;
  }

  @AllArgsConstructor
  @Builder
  @Data
  @NoArgsConstructor
  public static class ParentUnamedNotNullDto {

    @Valid
    private List<UnamedNotNullDto> array;

    @Valid
    private UnamedNotNullDto nested;

  }

  @AllArgsConstructor
  @Builder
  @Data
  @NoArgsConstructor
  public static class ParentUnamedNotNullOptionalDto {

    @Valid
    private List<UnamedNotNullOptionalDto> array;

    @Valid
    private UnamedNotNullOptionalDto nested;

  }

  @AllArgsConstructor
  @Builder
  @Data
  @NoArgsConstructor
  public static class ParentUnamedConstrainedDto {

    @Valid
    private List<UnamedConstrainedDto> array;

    @Valid
    private UnamedConstrainedDto nested;

  }

  @AllArgsConstructor
  @Builder
  @Data
  @NoArgsConstructor
  public static class ParentUnamedConstrainedOptionalDto {

    @Valid
    private List<UnamedConstrainedOptionalDto> array;

    @Valid
    private UnamedConstrainedOptionalDto nested;

  }

  @AllArgsConstructor
  @Builder
  @Data
  @NoArgsConstructor
  public static class ParentUnamedCustomConstrainedDto {

    @Valid
    private List<UnamedCustomConstrainedDto> array;

    @Valid
    private UnamedCustomConstrainedDto nested;

  }

  @AllArgsConstructor
  @Builder
  @Data
  @NoArgsConstructor
  public static class ParentUnamedCustomConstrainedOptionalDto {

    @Valid
    private List<UnamedCustomConstrainedOptionalDto> array;

    @Valid
    private UnamedCustomConstrainedOptionalDto nested;

  }

  @AllArgsConstructor
  @Builder
  @Data
  @NoArgsConstructor
  public static class ParentUnamedI18nConstrainedDto {

    @Valid
    private List<UnamedI18nConstrainedDto> array;

    @Valid
    private UnamedI18nConstrainedDto nested;

  }

  @AllArgsConstructor
  @Builder
  @Data
  @NoArgsConstructor
  public static class ParentUnamedI18nConstrainedOptionalDto {

    @Valid
    private List<UnamedI18nConstrainedOptionalDto> array;

    @Valid
    private UnamedI18nConstrainedOptionalDto nested;

  }

  @AllArgsConstructor
  @Builder
  @Data
  @NoArgsConstructor
  public static class ParentNamedNotNullDto {

    @Valid
    private List<NamedNotNullDto> array;

    @Valid
    private NamedNotNullDto nested;

  }

  @AllArgsConstructor
  @Builder
  @Data
  @NoArgsConstructor
  public static class ParentNamedNotNullOptionalDto {

    @Valid
    private List<NamedNotNullOptionalDto> array;

    @Valid
    private NamedNotNullOptionalDto nested;

  }

  @AllArgsConstructor
  @Builder
  @Data
  @NoArgsConstructor
  public static class ParentNamedConstrainedDto {

    @Valid
    private List<NamedConstrainedDto> array;

    @Valid
    private NamedConstrainedDto nested;

  }

  @AllArgsConstructor
  @Builder
  @Data
  @NoArgsConstructor
  public static class ParentNamedConstrainedOptionalDto {

    @Valid
    private List<NamedConstrainedOptionalDto> array;

    @Valid
    private NamedConstrainedOptionalDto nested;

  }

  @AllArgsConstructor
  @Builder
  @Data
  @NoArgsConstructor
  public static class ParentNamedCustomConstrainedDto {

    @Valid
    private List<NamedCustomConstrainedDto> array;

    @Valid
    private NamedCustomConstrainedDto nested;

  }

  @AllArgsConstructor
  @Builder
  @Data
  @NoArgsConstructor
  public static class ParentNamedCustomConstrainedOptionalDto {

    @Valid
    private List<NamedCustomConstrainedOptionalDto> array;

    @Valid
    private NamedCustomConstrainedOptionalDto nested;

  }

  @AllArgsConstructor
  @Builder
  @Data
  @NoArgsConstructor
  public static class ParentNamedI18nConstrainedDto {

    @Valid
    private List<NamedI18nConstrainedDto> array;

    @Valid
    private NamedI18nConstrainedDto nested;

  }

  @AllArgsConstructor
  @Builder
  @Data
  @NoArgsConstructor
  public static class ParentNamedI18nConstrainedOptionalDto {

    @Valid
    private List<NamedI18nConstrainedOptionalDto> array;

    @Valid
    private NamedI18nConstrainedOptionalDto nested;

  }

}
