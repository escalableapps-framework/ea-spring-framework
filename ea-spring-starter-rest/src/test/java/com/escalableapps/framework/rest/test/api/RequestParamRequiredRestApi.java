package com.escalableapps.framework.rest.test.api;

import static com.escalableapps.framework.rest.RestConfigurationTest.RestConfigurationTestContext.RECEIVED_VALUES;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.HEAD;
import static org.springframework.web.bind.annotation.RequestMethod.PATCH;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.Year;
import java.util.Optional;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.escalableapps.framework.core.util.JsonUtils;
import com.escalableapps.framework.rest.RestConfigurationTest.EnumAB;
import com.escalableapps.framework.rest.RestConfigurationTest.EnumLowerAB;
import com.escalableapps.framework.rest.RestConfigurationTest.UnamedNotNullDto;
import com.escalableapps.framework.rest.RestConfigurationTest.UnamedNotNullOptionalDto;

@RestController
@RequestMapping("/api/v1/request-parameter/required")
public class RequestParamRequiredRestApi {

  @RequestMapping(path = "/method-parameter/paramString", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamString(@RequestParam(required = true) String paramString) {
    return null;
  }

  @RequestMapping(path = "/method-parameter/paramInteger", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamInteger(@RequestParam(required = true) Integer paramInteger) {
    return null;
  }

  @RequestMapping(path = "/method-parameter/paramLong", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamLong(@RequestParam(required = true) Long paramLong) {
    return null;
  }

  @RequestMapping(path = "/method-parameter/paramBigInteger", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamBigInteger(
      @RequestParam(required = true) BigInteger paramBigInteger) {
    return null;
  }

  @RequestMapping(path = "/method-parameter/paramFloat", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamFloat(@RequestParam(required = true) Float paramFloat) {
    return null;
  }

  @RequestMapping(path = "/method-parameter/paramDouble", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamDouble(@RequestParam(required = true) Double paramDouble) {
    return null;
  }

  @RequestMapping(path = "/method-parameter/paramBigDecimal", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamBigDecimal(
      @RequestParam(required = true) BigDecimal paramBigDecimal) {
    return null;
  }

  @RequestMapping(path = "/method-parameter/paramBoolean", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamBoolean(@RequestParam(required = true) Boolean paramBoolean) {
    return null;
  }

  @RequestMapping(path = "/method-parameter/paramOffsetDateTime", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamOffsetDateTime(
      @RequestParam(required = true) OffsetDateTime paramOffsetDateTime) {
    return null;
  }

  @RequestMapping(path = "/method-parameter/paramLocalDateTime", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamLocalDateTime(
      @RequestParam(required = true) LocalDateTime paramLocalDateTime) {
    return null;
  }

  @RequestMapping(path = "/method-parameter/paramLocalDate", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamLocalDate(@RequestParam(required = true) LocalDate paramLocalDate) {
    return null;
  }

  @RequestMapping(path = "/method-parameter/paramLocalTime", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamLocalTime(@RequestParam(required = true) LocalTime paramLocalTime) {
    return null;
  }

  @RequestMapping(path = "/method-parameter/paramYear", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamYear(@RequestParam(required = true) Year paramYear) {
    return null;
  }

  @RequestMapping(path = "/method-parameter/paramEnumAB", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamEnumAB(@RequestParam(required = true) EnumAB paramEnumAB) {
    return null;
  }

  @RequestMapping(path = "/method-parameter/paramEnumLowerAB", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamEnumLowerAB(
      @RequestParam(required = true) EnumLowerAB paramEnumLowerAB) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/paramString", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamString(
      @RequestParam(required = true, name = "param-string") String paramString) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/paramInteger", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamInteger(
      @RequestParam(required = true, name = "param-integer") Integer paramInteger) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/paramLong", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamLong(
      @RequestParam(required = true, name = "param-long") Long paramLong) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/paramBigInteger", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamBigInteger(
      @RequestParam(required = true, name = "param-big-integer") BigInteger paramBigInteger) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/paramFloat", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamFloat(
      @RequestParam(required = true, name = "param-float") Float paramFloat) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/paramDouble", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamDouble(
      @RequestParam(required = true, name = "param-double") Double paramDouble) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/paramBigDecimal", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamBigDecimal(
      @RequestParam(required = true, name = "param-big-decimal") BigDecimal paramBigDecimal) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/paramBoolean", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamBoolean(
      @RequestParam(required = true, name = "param-boolean") Boolean paramBoolean) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/paramOffsetDateTime", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamOffsetDateTime(
      @RequestParam(required = true, name = "param-offset-date-time") OffsetDateTime paramOffsetDateTime) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/paramLocalDateTime", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamLocalDateTime(
      @RequestParam(required = true, name = "param-local-date-time") LocalDateTime paramLocalDateTime) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/paramLocalDate", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamLocalDate(
      @RequestParam(required = true, name = "param-local-date") LocalDate paramLocalDate) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/paramLocalTime", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamLocalTime(
      @RequestParam(required = true, name = "param-local-time") LocalTime paramLocalTime) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/paramYear", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamYear(
      @RequestParam(required = true, name = "param-year") Year paramYear) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/paramEnumAB", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamEnumAB(
      @RequestParam(required = true, name = "param-enum-ab") EnumAB paramEnumAB) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/paramEnumLowerAB", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamEnumLowerAB(
      @RequestParam(required = true, name = "param-enum-lower-ab") EnumLowerAB paramEnumLowerAB) {
    return null;
  }

  @RequestMapping(path = "/optional-method-parameter/paramString", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> optionalMethodParameterParamString(
      @RequestParam(required = true) Optional<String> paramString) {
    UnamedNotNullOptionalDto params = UnamedNotNullOptionalDto.builder() //
        .paramString(paramString) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/optional-method-parameter/paramInteger", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> optionalMethodParameterParamInteger(
      @RequestParam(required = true) Optional<Integer> paramInteger) {
    UnamedNotNullOptionalDto params = UnamedNotNullOptionalDto.builder() //
        .paramInteger(paramInteger) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/optional-method-parameter/paramLong", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> optionalMethodParameterParamLong(
      @RequestParam(required = true) Optional<Long> paramLong) {
    UnamedNotNullOptionalDto params = UnamedNotNullOptionalDto.builder() //
        .paramLong(paramLong) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/optional-method-parameter/paramBigInteger", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> optionalMethodParameterParamBigInteger(
      @RequestParam(required = true) Optional<BigInteger> paramBigInteger) {
    UnamedNotNullOptionalDto params = UnamedNotNullOptionalDto.builder() //
        .paramBigInteger(paramBigInteger) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/optional-method-parameter/paramFloat", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> optionalMethodParameterParamFloat(
      @RequestParam(required = true) Optional<Float> paramFloat) {
    UnamedNotNullOptionalDto params = UnamedNotNullOptionalDto.builder() //
        .paramFloat(paramFloat) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/optional-method-parameter/paramDouble", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> optionalMethodParameterParamDouble(
      @RequestParam(required = true) Optional<Double> paramDouble) {
    UnamedNotNullOptionalDto params = UnamedNotNullOptionalDto.builder() //
        .paramDouble(paramDouble) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/optional-method-parameter/paramBigDecimal", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> optionalMethodParameterParamBigDecimal(
      @RequestParam(required = true) Optional<BigDecimal> paramBigDecimal) {
    UnamedNotNullOptionalDto params = UnamedNotNullOptionalDto.builder() //
        .paramBigDecimal(paramBigDecimal) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/optional-method-parameter/paramBoolean", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> optionalMethodParameterParamBoolean(
      @RequestParam(required = true) Optional<Boolean> paramBoolean) {
    UnamedNotNullOptionalDto params = UnamedNotNullOptionalDto.builder() //
        .paramBoolean(paramBoolean) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/optional-method-parameter/paramOffsetDateTime", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> optionalMethodParameterParamOffsetDateTime(
      @RequestParam(required = true) Optional<OffsetDateTime> paramOffsetDateTime) {
    UnamedNotNullOptionalDto params = UnamedNotNullOptionalDto.builder() //
        .paramOffsetDateTime(paramOffsetDateTime) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/optional-method-parameter/paramLocalDateTime", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> optionalMethodParameterParamLocalDateTime(
      @RequestParam(required = true) Optional<LocalDateTime> paramLocalDateTime) {
    UnamedNotNullOptionalDto params = UnamedNotNullOptionalDto.builder() //
        .paramLocalDateTime(paramLocalDateTime) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/optional-method-parameter/paramLocalDate", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> optionalMethodParameterParamLocalDate(
      @RequestParam(required = true) Optional<LocalDate> paramLocalDate) {
    UnamedNotNullOptionalDto params = UnamedNotNullOptionalDto.builder() //
        .paramLocalDate(paramLocalDate) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/optional-method-parameter/paramLocalTime", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> optionalMethodParameterParamLocalTime(
      @RequestParam(required = true) Optional<LocalTime> paramLocalTime) {
    UnamedNotNullOptionalDto params = UnamedNotNullOptionalDto.builder() //
        .paramLocalTime(paramLocalTime) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/optional-method-parameter/paramYear", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> optionalMethodParameterParamYear(
      @RequestParam(required = true) Optional<Year> paramYear) {
    UnamedNotNullOptionalDto params = UnamedNotNullOptionalDto.builder() //
        .paramYear(paramYear) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/optional-method-parameter/paramEnumAB", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> optionalMethodParameterParamEnumAB(
      @RequestParam(required = true) Optional<EnumAB> paramEnumAB) {
    UnamedNotNullOptionalDto params = UnamedNotNullOptionalDto.builder() //
        .paramEnumAB(paramEnumAB) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/optional-method-parameter/paramEnumLowerAB", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> optionalMethodParameterParamEnumLowerAB(
      @RequestParam(required = true) Optional<EnumLowerAB> paramEnumLowerAB) {
    UnamedNotNullOptionalDto params = UnamedNotNullOptionalDto.builder() //
        .paramEnumLowerAB(paramEnumLowerAB) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/named-optional-method-parameter/paramString", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedOptionalMethodParameterParamString(
      @RequestParam(required = true, name = "param-string") Optional<String> paramString) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramString(paramString.orElse(null)) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/named-optional-method-parameter/paramInteger", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> namedOptionalMethodParameterParamInteger(
      @RequestParam(required = true, name = "param-integer") Optional<Integer> paramInteger) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramInteger(paramInteger.orElse(null)) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/named-optional-method-parameter/paramLong", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedOptionalMethodParameterParamLong(
      @RequestParam(required = true, name = "param-long") Optional<Long> paramLong) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramLong(paramLong.orElse(null)) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/named-optional-method-parameter/paramBigInteger", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> namedOptionalMethodParameterParamBigInteger(
      @RequestParam(required = true, name = "param-big-integer") Optional<BigInteger> paramBigInteger) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramBigInteger(paramBigInteger.orElse(null)) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/named-optional-method-parameter/paramFloat", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedOptionalMethodParameterParamFloat(
      @RequestParam(required = true, name = "param-float") Optional<Float> paramFloat) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramFloat(paramFloat.orElse(null)) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/named-optional-method-parameter/paramDouble", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedOptionalMethodParameterParamDouble(
      @RequestParam(required = true, name = "param-double") Optional<Double> paramDouble) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramDouble(paramDouble.orElse(null)) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/named-optional-method-parameter/paramBigDecimal", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> namedOptionalMethodParameterParamBigDecimal(
      @RequestParam(required = true, name = "param-big-decimal") Optional<BigDecimal> paramBigDecimal) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramBigDecimal(paramBigDecimal.orElse(null)) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/named-optional-method-parameter/paramBoolean", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> namedOptionalMethodParameterParamBoolean(
      @RequestParam(required = true, name = "param-boolean") Optional<Boolean> paramBoolean) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramBoolean(paramBoolean.orElse(null)) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/named-optional-method-parameter/paramOffsetDateTime", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> namedOptionalMethodParameterParamOffsetDateTime(
      @RequestParam(required = true, name = "param-offset-date-time") Optional<OffsetDateTime> paramOffsetDateTime) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramOffsetDateTime(paramOffsetDateTime.orElse(null)) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/named-optional-method-parameter/paramLocalDateTime", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> namedOptionalMethodParameterParamLocalDateTime(
      @RequestParam(required = true, name = "param-local-date-time") Optional<LocalDateTime> paramLocalDateTime) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramLocalDateTime(paramLocalDateTime.orElse(null)) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/named-optional-method-parameter/paramLocalDate", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> namedOptionalMethodParameterParamLocalDate(
      @RequestParam(required = true, name = "param-local-date") Optional<LocalDate> paramLocalDate) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramLocalDate(paramLocalDate.orElse(null)) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/named-optional-method-parameter/paramLocalTime", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> namedOptionalMethodParameterParamLocalTime(
      @RequestParam(required = true, name = "param-local-time") Optional<LocalTime> paramLocalTime) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramLocalTime(paramLocalTime.orElse(null)) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/named-optional-method-parameter/paramYear", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedOptionalMethodParameterParamYear(
      @RequestParam(required = true, name = "param-year") Optional<Year> paramYear) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramYear(paramYear.orElse(null)) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/named-optional-method-parameter/paramEnumAB", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedOptionalMethodParameterParamEnumAB(
      @RequestParam(required = true, name = "param-enum-ab") Optional<EnumAB> paramEnumAB) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramEnumAB(paramEnumAB.orElse(null)) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/named-optional-method-parameter/paramEnumLowerAB", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> namedOptionalMethodParameterParamEnumLowerAB(
      @RequestParam(required = true, name = "param-enum-lower-ab") Optional<EnumLowerAB> paramEnumLowerAB) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramEnumLowerAB(paramEnumLowerAB.orElse(null)) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }
}
