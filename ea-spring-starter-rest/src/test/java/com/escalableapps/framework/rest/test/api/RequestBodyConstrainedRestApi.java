package com.escalableapps.framework.rest.test.api;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.PATCH;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.escalableapps.framework.rest.RestConfigurationTest.BigDecimalValidationGroup;
import com.escalableapps.framework.rest.RestConfigurationTest.BigIntegerValidationGroup;
import com.escalableapps.framework.rest.RestConfigurationTest.BooleanValidationGroup;
import com.escalableapps.framework.rest.RestConfigurationTest.DoubleValidationGroup;
import com.escalableapps.framework.rest.RestConfigurationTest.EnumABGroup;
import com.escalableapps.framework.rest.RestConfigurationTest.EnumLowerABGroup;
import com.escalableapps.framework.rest.RestConfigurationTest.FloatValidationGroup;
import com.escalableapps.framework.rest.RestConfigurationTest.IntegerValidationGroup;
import com.escalableapps.framework.rest.RestConfigurationTest.LocalDateTimeValidationGroup;
import com.escalableapps.framework.rest.RestConfigurationTest.LocalDateValidationGroup;
import com.escalableapps.framework.rest.RestConfigurationTest.LocalTimeValidationGroup;
import com.escalableapps.framework.rest.RestConfigurationTest.LongValidationGroup;
import com.escalableapps.framework.rest.RestConfigurationTest.NamedConstrainedDto;
import com.escalableapps.framework.rest.RestConfigurationTest.NamedConstrainedOptionalDto;
import com.escalableapps.framework.rest.RestConfigurationTest.OffsetDateTimeValidationGroup;
import com.escalableapps.framework.rest.RestConfigurationTest.ParentNamedConstrainedDto;
import com.escalableapps.framework.rest.RestConfigurationTest.ParentNamedConstrainedOptionalDto;
import com.escalableapps.framework.rest.RestConfigurationTest.ParentUnamedConstrainedDto;
import com.escalableapps.framework.rest.RestConfigurationTest.ParentUnamedConstrainedOptionalDto;
import com.escalableapps.framework.rest.RestConfigurationTest.StringValidationGroup;
import com.escalableapps.framework.rest.RestConfigurationTest.UnamedConstrainedDto;
import com.escalableapps.framework.rest.RestConfigurationTest.UnamedConstrainedOptionalDto;
import com.escalableapps.framework.rest.RestConfigurationTest.YearValidationGroup;

@RestController
@RequestMapping("/api/v1/request-body/constrained")
@Validated
public class RequestBodyConstrainedRestApi {

  @RequestMapping(path = "/dto-attribute/paramString", method = {POST, PUT, PATCH, DELETE})
  @Validated({StringValidationGroup.class})
  public ResponseEntity<Void> dtoAttributeParamString(@RequestBody @Valid UnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/dto-attribute/paramInteger", method = {POST, PUT, PATCH, DELETE})
  @Validated(IntegerValidationGroup.class)
  public ResponseEntity<Void> dtoAttributeParamInteger(@RequestBody @Valid UnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/dto-attribute/paramLong", method = {POST, PUT, PATCH, DELETE})
  @Validated(LongValidationGroup.class)
  public ResponseEntity<Void> dtoAttributeParamLong(@RequestBody @Valid UnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/dto-attribute/paramBigInteger", method = {POST, PUT, PATCH, DELETE})
  @Validated(BigIntegerValidationGroup.class)
  public ResponseEntity<Void> dtoAttributeParamBigInteger(@RequestBody @Valid UnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/dto-attribute/paramFloat", method = {POST, PUT, PATCH, DELETE})
  @Validated(FloatValidationGroup.class)
  public ResponseEntity<Void> dtoAttributeParamFloat(@RequestBody @Valid UnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/dto-attribute/paramDouble", method = {POST, PUT, PATCH, DELETE})
  @Validated(DoubleValidationGroup.class)
  public ResponseEntity<Void> dtoAttributeParamDouble(@RequestBody @Valid UnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/dto-attribute/paramBigDecimal", method = {POST, PUT, PATCH, DELETE})
  @Validated(BigDecimalValidationGroup.class)
  public ResponseEntity<Void> dtoAttributeParamBigDecimal(@RequestBody @Valid UnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/dto-attribute/paramBoolean", method = {POST, PUT, PATCH, DELETE})
  @Validated(BooleanValidationGroup.class)
  public ResponseEntity<Void> dtoAttributeParamBoolean(@RequestBody @Valid UnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/dto-attribute/paramOffsetDateTime", method = {POST, PUT, PATCH, DELETE})
  @Validated(OffsetDateTimeValidationGroup.class)
  public ResponseEntity<Void> dtoAttributeParamOffsetDateTime(@RequestBody @Valid UnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/dto-attribute/paramLocalDateTime", method = {POST, PUT, PATCH, DELETE})
  @Validated(LocalDateTimeValidationGroup.class)
  public ResponseEntity<Void> dtoAttributeParamLocalDateTime(@RequestBody @Valid UnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/dto-attribute/paramLocalDate", method = {POST, PUT, PATCH, DELETE})
  @Validated(LocalDateValidationGroup.class)
  public ResponseEntity<Void> dtoAttributeParamLocalDate(@RequestBody @Valid UnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/dto-attribute/paramLocalTime", method = {POST, PUT, PATCH, DELETE})
  @Validated(LocalTimeValidationGroup.class)
  public ResponseEntity<Void> dtoAttributeParamLocalTime(@RequestBody @Valid UnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/dto-attribute/paramYear", method = {POST, PUT, PATCH, DELETE})
  @Validated(YearValidationGroup.class)
  public ResponseEntity<Void> dtoAttributeParamYear(@RequestBody @Valid UnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/dto-attribute/paramEnumAB", method = {POST, PUT, PATCH, DELETE})
  @Validated(EnumABGroup.class)
  public ResponseEntity<Void> dtoAttributeParamEnumAB(@RequestBody @Valid UnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/dto-attribute/paramEnumLowerAB", method = {POST, PUT, PATCH, DELETE})
  @Validated(EnumLowerABGroup.class)
  public ResponseEntity<Void> dtoAttributeParamEnumLowerAB(@RequestBody @Valid UnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/named-dto-attribute/paramString", method = {POST, PUT, PATCH, DELETE})
  @Validated({StringValidationGroup.class})
  public ResponseEntity<Void> namedDtoAttributeParamString(@RequestBody @Valid NamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/named-dto-attribute/paramInteger", method = {POST, PUT, PATCH, DELETE})
  @Validated(IntegerValidationGroup.class)
  public ResponseEntity<Void> namedDtoAttributeParamInteger(@RequestBody @Valid NamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/named-dto-attribute/paramLong", method = {POST, PUT, PATCH, DELETE})
  @Validated(LongValidationGroup.class)
  public ResponseEntity<Void> namedDtoAttributeParamLong(@RequestBody @Valid NamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/named-dto-attribute/paramBigInteger", method = {POST, PUT, PATCH, DELETE})
  @Validated(BigIntegerValidationGroup.class)
  public ResponseEntity<Void> namedDtoAttributeParamBigInteger(@RequestBody @Valid NamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/named-dto-attribute/paramFloat", method = {POST, PUT, PATCH, DELETE})
  @Validated(FloatValidationGroup.class)
  public ResponseEntity<Void> namedDtoAttributeParamFloat(@RequestBody @Valid NamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/named-dto-attribute/paramDouble", method = {POST, PUT, PATCH, DELETE})
  @Validated(DoubleValidationGroup.class)
  public ResponseEntity<Void> namedDtoAttributeParamDouble(@RequestBody @Valid NamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/named-dto-attribute/paramBigDecimal", method = {POST, PUT, PATCH, DELETE})
  @Validated(BigDecimalValidationGroup.class)
  public ResponseEntity<Void> namedDtoAttributeParamBigDecimal(@RequestBody @Valid NamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/named-dto-attribute/paramBoolean", method = {POST, PUT, PATCH, DELETE})
  @Validated(BooleanValidationGroup.class)
  public ResponseEntity<Void> namedDtoAttributeParamBoolean(@RequestBody @Valid NamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/named-dto-attribute/paramOffsetDateTime", method = {POST, PUT, PATCH, DELETE})
  @Validated(OffsetDateTimeValidationGroup.class)
  public ResponseEntity<Void> namedDtoAttributeParamOffsetDateTime(@RequestBody @Valid NamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/named-dto-attribute/paramLocalDateTime", method = {POST, PUT, PATCH, DELETE})
  @Validated(LocalDateTimeValidationGroup.class)
  public ResponseEntity<Void> namedDtoAttributeParamLocalDateTime(@RequestBody @Valid NamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/named-dto-attribute/paramLocalDate", method = {POST, PUT, PATCH, DELETE})
  @Validated(LocalDateValidationGroup.class)
  public ResponseEntity<Void> namedDtoAttributeParamLocalDate(@RequestBody @Valid NamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/named-dto-attribute/paramLocalTime", method = {POST, PUT, PATCH, DELETE})
  @Validated(LocalTimeValidationGroup.class)
  public ResponseEntity<Void> namedDtoAttributeParamLocalTime(@RequestBody @Valid NamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/named-dto-attribute/paramYear", method = {POST, PUT, PATCH, DELETE})
  @Validated(YearValidationGroup.class)
  public ResponseEntity<Void> namedDtoAttributeParamYear(@RequestBody @Valid NamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/named-dto-attribute/paramEnumAB", method = {POST, PUT, PATCH, DELETE})
  @Validated(EnumABGroup.class)
  public ResponseEntity<Void> namedDtoAttributeParamEnumAB(@RequestBody @Valid NamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/named-dto-attribute/paramEnumLowerAB", method = {POST, PUT, PATCH, DELETE})
  @Validated(EnumLowerABGroup.class)
  public ResponseEntity<Void> namedDtoAttributeParamEnumLowerAB(@RequestBody @Valid NamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/optional-dto-attribute/paramString", method = {POST, PUT, PATCH, DELETE})
  @Validated({StringValidationGroup.class})
  public ResponseEntity<Void> optionalDtoAttributeParamString(@RequestBody @Valid UnamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/optional-dto-attribute/paramInteger", method = {POST, PUT, PATCH, DELETE})
  @Validated(IntegerValidationGroup.class)
  public ResponseEntity<Void> optionalDtoAttributeParamInteger(
      @RequestBody @Valid UnamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/optional-dto-attribute/paramLong", method = {POST, PUT, PATCH, DELETE})
  @Validated(LongValidationGroup.class)
  public ResponseEntity<Void> optionalDtoAttributeParamLong(@RequestBody @Valid UnamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/optional-dto-attribute/paramBigInteger", method = {POST, PUT, PATCH, DELETE})
  @Validated(BigIntegerValidationGroup.class)
  public ResponseEntity<Void> optionalDtoAttributeParamBigInteger(
      @RequestBody @Valid UnamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/optional-dto-attribute/paramFloat", method = {POST, PUT, PATCH, DELETE})
  @Validated(FloatValidationGroup.class)
  public ResponseEntity<Void> optionalDtoAttributeParamFloat(@RequestBody @Valid UnamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/optional-dto-attribute/paramDouble", method = {POST, PUT, PATCH, DELETE})
  @Validated(DoubleValidationGroup.class)
  public ResponseEntity<Void> optionalDtoAttributeParamDouble(@RequestBody @Valid UnamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/optional-dto-attribute/paramBigDecimal", method = {POST, PUT, PATCH, DELETE})
  @Validated(BigDecimalValidationGroup.class)
  public ResponseEntity<Void> optionalDtoAttributeParamBigDecimal(
      @RequestBody @Valid UnamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/optional-dto-attribute/paramBoolean", method = {POST, PUT, PATCH, DELETE})
  @Validated(BooleanValidationGroup.class)
  public ResponseEntity<Void> optionalDtoAttributeParamBoolean(
      @RequestBody @Valid UnamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/optional-dto-attribute/paramOffsetDateTime", method = {POST, PUT, PATCH, DELETE})
  @Validated(OffsetDateTimeValidationGroup.class)
  public ResponseEntity<Void> optionalDtoAttributeParamOffsetDateTime(
      @RequestBody @Valid UnamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/optional-dto-attribute/paramLocalDateTime", method = {POST, PUT, PATCH, DELETE})
  @Validated(LocalDateTimeValidationGroup.class)
  public ResponseEntity<Void> optionalDtoAttributeParamLocalDateTime(
      @RequestBody @Valid UnamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/optional-dto-attribute/paramLocalDate", method = {POST, PUT, PATCH, DELETE})
  @Validated(LocalDateValidationGroup.class)
  public ResponseEntity<Void> optionalDtoAttributeParamLocalDate(
      @RequestBody @Valid UnamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/optional-dto-attribute/paramLocalTime", method = {POST, PUT, PATCH, DELETE})
  @Validated(LocalTimeValidationGroup.class)
  public ResponseEntity<Void> optionalDtoAttributeParamLocalTime(
      @RequestBody @Valid UnamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/optional-dto-attribute/paramYear", method = {POST, PUT, PATCH, DELETE})
  @Validated(YearValidationGroup.class)
  public ResponseEntity<Void> optionalDtoAttributeParamYear(@RequestBody @Valid UnamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/optional-dto-attribute/paramEnumAB", method = {POST, PUT, PATCH, DELETE})
  @Validated(EnumABGroup.class)
  public ResponseEntity<Void> optionalDtoAttributeParamEnumAB(@RequestBody @Valid UnamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/optional-dto-attribute/paramEnumLowerAB", method = {POST, PUT, PATCH, DELETE})
  @Validated(EnumLowerABGroup.class)
  public ResponseEntity<Void> optionalDtoAttributeParamEnumLowerAB(
      @RequestBody @Valid UnamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/named-optional-dto-attribute/paramString", method = {POST, PUT, PATCH, DELETE})
  @Validated({StringValidationGroup.class})
  public ResponseEntity<Void> namedOptionalDtoAttributeParamString(
      @RequestBody @Valid NamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/named-optional-dto-attribute/paramInteger", method = {POST, PUT, PATCH, DELETE})
  @Validated(IntegerValidationGroup.class)
  public ResponseEntity<Void> namedOptionalDtoAttributeParamInteger(
      @RequestBody @Valid NamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/named-optional-dto-attribute/paramLong", method = {POST, PUT, PATCH, DELETE})
  @Validated(LongValidationGroup.class)
  public ResponseEntity<Void> namedOptionalDtoAttributeParamLong(
      @RequestBody @Valid NamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/named-optional-dto-attribute/paramBigInteger", method = {POST, PUT, PATCH, DELETE})
  @Validated(BigIntegerValidationGroup.class)
  public ResponseEntity<Void> namedOptionalDtoAttributeParamBigInteger(
      @RequestBody @Valid NamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/named-optional-dto-attribute/paramFloat", method = {POST, PUT, PATCH, DELETE})
  @Validated(FloatValidationGroup.class)
  public ResponseEntity<Void> namedOptionalDtoAttributeParamFloat(
      @RequestBody @Valid NamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/named-optional-dto-attribute/paramDouble", method = {POST, PUT, PATCH, DELETE})
  @Validated(DoubleValidationGroup.class)
  public ResponseEntity<Void> namedOptionalDtoAttributeParamDouble(
      @RequestBody @Valid NamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/named-optional-dto-attribute/paramBigDecimal", method = {POST, PUT, PATCH, DELETE})
  @Validated(BigDecimalValidationGroup.class)
  public ResponseEntity<Void> namedOptionalDtoAttributeParamBigDecimal(
      @RequestBody @Valid NamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/named-optional-dto-attribute/paramBoolean", method = {POST, PUT, PATCH, DELETE})
  @Validated(BooleanValidationGroup.class)
  public ResponseEntity<Void> namedOptionalDtoAttributeParamBoolean(
      @RequestBody @Valid NamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/named-optional-dto-attribute/paramOffsetDateTime", method = {POST, PUT, PATCH, DELETE})
  @Validated(OffsetDateTimeValidationGroup.class)
  public ResponseEntity<Void> namedOptionalDtoAttributeParamOffsetDateTime(
      @RequestBody @Valid NamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/named-optional-dto-attribute/paramLocalDateTime", method = {POST, PUT, PATCH, DELETE})
  @Validated(LocalDateTimeValidationGroup.class)
  public ResponseEntity<Void> namedOptionalDtoAttributeParamLocalDateTime(
      @RequestBody @Valid NamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/named-optional-dto-attribute/paramLocalDate", method = {POST, PUT, PATCH, DELETE})
  @Validated(LocalDateValidationGroup.class)
  public ResponseEntity<Void> namedOptionalDtoAttributeParamLocalDate(
      @RequestBody @Valid NamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/named-optional-dto-attribute/paramLocalTime", method = {POST, PUT, PATCH, DELETE})
  @Validated(LocalTimeValidationGroup.class)
  public ResponseEntity<Void> namedOptionalDtoAttributeParamLocalTime(
      @RequestBody @Valid NamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/named-optional-dto-attribute/paramYear", method = {POST, PUT, PATCH, DELETE})
  @Validated(YearValidationGroup.class)
  public ResponseEntity<Void> namedOptionalDtoAttributeParamYear(
      @RequestBody @Valid NamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/named-optional-dto-attribute/paramEnumAB", method = {POST, PUT, PATCH, DELETE})
  @Validated(EnumABGroup.class)
  public ResponseEntity<Void> namedOptionalDtoAttributeParamEnumAB(
      @RequestBody @Valid NamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/named-optional-dto-attribute/paramEnumLowerAB", method = {POST, PUT, PATCH, DELETE})
  @Validated(EnumLowerABGroup.class)
  public ResponseEntity<Void> namedOptionalDtoAttributeParamEnumLowerAB(
      @RequestBody @Valid NamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-dto-attribute/paramString", method = {POST, PUT, PATCH, DELETE})
  @Validated({StringValidationGroup.class})
  public ResponseEntity<Void> nestedDtoAttributeParamString(@RequestBody @Valid ParentUnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-dto-attribute/paramInteger", method = {POST, PUT, PATCH, DELETE})
  @Validated(IntegerValidationGroup.class)
  public ResponseEntity<Void> nestedDtoAttributeParamInteger(@RequestBody @Valid ParentUnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-dto-attribute/paramLong", method = {POST, PUT, PATCH, DELETE})
  @Validated(LongValidationGroup.class)
  public ResponseEntity<Void> nestedDtoAttributeParamLong(@RequestBody @Valid ParentUnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-dto-attribute/paramBigInteger", method = {POST, PUT, PATCH, DELETE})
  @Validated(BigIntegerValidationGroup.class)
  public ResponseEntity<Void> nestedDtoAttributeParamBigInteger(@RequestBody @Valid ParentUnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-dto-attribute/paramFloat", method = {POST, PUT, PATCH, DELETE})
  @Validated(FloatValidationGroup.class)
  public ResponseEntity<Void> nestedDtoAttributeParamFloat(@RequestBody @Valid ParentUnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-dto-attribute/paramDouble", method = {POST, PUT, PATCH, DELETE})
  @Validated(DoubleValidationGroup.class)
  public ResponseEntity<Void> nestedDtoAttributeParamDouble(@RequestBody @Valid ParentUnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-dto-attribute/paramBigDecimal", method = {POST, PUT, PATCH, DELETE})
  @Validated(BigDecimalValidationGroup.class)
  public ResponseEntity<Void> nestedDtoAttributeParamBigDecimal(@RequestBody @Valid ParentUnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-dto-attribute/paramBoolean", method = {POST, PUT, PATCH, DELETE})
  @Validated(BooleanValidationGroup.class)
  public ResponseEntity<Void> nestedDtoAttributeParamBoolean(@RequestBody @Valid ParentUnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-dto-attribute/paramOffsetDateTime", method = {POST, PUT, PATCH, DELETE})
  @Validated(OffsetDateTimeValidationGroup.class)
  public ResponseEntity<Void> nestedDtoAttributeParamOffsetDateTime(
      @RequestBody @Valid ParentUnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-dto-attribute/paramLocalDateTime", method = {POST, PUT, PATCH, DELETE})
  @Validated(LocalDateTimeValidationGroup.class)
  public ResponseEntity<Void> nestedDtoAttributeParamLocalDateTime(
      @RequestBody @Valid ParentUnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-dto-attribute/paramLocalDate", method = {POST, PUT, PATCH, DELETE})
  @Validated(LocalDateValidationGroup.class)
  public ResponseEntity<Void> nestedDtoAttributeParamLocalDate(@RequestBody @Valid ParentUnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-dto-attribute/paramLocalTime", method = {POST, PUT, PATCH, DELETE})
  @Validated(LocalTimeValidationGroup.class)
  public ResponseEntity<Void> nestedDtoAttributeParamLocalTime(@RequestBody @Valid ParentUnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-dto-attribute/paramYear", method = {POST, PUT, PATCH, DELETE})
  @Validated(YearValidationGroup.class)
  public ResponseEntity<Void> nestedDtoAttributeParamYear(@RequestBody @Valid ParentUnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-dto-attribute/paramEnumAB", method = {POST, PUT, PATCH, DELETE})
  @Validated(EnumABGroup.class)
  public ResponseEntity<Void> nestedDtoAttributeParamEnumAB(@RequestBody @Valid ParentUnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-dto-attribute/paramEnumLowerAB", method = {POST, PUT, PATCH, DELETE})
  @Validated(EnumLowerABGroup.class)
  public ResponseEntity<Void> nestedDtoAttributeParamEnumLowerAB(
      @RequestBody @Valid ParentUnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-optional-dto-attribute/paramString", method = {POST, PUT, PATCH, DELETE})
  @Validated({StringValidationGroup.class})
  public ResponseEntity<Void> nestedOptionalDtoAttributeParamString(
      @RequestBody @Valid ParentUnamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-optional-dto-attribute/paramInteger", method = {POST, PUT, PATCH, DELETE})
  @Validated(IntegerValidationGroup.class)
  public ResponseEntity<Void> nestedOptionalDtoAttributeParamInteger(
      @RequestBody @Valid ParentUnamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-optional-dto-attribute/paramLong", method = {POST, PUT, PATCH, DELETE})
  @Validated(LongValidationGroup.class)
  public ResponseEntity<Void> nestedOptionalDtoAttributeParamLong(
      @RequestBody @Valid ParentUnamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-optional-dto-attribute/paramBigInteger", method = {POST, PUT, PATCH, DELETE})
  @Validated(BigIntegerValidationGroup.class)
  public ResponseEntity<Void> nestedOptionalDtoAttributeParamBigInteger(
      @RequestBody @Valid ParentUnamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-optional-dto-attribute/paramFloat", method = {POST, PUT, PATCH, DELETE})
  @Validated(FloatValidationGroup.class)
  public ResponseEntity<Void> nestedOptionalDtoAttributeParamFloat(
      @RequestBody @Valid ParentUnamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-optional-dto-attribute/paramDouble", method = {POST, PUT, PATCH, DELETE})
  @Validated(DoubleValidationGroup.class)
  public ResponseEntity<Void> nestedOptionalDtoAttributeParamDouble(
      @RequestBody @Valid ParentUnamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-optional-dto-attribute/paramBigDecimal", method = {POST, PUT, PATCH, DELETE})
  @Validated(BigDecimalValidationGroup.class)
  public ResponseEntity<Void> nestedOptionalDtoAttributeParamBigDecimal(
      @RequestBody @Valid ParentUnamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-optional-dto-attribute/paramBoolean", method = {POST, PUT, PATCH, DELETE})
  @Validated(BooleanValidationGroup.class)
  public ResponseEntity<Void> nestedOptionalDtoAttributeParamBoolean(
      @RequestBody @Valid ParentUnamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-optional-dto-attribute/paramOffsetDateTime", method = {POST, PUT, PATCH, DELETE})
  @Validated(OffsetDateTimeValidationGroup.class)
  public ResponseEntity<Void> nestedOptionalDtoAttributeParamOffsetDateTime(
      @RequestBody @Valid ParentUnamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-optional-dto-attribute/paramLocalDateTime", method = {POST, PUT, PATCH, DELETE})
  @Validated(LocalDateTimeValidationGroup.class)
  public ResponseEntity<Void> nestedOptionalDtoAttributeParamLocalDateTime(
      @RequestBody @Valid ParentUnamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-optional-dto-attribute/paramLocalDate", method = {POST, PUT, PATCH, DELETE})
  @Validated(LocalDateValidationGroup.class)
  public ResponseEntity<Void> nestedOptionalDtoAttributeParamLocalDate(
      @RequestBody @Valid ParentUnamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-optional-dto-attribute/paramLocalTime", method = {POST, PUT, PATCH, DELETE})
  @Validated(LocalTimeValidationGroup.class)
  public ResponseEntity<Void> nestedOptionalDtoAttributeParamLocalTime(
      @RequestBody @Valid ParentUnamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-optional-dto-attribute/paramYear", method = {POST, PUT, PATCH, DELETE})
  @Validated(YearValidationGroup.class)
  public ResponseEntity<Void> nestedOptionalDtoAttributeParamYear(
      @RequestBody @Valid ParentUnamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-optional-dto-attribute/paramEnumAB", method = {POST, PUT, PATCH, DELETE})
  @Validated(EnumABGroup.class)
  public ResponseEntity<Void> nestedOptionalDtoAttributeParamEnumAB(
      @RequestBody @Valid ParentUnamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-optional-dto-attribute/paramEnumLowerAB", method = {POST, PUT, PATCH, DELETE})
  @Validated(EnumLowerABGroup.class)
  public ResponseEntity<Void> nestedOptionalDtoAttributeParamEnumLowerAB(
      @RequestBody @Valid ParentUnamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/named-nested-dto-attribute/paramString", method = {POST, PUT, PATCH, DELETE})
  @Validated({StringValidationGroup.class})
  public ResponseEntity<Void> namedNestedDtoAttributeParamString(@RequestBody @Valid ParentNamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/named-nested-dto-attribute/paramInteger", method = {POST, PUT, PATCH, DELETE})
  @Validated(IntegerValidationGroup.class)
  public ResponseEntity<Void> namedNestedDtoAttributeParamInteger(
      @RequestBody @Valid ParentNamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/named-nested-dto-attribute/paramLong", method = {POST, PUT, PATCH, DELETE})
  @Validated(LongValidationGroup.class)
  public ResponseEntity<Void> namedNestedDtoAttributeParamLong(@RequestBody @Valid ParentNamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/named-nested-dto-attribute/paramBigInteger", method = {POST, PUT, PATCH, DELETE})
  @Validated(BigIntegerValidationGroup.class)
  public ResponseEntity<Void> namedNestedDtoAttributeParamBigInteger(
      @RequestBody @Valid ParentNamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/named-nested-dto-attribute/paramFloat", method = {POST, PUT, PATCH, DELETE})
  @Validated(FloatValidationGroup.class)
  public ResponseEntity<Void> namedNestedDtoAttributeParamFloat(@RequestBody @Valid ParentNamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/named-nested-dto-attribute/paramDouble", method = {POST, PUT, PATCH, DELETE})
  @Validated(DoubleValidationGroup.class)
  public ResponseEntity<Void> namedNestedDtoAttributeParamDouble(@RequestBody @Valid ParentNamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/named-nested-dto-attribute/paramBigDecimal", method = {POST, PUT, PATCH, DELETE})
  @Validated(BigDecimalValidationGroup.class)
  public ResponseEntity<Void> namedNestedDtoAttributeParamBigDecimal(
      @RequestBody @Valid ParentNamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/named-nested-dto-attribute/paramBoolean", method = {POST, PUT, PATCH, DELETE})
  @Validated(BooleanValidationGroup.class)
  public ResponseEntity<Void> namedNestedDtoAttributeParamBoolean(
      @RequestBody @Valid ParentNamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/named-nested-dto-attribute/paramOffsetDateTime", method = {POST, PUT, PATCH, DELETE})
  @Validated(OffsetDateTimeValidationGroup.class)
  public ResponseEntity<Void> namedNestedDtoAttributeParamOffsetDateTime(
      @RequestBody @Valid ParentNamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/named-nested-dto-attribute/paramLocalDateTime", method = {POST, PUT, PATCH, DELETE})
  @Validated(LocalDateTimeValidationGroup.class)
  public ResponseEntity<Void> namedNestedDtoAttributeParamLocalDateTime(
      @RequestBody @Valid ParentNamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/named-nested-dto-attribute/paramLocalDate", method = {POST, PUT, PATCH, DELETE})
  @Validated(LocalDateValidationGroup.class)
  public ResponseEntity<Void> namedNestedDtoAttributeParamLocalDate(
      @RequestBody @Valid ParentNamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/named-nested-dto-attribute/paramLocalTime", method = {POST, PUT, PATCH, DELETE})
  @Validated(LocalTimeValidationGroup.class)
  public ResponseEntity<Void> namedNestedDtoAttributeParamLocalTime(
      @RequestBody @Valid ParentNamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/named-nested-dto-attribute/paramYear", method = {POST, PUT, PATCH, DELETE})
  @Validated(YearValidationGroup.class)
  public ResponseEntity<Void> namedNestedDtoAttributeParamYear(@RequestBody @Valid ParentNamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/named-nested-dto-attribute/paramEnumAB", method = {POST, PUT, PATCH, DELETE})
  @Validated(EnumABGroup.class)
  public ResponseEntity<Void> namedNestedDtoAttributeParamEnumAB(@RequestBody @Valid ParentNamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/named-nested-dto-attribute/paramEnumLowerAB", method = {POST, PUT, PATCH, DELETE})
  @Validated(EnumLowerABGroup.class)
  public ResponseEntity<Void> namedNestedDtoAttributeParamEnumLowerAB(
      @RequestBody @Valid ParentNamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/named-nested-optional-dto-attribute/paramString", method = {POST, PUT, PATCH, DELETE})
  @Validated({StringValidationGroup.class})
  public ResponseEntity<Void> namedNestedOptionalDtoAttributeParamString(
      @RequestBody @Valid ParentNamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/named-nested-optional-dto-attribute/paramInteger", method = {POST, PUT, PATCH, DELETE})
  @Validated(IntegerValidationGroup.class)
  public ResponseEntity<Void> namedNestedOptionalDtoAttributeParamInteger(
      @RequestBody @Valid ParentNamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/named-nested-optional-dto-attribute/paramLong", method = {POST, PUT, PATCH, DELETE})
  @Validated(LongValidationGroup.class)
  public ResponseEntity<Void> namedNestedOptionalDtoAttributeParamLong(
      @RequestBody @Valid ParentNamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/named-nested-optional-dto-attribute/paramBigInteger", method = {POST, PUT, PATCH, DELETE})
  @Validated(BigIntegerValidationGroup.class)
  public ResponseEntity<Void> namedNestedOptionalDtoAttributeParamBigInteger(
      @RequestBody @Valid ParentNamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/named-nested-optional-dto-attribute/paramFloat", method = {POST, PUT, PATCH, DELETE})
  @Validated(FloatValidationGroup.class)
  public ResponseEntity<Void> namedNestedOptionalDtoAttributeParamFloat(
      @RequestBody @Valid ParentNamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/named-nested-optional-dto-attribute/paramDouble", method = {POST, PUT, PATCH, DELETE})
  @Validated(DoubleValidationGroup.class)
  public ResponseEntity<Void> namedNestedOptionalDtoAttributeParamDouble(
      @RequestBody @Valid ParentNamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/named-nested-optional-dto-attribute/paramBigDecimal", method = {POST, PUT, PATCH, DELETE})
  @Validated(BigDecimalValidationGroup.class)
  public ResponseEntity<Void> namedNestedOptionalDtoAttributeParamBigDecimal(
      @RequestBody @Valid ParentNamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/named-nested-optional-dto-attribute/paramBoolean", method = {POST, PUT, PATCH, DELETE})
  @Validated(BooleanValidationGroup.class)
  public ResponseEntity<Void> namedNestedOptionalDtoAttributeParamBoolean(
      @RequestBody @Valid ParentNamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/named-nested-optional-dto-attribute/paramOffsetDateTime", method = {POST, PUT, PATCH,
      DELETE})
  @Validated(OffsetDateTimeValidationGroup.class)
  public ResponseEntity<Void> namedNestedOptionalDtoAttributeParamOffsetDateTime(
      @RequestBody @Valid ParentNamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/named-nested-optional-dto-attribute/paramLocalDateTime", method = {POST, PUT, PATCH, DELETE})
  @Validated(LocalDateTimeValidationGroup.class)
  public ResponseEntity<Void> namedNestedOptionalDtoAttributeParamLocalDateTime(
      @RequestBody @Valid ParentNamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/named-nested-optional-dto-attribute/paramLocalDate", method = {POST, PUT, PATCH, DELETE})
  @Validated(LocalDateValidationGroup.class)
  public ResponseEntity<Void> namedNestedOptionalDtoAttributeParamLocalDate(
      @RequestBody @Valid ParentNamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/named-nested-optional-dto-attribute/paramLocalTime", method = {POST, PUT, PATCH, DELETE})
  @Validated(LocalTimeValidationGroup.class)
  public ResponseEntity<Void> namedNestedOptionalDtoAttributeParamLocalTime(
      @RequestBody @Valid ParentNamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/named-nested-optional-dto-attribute/paramYear", method = {POST, PUT, PATCH, DELETE})
  @Validated(YearValidationGroup.class)
  public ResponseEntity<Void> namedNestedOptionalDtoAttributeParamYear(
      @RequestBody @Valid ParentNamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/named-nested-optional-dto-attribute/paramEnumAB", method = {POST, PUT, PATCH, DELETE})
  @Validated(EnumABGroup.class)
  public ResponseEntity<Void> namedNestedOptionalDtoAttributeParamEnumAB(
      @RequestBody @Valid ParentNamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/named-nested-optional-dto-attribute/paramEnumLowerAB", method = {POST, PUT, PATCH, DELETE})
  @Validated(EnumLowerABGroup.class)
  public ResponseEntity<Void> namedNestedOptionalDtoAttributeParamEnumLowerAB(
      @RequestBody @Valid ParentNamedConstrainedOptionalDto params) {
    return null;
  }
}
