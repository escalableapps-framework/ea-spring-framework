package com.escalableapps.framework.rest.test.api;

import static com.escalableapps.framework.rest.RestConfigurationTest.RestConfigurationTestContext.RECEIVED_VALUES;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.HEAD;
import static org.springframework.web.bind.annotation.RequestMethod.PATCH;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.Year;
import java.util.Optional;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.escalableapps.framework.core.util.JsonUtils;
import com.escalableapps.framework.rest.RestConfigurationTest.EnumAB;
import com.escalableapps.framework.rest.RestConfigurationTest.EnumLowerAB;
import com.escalableapps.framework.rest.RestConfigurationTest.ParentUnamedNotNullDto;
import com.escalableapps.framework.rest.RestConfigurationTest.ParentUnamedNotNullOptionalDto;
import com.escalableapps.framework.rest.RestConfigurationTest.UnamedNotNullDto;
import com.escalableapps.framework.rest.RestConfigurationTest.UnamedNotNullOptionalDto;

@RestController
@RequestMapping("/api/v1/request-parameter")
public class RequestParamUnconstrainedRestApi {

  @RequestMapping(path = "/method-parameter", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameter( //
      @RequestParam(required = false) String paramString, //
      @RequestParam(required = false) Integer paramInteger, //
      @RequestParam(required = false) Long paramLong, //
      @RequestParam(required = false) BigInteger paramBigInteger, //
      @RequestParam(required = false) Float paramFloat, //
      @RequestParam(required = false) Double paramDouble, //
      @RequestParam(required = false) BigDecimal paramBigDecimal, //
      @RequestParam(required = false) Boolean paramBoolean, //
      @RequestParam(required = false) OffsetDateTime paramOffsetDateTime, //
      @RequestParam(required = false) LocalDateTime paramLocalDateTime, //
      @RequestParam(required = false) LocalDate paramLocalDate, //
      @RequestParam(required = false) LocalTime paramLocalTime, //
      @RequestParam(required = false) Year paramYear, //
      @RequestParam(required = false) EnumAB paramEnumAB, //
      @RequestParam(required = false) EnumLowerAB paramEnumLowerAB //
  ) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramString(paramString) //
        .paramInteger(paramInteger) //
        .paramLong(paramLong) //
        .paramBigInteger(paramBigInteger) //
        .paramFloat(paramFloat) //
        .paramDouble(paramDouble) //
        .paramBigDecimal(paramBigDecimal) //
        .paramBoolean(paramBoolean) //
        .paramOffsetDateTime(paramOffsetDateTime) //
        .paramLocalDateTime(paramLocalDateTime) //
        .paramLocalDate(paramLocalDate) //
        .paramLocalTime(paramLocalTime) //
        .paramYear(paramYear) //
        .paramEnumAB(paramEnumAB) //
        .paramEnumLowerAB(paramEnumLowerAB) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/named-method-parameter", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameter( //
      @RequestParam(required = false, name = "param-string") String paramString, //
      @RequestParam(required = false, name = "param-integer") Integer paramInteger, //
      @RequestParam(required = false, name = "param-long") Long paramLong, //
      @RequestParam(required = false, name = "param-big-integer") BigInteger paramBigInteger, //
      @RequestParam(required = false, name = "param-float") Float paramFloat, //
      @RequestParam(required = false, name = "param-double") Double paramDouble, //
      @RequestParam(required = false, name = "param-big-decimal") BigDecimal paramBigDecimal, //
      @RequestParam(required = false, name = "param-boolean") Boolean paramBoolean, //
      @RequestParam(required = false, name = "param-offset-date-time") OffsetDateTime paramOffsetDateTime, //
      @RequestParam(required = false, name = "param-local-date-time") LocalDateTime paramLocalDateTime, //
      @RequestParam(required = false, name = "param-local-date") LocalDate paramLocalDate, //
      @RequestParam(required = false, name = "param-local-time") LocalTime paramLocalTime, //
      @RequestParam(required = false, name = "param-year") Year paramYear, //
      @RequestParam(required = false, name = "param-enum-ab") EnumAB paramEnumAB, //
      @RequestParam(required = false, name = "param-enum-lower-ab") EnumLowerAB paramEnumLowerAB //
  ) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramString(paramString) //
        .paramInteger(paramInteger) //
        .paramLong(paramLong) //
        .paramBigInteger(paramBigInteger) //
        .paramFloat(paramFloat) //
        .paramDouble(paramDouble) //
        .paramBigDecimal(paramBigDecimal) //
        .paramBoolean(paramBoolean) //
        .paramOffsetDateTime(paramOffsetDateTime) //
        .paramLocalDateTime(paramLocalDateTime) //
        .paramLocalDate(paramLocalDate) //
        .paramLocalTime(paramLocalTime) //
        .paramYear(paramYear) //
        .paramEnumAB(paramEnumAB) //
        .paramEnumLowerAB(paramEnumLowerAB) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/optional-method-parameter", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> optionalMethodParameter( //
      @RequestParam Optional<String> paramString, //
      @RequestParam Optional<Integer> paramInteger, //
      @RequestParam Optional<Long> paramLong, //
      @RequestParam Optional<BigInteger> paramBigInteger, //
      @RequestParam Optional<Float> paramFloat, //
      @RequestParam Optional<Double> paramDouble, //
      @RequestParam Optional<BigDecimal> paramBigDecimal, //
      @RequestParam Optional<Boolean> paramBoolean, //
      @RequestParam Optional<OffsetDateTime> paramOffsetDateTime, //
      @RequestParam Optional<LocalDateTime> paramLocalDateTime, //
      @RequestParam Optional<LocalDate> paramLocalDate, //
      @RequestParam Optional<LocalTime> paramLocalTime, //
      @RequestParam Optional<Year> paramYear, //
      @RequestParam Optional<EnumAB> paramEnumAB, //
      @RequestParam Optional<EnumLowerAB> paramEnumLowerAB //
  ) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramString(paramString.orElse(null)) //
        .paramInteger(paramInteger.orElse(null)) //
        .paramLong(paramLong.orElse(null)) //
        .paramBigInteger(paramBigInteger.orElse(null)) //
        .paramFloat(paramFloat.orElse(null)) //
        .paramDouble(paramDouble.orElse(null)) //
        .paramBigDecimal(paramBigDecimal.orElse(null)) //
        .paramBoolean(paramBoolean.orElse(null)) //
        .paramOffsetDateTime(paramOffsetDateTime.orElse(null)) //
        .paramLocalDateTime(paramLocalDateTime.orElse(null)) //
        .paramLocalDate(paramLocalDate.orElse(null)) //
        .paramLocalTime(paramLocalTime.orElse(null)) //
        .paramYear(paramYear.orElse(null)) //
        .paramEnumAB(paramEnumAB.orElse(null)) //
        .paramEnumLowerAB(paramEnumLowerAB.orElse(null)) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/named-optional-method-parameter", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedOptionalMethodParameter( //
      @RequestParam(required = false, name = "param-string") Optional<String> paramString, //
      @RequestParam(required = false, name = "param-integer") Optional<Integer> paramInteger, //
      @RequestParam(required = false, name = "param-long") Optional<Long> paramLong, //
      @RequestParam(required = false, name = "param-big-integer") Optional<BigInteger> paramBigInteger, //
      @RequestParam(required = false, name = "param-float") Optional<Float> paramFloat, //
      @RequestParam(required = false, name = "param-double") Optional<Double> paramDouble, //
      @RequestParam(required = false, name = "param-big-decimal") Optional<BigDecimal> paramBigDecimal, //
      @RequestParam(required = false, name = "param-boolean") Optional<Boolean> paramBoolean, //
      @RequestParam(required = false, name = "param-offset-date-time") Optional<OffsetDateTime> paramOffsetDateTime, //
      @RequestParam(required = false, name = "param-local-date-time") Optional<LocalDateTime> paramLocalDateTime, //
      @RequestParam(required = false, name = "param-local-date") Optional<LocalDate> paramLocalDate, //
      @RequestParam(required = false, name = "param-local-time") Optional<LocalTime> paramLocalTime, //
      @RequestParam(required = false, name = "param-year") Optional<Year> paramYear, //
      @RequestParam(required = false, name = "param-enum-ab") Optional<EnumAB> paramEnumAB, //
      @RequestParam(required = false, name = "param-enum-lower-ab") Optional<EnumLowerAB> paramEnumLowerAB //
  ) {
    UnamedNotNullDto params = UnamedNotNullDto.builder() //
        .paramString(paramString.orElse(null)) //
        .paramInteger(paramInteger.orElse(null)) //
        .paramLong(paramLong.orElse(null)) //
        .paramBigInteger(paramBigInteger.orElse(null)) //
        .paramFloat(paramFloat.orElse(null)) //
        .paramDouble(paramDouble.orElse(null)) //
        .paramBigDecimal(paramBigDecimal.orElse(null)) //
        .paramBoolean(paramBoolean.orElse(null)) //
        .paramOffsetDateTime(paramOffsetDateTime.orElse(null)) //
        .paramLocalDateTime(paramLocalDateTime.orElse(null)) //
        .paramLocalDate(paramLocalDate.orElse(null)) //
        .paramLocalTime(paramLocalTime.orElse(null)) //
        .paramYear(paramYear.orElse(null)) //
        .paramEnumAB(paramEnumAB.orElse(null)) //
        .paramEnumLowerAB(paramEnumLowerAB.orElse(null)) //
        .build();
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/dto-attribute", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> dtoAttribute(UnamedNotNullDto params) {
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/optional-dto-attribute", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> optionalDtoAttribute(UnamedNotNullOptionalDto params) {
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/nested-dto-attribute", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> nestedDtoAttribute(ParentUnamedNotNullDto params) {
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }

  @RequestMapping(path = "/nested-optional-dto-attribute", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> nestedOptionalDtoAttribute(ParentUnamedNotNullOptionalDto params) {
    return ResponseEntity.noContent().header(RECEIVED_VALUES.name(), JsonUtils.asJson(params)).build();
  }
}
