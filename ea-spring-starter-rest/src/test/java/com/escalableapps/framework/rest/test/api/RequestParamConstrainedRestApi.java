package com.escalableapps.framework.rest.test.api;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.HEAD;
import static org.springframework.web.bind.annotation.RequestMethod.PATCH;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.Year;
import java.util.Optional;

import javax.validation.Valid;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.Max;
import javax.validation.constraints.Size;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.escalableapps.framework.core.validation.constraints.DateRange;
import com.escalableapps.framework.core.validation.constraints.DatetimeRange;
import com.escalableapps.framework.core.validation.constraints.YearRange;
import com.escalableapps.framework.rest.RestConfigurationTest.BigDecimalValidationGroup;
import com.escalableapps.framework.rest.RestConfigurationTest.BigIntegerValidationGroup;
import com.escalableapps.framework.rest.RestConfigurationTest.IntegerValidationGroup;
import com.escalableapps.framework.rest.RestConfigurationTest.LocalDateTimeValidationGroup;
import com.escalableapps.framework.rest.RestConfigurationTest.LocalDateValidationGroup;
import com.escalableapps.framework.rest.RestConfigurationTest.LongValidationGroup;
import com.escalableapps.framework.rest.RestConfigurationTest.OffsetDateTimeValidationGroup;
import com.escalableapps.framework.rest.RestConfigurationTest.ParentUnamedConstrainedDto;
import com.escalableapps.framework.rest.RestConfigurationTest.ParentUnamedConstrainedOptionalDto;
import com.escalableapps.framework.rest.RestConfigurationTest.StringValidationGroup;
import com.escalableapps.framework.rest.RestConfigurationTest.UnamedConstrainedDto;
import com.escalableapps.framework.rest.RestConfigurationTest.UnamedConstrainedOptionalDto;
import com.escalableapps.framework.rest.RestConfigurationTest.YearValidationGroup;

@RestController
@RequestMapping("/api/v1/request-parameter/constrained")
@Validated
public class RequestParamConstrainedRestApi {

  @RequestMapping(path = "/method-parameter/paramString", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamString(
      @RequestParam(required = false) @Size(max = 1) String paramString) {
    return null;
  }

  @RequestMapping(path = "/method-parameter/paramInteger", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamInteger(
      @RequestParam(required = false) @Max(1) Integer paramInteger) {
    return null;
  }

  @RequestMapping(path = "/method-parameter/paramLong", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamLong(@RequestParam(required = false) @Max(1) Long paramLong) {
    return null;
  }

  @RequestMapping(path = "/method-parameter/paramBigInteger", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamBigInteger(
      @RequestParam(required = false) @DecimalMax("1") BigInteger paramBigInteger) {
    return null;
  }

  @RequestMapping(path = "/method-parameter/paramBigDecimal", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamBigDecimal(
      @RequestParam(required = false) @DecimalMax("9.9") BigDecimal paramBigDecimal) {
    return null;
  }

  @RequestMapping(path = "/method-parameter/paramOffsetDateTime", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamOffsetDateTime(
      @RequestParam(required = false) @DatetimeRange(max = "2000-01-01T00:00:00Z") OffsetDateTime paramOffsetDateTime) {
    return null;
  }

  @RequestMapping(path = "/method-parameter/paramLocalDateTime", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamLocalDateTime(
      @RequestParam(required = false) @DatetimeRange(max = "2000-01-01T00:00:00Z") LocalDateTime paramLocalDateTime) {
    return null;
  }

  @RequestMapping(path = "/method-parameter/paramLocalDate", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamLocalDate(
      @RequestParam(required = false) @DateRange(max = "2000-01-01") LocalDate paramLocalDate) {
    return null;
  }

  @RequestMapping(path = "/method-parameter/paramYear", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> methodParameterParamYear(
      @RequestParam(required = false) @YearRange(max = "2000") Year paramYear) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/paramString", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamString(
      @RequestParam(required = true, name = "param-string") @Size(max = 1) String paramString) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/paramInteger", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamInteger(
      @RequestParam(required = true, name = "param-integer") @Max(1) Integer paramInteger) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/paramLong", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamLong(
      @RequestParam(required = true, name = "param-long") @Max(1) Long paramLong) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/paramBigInteger", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamBigInteger(
      @RequestParam(required = true, name = "param-big-integer") @DecimalMax("1") BigInteger paramBigInteger) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/paramBigDecimal", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamBigDecimal(
      @RequestParam(required = true, name = "param-big-decimal") @DecimalMax("9.9") BigDecimal paramBigDecimal) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/paramOffsetDateTime", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamOffsetDateTime(
      @RequestParam(required = true, name = "param-offset-date-time") @DatetimeRange(max = "2000-01-01T00:00:00Z") OffsetDateTime paramOffsetDateTime) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/paramLocalDateTime", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamLocalDateTime(
      @RequestParam(required = true, name = "param-local-date-time") @DatetimeRange(max = "2000-01-01T00:00:00Z") LocalDateTime paramLocalDateTime) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/paramLocalDate", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamLocalDate(
      @RequestParam(required = true, name = "param-local-date") @DateRange(max = "2000-01-01") LocalDate paramLocalDate) {
    return null;
  }

  @RequestMapping(path = "/named-method-parameter/paramYear", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedMethodParameterParamYear(
      @RequestParam(required = true, name = "param-year") @YearRange(max = "2000") Year paramYear) {
    return null;
  }

  @RequestMapping(path = "/optional-method-parameter/paramString", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> optionalMethodParameterParamString(
      @RequestParam(required = false) Optional<@Size(max = 1) String> paramString) {
    return null;
  }

  @RequestMapping(path = "/optional-method-parameter/paramInteger", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> optionalMethodParameterParamInteger(
      @RequestParam(required = false) Optional<@Max(1) Integer> paramInteger) {
    return null;
  }

  @RequestMapping(path = "/optional-method-parameter/paramLong", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> optionalMethodParameterParamLong(
      @RequestParam(required = false) Optional<@Max(1) Long> paramLong) {
    return null;
  }

  @RequestMapping(path = "/optional-method-parameter/paramBigInteger", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> optionalMethodParameterParamBigInteger(
      @RequestParam(required = false) Optional<@DecimalMax("1") BigInteger> paramBigInteger) {
    return null;
  }

  @RequestMapping(path = "/optional-method-parameter/paramBigDecimal", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> optionalMethodParameterParamBigDecimal(
      @RequestParam(required = false) Optional<@DecimalMax("9.9") BigDecimal> paramBigDecimal) {
    return null;
  }

  @RequestMapping(path = "/optional-method-parameter/paramOffsetDateTime", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> optionalMethodParameterParamOffsetDateTime(
      @RequestParam(required = false) Optional<@DatetimeRange(max = "2000-01-01T00:00:00Z") OffsetDateTime> paramOffsetDateTime) {
    return null;
  }

  @RequestMapping(path = "/optional-method-parameter/paramLocalDateTime", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> optionalMethodParameterParamLocalDateTime(
      @RequestParam(required = false) Optional<@DatetimeRange(max = "2000-01-01T00:00:00Z") LocalDateTime> paramLocalDateTime) {
    return null;
  }

  @RequestMapping(path = "/optional-method-parameter/paramLocalDate", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> optionalMethodParameterParamLocalDate(
      @RequestParam(required = false) Optional<@DateRange(max = "2000-01-01") LocalDate> paramLocalDate) {
    return null;
  }

  @RequestMapping(path = "/optional-method-parameter/paramYear", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> optionalMethodParameterParamYear(
      @RequestParam(required = false) Optional<@YearRange(max = "2000") Year> paramYear) {
    return null;
  }

  @RequestMapping(path = "/named-optional-method-parameter/paramString", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedOptionalMethodParameterParamString(
      @RequestParam(required = true, name = "param-string") Optional<@Size(max = 1) String> paramString) {
    return null;
  }

  @RequestMapping(path = "/named-optional-method-parameter/paramInteger", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> namedOptionalMethodParameterParamInteger(
      @RequestParam(required = true, name = "param-integer") Optional<@Max(1) Integer> paramInteger) {
    return null;
  }

  @RequestMapping(path = "/named-optional-method-parameter/paramLong", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedOptionalMethodParameterParamLong(
      @RequestParam(required = true, name = "param-long") Optional<@Max(1) Long> paramLong) {
    return null;
  }

  @RequestMapping(path = "/named-optional-method-parameter/paramBigInteger", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> namedOptionalMethodParameterParamBigInteger(
      @RequestParam(required = true, name = "param-big-integer") Optional<@DecimalMax("1") BigInteger> paramBigInteger) {
    return null;
  }

  @RequestMapping(path = "/named-optional-method-parameter/paramBigDecimal", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> namedOptionalMethodParameterParamBigDecimal(
      @RequestParam(required = true, name = "param-big-decimal") Optional<@DecimalMax("9.9") BigDecimal> paramBigDecimal) {
    return null;
  }

  @RequestMapping(path = "/named-optional-method-parameter/paramOffsetDateTime", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> namedOptionalMethodParameterParamOffsetDateTime(
      @RequestParam(required = true, name = "param-offset-date-time") Optional<@DatetimeRange(max = "2000-01-01T00:00:00Z") OffsetDateTime> paramOffsetDateTime) {
    return null;
  }

  @RequestMapping(path = "/named-optional-method-parameter/paramLocalDateTime", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> namedOptionalMethodParameterParamLocalDateTime(
      @RequestParam(required = true, name = "param-local-date-time") Optional<@DatetimeRange(max = "2000-01-01T00:00:00Z") LocalDateTime> paramLocalDateTime) {
    return null;
  }

  @RequestMapping(path = "/named-optional-method-parameter/paramLocalDate", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  public ResponseEntity<Void> namedOptionalMethodParameterParamLocalDate(
      @RequestParam(required = true, name = "param-local-date") Optional<@DateRange(max = "2000-01-01") LocalDate> paramLocalDate) {
    return null;
  }

  @RequestMapping(path = "/named-optional-method-parameter/paramYear", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  public ResponseEntity<Void> namedOptionalMethodParameterParamYear(
      @RequestParam(required = true, name = "param-year") Optional<@YearRange(max = "2000") Year> paramYear) {
    return null;
  }

  @RequestMapping(path = "/dto-attribute/paramString", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated({StringValidationGroup.class})
  public ResponseEntity<Void> dtoAttributeParamString(@Valid UnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/dto-attribute/paramInteger", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(IntegerValidationGroup.class)
  public ResponseEntity<Void> dtoAttributeParamInteger(@Valid UnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/dto-attribute/paramLong", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(LongValidationGroup.class)
  public ResponseEntity<Void> dtoAttributeParamLong(@Valid UnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/dto-attribute/paramBigInteger", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(BigIntegerValidationGroup.class)
  public ResponseEntity<Void> dtoAttributeParamBigInteger(@Valid UnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/dto-attribute/paramBigDecimal", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(BigDecimalValidationGroup.class)
  public ResponseEntity<Void> dtoAttributeParamBigDecimal(@Valid UnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/dto-attribute/paramOffsetDateTime", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(OffsetDateTimeValidationGroup.class)
  public ResponseEntity<Void> dtoAttributeParamOffsetDateTime(@Valid UnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/dto-attribute/paramLocalDateTime", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(LocalDateTimeValidationGroup.class)
  public ResponseEntity<Void> dtoAttributeParamLocalDateTime(@Valid UnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/dto-attribute/paramLocalDate", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(LocalDateValidationGroup.class)
  public ResponseEntity<Void> dtoAttributeParamLocalDate(@Valid UnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/dto-attribute/paramYear", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(YearValidationGroup.class)
  public ResponseEntity<Void> dtoAttributeParamYear(@Valid UnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/optional-dto-attribute/paramString", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated({StringValidationGroup.class})
  public ResponseEntity<Void> optionalDtoAttributeParamString(@Valid UnamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/optional-dto-attribute/paramInteger", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(IntegerValidationGroup.class)
  public ResponseEntity<Void> optionalDtoAttributeParamInteger(@Valid UnamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/optional-dto-attribute/paramLong", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(LongValidationGroup.class)
  public ResponseEntity<Void> optionalDtoAttributeParamLong(@Valid UnamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/optional-dto-attribute/paramBigInteger", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(BigIntegerValidationGroup.class)
  public ResponseEntity<Void> optionalDtoAttributeParamBigInteger(@Valid UnamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/optional-dto-attribute/paramBigDecimal", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(BigDecimalValidationGroup.class)
  public ResponseEntity<Void> optionalDtoAttributeParamBigDecimal(@Valid UnamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/optional-dto-attribute/paramOffsetDateTime", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(OffsetDateTimeValidationGroup.class)
  public ResponseEntity<Void> optionalDtoAttributeParamOffsetDateTime(@Valid UnamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/optional-dto-attribute/paramLocalDateTime", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(LocalDateTimeValidationGroup.class)
  public ResponseEntity<Void> optionalDtoAttributeParamLocalDateTime(@Valid UnamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/optional-dto-attribute/paramLocalDate", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(LocalDateValidationGroup.class)
  public ResponseEntity<Void> optionalDtoAttributeParamLocalDate(@Valid UnamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/optional-dto-attribute/paramYear", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(YearValidationGroup.class)
  public ResponseEntity<Void> optionalDtoAttributeParamYear(@Valid UnamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-dto-attribute/paramString", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated({StringValidationGroup.class})
  public ResponseEntity<Void> nestedDtoAttributeParamString(@Valid ParentUnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-dto-attribute/paramInteger", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(IntegerValidationGroup.class)
  public ResponseEntity<Void> nestedDtoAttributeParamInteger(@Valid ParentUnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-dto-attribute/paramLong", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(LongValidationGroup.class)
  public ResponseEntity<Void> nestedDtoAttributeParamLong(@Valid ParentUnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-dto-attribute/paramBigInteger", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(BigIntegerValidationGroup.class)
  public ResponseEntity<Void> nestedDtoAttributeParamBigInteger(@Valid ParentUnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-dto-attribute/paramBigDecimal", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(BigDecimalValidationGroup.class)
  public ResponseEntity<Void> nestedDtoAttributeParamBigDecimal(@Valid ParentUnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-dto-attribute/paramOffsetDateTime", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(OffsetDateTimeValidationGroup.class)
  public ResponseEntity<Void> nestedDtoAttributeParamOffsetDateTime(@Valid ParentUnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-dto-attribute/paramLocalDateTime", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(LocalDateTimeValidationGroup.class)
  public ResponseEntity<Void> nestedDtoAttributeParamLocalDateTime(@Valid ParentUnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-dto-attribute/paramLocalDate", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(LocalDateValidationGroup.class)
  public ResponseEntity<Void> nestedDtoAttributeParamLocalDate(@Valid ParentUnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-dto-attribute/paramYear", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(YearValidationGroup.class)
  public ResponseEntity<Void> nestedDtoAttributeParamYear(@Valid ParentUnamedConstrainedDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-optional-dto-attribute/paramString", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated({StringValidationGroup.class})
  public ResponseEntity<Void> nestedOptionalDtoAttributeParamString(@Valid ParentUnamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-optional-dto-attribute/paramInteger", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(IntegerValidationGroup.class)
  public ResponseEntity<Void> nestedOptionalDtoAttributeParamInteger(@Valid ParentUnamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-optional-dto-attribute/paramLong", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(LongValidationGroup.class)
  public ResponseEntity<Void> nestedOptionalDtoAttributeParamLong(@Valid ParentUnamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-optional-dto-attribute/paramBigInteger", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  @Validated(BigIntegerValidationGroup.class)
  public ResponseEntity<Void> nestedOptionalDtoAttributeParamBigInteger(
      @Valid ParentUnamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-optional-dto-attribute/paramBigDecimal", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  @Validated(BigDecimalValidationGroup.class)
  public ResponseEntity<Void> nestedOptionalDtoAttributeParamBigDecimal(
      @Valid ParentUnamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-optional-dto-attribute/paramOffsetDateTime", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  @Validated(OffsetDateTimeValidationGroup.class)
  public ResponseEntity<Void> nestedOptionalDtoAttributeParamOffsetDateTime(
      @Valid ParentUnamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-optional-dto-attribute/paramLocalDateTime", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  @Validated(LocalDateTimeValidationGroup.class)
  public ResponseEntity<Void> nestedOptionalDtoAttributeParamLocalDateTime(
      @Valid ParentUnamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-optional-dto-attribute/paramLocalDate", method = {GET, HEAD, POST, PUT, PATCH,
      DELETE})
  @Validated(LocalDateValidationGroup.class)
  public ResponseEntity<Void> nestedOptionalDtoAttributeParamLocalDate(
      @Valid ParentUnamedConstrainedOptionalDto params) {
    return null;
  }

  @RequestMapping(path = "/nested-optional-dto-attribute/paramYear", method = {GET, HEAD, POST, PUT, PATCH, DELETE})
  @Validated(YearValidationGroup.class)
  public ResponseEntity<Void> nestedOptionalDtoAttributeParamYear(@Valid ParentUnamedConstrainedOptionalDto params) {
    return null;
  }
}
