Feature: Path variable with default-validation for parameter value
  
  The REST service receives data via the path-variable and it does not accomplish with some constraint. The service responds with an error message.
  
   Rule: The conversion between data types is given by the following table
  
  | parameter type | java type      | validation              | validation                                   |
  |----------------|----------------|-------------------------|----------------------------------------------|
  | string         | String         | @Size                   | @Size(max = 1)                               |
  | number         | Integer        | @Min @Max               | @Max(1)                                      |
  | number         | Long           | @Min @Max               | @Max(1)                                      |
  | number         | BigInteger     | @DecimalMin @DecimalMax | @DecimalMax("1")                             |
  | number         | Float          |                         |                                              |
  | number         | Double         |                         |                                              |
  | number         | BigDecimal     | @DecimalMin @DecimalMax | @DecimalMax("9.9")                           |
  | boolean        | Boolean        |                         |                                              |
  | ISO-8601       | OffsetDateTime | @DatetimeRange          | @DatetimeRange(max = "2000-01-01T00:00:00Z") |
  | ISO-8601       | LocalDateTime  | @DatetimeRange          | @DatetimeRange(max = "2000-01-01T00:00:00Z") |
  | ISO-8601       | LocalDate      | @DateRange              | @DateRange(max = "2000-01-01")               |
  | ISO-8601       | LocalTime      |                         |                                              |
  | ISO-8601       | Year           | @YearRange              | @YearRange(max = "2000")                     |
  | string         | ENUM           |                         |                                              |

  Background: 
    Given the default timezone 'America/Puerto_Rico'

  Scenario Outline: The <method> call is made to the /api/v1/path-variable/constrained/method-parameter<path> endpoint (PathVariableConstrainedRestApi.class)
    this endpoint receives the path-variable in a method parameter, both having the same name
    and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/path-variable/constrained/method-parameter'<path>
    And the request with the <method> method
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                                        | message                                                                                    |
      | 'GET'    | '/paramString/ab'                           | 'paramString: size must be between 0 and 1'                                                |
      | 'HEAD'   | '/paramString/ab'                           |                                                                                            |
      | 'POST'   | '/paramString/ab'                           | 'paramString: size must be between 0 and 1'                                                |
      | 'PUT'    | '/paramString/ab'                           | 'paramString: size must be between 0 and 1'                                                |
      | 'PATCH'  | '/paramString/ab'                           | 'paramString: size must be between 0 and 1'                                                |
      | 'DELETE' | '/paramString/ab'                           | 'paramString: size must be between 0 and 1'                                                |
      | 'GET'    | '/paramInteger/10'                          | 'paramInteger: must be less than or equal to 1'                                            |
      | 'HEAD'   | '/paramInteger/10'                          |                                                                                            |
      | 'POST'   | '/paramInteger/10'                          | 'paramInteger: must be less than or equal to 1'                                            |
      | 'PUT'    | '/paramInteger/10'                          | 'paramInteger: must be less than or equal to 1'                                            |
      | 'PATCH'  | '/paramInteger/10'                          | 'paramInteger: must be less than or equal to 1'                                            |
      | 'DELETE' | '/paramInteger/10'                          | 'paramInteger: must be less than or equal to 1'                                            |
      | 'GET'    | '/paramLong/10'                             | 'paramLong: must be less than or equal to 1'                                               |
      | 'HEAD'   | '/paramLong/10'                             |                                                                                            |
      | 'POST'   | '/paramLong/10'                             | 'paramLong: must be less than or equal to 1'                                               |
      | 'PUT'    | '/paramLong/10'                             | 'paramLong: must be less than or equal to 1'                                               |
      | 'PATCH'  | '/paramLong/10'                             | 'paramLong: must be less than or equal to 1'                                               |
      | 'DELETE' | '/paramLong/10'                             | 'paramLong: must be less than or equal to 1'                                               |
      | 'GET'    | '/paramBigInteger/10'                       | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'HEAD'   | '/paramBigInteger/10'                       |                                                                                            |
      | 'POST'   | '/paramBigInteger/10'                       | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'PUT'    | '/paramBigInteger/10'                       | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'PATCH'  | '/paramBigInteger/10'                       | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'DELETE' | '/paramBigInteger/10'                       | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'GET'    | '/paramBigDecimal/10'                       | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'HEAD'   | '/paramBigDecimal/10'                       |                                                                                            |
      | 'POST'   | '/paramBigDecimal/10'                       | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PUT'    | '/paramBigDecimal/10'                       | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PATCH'  | '/paramBigDecimal/10'                       | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'DELETE' | '/paramBigDecimal/10'                       | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'GET'    | '/paramOffsetDateTime/2100-01-01T00:00:00Z' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'HEAD'   | '/paramOffsetDateTime/2100-01-01T00:00:00Z' |                                                                                            |
      | 'POST'   | '/paramOffsetDateTime/2100-01-01T00:00:00Z' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PUT'    | '/paramOffsetDateTime/2100-01-01T00:00:00Z' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PATCH'  | '/paramOffsetDateTime/2100-01-01T00:00:00Z' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'DELETE' | '/paramOffsetDateTime/2100-01-01T00:00:00Z' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'GET'    | '/paramLocalDateTime/2100-01-01T00:00:00Z'  | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'HEAD'   | '/paramLocalDateTime/2100-01-01T00:00:00Z'  |                                                                                            |
      | 'POST'   | '/paramLocalDateTime/2100-01-01T00:00:00Z'  | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PUT'    | '/paramLocalDateTime/2100-01-01T00:00:00Z'  | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PATCH'  | '/paramLocalDateTime/2100-01-01T00:00:00Z'  | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'DELETE' | '/paramLocalDateTime/2100-01-01T00:00:00Z'  | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'GET'    | '/paramLocalDate/2100-01-01'                | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'HEAD'   | '/paramLocalDate/2100-01-01'                |                                                                                            |
      | 'POST'   | '/paramLocalDate/2100-01-01'                | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PUT'    | '/paramLocalDate/2100-01-01'                | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PATCH'  | '/paramLocalDate/2100-01-01'                | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'DELETE' | '/paramLocalDate/2100-01-01'                | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'GET'    | '/paramYear/2100'                           | 'paramYear: must be between -999999999 and 2000'                                           |
      | 'HEAD'   | '/paramYear/2100'                           |                                                                                            |
      | 'POST'   | '/paramYear/2100'                           | 'paramYear: must be between -999999999 and 2000'                                           |
      | 'PUT'    | '/paramYear/2100'                           | 'paramYear: must be between -999999999 and 2000'                                           |
      | 'PATCH'  | '/paramYear/2100'                           | 'paramYear: must be between -999999999 and 2000'                                           |
      | 'DELETE' | '/paramYear/2100'                           | 'paramYear: must be between -999999999 and 2000'                                           |

  Scenario Outline: The <method> call is made to the /api/v1/path-variable/constrained/named-method-parameter<path> endpoint (PathVariableConstrainedRestApi.class)
    this endpoint receives the path-variable in a method parameter, both having the diferent names, a custom path-variable name is defined by the endpoint 
    and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/path-variable/constrained/named-method-parameter'<path>
    And the request with the <method> method
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                                           | message                                                                                    |
      | 'GET'    | '/param-string/ab'                             | 'paramString: size must be between 0 and 1'                                                |
      | 'HEAD'   | '/param-string/ab'                             |                                                                                            |
      | 'POST'   | '/param-string/ab'                             | 'paramString: size must be between 0 and 1'                                                |
      | 'PUT'    | '/param-string/ab'                             | 'paramString: size must be between 0 and 1'                                                |
      | 'PATCH'  | '/param-string/ab'                             | 'paramString: size must be between 0 and 1'                                                |
      | 'DELETE' | '/param-string/ab'                             | 'paramString: size must be between 0 and 1'                                                |
      | 'GET'    | '/param-integer/10'                            | 'paramInteger: must be less than or equal to 1'                                            |
      | 'HEAD'   | '/param-integer/10'                            |                                                                                            |
      | 'POST'   | '/param-integer/10'                            | 'paramInteger: must be less than or equal to 1'                                            |
      | 'PUT'    | '/param-integer/10'                            | 'paramInteger: must be less than or equal to 1'                                            |
      | 'PATCH'  | '/param-integer/10'                            | 'paramInteger: must be less than or equal to 1'                                            |
      | 'DELETE' | '/param-integer/10'                            | 'paramInteger: must be less than or equal to 1'                                            |
      | 'GET'    | '/param-long/10'                               | 'paramLong: must be less than or equal to 1'                                               |
      | 'HEAD'   | '/param-long/10'                               |                                                                                            |
      | 'POST'   | '/param-long/10'                               | 'paramLong: must be less than or equal to 1'                                               |
      | 'PUT'    | '/param-long/10'                               | 'paramLong: must be less than or equal to 1'                                               |
      | 'PATCH'  | '/param-long/10'                               | 'paramLong: must be less than or equal to 1'                                               |
      | 'DELETE' | '/param-long/10'                               | 'paramLong: must be less than or equal to 1'                                               |
      | 'GET'    | '/param-big-integer/10'                        | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'HEAD'   | '/param-big-integer/10'                        |                                                                                            |
      | 'POST'   | '/param-big-integer/10'                        | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'PUT'    | '/param-big-integer/10'                        | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'PATCH'  | '/param-big-integer/10'                        | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'DELETE' | '/param-big-integer/10'                        | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'GET'    | '/param-big-decimal/10'                        | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'HEAD'   | '/param-big-decimal/10'                        |                                                                                            |
      | 'POST'   | '/param-big-decimal/10'                        | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PUT'    | '/param-big-decimal/10'                        | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PATCH'  | '/param-big-decimal/10'                        | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'DELETE' | '/param-big-decimal/10'                        | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'GET'    | '/param-offset-date-time/2100-01-01T00:00:00Z' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'HEAD'   | '/param-offset-date-time/2100-01-01T00:00:00Z' |                                                                                            |
      | 'POST'   | '/param-offset-date-time/2100-01-01T00:00:00Z' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PUT'    | '/param-offset-date-time/2100-01-01T00:00:00Z' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PATCH'  | '/param-offset-date-time/2100-01-01T00:00:00Z' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'DELETE' | '/param-offset-date-time/2100-01-01T00:00:00Z' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'GET'    | '/param-local-date-time/2100-01-01T00:00:00Z'  | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'HEAD'   | '/param-local-date-time/2100-01-01T00:00:00Z'  |                                                                                            |
      | 'POST'   | '/param-local-date-time/2100-01-01T00:00:00Z'  | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PUT'    | '/param-local-date-time/2100-01-01T00:00:00Z'  | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PATCH'  | '/param-local-date-time/2100-01-01T00:00:00Z'  | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'DELETE' | '/param-local-date-time/2100-01-01T00:00:00Z'  | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'GET'    | '/param-local-date/2100-01-01'                 | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'HEAD'   | '/param-local-date/2100-01-01'                 |                                                                                            |
      | 'POST'   | '/param-local-date/2100-01-01'                 | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PUT'    | '/param-local-date/2100-01-01'                 | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PATCH'  | '/param-local-date/2100-01-01'                 | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'DELETE' | '/param-local-date/2100-01-01'                 | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'GET'    | '/param-year/2100'                             | 'paramYear: must be between -999999999 and 2000'                                           |
      | 'HEAD'   | '/param-year/2100'                             |                                                                                            |
      | 'POST'   | '/param-year/2100'                             | 'paramYear: must be between -999999999 and 2000'                                           |
      | 'PUT'    | '/param-year/2100'                             | 'paramYear: must be between -999999999 and 2000'                                           |
      | 'PATCH'  | '/param-year/2100'                             | 'paramYear: must be between -999999999 and 2000'                                           |
      | 'DELETE' | '/param-year/2100'                             | 'paramYear: must be between -999999999 and 2000'                                           |
