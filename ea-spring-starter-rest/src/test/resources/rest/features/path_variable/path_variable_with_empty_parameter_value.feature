Feature: Path variable with empty parameter value
  
  The REST service receives data via the path-variable and it does not accomplish with some constraint. The service responds with an error message.
  
   Rule: The conversion between data types is given by the following table
  
  | parameter type | java type      |
  |----------------|----------------|
  | string         | String         |
  | number         | Integer        |
  | number         | Long           |
  | number         | BigInteger     |
  | number         | Float          |
  | number         | Double         |
  | number         | BigDecimal     |
  | boolean        | Boolean        |
  | ISO-8601       | OffsetDateTime |
  | ISO-8601       | LocalDateTime  |
  | ISO-8601       | LocalDate      |
  | ISO-8601       | LocalTime      |
  | ISO-8601       | Year           |
  | string         | ENUM           |

  Background: 
    Given the default timezone 'America/Puerto_Rico'

  Scenario Outline: The <method> call is made to the /api/v1/path-variable/method-parameter<path> endpoint (RequestParamConstrainedRestApi.class)
    this endpoint receives the path-variable in a method parameter, both having the same name
    and the service correctly maps the sent empty value with the declared data type, responding with a success message.

    Given the endpoint '/api/v1/path-variable/method-parameter'<path>
    And the request with the <method> method
    When I execute the request
    Then I get the status code 404
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value "Not Found"

    Examples: 
      | method   | path                    |
      | 'GET'    | '/paramString/'         |
      | 'HEAD'   | '/paramString/'         |
      | 'POST'   | '/paramString/'         |
      | 'PUT'    | '/paramString/'         |
      | 'PATCH'  | '/paramString/'         |
      | 'DELETE' | '/paramString/'         |
      | 'GET'    | '/paramInteger/'        |
      | 'HEAD'   | '/paramInteger/'        |
      | 'POST'   | '/paramInteger/'        |
      | 'PUT'    | '/paramInteger/'        |
      | 'PATCH'  | '/paramInteger/'        |
      | 'DELETE' | '/paramInteger/'        |
      | 'GET'    | '/paramLong/'           |
      | 'HEAD'   | '/paramLong/'           |
      | 'POST'   | '/paramLong/'           |
      | 'PUT'    | '/paramLong/'           |
      | 'PATCH'  | '/paramLong/'           |
      | 'DELETE' | '/paramLong/'           |
      | 'GET'    | '/paramBigInteger/'     |
      | 'HEAD'   | '/paramBigInteger/'     |
      | 'POST'   | '/paramBigInteger/'     |
      | 'PUT'    | '/paramBigInteger/'     |
      | 'PATCH'  | '/paramBigInteger/'     |
      | 'DELETE' | '/paramBigInteger/'     |
      | 'GET'    | '/paramFloat/'          |
      | 'HEAD'   | '/paramFloat/'          |
      | 'POST'   | '/paramFloat/'          |
      | 'PUT'    | '/paramFloat/'          |
      | 'PATCH'  | '/paramFloat/'          |
      | 'DELETE' | '/paramFloat/'          |
      | 'GET'    | '/paramDouble/'         |
      | 'HEAD'   | '/paramDouble/'         |
      | 'POST'   | '/paramDouble/'         |
      | 'PUT'    | '/paramDouble/'         |
      | 'PATCH'  | '/paramDouble/'         |
      | 'DELETE' | '/paramDouble/'         |
      | 'GET'    | '/paramBigDecimal/'     |
      | 'HEAD'   | '/paramBigDecimal/'     |
      | 'POST'   | '/paramBigDecimal/'     |
      | 'PUT'    | '/paramBigDecimal/'     |
      | 'PATCH'  | '/paramBigDecimal/'     |
      | 'DELETE' | '/paramBigDecimal/'     |
      | 'GET'    | '/paramOffsetDateTime/' |
      | 'HEAD'   | '/paramOffsetDateTime/' |
      | 'POST'   | '/paramOffsetDateTime/' |
      | 'PUT'    | '/paramOffsetDateTime/' |
      | 'PATCH'  | '/paramOffsetDateTime/' |
      | 'DELETE' | '/paramOffsetDateTime/' |
      | 'GET'    | '/paramLocalDateTime/'  |
      | 'HEAD'   | '/paramLocalDateTime/'  |
      | 'POST'   | '/paramLocalDateTime/'  |
      | 'PUT'    | '/paramLocalDateTime/'  |
      | 'PATCH'  | '/paramLocalDateTime/'  |
      | 'DELETE' | '/paramLocalDateTime/'  |
      | 'GET'    | '/paramLocalDate/'      |
      | 'HEAD'   | '/paramLocalDate/'      |
      | 'POST'   | '/paramLocalDate/'      |
      | 'PUT'    | '/paramLocalDate/'      |
      | 'PATCH'  | '/paramLocalDate/'      |
      | 'DELETE' | '/paramLocalDate/'      |
      | 'GET'    | '/paramLocalTime/'      |
      | 'HEAD'   | '/paramLocalTime/'      |
      | 'POST'   | '/paramLocalTime/'      |
      | 'PUT'    | '/paramLocalTime/'      |
      | 'PATCH'  | '/paramLocalTime/'      |
      | 'DELETE' | '/paramLocalTime/'      |
      | 'GET'    | '/paramYear/'           |
      | 'HEAD'   | '/paramYear/'           |
      | 'POST'   | '/paramYear/'           |
      | 'PUT'    | '/paramYear/'           |
      | 'PATCH'  | '/paramYear/'           |
      | 'DELETE' | '/paramYear/'           |
      | 'GET'    | '/paramEnumAB/'         |
      | 'HEAD'   | '/paramEnumAB/'         |
      | 'POST'   | '/paramEnumAB/'         |
      | 'PUT'    | '/paramEnumAB/'         |
      | 'PATCH'  | '/paramEnumAB/'         |
      | 'DELETE' | '/paramEnumAB/'         |
      | 'GET'    | '/paramEnumLowerAB/'    |
      | 'HEAD'   | '/paramEnumLowerAB/'    |
      | 'POST'   | '/paramEnumLowerAB/'    |
      | 'PUT'    | '/paramEnumLowerAB/'    |
      | 'PATCH'  | '/paramEnumLowerAB/'    |
      | 'DELETE' | '/paramEnumLowerAB/'    |

  Scenario Outline: The <method> call is made to the /api/v1/path-variable/named-method-parameter<path> endpoint (RequestParamConstrainedRestApi.class)
    this endpoint receives the path-variable in a method parameter, both having the diferent names, a custom path-variable name is defined by the endpoint 
    and the service correctly maps the sent empty value with the declared data type, responding with a success message.

    Given the endpoint '/api/v1/path-variable/named-method-parameter'<path>
    And the request with the <method> method
    When I execute the request
    Then I get the status code 404
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value "Not Found"

    Examples: 
      | method   | path                       |
      | 'GET'    | '/param-string/'           |
      | 'HEAD'   | '/param-string/'           |
      | 'POST'   | '/param-string/'           |
      | 'PUT'    | '/param-string/'           |
      | 'PATCH'  | '/param-string/'           |
      | 'DELETE' | '/param-string/'           |
      | 'GET'    | '/param-integer/'          |
      | 'HEAD'   | '/param-integer/'          |
      | 'POST'   | '/param-integer/'          |
      | 'PUT'    | '/param-integer/'          |
      | 'PATCH'  | '/param-integer/'          |
      | 'DELETE' | '/param-integer/'          |
      | 'GET'    | '/param-long/'             |
      | 'HEAD'   | '/param-long/'             |
      | 'POST'   | '/param-long/'             |
      | 'PUT'    | '/param-long/'             |
      | 'PATCH'  | '/param-long/'             |
      | 'DELETE' | '/param-long/'             |
      | 'GET'    | '/param-big-integer/'      |
      | 'HEAD'   | '/param-big-integer/'      |
      | 'POST'   | '/param-big-integer/'      |
      | 'PUT'    | '/param-big-integer/'      |
      | 'PATCH'  | '/param-big-integer/'      |
      | 'DELETE' | '/param-big-integer/'      |
      | 'GET'    | '/param-float/'            |
      | 'HEAD'   | '/param-float/'            |
      | 'POST'   | '/param-float/'            |
      | 'PUT'    | '/param-float/'            |
      | 'PATCH'  | '/param-float/'            |
      | 'DELETE' | '/param-float/'            |
      | 'GET'    | '/param-double/'           |
      | 'HEAD'   | '/param-double/'           |
      | 'POST'   | '/param-double/'           |
      | 'PUT'    | '/param-double/'           |
      | 'PATCH'  | '/param-double/'           |
      | 'DELETE' | '/param-double/'           |
      | 'GET'    | '/param-big-decimal/'      |
      | 'HEAD'   | '/param-big-decimal/'      |
      | 'POST'   | '/param-big-decimal/'      |
      | 'PUT'    | '/param-big-decimal/'      |
      | 'PATCH'  | '/param-big-decimal/'      |
      | 'DELETE' | '/param-big-decimal/'      |
      | 'GET'    | '/param-offset-date-time/' |
      | 'HEAD'   | '/param-offset-date-time/' |
      | 'POST'   | '/param-offset-date-time/' |
      | 'PUT'    | '/param-offset-date-time/' |
      | 'PATCH'  | '/param-offset-date-time/' |
      | 'DELETE' | '/param-offset-date-time/' |
      | 'GET'    | '/param-local-date-time/'  |
      | 'HEAD'   | '/param-local-date-time/'  |
      | 'POST'   | '/param-local-date-time/'  |
      | 'PUT'    | '/param-local-date-time/'  |
      | 'PATCH'  | '/param-local-date-time/'  |
      | 'DELETE' | '/param-local-date-time/'  |
      | 'GET'    | '/param-local-date/'       |
      | 'HEAD'   | '/param-local-date/'       |
      | 'POST'   | '/param-local-date/'       |
      | 'PUT'    | '/param-local-date/'       |
      | 'PATCH'  | '/param-local-date/'       |
      | 'DELETE' | '/param-local-date/'       |
      | 'GET'    | '/param-local-time/'       |
      | 'HEAD'   | '/param-local-time/'       |
      | 'POST'   | '/param-local-time/'       |
      | 'PUT'    | '/param-local-time/'       |
      | 'PATCH'  | '/param-local-time/'       |
      | 'DELETE' | '/param-local-time/'       |
      | 'GET'    | '/param-year/'             |
      | 'HEAD'   | '/param-year/'             |
      | 'POST'   | '/param-year/'             |
      | 'PUT'    | '/param-year/'             |
      | 'PATCH'  | '/param-year/'             |
      | 'DELETE' | '/param-year/'             |
      | 'GET'    | '/param-enum-ab/'          |
      | 'HEAD'   | '/param-enum-ab/'          |
      | 'POST'   | '/param-enum-ab/'          |
      | 'PUT'    | '/param-enum-ab/'          |
      | 'PATCH'  | '/param-enum-ab/'          |
      | 'DELETE' | '/param-enum-ab/'          |
      | 'GET'    | '/param-enum-lower-ab/'    |
      | 'HEAD'   | '/param-enum-lower-ab/'    |
      | 'POST'   | '/param-enum-lower-ab/'    |
      | 'PUT'    | '/param-enum-lower-ab/'    |
      | 'PATCH'  | '/param-enum-lower-ab/'    |
      | 'DELETE' | '/param-enum-lower-ab/'    |
