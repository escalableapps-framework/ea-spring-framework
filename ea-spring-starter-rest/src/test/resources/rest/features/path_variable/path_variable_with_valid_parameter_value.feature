Feature: Path variable with valid parameter value
  
  The REST service receives data via the path-variable and it does not accomplish with some constraint. The service responds with an error message.
  
   Rule: The conversion between data types is given by the following table
  
  | parameter type | java type      |
  |----------------|----------------|
  | string         | String         |
  | number         | Integer        |
  | number         | Long           |
  | number         | BigInteger     |
  | number         | Float          |
  | number         | Double         |
  | number         | BigDecimal     |
  | boolean        | Boolean        |
  | ISO-8601       | OffsetDateTime |
  | ISO-8601       | LocalDateTime  |
  | ISO-8601       | LocalDate      |
  | ISO-8601       | LocalTime      |
  | ISO-8601       | Year           |
  | string         | ENUM           |

  Background: 
    Given the default timezone 'America/Puerto_Rico'

  Scenario Outline: The <method> call is made to the /api/v1/path-variable/method-parameter<path> endpoint (PathVariableUnconstrainedRestApi.class)
    this endpoint receives the path-variable in a method parameter, both having the same name
    and the service correctly maps the sent value with the declared data type, responding with a success message.

    Given the endpoint '/api/v1/path-variable/method-parameter'<path>
    And the request with the <method> method
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | path                                        | received value                                   |
      | 'GET'    | '/paramString/id'                           | '{"paramString":"id"}'                           |
      | 'HEAD'   | '/paramString/id'                           | '{"paramString":"id"}'                           |
      | 'POST'   | '/paramString/id'                           | '{"paramString":"id"}'                           |
      | 'PUT'    | '/paramString/id'                           | '{"paramString":"id"}'                           |
      | 'PATCH'  | '/paramString/id'                           | '{"paramString":"id"}'                           |
      | 'DELETE' | '/paramString/id'                           | '{"paramString":"id"}'                           |
      | 'GET'    | '/paramInteger/2'                           | '{"paramInteger":2}'                             |
      | 'HEAD'   | '/paramInteger/2'                           | '{"paramInteger":2}'                             |
      | 'POST'   | '/paramInteger/2'                           | '{"paramInteger":2}'                             |
      | 'PUT'    | '/paramInteger/2'                           | '{"paramInteger":2}'                             |
      | 'PATCH'  | '/paramInteger/2'                           | '{"paramInteger":2}'                             |
      | 'DELETE' | '/paramInteger/2'                           | '{"paramInteger":2}'                             |
      | 'GET'    | '/paramLong/2'                              | '{"paramLong":2}'                                |
      | 'HEAD'   | '/paramLong/2'                              | '{"paramLong":2}'                                |
      | 'POST'   | '/paramLong/2'                              | '{"paramLong":2}'                                |
      | 'PUT'    | '/paramLong/2'                              | '{"paramLong":2}'                                |
      | 'PATCH'  | '/paramLong/2'                              | '{"paramLong":2}'                                |
      | 'DELETE' | '/paramLong/2'                              | '{"paramLong":2}'                                |
      | 'GET'    | '/paramBigInteger/2'                        | '{"paramBigInteger":2}'                          |
      | 'HEAD'   | '/paramBigInteger/2'                        | '{"paramBigInteger":2}'                          |
      | 'POST'   | '/paramBigInteger/2'                        | '{"paramBigInteger":2}'                          |
      | 'PUT'    | '/paramBigInteger/2'                        | '{"paramBigInteger":2}'                          |
      | 'PATCH'  | '/paramBigInteger/2'                        | '{"paramBigInteger":2}'                          |
      | 'DELETE' | '/paramBigInteger/2'                        | '{"paramBigInteger":2}'                          |
      | 'GET'    | '/paramFloat/2.1'                           | '{"paramFloat":2.1}'                             |
      | 'HEAD'   | '/paramFloat/2.1'                           | '{"paramFloat":2.1}'                             |
      | 'POST'   | '/paramFloat/2.1'                           | '{"paramFloat":2.1}'                             |
      | 'PUT'    | '/paramFloat/2.1'                           | '{"paramFloat":2.1}'                             |
      | 'PATCH'  | '/paramFloat/2.1'                           | '{"paramFloat":2.1}'                             |
      | 'DELETE' | '/paramFloat/2.1'                           | '{"paramFloat":2.1}'                             |
      | 'GET'    | '/paramDouble/2.1'                          | '{"paramDouble":2.1}'                            |
      | 'HEAD'   | '/paramDouble/2.1'                          | '{"paramDouble":2.1}'                            |
      | 'POST'   | '/paramDouble/2.1'                          | '{"paramDouble":2.1}'                            |
      | 'PUT'    | '/paramDouble/2.1'                          | '{"paramDouble":2.1}'                            |
      | 'PATCH'  | '/paramDouble/2.1'                          | '{"paramDouble":2.1}'                            |
      | 'DELETE' | '/paramDouble/2.1'                          | '{"paramDouble":2.1}'                            |
      | 'GET'    | '/paramBigDecimal/2.1'                      | '{"paramBigDecimal":2.1}'                        |
      | 'HEAD'   | '/paramBigDecimal/2.1'                      | '{"paramBigDecimal":2.1}'                        |
      | 'POST'   | '/paramBigDecimal/2.1'                      | '{"paramBigDecimal":2.1}'                        |
      | 'PUT'    | '/paramBigDecimal/2.1'                      | '{"paramBigDecimal":2.1}'                        |
      | 'PATCH'  | '/paramBigDecimal/2.1'                      | '{"paramBigDecimal":2.1}'                        |
      | 'DELETE' | '/paramBigDecimal/2.1'                      | '{"paramBigDecimal":2.1}'                        |
      | 'GET'    | '/paramOffsetDateTime/2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'HEAD'   | '/paramOffsetDateTime/2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'POST'   | '/paramOffsetDateTime/2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'PUT'    | '/paramOffsetDateTime/2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'PATCH'  | '/paramOffsetDateTime/2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'DELETE' | '/paramOffsetDateTime/2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'GET'    | '/paramLocalDateTime/2000-01-01T00:00:00Z'  | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'HEAD'   | '/paramLocalDateTime/2000-01-01T00:00:00Z'  | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'POST'   | '/paramLocalDateTime/2000-01-01T00:00:00Z'  | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'PUT'    | '/paramLocalDateTime/2000-01-01T00:00:00Z'  | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'PATCH'  | '/paramLocalDateTime/2000-01-01T00:00:00Z'  | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'DELETE' | '/paramLocalDateTime/2000-01-01T00:00:00Z'  | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'GET'    | '/paramLocalDate/2000-01-01'                | '{"paramLocalDate":"2000-01-01"}'                |
      | 'HEAD'   | '/paramLocalDate/2000-01-01'                | '{"paramLocalDate":"2000-01-01"}'                |
      | 'POST'   | '/paramLocalDate/2000-01-01'                | '{"paramLocalDate":"2000-01-01"}'                |
      | 'PUT'    | '/paramLocalDate/2000-01-01'                | '{"paramLocalDate":"2000-01-01"}'                |
      | 'PATCH'  | '/paramLocalDate/2000-01-01'                | '{"paramLocalDate":"2000-01-01"}'                |
      | 'DELETE' | '/paramLocalDate/2000-01-01'                | '{"paramLocalDate":"2000-01-01"}'                |
      | 'GET'    | '/paramLocalTime/13:00:00'                  | '{"paramLocalTime":"13:00:00"}'                  |
      | 'HEAD'   | '/paramLocalTime/13:00:00'                  | '{"paramLocalTime":"13:00:00"}'                  |
      | 'POST'   | '/paramLocalTime/13:00:00'                  | '{"paramLocalTime":"13:00:00"}'                  |
      | 'PUT'    | '/paramLocalTime/13:00:00'                  | '{"paramLocalTime":"13:00:00"}'                  |
      | 'PATCH'  | '/paramLocalTime/13:00:00'                  | '{"paramLocalTime":"13:00:00"}'                  |
      | 'DELETE' | '/paramLocalTime/13:00:00'                  | '{"paramLocalTime":"13:00:00"}'                  |
      | 'GET'    | '/paramYear/2000'                           | '{"paramYear":"2000"}'                           |
      | 'HEAD'   | '/paramYear/2000'                           | '{"paramYear":"2000"}'                           |
      | 'POST'   | '/paramYear/2000'                           | '{"paramYear":"2000"}'                           |
      | 'PUT'    | '/paramYear/2000'                           | '{"paramYear":"2000"}'                           |
      | 'PATCH'  | '/paramYear/2000'                           | '{"paramYear":"2000"}'                           |
      | 'DELETE' | '/paramYear/2000'                           | '{"paramYear":"2000"}'                           |
      | 'GET'    | '/paramEnumAB/A'                            | '{"paramEnumAB":"A"}'                            |
      | 'HEAD'   | '/paramEnumAB/A'                            | '{"paramEnumAB":"A"}'                            |
      | 'POST'   | '/paramEnumAB/A'                            | '{"paramEnumAB":"A"}'                            |
      | 'PUT'    | '/paramEnumAB/A'                            | '{"paramEnumAB":"A"}'                            |
      | 'PATCH'  | '/paramEnumAB/A'                            | '{"paramEnumAB":"A"}'                            |
      | 'DELETE' | '/paramEnumAB/A'                            | '{"paramEnumAB":"A"}'                            |
      | 'GET'    | '/paramEnumLowerAB/b'                       | '{"paramEnumLowerAB":"b"}'                       |
      | 'HEAD'   | '/paramEnumLowerAB/b'                       | '{"paramEnumLowerAB":"b"}'                       |
      | 'POST'   | '/paramEnumLowerAB/b'                       | '{"paramEnumLowerAB":"b"}'                       |
      | 'PUT'    | '/paramEnumLowerAB/b'                       | '{"paramEnumLowerAB":"b"}'                       |
      | 'PATCH'  | '/paramEnumLowerAB/b'                       | '{"paramEnumLowerAB":"b"}'                       |
      | 'DELETE' | '/paramEnumLowerAB/b'                       | '{"paramEnumLowerAB":"b"}'                       |

  Scenario Outline: The <method> call is made to the /api/v1/path-variable/named-method-parameter<path> endpoint (PathVariableUnconstrainedRestApi.class)
    this endpoint receives the path-variable in a method parameter, both having the diferent names, a custom path-variable name is defined by the endpoint 
    and the service correctly maps the sent value with the declared data type, responding with a success message.

    Given the endpoint '/api/v1/path-variable/named-method-parameter'<path>
    And the request with the <method> method
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | path                                           | received value                                   |
      | 'GET'    | '/param-string/id'                             | '{"paramString":"id"}'                           |
      | 'HEAD'   | '/param-string/id'                             | '{"paramString":"id"}'                           |
      | 'POST'   | '/param-string/id'                             | '{"paramString":"id"}'                           |
      | 'PUT'    | '/param-string/id'                             | '{"paramString":"id"}'                           |
      | 'PATCH'  | '/param-string/id'                             | '{"paramString":"id"}'                           |
      | 'DELETE' | '/param-string/id'                             | '{"paramString":"id"}'                           |
      | 'GET'    | '/param-integer/2'                             | '{"paramInteger":2}'                             |
      | 'HEAD'   | '/param-integer/2'                             | '{"paramInteger":2}'                             |
      | 'POST'   | '/param-integer/2'                             | '{"paramInteger":2}'                             |
      | 'PUT'    | '/param-integer/2'                             | '{"paramInteger":2}'                             |
      | 'PATCH'  | '/param-integer/2'                             | '{"paramInteger":2}'                             |
      | 'DELETE' | '/param-integer/2'                             | '{"paramInteger":2}'                             |
      | 'GET'    | '/param-long/2'                                | '{"paramLong":2}'                                |
      | 'HEAD'   | '/param-long/2'                                | '{"paramLong":2}'                                |
      | 'POST'   | '/param-long/2'                                | '{"paramLong":2}'                                |
      | 'PUT'    | '/param-long/2'                                | '{"paramLong":2}'                                |
      | 'PATCH'  | '/param-long/2'                                | '{"paramLong":2}'                                |
      | 'DELETE' | '/param-long/2'                                | '{"paramLong":2}'                                |
      | 'GET'    | '/param-big-integer/2'                         | '{"paramBigInteger":2}'                          |
      | 'HEAD'   | '/param-big-integer/2'                         | '{"paramBigInteger":2}'                          |
      | 'POST'   | '/param-big-integer/2'                         | '{"paramBigInteger":2}'                          |
      | 'PUT'    | '/param-big-integer/2'                         | '{"paramBigInteger":2}'                          |
      | 'PATCH'  | '/param-big-integer/2'                         | '{"paramBigInteger":2}'                          |
      | 'DELETE' | '/param-big-integer/2'                         | '{"paramBigInteger":2}'                          |
      | 'GET'    | '/param-float/2.1'                             | '{"paramFloat":2.1}'                             |
      | 'HEAD'   | '/param-float/2.1'                             | '{"paramFloat":2.1}'                             |
      | 'POST'   | '/param-float/2.1'                             | '{"paramFloat":2.1}'                             |
      | 'PUT'    | '/param-float/2.1'                             | '{"paramFloat":2.1}'                             |
      | 'PATCH'  | '/param-float/2.1'                             | '{"paramFloat":2.1}'                             |
      | 'DELETE' | '/param-float/2.1'                             | '{"paramFloat":2.1}'                             |
      | 'GET'    | '/param-double/2.1'                            | '{"paramDouble":2.1}'                            |
      | 'HEAD'   | '/param-double/2.1'                            | '{"paramDouble":2.1}'                            |
      | 'POST'   | '/param-double/2.1'                            | '{"paramDouble":2.1}'                            |
      | 'PUT'    | '/param-double/2.1'                            | '{"paramDouble":2.1}'                            |
      | 'PATCH'  | '/param-double/2.1'                            | '{"paramDouble":2.1}'                            |
      | 'DELETE' | '/param-double/2.1'                            | '{"paramDouble":2.1}'                            |
      | 'GET'    | '/param-big-decimal/2.1'                       | '{"paramBigDecimal":2.1}'                        |
      | 'HEAD'   | '/param-big-decimal/2.1'                       | '{"paramBigDecimal":2.1}'                        |
      | 'POST'   | '/param-big-decimal/2.1'                       | '{"paramBigDecimal":2.1}'                        |
      | 'PUT'    | '/param-big-decimal/2.1'                       | '{"paramBigDecimal":2.1}'                        |
      | 'PATCH'  | '/param-big-decimal/2.1'                       | '{"paramBigDecimal":2.1}'                        |
      | 'DELETE' | '/param-big-decimal/2.1'                       | '{"paramBigDecimal":2.1}'                        |
      | 'GET'    | '/param-offset-date-time/2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'HEAD'   | '/param-offset-date-time/2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'POST'   | '/param-offset-date-time/2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'PUT'    | '/param-offset-date-time/2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'PATCH'  | '/param-offset-date-time/2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'DELETE' | '/param-offset-date-time/2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'GET'    | '/param-local-date-time/2000-01-01T00:00:00Z'  | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'HEAD'   | '/param-local-date-time/2000-01-01T00:00:00Z'  | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'POST'   | '/param-local-date-time/2000-01-01T00:00:00Z'  | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'PUT'    | '/param-local-date-time/2000-01-01T00:00:00Z'  | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'PATCH'  | '/param-local-date-time/2000-01-01T00:00:00Z'  | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'DELETE' | '/param-local-date-time/2000-01-01T00:00:00Z'  | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'GET'    | '/param-local-date/2000-01-01'                 | '{"paramLocalDate":"2000-01-01"}'                |
      | 'HEAD'   | '/param-local-date/2000-01-01'                 | '{"paramLocalDate":"2000-01-01"}'                |
      | 'POST'   | '/param-local-date/2000-01-01'                 | '{"paramLocalDate":"2000-01-01"}'                |
      | 'PUT'    | '/param-local-date/2000-01-01'                 | '{"paramLocalDate":"2000-01-01"}'                |
      | 'PATCH'  | '/param-local-date/2000-01-01'                 | '{"paramLocalDate":"2000-01-01"}'                |
      | 'DELETE' | '/param-local-date/2000-01-01'                 | '{"paramLocalDate":"2000-01-01"}'                |
      | 'GET'    | '/param-local-time/13:00:00'                   | '{"paramLocalTime":"13:00:00"}'                  |
      | 'HEAD'   | '/param-local-time/13:00:00'                   | '{"paramLocalTime":"13:00:00"}'                  |
      | 'POST'   | '/param-local-time/13:00:00'                   | '{"paramLocalTime":"13:00:00"}'                  |
      | 'PUT'    | '/param-local-time/13:00:00'                   | '{"paramLocalTime":"13:00:00"}'                  |
      | 'PATCH'  | '/param-local-time/13:00:00'                   | '{"paramLocalTime":"13:00:00"}'                  |
      | 'DELETE' | '/param-local-time/13:00:00'                   | '{"paramLocalTime":"13:00:00"}'                  |
      | 'GET'    | '/param-year/2000'                             | '{"paramYear":"2000"}'                           |
      | 'HEAD'   | '/param-year/2000'                             | '{"paramYear":"2000"}'                           |
      | 'POST'   | '/param-year/2000'                             | '{"paramYear":"2000"}'                           |
      | 'PUT'    | '/param-year/2000'                             | '{"paramYear":"2000"}'                           |
      | 'PATCH'  | '/param-year/2000'                             | '{"paramYear":"2000"}'                           |
      | 'DELETE' | '/param-year/2000'                             | '{"paramYear":"2000"}'                           |
      | 'GET'    | '/param-enum-ab/A'                             | '{"paramEnumAB":"A"}'                            |
      | 'HEAD'   | '/param-enum-ab/A'                             | '{"paramEnumAB":"A"}'                            |
      | 'POST'   | '/param-enum-ab/A'                             | '{"paramEnumAB":"A"}'                            |
      | 'PUT'    | '/param-enum-ab/A'                             | '{"paramEnumAB":"A"}'                            |
      | 'PATCH'  | '/param-enum-ab/A'                             | '{"paramEnumAB":"A"}'                            |
      | 'DELETE' | '/param-enum-ab/A'                             | '{"paramEnumAB":"A"}'                            |
      | 'GET'    | '/param-enum-lower-ab/b'                       | '{"paramEnumLowerAB":"b"}'                       |
      | 'HEAD'   | '/param-enum-lower-ab/b'                       | '{"paramEnumLowerAB":"b"}'                       |
      | 'POST'   | '/param-enum-lower-ab/b'                       | '{"paramEnumLowerAB":"b"}'                       |
      | 'PUT'    | '/param-enum-lower-ab/b'                       | '{"paramEnumLowerAB":"b"}'                       |
      | 'PATCH'  | '/param-enum-lower-ab/b'                       | '{"paramEnumLowerAB":"b"}'                       |
      | 'DELETE' | '/param-enum-lower-ab/b'                       | '{"paramEnumLowerAB":"b"}'                       |
