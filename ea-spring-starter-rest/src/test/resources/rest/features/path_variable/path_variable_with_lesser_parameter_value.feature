Feature: Path variable with lesser for parameter value
  
  The REST service receives data via the path-variable and cannot interpret the data because it is less than the minimum value allowed. The service responds with an error message.
  
   Rule: The conversion between data types is given by the following table
  
  | parameter type | java type      |
  |----------------|----------------|
  | string         | String         |
  | number         | Integer        |
  | number         | Long           |
  | number         | BigInteger     |
  | number         | Float          |
  | number         | Double         |
  | number         | BigDecimal     |
  | boolean        | Boolean        |
  | ISO-8601       | OffsetDateTime |
  | ISO-8601       | LocalDateTime  |
  | ISO-8601       | LocalDate      |
  | ISO-8601       | LocalTime      |
  | ISO-8601       | Year           |
  | string         | ENUM           |

  Background: 
    Given the default timezone 'America/Puerto_Rico'

  Scenario Outline: The <method> call is made to the /api/v1/path-variable/method-parameter<path> endpoint (PathVariableUnconstrainedRestApi.class)
    this endpoint receives the path-variable in a method parameter, both having the same name
    and the service cannot map the sent value to the declared data type, responding with an error message.

    Given the endpoint '/api/v1/path-variable/method-parameter'<path>
    And the request with the <method> method
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                                                   | message                                    |
      | 'GET'    | '/paramInteger/-2147483649'                            | 'paramInteger: is not a valid type'        |
      | 'HEAD'   | '/paramInteger/-2147483649'                            |                                            |
      | 'POST'   | '/paramInteger/-2147483649'                            | 'paramInteger: is not a valid type'        |
      | 'PUT'    | '/paramInteger/-2147483649'                            | 'paramInteger: is not a valid type'        |
      | 'PATCH'  | '/paramInteger/-2147483649'                            | 'paramInteger: is not a valid type'        |
      | 'DELETE' | '/paramInteger/-2147483649'                            | 'paramInteger: is not a valid type'        |
      | 'GET'    | '/paramLong/-9223372036854775809'                      | 'paramLong: is not a valid type'           |
      | 'HEAD'   | '/paramLong/-9223372036854775809'                      |                                            |
      | 'POST'   | '/paramLong/-9223372036854775809'                      | 'paramLong: is not a valid type'           |
      | 'PUT'    | '/paramLong/-9223372036854775809'                      | 'paramLong: is not a valid type'           |
      | 'PATCH'  | '/paramLong/-9223372036854775809'                      | 'paramLong: is not a valid type'           |
      | 'DELETE' | '/paramLong/-9223372036854775809'                      | 'paramLong: is not a valid type'           |
      | 'GET'    | '/paramOffsetDateTime/-1000000000-12-31T23:59:59.999Z' | 'paramOffsetDateTime: is not a valid type' |
      | 'HEAD'   | '/paramOffsetDateTime/-1000000000-12-31T23:59:59.999Z' |                                            |
      | 'POST'   | '/paramOffsetDateTime/-1000000000-12-31T23:59:59.999Z' | 'paramOffsetDateTime: is not a valid type' |
      | 'PUT'    | '/paramOffsetDateTime/-1000000000-12-31T23:59:59.999Z' | 'paramOffsetDateTime: is not a valid type' |
      | 'PATCH'  | '/paramOffsetDateTime/-1000000000-12-31T23:59:59.999Z' | 'paramOffsetDateTime: is not a valid type' |
      | 'DELETE' | '/paramOffsetDateTime/-1000000000-12-31T23:59:59.999Z' | 'paramOffsetDateTime: is not a valid type' |
      | 'GET'    | '/paramLocalDateTime/-1000000000-12-31T23:59:59.999Z'  | 'paramLocalDateTime: is not a valid type'  |
      | 'HEAD'   | '/paramLocalDateTime/-1000000000-12-31T23:59:59.999Z'  |                                            |
      | 'POST'   | '/paramLocalDateTime/-1000000000-12-31T23:59:59.999Z'  | 'paramLocalDateTime: is not a valid type'  |
      | 'PUT'    | '/paramLocalDateTime/-1000000000-12-31T23:59:59.999Z'  | 'paramLocalDateTime: is not a valid type'  |
      | 'PATCH'  | '/paramLocalDateTime/-1000000000-12-31T23:59:59.999Z'  | 'paramLocalDateTime: is not a valid type'  |
      | 'DELETE' | '/paramLocalDateTime/-1000000000-12-31T23:59:59.999Z'  | 'paramLocalDateTime: is not a valid type'  |
      | 'GET'    | '/paramLocalDate/-1000000000-12-31'                    | 'paramLocalDate: is not a valid type'      |
      | 'HEAD'   | '/paramLocalDate/-1000000000-12-31'                    |                                            |
      | 'POST'   | '/paramLocalDate/-1000000000-12-31'                    | 'paramLocalDate: is not a valid type'      |
      | 'PUT'    | '/paramLocalDate/-1000000000-12-31'                    | 'paramLocalDate: is not a valid type'      |
      | 'PATCH'  | '/paramLocalDate/-1000000000-12-31'                    | 'paramLocalDate: is not a valid type'      |
      | 'DELETE' | '/paramLocalDate/-1000000000-12-31'                    | 'paramLocalDate: is not a valid type'      |
      | 'GET'    | '/paramYear/-1000000000'                               | 'paramYear: is not a valid type'           |
      | 'HEAD'   | '/paramYear/-1000000000'                               |                                            |
      | 'POST'   | '/paramYear/-1000000000'                               | 'paramYear: is not a valid type'           |
      | 'PUT'    | '/paramYear/-1000000000'                               | 'paramYear: is not a valid type'           |
      | 'PATCH'  | '/paramYear/-1000000000'                               | 'paramYear: is not a valid type'           |
      | 'DELETE' | '/paramYear/-1000000000'                               | 'paramYear: is not a valid type'           |

  Scenario Outline: The <method> call is made to the /api/v1/path-variable/named-method-parameter<path> endpoint (PathVariableUnconstrainedRestApi.class)
    this endpoint receives the path-variable in a method parameter, both having the diferent names, a custom path-variable name is defined by the endpoint 
    and the service cannot map the sent value to the declared data type, responding with an error message.

    Given the endpoint '/api/v1/path-variable/named-method-parameter'<path>
    And the request with the <method> method
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                                                      | message                                       |
      | 'GET'    | '/param-integer/-2147483649'                              | 'param-integer: is not a valid type'          |
      | 'HEAD'   | '/param-integer/-2147483649'                              |                                               |
      | 'POST'   | '/param-integer/-2147483649'                              | 'param-integer: is not a valid type'          |
      | 'PUT'    | '/param-integer/-2147483649'                              | 'param-integer: is not a valid type'          |
      | 'PATCH'  | '/param-integer/-2147483649'                              | 'param-integer: is not a valid type'          |
      | 'DELETE' | '/param-integer/-2147483649'                              | 'param-integer: is not a valid type'          |
      | 'GET'    | '/param-long/-9223372036854775809'                        | 'param-long: is not a valid type'             |
      | 'HEAD'   | '/param-long/-9223372036854775809'                        |                                               |
      | 'POST'   | '/param-long/-9223372036854775809'                        | 'param-long: is not a valid type'             |
      | 'PUT'    | '/param-long/-9223372036854775809'                        | 'param-long: is not a valid type'             |
      | 'PATCH'  | '/param-long/-9223372036854775809'                        | 'param-long: is not a valid type'             |
      | 'DELETE' | '/param-long/-9223372036854775809'                        | 'param-long: is not a valid type'             |
      | 'GET'    | '/param-offset-date-time/-1000000000-12-31T23:59:59.999Z' | 'param-offset-date-time: is not a valid type' |
      | 'HEAD'   | '/param-offset-date-time/-1000000000-12-31T23:59:59.999Z' |                                               |
      | 'POST'   | '/param-offset-date-time/-1000000000-12-31T23:59:59.999Z' | 'param-offset-date-time: is not a valid type' |
      | 'PUT'    | '/param-offset-date-time/-1000000000-12-31T23:59:59.999Z' | 'param-offset-date-time: is not a valid type' |
      | 'PATCH'  | '/param-offset-date-time/-1000000000-12-31T23:59:59.999Z' | 'param-offset-date-time: is not a valid type' |
      | 'DELETE' | '/param-offset-date-time/-1000000000-12-31T23:59:59.999Z' | 'param-offset-date-time: is not a valid type' |
      | 'GET'    | '/param-local-date-time/-1000000000-12-31T23:59:59.999Z'  | 'param-local-date-time: is not a valid type'  |
      | 'HEAD'   | '/param-local-date-time/-1000000000-12-31T23:59:59.999Z'  |                                               |
      | 'POST'   | '/param-local-date-time/-1000000000-12-31T23:59:59.999Z'  | 'param-local-date-time: is not a valid type'  |
      | 'PUT'    | '/param-local-date-time/-1000000000-12-31T23:59:59.999Z'  | 'param-local-date-time: is not a valid type'  |
      | 'PATCH'  | '/param-local-date-time/-1000000000-12-31T23:59:59.999Z'  | 'param-local-date-time: is not a valid type'  |
      | 'DELETE' | '/param-local-date-time/-1000000000-12-31T23:59:59.999Z'  | 'param-local-date-time: is not a valid type'  |
      | 'GET'    | '/param-local-date/-1000000000-12-31'                     | 'param-local-date: is not a valid type'       |
      | 'HEAD'   | '/param-local-date/-1000000000-12-31'                     |                                               |
      | 'POST'   | '/param-local-date/-1000000000-12-31'                     | 'param-local-date: is not a valid type'       |
      | 'PUT'    | '/param-local-date/-1000000000-12-31'                     | 'param-local-date: is not a valid type'       |
      | 'PATCH'  | '/param-local-date/-1000000000-12-31'                     | 'param-local-date: is not a valid type'       |
      | 'DELETE' | '/param-local-date/-1000000000-12-31'                     | 'param-local-date: is not a valid type'       |
      | 'GET'    | '/param-year/-1000000000'                                 | 'param-year: is not a valid type'             |
      | 'HEAD'   | '/param-year/-1000000000'                                 |                                               |
      | 'POST'   | '/param-year/-1000000000'                                 | 'param-year: is not a valid type'             |
      | 'PUT'    | '/param-year/-1000000000'                                 | 'param-year: is not a valid type'             |
      | 'PATCH'  | '/param-year/-1000000000'                                 | 'param-year: is not a valid type'             |
      | 'DELETE' | '/param-year/-1000000000'                                 | 'param-year: is not a valid type'             |
