Feature: Path variable with i18n-validation for parameter value
  
  The REST service receives data via the path-variable and it does not accomplish with some constraint. The service responds with an error message.
  
   Rule: The conversion between data types is given by the following table
  
  | parameter type | java type      | validation              | validation                                   |
  |----------------|----------------|-------------------------|----------------------------------------------|
  | string         | String         | @Size                   | @Size(max = 1)                               |
  | number         | Integer        | @Min @Max               | @Max(1)                                      |
  | number         | Long           | @Min @Max               | @Max(1)                                      |
  | number         | BigInteger     | @DecimalMin @DecimalMax | @DecimalMax("1")                             |
  | number         | Float          |                         |                                              |
  | number         | Double         |                         |                                              |
  | number         | BigDecimal     | @DecimalMin @DecimalMax | @DecimalMax("9.9")                           |
  | boolean        | Boolean        |                         |                                              |
  | ISO-8601       | OffsetDateTime | @DatetimeRange          | @DatetimeRange(max = "2000-01-01T00:00:00Z") |
  | ISO-8601       | LocalDateTime  | @DatetimeRange          | @DatetimeRange(max = "2000-01-01T00:00:00Z") |
  | ISO-8601       | LocalDate      | @DateRange              | @DateRange(max = "2000-01-01")               |
  | ISO-8601       | LocalTime      |                         |                                              |
  | ISO-8601       | Year           | @YearRange              | @YearRange(max = "2000")                     |
  | string         | ENUM           |                         |                                              |

  Background: 
    Given the default timezone 'America/Puerto_Rico'

  Scenario Outline: The <method> call is made to the /api/v1/path-variable/i18n-constrained/method-parameter<path> endpoint (PathVariableI18nConstrainedRestApi.class)
    this endpoint receives the path-variable in a method parameter, both having the same name
    and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/path-variable/i18n-constrained/method-parameter'<path>
    And the request with the <method> method
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                                        | message                                               |
      | 'GET'    | '/paramString/ab'                           | 'paramString: is not a valid string'                  |
      | 'HEAD'   | '/paramString/ab'                           |                                                       |
      | 'POST'   | '/paramString/ab'                           | 'paramString: is not a valid string'                  |
      | 'PUT'    | '/paramString/ab'                           | 'paramString: is not a valid string'                  |
      | 'PATCH'  | '/paramString/ab'                           | 'paramString: is not a valid string'                  |
      | 'DELETE' | '/paramString/ab'                           | 'paramString: is not a valid string'                  |
      | 'GET'    | '/paramInteger/10'                          | 'paramInteger: is not a valid integer'                |
      | 'HEAD'   | '/paramInteger/10'                          |                                                       |
      | 'POST'   | '/paramInteger/10'                          | 'paramInteger: is not a valid integer'                |
      | 'PUT'    | '/paramInteger/10'                          | 'paramInteger: is not a valid integer'                |
      | 'PATCH'  | '/paramInteger/10'                          | 'paramInteger: is not a valid integer'                |
      | 'DELETE' | '/paramInteger/10'                          | 'paramInteger: is not a valid integer'                |
      | 'GET'    | '/paramLong/10'                             | 'paramLong: is not a valid long'                      |
      | 'HEAD'   | '/paramLong/10'                             |                                                       |
      | 'POST'   | '/paramLong/10'                             | 'paramLong: is not a valid long'                      |
      | 'PUT'    | '/paramLong/10'                             | 'paramLong: is not a valid long'                      |
      | 'PATCH'  | '/paramLong/10'                             | 'paramLong: is not a valid long'                      |
      | 'DELETE' | '/paramLong/10'                             | 'paramLong: is not a valid long'                      |
      | 'GET'    | '/paramBigInteger/10'                       | 'paramBigInteger: is not a valid big-integer'         |
      | 'HEAD'   | '/paramBigInteger/10'                       |                                                       |
      | 'POST'   | '/paramBigInteger/10'                       | 'paramBigInteger: is not a valid big-integer'         |
      | 'PUT'    | '/paramBigInteger/10'                       | 'paramBigInteger: is not a valid big-integer'         |
      | 'PATCH'  | '/paramBigInteger/10'                       | 'paramBigInteger: is not a valid big-integer'         |
      | 'DELETE' | '/paramBigInteger/10'                       | 'paramBigInteger: is not a valid big-integer'         |
      | 'GET'    | '/paramBigDecimal/10'                       | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'HEAD'   | '/paramBigDecimal/10'                       |                                                       |
      | 'POST'   | '/paramBigDecimal/10'                       | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'PUT'    | '/paramBigDecimal/10'                       | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'PATCH'  | '/paramBigDecimal/10'                       | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'DELETE' | '/paramBigDecimal/10'                       | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'GET'    | '/paramOffsetDateTime/2100-01-01T00:00:00Z' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'HEAD'   | '/paramOffsetDateTime/2100-01-01T00:00:00Z' |                                                       |
      | 'POST'   | '/paramOffsetDateTime/2100-01-01T00:00:00Z' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PUT'    | '/paramOffsetDateTime/2100-01-01T00:00:00Z' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PATCH'  | '/paramOffsetDateTime/2100-01-01T00:00:00Z' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'DELETE' | '/paramOffsetDateTime/2100-01-01T00:00:00Z' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'GET'    | '/paramLocalDateTime/2100-01-01T00:00:00Z'  | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'HEAD'   | '/paramLocalDateTime/2100-01-01T00:00:00Z'  |                                                       |
      | 'POST'   | '/paramLocalDateTime/2100-01-01T00:00:00Z'  | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'PUT'    | '/paramLocalDateTime/2100-01-01T00:00:00Z'  | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'PATCH'  | '/paramLocalDateTime/2100-01-01T00:00:00Z'  | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'DELETE' | '/paramLocalDateTime/2100-01-01T00:00:00Z'  | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'GET'    | '/paramLocalDate/2100-01-01'                | 'paramLocalDate: is not a valid date'                 |
      | 'HEAD'   | '/paramLocalDate/2100-01-01'                |                                                       |
      | 'POST'   | '/paramLocalDate/2100-01-01'                | 'paramLocalDate: is not a valid date'                 |
      | 'PUT'    | '/paramLocalDate/2100-01-01'                | 'paramLocalDate: is not a valid date'                 |
      | 'PATCH'  | '/paramLocalDate/2100-01-01'                | 'paramLocalDate: is not a valid date'                 |
      | 'DELETE' | '/paramLocalDate/2100-01-01'                | 'paramLocalDate: is not a valid date'                 |
      | 'GET'    | '/paramYear/2100'                           | 'paramYear: is not a valid year'                      |
      | 'HEAD'   | '/paramYear/2100'                           |                                                       |
      | 'POST'   | '/paramYear/2100'                           | 'paramYear: is not a valid year'                      |
      | 'PUT'    | '/paramYear/2100'                           | 'paramYear: is not a valid year'                      |
      | 'PATCH'  | '/paramYear/2100'                           | 'paramYear: is not a valid year'                      |
      | 'DELETE' | '/paramYear/2100'                           | 'paramYear: is not a valid year'                      |

  Scenario Outline: The <method> call is made to the /api/v1/path-variable/i18n-constrained/named-method-parameter<path> endpoint (PathVariableI18nConstrainedRestApi.class)
    this endpoint receives the path-variable in a method parameter, both having the diferent names, a custom path-variable name is defined by the endpoint 
    and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/path-variable/i18n-constrained/named-method-parameter'<path>
    And the request with the <method> method
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                                           | message                                               |
      | 'GET'    | '/param-string/ab'                             | 'paramString: is not a valid string'                  |
      | 'HEAD'   | '/param-string/ab'                             |                                                       |
      | 'POST'   | '/param-string/ab'                             | 'paramString: is not a valid string'                  |
      | 'PUT'    | '/param-string/ab'                             | 'paramString: is not a valid string'                  |
      | 'PATCH'  | '/param-string/ab'                             | 'paramString: is not a valid string'                  |
      | 'DELETE' | '/param-string/ab'                             | 'paramString: is not a valid string'                  |
      | 'GET'    | '/param-integer/10'                            | 'paramInteger: is not a valid integer'                |
      | 'HEAD'   | '/param-integer/10'                            |                                                       |
      | 'POST'   | '/param-integer/10'                            | 'paramInteger: is not a valid integer'                |
      | 'PUT'    | '/param-integer/10'                            | 'paramInteger: is not a valid integer'                |
      | 'PATCH'  | '/param-integer/10'                            | 'paramInteger: is not a valid integer'                |
      | 'DELETE' | '/param-integer/10'                            | 'paramInteger: is not a valid integer'                |
      | 'GET'    | '/param-long/10'                               | 'paramLong: is not a valid long'                      |
      | 'HEAD'   | '/param-long/10'                               |                                                       |
      | 'POST'   | '/param-long/10'                               | 'paramLong: is not a valid long'                      |
      | 'PUT'    | '/param-long/10'                               | 'paramLong: is not a valid long'                      |
      | 'PATCH'  | '/param-long/10'                               | 'paramLong: is not a valid long'                      |
      | 'DELETE' | '/param-long/10'                               | 'paramLong: is not a valid long'                      |
      | 'GET'    | '/param-big-integer/10'                        | 'paramBigInteger: is not a valid big-integer'         |
      | 'HEAD'   | '/param-big-integer/10'                        |                                                       |
      | 'POST'   | '/param-big-integer/10'                        | 'paramBigInteger: is not a valid big-integer'         |
      | 'PUT'    | '/param-big-integer/10'                        | 'paramBigInteger: is not a valid big-integer'         |
      | 'PATCH'  | '/param-big-integer/10'                        | 'paramBigInteger: is not a valid big-integer'         |
      | 'DELETE' | '/param-big-integer/10'                        | 'paramBigInteger: is not a valid big-integer'         |
      | 'GET'    | '/param-big-decimal/10'                        | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'HEAD'   | '/param-big-decimal/10'                        |                                                       |
      | 'POST'   | '/param-big-decimal/10'                        | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'PUT'    | '/param-big-decimal/10'                        | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'PATCH'  | '/param-big-decimal/10'                        | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'DELETE' | '/param-big-decimal/10'                        | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'GET'    | '/param-offset-date-time/2100-01-01T00:00:00Z' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'HEAD'   | '/param-offset-date-time/2100-01-01T00:00:00Z' |                                                       |
      | 'POST'   | '/param-offset-date-time/2100-01-01T00:00:00Z' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PUT'    | '/param-offset-date-time/2100-01-01T00:00:00Z' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PATCH'  | '/param-offset-date-time/2100-01-01T00:00:00Z' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'DELETE' | '/param-offset-date-time/2100-01-01T00:00:00Z' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'GET'    | '/param-local-date-time/2100-01-01T00:00:00Z'  | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'HEAD'   | '/param-local-date-time/2100-01-01T00:00:00Z'  |                                                       |
      | 'POST'   | '/param-local-date-time/2100-01-01T00:00:00Z'  | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'PUT'    | '/param-local-date-time/2100-01-01T00:00:00Z'  | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'PATCH'  | '/param-local-date-time/2100-01-01T00:00:00Z'  | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'DELETE' | '/param-local-date-time/2100-01-01T00:00:00Z'  | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'GET'    | '/param-local-date/2100-01-01'                 | 'paramLocalDate: is not a valid date'                 |
      | 'HEAD'   | '/param-local-date/2100-01-01'                 |                                                       |
      | 'POST'   | '/param-local-date/2100-01-01'                 | 'paramLocalDate: is not a valid date'                 |
      | 'PUT'    | '/param-local-date/2100-01-01'                 | 'paramLocalDate: is not a valid date'                 |
      | 'PATCH'  | '/param-local-date/2100-01-01'                 | 'paramLocalDate: is not a valid date'                 |
      | 'DELETE' | '/param-local-date/2100-01-01'                 | 'paramLocalDate: is not a valid date'                 |
      | 'GET'    | '/param-year/2100'                             | 'paramYear: is not a valid year'                      |
      | 'HEAD'   | '/param-year/2100'                             |                                                       |
      | 'POST'   | '/param-year/2100'                             | 'paramYear: is not a valid year'                      |
      | 'PUT'    | '/param-year/2100'                             | 'paramYear: is not a valid year'                      |
      | 'PATCH'  | '/param-year/2100'                             | 'paramYear: is not a valid year'                      |
      | 'DELETE' | '/param-year/2100'                             | 'paramYear: is not a valid year'                      |
