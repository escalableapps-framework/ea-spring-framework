Feature: Request body with non-null parameter value
  
  The REST service receives data via the request body and interprets it as null.  The service responds with an error message.
  
   Rule: The conversion between data types is given by the following table
  
  | parameter type | java type      |
  |----------------|----------------|
  | string         | String         |
  | number         | Integer        |
  | number         | Long           |
  | number         | BigInteger     |
  | number         | Float          |
  | number         | Double         |
  | number         | BigDecimal     |
  | boolean        | Boolean        |
  | ISO-8601       | OffsetDateTime |
  | ISO-8601       | LocalDateTime  |
  | ISO-8601       | LocalDate      |
  | ISO-8601       | LocalTime      |
  | ISO-8601       | Year           |
  | string         | ENUM           |

  Background: 
    Given the default timezone 'America/Puerto_Rico'

  Scenario Outline: The <method> call is made to the /api/v1/request-body/not-null/dto-attribute<path> endpoint with the body <request body> (RequestBodyConstrainedNotNullRestApi.class)
    this endpoint receives the request body-attibute in a dto attribute, both having the same name
    and the service maps the sent empty value to null, but service responding with an error message since data is must not be null.

    Given the endpoint '/api/v1/request-body/not-null/dto-attribute'<path>
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | request body                   | message                                 |
      | 'POST'   | '/paramString'         | '{"paramString":null}'         | 'paramString: must not be null'         |
      | 'PUT'    | '/paramString'         | '{"paramString":null}'         | 'paramString: must not be null'         |
      | 'PATCH'  | '/paramString'         | '{"paramString":null}'         | 'paramString: must not be null'         |
      | 'DELETE' | '/paramString'         | '{"paramString":null}'         | 'paramString: must not be null'         |
      | 'POST'   | '/paramInteger'        | '{"paramInteger":null}'        | 'paramInteger: must not be null'        |
      | 'PUT'    | '/paramInteger'        | '{"paramInteger":null}'        | 'paramInteger: must not be null'        |
      | 'PATCH'  | '/paramInteger'        | '{"paramInteger":null}'        | 'paramInteger: must not be null'        |
      | 'DELETE' | '/paramInteger'        | '{"paramInteger":null}'        | 'paramInteger: must not be null'        |
      | 'POST'   | '/paramLong'           | '{"paramLong":null}'           | 'paramLong: must not be null'           |
      | 'PUT'    | '/paramLong'           | '{"paramLong":null}'           | 'paramLong: must not be null'           |
      | 'PATCH'  | '/paramLong'           | '{"paramLong":null}'           | 'paramLong: must not be null'           |
      | 'DELETE' | '/paramLong'           | '{"paramLong":null}'           | 'paramLong: must not be null'           |
      | 'POST'   | '/paramBigInteger'     | '{"paramBigInteger":null}'     | 'paramBigInteger: must not be null'     |
      | 'PUT'    | '/paramBigInteger'     | '{"paramBigInteger":null}'     | 'paramBigInteger: must not be null'     |
      | 'PATCH'  | '/paramBigInteger'     | '{"paramBigInteger":null}'     | 'paramBigInteger: must not be null'     |
      | 'DELETE' | '/paramBigInteger'     | '{"paramBigInteger":null}'     | 'paramBigInteger: must not be null'     |
      | 'POST'   | '/paramFloat'          | '{"paramFloat":null}'          | 'paramFloat: must not be null'          |
      | 'PUT'    | '/paramFloat'          | '{"paramFloat":null}'          | 'paramFloat: must not be null'          |
      | 'PATCH'  | '/paramFloat'          | '{"paramFloat":null}'          | 'paramFloat: must not be null'          |
      | 'DELETE' | '/paramFloat'          | '{"paramFloat":null}'          | 'paramFloat: must not be null'          |
      | 'POST'   | '/paramDouble'         | '{"paramDouble":null}'         | 'paramDouble: must not be null'         |
      | 'PUT'    | '/paramDouble'         | '{"paramDouble":null}'         | 'paramDouble: must not be null'         |
      | 'PATCH'  | '/paramDouble'         | '{"paramDouble":null}'         | 'paramDouble: must not be null'         |
      | 'DELETE' | '/paramDouble'         | '{"paramDouble":null}'         | 'paramDouble: must not be null'         |
      | 'POST'   | '/paramBigDecimal'     | '{"paramBigDecimal":null}'     | 'paramBigDecimal: must not be null'     |
      | 'PUT'    | '/paramBigDecimal'     | '{"paramBigDecimal":null}'     | 'paramBigDecimal: must not be null'     |
      | 'PATCH'  | '/paramBigDecimal'     | '{"paramBigDecimal":null}'     | 'paramBigDecimal: must not be null'     |
      | 'DELETE' | '/paramBigDecimal'     | '{"paramBigDecimal":null}'     | 'paramBigDecimal: must not be null'     |
      | 'POST'   | '/paramBoolean'        | '{"paramBoolean":null}'        | 'paramBoolean: must not be null'        |
      | 'PUT'    | '/paramBoolean'        | '{"paramBoolean":null}'        | 'paramBoolean: must not be null'        |
      | 'PATCH'  | '/paramBoolean'        | '{"paramBoolean":null}'        | 'paramBoolean: must not be null'        |
      | 'DELETE' | '/paramBoolean'        | '{"paramBoolean":null}'        | 'paramBoolean: must not be null'        |
      | 'POST'   | '/paramOffsetDateTime' | '{"paramOffsetDateTime":null}' | 'paramOffsetDateTime: must not be null' |
      | 'PUT'    | '/paramOffsetDateTime' | '{"paramOffsetDateTime":null}' | 'paramOffsetDateTime: must not be null' |
      | 'PATCH'  | '/paramOffsetDateTime' | '{"paramOffsetDateTime":null}' | 'paramOffsetDateTime: must not be null' |
      | 'DELETE' | '/paramOffsetDateTime' | '{"paramOffsetDateTime":null}' | 'paramOffsetDateTime: must not be null' |
      | 'POST'   | '/paramLocalDateTime'  | '{"paramLocalDateTime":null}'  | 'paramLocalDateTime: must not be null'  |
      | 'PUT'    | '/paramLocalDateTime'  | '{"paramLocalDateTime":null}'  | 'paramLocalDateTime: must not be null'  |
      | 'PATCH'  | '/paramLocalDateTime'  | '{"paramLocalDateTime":null}'  | 'paramLocalDateTime: must not be null'  |
      | 'DELETE' | '/paramLocalDateTime'  | '{"paramLocalDateTime":null}'  | 'paramLocalDateTime: must not be null'  |
      | 'POST'   | '/paramLocalDate'      | '{"paramLocalDate":null}'      | 'paramLocalDate: must not be null'      |
      | 'PUT'    | '/paramLocalDate'      | '{"paramLocalDate":null}'      | 'paramLocalDate: must not be null'      |
      | 'PATCH'  | '/paramLocalDate'      | '{"paramLocalDate":null}'      | 'paramLocalDate: must not be null'      |
      | 'DELETE' | '/paramLocalDate'      | '{"paramLocalDate":null}'      | 'paramLocalDate: must not be null'      |
      | 'POST'   | '/paramLocalTime'      | '{"paramLocalTime":null}'      | 'paramLocalTime: must not be null'      |
      | 'PUT'    | '/paramLocalTime'      | '{"paramLocalTime":null}'      | 'paramLocalTime: must not be null'      |
      | 'PATCH'  | '/paramLocalTime'      | '{"paramLocalTime":null}'      | 'paramLocalTime: must not be null'      |
      | 'DELETE' | '/paramLocalTime'      | '{"paramLocalTime":null}'      | 'paramLocalTime: must not be null'      |
      | 'POST'   | '/paramYear'           | '{"paramYear":null}'           | 'paramYear: must not be null'           |
      | 'PUT'    | '/paramYear'           | '{"paramYear":null}'           | 'paramYear: must not be null'           |
      | 'PATCH'  | '/paramYear'           | '{"paramYear":null}'           | 'paramYear: must not be null'           |
      | 'DELETE' | '/paramYear'           | '{"paramYear":null}'           | 'paramYear: must not be null'           |
      | 'POST'   | '/paramEnumAB'         | '{"paramEnumAB":null}'         | 'paramEnumAB: must not be null'         |
      | 'PUT'    | '/paramEnumAB'         | '{"paramEnumAB":null}'         | 'paramEnumAB: must not be null'         |
      | 'PATCH'  | '/paramEnumAB'         | '{"paramEnumAB":null}'         | 'paramEnumAB: must not be null'         |
      | 'DELETE' | '/paramEnumAB'         | '{"paramEnumAB":null}'         | 'paramEnumAB: must not be null'         |
      | 'POST'   | '/paramEnumLowerAB'    | '{"paramEnumLowerAB":null}'    | 'paramEnumLowerAB: must not be null'    |
      | 'PUT'    | '/paramEnumLowerAB'    | '{"paramEnumLowerAB":null}'    | 'paramEnumLowerAB: must not be null'    |
      | 'PATCH'  | '/paramEnumLowerAB'    | '{"paramEnumLowerAB":null}'    | 'paramEnumLowerAB: must not be null'    |
      | 'DELETE' | '/paramEnumLowerAB'    | '{"paramEnumLowerAB":null}'    | 'paramEnumLowerAB: must not be null'    |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/not-null/named-dto-attribute<path> endpoint with the body <request body> (RequestBodyConstrainedNotNullRestApi.class)
    this endpoint receives the request body-attibute in a dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint 
    and the service maps the sent empty value to null, but service responding with an error message since data is must not be null.

    Given the endpoint '/api/v1/request-body/not-null/named-dto-attribute'<path>
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | request body                      | message                                 |
      | 'POST'   | '/paramString'         | '{"param-string":null}'           | 'paramString: must not be null'         |
      | 'PUT'    | '/paramString'         | '{"param-string":null}'           | 'paramString: must not be null'         |
      | 'PATCH'  | '/paramString'         | '{"param-string":null}'           | 'paramString: must not be null'         |
      | 'DELETE' | '/paramString'         | '{"param-string":null}'           | 'paramString: must not be null'         |
      | 'POST'   | '/paramInteger'        | '{"param-integer":null}'          | 'paramInteger: must not be null'        |
      | 'PUT'    | '/paramInteger'        | '{"param-integer":null}'          | 'paramInteger: must not be null'        |
      | 'PATCH'  | '/paramInteger'        | '{"param-integer":null}'          | 'paramInteger: must not be null'        |
      | 'DELETE' | '/paramInteger'        | '{"param-integer":null}'          | 'paramInteger: must not be null'        |
      | 'POST'   | '/paramLong'           | '{"param-long":null}'             | 'paramLong: must not be null'           |
      | 'PUT'    | '/paramLong'           | '{"param-long":null}'             | 'paramLong: must not be null'           |
      | 'PATCH'  | '/paramLong'           | '{"param-long":null}'             | 'paramLong: must not be null'           |
      | 'DELETE' | '/paramLong'           | '{"param-long":null}'             | 'paramLong: must not be null'           |
      | 'POST'   | '/paramBigInteger'     | '{"param-big-integer":null}'      | 'paramBigInteger: must not be null'     |
      | 'PUT'    | '/paramBigInteger'     | '{"param-big-integer":null}'      | 'paramBigInteger: must not be null'     |
      | 'PATCH'  | '/paramBigInteger'     | '{"param-big-integer":null}'      | 'paramBigInteger: must not be null'     |
      | 'DELETE' | '/paramBigInteger'     | '{"param-big-integer":null}'      | 'paramBigInteger: must not be null'     |
      | 'POST'   | '/paramFloat'          | '{"param-float":null}'            | 'paramFloat: must not be null'          |
      | 'PUT'    | '/paramFloat'          | '{"param-float":null}'            | 'paramFloat: must not be null'          |
      | 'PATCH'  | '/paramFloat'          | '{"param-float":null}'            | 'paramFloat: must not be null'          |
      | 'DELETE' | '/paramFloat'          | '{"param-float":null}'            | 'paramFloat: must not be null'          |
      | 'POST'   | '/paramDouble'         | '{"param-double":null}'           | 'paramDouble: must not be null'         |
      | 'PUT'    | '/paramDouble'         | '{"param-double":null}'           | 'paramDouble: must not be null'         |
      | 'PATCH'  | '/paramDouble'         | '{"param-double":null}'           | 'paramDouble: must not be null'         |
      | 'DELETE' | '/paramDouble'         | '{"param-double":null}'           | 'paramDouble: must not be null'         |
      | 'POST'   | '/paramBigDecimal'     | '{"param-big-decimal":null}'      | 'paramBigDecimal: must not be null'     |
      | 'PUT'    | '/paramBigDecimal'     | '{"param-big-decimal":null}'      | 'paramBigDecimal: must not be null'     |
      | 'PATCH'  | '/paramBigDecimal'     | '{"param-big-decimal":null}'      | 'paramBigDecimal: must not be null'     |
      | 'DELETE' | '/paramBigDecimal'     | '{"param-big-decimal":null}'      | 'paramBigDecimal: must not be null'     |
      | 'POST'   | '/paramBoolean'        | '{"param-boolean":null}'          | 'paramBoolean: must not be null'        |
      | 'PUT'    | '/paramBoolean'        | '{"param-boolean":null}'          | 'paramBoolean: must not be null'        |
      | 'PATCH'  | '/paramBoolean'        | '{"param-boolean":null}'          | 'paramBoolean: must not be null'        |
      | 'DELETE' | '/paramBoolean'        | '{"param-boolean":null}'          | 'paramBoolean: must not be null'        |
      | 'POST'   | '/paramOffsetDateTime' | '{"param-offset-date-time":null}' | 'paramOffsetDateTime: must not be null' |
      | 'PUT'    | '/paramOffsetDateTime' | '{"param-offset-date-time":null}' | 'paramOffsetDateTime: must not be null' |
      | 'PATCH'  | '/paramOffsetDateTime' | '{"param-offset-date-time":null}' | 'paramOffsetDateTime: must not be null' |
      | 'DELETE' | '/paramOffsetDateTime' | '{"param-offset-date-time":null}' | 'paramOffsetDateTime: must not be null' |
      | 'POST'   | '/paramLocalDateTime'  | '{"param-local-date-time":null}'  | 'paramLocalDateTime: must not be null'  |
      | 'PUT'    | '/paramLocalDateTime'  | '{"param-local-date-time":null}'  | 'paramLocalDateTime: must not be null'  |
      | 'PATCH'  | '/paramLocalDateTime'  | '{"param-local-date-time":null}'  | 'paramLocalDateTime: must not be null'  |
      | 'DELETE' | '/paramLocalDateTime'  | '{"param-local-date-time":null}'  | 'paramLocalDateTime: must not be null'  |
      | 'POST'   | '/paramLocalDate'      | '{"param-local-date":null}'       | 'paramLocalDate: must not be null'      |
      | 'PUT'    | '/paramLocalDate'      | '{"param-local-date":null}'       | 'paramLocalDate: must not be null'      |
      | 'PATCH'  | '/paramLocalDate'      | '{"param-local-date":null}'       | 'paramLocalDate: must not be null'      |
      | 'DELETE' | '/paramLocalDate'      | '{"param-local-date":null}'       | 'paramLocalDate: must not be null'      |
      | 'POST'   | '/paramLocalTime'      | '{"param-local-time":null}'       | 'paramLocalTime: must not be null'      |
      | 'PUT'    | '/paramLocalTime'      | '{"param-local-time":null}'       | 'paramLocalTime: must not be null'      |
      | 'PATCH'  | '/paramLocalTime'      | '{"param-local-time":null}'       | 'paramLocalTime: must not be null'      |
      | 'DELETE' | '/paramLocalTime'      | '{"param-local-time":null}'       | 'paramLocalTime: must not be null'      |
      | 'POST'   | '/paramYear'           | '{"param-year":null}'             | 'paramYear: must not be null'           |
      | 'PUT'    | '/paramYear'           | '{"param-year":null}'             | 'paramYear: must not be null'           |
      | 'PATCH'  | '/paramYear'           | '{"param-year":null}'             | 'paramYear: must not be null'           |
      | 'DELETE' | '/paramYear'           | '{"param-year":null}'             | 'paramYear: must not be null'           |
      | 'POST'   | '/paramEnumAB'         | '{"param-enum-ab":null}'          | 'paramEnumAB: must not be null'         |
      | 'PUT'    | '/paramEnumAB'         | '{"param-enum-ab":null}'          | 'paramEnumAB: must not be null'         |
      | 'PATCH'  | '/paramEnumAB'         | '{"param-enum-ab":null}'          | 'paramEnumAB: must not be null'         |
      | 'DELETE' | '/paramEnumAB'         | '{"param-enum-ab":null}'          | 'paramEnumAB: must not be null'         |
      | 'POST'   | '/paramEnumLowerAB'    | '{"param-enum-lower-ab":null}'    | 'paramEnumLowerAB: must not be null'    |
      | 'PUT'    | '/paramEnumLowerAB'    | '{"param-enum-lower-ab":null}'    | 'paramEnumLowerAB: must not be null'    |
      | 'PATCH'  | '/paramEnumLowerAB'    | '{"param-enum-lower-ab":null}'    | 'paramEnumLowerAB: must not be null'    |
      | 'DELETE' | '/paramEnumLowerAB'    | '{"param-enum-lower-ab":null}'    | 'paramEnumLowerAB: must not be null'    |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/not-null/optional-dto-attribute<path> endpoint with the body <request body> (RequestBodyConstrainedNotNullRestApi.class)
    this endpoint receives the request body-attibute in an optional dto attribute, both having the same name
    and the service maps the sent empty value to null, but service responding with an error message since data is must not be null.

    Given the endpoint '/api/v1/request-body/not-null/optional-dto-attribute'<path>
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | request body                   | message                                 |
      | 'POST'   | '/paramString'         | '{"paramString":null}'         | 'paramString: must not be null'         |
      | 'PUT'    | '/paramString'         | '{"paramString":null}'         | 'paramString: must not be null'         |
      | 'PATCH'  | '/paramString'         | '{"paramString":null}'         | 'paramString: must not be null'         |
      | 'DELETE' | '/paramString'         | '{"paramString":null}'         | 'paramString: must not be null'         |
      | 'POST'   | '/paramInteger'        | '{"paramInteger":null}'        | 'paramInteger: must not be null'        |
      | 'PUT'    | '/paramInteger'        | '{"paramInteger":null}'        | 'paramInteger: must not be null'        |
      | 'PATCH'  | '/paramInteger'        | '{"paramInteger":null}'        | 'paramInteger: must not be null'        |
      | 'DELETE' | '/paramInteger'        | '{"paramInteger":null}'        | 'paramInteger: must not be null'        |
      | 'POST'   | '/paramLong'           | '{"paramLong":null}'           | 'paramLong: must not be null'           |
      | 'PUT'    | '/paramLong'           | '{"paramLong":null}'           | 'paramLong: must not be null'           |
      | 'PATCH'  | '/paramLong'           | '{"paramLong":null}'           | 'paramLong: must not be null'           |
      | 'DELETE' | '/paramLong'           | '{"paramLong":null}'           | 'paramLong: must not be null'           |
      | 'POST'   | '/paramBigInteger'     | '{"paramBigInteger":null}'     | 'paramBigInteger: must not be null'     |
      | 'PUT'    | '/paramBigInteger'     | '{"paramBigInteger":null}'     | 'paramBigInteger: must not be null'     |
      | 'PATCH'  | '/paramBigInteger'     | '{"paramBigInteger":null}'     | 'paramBigInteger: must not be null'     |
      | 'DELETE' | '/paramBigInteger'     | '{"paramBigInteger":null}'     | 'paramBigInteger: must not be null'     |
      | 'POST'   | '/paramFloat'          | '{"paramFloat":null}'          | 'paramFloat: must not be null'          |
      | 'PUT'    | '/paramFloat'          | '{"paramFloat":null}'          | 'paramFloat: must not be null'          |
      | 'PATCH'  | '/paramFloat'          | '{"paramFloat":null}'          | 'paramFloat: must not be null'          |
      | 'DELETE' | '/paramFloat'          | '{"paramFloat":null}'          | 'paramFloat: must not be null'          |
      | 'POST'   | '/paramDouble'         | '{"paramDouble":null}'         | 'paramDouble: must not be null'         |
      | 'PUT'    | '/paramDouble'         | '{"paramDouble":null}'         | 'paramDouble: must not be null'         |
      | 'PATCH'  | '/paramDouble'         | '{"paramDouble":null}'         | 'paramDouble: must not be null'         |
      | 'DELETE' | '/paramDouble'         | '{"paramDouble":null}'         | 'paramDouble: must not be null'         |
      | 'POST'   | '/paramBigDecimal'     | '{"paramBigDecimal":null}'     | 'paramBigDecimal: must not be null'     |
      | 'PUT'    | '/paramBigDecimal'     | '{"paramBigDecimal":null}'     | 'paramBigDecimal: must not be null'     |
      | 'PATCH'  | '/paramBigDecimal'     | '{"paramBigDecimal":null}'     | 'paramBigDecimal: must not be null'     |
      | 'DELETE' | '/paramBigDecimal'     | '{"paramBigDecimal":null}'     | 'paramBigDecimal: must not be null'     |
      | 'POST'   | '/paramBoolean'        | '{"paramBoolean":null}'        | 'paramBoolean: must not be null'        |
      | 'PUT'    | '/paramBoolean'        | '{"paramBoolean":null}'        | 'paramBoolean: must not be null'        |
      | 'PATCH'  | '/paramBoolean'        | '{"paramBoolean":null}'        | 'paramBoolean: must not be null'        |
      | 'DELETE' | '/paramBoolean'        | '{"paramBoolean":null}'        | 'paramBoolean: must not be null'        |
      | 'POST'   | '/paramOffsetDateTime' | '{"paramOffsetDateTime":null}' | 'paramOffsetDateTime: must not be null' |
      | 'PUT'    | '/paramOffsetDateTime' | '{"paramOffsetDateTime":null}' | 'paramOffsetDateTime: must not be null' |
      | 'PATCH'  | '/paramOffsetDateTime' | '{"paramOffsetDateTime":null}' | 'paramOffsetDateTime: must not be null' |
      | 'DELETE' | '/paramOffsetDateTime' | '{"paramOffsetDateTime":null}' | 'paramOffsetDateTime: must not be null' |
      | 'POST'   | '/paramLocalDateTime'  | '{"paramLocalDateTime":null}'  | 'paramLocalDateTime: must not be null'  |
      | 'PUT'    | '/paramLocalDateTime'  | '{"paramLocalDateTime":null}'  | 'paramLocalDateTime: must not be null'  |
      | 'PATCH'  | '/paramLocalDateTime'  | '{"paramLocalDateTime":null}'  | 'paramLocalDateTime: must not be null'  |
      | 'DELETE' | '/paramLocalDateTime'  | '{"paramLocalDateTime":null}'  | 'paramLocalDateTime: must not be null'  |
      | 'POST'   | '/paramLocalDate'      | '{"paramLocalDate":null}'      | 'paramLocalDate: must not be null'      |
      | 'PUT'    | '/paramLocalDate'      | '{"paramLocalDate":null}'      | 'paramLocalDate: must not be null'      |
      | 'PATCH'  | '/paramLocalDate'      | '{"paramLocalDate":null}'      | 'paramLocalDate: must not be null'      |
      | 'DELETE' | '/paramLocalDate'      | '{"paramLocalDate":null}'      | 'paramLocalDate: must not be null'      |
      | 'POST'   | '/paramLocalTime'      | '{"paramLocalTime":null}'      | 'paramLocalTime: must not be null'      |
      | 'PUT'    | '/paramLocalTime'      | '{"paramLocalTime":null}'      | 'paramLocalTime: must not be null'      |
      | 'PATCH'  | '/paramLocalTime'      | '{"paramLocalTime":null}'      | 'paramLocalTime: must not be null'      |
      | 'DELETE' | '/paramLocalTime'      | '{"paramLocalTime":null}'      | 'paramLocalTime: must not be null'      |
      | 'POST'   | '/paramYear'           | '{"paramYear":null}'           | 'paramYear: must not be null'           |
      | 'PUT'    | '/paramYear'           | '{"paramYear":null}'           | 'paramYear: must not be null'           |
      | 'PATCH'  | '/paramYear'           | '{"paramYear":null}'           | 'paramYear: must not be null'           |
      | 'DELETE' | '/paramYear'           | '{"paramYear":null}'           | 'paramYear: must not be null'           |
      | 'POST'   | '/paramEnumAB'         | '{"paramEnumAB":null}'         | 'paramEnumAB: must not be null'         |
      | 'PUT'    | '/paramEnumAB'         | '{"paramEnumAB":null}'         | 'paramEnumAB: must not be null'         |
      | 'PATCH'  | '/paramEnumAB'         | '{"paramEnumAB":null}'         | 'paramEnumAB: must not be null'         |
      | 'DELETE' | '/paramEnumAB'         | '{"paramEnumAB":null}'         | 'paramEnumAB: must not be null'         |
      | 'POST'   | '/paramEnumLowerAB'    | '{"paramEnumLowerAB":null}'    | 'paramEnumLowerAB: must not be null'    |
      | 'PUT'    | '/paramEnumLowerAB'    | '{"paramEnumLowerAB":null}'    | 'paramEnumLowerAB: must not be null'    |
      | 'PATCH'  | '/paramEnumLowerAB'    | '{"paramEnumLowerAB":null}'    | 'paramEnumLowerAB: must not be null'    |
      | 'DELETE' | '/paramEnumLowerAB'    | '{"paramEnumLowerAB":null}'    | 'paramEnumLowerAB: must not be null'    |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/not-null/named-optional-dto-attribute<path> endpoint with the body <request body> (RequestBodyConstrainedNotNullRestApi.class)
    this endpoint receives the request body-attibute in an optional dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
    and the service maps the sent empty value to null, but service responding with an error message since data is must not be null.

    Given the endpoint '/api/v1/request-body/not-null/named-optional-dto-attribute'<path>
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | request body                      | message                                 |
      | 'POST'   | '/paramString'         | '{"param-string":null}'           | 'paramString: must not be null'         |
      | 'PUT'    | '/paramString'         | '{"param-string":null}'           | 'paramString: must not be null'         |
      | 'PATCH'  | '/paramString'         | '{"param-string":null}'           | 'paramString: must not be null'         |
      | 'DELETE' | '/paramString'         | '{"param-string":null}'           | 'paramString: must not be null'         |
      | 'POST'   | '/paramInteger'        | '{"param-integer":null}'          | 'paramInteger: must not be null'        |
      | 'PUT'    | '/paramInteger'        | '{"param-integer":null}'          | 'paramInteger: must not be null'        |
      | 'PATCH'  | '/paramInteger'        | '{"param-integer":null}'          | 'paramInteger: must not be null'        |
      | 'DELETE' | '/paramInteger'        | '{"param-integer":null}'          | 'paramInteger: must not be null'        |
      | 'POST'   | '/paramLong'           | '{"param-long":null}'             | 'paramLong: must not be null'           |
      | 'PUT'    | '/paramLong'           | '{"param-long":null}'             | 'paramLong: must not be null'           |
      | 'PATCH'  | '/paramLong'           | '{"param-long":null}'             | 'paramLong: must not be null'           |
      | 'DELETE' | '/paramLong'           | '{"param-long":null}'             | 'paramLong: must not be null'           |
      | 'POST'   | '/paramBigInteger'     | '{"param-big-integer":null}'      | 'paramBigInteger: must not be null'     |
      | 'PUT'    | '/paramBigInteger'     | '{"param-big-integer":null}'      | 'paramBigInteger: must not be null'     |
      | 'PATCH'  | '/paramBigInteger'     | '{"param-big-integer":null}'      | 'paramBigInteger: must not be null'     |
      | 'DELETE' | '/paramBigInteger'     | '{"param-big-integer":null}'      | 'paramBigInteger: must not be null'     |
      | 'POST'   | '/paramFloat'          | '{"param-float":null}'            | 'paramFloat: must not be null'          |
      | 'PUT'    | '/paramFloat'          | '{"param-float":null}'            | 'paramFloat: must not be null'          |
      | 'PATCH'  | '/paramFloat'          | '{"param-float":null}'            | 'paramFloat: must not be null'          |
      | 'DELETE' | '/paramFloat'          | '{"param-float":null}'            | 'paramFloat: must not be null'          |
      | 'POST'   | '/paramDouble'         | '{"param-double":null}'           | 'paramDouble: must not be null'         |
      | 'PUT'    | '/paramDouble'         | '{"param-double":null}'           | 'paramDouble: must not be null'         |
      | 'PATCH'  | '/paramDouble'         | '{"param-double":null}'           | 'paramDouble: must not be null'         |
      | 'DELETE' | '/paramDouble'         | '{"param-double":null}'           | 'paramDouble: must not be null'         |
      | 'POST'   | '/paramBigDecimal'     | '{"param-big-decimal":null}'      | 'paramBigDecimal: must not be null'     |
      | 'PUT'    | '/paramBigDecimal'     | '{"param-big-decimal":null}'      | 'paramBigDecimal: must not be null'     |
      | 'PATCH'  | '/paramBigDecimal'     | '{"param-big-decimal":null}'      | 'paramBigDecimal: must not be null'     |
      | 'DELETE' | '/paramBigDecimal'     | '{"param-big-decimal":null}'      | 'paramBigDecimal: must not be null'     |
      | 'POST'   | '/paramBoolean'        | '{"param-boolean":null}'          | 'paramBoolean: must not be null'        |
      | 'PUT'    | '/paramBoolean'        | '{"param-boolean":null}'          | 'paramBoolean: must not be null'        |
      | 'PATCH'  | '/paramBoolean'        | '{"param-boolean":null}'          | 'paramBoolean: must not be null'        |
      | 'DELETE' | '/paramBoolean'        | '{"param-boolean":null}'          | 'paramBoolean: must not be null'        |
      | 'POST'   | '/paramOffsetDateTime' | '{"param-offset-date-time":null}' | 'paramOffsetDateTime: must not be null' |
      | 'PUT'    | '/paramOffsetDateTime' | '{"param-offset-date-time":null}' | 'paramOffsetDateTime: must not be null' |
      | 'PATCH'  | '/paramOffsetDateTime' | '{"param-offset-date-time":null}' | 'paramOffsetDateTime: must not be null' |
      | 'DELETE' | '/paramOffsetDateTime' | '{"param-offset-date-time":null}' | 'paramOffsetDateTime: must not be null' |
      | 'POST'   | '/paramLocalDateTime'  | '{"param-local-date-time":null}'  | 'paramLocalDateTime: must not be null'  |
      | 'PUT'    | '/paramLocalDateTime'  | '{"param-local-date-time":null}'  | 'paramLocalDateTime: must not be null'  |
      | 'PATCH'  | '/paramLocalDateTime'  | '{"param-local-date-time":null}'  | 'paramLocalDateTime: must not be null'  |
      | 'DELETE' | '/paramLocalDateTime'  | '{"param-local-date-time":null}'  | 'paramLocalDateTime: must not be null'  |
      | 'POST'   | '/paramLocalDate'      | '{"param-local-date":null}'       | 'paramLocalDate: must not be null'      |
      | 'PUT'    | '/paramLocalDate'      | '{"param-local-date":null}'       | 'paramLocalDate: must not be null'      |
      | 'PATCH'  | '/paramLocalDate'      | '{"param-local-date":null}'       | 'paramLocalDate: must not be null'      |
      | 'DELETE' | '/paramLocalDate'      | '{"param-local-date":null}'       | 'paramLocalDate: must not be null'      |
      | 'POST'   | '/paramLocalTime'      | '{"param-local-time":null}'       | 'paramLocalTime: must not be null'      |
      | 'PUT'    | '/paramLocalTime'      | '{"param-local-time":null}'       | 'paramLocalTime: must not be null'      |
      | 'PATCH'  | '/paramLocalTime'      | '{"param-local-time":null}'       | 'paramLocalTime: must not be null'      |
      | 'DELETE' | '/paramLocalTime'      | '{"param-local-time":null}'       | 'paramLocalTime: must not be null'      |
      | 'POST'   | '/paramYear'           | '{"param-year":null}'             | 'paramYear: must not be null'           |
      | 'PUT'    | '/paramYear'           | '{"param-year":null}'             | 'paramYear: must not be null'           |
      | 'PATCH'  | '/paramYear'           | '{"param-year":null}'             | 'paramYear: must not be null'           |
      | 'DELETE' | '/paramYear'           | '{"param-year":null}'             | 'paramYear: must not be null'           |
      | 'POST'   | '/paramEnumAB'         | '{"param-enum-ab":null}'          | 'paramEnumAB: must not be null'         |
      | 'PUT'    | '/paramEnumAB'         | '{"param-enum-ab":null}'          | 'paramEnumAB: must not be null'         |
      | 'PATCH'  | '/paramEnumAB'         | '{"param-enum-ab":null}'          | 'paramEnumAB: must not be null'         |
      | 'DELETE' | '/paramEnumAB'         | '{"param-enum-ab":null}'          | 'paramEnumAB: must not be null'         |
      | 'POST'   | '/paramEnumLowerAB'    | '{"param-enum-lower-ab":null}'    | 'paramEnumLowerAB: must not be null'    |
      | 'PUT'    | '/paramEnumLowerAB'    | '{"param-enum-lower-ab":null}'    | 'paramEnumLowerAB: must not be null'    |
      | 'PATCH'  | '/paramEnumLowerAB'    | '{"param-enum-lower-ab":null}'    | 'paramEnumLowerAB: must not be null'    |
      | 'DELETE' | '/paramEnumLowerAB'    | '{"param-enum-lower-ab":null}'    | 'paramEnumLowerAB: must not be null'    |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/not-null/nested-dto-attribute<path> endpoint with the body <request body> (RequestBodyConstrainedNotNullRestApi.class)
    this endpoint receives the request body-attibute in a nested dto attribute, both having the same name
    and the service maps the sent empty value to null, but service responding with an error message since data is must not be null.

    Given the endpoint '/api/v1/request-body/not-null/nested-dto-attribute'<path>
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | request body                              | message                                        |
      | 'POST'   | '/paramString'         | '{"nested":{"paramString":null}}'         | 'nested.paramString: must not be null'         |
      | 'PUT'    | '/paramString'         | '{"nested":{"paramString":null}}'         | 'nested.paramString: must not be null'         |
      | 'PATCH'  | '/paramString'         | '{"nested":{"paramString":null}}'         | 'nested.paramString: must not be null'         |
      | 'DELETE' | '/paramString'         | '{"nested":{"paramString":null}}'         | 'nested.paramString: must not be null'         |
      | 'POST'   | '/paramInteger'        | '{"nested":{"paramInteger":null}}'        | 'nested.paramInteger: must not be null'        |
      | 'PUT'    | '/paramInteger'        | '{"nested":{"paramInteger":null}}'        | 'nested.paramInteger: must not be null'        |
      | 'PATCH'  | '/paramInteger'        | '{"nested":{"paramInteger":null}}'        | 'nested.paramInteger: must not be null'        |
      | 'DELETE' | '/paramInteger'        | '{"nested":{"paramInteger":null}}'        | 'nested.paramInteger: must not be null'        |
      | 'POST'   | '/paramLong'           | '{"nested":{"paramLong":null}}'           | 'nested.paramLong: must not be null'           |
      | 'PUT'    | '/paramLong'           | '{"nested":{"paramLong":null}}'           | 'nested.paramLong: must not be null'           |
      | 'PATCH'  | '/paramLong'           | '{"nested":{"paramLong":null}}'           | 'nested.paramLong: must not be null'           |
      | 'DELETE' | '/paramLong'           | '{"nested":{"paramLong":null}}'           | 'nested.paramLong: must not be null'           |
      | 'POST'   | '/paramBigInteger'     | '{"nested":{"paramBigInteger":null}}'     | 'nested.paramBigInteger: must not be null'     |
      | 'PUT'    | '/paramBigInteger'     | '{"nested":{"paramBigInteger":null}}'     | 'nested.paramBigInteger: must not be null'     |
      | 'PATCH'  | '/paramBigInteger'     | '{"nested":{"paramBigInteger":null}}'     | 'nested.paramBigInteger: must not be null'     |
      | 'DELETE' | '/paramBigInteger'     | '{"nested":{"paramBigInteger":null}}'     | 'nested.paramBigInteger: must not be null'     |
      | 'POST'   | '/paramFloat'          | '{"nested":{"paramFloat":null}}'          | 'nested.paramFloat: must not be null'          |
      | 'PUT'    | '/paramFloat'          | '{"nested":{"paramFloat":null}}'          | 'nested.paramFloat: must not be null'          |
      | 'PATCH'  | '/paramFloat'          | '{"nested":{"paramFloat":null}}'          | 'nested.paramFloat: must not be null'          |
      | 'DELETE' | '/paramFloat'          | '{"nested":{"paramFloat":null}}'          | 'nested.paramFloat: must not be null'          |
      | 'POST'   | '/paramDouble'         | '{"nested":{"paramDouble":null}}'         | 'nested.paramDouble: must not be null'         |
      | 'PUT'    | '/paramDouble'         | '{"nested":{"paramDouble":null}}'         | 'nested.paramDouble: must not be null'         |
      | 'PATCH'  | '/paramDouble'         | '{"nested":{"paramDouble":null}}'         | 'nested.paramDouble: must not be null'         |
      | 'DELETE' | '/paramDouble'         | '{"nested":{"paramDouble":null}}'         | 'nested.paramDouble: must not be null'         |
      | 'POST'   | '/paramBigDecimal'     | '{"nested":{"paramBigDecimal":null}}'     | 'nested.paramBigDecimal: must not be null'     |
      | 'PUT'    | '/paramBigDecimal'     | '{"nested":{"paramBigDecimal":null}}'     | 'nested.paramBigDecimal: must not be null'     |
      | 'PATCH'  | '/paramBigDecimal'     | '{"nested":{"paramBigDecimal":null}}'     | 'nested.paramBigDecimal: must not be null'     |
      | 'DELETE' | '/paramBigDecimal'     | '{"nested":{"paramBigDecimal":null}}'     | 'nested.paramBigDecimal: must not be null'     |
      | 'POST'   | '/paramBoolean'        | '{"nested":{"paramBoolean":null}}'        | 'nested.paramBoolean: must not be null'        |
      | 'PUT'    | '/paramBoolean'        | '{"nested":{"paramBoolean":null}}'        | 'nested.paramBoolean: must not be null'        |
      | 'PATCH'  | '/paramBoolean'        | '{"nested":{"paramBoolean":null}}'        | 'nested.paramBoolean: must not be null'        |
      | 'DELETE' | '/paramBoolean'        | '{"nested":{"paramBoolean":null}}'        | 'nested.paramBoolean: must not be null'        |
      | 'POST'   | '/paramOffsetDateTime' | '{"nested":{"paramOffsetDateTime":null}}' | 'nested.paramOffsetDateTime: must not be null' |
      | 'PUT'    | '/paramOffsetDateTime' | '{"nested":{"paramOffsetDateTime":null}}' | 'nested.paramOffsetDateTime: must not be null' |
      | 'PATCH'  | '/paramOffsetDateTime' | '{"nested":{"paramOffsetDateTime":null}}' | 'nested.paramOffsetDateTime: must not be null' |
      | 'DELETE' | '/paramOffsetDateTime' | '{"nested":{"paramOffsetDateTime":null}}' | 'nested.paramOffsetDateTime: must not be null' |
      | 'POST'   | '/paramLocalDateTime'  | '{"nested":{"paramLocalDateTime":null}}'  | 'nested.paramLocalDateTime: must not be null'  |
      | 'PUT'    | '/paramLocalDateTime'  | '{"nested":{"paramLocalDateTime":null}}'  | 'nested.paramLocalDateTime: must not be null'  |
      | 'PATCH'  | '/paramLocalDateTime'  | '{"nested":{"paramLocalDateTime":null}}'  | 'nested.paramLocalDateTime: must not be null'  |
      | 'DELETE' | '/paramLocalDateTime'  | '{"nested":{"paramLocalDateTime":null}}'  | 'nested.paramLocalDateTime: must not be null'  |
      | 'POST'   | '/paramLocalDate'      | '{"nested":{"paramLocalDate":null}}'      | 'nested.paramLocalDate: must not be null'      |
      | 'PUT'    | '/paramLocalDate'      | '{"nested":{"paramLocalDate":null}}'      | 'nested.paramLocalDate: must not be null'      |
      | 'PATCH'  | '/paramLocalDate'      | '{"nested":{"paramLocalDate":null}}'      | 'nested.paramLocalDate: must not be null'      |
      | 'DELETE' | '/paramLocalDate'      | '{"nested":{"paramLocalDate":null}}'      | 'nested.paramLocalDate: must not be null'      |
      | 'POST'   | '/paramLocalTime'      | '{"nested":{"paramLocalTime":null}}'      | 'nested.paramLocalTime: must not be null'      |
      | 'PUT'    | '/paramLocalTime'      | '{"nested":{"paramLocalTime":null}}'      | 'nested.paramLocalTime: must not be null'      |
      | 'PATCH'  | '/paramLocalTime'      | '{"nested":{"paramLocalTime":null}}'      | 'nested.paramLocalTime: must not be null'      |
      | 'DELETE' | '/paramLocalTime'      | '{"nested":{"paramLocalTime":null}}'      | 'nested.paramLocalTime: must not be null'      |
      | 'POST'   | '/paramYear'           | '{"nested":{"paramYear":null}}'           | 'nested.paramYear: must not be null'           |
      | 'PUT'    | '/paramYear'           | '{"nested":{"paramYear":null}}'           | 'nested.paramYear: must not be null'           |
      | 'PATCH'  | '/paramYear'           | '{"nested":{"paramYear":null}}'           | 'nested.paramYear: must not be null'           |
      | 'DELETE' | '/paramYear'           | '{"nested":{"paramYear":null}}'           | 'nested.paramYear: must not be null'           |
      | 'POST'   | '/paramEnumAB'         | '{"nested":{"paramEnumAB":null}}'         | 'nested.paramEnumAB: must not be null'         |
      | 'PUT'    | '/paramEnumAB'         | '{"nested":{"paramEnumAB":null}}'         | 'nested.paramEnumAB: must not be null'         |
      | 'PATCH'  | '/paramEnumAB'         | '{"nested":{"paramEnumAB":null}}'         | 'nested.paramEnumAB: must not be null'         |
      | 'DELETE' | '/paramEnumAB'         | '{"nested":{"paramEnumAB":null}}'         | 'nested.paramEnumAB: must not be null'         |
      | 'POST'   | '/paramEnumLowerAB'    | '{"nested":{"paramEnumLowerAB":null}}'    | 'nested.paramEnumLowerAB: must not be null'    |
      | 'PUT'    | '/paramEnumLowerAB'    | '{"nested":{"paramEnumLowerAB":null}}'    | 'nested.paramEnumLowerAB: must not be null'    |
      | 'PATCH'  | '/paramEnumLowerAB'    | '{"nested":{"paramEnumLowerAB":null}}'    | 'nested.paramEnumLowerAB: must not be null'    |
      | 'DELETE' | '/paramEnumLowerAB'    | '{"nested":{"paramEnumLowerAB":null}}'    | 'nested.paramEnumLowerAB: must not be null'    |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/not-null/named-nested-dto-attribute<path> endpoint with the body <request body> (RequestBodyConstrainedNotNullRestApi.class)
    this endpoint receives the request body-attibute in a nested dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
    and the service maps the sent empty value to null, but service responding with an error message since data is must not be null.

    Given the endpoint '/api/v1/request-body/not-null/named-nested-dto-attribute'<path>
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | request body                                 | message                                        |
      | 'POST'   | '/paramString'         | '{"nested":{"param-string":null}}'           | 'nested.paramString: must not be null'         |
      | 'PUT'    | '/paramString'         | '{"nested":{"param-string":null}}'           | 'nested.paramString: must not be null'         |
      | 'PATCH'  | '/paramString'         | '{"nested":{"param-string":null}}'           | 'nested.paramString: must not be null'         |
      | 'DELETE' | '/paramString'         | '{"nested":{"param-string":null}}'           | 'nested.paramString: must not be null'         |
      | 'POST'   | '/paramInteger'        | '{"nested":{"param-integer":null}}'          | 'nested.paramInteger: must not be null'        |
      | 'PUT'    | '/paramInteger'        | '{"nested":{"param-integer":null}}'          | 'nested.paramInteger: must not be null'        |
      | 'PATCH'  | '/paramInteger'        | '{"nested":{"param-integer":null}}'          | 'nested.paramInteger: must not be null'        |
      | 'DELETE' | '/paramInteger'        | '{"nested":{"param-integer":null}}'          | 'nested.paramInteger: must not be null'        |
      | 'POST'   | '/paramLong'           | '{"nested":{"param-long":null}}'             | 'nested.paramLong: must not be null'           |
      | 'PUT'    | '/paramLong'           | '{"nested":{"param-long":null}}'             | 'nested.paramLong: must not be null'           |
      | 'PATCH'  | '/paramLong'           | '{"nested":{"param-long":null}}'             | 'nested.paramLong: must not be null'           |
      | 'DELETE' | '/paramLong'           | '{"nested":{"param-long":null}}'             | 'nested.paramLong: must not be null'           |
      | 'POST'   | '/paramBigInteger'     | '{"nested":{"param-big-integer":null}}'      | 'nested.paramBigInteger: must not be null'     |
      | 'PUT'    | '/paramBigInteger'     | '{"nested":{"param-big-integer":null}}'      | 'nested.paramBigInteger: must not be null'     |
      | 'PATCH'  | '/paramBigInteger'     | '{"nested":{"param-big-integer":null}}'      | 'nested.paramBigInteger: must not be null'     |
      | 'DELETE' | '/paramBigInteger'     | '{"nested":{"param-big-integer":null}}'      | 'nested.paramBigInteger: must not be null'     |
      | 'POST'   | '/paramFloat'          | '{"nested":{"param-float":null}}'            | 'nested.paramFloat: must not be null'          |
      | 'PUT'    | '/paramFloat'          | '{"nested":{"param-float":null}}'            | 'nested.paramFloat: must not be null'          |
      | 'PATCH'  | '/paramFloat'          | '{"nested":{"param-float":null}}'            | 'nested.paramFloat: must not be null'          |
      | 'DELETE' | '/paramFloat'          | '{"nested":{"param-float":null}}'            | 'nested.paramFloat: must not be null'          |
      | 'POST'   | '/paramDouble'         | '{"nested":{"param-double":null}}'           | 'nested.paramDouble: must not be null'         |
      | 'PUT'    | '/paramDouble'         | '{"nested":{"param-double":null}}'           | 'nested.paramDouble: must not be null'         |
      | 'PATCH'  | '/paramDouble'         | '{"nested":{"param-double":null}}'           | 'nested.paramDouble: must not be null'         |
      | 'DELETE' | '/paramDouble'         | '{"nested":{"param-double":null}}'           | 'nested.paramDouble: must not be null'         |
      | 'POST'   | '/paramBigDecimal'     | '{"nested":{"param-big-decimal":null}}'      | 'nested.paramBigDecimal: must not be null'     |
      | 'PUT'    | '/paramBigDecimal'     | '{"nested":{"param-big-decimal":null}}'      | 'nested.paramBigDecimal: must not be null'     |
      | 'PATCH'  | '/paramBigDecimal'     | '{"nested":{"param-big-decimal":null}}'      | 'nested.paramBigDecimal: must not be null'     |
      | 'DELETE' | '/paramBigDecimal'     | '{"nested":{"param-big-decimal":null}}'      | 'nested.paramBigDecimal: must not be null'     |
      | 'POST'   | '/paramBoolean'        | '{"nested":{"param-boolean":null}}'          | 'nested.paramBoolean: must not be null'        |
      | 'PUT'    | '/paramBoolean'        | '{"nested":{"param-boolean":null}}'          | 'nested.paramBoolean: must not be null'        |
      | 'PATCH'  | '/paramBoolean'        | '{"nested":{"param-boolean":null}}'          | 'nested.paramBoolean: must not be null'        |
      | 'DELETE' | '/paramBoolean'        | '{"nested":{"param-boolean":null}}'          | 'nested.paramBoolean: must not be null'        |
      | 'POST'   | '/paramOffsetDateTime' | '{"nested":{"param-offset-date-time":null}}' | 'nested.paramOffsetDateTime: must not be null' |
      | 'PUT'    | '/paramOffsetDateTime' | '{"nested":{"param-offset-date-time":null}}' | 'nested.paramOffsetDateTime: must not be null' |
      | 'PATCH'  | '/paramOffsetDateTime' | '{"nested":{"param-offset-date-time":null}}' | 'nested.paramOffsetDateTime: must not be null' |
      | 'DELETE' | '/paramOffsetDateTime' | '{"nested":{"param-offset-date-time":null}}' | 'nested.paramOffsetDateTime: must not be null' |
      | 'POST'   | '/paramLocalDateTime'  | '{"nested":{"param-local-date-time":null}}'  | 'nested.paramLocalDateTime: must not be null'  |
      | 'PUT'    | '/paramLocalDateTime'  | '{"nested":{"param-local-date-time":null}}'  | 'nested.paramLocalDateTime: must not be null'  |
      | 'PATCH'  | '/paramLocalDateTime'  | '{"nested":{"param-local-date-time":null}}'  | 'nested.paramLocalDateTime: must not be null'  |
      | 'DELETE' | '/paramLocalDateTime'  | '{"nested":{"param-local-date-time":null}}'  | 'nested.paramLocalDateTime: must not be null'  |
      | 'POST'   | '/paramLocalDate'      | '{"nested":{"param-local-date":null}}'       | 'nested.paramLocalDate: must not be null'      |
      | 'PUT'    | '/paramLocalDate'      | '{"nested":{"param-local-date":null}}'       | 'nested.paramLocalDate: must not be null'      |
      | 'PATCH'  | '/paramLocalDate'      | '{"nested":{"param-local-date":null}}'       | 'nested.paramLocalDate: must not be null'      |
      | 'DELETE' | '/paramLocalDate'      | '{"nested":{"param-local-date":null}}'       | 'nested.paramLocalDate: must not be null'      |
      | 'POST'   | '/paramLocalTime'      | '{"nested":{"param-local-time":null}}'       | 'nested.paramLocalTime: must not be null'      |
      | 'PUT'    | '/paramLocalTime'      | '{"nested":{"param-local-time":null}}'       | 'nested.paramLocalTime: must not be null'      |
      | 'PATCH'  | '/paramLocalTime'      | '{"nested":{"param-local-time":null}}'       | 'nested.paramLocalTime: must not be null'      |
      | 'DELETE' | '/paramLocalTime'      | '{"nested":{"param-local-time":null}}'       | 'nested.paramLocalTime: must not be null'      |
      | 'POST'   | '/paramYear'           | '{"nested":{"param-year":null}}'             | 'nested.paramYear: must not be null'           |
      | 'PUT'    | '/paramYear'           | '{"nested":{"param-year":null}}'             | 'nested.paramYear: must not be null'           |
      | 'PATCH'  | '/paramYear'           | '{"nested":{"param-year":null}}'             | 'nested.paramYear: must not be null'           |
      | 'DELETE' | '/paramYear'           | '{"nested":{"param-year":null}}'             | 'nested.paramYear: must not be null'           |
      | 'POST'   | '/paramEnumAB'         | '{"nested":{"param-enum-ab":null}}'          | 'nested.paramEnumAB: must not be null'         |
      | 'PUT'    | '/paramEnumAB'         | '{"nested":{"param-enum-ab":null}}'          | 'nested.paramEnumAB: must not be null'         |
      | 'PATCH'  | '/paramEnumAB'         | '{"nested":{"param-enum-ab":null}}'          | 'nested.paramEnumAB: must not be null'         |
      | 'DELETE' | '/paramEnumAB'         | '{"nested":{"param-enum-ab":null}}'          | 'nested.paramEnumAB: must not be null'         |
      | 'POST'   | '/paramEnumLowerAB'    | '{"nested":{"param-enum-lower-ab":null}}'    | 'nested.paramEnumLowerAB: must not be null'    |
      | 'PUT'    | '/paramEnumLowerAB'    | '{"nested":{"param-enum-lower-ab":null}}'    | 'nested.paramEnumLowerAB: must not be null'    |
      | 'PATCH'  | '/paramEnumLowerAB'    | '{"nested":{"param-enum-lower-ab":null}}'    | 'nested.paramEnumLowerAB: must not be null'    |
      | 'DELETE' | '/paramEnumLowerAB'    | '{"nested":{"param-enum-lower-ab":null}}'    | 'nested.paramEnumLowerAB: must not be null'    |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/not-null/nested-optional-dto-attribute<path> endpoint with the body <request body> (RequestBodyConstrainedNotNullRestApi.class)
     this endpoint receives the request body-attibute in a nested optional dto attribute, both having the same name
     and the service maps the sent empty value to null, but service responding with an error message since data is must not be null.

    Given the endpoint '/api/v1/request-body/not-null/nested-optional-dto-attribute'<path>
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | request body                              | message                                        |
      | 'POST'   | '/paramString'         | '{"nested":{"paramString":null}}'         | 'nested.paramString: must not be null'         |
      | 'PUT'    | '/paramString'         | '{"nested":{"paramString":null}}'         | 'nested.paramString: must not be null'         |
      | 'PATCH'  | '/paramString'         | '{"nested":{"paramString":null}}'         | 'nested.paramString: must not be null'         |
      | 'DELETE' | '/paramString'         | '{"nested":{"paramString":null}}'         | 'nested.paramString: must not be null'         |
      | 'POST'   | '/paramInteger'        | '{"nested":{"paramInteger":null}}'        | 'nested.paramInteger: must not be null'        |
      | 'PUT'    | '/paramInteger'        | '{"nested":{"paramInteger":null}}'        | 'nested.paramInteger: must not be null'        |
      | 'PATCH'  | '/paramInteger'        | '{"nested":{"paramInteger":null}}'        | 'nested.paramInteger: must not be null'        |
      | 'DELETE' | '/paramInteger'        | '{"nested":{"paramInteger":null}}'        | 'nested.paramInteger: must not be null'        |
      | 'POST'   | '/paramLong'           | '{"nested":{"paramLong":null}}'           | 'nested.paramLong: must not be null'           |
      | 'PUT'    | '/paramLong'           | '{"nested":{"paramLong":null}}'           | 'nested.paramLong: must not be null'           |
      | 'PATCH'  | '/paramLong'           | '{"nested":{"paramLong":null}}'           | 'nested.paramLong: must not be null'           |
      | 'DELETE' | '/paramLong'           | '{"nested":{"paramLong":null}}'           | 'nested.paramLong: must not be null'           |
      | 'POST'   | '/paramBigInteger'     | '{"nested":{"paramBigInteger":null}}'     | 'nested.paramBigInteger: must not be null'     |
      | 'PUT'    | '/paramBigInteger'     | '{"nested":{"paramBigInteger":null}}'     | 'nested.paramBigInteger: must not be null'     |
      | 'PATCH'  | '/paramBigInteger'     | '{"nested":{"paramBigInteger":null}}'     | 'nested.paramBigInteger: must not be null'     |
      | 'DELETE' | '/paramBigInteger'     | '{"nested":{"paramBigInteger":null}}'     | 'nested.paramBigInteger: must not be null'     |
      | 'POST'   | '/paramFloat'          | '{"nested":{"paramFloat":null}}'          | 'nested.paramFloat: must not be null'          |
      | 'PUT'    | '/paramFloat'          | '{"nested":{"paramFloat":null}}'          | 'nested.paramFloat: must not be null'          |
      | 'PATCH'  | '/paramFloat'          | '{"nested":{"paramFloat":null}}'          | 'nested.paramFloat: must not be null'          |
      | 'DELETE' | '/paramFloat'          | '{"nested":{"paramFloat":null}}'          | 'nested.paramFloat: must not be null'          |
      | 'POST'   | '/paramDouble'         | '{"nested":{"paramDouble":null}}'         | 'nested.paramDouble: must not be null'         |
      | 'PUT'    | '/paramDouble'         | '{"nested":{"paramDouble":null}}'         | 'nested.paramDouble: must not be null'         |
      | 'PATCH'  | '/paramDouble'         | '{"nested":{"paramDouble":null}}'         | 'nested.paramDouble: must not be null'         |
      | 'DELETE' | '/paramDouble'         | '{"nested":{"paramDouble":null}}'         | 'nested.paramDouble: must not be null'         |
      | 'POST'   | '/paramBigDecimal'     | '{"nested":{"paramBigDecimal":null}}'     | 'nested.paramBigDecimal: must not be null'     |
      | 'PUT'    | '/paramBigDecimal'     | '{"nested":{"paramBigDecimal":null}}'     | 'nested.paramBigDecimal: must not be null'     |
      | 'PATCH'  | '/paramBigDecimal'     | '{"nested":{"paramBigDecimal":null}}'     | 'nested.paramBigDecimal: must not be null'     |
      | 'DELETE' | '/paramBigDecimal'     | '{"nested":{"paramBigDecimal":null}}'     | 'nested.paramBigDecimal: must not be null'     |
      | 'POST'   | '/paramBoolean'        | '{"nested":{"paramBoolean":null}}'        | 'nested.paramBoolean: must not be null'        |
      | 'PUT'    | '/paramBoolean'        | '{"nested":{"paramBoolean":null}}'        | 'nested.paramBoolean: must not be null'        |
      | 'PATCH'  | '/paramBoolean'        | '{"nested":{"paramBoolean":null}}'        | 'nested.paramBoolean: must not be null'        |
      | 'DELETE' | '/paramBoolean'        | '{"nested":{"paramBoolean":null}}'        | 'nested.paramBoolean: must not be null'        |
      | 'POST'   | '/paramOffsetDateTime' | '{"nested":{"paramOffsetDateTime":null}}' | 'nested.paramOffsetDateTime: must not be null' |
      | 'PUT'    | '/paramOffsetDateTime' | '{"nested":{"paramOffsetDateTime":null}}' | 'nested.paramOffsetDateTime: must not be null' |
      | 'PATCH'  | '/paramOffsetDateTime' | '{"nested":{"paramOffsetDateTime":null}}' | 'nested.paramOffsetDateTime: must not be null' |
      | 'DELETE' | '/paramOffsetDateTime' | '{"nested":{"paramOffsetDateTime":null}}' | 'nested.paramOffsetDateTime: must not be null' |
      | 'POST'   | '/paramLocalDateTime'  | '{"nested":{"paramLocalDateTime":null}}'  | 'nested.paramLocalDateTime: must not be null'  |
      | 'PUT'    | '/paramLocalDateTime'  | '{"nested":{"paramLocalDateTime":null}}'  | 'nested.paramLocalDateTime: must not be null'  |
      | 'PATCH'  | '/paramLocalDateTime'  | '{"nested":{"paramLocalDateTime":null}}'  | 'nested.paramLocalDateTime: must not be null'  |
      | 'DELETE' | '/paramLocalDateTime'  | '{"nested":{"paramLocalDateTime":null}}'  | 'nested.paramLocalDateTime: must not be null'  |
      | 'POST'   | '/paramLocalDate'      | '{"nested":{"paramLocalDate":null}}'      | 'nested.paramLocalDate: must not be null'      |
      | 'PUT'    | '/paramLocalDate'      | '{"nested":{"paramLocalDate":null}}'      | 'nested.paramLocalDate: must not be null'      |
      | 'PATCH'  | '/paramLocalDate'      | '{"nested":{"paramLocalDate":null}}'      | 'nested.paramLocalDate: must not be null'      |
      | 'DELETE' | '/paramLocalDate'      | '{"nested":{"paramLocalDate":null}}'      | 'nested.paramLocalDate: must not be null'      |
      | 'POST'   | '/paramLocalTime'      | '{"nested":{"paramLocalTime":null}}'      | 'nested.paramLocalTime: must not be null'      |
      | 'PUT'    | '/paramLocalTime'      | '{"nested":{"paramLocalTime":null}}'      | 'nested.paramLocalTime: must not be null'      |
      | 'PATCH'  | '/paramLocalTime'      | '{"nested":{"paramLocalTime":null}}'      | 'nested.paramLocalTime: must not be null'      |
      | 'DELETE' | '/paramLocalTime'      | '{"nested":{"paramLocalTime":null}}'      | 'nested.paramLocalTime: must not be null'      |
      | 'POST'   | '/paramYear'           | '{"nested":{"paramYear":null}}'           | 'nested.paramYear: must not be null'           |
      | 'PUT'    | '/paramYear'           | '{"nested":{"paramYear":null}}'           | 'nested.paramYear: must not be null'           |
      | 'PATCH'  | '/paramYear'           | '{"nested":{"paramYear":null}}'           | 'nested.paramYear: must not be null'           |
      | 'DELETE' | '/paramYear'           | '{"nested":{"paramYear":null}}'           | 'nested.paramYear: must not be null'           |
      | 'POST'   | '/paramEnumAB'         | '{"nested":{"paramEnumAB":null}}'         | 'nested.paramEnumAB: must not be null'         |
      | 'PUT'    | '/paramEnumAB'         | '{"nested":{"paramEnumAB":null}}'         | 'nested.paramEnumAB: must not be null'         |
      | 'PATCH'  | '/paramEnumAB'         | '{"nested":{"paramEnumAB":null}}'         | 'nested.paramEnumAB: must not be null'         |
      | 'DELETE' | '/paramEnumAB'         | '{"nested":{"paramEnumAB":null}}'         | 'nested.paramEnumAB: must not be null'         |
      | 'POST'   | '/paramEnumLowerAB'    | '{"nested":{"paramEnumLowerAB":null}}'    | 'nested.paramEnumLowerAB: must not be null'    |
      | 'PUT'    | '/paramEnumLowerAB'    | '{"nested":{"paramEnumLowerAB":null}}'    | 'nested.paramEnumLowerAB: must not be null'    |
      | 'PATCH'  | '/paramEnumLowerAB'    | '{"nested":{"paramEnumLowerAB":null}}'    | 'nested.paramEnumLowerAB: must not be null'    |
      | 'DELETE' | '/paramEnumLowerAB'    | '{"nested":{"paramEnumLowerAB":null}}'    | 'nested.paramEnumLowerAB: must not be null'    |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/not-null/named-nested-optional-dto-attribute<path> endpoint with the body <request body> (RequestBodyConstrainedNotNullRestApi.class)
    this endpoint receives the request body-attibute in a nested optional dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
    and the service maps the sent empty value to null, but service responding with an error message since data is must not be null.

    Given the endpoint '/api/v1/request-body/not-null/named-nested-optional-dto-attribute'<path>
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | request body                                 | message                                        |
      | 'POST'   | '/paramString'         | '{"nested":{"param-string":null}}'           | 'nested.paramString: must not be null'         |
      | 'PUT'    | '/paramString'         | '{"nested":{"param-string":null}}'           | 'nested.paramString: must not be null'         |
      | 'PATCH'  | '/paramString'         | '{"nested":{"param-string":null}}'           | 'nested.paramString: must not be null'         |
      | 'DELETE' | '/paramString'         | '{"nested":{"param-string":null}}'           | 'nested.paramString: must not be null'         |
      | 'POST'   | '/paramInteger'        | '{"nested":{"param-integer":null}}'          | 'nested.paramInteger: must not be null'        |
      | 'PUT'    | '/paramInteger'        | '{"nested":{"param-integer":null}}'          | 'nested.paramInteger: must not be null'        |
      | 'PATCH'  | '/paramInteger'        | '{"nested":{"param-integer":null}}'          | 'nested.paramInteger: must not be null'        |
      | 'DELETE' | '/paramInteger'        | '{"nested":{"param-integer":null}}'          | 'nested.paramInteger: must not be null'        |
      | 'POST'   | '/paramLong'           | '{"nested":{"param-long":null}}'             | 'nested.paramLong: must not be null'           |
      | 'PUT'    | '/paramLong'           | '{"nested":{"param-long":null}}'             | 'nested.paramLong: must not be null'           |
      | 'PATCH'  | '/paramLong'           | '{"nested":{"param-long":null}}'             | 'nested.paramLong: must not be null'           |
      | 'DELETE' | '/paramLong'           | '{"nested":{"param-long":null}}'             | 'nested.paramLong: must not be null'           |
      | 'POST'   | '/paramBigInteger'     | '{"nested":{"param-big-integer":null}}'      | 'nested.paramBigInteger: must not be null'     |
      | 'PUT'    | '/paramBigInteger'     | '{"nested":{"param-big-integer":null}}'      | 'nested.paramBigInteger: must not be null'     |
      | 'PATCH'  | '/paramBigInteger'     | '{"nested":{"param-big-integer":null}}'      | 'nested.paramBigInteger: must not be null'     |
      | 'DELETE' | '/paramBigInteger'     | '{"nested":{"param-big-integer":null}}'      | 'nested.paramBigInteger: must not be null'     |
      | 'POST'   | '/paramFloat'          | '{"nested":{"param-float":null}}'            | 'nested.paramFloat: must not be null'          |
      | 'PUT'    | '/paramFloat'          | '{"nested":{"param-float":null}}'            | 'nested.paramFloat: must not be null'          |
      | 'PATCH'  | '/paramFloat'          | '{"nested":{"param-float":null}}'            | 'nested.paramFloat: must not be null'          |
      | 'DELETE' | '/paramFloat'          | '{"nested":{"param-float":null}}'            | 'nested.paramFloat: must not be null'          |
      | 'POST'   | '/paramDouble'         | '{"nested":{"param-double":null}}'           | 'nested.paramDouble: must not be null'         |
      | 'PUT'    | '/paramDouble'         | '{"nested":{"param-double":null}}'           | 'nested.paramDouble: must not be null'         |
      | 'PATCH'  | '/paramDouble'         | '{"nested":{"param-double":null}}'           | 'nested.paramDouble: must not be null'         |
      | 'DELETE' | '/paramDouble'         | '{"nested":{"param-double":null}}'           | 'nested.paramDouble: must not be null'         |
      | 'POST'   | '/paramBigDecimal'     | '{"nested":{"param-big-decimal":null}}'      | 'nested.paramBigDecimal: must not be null'     |
      | 'PUT'    | '/paramBigDecimal'     | '{"nested":{"param-big-decimal":null}}'      | 'nested.paramBigDecimal: must not be null'     |
      | 'PATCH'  | '/paramBigDecimal'     | '{"nested":{"param-big-decimal":null}}'      | 'nested.paramBigDecimal: must not be null'     |
      | 'DELETE' | '/paramBigDecimal'     | '{"nested":{"param-big-decimal":null}}'      | 'nested.paramBigDecimal: must not be null'     |
      | 'POST'   | '/paramBoolean'        | '{"nested":{"param-boolean":null}}'          | 'nested.paramBoolean: must not be null'        |
      | 'PUT'    | '/paramBoolean'        | '{"nested":{"param-boolean":null}}'          | 'nested.paramBoolean: must not be null'        |
      | 'PATCH'  | '/paramBoolean'        | '{"nested":{"param-boolean":null}}'          | 'nested.paramBoolean: must not be null'        |
      | 'DELETE' | '/paramBoolean'        | '{"nested":{"param-boolean":null}}'          | 'nested.paramBoolean: must not be null'        |
      | 'POST'   | '/paramOffsetDateTime' | '{"nested":{"param-offset-date-time":null}}' | 'nested.paramOffsetDateTime: must not be null' |
      | 'PUT'    | '/paramOffsetDateTime' | '{"nested":{"param-offset-date-time":null}}' | 'nested.paramOffsetDateTime: must not be null' |
      | 'PATCH'  | '/paramOffsetDateTime' | '{"nested":{"param-offset-date-time":null}}' | 'nested.paramOffsetDateTime: must not be null' |
      | 'DELETE' | '/paramOffsetDateTime' | '{"nested":{"param-offset-date-time":null}}' | 'nested.paramOffsetDateTime: must not be null' |
      | 'POST'   | '/paramLocalDateTime'  | '{"nested":{"param-local-date-time":null}}'  | 'nested.paramLocalDateTime: must not be null'  |
      | 'PUT'    | '/paramLocalDateTime'  | '{"nested":{"param-local-date-time":null}}'  | 'nested.paramLocalDateTime: must not be null'  |
      | 'PATCH'  | '/paramLocalDateTime'  | '{"nested":{"param-local-date-time":null}}'  | 'nested.paramLocalDateTime: must not be null'  |
      | 'DELETE' | '/paramLocalDateTime'  | '{"nested":{"param-local-date-time":null}}'  | 'nested.paramLocalDateTime: must not be null'  |
      | 'POST'   | '/paramLocalDate'      | '{"nested":{"param-local-date":null}}'       | 'nested.paramLocalDate: must not be null'      |
      | 'PUT'    | '/paramLocalDate'      | '{"nested":{"param-local-date":null}}'       | 'nested.paramLocalDate: must not be null'      |
      | 'PATCH'  | '/paramLocalDate'      | '{"nested":{"param-local-date":null}}'       | 'nested.paramLocalDate: must not be null'      |
      | 'DELETE' | '/paramLocalDate'      | '{"nested":{"param-local-date":null}}'       | 'nested.paramLocalDate: must not be null'      |
      | 'POST'   | '/paramLocalTime'      | '{"nested":{"param-local-time":null}}'       | 'nested.paramLocalTime: must not be null'      |
      | 'PUT'    | '/paramLocalTime'      | '{"nested":{"param-local-time":null}}'       | 'nested.paramLocalTime: must not be null'      |
      | 'PATCH'  | '/paramLocalTime'      | '{"nested":{"param-local-time":null}}'       | 'nested.paramLocalTime: must not be null'      |
      | 'DELETE' | '/paramLocalTime'      | '{"nested":{"param-local-time":null}}'       | 'nested.paramLocalTime: must not be null'      |
      | 'POST'   | '/paramYear'           | '{"nested":{"param-year":null}}'             | 'nested.paramYear: must not be null'           |
      | 'PUT'    | '/paramYear'           | '{"nested":{"param-year":null}}'             | 'nested.paramYear: must not be null'           |
      | 'PATCH'  | '/paramYear'           | '{"nested":{"param-year":null}}'             | 'nested.paramYear: must not be null'           |
      | 'DELETE' | '/paramYear'           | '{"nested":{"param-year":null}}'             | 'nested.paramYear: must not be null'           |
      | 'POST'   | '/paramEnumAB'         | '{"nested":{"param-enum-ab":null}}'          | 'nested.paramEnumAB: must not be null'         |
      | 'PUT'    | '/paramEnumAB'         | '{"nested":{"param-enum-ab":null}}'          | 'nested.paramEnumAB: must not be null'         |
      | 'PATCH'  | '/paramEnumAB'         | '{"nested":{"param-enum-ab":null}}'          | 'nested.paramEnumAB: must not be null'         |
      | 'DELETE' | '/paramEnumAB'         | '{"nested":{"param-enum-ab":null}}'          | 'nested.paramEnumAB: must not be null'         |
      | 'POST'   | '/paramEnumLowerAB'    | '{"nested":{"param-enum-lower-ab":null}}'    | 'nested.paramEnumLowerAB: must not be null'    |
      | 'PUT'    | '/paramEnumLowerAB'    | '{"nested":{"param-enum-lower-ab":null}}'    | 'nested.paramEnumLowerAB: must not be null'    |
      | 'PATCH'  | '/paramEnumLowerAB'    | '{"nested":{"param-enum-lower-ab":null}}'    | 'nested.paramEnumLowerAB: must not be null'    |
      | 'DELETE' | '/paramEnumLowerAB'    | '{"nested":{"param-enum-lower-ab":null}}'    | 'nested.paramEnumLowerAB: must not be null'    |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/not-null/nested-dto-attribute<path> endpoint with the body <request body> (RequestBodyConstrainedNotNullRestApi.class)
    this endpoint receives the request body-attibute in a nested dto attribute from an array, both having the same name
    and the service maps the sent empty value to null, but service responding with an error message since data is must not be null.

    Given the endpoint '/api/v1/request-body/not-null/nested-dto-attribute'<path>
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | request body                               | message                                          |
      | 'POST'   | '/paramString'         | '{"array":[{"paramString":null}]}'         | 'array[0].paramString: must not be null'         |
      | 'PUT'    | '/paramString'         | '{"array":[{"paramString":null}]}'         | 'array[0].paramString: must not be null'         |
      | 'PATCH'  | '/paramString'         | '{"array":[{"paramString":null}]}'         | 'array[0].paramString: must not be null'         |
      | 'DELETE' | '/paramString'         | '{"array":[{"paramString":null}]}'         | 'array[0].paramString: must not be null'         |
      | 'POST'   | '/paramInteger'        | '{"array":[{"paramInteger":null}]}'        | 'array[0].paramInteger: must not be null'        |
      | 'PUT'    | '/paramInteger'        | '{"array":[{"paramInteger":null}]}'        | 'array[0].paramInteger: must not be null'        |
      | 'PATCH'  | '/paramInteger'        | '{"array":[{"paramInteger":null}]}'        | 'array[0].paramInteger: must not be null'        |
      | 'DELETE' | '/paramInteger'        | '{"array":[{"paramInteger":null}]}'        | 'array[0].paramInteger: must not be null'        |
      | 'POST'   | '/paramLong'           | '{"array":[{"paramLong":null}]}'           | 'array[0].paramLong: must not be null'           |
      | 'PUT'    | '/paramLong'           | '{"array":[{"paramLong":null}]}'           | 'array[0].paramLong: must not be null'           |
      | 'PATCH'  | '/paramLong'           | '{"array":[{"paramLong":null}]}'           | 'array[0].paramLong: must not be null'           |
      | 'DELETE' | '/paramLong'           | '{"array":[{"paramLong":null}]}'           | 'array[0].paramLong: must not be null'           |
      | 'POST'   | '/paramBigInteger'     | '{"array":[{"paramBigInteger":null}]}'     | 'array[0].paramBigInteger: must not be null'     |
      | 'PUT'    | '/paramBigInteger'     | '{"array":[{"paramBigInteger":null}]}'     | 'array[0].paramBigInteger: must not be null'     |
      | 'PATCH'  | '/paramBigInteger'     | '{"array":[{"paramBigInteger":null}]}'     | 'array[0].paramBigInteger: must not be null'     |
      | 'DELETE' | '/paramBigInteger'     | '{"array":[{"paramBigInteger":null}]}'     | 'array[0].paramBigInteger: must not be null'     |
      | 'POST'   | '/paramFloat'          | '{"array":[{"paramFloat":null}]}'          | 'array[0].paramFloat: must not be null'          |
      | 'PUT'    | '/paramFloat'          | '{"array":[{"paramFloat":null}]}'          | 'array[0].paramFloat: must not be null'          |
      | 'PATCH'  | '/paramFloat'          | '{"array":[{"paramFloat":null}]}'          | 'array[0].paramFloat: must not be null'          |
      | 'DELETE' | '/paramFloat'          | '{"array":[{"paramFloat":null}]}'          | 'array[0].paramFloat: must not be null'          |
      | 'POST'   | '/paramDouble'         | '{"array":[{"paramDouble":null}]}'         | 'array[0].paramDouble: must not be null'         |
      | 'PUT'    | '/paramDouble'         | '{"array":[{"paramDouble":null}]}'         | 'array[0].paramDouble: must not be null'         |
      | 'PATCH'  | '/paramDouble'         | '{"array":[{"paramDouble":null}]}'         | 'array[0].paramDouble: must not be null'         |
      | 'DELETE' | '/paramDouble'         | '{"array":[{"paramDouble":null}]}'         | 'array[0].paramDouble: must not be null'         |
      | 'POST'   | '/paramBigDecimal'     | '{"array":[{"paramBigDecimal":null}]}'     | 'array[0].paramBigDecimal: must not be null'     |
      | 'PUT'    | '/paramBigDecimal'     | '{"array":[{"paramBigDecimal":null}]}'     | 'array[0].paramBigDecimal: must not be null'     |
      | 'PATCH'  | '/paramBigDecimal'     | '{"array":[{"paramBigDecimal":null}]}'     | 'array[0].paramBigDecimal: must not be null'     |
      | 'DELETE' | '/paramBigDecimal'     | '{"array":[{"paramBigDecimal":null}]}'     | 'array[0].paramBigDecimal: must not be null'     |
      | 'POST'   | '/paramBoolean'        | '{"array":[{"paramBoolean":null}]}'        | 'array[0].paramBoolean: must not be null'        |
      | 'PUT'    | '/paramBoolean'        | '{"array":[{"paramBoolean":null}]}'        | 'array[0].paramBoolean: must not be null'        |
      | 'PATCH'  | '/paramBoolean'        | '{"array":[{"paramBoolean":null}]}'        | 'array[0].paramBoolean: must not be null'        |
      | 'DELETE' | '/paramBoolean'        | '{"array":[{"paramBoolean":null}]}'        | 'array[0].paramBoolean: must not be null'        |
      | 'POST'   | '/paramOffsetDateTime' | '{"array":[{"paramOffsetDateTime":null}]}' | 'array[0].paramOffsetDateTime: must not be null' |
      | 'PUT'    | '/paramOffsetDateTime' | '{"array":[{"paramOffsetDateTime":null}]}' | 'array[0].paramOffsetDateTime: must not be null' |
      | 'PATCH'  | '/paramOffsetDateTime' | '{"array":[{"paramOffsetDateTime":null}]}' | 'array[0].paramOffsetDateTime: must not be null' |
      | 'DELETE' | '/paramOffsetDateTime' | '{"array":[{"paramOffsetDateTime":null}]}' | 'array[0].paramOffsetDateTime: must not be null' |
      | 'POST'   | '/paramLocalDateTime'  | '{"array":[{"paramLocalDateTime":null}]}'  | 'array[0].paramLocalDateTime: must not be null'  |
      | 'PUT'    | '/paramLocalDateTime'  | '{"array":[{"paramLocalDateTime":null}]}'  | 'array[0].paramLocalDateTime: must not be null'  |
      | 'PATCH'  | '/paramLocalDateTime'  | '{"array":[{"paramLocalDateTime":null}]}'  | 'array[0].paramLocalDateTime: must not be null'  |
      | 'DELETE' | '/paramLocalDateTime'  | '{"array":[{"paramLocalDateTime":null}]}'  | 'array[0].paramLocalDateTime: must not be null'  |
      | 'POST'   | '/paramLocalDate'      | '{"array":[{"paramLocalDate":null}]}'      | 'array[0].paramLocalDate: must not be null'      |
      | 'PUT'    | '/paramLocalDate'      | '{"array":[{"paramLocalDate":null}]}'      | 'array[0].paramLocalDate: must not be null'      |
      | 'PATCH'  | '/paramLocalDate'      | '{"array":[{"paramLocalDate":null}]}'      | 'array[0].paramLocalDate: must not be null'      |
      | 'DELETE' | '/paramLocalDate'      | '{"array":[{"paramLocalDate":null}]}'      | 'array[0].paramLocalDate: must not be null'      |
      | 'POST'   | '/paramLocalTime'      | '{"array":[{"paramLocalTime":null}]}'      | 'array[0].paramLocalTime: must not be null'      |
      | 'PUT'    | '/paramLocalTime'      | '{"array":[{"paramLocalTime":null}]}'      | 'array[0].paramLocalTime: must not be null'      |
      | 'PATCH'  | '/paramLocalTime'      | '{"array":[{"paramLocalTime":null}]}'      | 'array[0].paramLocalTime: must not be null'      |
      | 'DELETE' | '/paramLocalTime'      | '{"array":[{"paramLocalTime":null}]}'      | 'array[0].paramLocalTime: must not be null'      |
      | 'POST'   | '/paramYear'           | '{"array":[{"paramYear":null}]}'           | 'array[0].paramYear: must not be null'           |
      | 'PUT'    | '/paramYear'           | '{"array":[{"paramYear":null}]}'           | 'array[0].paramYear: must not be null'           |
      | 'PATCH'  | '/paramYear'           | '{"array":[{"paramYear":null}]}'           | 'array[0].paramYear: must not be null'           |
      | 'DELETE' | '/paramYear'           | '{"array":[{"paramYear":null}]}'           | 'array[0].paramYear: must not be null'           |
      | 'POST'   | '/paramEnumAB'         | '{"array":[{"paramEnumAB":null}]}'         | 'array[0].paramEnumAB: must not be null'         |
      | 'PUT'    | '/paramEnumAB'         | '{"array":[{"paramEnumAB":null}]}'         | 'array[0].paramEnumAB: must not be null'         |
      | 'PATCH'  | '/paramEnumAB'         | '{"array":[{"paramEnumAB":null}]}'         | 'array[0].paramEnumAB: must not be null'         |
      | 'DELETE' | '/paramEnumAB'         | '{"array":[{"paramEnumAB":null}]}'         | 'array[0].paramEnumAB: must not be null'         |
      | 'POST'   | '/paramEnumLowerAB'    | '{"array":[{"paramEnumLowerAB":null}]}'    | 'array[0].paramEnumLowerAB: must not be null'    |
      | 'PUT'    | '/paramEnumLowerAB'    | '{"array":[{"paramEnumLowerAB":null}]}'    | 'array[0].paramEnumLowerAB: must not be null'    |
      | 'PATCH'  | '/paramEnumLowerAB'    | '{"array":[{"paramEnumLowerAB":null}]}'    | 'array[0].paramEnumLowerAB: must not be null'    |
      | 'DELETE' | '/paramEnumLowerAB'    | '{"array":[{"paramEnumLowerAB":null}]}'    | 'array[0].paramEnumLowerAB: must not be null'    |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/not-null/named-nested-dto-attribute<path> endpoint with the body <request body> (RequestBodyConstrainedNotNullRestApi.class)
    this endpoint receives the request body-attibute in a nested dto attribute from an array, both having the diferent names, a custom body-attibute name is defined by the endpoint
    and the service maps the sent empty value to null, but service responding with an error message since data is must not be null.

    Given the endpoint '/api/v1/request-body/not-null/named-nested-dto-attribute'<path>
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | request body                                  | message                                          |
      | 'POST'   | '/paramString'         | '{"array":[{"param-string":null}]}'           | 'array[0].paramString: must not be null'         |
      | 'PUT'    | '/paramString'         | '{"array":[{"param-string":null}]}'           | 'array[0].paramString: must not be null'         |
      | 'PATCH'  | '/paramString'         | '{"array":[{"param-string":null}]}'           | 'array[0].paramString: must not be null'         |
      | 'DELETE' | '/paramString'         | '{"array":[{"param-string":null}]}'           | 'array[0].paramString: must not be null'         |
      | 'POST'   | '/paramInteger'        | '{"array":[{"param-integer":null}]}'          | 'array[0].paramInteger: must not be null'        |
      | 'PUT'    | '/paramInteger'        | '{"array":[{"param-integer":null}]}'          | 'array[0].paramInteger: must not be null'        |
      | 'PATCH'  | '/paramInteger'        | '{"array":[{"param-integer":null}]}'          | 'array[0].paramInteger: must not be null'        |
      | 'DELETE' | '/paramInteger'        | '{"array":[{"param-integer":null}]}'          | 'array[0].paramInteger: must not be null'        |
      | 'POST'   | '/paramLong'           | '{"array":[{"param-long":null}]}'             | 'array[0].paramLong: must not be null'           |
      | 'PUT'    | '/paramLong'           | '{"array":[{"param-long":null}]}'             | 'array[0].paramLong: must not be null'           |
      | 'PATCH'  | '/paramLong'           | '{"array":[{"param-long":null}]}'             | 'array[0].paramLong: must not be null'           |
      | 'DELETE' | '/paramLong'           | '{"array":[{"param-long":null}]}'             | 'array[0].paramLong: must not be null'           |
      | 'POST'   | '/paramBigInteger'     | '{"array":[{"param-big-integer":null}]}'      | 'array[0].paramBigInteger: must not be null'     |
      | 'PUT'    | '/paramBigInteger'     | '{"array":[{"param-big-integer":null}]}'      | 'array[0].paramBigInteger: must not be null'     |
      | 'PATCH'  | '/paramBigInteger'     | '{"array":[{"param-big-integer":null}]}'      | 'array[0].paramBigInteger: must not be null'     |
      | 'DELETE' | '/paramBigInteger'     | '{"array":[{"param-big-integer":null}]}'      | 'array[0].paramBigInteger: must not be null'     |
      | 'POST'   | '/paramFloat'          | '{"array":[{"param-float":null}]}'            | 'array[0].paramFloat: must not be null'          |
      | 'PUT'    | '/paramFloat'          | '{"array":[{"param-float":null}]}'            | 'array[0].paramFloat: must not be null'          |
      | 'PATCH'  | '/paramFloat'          | '{"array":[{"param-float":null}]}'            | 'array[0].paramFloat: must not be null'          |
      | 'DELETE' | '/paramFloat'          | '{"array":[{"param-float":null}]}'            | 'array[0].paramFloat: must not be null'          |
      | 'POST'   | '/paramDouble'         | '{"array":[{"param-double":null}]}'           | 'array[0].paramDouble: must not be null'         |
      | 'PUT'    | '/paramDouble'         | '{"array":[{"param-double":null}]}'           | 'array[0].paramDouble: must not be null'         |
      | 'PATCH'  | '/paramDouble'         | '{"array":[{"param-double":null}]}'           | 'array[0].paramDouble: must not be null'         |
      | 'DELETE' | '/paramDouble'         | '{"array":[{"param-double":null}]}'           | 'array[0].paramDouble: must not be null'         |
      | 'POST'   | '/paramBigDecimal'     | '{"array":[{"param-big-decimal":null}]}'      | 'array[0].paramBigDecimal: must not be null'     |
      | 'PUT'    | '/paramBigDecimal'     | '{"array":[{"param-big-decimal":null}]}'      | 'array[0].paramBigDecimal: must not be null'     |
      | 'PATCH'  | '/paramBigDecimal'     | '{"array":[{"param-big-decimal":null}]}'      | 'array[0].paramBigDecimal: must not be null'     |
      | 'DELETE' | '/paramBigDecimal'     | '{"array":[{"param-big-decimal":null}]}'      | 'array[0].paramBigDecimal: must not be null'     |
      | 'POST'   | '/paramBoolean'        | '{"array":[{"param-boolean":null}]}'          | 'array[0].paramBoolean: must not be null'        |
      | 'PUT'    | '/paramBoolean'        | '{"array":[{"param-boolean":null}]}'          | 'array[0].paramBoolean: must not be null'        |
      | 'PATCH'  | '/paramBoolean'        | '{"array":[{"param-boolean":null}]}'          | 'array[0].paramBoolean: must not be null'        |
      | 'DELETE' | '/paramBoolean'        | '{"array":[{"param-boolean":null}]}'          | 'array[0].paramBoolean: must not be null'        |
      | 'POST'   | '/paramOffsetDateTime' | '{"array":[{"param-offset-date-time":null}]}' | 'array[0].paramOffsetDateTime: must not be null' |
      | 'PUT'    | '/paramOffsetDateTime' | '{"array":[{"param-offset-date-time":null}]}' | 'array[0].paramOffsetDateTime: must not be null' |
      | 'PATCH'  | '/paramOffsetDateTime' | '{"array":[{"param-offset-date-time":null}]}' | 'array[0].paramOffsetDateTime: must not be null' |
      | 'DELETE' | '/paramOffsetDateTime' | '{"array":[{"param-offset-date-time":null}]}' | 'array[0].paramOffsetDateTime: must not be null' |
      | 'POST'   | '/paramLocalDateTime'  | '{"array":[{"param-local-date-time":null}]}'  | 'array[0].paramLocalDateTime: must not be null'  |
      | 'PUT'    | '/paramLocalDateTime'  | '{"array":[{"param-local-date-time":null}]}'  | 'array[0].paramLocalDateTime: must not be null'  |
      | 'PATCH'  | '/paramLocalDateTime'  | '{"array":[{"param-local-date-time":null}]}'  | 'array[0].paramLocalDateTime: must not be null'  |
      | 'DELETE' | '/paramLocalDateTime'  | '{"array":[{"param-local-date-time":null}]}'  | 'array[0].paramLocalDateTime: must not be null'  |
      | 'POST'   | '/paramLocalDate'      | '{"array":[{"param-local-date":null}]}'       | 'array[0].paramLocalDate: must not be null'      |
      | 'PUT'    | '/paramLocalDate'      | '{"array":[{"param-local-date":null}]}'       | 'array[0].paramLocalDate: must not be null'      |
      | 'PATCH'  | '/paramLocalDate'      | '{"array":[{"param-local-date":null}]}'       | 'array[0].paramLocalDate: must not be null'      |
      | 'DELETE' | '/paramLocalDate'      | '{"array":[{"param-local-date":null}]}'       | 'array[0].paramLocalDate: must not be null'      |
      | 'POST'   | '/paramLocalTime'      | '{"array":[{"param-local-time":null}]}'       | 'array[0].paramLocalTime: must not be null'      |
      | 'PUT'    | '/paramLocalTime'      | '{"array":[{"param-local-time":null}]}'       | 'array[0].paramLocalTime: must not be null'      |
      | 'PATCH'  | '/paramLocalTime'      | '{"array":[{"param-local-time":null}]}'       | 'array[0].paramLocalTime: must not be null'      |
      | 'DELETE' | '/paramLocalTime'      | '{"array":[{"param-local-time":null}]}'       | 'array[0].paramLocalTime: must not be null'      |
      | 'POST'   | '/paramYear'           | '{"array":[{"param-year":null}]}'             | 'array[0].paramYear: must not be null'           |
      | 'PUT'    | '/paramYear'           | '{"array":[{"param-year":null}]}'             | 'array[0].paramYear: must not be null'           |
      | 'PATCH'  | '/paramYear'           | '{"array":[{"param-year":null}]}'             | 'array[0].paramYear: must not be null'           |
      | 'DELETE' | '/paramYear'           | '{"array":[{"param-year":null}]}'             | 'array[0].paramYear: must not be null'           |
      | 'POST'   | '/paramEnumAB'         | '{"array":[{"param-enum-ab":null}]}'          | 'array[0].paramEnumAB: must not be null'         |
      | 'PUT'    | '/paramEnumAB'         | '{"array":[{"param-enum-ab":null}]}'          | 'array[0].paramEnumAB: must not be null'         |
      | 'PATCH'  | '/paramEnumAB'         | '{"array":[{"param-enum-ab":null}]}'          | 'array[0].paramEnumAB: must not be null'         |
      | 'DELETE' | '/paramEnumAB'         | '{"array":[{"param-enum-ab":null}]}'          | 'array[0].paramEnumAB: must not be null'         |
      | 'POST'   | '/paramEnumLowerAB'    | '{"array":[{"param-enum-lower-ab":null}]}'    | 'array[0].paramEnumLowerAB: must not be null'    |
      | 'PUT'    | '/paramEnumLowerAB'    | '{"array":[{"param-enum-lower-ab":null}]}'    | 'array[0].paramEnumLowerAB: must not be null'    |
      | 'PATCH'  | '/paramEnumLowerAB'    | '{"array":[{"param-enum-lower-ab":null}]}'    | 'array[0].paramEnumLowerAB: must not be null'    |
      | 'DELETE' | '/paramEnumLowerAB'    | '{"array":[{"param-enum-lower-ab":null}]}'    | 'array[0].paramEnumLowerAB: must not be null'    |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/not-null/nested-optional-dto-attribute<path> endpoint with the body <request body> (RequestBodyConstrainedNotNullRestApi.class)
    this endpoint receives the request body-attibute in a nested optional dto attribute from an array, both having the same name
    and the service maps the sent empty value to null, but service responding with an error message since data is must not be null.

    Given the endpoint '/api/v1/request-body/not-null/nested-optional-dto-attribute'<path>
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | request body                               | message                                          |
      | 'POST'   | '/paramString'         | '{"array":[{"paramString":null}]}'         | 'array[0].paramString: must not be null'         |
      | 'PUT'    | '/paramString'         | '{"array":[{"paramString":null}]}'         | 'array[0].paramString: must not be null'         |
      | 'PATCH'  | '/paramString'         | '{"array":[{"paramString":null}]}'         | 'array[0].paramString: must not be null'         |
      | 'DELETE' | '/paramString'         | '{"array":[{"paramString":null}]}'         | 'array[0].paramString: must not be null'         |
      | 'POST'   | '/paramInteger'        | '{"array":[{"paramInteger":null}]}'        | 'array[0].paramInteger: must not be null'        |
      | 'PUT'    | '/paramInteger'        | '{"array":[{"paramInteger":null}]}'        | 'array[0].paramInteger: must not be null'        |
      | 'PATCH'  | '/paramInteger'        | '{"array":[{"paramInteger":null}]}'        | 'array[0].paramInteger: must not be null'        |
      | 'DELETE' | '/paramInteger'        | '{"array":[{"paramInteger":null}]}'        | 'array[0].paramInteger: must not be null'        |
      | 'POST'   | '/paramLong'           | '{"array":[{"paramLong":null}]}'           | 'array[0].paramLong: must not be null'           |
      | 'PUT'    | '/paramLong'           | '{"array":[{"paramLong":null}]}'           | 'array[0].paramLong: must not be null'           |
      | 'PATCH'  | '/paramLong'           | '{"array":[{"paramLong":null}]}'           | 'array[0].paramLong: must not be null'           |
      | 'DELETE' | '/paramLong'           | '{"array":[{"paramLong":null}]}'           | 'array[0].paramLong: must not be null'           |
      | 'POST'   | '/paramBigInteger'     | '{"array":[{"paramBigInteger":null}]}'     | 'array[0].paramBigInteger: must not be null'     |
      | 'PUT'    | '/paramBigInteger'     | '{"array":[{"paramBigInteger":null}]}'     | 'array[0].paramBigInteger: must not be null'     |
      | 'PATCH'  | '/paramBigInteger'     | '{"array":[{"paramBigInteger":null}]}'     | 'array[0].paramBigInteger: must not be null'     |
      | 'DELETE' | '/paramBigInteger'     | '{"array":[{"paramBigInteger":null}]}'     | 'array[0].paramBigInteger: must not be null'     |
      | 'POST'   | '/paramFloat'          | '{"array":[{"paramFloat":null}]}'          | 'array[0].paramFloat: must not be null'          |
      | 'PUT'    | '/paramFloat'          | '{"array":[{"paramFloat":null}]}'          | 'array[0].paramFloat: must not be null'          |
      | 'PATCH'  | '/paramFloat'          | '{"array":[{"paramFloat":null}]}'          | 'array[0].paramFloat: must not be null'          |
      | 'DELETE' | '/paramFloat'          | '{"array":[{"paramFloat":null}]}'          | 'array[0].paramFloat: must not be null'          |
      | 'POST'   | '/paramDouble'         | '{"array":[{"paramDouble":null}]}'         | 'array[0].paramDouble: must not be null'         |
      | 'PUT'    | '/paramDouble'         | '{"array":[{"paramDouble":null}]}'         | 'array[0].paramDouble: must not be null'         |
      | 'PATCH'  | '/paramDouble'         | '{"array":[{"paramDouble":null}]}'         | 'array[0].paramDouble: must not be null'         |
      | 'DELETE' | '/paramDouble'         | '{"array":[{"paramDouble":null}]}'         | 'array[0].paramDouble: must not be null'         |
      | 'POST'   | '/paramBigDecimal'     | '{"array":[{"paramBigDecimal":null}]}'     | 'array[0].paramBigDecimal: must not be null'     |
      | 'PUT'    | '/paramBigDecimal'     | '{"array":[{"paramBigDecimal":null}]}'     | 'array[0].paramBigDecimal: must not be null'     |
      | 'PATCH'  | '/paramBigDecimal'     | '{"array":[{"paramBigDecimal":null}]}'     | 'array[0].paramBigDecimal: must not be null'     |
      | 'DELETE' | '/paramBigDecimal'     | '{"array":[{"paramBigDecimal":null}]}'     | 'array[0].paramBigDecimal: must not be null'     |
      | 'POST'   | '/paramBoolean'        | '{"array":[{"paramBoolean":null}]}'        | 'array[0].paramBoolean: must not be null'        |
      | 'PUT'    | '/paramBoolean'        | '{"array":[{"paramBoolean":null}]}'        | 'array[0].paramBoolean: must not be null'        |
      | 'PATCH'  | '/paramBoolean'        | '{"array":[{"paramBoolean":null}]}'        | 'array[0].paramBoolean: must not be null'        |
      | 'DELETE' | '/paramBoolean'        | '{"array":[{"paramBoolean":null}]}'        | 'array[0].paramBoolean: must not be null'        |
      | 'POST'   | '/paramOffsetDateTime' | '{"array":[{"paramOffsetDateTime":null}]}' | 'array[0].paramOffsetDateTime: must not be null' |
      | 'PUT'    | '/paramOffsetDateTime' | '{"array":[{"paramOffsetDateTime":null}]}' | 'array[0].paramOffsetDateTime: must not be null' |
      | 'PATCH'  | '/paramOffsetDateTime' | '{"array":[{"paramOffsetDateTime":null}]}' | 'array[0].paramOffsetDateTime: must not be null' |
      | 'DELETE' | '/paramOffsetDateTime' | '{"array":[{"paramOffsetDateTime":null}]}' | 'array[0].paramOffsetDateTime: must not be null' |
      | 'POST'   | '/paramLocalDateTime'  | '{"array":[{"paramLocalDateTime":null}]}'  | 'array[0].paramLocalDateTime: must not be null'  |
      | 'PUT'    | '/paramLocalDateTime'  | '{"array":[{"paramLocalDateTime":null}]}'  | 'array[0].paramLocalDateTime: must not be null'  |
      | 'PATCH'  | '/paramLocalDateTime'  | '{"array":[{"paramLocalDateTime":null}]}'  | 'array[0].paramLocalDateTime: must not be null'  |
      | 'DELETE' | '/paramLocalDateTime'  | '{"array":[{"paramLocalDateTime":null}]}'  | 'array[0].paramLocalDateTime: must not be null'  |
      | 'POST'   | '/paramLocalDate'      | '{"array":[{"paramLocalDate":null}]}'      | 'array[0].paramLocalDate: must not be null'      |
      | 'PUT'    | '/paramLocalDate'      | '{"array":[{"paramLocalDate":null}]}'      | 'array[0].paramLocalDate: must not be null'      |
      | 'PATCH'  | '/paramLocalDate'      | '{"array":[{"paramLocalDate":null}]}'      | 'array[0].paramLocalDate: must not be null'      |
      | 'DELETE' | '/paramLocalDate'      | '{"array":[{"paramLocalDate":null}]}'      | 'array[0].paramLocalDate: must not be null'      |
      | 'POST'   | '/paramLocalTime'      | '{"array":[{"paramLocalTime":null}]}'      | 'array[0].paramLocalTime: must not be null'      |
      | 'PUT'    | '/paramLocalTime'      | '{"array":[{"paramLocalTime":null}]}'      | 'array[0].paramLocalTime: must not be null'      |
      | 'PATCH'  | '/paramLocalTime'      | '{"array":[{"paramLocalTime":null}]}'      | 'array[0].paramLocalTime: must not be null'      |
      | 'DELETE' | '/paramLocalTime'      | '{"array":[{"paramLocalTime":null}]}'      | 'array[0].paramLocalTime: must not be null'      |
      | 'POST'   | '/paramYear'           | '{"array":[{"paramYear":null}]}'           | 'array[0].paramYear: must not be null'           |
      | 'PUT'    | '/paramYear'           | '{"array":[{"paramYear":null}]}'           | 'array[0].paramYear: must not be null'           |
      | 'PATCH'  | '/paramYear'           | '{"array":[{"paramYear":null}]}'           | 'array[0].paramYear: must not be null'           |
      | 'DELETE' | '/paramYear'           | '{"array":[{"paramYear":null}]}'           | 'array[0].paramYear: must not be null'           |
      | 'POST'   | '/paramEnumAB'         | '{"array":[{"paramEnumAB":null}]}'         | 'array[0].paramEnumAB: must not be null'         |
      | 'PUT'    | '/paramEnumAB'         | '{"array":[{"paramEnumAB":null}]}'         | 'array[0].paramEnumAB: must not be null'         |
      | 'PATCH'  | '/paramEnumAB'         | '{"array":[{"paramEnumAB":null}]}'         | 'array[0].paramEnumAB: must not be null'         |
      | 'DELETE' | '/paramEnumAB'         | '{"array":[{"paramEnumAB":null}]}'         | 'array[0].paramEnumAB: must not be null'         |
      | 'POST'   | '/paramEnumLowerAB'    | '{"array":[{"paramEnumLowerAB":null}]}'    | 'array[0].paramEnumLowerAB: must not be null'    |
      | 'PUT'    | '/paramEnumLowerAB'    | '{"array":[{"paramEnumLowerAB":null}]}'    | 'array[0].paramEnumLowerAB: must not be null'    |
      | 'PATCH'  | '/paramEnumLowerAB'    | '{"array":[{"paramEnumLowerAB":null}]}'    | 'array[0].paramEnumLowerAB: must not be null'    |
      | 'DELETE' | '/paramEnumLowerAB'    | '{"array":[{"paramEnumLowerAB":null}]}'    | 'array[0].paramEnumLowerAB: must not be null'    |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/not-null/named-nested-optional-dto-attribute<path> endpoint with the body <request body> (RequestBodyConstrainedNotNullRestApi.class)
    this endpoint receives the request body-attibute in a nested optional dto attribute from an array, both having the diferent names, a custom body-attibute name is defined by the endpoint
    and the service maps the sent empty value to null, but service responding with an error message since data is must not be null.

    Given the endpoint '/api/v1/request-body/not-null/named-nested-optional-dto-attribute'<path>
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | request body                                  | message                                          |
      | 'POST'   | '/paramString'         | '{"array":[{"param-string":null}]}'           | 'array[0].paramString: must not be null'         |
      | 'PUT'    | '/paramString'         | '{"array":[{"param-string":null}]}'           | 'array[0].paramString: must not be null'         |
      | 'PATCH'  | '/paramString'         | '{"array":[{"param-string":null}]}'           | 'array[0].paramString: must not be null'         |
      | 'DELETE' | '/paramString'         | '{"array":[{"param-string":null}]}'           | 'array[0].paramString: must not be null'         |
      | 'POST'   | '/paramInteger'        | '{"array":[{"param-integer":null}]}'          | 'array[0].paramInteger: must not be null'        |
      | 'PUT'    | '/paramInteger'        | '{"array":[{"param-integer":null}]}'          | 'array[0].paramInteger: must not be null'        |
      | 'PATCH'  | '/paramInteger'        | '{"array":[{"param-integer":null}]}'          | 'array[0].paramInteger: must not be null'        |
      | 'DELETE' | '/paramInteger'        | '{"array":[{"param-integer":null}]}'          | 'array[0].paramInteger: must not be null'        |
      | 'POST'   | '/paramLong'           | '{"array":[{"param-long":null}]}'             | 'array[0].paramLong: must not be null'           |
      | 'PUT'    | '/paramLong'           | '{"array":[{"param-long":null}]}'             | 'array[0].paramLong: must not be null'           |
      | 'PATCH'  | '/paramLong'           | '{"array":[{"param-long":null}]}'             | 'array[0].paramLong: must not be null'           |
      | 'DELETE' | '/paramLong'           | '{"array":[{"param-long":null}]}'             | 'array[0].paramLong: must not be null'           |
      | 'POST'   | '/paramBigInteger'     | '{"array":[{"param-big-integer":null}]}'      | 'array[0].paramBigInteger: must not be null'     |
      | 'PUT'    | '/paramBigInteger'     | '{"array":[{"param-big-integer":null}]}'      | 'array[0].paramBigInteger: must not be null'     |
      | 'PATCH'  | '/paramBigInteger'     | '{"array":[{"param-big-integer":null}]}'      | 'array[0].paramBigInteger: must not be null'     |
      | 'DELETE' | '/paramBigInteger'     | '{"array":[{"param-big-integer":null}]}'      | 'array[0].paramBigInteger: must not be null'     |
      | 'POST'   | '/paramFloat'          | '{"array":[{"param-float":null}]}'            | 'array[0].paramFloat: must not be null'          |
      | 'PUT'    | '/paramFloat'          | '{"array":[{"param-float":null}]}'            | 'array[0].paramFloat: must not be null'          |
      | 'PATCH'  | '/paramFloat'          | '{"array":[{"param-float":null}]}'            | 'array[0].paramFloat: must not be null'          |
      | 'DELETE' | '/paramFloat'          | '{"array":[{"param-float":null}]}'            | 'array[0].paramFloat: must not be null'          |
      | 'POST'   | '/paramDouble'         | '{"array":[{"param-double":null}]}'           | 'array[0].paramDouble: must not be null'         |
      | 'PUT'    | '/paramDouble'         | '{"array":[{"param-double":null}]}'           | 'array[0].paramDouble: must not be null'         |
      | 'PATCH'  | '/paramDouble'         | '{"array":[{"param-double":null}]}'           | 'array[0].paramDouble: must not be null'         |
      | 'DELETE' | '/paramDouble'         | '{"array":[{"param-double":null}]}'           | 'array[0].paramDouble: must not be null'         |
      | 'POST'   | '/paramBigDecimal'     | '{"array":[{"param-big-decimal":null}]}'      | 'array[0].paramBigDecimal: must not be null'     |
      | 'PUT'    | '/paramBigDecimal'     | '{"array":[{"param-big-decimal":null}]}'      | 'array[0].paramBigDecimal: must not be null'     |
      | 'PATCH'  | '/paramBigDecimal'     | '{"array":[{"param-big-decimal":null}]}'      | 'array[0].paramBigDecimal: must not be null'     |
      | 'DELETE' | '/paramBigDecimal'     | '{"array":[{"param-big-decimal":null}]}'      | 'array[0].paramBigDecimal: must not be null'     |
      | 'POST'   | '/paramBoolean'        | '{"array":[{"param-boolean":null}]}'          | 'array[0].paramBoolean: must not be null'        |
      | 'PUT'    | '/paramBoolean'        | '{"array":[{"param-boolean":null}]}'          | 'array[0].paramBoolean: must not be null'        |
      | 'PATCH'  | '/paramBoolean'        | '{"array":[{"param-boolean":null}]}'          | 'array[0].paramBoolean: must not be null'        |
      | 'DELETE' | '/paramBoolean'        | '{"array":[{"param-boolean":null}]}'          | 'array[0].paramBoolean: must not be null'        |
      | 'POST'   | '/paramOffsetDateTime' | '{"array":[{"param-offset-date-time":null}]}' | 'array[0].paramOffsetDateTime: must not be null' |
      | 'PUT'    | '/paramOffsetDateTime' | '{"array":[{"param-offset-date-time":null}]}' | 'array[0].paramOffsetDateTime: must not be null' |
      | 'PATCH'  | '/paramOffsetDateTime' | '{"array":[{"param-offset-date-time":null}]}' | 'array[0].paramOffsetDateTime: must not be null' |
      | 'DELETE' | '/paramOffsetDateTime' | '{"array":[{"param-offset-date-time":null}]}' | 'array[0].paramOffsetDateTime: must not be null' |
      | 'POST'   | '/paramLocalDateTime'  | '{"array":[{"param-local-date-time":null}]}'  | 'array[0].paramLocalDateTime: must not be null'  |
      | 'PUT'    | '/paramLocalDateTime'  | '{"array":[{"param-local-date-time":null}]}'  | 'array[0].paramLocalDateTime: must not be null'  |
      | 'PATCH'  | '/paramLocalDateTime'  | '{"array":[{"param-local-date-time":null}]}'  | 'array[0].paramLocalDateTime: must not be null'  |
      | 'DELETE' | '/paramLocalDateTime'  | '{"array":[{"param-local-date-time":null}]}'  | 'array[0].paramLocalDateTime: must not be null'  |
      | 'POST'   | '/paramLocalDate'      | '{"array":[{"param-local-date":null}]}'       | 'array[0].paramLocalDate: must not be null'      |
      | 'PUT'    | '/paramLocalDate'      | '{"array":[{"param-local-date":null}]}'       | 'array[0].paramLocalDate: must not be null'      |
      | 'PATCH'  | '/paramLocalDate'      | '{"array":[{"param-local-date":null}]}'       | 'array[0].paramLocalDate: must not be null'      |
      | 'DELETE' | '/paramLocalDate'      | '{"array":[{"param-local-date":null}]}'       | 'array[0].paramLocalDate: must not be null'      |
      | 'POST'   | '/paramLocalTime'      | '{"array":[{"param-local-time":null}]}'       | 'array[0].paramLocalTime: must not be null'      |
      | 'PUT'    | '/paramLocalTime'      | '{"array":[{"param-local-time":null}]}'       | 'array[0].paramLocalTime: must not be null'      |
      | 'PATCH'  | '/paramLocalTime'      | '{"array":[{"param-local-time":null}]}'       | 'array[0].paramLocalTime: must not be null'      |
      | 'DELETE' | '/paramLocalTime'      | '{"array":[{"param-local-time":null}]}'       | 'array[0].paramLocalTime: must not be null'      |
      | 'POST'   | '/paramYear'           | '{"array":[{"param-year":null}]}'             | 'array[0].paramYear: must not be null'           |
      | 'PUT'    | '/paramYear'           | '{"array":[{"param-year":null}]}'             | 'array[0].paramYear: must not be null'           |
      | 'PATCH'  | '/paramYear'           | '{"array":[{"param-year":null}]}'             | 'array[0].paramYear: must not be null'           |
      | 'DELETE' | '/paramYear'           | '{"array":[{"param-year":null}]}'             | 'array[0].paramYear: must not be null'           |
      | 'POST'   | '/paramEnumAB'         | '{"array":[{"param-enum-ab":null}]}'          | 'array[0].paramEnumAB: must not be null'         |
      | 'PUT'    | '/paramEnumAB'         | '{"array":[{"param-enum-ab":null}]}'          | 'array[0].paramEnumAB: must not be null'         |
      | 'PATCH'  | '/paramEnumAB'         | '{"array":[{"param-enum-ab":null}]}'          | 'array[0].paramEnumAB: must not be null'         |
      | 'DELETE' | '/paramEnumAB'         | '{"array":[{"param-enum-ab":null}]}'          | 'array[0].paramEnumAB: must not be null'         |
      | 'POST'   | '/paramEnumLowerAB'    | '{"array":[{"param-enum-lower-ab":null}]}'    | 'array[0].paramEnumLowerAB: must not be null'    |
      | 'PUT'    | '/paramEnumLowerAB'    | '{"array":[{"param-enum-lower-ab":null}]}'    | 'array[0].paramEnumLowerAB: must not be null'    |
      | 'PATCH'  | '/paramEnumLowerAB'    | '{"array":[{"param-enum-lower-ab":null}]}'    | 'array[0].paramEnumLowerAB: must not be null'    |
      | 'DELETE' | '/paramEnumLowerAB'    | '{"array":[{"param-enum-lower-ab":null}]}'    | 'array[0].paramEnumLowerAB: must not be null'    |
