Feature: Request body with valid parameter value
  
  The REST service receives data via the request body and successfully interprets the data for the declared data type. The service responds with a success message.
  
   Rule: The conversion between data types is given by the following table
  
  | parameter type | java type      |
  |----------------|----------------|
  | string         | String         |
  | number         | Integer        |
  | number         | Long           |
  | number         | BigInteger     |
  | number         | Float          |
  | number         | Double         |
  | number         | BigDecimal     |
  | boolean        | Boolean        |
  | ISO-8601       | OffsetDateTime |
  | ISO-8601       | LocalDateTime  |
  | ISO-8601       | LocalDate      |
  | ISO-8601       | LocalTime      |
  | ISO-8601       | Year           |
  | string         | ENUM           |

  Background: 
    Given the default timezone 'America/Puerto_Rico'

  Scenario Outline: The <method> call is made to the /api/v1/request-body/dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in a dto attribute, both having the same name
    and the service correctly maps the sent value with the declared data type, responding with a success message.

    Given the endpoint '/api/v1/request-body/dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | request body                                     | received value                                   |
      | 'POST'   | '{"paramString":"id"}'                           | '{"paramString":"id"}'                           |
      | 'PUT'    | '{"paramString":"id"}'                           | '{"paramString":"id"}'                           |
      | 'PATCH'  | '{"paramString":"id"}'                           | '{"paramString":"id"}'                           |
      | 'DELETE' | '{"paramString":"id"}'                           | '{"paramString":"id"}'                           |
      | 'POST'   | '{"paramInteger":2}'                             | '{"paramInteger":2}'                             |
      | 'PUT'    | '{"paramInteger":2}'                             | '{"paramInteger":2}'                             |
      | 'PATCH'  | '{"paramInteger":2}'                             | '{"paramInteger":2}'                             |
      | 'DELETE' | '{"paramInteger":2}'                             | '{"paramInteger":2}'                             |
      | 'POST'   | '{"paramLong":2}'                                | '{"paramLong":2}'                                |
      | 'PUT'    | '{"paramLong":2}'                                | '{"paramLong":2}'                                |
      | 'PATCH'  | '{"paramLong":2}'                                | '{"paramLong":2}'                                |
      | 'DELETE' | '{"paramLong":2}'                                | '{"paramLong":2}'                                |
      | 'POST'   | '{"paramBigInteger":2}'                          | '{"paramBigInteger":2}'                          |
      | 'PUT'    | '{"paramBigInteger":2}'                          | '{"paramBigInteger":2}'                          |
      | 'PATCH'  | '{"paramBigInteger":2}'                          | '{"paramBigInteger":2}'                          |
      | 'DELETE' | '{"paramBigInteger":2}'                          | '{"paramBigInteger":2}'                          |
      | 'POST'   | '{"paramFloat":2.1}'                             | '{"paramFloat":2.1}'                             |
      | 'PUT'    | '{"paramFloat":2.1}'                             | '{"paramFloat":2.1}'                             |
      | 'PATCH'  | '{"paramFloat":2.1}'                             | '{"paramFloat":2.1}'                             |
      | 'DELETE' | '{"paramFloat":2.1}'                             | '{"paramFloat":2.1}'                             |
      | 'POST'   | '{"paramDouble":2.1}'                            | '{"paramDouble":2.1}'                            |
      | 'PUT'    | '{"paramDouble":2.1}'                            | '{"paramDouble":2.1}'                            |
      | 'PATCH'  | '{"paramDouble":2.1}'                            | '{"paramDouble":2.1}'                            |
      | 'DELETE' | '{"paramDouble":2.1}'                            | '{"paramDouble":2.1}'                            |
      | 'POST'   | '{"paramBigDecimal":2.1}'                        | '{"paramBigDecimal":2.1}'                        |
      | 'PUT'    | '{"paramBigDecimal":2.1}'                        | '{"paramBigDecimal":2.1}'                        |
      | 'PATCH'  | '{"paramBigDecimal":2.1}'                        | '{"paramBigDecimal":2.1}'                        |
      | 'DELETE' | '{"paramBigDecimal":2.1}'                        | '{"paramBigDecimal":2.1}'                        |
      | 'POST'   | '{"paramBoolean":true}'                          | '{"paramBoolean":true}'                          |
      | 'PUT'    | '{"paramBoolean":true}'                          | '{"paramBoolean":true}'                          |
      | 'PATCH'  | '{"paramBoolean":true}'                          | '{"paramBoolean":true}'                          |
      | 'DELETE' | '{"paramBoolean":true}'                          | '{"paramBoolean":true}'                          |
      | 'POST'   | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'PUT'    | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'PATCH'  | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'DELETE' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'POST'   | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'PUT'    | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'PATCH'  | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'DELETE' | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'POST'   | '{"paramLocalDate":"2000-01-01"}'                | '{"paramLocalDate":"2000-01-01"}'                |
      | 'PUT'    | '{"paramLocalDate":"2000-01-01"}'                | '{"paramLocalDate":"2000-01-01"}'                |
      | 'PATCH'  | '{"paramLocalDate":"2000-01-01"}'                | '{"paramLocalDate":"2000-01-01"}'                |
      | 'DELETE' | '{"paramLocalDate":"2000-01-01"}'                | '{"paramLocalDate":"2000-01-01"}'                |
      | 'POST'   | '{"paramLocalTime":"13:00:00"}'                  | '{"paramLocalTime":"13:00:00"}'                  |
      | 'PUT'    | '{"paramLocalTime":"13:00:00"}'                  | '{"paramLocalTime":"13:00:00"}'                  |
      | 'PATCH'  | '{"paramLocalTime":"13:00:00"}'                  | '{"paramLocalTime":"13:00:00"}'                  |
      | 'DELETE' | '{"paramLocalTime":"13:00:00"}'                  | '{"paramLocalTime":"13:00:00"}'                  |
      | 'POST'   | '{"paramYear":"2000"}'                           | '{"paramYear":"2000"}'                           |
      | 'PUT'    | '{"paramYear":"2000"}'                           | '{"paramYear":"2000"}'                           |
      | 'PATCH'  | '{"paramYear":"2000"}'                           | '{"paramYear":"2000"}'                           |
      | 'DELETE' | '{"paramYear":"2000"}'                           | '{"paramYear":"2000"}'                           |
      | 'POST'   | '{"paramEnumAB":"A"}'                            | '{"paramEnumAB":"A"}'                            |
      | 'PUT'    | '{"paramEnumAB":"A"}'                            | '{"paramEnumAB":"A"}'                            |
      | 'PATCH'  | '{"paramEnumAB":"A"}'                            | '{"paramEnumAB":"A"}'                            |
      | 'DELETE' | '{"paramEnumAB":"A"}'                            | '{"paramEnumAB":"A"}'                            |
      | 'POST'   | '{"paramEnumLowerAB":"b"}'                       | '{"paramEnumLowerAB":"b"}'                       |
      | 'PUT'    | '{"paramEnumLowerAB":"b"}'                       | '{"paramEnumLowerAB":"b"}'                       |
      | 'PATCH'  | '{"paramEnumLowerAB":"b"}'                       | '{"paramEnumLowerAB":"b"}'                       |
      | 'DELETE' | '{"paramEnumLowerAB":"b"}'                       | '{"paramEnumLowerAB":"b"}'                       |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/named-dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in a dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint 
    and the service correctly maps the sent value with the declared data type, responding with a success message.

    Given the endpoint '/api/v1/request-body/named-dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | request body                                        | received value                                   |
      | 'POST'   | '{"param-string":"id"}'                             | '{"paramString":"id"}'                           |
      | 'PUT'    | '{"param-string":"id"}'                             | '{"paramString":"id"}'                           |
      | 'PATCH'  | '{"param-string":"id"}'                             | '{"paramString":"id"}'                           |
      | 'DELETE' | '{"param-string":"id"}'                             | '{"paramString":"id"}'                           |
      | 'POST'   | '{"param-integer":2}'                               | '{"paramInteger":2}'                             |
      | 'PUT'    | '{"param-integer":2}'                               | '{"paramInteger":2}'                             |
      | 'PATCH'  | '{"param-integer":2}'                               | '{"paramInteger":2}'                             |
      | 'DELETE' | '{"param-integer":2}'                               | '{"paramInteger":2}'                             |
      | 'POST'   | '{"param-long":2}'                                  | '{"paramLong":2}'                                |
      | 'PUT'    | '{"param-long":2}'                                  | '{"paramLong":2}'                                |
      | 'PATCH'  | '{"param-long":2}'                                  | '{"paramLong":2}'                                |
      | 'DELETE' | '{"param-long":2}'                                  | '{"paramLong":2}'                                |
      | 'POST'   | '{"param-big-integer":2}'                           | '{"paramBigInteger":2}'                          |
      | 'PUT'    | '{"param-big-integer":2}'                           | '{"paramBigInteger":2}'                          |
      | 'PATCH'  | '{"param-big-integer":2}'                           | '{"paramBigInteger":2}'                          |
      | 'DELETE' | '{"param-big-integer":2}'                           | '{"paramBigInteger":2}'                          |
      | 'POST'   | '{"param-float":2.1}'                               | '{"paramFloat":2.1}'                             |
      | 'PUT'    | '{"param-float":2.1}'                               | '{"paramFloat":2.1}'                             |
      | 'PATCH'  | '{"param-float":2.1}'                               | '{"paramFloat":2.1}'                             |
      | 'DELETE' | '{"param-float":2.1}'                               | '{"paramFloat":2.1}'                             |
      | 'POST'   | '{"param-double":2.1}'                              | '{"paramDouble":2.1}'                            |
      | 'PUT'    | '{"param-double":2.1}'                              | '{"paramDouble":2.1}'                            |
      | 'PATCH'  | '{"param-double":2.1}'                              | '{"paramDouble":2.1}'                            |
      | 'DELETE' | '{"param-double":2.1}'                              | '{"paramDouble":2.1}'                            |
      | 'POST'   | '{"param-big-decimal":2.1}'                         | '{"paramBigDecimal":2.1}'                        |
      | 'PUT'    | '{"param-big-decimal":2.1}'                         | '{"paramBigDecimal":2.1}'                        |
      | 'PATCH'  | '{"param-big-decimal":2.1}'                         | '{"paramBigDecimal":2.1}'                        |
      | 'DELETE' | '{"param-big-decimal":2.1}'                         | '{"paramBigDecimal":2.1}'                        |
      | 'POST'   | '{"param-boolean":true}'                            | '{"paramBoolean":true}'                          |
      | 'PUT'    | '{"param-boolean":true}'                            | '{"paramBoolean":true}'                          |
      | 'PATCH'  | '{"param-boolean":true}'                            | '{"paramBoolean":true}'                          |
      | 'DELETE' | '{"param-boolean":true}'                            | '{"paramBoolean":true}'                          |
      | 'POST'   | '{"param-offset-date-time":"2000-01-01T00:00:00Z"}' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'PUT'    | '{"param-offset-date-time":"2000-01-01T00:00:00Z"}' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'PATCH'  | '{"param-offset-date-time":"2000-01-01T00:00:00Z"}' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'DELETE' | '{"param-offset-date-time":"2000-01-01T00:00:00Z"}' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'POST'   | '{"param-local-date-time":"2000-01-01T00:00:00Z"}'  | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'PUT'    | '{"param-local-date-time":"2000-01-01T00:00:00Z"}'  | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'PATCH'  | '{"param-local-date-time":"2000-01-01T00:00:00Z"}'  | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'DELETE' | '{"param-local-date-time":"2000-01-01T00:00:00Z"}'  | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'POST'   | '{"param-local-date":"2000-01-01"}'                 | '{"paramLocalDate":"2000-01-01"}'                |
      | 'PUT'    | '{"param-local-date":"2000-01-01"}'                 | '{"paramLocalDate":"2000-01-01"}'                |
      | 'PATCH'  | '{"param-local-date":"2000-01-01"}'                 | '{"paramLocalDate":"2000-01-01"}'                |
      | 'DELETE' | '{"param-local-date":"2000-01-01"}'                 | '{"paramLocalDate":"2000-01-01"}'                |
      | 'POST'   | '{"param-local-time":"13:00:00"}'                   | '{"paramLocalTime":"13:00:00"}'                  |
      | 'PUT'    | '{"param-local-time":"13:00:00"}'                   | '{"paramLocalTime":"13:00:00"}'                  |
      | 'PATCH'  | '{"param-local-time":"13:00:00"}'                   | '{"paramLocalTime":"13:00:00"}'                  |
      | 'DELETE' | '{"param-local-time":"13:00:00"}'                   | '{"paramLocalTime":"13:00:00"}'                  |
      | 'POST'   | '{"param-year":"2000"}'                             | '{"paramYear":"2000"}'                           |
      | 'PUT'    | '{"param-year":"2000"}'                             | '{"paramYear":"2000"}'                           |
      | 'PATCH'  | '{"param-year":"2000"}'                             | '{"paramYear":"2000"}'                           |
      | 'DELETE' | '{"param-year":"2000"}'                             | '{"paramYear":"2000"}'                           |
      | 'POST'   | '{"param-enum-ab":"A"}'                             | '{"paramEnumAB":"A"}'                            |
      | 'PUT'    | '{"param-enum-ab":"A"}'                             | '{"paramEnumAB":"A"}'                            |
      | 'PATCH'  | '{"param-enum-ab":"A"}'                             | '{"paramEnumAB":"A"}'                            |
      | 'DELETE' | '{"param-enum-ab":"A"}'                             | '{"paramEnumAB":"A"}'                            |
      | 'POST'   | '{"param-enum-lower-ab":"b"}'                       | '{"paramEnumLowerAB":"b"}'                       |
      | 'PUT'    | '{"param-enum-lower-ab":"b"}'                       | '{"paramEnumLowerAB":"b"}'                       |
      | 'PATCH'  | '{"param-enum-lower-ab":"b"}'                       | '{"paramEnumLowerAB":"b"}'                       |
      | 'DELETE' | '{"param-enum-lower-ab":"b"}'                       | '{"paramEnumLowerAB":"b"}'                       |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/optional-dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in an optional dto attribute, both having the same name
    and the service correctly maps the sent value with the declared data type, responding with a success message.

    Given the endpoint '/api/v1/request-body/optional-dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | request body                                     | received value                                   |
      | 'POST'   | '{"paramString":"id"}'                           | '{"paramString":"id"}'                           |
      | 'PUT'    | '{"paramString":"id"}'                           | '{"paramString":"id"}'                           |
      | 'PATCH'  | '{"paramString":"id"}'                           | '{"paramString":"id"}'                           |
      | 'DELETE' | '{"paramString":"id"}'                           | '{"paramString":"id"}'                           |
      | 'POST'   | '{"paramInteger":2}'                             | '{"paramInteger":2}'                             |
      | 'PUT'    | '{"paramInteger":2}'                             | '{"paramInteger":2}'                             |
      | 'PATCH'  | '{"paramInteger":2}'                             | '{"paramInteger":2}'                             |
      | 'DELETE' | '{"paramInteger":2}'                             | '{"paramInteger":2}'                             |
      | 'POST'   | '{"paramLong":2}'                                | '{"paramLong":2}'                                |
      | 'PUT'    | '{"paramLong":2}'                                | '{"paramLong":2}'                                |
      | 'PATCH'  | '{"paramLong":2}'                                | '{"paramLong":2}'                                |
      | 'DELETE' | '{"paramLong":2}'                                | '{"paramLong":2}'                                |
      | 'POST'   | '{"paramBigInteger":2}'                          | '{"paramBigInteger":2}'                          |
      | 'PUT'    | '{"paramBigInteger":2}'                          | '{"paramBigInteger":2}'                          |
      | 'PATCH'  | '{"paramBigInteger":2}'                          | '{"paramBigInteger":2}'                          |
      | 'DELETE' | '{"paramBigInteger":2}'                          | '{"paramBigInteger":2}'                          |
      | 'POST'   | '{"paramFloat":2.1}'                             | '{"paramFloat":2.1}'                             |
      | 'PUT'    | '{"paramFloat":2.1}'                             | '{"paramFloat":2.1}'                             |
      | 'PATCH'  | '{"paramFloat":2.1}'                             | '{"paramFloat":2.1}'                             |
      | 'DELETE' | '{"paramFloat":2.1}'                             | '{"paramFloat":2.1}'                             |
      | 'POST'   | '{"paramDouble":2.1}'                            | '{"paramDouble":2.1}'                            |
      | 'PUT'    | '{"paramDouble":2.1}'                            | '{"paramDouble":2.1}'                            |
      | 'PATCH'  | '{"paramDouble":2.1}'                            | '{"paramDouble":2.1}'                            |
      | 'DELETE' | '{"paramDouble":2.1}'                            | '{"paramDouble":2.1}'                            |
      | 'POST'   | '{"paramBigDecimal":2.1}'                        | '{"paramBigDecimal":2.1}'                        |
      | 'PUT'    | '{"paramBigDecimal":2.1}'                        | '{"paramBigDecimal":2.1}'                        |
      | 'PATCH'  | '{"paramBigDecimal":2.1}'                        | '{"paramBigDecimal":2.1}'                        |
      | 'DELETE' | '{"paramBigDecimal":2.1}'                        | '{"paramBigDecimal":2.1}'                        |
      | 'POST'   | '{"paramBoolean":true}'                          | '{"paramBoolean":true}'                          |
      | 'PUT'    | '{"paramBoolean":true}'                          | '{"paramBoolean":true}'                          |
      | 'PATCH'  | '{"paramBoolean":true}'                          | '{"paramBoolean":true}'                          |
      | 'DELETE' | '{"paramBoolean":true}'                          | '{"paramBoolean":true}'                          |
      | 'POST'   | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'PUT'    | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'PATCH'  | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'DELETE' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'POST'   | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'PUT'    | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'PATCH'  | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'DELETE' | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'POST'   | '{"paramLocalDate":"2000-01-01"}'                | '{"paramLocalDate":"2000-01-01"}'                |
      | 'PUT'    | '{"paramLocalDate":"2000-01-01"}'                | '{"paramLocalDate":"2000-01-01"}'                |
      | 'PATCH'  | '{"paramLocalDate":"2000-01-01"}'                | '{"paramLocalDate":"2000-01-01"}'                |
      | 'DELETE' | '{"paramLocalDate":"2000-01-01"}'                | '{"paramLocalDate":"2000-01-01"}'                |
      | 'POST'   | '{"paramLocalTime":"13:00:00"}'                  | '{"paramLocalTime":"13:00:00"}'                  |
      | 'PUT'    | '{"paramLocalTime":"13:00:00"}'                  | '{"paramLocalTime":"13:00:00"}'                  |
      | 'PATCH'  | '{"paramLocalTime":"13:00:00"}'                  | '{"paramLocalTime":"13:00:00"}'                  |
      | 'DELETE' | '{"paramLocalTime":"13:00:00"}'                  | '{"paramLocalTime":"13:00:00"}'                  |
      | 'POST'   | '{"paramYear":"2000"}'                           | '{"paramYear":"2000"}'                           |
      | 'PUT'    | '{"paramYear":"2000"}'                           | '{"paramYear":"2000"}'                           |
      | 'PATCH'  | '{"paramYear":"2000"}'                           | '{"paramYear":"2000"}'                           |
      | 'DELETE' | '{"paramYear":"2000"}'                           | '{"paramYear":"2000"}'                           |
      | 'POST'   | '{"paramEnumAB":"A"}'                            | '{"paramEnumAB":"A"}'                            |
      | 'PUT'    | '{"paramEnumAB":"A"}'                            | '{"paramEnumAB":"A"}'                            |
      | 'PATCH'  | '{"paramEnumAB":"A"}'                            | '{"paramEnumAB":"A"}'                            |
      | 'DELETE' | '{"paramEnumAB":"A"}'                            | '{"paramEnumAB":"A"}'                            |
      | 'POST'   | '{"paramEnumLowerAB":"b"}'                       | '{"paramEnumLowerAB":"b"}'                       |
      | 'PUT'    | '{"paramEnumLowerAB":"b"}'                       | '{"paramEnumLowerAB":"b"}'                       |
      | 'PATCH'  | '{"paramEnumLowerAB":"b"}'                       | '{"paramEnumLowerAB":"b"}'                       |
      | 'DELETE' | '{"paramEnumLowerAB":"b"}'                       | '{"paramEnumLowerAB":"b"}'                       |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/named-optional-dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in an optional dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
    and the service correctly maps the sent value with the declared data type, responding with a success message.

    Given the endpoint '/api/v1/request-body/named-optional-dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | request body                                        | received value                                   |
      | 'POST'   | '{"param-string":"id"}'                             | '{"paramString":"id"}'                           |
      | 'PUT'    | '{"param-string":"id"}'                             | '{"paramString":"id"}'                           |
      | 'PATCH'  | '{"param-string":"id"}'                             | '{"paramString":"id"}'                           |
      | 'DELETE' | '{"param-string":"id"}'                             | '{"paramString":"id"}'                           |
      | 'POST'   | '{"param-integer":2}'                               | '{"paramInteger":2}'                             |
      | 'PUT'    | '{"param-integer":2}'                               | '{"paramInteger":2}'                             |
      | 'PATCH'  | '{"param-integer":2}'                               | '{"paramInteger":2}'                             |
      | 'DELETE' | '{"param-integer":2}'                               | '{"paramInteger":2}'                             |
      | 'POST'   | '{"param-long":2}'                                  | '{"paramLong":2}'                                |
      | 'PUT'    | '{"param-long":2}'                                  | '{"paramLong":2}'                                |
      | 'PATCH'  | '{"param-long":2}'                                  | '{"paramLong":2}'                                |
      | 'DELETE' | '{"param-long":2}'                                  | '{"paramLong":2}'                                |
      | 'POST'   | '{"param-big-integer":2}'                           | '{"paramBigInteger":2}'                          |
      | 'PUT'    | '{"param-big-integer":2}'                           | '{"paramBigInteger":2}'                          |
      | 'PATCH'  | '{"param-big-integer":2}'                           | '{"paramBigInteger":2}'                          |
      | 'DELETE' | '{"param-big-integer":2}'                           | '{"paramBigInteger":2}'                          |
      | 'POST'   | '{"param-float":2.1}'                               | '{"paramFloat":2.1}'                             |
      | 'PUT'    | '{"param-float":2.1}'                               | '{"paramFloat":2.1}'                             |
      | 'PATCH'  | '{"param-float":2.1}'                               | '{"paramFloat":2.1}'                             |
      | 'DELETE' | '{"param-float":2.1}'                               | '{"paramFloat":2.1}'                             |
      | 'POST'   | '{"param-double":2.1}'                              | '{"paramDouble":2.1}'                            |
      | 'PUT'    | '{"param-double":2.1}'                              | '{"paramDouble":2.1}'                            |
      | 'PATCH'  | '{"param-double":2.1}'                              | '{"paramDouble":2.1}'                            |
      | 'DELETE' | '{"param-double":2.1}'                              | '{"paramDouble":2.1}'                            |
      | 'POST'   | '{"param-big-decimal":2.1}'                         | '{"paramBigDecimal":2.1}'                        |
      | 'PUT'    | '{"param-big-decimal":2.1}'                         | '{"paramBigDecimal":2.1}'                        |
      | 'PATCH'  | '{"param-big-decimal":2.1}'                         | '{"paramBigDecimal":2.1}'                        |
      | 'DELETE' | '{"param-big-decimal":2.1}'                         | '{"paramBigDecimal":2.1}'                        |
      | 'POST'   | '{"param-boolean":true}'                            | '{"paramBoolean":true}'                          |
      | 'PUT'    | '{"param-boolean":true}'                            | '{"paramBoolean":true}'                          |
      | 'PATCH'  | '{"param-boolean":true}'                            | '{"paramBoolean":true}'                          |
      | 'DELETE' | '{"param-boolean":true}'                            | '{"paramBoolean":true}'                          |
      | 'POST'   | '{"param-offset-date-time":"2000-01-01T00:00:00Z"}' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'PUT'    | '{"param-offset-date-time":"2000-01-01T00:00:00Z"}' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'PATCH'  | '{"param-offset-date-time":"2000-01-01T00:00:00Z"}' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'DELETE' | '{"param-offset-date-time":"2000-01-01T00:00:00Z"}' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'POST'   | '{"param-local-date-time":"2000-01-01T00:00:00Z"}'  | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'PUT'    | '{"param-local-date-time":"2000-01-01T00:00:00Z"}'  | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'PATCH'  | '{"param-local-date-time":"2000-01-01T00:00:00Z"}'  | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'DELETE' | '{"param-local-date-time":"2000-01-01T00:00:00Z"}'  | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'POST'   | '{"param-local-date":"2000-01-01"}'                 | '{"paramLocalDate":"2000-01-01"}'                |
      | 'PUT'    | '{"param-local-date":"2000-01-01"}'                 | '{"paramLocalDate":"2000-01-01"}'                |
      | 'PATCH'  | '{"param-local-date":"2000-01-01"}'                 | '{"paramLocalDate":"2000-01-01"}'                |
      | 'DELETE' | '{"param-local-date":"2000-01-01"}'                 | '{"paramLocalDate":"2000-01-01"}'                |
      | 'POST'   | '{"param-local-time":"13:00:00"}'                   | '{"paramLocalTime":"13:00:00"}'                  |
      | 'PUT'    | '{"param-local-time":"13:00:00"}'                   | '{"paramLocalTime":"13:00:00"}'                  |
      | 'PATCH'  | '{"param-local-time":"13:00:00"}'                   | '{"paramLocalTime":"13:00:00"}'                  |
      | 'DELETE' | '{"param-local-time":"13:00:00"}'                   | '{"paramLocalTime":"13:00:00"}'                  |
      | 'POST'   | '{"param-year":"2000"}'                             | '{"paramYear":"2000"}'                           |
      | 'PUT'    | '{"param-year":"2000"}'                             | '{"paramYear":"2000"}'                           |
      | 'PATCH'  | '{"param-year":"2000"}'                             | '{"paramYear":"2000"}'                           |
      | 'DELETE' | '{"param-year":"2000"}'                             | '{"paramYear":"2000"}'                           |
      | 'POST'   | '{"param-enum-ab":"A"}'                             | '{"paramEnumAB":"A"}'                            |
      | 'PUT'    | '{"param-enum-ab":"A"}'                             | '{"paramEnumAB":"A"}'                            |
      | 'PATCH'  | '{"param-enum-ab":"A"}'                             | '{"paramEnumAB":"A"}'                            |
      | 'DELETE' | '{"param-enum-ab":"A"}'                             | '{"paramEnumAB":"A"}'                            |
      | 'POST'   | '{"param-enum-lower-ab":"b"}'                       | '{"paramEnumLowerAB":"b"}'                       |
      | 'PUT'    | '{"param-enum-lower-ab":"b"}'                       | '{"paramEnumLowerAB":"b"}'                       |
      | 'PATCH'  | '{"param-enum-lower-ab":"b"}'                       | '{"paramEnumLowerAB":"b"}'                       |
      | 'DELETE' | '{"param-enum-lower-ab":"b"}'                       | '{"paramEnumLowerAB":"b"}'                       |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/nested-dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in a nested dto attribute, both having the same name
    and the service correctly maps the sent value with the declared data type, responding with a success message.

    Given the endpoint '/api/v1/request-body/nested-dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | request body                                                | received value                                              |
      | 'POST'   | '{"nested":{"paramString":"id"}}'                           | '{"nested":{"paramString":"id"}}'                           |
      | 'PUT'    | '{"nested":{"paramString":"id"}}'                           | '{"nested":{"paramString":"id"}}'                           |
      | 'PATCH'  | '{"nested":{"paramString":"id"}}'                           | '{"nested":{"paramString":"id"}}'                           |
      | 'DELETE' | '{"nested":{"paramString":"id"}}'                           | '{"nested":{"paramString":"id"}}'                           |
      | 'POST'   | '{"nested":{"paramInteger":2}}'                             | '{"nested":{"paramInteger":2}}'                             |
      | 'PUT'    | '{"nested":{"paramInteger":2}}'                             | '{"nested":{"paramInteger":2}}'                             |
      | 'PATCH'  | '{"nested":{"paramInteger":2}}'                             | '{"nested":{"paramInteger":2}}'                             |
      | 'DELETE' | '{"nested":{"paramInteger":2}}'                             | '{"nested":{"paramInteger":2}}'                             |
      | 'POST'   | '{"nested":{"paramLong":2}}'                                | '{"nested":{"paramLong":2}}'                                |
      | 'PUT'    | '{"nested":{"paramLong":2}}'                                | '{"nested":{"paramLong":2}}'                                |
      | 'PATCH'  | '{"nested":{"paramLong":2}}'                                | '{"nested":{"paramLong":2}}'                                |
      | 'DELETE' | '{"nested":{"paramLong":2}}'                                | '{"nested":{"paramLong":2}}'                                |
      | 'POST'   | '{"nested":{"paramBigInteger":2}}'                          | '{"nested":{"paramBigInteger":2}}'                          |
      | 'PUT'    | '{"nested":{"paramBigInteger":2}}'                          | '{"nested":{"paramBigInteger":2}}'                          |
      | 'PATCH'  | '{"nested":{"paramBigInteger":2}}'                          | '{"nested":{"paramBigInteger":2}}'                          |
      | 'DELETE' | '{"nested":{"paramBigInteger":2}}'                          | '{"nested":{"paramBigInteger":2}}'                          |
      | 'POST'   | '{"nested":{"paramFloat":2.1}}'                             | '{"nested":{"paramFloat":2.1}}'                             |
      | 'PUT'    | '{"nested":{"paramFloat":2.1}}'                             | '{"nested":{"paramFloat":2.1}}'                             |
      | 'PATCH'  | '{"nested":{"paramFloat":2.1}}'                             | '{"nested":{"paramFloat":2.1}}'                             |
      | 'DELETE' | '{"nested":{"paramFloat":2.1}}'                             | '{"nested":{"paramFloat":2.1}}'                             |
      | 'POST'   | '{"nested":{"paramDouble":2.1}}'                            | '{"nested":{"paramDouble":2.1}}'                            |
      | 'PUT'    | '{"nested":{"paramDouble":2.1}}'                            | '{"nested":{"paramDouble":2.1}}'                            |
      | 'PATCH'  | '{"nested":{"paramDouble":2.1}}'                            | '{"nested":{"paramDouble":2.1}}'                            |
      | 'DELETE' | '{"nested":{"paramDouble":2.1}}'                            | '{"nested":{"paramDouble":2.1}}'                            |
      | 'POST'   | '{"nested":{"paramBigDecimal":2.1}}'                        | '{"nested":{"paramBigDecimal":2.1}}'                        |
      | 'PUT'    | '{"nested":{"paramBigDecimal":2.1}}'                        | '{"nested":{"paramBigDecimal":2.1}}'                        |
      | 'PATCH'  | '{"nested":{"paramBigDecimal":2.1}}'                        | '{"nested":{"paramBigDecimal":2.1}}'                        |
      | 'DELETE' | '{"nested":{"paramBigDecimal":2.1}}'                        | '{"nested":{"paramBigDecimal":2.1}}'                        |
      | 'POST'   | '{"nested":{"paramBoolean":true}}'                          | '{"nested":{"paramBoolean":true}}'                          |
      | 'PUT'    | '{"nested":{"paramBoolean":true}}'                          | '{"nested":{"paramBoolean":true}}'                          |
      | 'PATCH'  | '{"nested":{"paramBoolean":true}}'                          | '{"nested":{"paramBoolean":true}}'                          |
      | 'DELETE' | '{"nested":{"paramBoolean":true}}'                          | '{"nested":{"paramBoolean":true}}'                          |
      | 'POST'   | '{"nested":{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}}' | '{"nested":{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}}' |
      | 'PUT'    | '{"nested":{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}}' | '{"nested":{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}}' |
      | 'PATCH'  | '{"nested":{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}}' | '{"nested":{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}}' |
      | 'DELETE' | '{"nested":{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}}' | '{"nested":{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}}' |
      | 'POST'   | '{"nested":{"paramLocalDateTime":"2000-01-01T00:00:00Z"}}'  | '{"nested":{"paramLocalDateTime":"2000-01-01T00:00:00Z"}}'  |
      | 'PUT'    | '{"nested":{"paramLocalDateTime":"2000-01-01T00:00:00Z"}}'  | '{"nested":{"paramLocalDateTime":"2000-01-01T00:00:00Z"}}'  |
      | 'PATCH'  | '{"nested":{"paramLocalDateTime":"2000-01-01T00:00:00Z"}}'  | '{"nested":{"paramLocalDateTime":"2000-01-01T00:00:00Z"}}'  |
      | 'DELETE' | '{"nested":{"paramLocalDateTime":"2000-01-01T00:00:00Z"}}'  | '{"nested":{"paramLocalDateTime":"2000-01-01T00:00:00Z"}}'  |
      | 'POST'   | '{"nested":{"paramLocalDate":"2000-01-01"}}'                | '{"nested":{"paramLocalDate":"2000-01-01"}}'                |
      | 'PUT'    | '{"nested":{"paramLocalDate":"2000-01-01"}}'                | '{"nested":{"paramLocalDate":"2000-01-01"}}'                |
      | 'PATCH'  | '{"nested":{"paramLocalDate":"2000-01-01"}}'                | '{"nested":{"paramLocalDate":"2000-01-01"}}'                |
      | 'DELETE' | '{"nested":{"paramLocalDate":"2000-01-01"}}'                | '{"nested":{"paramLocalDate":"2000-01-01"}}'                |
      | 'POST'   | '{"nested":{"paramLocalTime":"13:00:00"}}'                  | '{"nested":{"paramLocalTime":"13:00:00"}}'                  |
      | 'PUT'    | '{"nested":{"paramLocalTime":"13:00:00"}}'                  | '{"nested":{"paramLocalTime":"13:00:00"}}'                  |
      | 'PATCH'  | '{"nested":{"paramLocalTime":"13:00:00"}}'                  | '{"nested":{"paramLocalTime":"13:00:00"}}'                  |
      | 'DELETE' | '{"nested":{"paramLocalTime":"13:00:00"}}'                  | '{"nested":{"paramLocalTime":"13:00:00"}}'                  |
      | 'POST'   | '{"nested":{"paramYear":"2000"}}'                           | '{"nested":{"paramYear":"2000"}}'                           |
      | 'PUT'    | '{"nested":{"paramYear":"2000"}}'                           | '{"nested":{"paramYear":"2000"}}'                           |
      | 'PATCH'  | '{"nested":{"paramYear":"2000"}}'                           | '{"nested":{"paramYear":"2000"}}'                           |
      | 'DELETE' | '{"nested":{"paramYear":"2000"}}'                           | '{"nested":{"paramYear":"2000"}}'                           |
      | 'POST'   | '{"nested":{"paramEnumAB":"A"}}'                            | '{"nested":{"paramEnumAB":"A"}}'                            |
      | 'PUT'    | '{"nested":{"paramEnumAB":"A"}}'                            | '{"nested":{"paramEnumAB":"A"}}'                            |
      | 'PATCH'  | '{"nested":{"paramEnumAB":"A"}}'                            | '{"nested":{"paramEnumAB":"A"}}'                            |
      | 'DELETE' | '{"nested":{"paramEnumAB":"A"}}'                            | '{"nested":{"paramEnumAB":"A"}}'                            |
      | 'POST'   | '{"nested":{"paramEnumLowerAB":"b"}}'                       | '{"nested":{"paramEnumLowerAB":"b"}}'                       |
      | 'PUT'    | '{"nested":{"paramEnumLowerAB":"b"}}'                       | '{"nested":{"paramEnumLowerAB":"b"}}'                       |
      | 'PATCH'  | '{"nested":{"paramEnumLowerAB":"b"}}'                       | '{"nested":{"paramEnumLowerAB":"b"}}'                       |
      | 'DELETE' | '{"nested":{"paramEnumLowerAB":"b"}}'                       | '{"nested":{"paramEnumLowerAB":"b"}}'                       |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/named-nested-optional-dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in a nested dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
    and the service correctly maps the sent value with the declared data type, responding with a success message.

    Given the endpoint '/api/v1/request-body/named-nested-optional-dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | request body                                                   | received value                                              |
      | 'POST'   | '{"nested":{"param-string":"id"}}'                             | '{"nested":{"paramString":"id"}}'                           |
      | 'PUT'    | '{"nested":{"param-string":"id"}}'                             | '{"nested":{"paramString":"id"}}'                           |
      | 'PATCH'  | '{"nested":{"param-string":"id"}}'                             | '{"nested":{"paramString":"id"}}'                           |
      | 'DELETE' | '{"nested":{"param-string":"id"}}'                             | '{"nested":{"paramString":"id"}}'                           |
      | 'POST'   | '{"nested":{"param-integer":2}}'                               | '{"nested":{"paramInteger":2}}'                             |
      | 'PUT'    | '{"nested":{"param-integer":2}}'                               | '{"nested":{"paramInteger":2}}'                             |
      | 'PATCH'  | '{"nested":{"param-integer":2}}'                               | '{"nested":{"paramInteger":2}}'                             |
      | 'DELETE' | '{"nested":{"param-integer":2}}'                               | '{"nested":{"paramInteger":2}}'                             |
      | 'POST'   | '{"nested":{"param-long":2}}'                                  | '{"nested":{"paramLong":2}}'                                |
      | 'PUT'    | '{"nested":{"param-long":2}}'                                  | '{"nested":{"paramLong":2}}'                                |
      | 'PATCH'  | '{"nested":{"param-long":2}}'                                  | '{"nested":{"paramLong":2}}'                                |
      | 'DELETE' | '{"nested":{"param-long":2}}'                                  | '{"nested":{"paramLong":2}}'                                |
      | 'POST'   | '{"nested":{"param-big-integer":2}}'                           | '{"nested":{"paramBigInteger":2}}'                          |
      | 'PUT'    | '{"nested":{"param-big-integer":2}}'                           | '{"nested":{"paramBigInteger":2}}'                          |
      | 'PATCH'  | '{"nested":{"param-big-integer":2}}'                           | '{"nested":{"paramBigInteger":2}}'                          |
      | 'DELETE' | '{"nested":{"param-big-integer":2}}'                           | '{"nested":{"paramBigInteger":2}}'                          |
      | 'POST'   | '{"nested":{"param-float":2.1}}'                               | '{"nested":{"paramFloat":2.1}}'                             |
      | 'PUT'    | '{"nested":{"param-float":2.1}}'                               | '{"nested":{"paramFloat":2.1}}'                             |
      | 'PATCH'  | '{"nested":{"param-float":2.1}}'                               | '{"nested":{"paramFloat":2.1}}'                             |
      | 'DELETE' | '{"nested":{"param-float":2.1}}'                               | '{"nested":{"paramFloat":2.1}}'                             |
      | 'POST'   | '{"nested":{"param-double":2.1}}'                              | '{"nested":{"paramDouble":2.1}}'                            |
      | 'PUT'    | '{"nested":{"param-double":2.1}}'                              | '{"nested":{"paramDouble":2.1}}'                            |
      | 'PATCH'  | '{"nested":{"param-double":2.1}}'                              | '{"nested":{"paramDouble":2.1}}'                            |
      | 'DELETE' | '{"nested":{"param-double":2.1}}'                              | '{"nested":{"paramDouble":2.1}}'                            |
      | 'POST'   | '{"nested":{"param-big-decimal":2.1}}'                         | '{"nested":{"paramBigDecimal":2.1}}'                        |
      | 'PUT'    | '{"nested":{"param-big-decimal":2.1}}'                         | '{"nested":{"paramBigDecimal":2.1}}'                        |
      | 'PATCH'  | '{"nested":{"param-big-decimal":2.1}}'                         | '{"nested":{"paramBigDecimal":2.1}}'                        |
      | 'DELETE' | '{"nested":{"param-big-decimal":2.1}}'                         | '{"nested":{"paramBigDecimal":2.1}}'                        |
      | 'POST'   | '{"nested":{"param-boolean":true}}'                            | '{"nested":{"paramBoolean":true}}'                          |
      | 'PUT'    | '{"nested":{"param-boolean":true}}'                            | '{"nested":{"paramBoolean":true}}'                          |
      | 'PATCH'  | '{"nested":{"param-boolean":true}}'                            | '{"nested":{"paramBoolean":true}}'                          |
      | 'DELETE' | '{"nested":{"param-boolean":true}}'                            | '{"nested":{"paramBoolean":true}}'                          |
      | 'POST'   | '{"nested":{"param-offset-date-time":"2000-01-01T00:00:00Z"}}' | '{"nested":{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}}' |
      | 'PUT'    | '{"nested":{"param-offset-date-time":"2000-01-01T00:00:00Z"}}' | '{"nested":{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}}' |
      | 'PATCH'  | '{"nested":{"param-offset-date-time":"2000-01-01T00:00:00Z"}}' | '{"nested":{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}}' |
      | 'DELETE' | '{"nested":{"param-offset-date-time":"2000-01-01T00:00:00Z"}}' | '{"nested":{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}}' |
      | 'POST'   | '{"nested":{"param-local-date-time":"2000-01-01T00:00:00Z"}}'  | '{"nested":{"paramLocalDateTime":"2000-01-01T00:00:00Z"}}'  |
      | 'PUT'    | '{"nested":{"param-local-date-time":"2000-01-01T00:00:00Z"}}'  | '{"nested":{"paramLocalDateTime":"2000-01-01T00:00:00Z"}}'  |
      | 'PATCH'  | '{"nested":{"param-local-date-time":"2000-01-01T00:00:00Z"}}'  | '{"nested":{"paramLocalDateTime":"2000-01-01T00:00:00Z"}}'  |
      | 'DELETE' | '{"nested":{"param-local-date-time":"2000-01-01T00:00:00Z"}}'  | '{"nested":{"paramLocalDateTime":"2000-01-01T00:00:00Z"}}'  |
      | 'POST'   | '{"nested":{"param-local-date":"2000-01-01"}}'                 | '{"nested":{"paramLocalDate":"2000-01-01"}}'                |
      | 'PUT'    | '{"nested":{"param-local-date":"2000-01-01"}}'                 | '{"nested":{"paramLocalDate":"2000-01-01"}}'                |
      | 'PATCH'  | '{"nested":{"param-local-date":"2000-01-01"}}'                 | '{"nested":{"paramLocalDate":"2000-01-01"}}'                |
      | 'DELETE' | '{"nested":{"param-local-date":"2000-01-01"}}'                 | '{"nested":{"paramLocalDate":"2000-01-01"}}'                |
      | 'POST'   | '{"nested":{"param-local-time":"13:00:00"}}'                   | '{"nested":{"paramLocalTime":"13:00:00"}}'                  |
      | 'PUT'    | '{"nested":{"param-local-time":"13:00:00"}}'                   | '{"nested":{"paramLocalTime":"13:00:00"}}'                  |
      | 'PATCH'  | '{"nested":{"param-local-time":"13:00:00"}}'                   | '{"nested":{"paramLocalTime":"13:00:00"}}'                  |
      | 'DELETE' | '{"nested":{"param-local-time":"13:00:00"}}'                   | '{"nested":{"paramLocalTime":"13:00:00"}}'                  |
      | 'POST'   | '{"nested":{"param-year":"2000"}}'                             | '{"nested":{"paramYear":"2000"}}'                           |
      | 'PUT'    | '{"nested":{"param-year":"2000"}}'                             | '{"nested":{"paramYear":"2000"}}'                           |
      | 'PATCH'  | '{"nested":{"param-year":"2000"}}'                             | '{"nested":{"paramYear":"2000"}}'                           |
      | 'DELETE' | '{"nested":{"param-year":"2000"}}'                             | '{"nested":{"paramYear":"2000"}}'                           |
      | 'POST'   | '{"nested":{"param-enum-ab":"A"}}'                             | '{"nested":{"paramEnumAB":"A"}}'                            |
      | 'PUT'    | '{"nested":{"param-enum-ab":"A"}}'                             | '{"nested":{"paramEnumAB":"A"}}'                            |
      | 'PATCH'  | '{"nested":{"param-enum-ab":"A"}}'                             | '{"nested":{"paramEnumAB":"A"}}'                            |
      | 'DELETE' | '{"nested":{"param-enum-ab":"A"}}'                             | '{"nested":{"paramEnumAB":"A"}}'                            |
      | 'POST'   | '{"nested":{"param-enum-lower-ab":"b"}}'                       | '{"nested":{"paramEnumLowerAB":"b"}}'                       |
      | 'PUT'    | '{"nested":{"param-enum-lower-ab":"b"}}'                       | '{"nested":{"paramEnumLowerAB":"b"}}'                       |
      | 'PATCH'  | '{"nested":{"param-enum-lower-ab":"b"}}'                       | '{"nested":{"paramEnumLowerAB":"b"}}'                       |
      | 'DELETE' | '{"nested":{"param-enum-lower-ab":"b"}}'                       | '{"nested":{"paramEnumLowerAB":"b"}}'                       |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/nested-optional-dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in a nested optional dto attribute, both having the same name
    and the service correctly maps the sent value with the declared data type, responding with a success message.

    Given the endpoint '/api/v1/request-body/nested-optional-dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | request body                                                | received value                                              |
      | 'POST'   | '{"nested":{"paramString":"id"}}'                           | '{"nested":{"paramString":"id"}}'                           |
      | 'PUT'    | '{"nested":{"paramString":"id"}}'                           | '{"nested":{"paramString":"id"}}'                           |
      | 'PATCH'  | '{"nested":{"paramString":"id"}}'                           | '{"nested":{"paramString":"id"}}'                           |
      | 'DELETE' | '{"nested":{"paramString":"id"}}'                           | '{"nested":{"paramString":"id"}}'                           |
      | 'POST'   | '{"nested":{"paramInteger":2}}'                             | '{"nested":{"paramInteger":2}}'                             |
      | 'PUT'    | '{"nested":{"paramInteger":2}}'                             | '{"nested":{"paramInteger":2}}'                             |
      | 'PATCH'  | '{"nested":{"paramInteger":2}}'                             | '{"nested":{"paramInteger":2}}'                             |
      | 'DELETE' | '{"nested":{"paramInteger":2}}'                             | '{"nested":{"paramInteger":2}}'                             |
      | 'POST'   | '{"nested":{"paramLong":2}}'                                | '{"nested":{"paramLong":2}}'                                |
      | 'PUT'    | '{"nested":{"paramLong":2}}'                                | '{"nested":{"paramLong":2}}'                                |
      | 'PATCH'  | '{"nested":{"paramLong":2}}'                                | '{"nested":{"paramLong":2}}'                                |
      | 'DELETE' | '{"nested":{"paramLong":2}}'                                | '{"nested":{"paramLong":2}}'                                |
      | 'POST'   | '{"nested":{"paramBigInteger":2}}'                          | '{"nested":{"paramBigInteger":2}}'                          |
      | 'PUT'    | '{"nested":{"paramBigInteger":2}}'                          | '{"nested":{"paramBigInteger":2}}'                          |
      | 'PATCH'  | '{"nested":{"paramBigInteger":2}}'                          | '{"nested":{"paramBigInteger":2}}'                          |
      | 'DELETE' | '{"nested":{"paramBigInteger":2}}'                          | '{"nested":{"paramBigInteger":2}}'                          |
      | 'POST'   | '{"nested":{"paramFloat":2.1}}'                             | '{"nested":{"paramFloat":2.1}}'                             |
      | 'PUT'    | '{"nested":{"paramFloat":2.1}}'                             | '{"nested":{"paramFloat":2.1}}'                             |
      | 'PATCH'  | '{"nested":{"paramFloat":2.1}}'                             | '{"nested":{"paramFloat":2.1}}'                             |
      | 'DELETE' | '{"nested":{"paramFloat":2.1}}'                             | '{"nested":{"paramFloat":2.1}}'                             |
      | 'POST'   | '{"nested":{"paramDouble":2.1}}'                            | '{"nested":{"paramDouble":2.1}}'                            |
      | 'PUT'    | '{"nested":{"paramDouble":2.1}}'                            | '{"nested":{"paramDouble":2.1}}'                            |
      | 'PATCH'  | '{"nested":{"paramDouble":2.1}}'                            | '{"nested":{"paramDouble":2.1}}'                            |
      | 'DELETE' | '{"nested":{"paramDouble":2.1}}'                            | '{"nested":{"paramDouble":2.1}}'                            |
      | 'POST'   | '{"nested":{"paramBigDecimal":2.1}}'                        | '{"nested":{"paramBigDecimal":2.1}}'                        |
      | 'PUT'    | '{"nested":{"paramBigDecimal":2.1}}'                        | '{"nested":{"paramBigDecimal":2.1}}'                        |
      | 'PATCH'  | '{"nested":{"paramBigDecimal":2.1}}'                        | '{"nested":{"paramBigDecimal":2.1}}'                        |
      | 'DELETE' | '{"nested":{"paramBigDecimal":2.1}}'                        | '{"nested":{"paramBigDecimal":2.1}}'                        |
      | 'POST'   | '{"nested":{"paramBoolean":true}}'                          | '{"nested":{"paramBoolean":true}}'                          |
      | 'PUT'    | '{"nested":{"paramBoolean":true}}'                          | '{"nested":{"paramBoolean":true}}'                          |
      | 'PATCH'  | '{"nested":{"paramBoolean":true}}'                          | '{"nested":{"paramBoolean":true}}'                          |
      | 'DELETE' | '{"nested":{"paramBoolean":true}}'                          | '{"nested":{"paramBoolean":true}}'                          |
      | 'POST'   | '{"nested":{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}}' | '{"nested":{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}}' |
      | 'PUT'    | '{"nested":{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}}' | '{"nested":{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}}' |
      | 'PATCH'  | '{"nested":{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}}' | '{"nested":{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}}' |
      | 'DELETE' | '{"nested":{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}}' | '{"nested":{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}}' |
      | 'POST'   | '{"nested":{"paramLocalDateTime":"2000-01-01T00:00:00Z"}}'  | '{"nested":{"paramLocalDateTime":"2000-01-01T00:00:00Z"}}'  |
      | 'PUT'    | '{"nested":{"paramLocalDateTime":"2000-01-01T00:00:00Z"}}'  | '{"nested":{"paramLocalDateTime":"2000-01-01T00:00:00Z"}}'  |
      | 'PATCH'  | '{"nested":{"paramLocalDateTime":"2000-01-01T00:00:00Z"}}'  | '{"nested":{"paramLocalDateTime":"2000-01-01T00:00:00Z"}}'  |
      | 'DELETE' | '{"nested":{"paramLocalDateTime":"2000-01-01T00:00:00Z"}}'  | '{"nested":{"paramLocalDateTime":"2000-01-01T00:00:00Z"}}'  |
      | 'POST'   | '{"nested":{"paramLocalDate":"2000-01-01"}}'                | '{"nested":{"paramLocalDate":"2000-01-01"}}'                |
      | 'PUT'    | '{"nested":{"paramLocalDate":"2000-01-01"}}'                | '{"nested":{"paramLocalDate":"2000-01-01"}}'                |
      | 'PATCH'  | '{"nested":{"paramLocalDate":"2000-01-01"}}'                | '{"nested":{"paramLocalDate":"2000-01-01"}}'                |
      | 'DELETE' | '{"nested":{"paramLocalDate":"2000-01-01"}}'                | '{"nested":{"paramLocalDate":"2000-01-01"}}'                |
      | 'POST'   | '{"nested":{"paramLocalTime":"13:00:00"}}'                  | '{"nested":{"paramLocalTime":"13:00:00"}}'                  |
      | 'PUT'    | '{"nested":{"paramLocalTime":"13:00:00"}}'                  | '{"nested":{"paramLocalTime":"13:00:00"}}'                  |
      | 'PATCH'  | '{"nested":{"paramLocalTime":"13:00:00"}}'                  | '{"nested":{"paramLocalTime":"13:00:00"}}'                  |
      | 'DELETE' | '{"nested":{"paramLocalTime":"13:00:00"}}'                  | '{"nested":{"paramLocalTime":"13:00:00"}}'                  |
      | 'POST'   | '{"nested":{"paramYear":"2000"}}'                           | '{"nested":{"paramYear":"2000"}}'                           |
      | 'PUT'    | '{"nested":{"paramYear":"2000"}}'                           | '{"nested":{"paramYear":"2000"}}'                           |
      | 'PATCH'  | '{"nested":{"paramYear":"2000"}}'                           | '{"nested":{"paramYear":"2000"}}'                           |
      | 'DELETE' | '{"nested":{"paramYear":"2000"}}'                           | '{"nested":{"paramYear":"2000"}}'                           |
      | 'POST'   | '{"nested":{"paramEnumAB":"A"}}'                            | '{"nested":{"paramEnumAB":"A"}}'                            |
      | 'PUT'    | '{"nested":{"paramEnumAB":"A"}}'                            | '{"nested":{"paramEnumAB":"A"}}'                            |
      | 'PATCH'  | '{"nested":{"paramEnumAB":"A"}}'                            | '{"nested":{"paramEnumAB":"A"}}'                            |
      | 'DELETE' | '{"nested":{"paramEnumAB":"A"}}'                            | '{"nested":{"paramEnumAB":"A"}}'                            |
      | 'POST'   | '{"nested":{"paramEnumLowerAB":"b"}}'                       | '{"nested":{"paramEnumLowerAB":"b"}}'                       |
      | 'PUT'    | '{"nested":{"paramEnumLowerAB":"b"}}'                       | '{"nested":{"paramEnumLowerAB":"b"}}'                       |
      | 'PATCH'  | '{"nested":{"paramEnumLowerAB":"b"}}'                       | '{"nested":{"paramEnumLowerAB":"b"}}'                       |
      | 'DELETE' | '{"nested":{"paramEnumLowerAB":"b"}}'                       | '{"nested":{"paramEnumLowerAB":"b"}}'                       |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/named-nested-dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in a nested optional dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
    and the service correctly maps the sent value with the declared data type, responding with a success message.

    Given the endpoint '/api/v1/request-body/named-nested-dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | request body                                                   | received value                                              |
      | 'POST'   | '{"nested":{"param-string":"id"}}'                             | '{"nested":{"paramString":"id"}}'                           |
      | 'PUT'    | '{"nested":{"param-string":"id"}}'                             | '{"nested":{"paramString":"id"}}'                           |
      | 'PATCH'  | '{"nested":{"param-string":"id"}}'                             | '{"nested":{"paramString":"id"}}'                           |
      | 'DELETE' | '{"nested":{"param-string":"id"}}'                             | '{"nested":{"paramString":"id"}}'                           |
      | 'POST'   | '{"nested":{"param-integer":2}}'                               | '{"nested":{"paramInteger":2}}'                             |
      | 'PUT'    | '{"nested":{"param-integer":2}}'                               | '{"nested":{"paramInteger":2}}'                             |
      | 'PATCH'  | '{"nested":{"param-integer":2}}'                               | '{"nested":{"paramInteger":2}}'                             |
      | 'DELETE' | '{"nested":{"param-integer":2}}'                               | '{"nested":{"paramInteger":2}}'                             |
      | 'POST'   | '{"nested":{"param-long":2}}'                                  | '{"nested":{"paramLong":2}}'                                |
      | 'PUT'    | '{"nested":{"param-long":2}}'                                  | '{"nested":{"paramLong":2}}'                                |
      | 'PATCH'  | '{"nested":{"param-long":2}}'                                  | '{"nested":{"paramLong":2}}'                                |
      | 'DELETE' | '{"nested":{"param-long":2}}'                                  | '{"nested":{"paramLong":2}}'                                |
      | 'POST'   | '{"nested":{"param-big-integer":2}}'                           | '{"nested":{"paramBigInteger":2}}'                          |
      | 'PUT'    | '{"nested":{"param-big-integer":2}}'                           | '{"nested":{"paramBigInteger":2}}'                          |
      | 'PATCH'  | '{"nested":{"param-big-integer":2}}'                           | '{"nested":{"paramBigInteger":2}}'                          |
      | 'DELETE' | '{"nested":{"param-big-integer":2}}'                           | '{"nested":{"paramBigInteger":2}}'                          |
      | 'POST'   | '{"nested":{"param-float":2.1}}'                               | '{"nested":{"paramFloat":2.1}}'                             |
      | 'PUT'    | '{"nested":{"param-float":2.1}}'                               | '{"nested":{"paramFloat":2.1}}'                             |
      | 'PATCH'  | '{"nested":{"param-float":2.1}}'                               | '{"nested":{"paramFloat":2.1}}'                             |
      | 'DELETE' | '{"nested":{"param-float":2.1}}'                               | '{"nested":{"paramFloat":2.1}}'                             |
      | 'POST'   | '{"nested":{"param-double":2.1}}'                              | '{"nested":{"paramDouble":2.1}}'                            |
      | 'PUT'    | '{"nested":{"param-double":2.1}}'                              | '{"nested":{"paramDouble":2.1}}'                            |
      | 'PATCH'  | '{"nested":{"param-double":2.1}}'                              | '{"nested":{"paramDouble":2.1}}'                            |
      | 'DELETE' | '{"nested":{"param-double":2.1}}'                              | '{"nested":{"paramDouble":2.1}}'                            |
      | 'POST'   | '{"nested":{"param-big-decimal":2.1}}'                         | '{"nested":{"paramBigDecimal":2.1}}'                        |
      | 'PUT'    | '{"nested":{"param-big-decimal":2.1}}'                         | '{"nested":{"paramBigDecimal":2.1}}'                        |
      | 'PATCH'  | '{"nested":{"param-big-decimal":2.1}}'                         | '{"nested":{"paramBigDecimal":2.1}}'                        |
      | 'DELETE' | '{"nested":{"param-big-decimal":2.1}}'                         | '{"nested":{"paramBigDecimal":2.1}}'                        |
      | 'POST'   | '{"nested":{"param-boolean":true}}'                            | '{"nested":{"paramBoolean":true}}'                          |
      | 'PUT'    | '{"nested":{"param-boolean":true}}'                            | '{"nested":{"paramBoolean":true}}'                          |
      | 'PATCH'  | '{"nested":{"param-boolean":true}}'                            | '{"nested":{"paramBoolean":true}}'                          |
      | 'DELETE' | '{"nested":{"param-boolean":true}}'                            | '{"nested":{"paramBoolean":true}}'                          |
      | 'POST'   | '{"nested":{"param-offset-date-time":"2000-01-01T00:00:00Z"}}' | '{"nested":{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}}' |
      | 'PUT'    | '{"nested":{"param-offset-date-time":"2000-01-01T00:00:00Z"}}' | '{"nested":{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}}' |
      | 'PATCH'  | '{"nested":{"param-offset-date-time":"2000-01-01T00:00:00Z"}}' | '{"nested":{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}}' |
      | 'DELETE' | '{"nested":{"param-offset-date-time":"2000-01-01T00:00:00Z"}}' | '{"nested":{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}}' |
      | 'POST'   | '{"nested":{"param-local-date-time":"2000-01-01T00:00:00Z"}}'  | '{"nested":{"paramLocalDateTime":"2000-01-01T00:00:00Z"}}'  |
      | 'PUT'    | '{"nested":{"param-local-date-time":"2000-01-01T00:00:00Z"}}'  | '{"nested":{"paramLocalDateTime":"2000-01-01T00:00:00Z"}}'  |
      | 'PATCH'  | '{"nested":{"param-local-date-time":"2000-01-01T00:00:00Z"}}'  | '{"nested":{"paramLocalDateTime":"2000-01-01T00:00:00Z"}}'  |
      | 'DELETE' | '{"nested":{"param-local-date-time":"2000-01-01T00:00:00Z"}}'  | '{"nested":{"paramLocalDateTime":"2000-01-01T00:00:00Z"}}'  |
      | 'POST'   | '{"nested":{"param-local-date":"2000-01-01"}}'                 | '{"nested":{"paramLocalDate":"2000-01-01"}}'                |
      | 'PUT'    | '{"nested":{"param-local-date":"2000-01-01"}}'                 | '{"nested":{"paramLocalDate":"2000-01-01"}}'                |
      | 'PATCH'  | '{"nested":{"param-local-date":"2000-01-01"}}'                 | '{"nested":{"paramLocalDate":"2000-01-01"}}'                |
      | 'DELETE' | '{"nested":{"param-local-date":"2000-01-01"}}'                 | '{"nested":{"paramLocalDate":"2000-01-01"}}'                |
      | 'POST'   | '{"nested":{"param-local-time":"13:00:00"}}'                   | '{"nested":{"paramLocalTime":"13:00:00"}}'                  |
      | 'PUT'    | '{"nested":{"param-local-time":"13:00:00"}}'                   | '{"nested":{"paramLocalTime":"13:00:00"}}'                  |
      | 'PATCH'  | '{"nested":{"param-local-time":"13:00:00"}}'                   | '{"nested":{"paramLocalTime":"13:00:00"}}'                  |
      | 'DELETE' | '{"nested":{"param-local-time":"13:00:00"}}'                   | '{"nested":{"paramLocalTime":"13:00:00"}}'                  |
      | 'POST'   | '{"nested":{"param-year":"2000"}}'                             | '{"nested":{"paramYear":"2000"}}'                           |
      | 'PUT'    | '{"nested":{"param-year":"2000"}}'                             | '{"nested":{"paramYear":"2000"}}'                           |
      | 'PATCH'  | '{"nested":{"param-year":"2000"}}'                             | '{"nested":{"paramYear":"2000"}}'                           |
      | 'DELETE' | '{"nested":{"param-year":"2000"}}'                             | '{"nested":{"paramYear":"2000"}}'                           |
      | 'POST'   | '{"nested":{"param-enum-ab":"A"}}'                             | '{"nested":{"paramEnumAB":"A"}}'                            |
      | 'PUT'    | '{"nested":{"param-enum-ab":"A"}}'                             | '{"nested":{"paramEnumAB":"A"}}'                            |
      | 'PATCH'  | '{"nested":{"param-enum-ab":"A"}}'                             | '{"nested":{"paramEnumAB":"A"}}'                            |
      | 'DELETE' | '{"nested":{"param-enum-ab":"A"}}'                             | '{"nested":{"paramEnumAB":"A"}}'                            |
      | 'POST'   | '{"nested":{"param-enum-lower-ab":"b"}}'                       | '{"nested":{"paramEnumLowerAB":"b"}}'                       |
      | 'PUT'    | '{"nested":{"param-enum-lower-ab":"b"}}'                       | '{"nested":{"paramEnumLowerAB":"b"}}'                       |
      | 'PATCH'  | '{"nested":{"param-enum-lower-ab":"b"}}'                       | '{"nested":{"paramEnumLowerAB":"b"}}'                       |
      | 'DELETE' | '{"nested":{"param-enum-lower-ab":"b"}}'                       | '{"nested":{"paramEnumLowerAB":"b"}}'                       |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/nested-dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in a nested dto attribute from an array, both having the same name
    and the service correctly maps the sent value with the declared data type, responding with a success message.

    Given the endpoint '/api/v1/request-body/nested-dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | request body                                                 | received value                                               |
      | 'POST'   | '{"array":[{"paramString":"id"}]}'                           | '{"array":[{"paramString":"id"}]}'                           |
      | 'PUT'    | '{"array":[{"paramString":"id"}]}'                           | '{"array":[{"paramString":"id"}]}'                           |
      | 'PATCH'  | '{"array":[{"paramString":"id"}]}'                           | '{"array":[{"paramString":"id"}]}'                           |
      | 'DELETE' | '{"array":[{"paramString":"id"}]}'                           | '{"array":[{"paramString":"id"}]}'                           |
      | 'POST'   | '{"array":[{"paramInteger":2}]}'                             | '{"array":[{"paramInteger":2}]}'                             |
      | 'PUT'    | '{"array":[{"paramInteger":2}]}'                             | '{"array":[{"paramInteger":2}]}'                             |
      | 'PATCH'  | '{"array":[{"paramInteger":2}]}'                             | '{"array":[{"paramInteger":2}]}'                             |
      | 'DELETE' | '{"array":[{"paramInteger":2}]}'                             | '{"array":[{"paramInteger":2}]}'                             |
      | 'POST'   | '{"array":[{"paramLong":2}]}'                                | '{"array":[{"paramLong":2}]}'                                |
      | 'PUT'    | '{"array":[{"paramLong":2}]}'                                | '{"array":[{"paramLong":2}]}'                                |
      | 'PATCH'  | '{"array":[{"paramLong":2}]}'                                | '{"array":[{"paramLong":2}]}'                                |
      | 'DELETE' | '{"array":[{"paramLong":2}]}'                                | '{"array":[{"paramLong":2}]}'                                |
      | 'POST'   | '{"array":[{"paramBigInteger":2}]}'                          | '{"array":[{"paramBigInteger":2}]}'                          |
      | 'PUT'    | '{"array":[{"paramBigInteger":2}]}'                          | '{"array":[{"paramBigInteger":2}]}'                          |
      | 'PATCH'  | '{"array":[{"paramBigInteger":2}]}'                          | '{"array":[{"paramBigInteger":2}]}'                          |
      | 'DELETE' | '{"array":[{"paramBigInteger":2}]}'                          | '{"array":[{"paramBigInteger":2}]}'                          |
      | 'POST'   | '{"array":[{"paramFloat":2.1}]}'                             | '{"array":[{"paramFloat":2.1}]}'                             |
      | 'PUT'    | '{"array":[{"paramFloat":2.1}]}'                             | '{"array":[{"paramFloat":2.1}]}'                             |
      | 'PATCH'  | '{"array":[{"paramFloat":2.1}]}'                             | '{"array":[{"paramFloat":2.1}]}'                             |
      | 'DELETE' | '{"array":[{"paramFloat":2.1}]}'                             | '{"array":[{"paramFloat":2.1}]}'                             |
      | 'POST'   | '{"array":[{"paramDouble":2.1}]}'                            | '{"array":[{"paramDouble":2.1}]}'                            |
      | 'PUT'    | '{"array":[{"paramDouble":2.1}]}'                            | '{"array":[{"paramDouble":2.1}]}'                            |
      | 'PATCH'  | '{"array":[{"paramDouble":2.1}]}'                            | '{"array":[{"paramDouble":2.1}]}'                            |
      | 'DELETE' | '{"array":[{"paramDouble":2.1}]}'                            | '{"array":[{"paramDouble":2.1}]}'                            |
      | 'POST'   | '{"array":[{"paramBigDecimal":2.1}]}'                        | '{"array":[{"paramBigDecimal":2.1}]}'                        |
      | 'PUT'    | '{"array":[{"paramBigDecimal":2.1}]}'                        | '{"array":[{"paramBigDecimal":2.1}]}'                        |
      | 'PATCH'  | '{"array":[{"paramBigDecimal":2.1}]}'                        | '{"array":[{"paramBigDecimal":2.1}]}'                        |
      | 'DELETE' | '{"array":[{"paramBigDecimal":2.1}]}'                        | '{"array":[{"paramBigDecimal":2.1}]}'                        |
      | 'POST'   | '{"array":[{"paramBoolean":true}]}'                          | '{"array":[{"paramBoolean":true}]}'                          |
      | 'PUT'    | '{"array":[{"paramBoolean":true}]}'                          | '{"array":[{"paramBoolean":true}]}'                          |
      | 'PATCH'  | '{"array":[{"paramBoolean":true}]}'                          | '{"array":[{"paramBoolean":true}]}'                          |
      | 'DELETE' | '{"array":[{"paramBoolean":true}]}'                          | '{"array":[{"paramBoolean":true}]}'                          |
      | 'POST'   | '{"array":[{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}]}' | '{"array":[{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}]}' |
      | 'PUT'    | '{"array":[{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}]}' | '{"array":[{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}]}' |
      | 'PATCH'  | '{"array":[{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}]}' | '{"array":[{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}]}' |
      | 'DELETE' | '{"array":[{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}]}' | '{"array":[{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}]}' |
      | 'POST'   | '{"array":[{"paramLocalDateTime":"2000-01-01T00:00:00Z"}]}'  | '{"array":[{"paramLocalDateTime":"2000-01-01T00:00:00Z"}]}'  |
      | 'PUT'    | '{"array":[{"paramLocalDateTime":"2000-01-01T00:00:00Z"}]}'  | '{"array":[{"paramLocalDateTime":"2000-01-01T00:00:00Z"}]}'  |
      | 'PATCH'  | '{"array":[{"paramLocalDateTime":"2000-01-01T00:00:00Z"}]}'  | '{"array":[{"paramLocalDateTime":"2000-01-01T00:00:00Z"}]}'  |
      | 'DELETE' | '{"array":[{"paramLocalDateTime":"2000-01-01T00:00:00Z"}]}'  | '{"array":[{"paramLocalDateTime":"2000-01-01T00:00:00Z"}]}'  |
      | 'POST'   | '{"array":[{"paramLocalDate":"2000-01-01"}]}'                | '{"array":[{"paramLocalDate":"2000-01-01"}]}'                |
      | 'PUT'    | '{"array":[{"paramLocalDate":"2000-01-01"}]}'                | '{"array":[{"paramLocalDate":"2000-01-01"}]}'                |
      | 'PATCH'  | '{"array":[{"paramLocalDate":"2000-01-01"}]}'                | '{"array":[{"paramLocalDate":"2000-01-01"}]}'                |
      | 'DELETE' | '{"array":[{"paramLocalDate":"2000-01-01"}]}'                | '{"array":[{"paramLocalDate":"2000-01-01"}]}'                |
      | 'POST'   | '{"array":[{"paramLocalTime":"13:00:00"}]}'                  | '{"array":[{"paramLocalTime":"13:00:00"}]}'                  |
      | 'PUT'    | '{"array":[{"paramLocalTime":"13:00:00"}]}'                  | '{"array":[{"paramLocalTime":"13:00:00"}]}'                  |
      | 'PATCH'  | '{"array":[{"paramLocalTime":"13:00:00"}]}'                  | '{"array":[{"paramLocalTime":"13:00:00"}]}'                  |
      | 'DELETE' | '{"array":[{"paramLocalTime":"13:00:00"}]}'                  | '{"array":[{"paramLocalTime":"13:00:00"}]}'                  |
      | 'POST'   | '{"array":[{"paramYear":"2000"}]}'                           | '{"array":[{"paramYear":"2000"}]}'                           |
      | 'PUT'    | '{"array":[{"paramYear":"2000"}]}'                           | '{"array":[{"paramYear":"2000"}]}'                           |
      | 'PATCH'  | '{"array":[{"paramYear":"2000"}]}'                           | '{"array":[{"paramYear":"2000"}]}'                           |
      | 'DELETE' | '{"array":[{"paramYear":"2000"}]}'                           | '{"array":[{"paramYear":"2000"}]}'                           |
      | 'POST'   | '{"array":[{"paramEnumAB":"A"}]}'                            | '{"array":[{"paramEnumAB":"A"}]}'                            |
      | 'PUT'    | '{"array":[{"paramEnumAB":"A"}]}'                            | '{"array":[{"paramEnumAB":"A"}]}'                            |
      | 'PATCH'  | '{"array":[{"paramEnumAB":"A"}]}'                            | '{"array":[{"paramEnumAB":"A"}]}'                            |
      | 'DELETE' | '{"array":[{"paramEnumAB":"A"}]}'                            | '{"array":[{"paramEnumAB":"A"}]}'                            |
      | 'POST'   | '{"array":[{"paramEnumLowerAB":"b"}]}'                       | '{"array":[{"paramEnumLowerAB":"b"}]}'                       |
      | 'PUT'    | '{"array":[{"paramEnumLowerAB":"b"}]}'                       | '{"array":[{"paramEnumLowerAB":"b"}]}'                       |
      | 'PATCH'  | '{"array":[{"paramEnumLowerAB":"b"}]}'                       | '{"array":[{"paramEnumLowerAB":"b"}]}'                       |
      | 'DELETE' | '{"array":[{"paramEnumLowerAB":"b"}]}'                       | '{"array":[{"paramEnumLowerAB":"b"}]}'                       |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/named-nested-dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in a nested dto attribute from an array, both having the diferent names, a custom body-attibute name is defined by the endpoint
    and the service correctly maps the sent value with the declared data type, responding with a success message.

    Given the endpoint '/api/v1/request-body/named-nested-dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | request body                                                    | received value                                               |
      | 'POST'   | '{"array":[{"param-string":"id"}]}'                             | '{"array":[{"paramString":"id"}]}'                           |
      | 'PUT'    | '{"array":[{"param-string":"id"}]}'                             | '{"array":[{"paramString":"id"}]}'                           |
      | 'PATCH'  | '{"array":[{"param-string":"id"}]}'                             | '{"array":[{"paramString":"id"}]}'                           |
      | 'DELETE' | '{"array":[{"param-string":"id"}]}'                             | '{"array":[{"paramString":"id"}]}'                           |
      | 'POST'   | '{"array":[{"param-integer":2}]}'                               | '{"array":[{"paramInteger":2}]}'                             |
      | 'PUT'    | '{"array":[{"param-integer":2}]}'                               | '{"array":[{"paramInteger":2}]}'                             |
      | 'PATCH'  | '{"array":[{"param-integer":2}]}'                               | '{"array":[{"paramInteger":2}]}'                             |
      | 'DELETE' | '{"array":[{"param-integer":2}]}'                               | '{"array":[{"paramInteger":2}]}'                             |
      | 'POST'   | '{"array":[{"param-long":2}]}'                                  | '{"array":[{"paramLong":2}]}'                                |
      | 'PUT'    | '{"array":[{"param-long":2}]}'                                  | '{"array":[{"paramLong":2}]}'                                |
      | 'PATCH'  | '{"array":[{"param-long":2}]}'                                  | '{"array":[{"paramLong":2}]}'                                |
      | 'DELETE' | '{"array":[{"param-long":2}]}'                                  | '{"array":[{"paramLong":2}]}'                                |
      | 'POST'   | '{"array":[{"param-big-integer":2}]}'                           | '{"array":[{"paramBigInteger":2}]}'                          |
      | 'PUT'    | '{"array":[{"param-big-integer":2}]}'                           | '{"array":[{"paramBigInteger":2}]}'                          |
      | 'PATCH'  | '{"array":[{"param-big-integer":2}]}'                           | '{"array":[{"paramBigInteger":2}]}'                          |
      | 'DELETE' | '{"array":[{"param-big-integer":2}]}'                           | '{"array":[{"paramBigInteger":2}]}'                          |
      | 'POST'   | '{"array":[{"param-float":2.1}]}'                               | '{"array":[{"paramFloat":2.1}]}'                             |
      | 'PUT'    | '{"array":[{"param-float":2.1}]}'                               | '{"array":[{"paramFloat":2.1}]}'                             |
      | 'PATCH'  | '{"array":[{"param-float":2.1}]}'                               | '{"array":[{"paramFloat":2.1}]}'                             |
      | 'DELETE' | '{"array":[{"param-float":2.1}]}'                               | '{"array":[{"paramFloat":2.1}]}'                             |
      | 'POST'   | '{"array":[{"param-double":2.1}]}'                              | '{"array":[{"paramDouble":2.1}]}'                            |
      | 'PUT'    | '{"array":[{"param-double":2.1}]}'                              | '{"array":[{"paramDouble":2.1}]}'                            |
      | 'PATCH'  | '{"array":[{"param-double":2.1}]}'                              | '{"array":[{"paramDouble":2.1}]}'                            |
      | 'DELETE' | '{"array":[{"param-double":2.1}]}'                              | '{"array":[{"paramDouble":2.1}]}'                            |
      | 'POST'   | '{"array":[{"param-big-decimal":2.1}]}'                         | '{"array":[{"paramBigDecimal":2.1}]}'                        |
      | 'PUT'    | '{"array":[{"param-big-decimal":2.1}]}'                         | '{"array":[{"paramBigDecimal":2.1}]}'                        |
      | 'PATCH'  | '{"array":[{"param-big-decimal":2.1}]}'                         | '{"array":[{"paramBigDecimal":2.1}]}'                        |
      | 'DELETE' | '{"array":[{"param-big-decimal":2.1}]}'                         | '{"array":[{"paramBigDecimal":2.1}]}'                        |
      | 'POST'   | '{"array":[{"param-boolean":true}]}'                            | '{"array":[{"paramBoolean":true}]}'                          |
      | 'PUT'    | '{"array":[{"param-boolean":true}]}'                            | '{"array":[{"paramBoolean":true}]}'                          |
      | 'PATCH'  | '{"array":[{"param-boolean":true}]}'                            | '{"array":[{"paramBoolean":true}]}'                          |
      | 'DELETE' | '{"array":[{"param-boolean":true}]}'                            | '{"array":[{"paramBoolean":true}]}'                          |
      | 'POST'   | '{"array":[{"param-offset-date-time":"2000-01-01T00:00:00Z"}]}' | '{"array":[{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}]}' |
      | 'PUT'    | '{"array":[{"param-offset-date-time":"2000-01-01T00:00:00Z"}]}' | '{"array":[{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}]}' |
      | 'PATCH'  | '{"array":[{"param-offset-date-time":"2000-01-01T00:00:00Z"}]}' | '{"array":[{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}]}' |
      | 'DELETE' | '{"array":[{"param-offset-date-time":"2000-01-01T00:00:00Z"}]}' | '{"array":[{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}]}' |
      | 'POST'   | '{"array":[{"param-local-date-time":"2000-01-01T00:00:00Z"}]}'  | '{"array":[{"paramLocalDateTime":"2000-01-01T00:00:00Z"}]}'  |
      | 'PUT'    | '{"array":[{"param-local-date-time":"2000-01-01T00:00:00Z"}]}'  | '{"array":[{"paramLocalDateTime":"2000-01-01T00:00:00Z"}]}'  |
      | 'PATCH'  | '{"array":[{"param-local-date-time":"2000-01-01T00:00:00Z"}]}'  | '{"array":[{"paramLocalDateTime":"2000-01-01T00:00:00Z"}]}'  |
      | 'DELETE' | '{"array":[{"param-local-date-time":"2000-01-01T00:00:00Z"}]}'  | '{"array":[{"paramLocalDateTime":"2000-01-01T00:00:00Z"}]}'  |
      | 'POST'   | '{"array":[{"param-local-date":"2000-01-01"}]}'                 | '{"array":[{"paramLocalDate":"2000-01-01"}]}'                |
      | 'PUT'    | '{"array":[{"param-local-date":"2000-01-01"}]}'                 | '{"array":[{"paramLocalDate":"2000-01-01"}]}'                |
      | 'PATCH'  | '{"array":[{"param-local-date":"2000-01-01"}]}'                 | '{"array":[{"paramLocalDate":"2000-01-01"}]}'                |
      | 'DELETE' | '{"array":[{"param-local-date":"2000-01-01"}]}'                 | '{"array":[{"paramLocalDate":"2000-01-01"}]}'                |
      | 'POST'   | '{"array":[{"param-local-time":"13:00:00"}]}'                   | '{"array":[{"paramLocalTime":"13:00:00"}]}'                  |
      | 'PUT'    | '{"array":[{"param-local-time":"13:00:00"}]}'                   | '{"array":[{"paramLocalTime":"13:00:00"}]}'                  |
      | 'PATCH'  | '{"array":[{"param-local-time":"13:00:00"}]}'                   | '{"array":[{"paramLocalTime":"13:00:00"}]}'                  |
      | 'DELETE' | '{"array":[{"param-local-time":"13:00:00"}]}'                   | '{"array":[{"paramLocalTime":"13:00:00"}]}'                  |
      | 'POST'   | '{"array":[{"param-year":"2000"}]}'                             | '{"array":[{"paramYear":"2000"}]}'                           |
      | 'PUT'    | '{"array":[{"param-year":"2000"}]}'                             | '{"array":[{"paramYear":"2000"}]}'                           |
      | 'PATCH'  | '{"array":[{"param-year":"2000"}]}'                             | '{"array":[{"paramYear":"2000"}]}'                           |
      | 'DELETE' | '{"array":[{"param-year":"2000"}]}'                             | '{"array":[{"paramYear":"2000"}]}'                           |
      | 'POST'   | '{"array":[{"param-enum-ab":"A"}]}'                             | '{"array":[{"paramEnumAB":"A"}]}'                            |
      | 'PUT'    | '{"array":[{"param-enum-ab":"A"}]}'                             | '{"array":[{"paramEnumAB":"A"}]}'                            |
      | 'PATCH'  | '{"array":[{"param-enum-ab":"A"}]}'                             | '{"array":[{"paramEnumAB":"A"}]}'                            |
      | 'DELETE' | '{"array":[{"param-enum-ab":"A"}]}'                             | '{"array":[{"paramEnumAB":"A"}]}'                            |
      | 'POST'   | '{"array":[{"param-enum-lower-ab":"b"}]}'                       | '{"array":[{"paramEnumLowerAB":"b"}]}'                       |
      | 'PUT'    | '{"array":[{"param-enum-lower-ab":"b"}]}'                       | '{"array":[{"paramEnumLowerAB":"b"}]}'                       |
      | 'PATCH'  | '{"array":[{"param-enum-lower-ab":"b"}]}'                       | '{"array":[{"paramEnumLowerAB":"b"}]}'                       |
      | 'DELETE' | '{"array":[{"param-enum-lower-ab":"b"}]}'                       | '{"array":[{"paramEnumLowerAB":"b"}]}'                       |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/nested-optional-dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in a nested optional dto attribute from an array, both having the same name
    and the service correctly maps the sent value with the declared data type, responding with a success message.

    Given the endpoint '/api/v1/request-body/nested-optional-dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | request body                                                 | received value                                               |
      | 'POST'   | '{"array":[{"paramString":"id"}]}'                           | '{"array":[{"paramString":"id"}]}'                           |
      | 'PUT'    | '{"array":[{"paramString":"id"}]}'                           | '{"array":[{"paramString":"id"}]}'                           |
      | 'PATCH'  | '{"array":[{"paramString":"id"}]}'                           | '{"array":[{"paramString":"id"}]}'                           |
      | 'DELETE' | '{"array":[{"paramString":"id"}]}'                           | '{"array":[{"paramString":"id"}]}'                           |
      | 'POST'   | '{"array":[{"paramInteger":2}]}'                             | '{"array":[{"paramInteger":2}]}'                             |
      | 'PUT'    | '{"array":[{"paramInteger":2}]}'                             | '{"array":[{"paramInteger":2}]}'                             |
      | 'PATCH'  | '{"array":[{"paramInteger":2}]}'                             | '{"array":[{"paramInteger":2}]}'                             |
      | 'DELETE' | '{"array":[{"paramInteger":2}]}'                             | '{"array":[{"paramInteger":2}]}'                             |
      | 'POST'   | '{"array":[{"paramLong":2}]}'                                | '{"array":[{"paramLong":2}]}'                                |
      | 'PUT'    | '{"array":[{"paramLong":2}]}'                                | '{"array":[{"paramLong":2}]}'                                |
      | 'PATCH'  | '{"array":[{"paramLong":2}]}'                                | '{"array":[{"paramLong":2}]}'                                |
      | 'DELETE' | '{"array":[{"paramLong":2}]}'                                | '{"array":[{"paramLong":2}]}'                                |
      | 'POST'   | '{"array":[{"paramBigInteger":2}]}'                          | '{"array":[{"paramBigInteger":2}]}'                          |
      | 'PUT'    | '{"array":[{"paramBigInteger":2}]}'                          | '{"array":[{"paramBigInteger":2}]}'                          |
      | 'PATCH'  | '{"array":[{"paramBigInteger":2}]}'                          | '{"array":[{"paramBigInteger":2}]}'                          |
      | 'DELETE' | '{"array":[{"paramBigInteger":2}]}'                          | '{"array":[{"paramBigInteger":2}]}'                          |
      | 'POST'   | '{"array":[{"paramFloat":2.1}]}'                             | '{"array":[{"paramFloat":2.1}]}'                             |
      | 'PUT'    | '{"array":[{"paramFloat":2.1}]}'                             | '{"array":[{"paramFloat":2.1}]}'                             |
      | 'PATCH'  | '{"array":[{"paramFloat":2.1}]}'                             | '{"array":[{"paramFloat":2.1}]}'                             |
      | 'DELETE' | '{"array":[{"paramFloat":2.1}]}'                             | '{"array":[{"paramFloat":2.1}]}'                             |
      | 'POST'   | '{"array":[{"paramDouble":2.1}]}'                            | '{"array":[{"paramDouble":2.1}]}'                            |
      | 'PUT'    | '{"array":[{"paramDouble":2.1}]}'                            | '{"array":[{"paramDouble":2.1}]}'                            |
      | 'PATCH'  | '{"array":[{"paramDouble":2.1}]}'                            | '{"array":[{"paramDouble":2.1}]}'                            |
      | 'DELETE' | '{"array":[{"paramDouble":2.1}]}'                            | '{"array":[{"paramDouble":2.1}]}'                            |
      | 'POST'   | '{"array":[{"paramBigDecimal":2.1}]}'                        | '{"array":[{"paramBigDecimal":2.1}]}'                        |
      | 'PUT'    | '{"array":[{"paramBigDecimal":2.1}]}'                        | '{"array":[{"paramBigDecimal":2.1}]}'                        |
      | 'PATCH'  | '{"array":[{"paramBigDecimal":2.1}]}'                        | '{"array":[{"paramBigDecimal":2.1}]}'                        |
      | 'DELETE' | '{"array":[{"paramBigDecimal":2.1}]}'                        | '{"array":[{"paramBigDecimal":2.1}]}'                        |
      | 'POST'   | '{"array":[{"paramBoolean":true}]}'                          | '{"array":[{"paramBoolean":true}]}'                          |
      | 'PUT'    | '{"array":[{"paramBoolean":true}]}'                          | '{"array":[{"paramBoolean":true}]}'                          |
      | 'PATCH'  | '{"array":[{"paramBoolean":true}]}'                          | '{"array":[{"paramBoolean":true}]}'                          |
      | 'DELETE' | '{"array":[{"paramBoolean":true}]}'                          | '{"array":[{"paramBoolean":true}]}'                          |
      | 'POST'   | '{"array":[{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}]}' | '{"array":[{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}]}' |
      | 'PUT'    | '{"array":[{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}]}' | '{"array":[{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}]}' |
      | 'PATCH'  | '{"array":[{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}]}' | '{"array":[{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}]}' |
      | 'DELETE' | '{"array":[{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}]}' | '{"array":[{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}]}' |
      | 'POST'   | '{"array":[{"paramLocalDateTime":"2000-01-01T00:00:00Z"}]}'  | '{"array":[{"paramLocalDateTime":"2000-01-01T00:00:00Z"}]}'  |
      | 'PUT'    | '{"array":[{"paramLocalDateTime":"2000-01-01T00:00:00Z"}]}'  | '{"array":[{"paramLocalDateTime":"2000-01-01T00:00:00Z"}]}'  |
      | 'PATCH'  | '{"array":[{"paramLocalDateTime":"2000-01-01T00:00:00Z"}]}'  | '{"array":[{"paramLocalDateTime":"2000-01-01T00:00:00Z"}]}'  |
      | 'DELETE' | '{"array":[{"paramLocalDateTime":"2000-01-01T00:00:00Z"}]}'  | '{"array":[{"paramLocalDateTime":"2000-01-01T00:00:00Z"}]}'  |
      | 'POST'   | '{"array":[{"paramLocalDate":"2000-01-01"}]}'                | '{"array":[{"paramLocalDate":"2000-01-01"}]}'                |
      | 'PUT'    | '{"array":[{"paramLocalDate":"2000-01-01"}]}'                | '{"array":[{"paramLocalDate":"2000-01-01"}]}'                |
      | 'PATCH'  | '{"array":[{"paramLocalDate":"2000-01-01"}]}'                | '{"array":[{"paramLocalDate":"2000-01-01"}]}'                |
      | 'DELETE' | '{"array":[{"paramLocalDate":"2000-01-01"}]}'                | '{"array":[{"paramLocalDate":"2000-01-01"}]}'                |
      | 'POST'   | '{"array":[{"paramLocalTime":"13:00:00"}]}'                  | '{"array":[{"paramLocalTime":"13:00:00"}]}'                  |
      | 'PUT'    | '{"array":[{"paramLocalTime":"13:00:00"}]}'                  | '{"array":[{"paramLocalTime":"13:00:00"}]}'                  |
      | 'PATCH'  | '{"array":[{"paramLocalTime":"13:00:00"}]}'                  | '{"array":[{"paramLocalTime":"13:00:00"}]}'                  |
      | 'DELETE' | '{"array":[{"paramLocalTime":"13:00:00"}]}'                  | '{"array":[{"paramLocalTime":"13:00:00"}]}'                  |
      | 'POST'   | '{"array":[{"paramYear":"2000"}]}'                           | '{"array":[{"paramYear":"2000"}]}'                           |
      | 'PUT'    | '{"array":[{"paramYear":"2000"}]}'                           | '{"array":[{"paramYear":"2000"}]}'                           |
      | 'PATCH'  | '{"array":[{"paramYear":"2000"}]}'                           | '{"array":[{"paramYear":"2000"}]}'                           |
      | 'DELETE' | '{"array":[{"paramYear":"2000"}]}'                           | '{"array":[{"paramYear":"2000"}]}'                           |
      | 'POST'   | '{"array":[{"paramEnumAB":"A"}]}'                            | '{"array":[{"paramEnumAB":"A"}]}'                            |
      | 'PUT'    | '{"array":[{"paramEnumAB":"A"}]}'                            | '{"array":[{"paramEnumAB":"A"}]}'                            |
      | 'PATCH'  | '{"array":[{"paramEnumAB":"A"}]}'                            | '{"array":[{"paramEnumAB":"A"}]}'                            |
      | 'DELETE' | '{"array":[{"paramEnumAB":"A"}]}'                            | '{"array":[{"paramEnumAB":"A"}]}'                            |
      | 'POST'   | '{"array":[{"paramEnumLowerAB":"b"}]}'                       | '{"array":[{"paramEnumLowerAB":"b"}]}'                       |
      | 'PUT'    | '{"array":[{"paramEnumLowerAB":"b"}]}'                       | '{"array":[{"paramEnumLowerAB":"b"}]}'                       |
      | 'PATCH'  | '{"array":[{"paramEnumLowerAB":"b"}]}'                       | '{"array":[{"paramEnumLowerAB":"b"}]}'                       |
      | 'DELETE' | '{"array":[{"paramEnumLowerAB":"b"}]}'                       | '{"array":[{"paramEnumLowerAB":"b"}]}'                       |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/named-nested-optional-dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in a nested optional dto attribute from an array, both having the diferent names, a custom body-attibute name is defined by the endpoint
    and the service correctly maps the sent value with the declared data type, responding with a success message.

    Given the endpoint '/api/v1/request-body/named-nested-optional-dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | request body                                                    | received value                                               |
      | 'POST'   | '{"array":[{"param-string":"id"}]}'                             | '{"array":[{"paramString":"id"}]}'                           |
      | 'PUT'    | '{"array":[{"param-string":"id"}]}'                             | '{"array":[{"paramString":"id"}]}'                           |
      | 'PATCH'  | '{"array":[{"param-string":"id"}]}'                             | '{"array":[{"paramString":"id"}]}'                           |
      | 'DELETE' | '{"array":[{"param-string":"id"}]}'                             | '{"array":[{"paramString":"id"}]}'                           |
      | 'POST'   | '{"array":[{"param-integer":2}]}'                               | '{"array":[{"paramInteger":2}]}'                             |
      | 'PUT'    | '{"array":[{"param-integer":2}]}'                               | '{"array":[{"paramInteger":2}]}'                             |
      | 'PATCH'  | '{"array":[{"param-integer":2}]}'                               | '{"array":[{"paramInteger":2}]}'                             |
      | 'DELETE' | '{"array":[{"param-integer":2}]}'                               | '{"array":[{"paramInteger":2}]}'                             |
      | 'POST'   | '{"array":[{"param-long":2}]}'                                  | '{"array":[{"paramLong":2}]}'                                |
      | 'PUT'    | '{"array":[{"param-long":2}]}'                                  | '{"array":[{"paramLong":2}]}'                                |
      | 'PATCH'  | '{"array":[{"param-long":2}]}'                                  | '{"array":[{"paramLong":2}]}'                                |
      | 'DELETE' | '{"array":[{"param-long":2}]}'                                  | '{"array":[{"paramLong":2}]}'                                |
      | 'POST'   | '{"array":[{"param-big-integer":2}]}'                           | '{"array":[{"paramBigInteger":2}]}'                          |
      | 'PUT'    | '{"array":[{"param-big-integer":2}]}'                           | '{"array":[{"paramBigInteger":2}]}'                          |
      | 'PATCH'  | '{"array":[{"param-big-integer":2}]}'                           | '{"array":[{"paramBigInteger":2}]}'                          |
      | 'DELETE' | '{"array":[{"param-big-integer":2}]}'                           | '{"array":[{"paramBigInteger":2}]}'                          |
      | 'POST'   | '{"array":[{"param-float":2.1}]}'                               | '{"array":[{"paramFloat":2.1}]}'                             |
      | 'PUT'    | '{"array":[{"param-float":2.1}]}'                               | '{"array":[{"paramFloat":2.1}]}'                             |
      | 'PATCH'  | '{"array":[{"param-float":2.1}]}'                               | '{"array":[{"paramFloat":2.1}]}'                             |
      | 'DELETE' | '{"array":[{"param-float":2.1}]}'                               | '{"array":[{"paramFloat":2.1}]}'                             |
      | 'POST'   | '{"array":[{"param-double":2.1}]}'                              | '{"array":[{"paramDouble":2.1}]}'                            |
      | 'PUT'    | '{"array":[{"param-double":2.1}]}'                              | '{"array":[{"paramDouble":2.1}]}'                            |
      | 'PATCH'  | '{"array":[{"param-double":2.1}]}'                              | '{"array":[{"paramDouble":2.1}]}'                            |
      | 'DELETE' | '{"array":[{"param-double":2.1}]}'                              | '{"array":[{"paramDouble":2.1}]}'                            |
      | 'POST'   | '{"array":[{"param-big-decimal":2.1}]}'                         | '{"array":[{"paramBigDecimal":2.1}]}'                        |
      | 'PUT'    | '{"array":[{"param-big-decimal":2.1}]}'                         | '{"array":[{"paramBigDecimal":2.1}]}'                        |
      | 'PATCH'  | '{"array":[{"param-big-decimal":2.1}]}'                         | '{"array":[{"paramBigDecimal":2.1}]}'                        |
      | 'DELETE' | '{"array":[{"param-big-decimal":2.1}]}'                         | '{"array":[{"paramBigDecimal":2.1}]}'                        |
      | 'POST'   | '{"array":[{"param-boolean":true}]}'                            | '{"array":[{"paramBoolean":true}]}'                          |
      | 'PUT'    | '{"array":[{"param-boolean":true}]}'                            | '{"array":[{"paramBoolean":true}]}'                          |
      | 'PATCH'  | '{"array":[{"param-boolean":true}]}'                            | '{"array":[{"paramBoolean":true}]}'                          |
      | 'DELETE' | '{"array":[{"param-boolean":true}]}'                            | '{"array":[{"paramBoolean":true}]}'                          |
      | 'POST'   | '{"array":[{"param-offset-date-time":"2000-01-01T00:00:00Z"}]}' | '{"array":[{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}]}' |
      | 'PUT'    | '{"array":[{"param-offset-date-time":"2000-01-01T00:00:00Z"}]}' | '{"array":[{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}]}' |
      | 'PATCH'  | '{"array":[{"param-offset-date-time":"2000-01-01T00:00:00Z"}]}' | '{"array":[{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}]}' |
      | 'DELETE' | '{"array":[{"param-offset-date-time":"2000-01-01T00:00:00Z"}]}' | '{"array":[{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}]}' |
      | 'POST'   | '{"array":[{"param-local-date-time":"2000-01-01T00:00:00Z"}]}'  | '{"array":[{"paramLocalDateTime":"2000-01-01T00:00:00Z"}]}'  |
      | 'PUT'    | '{"array":[{"param-local-date-time":"2000-01-01T00:00:00Z"}]}'  | '{"array":[{"paramLocalDateTime":"2000-01-01T00:00:00Z"}]}'  |
      | 'PATCH'  | '{"array":[{"param-local-date-time":"2000-01-01T00:00:00Z"}]}'  | '{"array":[{"paramLocalDateTime":"2000-01-01T00:00:00Z"}]}'  |
      | 'DELETE' | '{"array":[{"param-local-date-time":"2000-01-01T00:00:00Z"}]}'  | '{"array":[{"paramLocalDateTime":"2000-01-01T00:00:00Z"}]}'  |
      | 'POST'   | '{"array":[{"param-local-date":"2000-01-01"}]}'                 | '{"array":[{"paramLocalDate":"2000-01-01"}]}'                |
      | 'PUT'    | '{"array":[{"param-local-date":"2000-01-01"}]}'                 | '{"array":[{"paramLocalDate":"2000-01-01"}]}'                |
      | 'PATCH'  | '{"array":[{"param-local-date":"2000-01-01"}]}'                 | '{"array":[{"paramLocalDate":"2000-01-01"}]}'                |
      | 'DELETE' | '{"array":[{"param-local-date":"2000-01-01"}]}'                 | '{"array":[{"paramLocalDate":"2000-01-01"}]}'                |
      | 'POST'   | '{"array":[{"param-local-time":"13:00:00"}]}'                   | '{"array":[{"paramLocalTime":"13:00:00"}]}'                  |
      | 'PUT'    | '{"array":[{"param-local-time":"13:00:00"}]}'                   | '{"array":[{"paramLocalTime":"13:00:00"}]}'                  |
      | 'PATCH'  | '{"array":[{"param-local-time":"13:00:00"}]}'                   | '{"array":[{"paramLocalTime":"13:00:00"}]}'                  |
      | 'DELETE' | '{"array":[{"param-local-time":"13:00:00"}]}'                   | '{"array":[{"paramLocalTime":"13:00:00"}]}'                  |
      | 'POST'   | '{"array":[{"param-year":"2000"}]}'                             | '{"array":[{"paramYear":"2000"}]}'                           |
      | 'PUT'    | '{"array":[{"param-year":"2000"}]}'                             | '{"array":[{"paramYear":"2000"}]}'                           |
      | 'PATCH'  | '{"array":[{"param-year":"2000"}]}'                             | '{"array":[{"paramYear":"2000"}]}'                           |
      | 'DELETE' | '{"array":[{"param-year":"2000"}]}'                             | '{"array":[{"paramYear":"2000"}]}'                           |
      | 'POST'   | '{"array":[{"param-enum-ab":"A"}]}'                             | '{"array":[{"paramEnumAB":"A"}]}'                            |
      | 'PUT'    | '{"array":[{"param-enum-ab":"A"}]}'                             | '{"array":[{"paramEnumAB":"A"}]}'                            |
      | 'PATCH'  | '{"array":[{"param-enum-ab":"A"}]}'                             | '{"array":[{"paramEnumAB":"A"}]}'                            |
      | 'DELETE' | '{"array":[{"param-enum-ab":"A"}]}'                             | '{"array":[{"paramEnumAB":"A"}]}'                            |
      | 'POST'   | '{"array":[{"param-enum-lower-ab":"b"}]}'                       | '{"array":[{"paramEnumLowerAB":"b"}]}'                       |
      | 'PUT'    | '{"array":[{"param-enum-lower-ab":"b"}]}'                       | '{"array":[{"paramEnumLowerAB":"b"}]}'                       |
      | 'PATCH'  | '{"array":[{"param-enum-lower-ab":"b"}]}'                       | '{"array":[{"paramEnumLowerAB":"b"}]}'                       |
      | 'DELETE' | '{"array":[{"param-enum-lower-ab":"b"}]}'                       | '{"array":[{"paramEnumLowerAB":"b"}]}'                       |
