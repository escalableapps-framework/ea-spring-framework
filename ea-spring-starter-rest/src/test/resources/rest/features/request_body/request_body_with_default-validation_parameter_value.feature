Feature: Request body with default-validation parameter value
  
  The REST service receives data via the request body and it does not accomplish with some constraint. The service responds with an error message.
  
   Rule: The conversion between data types is given by the following table
  
  | parameter type | java type      | validation              | validation                                   |
  |----------------|----------------|-------------------------|----------------------------------------------|
  | string         | String         | @Size                   | @Size(max = 1)                               |
  | number         | Integer        | @Min @Max               | @Max(1)                                      |
  | number         | Long           | @Min @Max               | @Max(1)                                      |
  | number         | BigInteger     | @DecimalMin @DecimalMax | @DecimalMax("1")                             |
  | number         | Float          |                         |                                              |
  | number         | Double         |                         |                                              |
  | number         | BigDecimal     | @DecimalMin @DecimalMax | @DecimalMax("9.9")                           |
  | boolean        | Boolean        |                         |                                              |
  | ISO-8601       | OffsetDateTime | @DatetimeRange          | @DatetimeRange(max = "2000-01-01T00:00:00Z") |
  | ISO-8601       | LocalDateTime  | @DatetimeRange          | @DatetimeRange(max = "2000-01-01T00:00:00Z") |
  | ISO-8601       | LocalDate      | @DateRange              | @DateRange(max = "2000-01-01")               |
  | ISO-8601       | LocalTime      |                         |                                              |
  | ISO-8601       | Year           | @YearRange              | @YearRange(max = "2000")                     |
  | string         | ENUM           |                         |                                              |

  Background: 
    Given the default timezone 'America/Puerto_Rico'

  Scenario Outline: The <method> call is made to the /api/v1/request-body/constrained/dto-attribute<path> endpoint with the body <request body> (RequestBodyConstrainedNotNullRestApi.class)
    this endpoint receives the request body-attibute in a dto attribute, both having the same name
    and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/request-body/constrained/dto-attribute'<path>
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | request body                                     | message                                                                                    |
      | 'POST'   | '/paramString'         | '{"paramString":"ab"}'                           | 'paramString: size must be between 0 and 1'                                                |
      | 'PUT'    | '/paramString'         | '{"paramString":"ab"}'                           | 'paramString: size must be between 0 and 1'                                                |
      | 'PATCH'  | '/paramString'         | '{"paramString":"ab"}'                           | 'paramString: size must be between 0 and 1'                                                |
      | 'DELETE' | '/paramString'         | '{"paramString":"ab"}'                           | 'paramString: size must be between 0 and 1'                                                |
      | 'POST'   | '/paramInteger'        | '{"paramInteger":10}'                            | 'paramInteger: must be less than or equal to 1'                                            |
      | 'PUT'    | '/paramInteger'        | '{"paramInteger":10}'                            | 'paramInteger: must be less than or equal to 1'                                            |
      | 'PATCH'  | '/paramInteger'        | '{"paramInteger":10}'                            | 'paramInteger: must be less than or equal to 1'                                            |
      | 'DELETE' | '/paramInteger'        | '{"paramInteger":10}'                            | 'paramInteger: must be less than or equal to 1'                                            |
      | 'POST'   | '/paramLong'           | '{"paramLong":10}'                               | 'paramLong: must be less than or equal to 1'                                               |
      | 'PUT'    | '/paramLong'           | '{"paramLong":10}'                               | 'paramLong: must be less than or equal to 1'                                               |
      | 'PATCH'  | '/paramLong'           | '{"paramLong":10}'                               | 'paramLong: must be less than or equal to 1'                                               |
      | 'DELETE' | '/paramLong'           | '{"paramLong":10}'                               | 'paramLong: must be less than or equal to 1'                                               |
      | 'POST'   | '/paramBigInteger'     | '{"paramBigInteger":10}'                         | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'PUT'    | '/paramBigInteger'     | '{"paramBigInteger":10}'                         | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'PATCH'  | '/paramBigInteger'     | '{"paramBigInteger":10}'                         | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'DELETE' | '/paramBigInteger'     | '{"paramBigInteger":10}'                         | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'POST'   | '/paramBigDecimal'     | '{"paramBigDecimal":10}'                         | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PUT'    | '/paramBigDecimal'     | '{"paramBigDecimal":10}'                         | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PATCH'  | '/paramBigDecimal'     | '{"paramBigDecimal":10}'                         | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'DELETE' | '/paramBigDecimal'     | '{"paramBigDecimal":10}'                         | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'POST'   | '/paramOffsetDateTime' | '{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PUT'    | '/paramOffsetDateTime' | '{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PATCH'  | '/paramOffsetDateTime' | '{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'DELETE' | '/paramOffsetDateTime' | '{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'POST'   | '/paramLocalDateTime'  | '{"paramLocalDateTime":"2100-01-01T00:00:00Z"}'  | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PUT'    | '/paramLocalDateTime'  | '{"paramLocalDateTime":"2100-01-01T00:00:00Z"}'  | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PATCH'  | '/paramLocalDateTime'  | '{"paramLocalDateTime":"2100-01-01T00:00:00Z"}'  | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'DELETE' | '/paramLocalDateTime'  | '{"paramLocalDateTime":"2100-01-01T00:00:00Z"}'  | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'POST'   | '/paramLocalDate'      | '{"paramLocalDate":"2100-01-01"}'                | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PUT'    | '/paramLocalDate'      | '{"paramLocalDate":"2100-01-01"}'                | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PATCH'  | '/paramLocalDate'      | '{"paramLocalDate":"2100-01-01"}'                | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'DELETE' | '/paramLocalDate'      | '{"paramLocalDate":"2100-01-01"}'                | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'POST'   | '/paramYear'           | '{"paramYear":"2100"}'                           | 'paramYear: must be between -999999999 and 2000'                                           |
      | 'PUT'    | '/paramYear'           | '{"paramYear":"2100"}'                           | 'paramYear: must be between -999999999 and 2000'                                           |
      | 'PATCH'  | '/paramYear'           | '{"paramYear":"2100"}'                           | 'paramYear: must be between -999999999 and 2000'                                           |
      | 'DELETE' | '/paramYear'           | '{"paramYear":"2100"}'                           | 'paramYear: must be between -999999999 and 2000'                                           |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/constrained/named-dto-attribute<path> endpoint with the body <request body> (RequestBodyConstrainedNotNullRestApi.class)
    this endpoint receives the request body-attibute in a dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint 
    and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/request-body/constrained/named-dto-attribute'<path>
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | request body                                        | message                                                                                    |
      | 'POST'   | '/paramString'         | '{"param-string":"ab"}'                             | 'paramString: size must be between 0 and 1'                                                |
      | 'PUT'    | '/paramString'         | '{"param-string":"ab"}'                             | 'paramString: size must be between 0 and 1'                                                |
      | 'PATCH'  | '/paramString'         | '{"param-string":"ab"}'                             | 'paramString: size must be between 0 and 1'                                                |
      | 'DELETE' | '/paramString'         | '{"param-string":"ab"}'                             | 'paramString: size must be between 0 and 1'                                                |
      | 'POST'   | '/paramInteger'        | '{"param-integer":10}'                              | 'paramInteger: must be less than or equal to 1'                                            |
      | 'PUT'    | '/paramInteger'        | '{"param-integer":10}'                              | 'paramInteger: must be less than or equal to 1'                                            |
      | 'PATCH'  | '/paramInteger'        | '{"param-integer":10}'                              | 'paramInteger: must be less than or equal to 1'                                            |
      | 'DELETE' | '/paramInteger'        | '{"param-integer":10}'                              | 'paramInteger: must be less than or equal to 1'                                            |
      | 'POST'   | '/paramLong'           | '{"param-long":10}'                                 | 'paramLong: must be less than or equal to 1'                                               |
      | 'PUT'    | '/paramLong'           | '{"param-long":10}'                                 | 'paramLong: must be less than or equal to 1'                                               |
      | 'PATCH'  | '/paramLong'           | '{"param-long":10}'                                 | 'paramLong: must be less than or equal to 1'                                               |
      | 'DELETE' | '/paramLong'           | '{"param-long":10}'                                 | 'paramLong: must be less than or equal to 1'                                               |
      | 'POST'   | '/paramBigInteger'     | '{"param-big-integer":10}'                          | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'PUT'    | '/paramBigInteger'     | '{"param-big-integer":10}'                          | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'PATCH'  | '/paramBigInteger'     | '{"param-big-integer":10}'                          | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'DELETE' | '/paramBigInteger'     | '{"param-big-integer":10}'                          | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'POST'   | '/paramBigDecimal'     | '{"param-big-decimal":10}'                          | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PUT'    | '/paramBigDecimal'     | '{"param-big-decimal":10}'                          | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PATCH'  | '/paramBigDecimal'     | '{"param-big-decimal":10}'                          | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'DELETE' | '/paramBigDecimal'     | '{"param-big-decimal":10}'                          | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'POST'   | '/paramOffsetDateTime' | '{"param-offset-date-time":"2100-01-01T00:00:00Z"}' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PUT'    | '/paramOffsetDateTime' | '{"param-offset-date-time":"2100-01-01T00:00:00Z"}' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PATCH'  | '/paramOffsetDateTime' | '{"param-offset-date-time":"2100-01-01T00:00:00Z"}' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'DELETE' | '/paramOffsetDateTime' | '{"param-offset-date-time":"2100-01-01T00:00:00Z"}' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'POST'   | '/paramLocalDateTime'  | '{"param-local-date-time":"2100-01-01T00:00:00Z"}'  | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PUT'    | '/paramLocalDateTime'  | '{"param-local-date-time":"2100-01-01T00:00:00Z"}'  | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PATCH'  | '/paramLocalDateTime'  | '{"param-local-date-time":"2100-01-01T00:00:00Z"}'  | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'DELETE' | '/paramLocalDateTime'  | '{"param-local-date-time":"2100-01-01T00:00:00Z"}'  | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'POST'   | '/paramLocalDate'      | '{"param-local-date":"2100-01-01"}'                 | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PUT'    | '/paramLocalDate'      | '{"param-local-date":"2100-01-01"}'                 | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PATCH'  | '/paramLocalDate'      | '{"param-local-date":"2100-01-01"}'                 | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'DELETE' | '/paramLocalDate'      | '{"param-local-date":"2100-01-01"}'                 | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'POST'   | '/paramYear'           | '{"param-year":"2100"}'                             | 'paramYear: must be between -999999999 and 2000'                                           |
      | 'PUT'    | '/paramYear'           | '{"param-year":"2100"}'                             | 'paramYear: must be between -999999999 and 2000'                                           |
      | 'PATCH'  | '/paramYear'           | '{"param-year":"2100"}'                             | 'paramYear: must be between -999999999 and 2000'                                           |
      | 'DELETE' | '/paramYear'           | '{"param-year":"2100"}'                             | 'paramYear: must be between -999999999 and 2000'                                           |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/constrained/optional-dto-attribute<path> endpoint with the body <request body> (RequestBodyConstrainedNotNullRestApi.class)
    this endpoint receives the request body-attibute in an optional dto attribute, both having the same name
    and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/request-body/constrained/optional-dto-attribute'<path>
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | request body                                     | message                                                                                    |
      | 'POST'   | '/paramString'         | '{"paramString":"ab"}'                           | 'paramString: size must be between 0 and 1'                                                |
      | 'PUT'    | '/paramString'         | '{"paramString":"ab"}'                           | 'paramString: size must be between 0 and 1'                                                |
      | 'PATCH'  | '/paramString'         | '{"paramString":"ab"}'                           | 'paramString: size must be between 0 and 1'                                                |
      | 'DELETE' | '/paramString'         | '{"paramString":"ab"}'                           | 'paramString: size must be between 0 and 1'                                                |
      | 'POST'   | '/paramInteger'        | '{"paramInteger":10}'                            | 'paramInteger: must be less than or equal to 1'                                            |
      | 'PUT'    | '/paramInteger'        | '{"paramInteger":10}'                            | 'paramInteger: must be less than or equal to 1'                                            |
      | 'PATCH'  | '/paramInteger'        | '{"paramInteger":10}'                            | 'paramInteger: must be less than or equal to 1'                                            |
      | 'DELETE' | '/paramInteger'        | '{"paramInteger":10}'                            | 'paramInteger: must be less than or equal to 1'                                            |
      | 'POST'   | '/paramLong'           | '{"paramLong":10}'                               | 'paramLong: must be less than or equal to 1'                                               |
      | 'PUT'    | '/paramLong'           | '{"paramLong":10}'                               | 'paramLong: must be less than or equal to 1'                                               |
      | 'PATCH'  | '/paramLong'           | '{"paramLong":10}'                               | 'paramLong: must be less than or equal to 1'                                               |
      | 'DELETE' | '/paramLong'           | '{"paramLong":10}'                               | 'paramLong: must be less than or equal to 1'                                               |
      | 'POST'   | '/paramBigInteger'     | '{"paramBigInteger":10}'                         | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'PUT'    | '/paramBigInteger'     | '{"paramBigInteger":10}'                         | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'PATCH'  | '/paramBigInteger'     | '{"paramBigInteger":10}'                         | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'DELETE' | '/paramBigInteger'     | '{"paramBigInteger":10}'                         | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'POST'   | '/paramBigDecimal'     | '{"paramBigDecimal":10}'                         | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PUT'    | '/paramBigDecimal'     | '{"paramBigDecimal":10}'                         | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PATCH'  | '/paramBigDecimal'     | '{"paramBigDecimal":10}'                         | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'DELETE' | '/paramBigDecimal'     | '{"paramBigDecimal":10}'                         | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'POST'   | '/paramOffsetDateTime' | '{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PUT'    | '/paramOffsetDateTime' | '{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PATCH'  | '/paramOffsetDateTime' | '{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'DELETE' | '/paramOffsetDateTime' | '{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'POST'   | '/paramLocalDateTime'  | '{"paramLocalDateTime":"2100-01-01T00:00:00Z"}'  | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PUT'    | '/paramLocalDateTime'  | '{"paramLocalDateTime":"2100-01-01T00:00:00Z"}'  | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PATCH'  | '/paramLocalDateTime'  | '{"paramLocalDateTime":"2100-01-01T00:00:00Z"}'  | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'DELETE' | '/paramLocalDateTime'  | '{"paramLocalDateTime":"2100-01-01T00:00:00Z"}'  | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'POST'   | '/paramLocalDate'      | '{"paramLocalDate":"2100-01-01"}'                | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PUT'    | '/paramLocalDate'      | '{"paramLocalDate":"2100-01-01"}'                | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PATCH'  | '/paramLocalDate'      | '{"paramLocalDate":"2100-01-01"}'                | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'DELETE' | '/paramLocalDate'      | '{"paramLocalDate":"2100-01-01"}'                | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'POST'   | '/paramYear'           | '{"paramYear":"2100"}'                           | 'paramYear: must be between -999999999 and 2000'                                           |
      | 'PUT'    | '/paramYear'           | '{"paramYear":"2100"}'                           | 'paramYear: must be between -999999999 and 2000'                                           |
      | 'PATCH'  | '/paramYear'           | '{"paramYear":"2100"}'                           | 'paramYear: must be between -999999999 and 2000'                                           |
      | 'DELETE' | '/paramYear'           | '{"paramYear":"2100"}'                           | 'paramYear: must be between -999999999 and 2000'                                           |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/constrained/named-optional-dto-attribute<path> endpoint with the body <request body> (RequestBodyConstrainedNotNullRestApi.class)
    this endpoint receives the request body-attibute in an optional dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
    and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/request-body/constrained/named-optional-dto-attribute'<path>
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | request body                                        | message                                                                                    |
      | 'POST'   | '/paramString'         | '{"param-string":"ab"}'                             | 'paramString: size must be between 0 and 1'                                                |
      | 'PUT'    | '/paramString'         | '{"param-string":"ab"}'                             | 'paramString: size must be between 0 and 1'                                                |
      | 'PATCH'  | '/paramString'         | '{"param-string":"ab"}'                             | 'paramString: size must be between 0 and 1'                                                |
      | 'DELETE' | '/paramString'         | '{"param-string":"ab"}'                             | 'paramString: size must be between 0 and 1'                                                |
      | 'POST'   | '/paramInteger'        | '{"param-integer":10}'                              | 'paramInteger: must be less than or equal to 1'                                            |
      | 'PUT'    | '/paramInteger'        | '{"param-integer":10}'                              | 'paramInteger: must be less than or equal to 1'                                            |
      | 'PATCH'  | '/paramInteger'        | '{"param-integer":10}'                              | 'paramInteger: must be less than or equal to 1'                                            |
      | 'DELETE' | '/paramInteger'        | '{"param-integer":10}'                              | 'paramInteger: must be less than or equal to 1'                                            |
      | 'POST'   | '/paramLong'           | '{"param-long":10}'                                 | 'paramLong: must be less than or equal to 1'                                               |
      | 'PUT'    | '/paramLong'           | '{"param-long":10}'                                 | 'paramLong: must be less than or equal to 1'                                               |
      | 'PATCH'  | '/paramLong'           | '{"param-long":10}'                                 | 'paramLong: must be less than or equal to 1'                                               |
      | 'DELETE' | '/paramLong'           | '{"param-long":10}'                                 | 'paramLong: must be less than or equal to 1'                                               |
      | 'POST'   | '/paramBigInteger'     | '{"param-big-integer":10}'                          | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'PUT'    | '/paramBigInteger'     | '{"param-big-integer":10}'                          | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'PATCH'  | '/paramBigInteger'     | '{"param-big-integer":10}'                          | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'DELETE' | '/paramBigInteger'     | '{"param-big-integer":10}'                          | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'POST'   | '/paramBigDecimal'     | '{"param-big-decimal":10}'                          | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PUT'    | '/paramBigDecimal'     | '{"param-big-decimal":10}'                          | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PATCH'  | '/paramBigDecimal'     | '{"param-big-decimal":10}'                          | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'DELETE' | '/paramBigDecimal'     | '{"param-big-decimal":10}'                          | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'POST'   | '/paramOffsetDateTime' | '{"param-offset-date-time":"2100-01-01T00:00:00Z"}' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PUT'    | '/paramOffsetDateTime' | '{"param-offset-date-time":"2100-01-01T00:00:00Z"}' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PATCH'  | '/paramOffsetDateTime' | '{"param-offset-date-time":"2100-01-01T00:00:00Z"}' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'DELETE' | '/paramOffsetDateTime' | '{"param-offset-date-time":"2100-01-01T00:00:00Z"}' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'POST'   | '/paramLocalDateTime'  | '{"param-local-date-time":"2100-01-01T00:00:00Z"}'  | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PUT'    | '/paramLocalDateTime'  | '{"param-local-date-time":"2100-01-01T00:00:00Z"}'  | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PATCH'  | '/paramLocalDateTime'  | '{"param-local-date-time":"2100-01-01T00:00:00Z"}'  | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'DELETE' | '/paramLocalDateTime'  | '{"param-local-date-time":"2100-01-01T00:00:00Z"}'  | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'POST'   | '/paramLocalDate'      | '{"param-local-date":"2100-01-01"}'                 | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PUT'    | '/paramLocalDate'      | '{"param-local-date":"2100-01-01"}'                 | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PATCH'  | '/paramLocalDate'      | '{"param-local-date":"2100-01-01"}'                 | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'DELETE' | '/paramLocalDate'      | '{"param-local-date":"2100-01-01"}'                 | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'POST'   | '/paramYear'           | '{"param-year":"2100"}'                             | 'paramYear: must be between -999999999 and 2000'                                           |
      | 'PUT'    | '/paramYear'           | '{"param-year":"2100"}'                             | 'paramYear: must be between -999999999 and 2000'                                           |
      | 'PATCH'  | '/paramYear'           | '{"param-year":"2100"}'                             | 'paramYear: must be between -999999999 and 2000'                                           |
      | 'DELETE' | '/paramYear'           | '{"param-year":"2100"}'                             | 'paramYear: must be between -999999999 and 2000'                                           |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/constrained/nested-dto-attribute<path> endpoint with the body <request body> (RequestBodyConstrainedNotNullRestApi.class)
    this endpoint receives the request body-attibute in a nested dto attribute, both having the same name
    and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/request-body/constrained/nested-dto-attribute'<path>
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | request body                                                | message                                                                                           |
      | 'POST'   | '/paramString'         | '{"nested":{"paramString":"ab"}}'                           | 'nested.paramString: size must be between 0 and 1'                                                |
      | 'PUT'    | '/paramString'         | '{"nested":{"paramString":"ab"}}'                           | 'nested.paramString: size must be between 0 and 1'                                                |
      | 'PATCH'  | '/paramString'         | '{"nested":{"paramString":"ab"}}'                           | 'nested.paramString: size must be between 0 and 1'                                                |
      | 'DELETE' | '/paramString'         | '{"nested":{"paramString":"ab"}}'                           | 'nested.paramString: size must be between 0 and 1'                                                |
      | 'POST'   | '/paramInteger'        | '{"nested":{"paramInteger":10}}'                            | 'nested.paramInteger: must be less than or equal to 1'                                            |
      | 'PUT'    | '/paramInteger'        | '{"nested":{"paramInteger":10}}'                            | 'nested.paramInteger: must be less than or equal to 1'                                            |
      | 'PATCH'  | '/paramInteger'        | '{"nested":{"paramInteger":10}}'                            | 'nested.paramInteger: must be less than or equal to 1'                                            |
      | 'DELETE' | '/paramInteger'        | '{"nested":{"paramInteger":10}}'                            | 'nested.paramInteger: must be less than or equal to 1'                                            |
      | 'POST'   | '/paramLong'           | '{"nested":{"paramLong":10}}'                               | 'nested.paramLong: must be less than or equal to 1'                                               |
      | 'PUT'    | '/paramLong'           | '{"nested":{"paramLong":10}}'                               | 'nested.paramLong: must be less than or equal to 1'                                               |
      | 'PATCH'  | '/paramLong'           | '{"nested":{"paramLong":10}}'                               | 'nested.paramLong: must be less than or equal to 1'                                               |
      | 'DELETE' | '/paramLong'           | '{"nested":{"paramLong":10}}'                               | 'nested.paramLong: must be less than or equal to 1'                                               |
      | 'POST'   | '/paramBigInteger'     | '{"nested":{"paramBigInteger":10}}'                         | 'nested.paramBigInteger: must be less than or equal to 1'                                         |
      | 'PUT'    | '/paramBigInteger'     | '{"nested":{"paramBigInteger":10}}'                         | 'nested.paramBigInteger: must be less than or equal to 1'                                         |
      | 'PATCH'  | '/paramBigInteger'     | '{"nested":{"paramBigInteger":10}}'                         | 'nested.paramBigInteger: must be less than or equal to 1'                                         |
      | 'DELETE' | '/paramBigInteger'     | '{"nested":{"paramBigInteger":10}}'                         | 'nested.paramBigInteger: must be less than or equal to 1'                                         |
      | 'POST'   | '/paramBigDecimal'     | '{"nested":{"paramBigDecimal":10}}'                         | 'nested.paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PUT'    | '/paramBigDecimal'     | '{"nested":{"paramBigDecimal":10}}'                         | 'nested.paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PATCH'  | '/paramBigDecimal'     | '{"nested":{"paramBigDecimal":10}}'                         | 'nested.paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'DELETE' | '/paramBigDecimal'     | '{"nested":{"paramBigDecimal":10}}'                         | 'nested.paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'POST'   | '/paramOffsetDateTime' | '{"nested":{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}}' | 'nested.paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PUT'    | '/paramOffsetDateTime' | '{"nested":{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}}' | 'nested.paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PATCH'  | '/paramOffsetDateTime' | '{"nested":{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}}' | 'nested.paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'DELETE' | '/paramOffsetDateTime' | '{"nested":{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}}' | 'nested.paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'POST'   | '/paramLocalDateTime'  | '{"nested":{"paramLocalDateTime":"2100-01-01T00:00:00Z"}}'  | 'nested.paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PUT'    | '/paramLocalDateTime'  | '{"nested":{"paramLocalDateTime":"2100-01-01T00:00:00Z"}}'  | 'nested.paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PATCH'  | '/paramLocalDateTime'  | '{"nested":{"paramLocalDateTime":"2100-01-01T00:00:00Z"}}'  | 'nested.paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'DELETE' | '/paramLocalDateTime'  | '{"nested":{"paramLocalDateTime":"2100-01-01T00:00:00Z"}}'  | 'nested.paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'POST'   | '/paramLocalDate'      | '{"nested":{"paramLocalDate":"2100-01-01"}}'                | 'nested.paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PUT'    | '/paramLocalDate'      | '{"nested":{"paramLocalDate":"2100-01-01"}}'                | 'nested.paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PATCH'  | '/paramLocalDate'      | '{"nested":{"paramLocalDate":"2100-01-01"}}'                | 'nested.paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'DELETE' | '/paramLocalDate'      | '{"nested":{"paramLocalDate":"2100-01-01"}}'                | 'nested.paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'POST'   | '/paramYear'           | '{"nested":{"paramYear":"2100"}}'                           | 'nested.paramYear: must be between -999999999 and 2000'                                           |
      | 'PUT'    | '/paramYear'           | '{"nested":{"paramYear":"2100"}}'                           | 'nested.paramYear: must be between -999999999 and 2000'                                           |
      | 'PATCH'  | '/paramYear'           | '{"nested":{"paramYear":"2100"}}'                           | 'nested.paramYear: must be between -999999999 and 2000'                                           |
      | 'DELETE' | '/paramYear'           | '{"nested":{"paramYear":"2100"}}'                           | 'nested.paramYear: must be between -999999999 and 2000'                                           |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/constrained/named-nested-dto-attribute<path> endpoint with the body <request body> (RequestBodyConstrainedNotNullRestApi.class)
    this endpoint receives the request body-attibute in a nested dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
    and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/request-body/constrained/named-nested-dto-attribute'<path>
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | request body                                                   | message                                                                                           |
      | 'POST'   | '/paramString'         | '{"nested":{"param-string":"ab"}}'                             | 'nested.paramString: size must be between 0 and 1'                                                |
      | 'PUT'    | '/paramString'         | '{"nested":{"param-string":"ab"}}'                             | 'nested.paramString: size must be between 0 and 1'                                                |
      | 'PATCH'  | '/paramString'         | '{"nested":{"param-string":"ab"}}'                             | 'nested.paramString: size must be between 0 and 1'                                                |
      | 'DELETE' | '/paramString'         | '{"nested":{"param-string":"ab"}}'                             | 'nested.paramString: size must be between 0 and 1'                                                |
      | 'POST'   | '/paramInteger'        | '{"nested":{"param-integer":10}}'                              | 'nested.paramInteger: must be less than or equal to 1'                                            |
      | 'PUT'    | '/paramInteger'        | '{"nested":{"param-integer":10}}'                              | 'nested.paramInteger: must be less than or equal to 1'                                            |
      | 'PATCH'  | '/paramInteger'        | '{"nested":{"param-integer":10}}'                              | 'nested.paramInteger: must be less than or equal to 1'                                            |
      | 'DELETE' | '/paramInteger'        | '{"nested":{"param-integer":10}}'                              | 'nested.paramInteger: must be less than or equal to 1'                                            |
      | 'POST'   | '/paramLong'           | '{"nested":{"param-long":10}}'                                 | 'nested.paramLong: must be less than or equal to 1'                                               |
      | 'PUT'    | '/paramLong'           | '{"nested":{"param-long":10}}'                                 | 'nested.paramLong: must be less than or equal to 1'                                               |
      | 'PATCH'  | '/paramLong'           | '{"nested":{"param-long":10}}'                                 | 'nested.paramLong: must be less than or equal to 1'                                               |
      | 'DELETE' | '/paramLong'           | '{"nested":{"param-long":10}}'                                 | 'nested.paramLong: must be less than or equal to 1'                                               |
      | 'POST'   | '/paramBigInteger'     | '{"nested":{"param-big-integer":10}}'                          | 'nested.paramBigInteger: must be less than or equal to 1'                                         |
      | 'PUT'    | '/paramBigInteger'     | '{"nested":{"param-big-integer":10}}'                          | 'nested.paramBigInteger: must be less than or equal to 1'                                         |
      | 'PATCH'  | '/paramBigInteger'     | '{"nested":{"param-big-integer":10}}'                          | 'nested.paramBigInteger: must be less than or equal to 1'                                         |
      | 'DELETE' | '/paramBigInteger'     | '{"nested":{"param-big-integer":10}}'                          | 'nested.paramBigInteger: must be less than or equal to 1'                                         |
      | 'POST'   | '/paramBigDecimal'     | '{"nested":{"param-big-decimal":10}}'                          | 'nested.paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PUT'    | '/paramBigDecimal'     | '{"nested":{"param-big-decimal":10}}'                          | 'nested.paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PATCH'  | '/paramBigDecimal'     | '{"nested":{"param-big-decimal":10}}'                          | 'nested.paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'DELETE' | '/paramBigDecimal'     | '{"nested":{"param-big-decimal":10}}'                          | 'nested.paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'POST'   | '/paramOffsetDateTime' | '{"nested":{"param-offset-date-time":"2100-01-01T00:00:00Z"}}' | 'nested.paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PUT'    | '/paramOffsetDateTime' | '{"nested":{"param-offset-date-time":"2100-01-01T00:00:00Z"}}' | 'nested.paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PATCH'  | '/paramOffsetDateTime' | '{"nested":{"param-offset-date-time":"2100-01-01T00:00:00Z"}}' | 'nested.paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'DELETE' | '/paramOffsetDateTime' | '{"nested":{"param-offset-date-time":"2100-01-01T00:00:00Z"}}' | 'nested.paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'POST'   | '/paramLocalDateTime'  | '{"nested":{"param-local-date-time":"2100-01-01T00:00:00Z"}}'  | 'nested.paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PUT'    | '/paramLocalDateTime'  | '{"nested":{"param-local-date-time":"2100-01-01T00:00:00Z"}}'  | 'nested.paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PATCH'  | '/paramLocalDateTime'  | '{"nested":{"param-local-date-time":"2100-01-01T00:00:00Z"}}'  | 'nested.paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'DELETE' | '/paramLocalDateTime'  | '{"nested":{"param-local-date-time":"2100-01-01T00:00:00Z"}}'  | 'nested.paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'POST'   | '/paramLocalDate'      | '{"nested":{"param-local-date":"2100-01-01"}}'                 | 'nested.paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PUT'    | '/paramLocalDate'      | '{"nested":{"param-local-date":"2100-01-01"}}'                 | 'nested.paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PATCH'  | '/paramLocalDate'      | '{"nested":{"param-local-date":"2100-01-01"}}'                 | 'nested.paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'DELETE' | '/paramLocalDate'      | '{"nested":{"param-local-date":"2100-01-01"}}'                 | 'nested.paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'POST'   | '/paramYear'           | '{"nested":{"param-year":"2100"}}'                             | 'nested.paramYear: must be between -999999999 and 2000'                                           |
      | 'PUT'    | '/paramYear'           | '{"nested":{"param-year":"2100"}}'                             | 'nested.paramYear: must be between -999999999 and 2000'                                           |
      | 'PATCH'  | '/paramYear'           | '{"nested":{"param-year":"2100"}}'                             | 'nested.paramYear: must be between -999999999 and 2000'                                           |
      | 'DELETE' | '/paramYear'           | '{"nested":{"param-year":"2100"}}'                             | 'nested.paramYear: must be between -999999999 and 2000'                                           |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/constrained/nested-optional-dto-attribute<path> endpoint with the body <request body> (RequestBodyConstrainedNotNullRestApi.class)
     this endpoint receives the request body-attibute in a nested optional dto attribute, both having the same name
     and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/request-body/constrained/nested-optional-dto-attribute'<path>
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | request body                                                | message                                                                                           |
      | 'POST'   | '/paramString'         | '{"nested":{"paramString":"ab"}}'                           | 'nested.paramString: size must be between 0 and 1'                                                |
      | 'PUT'    | '/paramString'         | '{"nested":{"paramString":"ab"}}'                           | 'nested.paramString: size must be between 0 and 1'                                                |
      | 'PATCH'  | '/paramString'         | '{"nested":{"paramString":"ab"}}'                           | 'nested.paramString: size must be between 0 and 1'                                                |
      | 'DELETE' | '/paramString'         | '{"nested":{"paramString":"ab"}}'                           | 'nested.paramString: size must be between 0 and 1'                                                |
      | 'POST'   | '/paramInteger'        | '{"nested":{"paramInteger":10}}'                            | 'nested.paramInteger: must be less than or equal to 1'                                            |
      | 'PUT'    | '/paramInteger'        | '{"nested":{"paramInteger":10}}'                            | 'nested.paramInteger: must be less than or equal to 1'                                            |
      | 'PATCH'  | '/paramInteger'        | '{"nested":{"paramInteger":10}}'                            | 'nested.paramInteger: must be less than or equal to 1'                                            |
      | 'DELETE' | '/paramInteger'        | '{"nested":{"paramInteger":10}}'                            | 'nested.paramInteger: must be less than or equal to 1'                                            |
      | 'POST'   | '/paramLong'           | '{"nested":{"paramLong":10}}'                               | 'nested.paramLong: must be less than or equal to 1'                                               |
      | 'PUT'    | '/paramLong'           | '{"nested":{"paramLong":10}}'                               | 'nested.paramLong: must be less than or equal to 1'                                               |
      | 'PATCH'  | '/paramLong'           | '{"nested":{"paramLong":10}}'                               | 'nested.paramLong: must be less than or equal to 1'                                               |
      | 'DELETE' | '/paramLong'           | '{"nested":{"paramLong":10}}'                               | 'nested.paramLong: must be less than or equal to 1'                                               |
      | 'POST'   | '/paramBigInteger'     | '{"nested":{"paramBigInteger":10}}'                         | 'nested.paramBigInteger: must be less than or equal to 1'                                         |
      | 'PUT'    | '/paramBigInteger'     | '{"nested":{"paramBigInteger":10}}'                         | 'nested.paramBigInteger: must be less than or equal to 1'                                         |
      | 'PATCH'  | '/paramBigInteger'     | '{"nested":{"paramBigInteger":10}}'                         | 'nested.paramBigInteger: must be less than or equal to 1'                                         |
      | 'DELETE' | '/paramBigInteger'     | '{"nested":{"paramBigInteger":10}}'                         | 'nested.paramBigInteger: must be less than or equal to 1'                                         |
      | 'POST'   | '/paramBigDecimal'     | '{"nested":{"paramBigDecimal":10}}'                         | 'nested.paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PUT'    | '/paramBigDecimal'     | '{"nested":{"paramBigDecimal":10}}'                         | 'nested.paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PATCH'  | '/paramBigDecimal'     | '{"nested":{"paramBigDecimal":10}}'                         | 'nested.paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'DELETE' | '/paramBigDecimal'     | '{"nested":{"paramBigDecimal":10}}'                         | 'nested.paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'POST'   | '/paramOffsetDateTime' | '{"nested":{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}}' | 'nested.paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PUT'    | '/paramOffsetDateTime' | '{"nested":{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}}' | 'nested.paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PATCH'  | '/paramOffsetDateTime' | '{"nested":{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}}' | 'nested.paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'DELETE' | '/paramOffsetDateTime' | '{"nested":{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}}' | 'nested.paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'POST'   | '/paramLocalDateTime'  | '{"nested":{"paramLocalDateTime":"2100-01-01T00:00:00Z"}}'  | 'nested.paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PUT'    | '/paramLocalDateTime'  | '{"nested":{"paramLocalDateTime":"2100-01-01T00:00:00Z"}}'  | 'nested.paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PATCH'  | '/paramLocalDateTime'  | '{"nested":{"paramLocalDateTime":"2100-01-01T00:00:00Z"}}'  | 'nested.paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'DELETE' | '/paramLocalDateTime'  | '{"nested":{"paramLocalDateTime":"2100-01-01T00:00:00Z"}}'  | 'nested.paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'POST'   | '/paramLocalDate'      | '{"nested":{"paramLocalDate":"2100-01-01"}}'                | 'nested.paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PUT'    | '/paramLocalDate'      | '{"nested":{"paramLocalDate":"2100-01-01"}}'                | 'nested.paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PATCH'  | '/paramLocalDate'      | '{"nested":{"paramLocalDate":"2100-01-01"}}'                | 'nested.paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'DELETE' | '/paramLocalDate'      | '{"nested":{"paramLocalDate":"2100-01-01"}}'                | 'nested.paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'POST'   | '/paramYear'           | '{"nested":{"paramYear":"2100"}}'                           | 'nested.paramYear: must be between -999999999 and 2000'                                           |
      | 'PUT'    | '/paramYear'           | '{"nested":{"paramYear":"2100"}}'                           | 'nested.paramYear: must be between -999999999 and 2000'                                           |
      | 'PATCH'  | '/paramYear'           | '{"nested":{"paramYear":"2100"}}'                           | 'nested.paramYear: must be between -999999999 and 2000'                                           |
      | 'DELETE' | '/paramYear'           | '{"nested":{"paramYear":"2100"}}'                           | 'nested.paramYear: must be between -999999999 and 2000'                                           |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/constrained/named-nested-optional-dto-attribute<path> endpoint with the body <request body> (RequestBodyConstrainedNotNullRestApi.class)
    this endpoint receives the request body-attibute in a nested optional dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
    and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/request-body/constrained/named-nested-optional-dto-attribute'<path>
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | request body                                                   | message                                                                                           |
      | 'POST'   | '/paramString'         | '{"nested":{"param-string":"ab"}}'                             | 'nested.paramString: size must be between 0 and 1'                                                |
      | 'PUT'    | '/paramString'         | '{"nested":{"param-string":"ab"}}'                             | 'nested.paramString: size must be between 0 and 1'                                                |
      | 'PATCH'  | '/paramString'         | '{"nested":{"param-string":"ab"}}'                             | 'nested.paramString: size must be between 0 and 1'                                                |
      | 'DELETE' | '/paramString'         | '{"nested":{"param-string":"ab"}}'                             | 'nested.paramString: size must be between 0 and 1'                                                |
      | 'POST'   | '/paramInteger'        | '{"nested":{"param-integer":10}}'                              | 'nested.paramInteger: must be less than or equal to 1'                                            |
      | 'PUT'    | '/paramInteger'        | '{"nested":{"param-integer":10}}'                              | 'nested.paramInteger: must be less than or equal to 1'                                            |
      | 'PATCH'  | '/paramInteger'        | '{"nested":{"param-integer":10}}'                              | 'nested.paramInteger: must be less than or equal to 1'                                            |
      | 'DELETE' | '/paramInteger'        | '{"nested":{"param-integer":10}}'                              | 'nested.paramInteger: must be less than or equal to 1'                                            |
      | 'POST'   | '/paramLong'           | '{"nested":{"param-long":10}}'                                 | 'nested.paramLong: must be less than or equal to 1'                                               |
      | 'PUT'    | '/paramLong'           | '{"nested":{"param-long":10}}'                                 | 'nested.paramLong: must be less than or equal to 1'                                               |
      | 'PATCH'  | '/paramLong'           | '{"nested":{"param-long":10}}'                                 | 'nested.paramLong: must be less than or equal to 1'                                               |
      | 'DELETE' | '/paramLong'           | '{"nested":{"param-long":10}}'                                 | 'nested.paramLong: must be less than or equal to 1'                                               |
      | 'POST'   | '/paramBigInteger'     | '{"nested":{"param-big-integer":10}}'                          | 'nested.paramBigInteger: must be less than or equal to 1'                                         |
      | 'PUT'    | '/paramBigInteger'     | '{"nested":{"param-big-integer":10}}'                          | 'nested.paramBigInteger: must be less than or equal to 1'                                         |
      | 'PATCH'  | '/paramBigInteger'     | '{"nested":{"param-big-integer":10}}'                          | 'nested.paramBigInteger: must be less than or equal to 1'                                         |
      | 'DELETE' | '/paramBigInteger'     | '{"nested":{"param-big-integer":10}}'                          | 'nested.paramBigInteger: must be less than or equal to 1'                                         |
      | 'POST'   | '/paramBigDecimal'     | '{"nested":{"param-big-decimal":10}}'                          | 'nested.paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PUT'    | '/paramBigDecimal'     | '{"nested":{"param-big-decimal":10}}'                          | 'nested.paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PATCH'  | '/paramBigDecimal'     | '{"nested":{"param-big-decimal":10}}'                          | 'nested.paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'DELETE' | '/paramBigDecimal'     | '{"nested":{"param-big-decimal":10}}'                          | 'nested.paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'POST'   | '/paramOffsetDateTime' | '{"nested":{"param-offset-date-time":"2100-01-01T00:00:00Z"}}' | 'nested.paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PUT'    | '/paramOffsetDateTime' | '{"nested":{"param-offset-date-time":"2100-01-01T00:00:00Z"}}' | 'nested.paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PATCH'  | '/paramOffsetDateTime' | '{"nested":{"param-offset-date-time":"2100-01-01T00:00:00Z"}}' | 'nested.paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'DELETE' | '/paramOffsetDateTime' | '{"nested":{"param-offset-date-time":"2100-01-01T00:00:00Z"}}' | 'nested.paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'POST'   | '/paramLocalDateTime'  | '{"nested":{"param-local-date-time":"2100-01-01T00:00:00Z"}}'  | 'nested.paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PUT'    | '/paramLocalDateTime'  | '{"nested":{"param-local-date-time":"2100-01-01T00:00:00Z"}}'  | 'nested.paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PATCH'  | '/paramLocalDateTime'  | '{"nested":{"param-local-date-time":"2100-01-01T00:00:00Z"}}'  | 'nested.paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'DELETE' | '/paramLocalDateTime'  | '{"nested":{"param-local-date-time":"2100-01-01T00:00:00Z"}}'  | 'nested.paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'POST'   | '/paramLocalDate'      | '{"nested":{"param-local-date":"2100-01-01"}}'                 | 'nested.paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PUT'    | '/paramLocalDate'      | '{"nested":{"param-local-date":"2100-01-01"}}'                 | 'nested.paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PATCH'  | '/paramLocalDate'      | '{"nested":{"param-local-date":"2100-01-01"}}'                 | 'nested.paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'DELETE' | '/paramLocalDate'      | '{"nested":{"param-local-date":"2100-01-01"}}'                 | 'nested.paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'POST'   | '/paramYear'           | '{"nested":{"param-year":"2100"}}'                             | 'nested.paramYear: must be between -999999999 and 2000'                                           |
      | 'PUT'    | '/paramYear'           | '{"nested":{"param-year":"2100"}}'                             | 'nested.paramYear: must be between -999999999 and 2000'                                           |
      | 'PATCH'  | '/paramYear'           | '{"nested":{"param-year":"2100"}}'                             | 'nested.paramYear: must be between -999999999 and 2000'                                           |
      | 'DELETE' | '/paramYear'           | '{"nested":{"param-year":"2100"}}'                             | 'nested.paramYear: must be between -999999999 and 2000'                                           |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/constrained/nested-dto-attribute<path> endpoint with the body <request body> (RequestBodyConstrainedNotNullRestApi.class)
    this endpoint receives the request body-attibute in a nested dto attribute from an array, both having the same name
    and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/request-body/constrained/nested-dto-attribute'<path>
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | request body                                                 | message                                                                                             |
      | 'POST'   | '/paramString'         | '{"array":[{"paramString":"ab"}]}'                           | 'array[0].paramString: size must be between 0 and 1'                                                |
      | 'PUT'    | '/paramString'         | '{"array":[{"paramString":"ab"}]}'                           | 'array[0].paramString: size must be between 0 and 1'                                                |
      | 'PATCH'  | '/paramString'         | '{"array":[{"paramString":"ab"}]}'                           | 'array[0].paramString: size must be between 0 and 1'                                                |
      | 'DELETE' | '/paramString'         | '{"array":[{"paramString":"ab"}]}'                           | 'array[0].paramString: size must be between 0 and 1'                                                |
      | 'POST'   | '/paramInteger'        | '{"array":[{"paramInteger":10}]}'                            | 'array[0].paramInteger: must be less than or equal to 1'                                            |
      | 'PUT'    | '/paramInteger'        | '{"array":[{"paramInteger":10}]}'                            | 'array[0].paramInteger: must be less than or equal to 1'                                            |
      | 'PATCH'  | '/paramInteger'        | '{"array":[{"paramInteger":10}]}'                            | 'array[0].paramInteger: must be less than or equal to 1'                                            |
      | 'DELETE' | '/paramInteger'        | '{"array":[{"paramInteger":10}]}'                            | 'array[0].paramInteger: must be less than or equal to 1'                                            |
      | 'POST'   | '/paramLong'           | '{"array":[{"paramLong":10}]}'                               | 'array[0].paramLong: must be less than or equal to 1'                                               |
      | 'PUT'    | '/paramLong'           | '{"array":[{"paramLong":10}]}'                               | 'array[0].paramLong: must be less than or equal to 1'                                               |
      | 'PATCH'  | '/paramLong'           | '{"array":[{"paramLong":10}]}'                               | 'array[0].paramLong: must be less than or equal to 1'                                               |
      | 'DELETE' | '/paramLong'           | '{"array":[{"paramLong":10}]}'                               | 'array[0].paramLong: must be less than or equal to 1'                                               |
      | 'POST'   | '/paramBigInteger'     | '{"array":[{"paramBigInteger":10}]}'                         | 'array[0].paramBigInteger: must be less than or equal to 1'                                         |
      | 'PUT'    | '/paramBigInteger'     | '{"array":[{"paramBigInteger":10}]}'                         | 'array[0].paramBigInteger: must be less than or equal to 1'                                         |
      | 'PATCH'  | '/paramBigInteger'     | '{"array":[{"paramBigInteger":10}]}'                         | 'array[0].paramBigInteger: must be less than or equal to 1'                                         |
      | 'DELETE' | '/paramBigInteger'     | '{"array":[{"paramBigInteger":10}]}'                         | 'array[0].paramBigInteger: must be less than or equal to 1'                                         |
      | 'POST'   | '/paramBigDecimal'     | '{"array":[{"paramBigDecimal":10}]}'                         | 'array[0].paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PUT'    | '/paramBigDecimal'     | '{"array":[{"paramBigDecimal":10}]}'                         | 'array[0].paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PATCH'  | '/paramBigDecimal'     | '{"array":[{"paramBigDecimal":10}]}'                         | 'array[0].paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'DELETE' | '/paramBigDecimal'     | '{"array":[{"paramBigDecimal":10}]}'                         | 'array[0].paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'POST'   | '/paramOffsetDateTime' | '{"array":[{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}]}' | 'array[0].paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PUT'    | '/paramOffsetDateTime' | '{"array":[{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}]}' | 'array[0].paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PATCH'  | '/paramOffsetDateTime' | '{"array":[{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}]}' | 'array[0].paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'DELETE' | '/paramOffsetDateTime' | '{"array":[{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}]}' | 'array[0].paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'POST'   | '/paramLocalDateTime'  | '{"array":[{"paramLocalDateTime":"2100-01-01T00:00:00Z"}]}'  | 'array[0].paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PUT'    | '/paramLocalDateTime'  | '{"array":[{"paramLocalDateTime":"2100-01-01T00:00:00Z"}]}'  | 'array[0].paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PATCH'  | '/paramLocalDateTime'  | '{"array":[{"paramLocalDateTime":"2100-01-01T00:00:00Z"}]}'  | 'array[0].paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'DELETE' | '/paramLocalDateTime'  | '{"array":[{"paramLocalDateTime":"2100-01-01T00:00:00Z"}]}'  | 'array[0].paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'POST'   | '/paramLocalDate'      | '{"array":[{"paramLocalDate":"2100-01-01"}]}'                | 'array[0].paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PUT'    | '/paramLocalDate'      | '{"array":[{"paramLocalDate":"2100-01-01"}]}'                | 'array[0].paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PATCH'  | '/paramLocalDate'      | '{"array":[{"paramLocalDate":"2100-01-01"}]}'                | 'array[0].paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'DELETE' | '/paramLocalDate'      | '{"array":[{"paramLocalDate":"2100-01-01"}]}'                | 'array[0].paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'POST'   | '/paramYear'           | '{"array":[{"paramYear":"2100"}]}'                           | 'array[0].paramYear: must be between -999999999 and 2000'                                           |
      | 'PUT'    | '/paramYear'           | '{"array":[{"paramYear":"2100"}]}'                           | 'array[0].paramYear: must be between -999999999 and 2000'                                           |
      | 'PATCH'  | '/paramYear'           | '{"array":[{"paramYear":"2100"}]}'                           | 'array[0].paramYear: must be between -999999999 and 2000'                                           |
      | 'DELETE' | '/paramYear'           | '{"array":[{"paramYear":"2100"}]}'                           | 'array[0].paramYear: must be between -999999999 and 2000'                                           |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/constrained/named-nested-dto-attribute<path> endpoint with the body <request body> (RequestBodyConstrainedNotNullRestApi.class)
    this endpoint receives the request body-attibute in a nested dto attribute from an array, both having the diferent names, a custom body-attibute name is defined by the endpoint
    and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/request-body/constrained/named-nested-dto-attribute'<path>
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | request body                                                    | message                                                                                             |
      | 'POST'   | '/paramString'         | '{"array":[{"param-string":"ab"}]}'                             | 'array[0].paramString: size must be between 0 and 1'                                                |
      | 'PUT'    | '/paramString'         | '{"array":[{"param-string":"ab"}]}'                             | 'array[0].paramString: size must be between 0 and 1'                                                |
      | 'PATCH'  | '/paramString'         | '{"array":[{"param-string":"ab"}]}'                             | 'array[0].paramString: size must be between 0 and 1'                                                |
      | 'DELETE' | '/paramString'         | '{"array":[{"param-string":"ab"}]}'                             | 'array[0].paramString: size must be between 0 and 1'                                                |
      | 'POST'   | '/paramInteger'        | '{"array":[{"param-integer":10}]}'                              | 'array[0].paramInteger: must be less than or equal to 1'                                            |
      | 'PUT'    | '/paramInteger'        | '{"array":[{"param-integer":10}]}'                              | 'array[0].paramInteger: must be less than or equal to 1'                                            |
      | 'PATCH'  | '/paramInteger'        | '{"array":[{"param-integer":10}]}'                              | 'array[0].paramInteger: must be less than or equal to 1'                                            |
      | 'DELETE' | '/paramInteger'        | '{"array":[{"param-integer":10}]}'                              | 'array[0].paramInteger: must be less than or equal to 1'                                            |
      | 'POST'   | '/paramLong'           | '{"array":[{"param-long":10}]}'                                 | 'array[0].paramLong: must be less than or equal to 1'                                               |
      | 'PUT'    | '/paramLong'           | '{"array":[{"param-long":10}]}'                                 | 'array[0].paramLong: must be less than or equal to 1'                                               |
      | 'PATCH'  | '/paramLong'           | '{"array":[{"param-long":10}]}'                                 | 'array[0].paramLong: must be less than or equal to 1'                                               |
      | 'DELETE' | '/paramLong'           | '{"array":[{"param-long":10}]}'                                 | 'array[0].paramLong: must be less than or equal to 1'                                               |
      | 'POST'   | '/paramBigInteger'     | '{"array":[{"param-big-integer":10}]}'                          | 'array[0].paramBigInteger: must be less than or equal to 1'                                         |
      | 'PUT'    | '/paramBigInteger'     | '{"array":[{"param-big-integer":10}]}'                          | 'array[0].paramBigInteger: must be less than or equal to 1'                                         |
      | 'PATCH'  | '/paramBigInteger'     | '{"array":[{"param-big-integer":10}]}'                          | 'array[0].paramBigInteger: must be less than or equal to 1'                                         |
      | 'DELETE' | '/paramBigInteger'     | '{"array":[{"param-big-integer":10}]}'                          | 'array[0].paramBigInteger: must be less than or equal to 1'                                         |
      | 'POST'   | '/paramBigDecimal'     | '{"array":[{"param-big-decimal":10}]}'                          | 'array[0].paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PUT'    | '/paramBigDecimal'     | '{"array":[{"param-big-decimal":10}]}'                          | 'array[0].paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PATCH'  | '/paramBigDecimal'     | '{"array":[{"param-big-decimal":10}]}'                          | 'array[0].paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'DELETE' | '/paramBigDecimal'     | '{"array":[{"param-big-decimal":10}]}'                          | 'array[0].paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'POST'   | '/paramOffsetDateTime' | '{"array":[{"param-offset-date-time":"2100-01-01T00:00:00Z"}]}' | 'array[0].paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PUT'    | '/paramOffsetDateTime' | '{"array":[{"param-offset-date-time":"2100-01-01T00:00:00Z"}]}' | 'array[0].paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PATCH'  | '/paramOffsetDateTime' | '{"array":[{"param-offset-date-time":"2100-01-01T00:00:00Z"}]}' | 'array[0].paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'DELETE' | '/paramOffsetDateTime' | '{"array":[{"param-offset-date-time":"2100-01-01T00:00:00Z"}]}' | 'array[0].paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'POST'   | '/paramLocalDateTime'  | '{"array":[{"param-local-date-time":"2100-01-01T00:00:00Z"}]}'  | 'array[0].paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PUT'    | '/paramLocalDateTime'  | '{"array":[{"param-local-date-time":"2100-01-01T00:00:00Z"}]}'  | 'array[0].paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PATCH'  | '/paramLocalDateTime'  | '{"array":[{"param-local-date-time":"2100-01-01T00:00:00Z"}]}'  | 'array[0].paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'DELETE' | '/paramLocalDateTime'  | '{"array":[{"param-local-date-time":"2100-01-01T00:00:00Z"}]}'  | 'array[0].paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'POST'   | '/paramLocalDate'      | '{"array":[{"param-local-date":"2100-01-01"}]}'                 | 'array[0].paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PUT'    | '/paramLocalDate'      | '{"array":[{"param-local-date":"2100-01-01"}]}'                 | 'array[0].paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PATCH'  | '/paramLocalDate'      | '{"array":[{"param-local-date":"2100-01-01"}]}'                 | 'array[0].paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'DELETE' | '/paramLocalDate'      | '{"array":[{"param-local-date":"2100-01-01"}]}'                 | 'array[0].paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'POST'   | '/paramYear'           | '{"array":[{"param-year":"2100"}]}'                             | 'array[0].paramYear: must be between -999999999 and 2000'                                           |
      | 'PUT'    | '/paramYear'           | '{"array":[{"param-year":"2100"}]}'                             | 'array[0].paramYear: must be between -999999999 and 2000'                                           |
      | 'PATCH'  | '/paramYear'           | '{"array":[{"param-year":"2100"}]}'                             | 'array[0].paramYear: must be between -999999999 and 2000'                                           |
      | 'DELETE' | '/paramYear'           | '{"array":[{"param-year":"2100"}]}'                             | 'array[0].paramYear: must be between -999999999 and 2000'                                           |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/constrained/nested-optional-dto-attribute<path> endpoint with the body <request body> (RequestBodyConstrainedNotNullRestApi.class)
    this endpoint receives the request body-attibute in a nested optional dto attribute from an array, both having the same name
    and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/request-body/constrained/nested-optional-dto-attribute'<path>
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | request body                                                 | message                                                                                             |
      | 'POST'   | '/paramString'         | '{"array":[{"paramString":"ab"}]}'                           | 'array[0].paramString: size must be between 0 and 1'                                                |
      | 'PUT'    | '/paramString'         | '{"array":[{"paramString":"ab"}]}'                           | 'array[0].paramString: size must be between 0 and 1'                                                |
      | 'PATCH'  | '/paramString'         | '{"array":[{"paramString":"ab"}]}'                           | 'array[0].paramString: size must be between 0 and 1'                                                |
      | 'DELETE' | '/paramString'         | '{"array":[{"paramString":"ab"}]}'                           | 'array[0].paramString: size must be between 0 and 1'                                                |
      | 'POST'   | '/paramInteger'        | '{"array":[{"paramInteger":10}]}'                            | 'array[0].paramInteger: must be less than or equal to 1'                                            |
      | 'PUT'    | '/paramInteger'        | '{"array":[{"paramInteger":10}]}'                            | 'array[0].paramInteger: must be less than or equal to 1'                                            |
      | 'PATCH'  | '/paramInteger'        | '{"array":[{"paramInteger":10}]}'                            | 'array[0].paramInteger: must be less than or equal to 1'                                            |
      | 'DELETE' | '/paramInteger'        | '{"array":[{"paramInteger":10}]}'                            | 'array[0].paramInteger: must be less than or equal to 1'                                            |
      | 'POST'   | '/paramLong'           | '{"array":[{"paramLong":10}]}'                               | 'array[0].paramLong: must be less than or equal to 1'                                               |
      | 'PUT'    | '/paramLong'           | '{"array":[{"paramLong":10}]}'                               | 'array[0].paramLong: must be less than or equal to 1'                                               |
      | 'PATCH'  | '/paramLong'           | '{"array":[{"paramLong":10}]}'                               | 'array[0].paramLong: must be less than or equal to 1'                                               |
      | 'DELETE' | '/paramLong'           | '{"array":[{"paramLong":10}]}'                               | 'array[0].paramLong: must be less than or equal to 1'                                               |
      | 'POST'   | '/paramBigInteger'     | '{"array":[{"paramBigInteger":10}]}'                         | 'array[0].paramBigInteger: must be less than or equal to 1'                                         |
      | 'PUT'    | '/paramBigInteger'     | '{"array":[{"paramBigInteger":10}]}'                         | 'array[0].paramBigInteger: must be less than or equal to 1'                                         |
      | 'PATCH'  | '/paramBigInteger'     | '{"array":[{"paramBigInteger":10}]}'                         | 'array[0].paramBigInteger: must be less than or equal to 1'                                         |
      | 'DELETE' | '/paramBigInteger'     | '{"array":[{"paramBigInteger":10}]}'                         | 'array[0].paramBigInteger: must be less than or equal to 1'                                         |
      | 'POST'   | '/paramBigDecimal'     | '{"array":[{"paramBigDecimal":10}]}'                         | 'array[0].paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PUT'    | '/paramBigDecimal'     | '{"array":[{"paramBigDecimal":10}]}'                         | 'array[0].paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PATCH'  | '/paramBigDecimal'     | '{"array":[{"paramBigDecimal":10}]}'                         | 'array[0].paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'DELETE' | '/paramBigDecimal'     | '{"array":[{"paramBigDecimal":10}]}'                         | 'array[0].paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'POST'   | '/paramOffsetDateTime' | '{"array":[{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}]}' | 'array[0].paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PUT'    | '/paramOffsetDateTime' | '{"array":[{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}]}' | 'array[0].paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PATCH'  | '/paramOffsetDateTime' | '{"array":[{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}]}' | 'array[0].paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'DELETE' | '/paramOffsetDateTime' | '{"array":[{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}]}' | 'array[0].paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'POST'   | '/paramLocalDateTime'  | '{"array":[{"paramLocalDateTime":"2100-01-01T00:00:00Z"}]}'  | 'array[0].paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PUT'    | '/paramLocalDateTime'  | '{"array":[{"paramLocalDateTime":"2100-01-01T00:00:00Z"}]}'  | 'array[0].paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PATCH'  | '/paramLocalDateTime'  | '{"array":[{"paramLocalDateTime":"2100-01-01T00:00:00Z"}]}'  | 'array[0].paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'DELETE' | '/paramLocalDateTime'  | '{"array":[{"paramLocalDateTime":"2100-01-01T00:00:00Z"}]}'  | 'array[0].paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'POST'   | '/paramLocalDate'      | '{"array":[{"paramLocalDate":"2100-01-01"}]}'                | 'array[0].paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PUT'    | '/paramLocalDate'      | '{"array":[{"paramLocalDate":"2100-01-01"}]}'                | 'array[0].paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PATCH'  | '/paramLocalDate'      | '{"array":[{"paramLocalDate":"2100-01-01"}]}'                | 'array[0].paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'DELETE' | '/paramLocalDate'      | '{"array":[{"paramLocalDate":"2100-01-01"}]}'                | 'array[0].paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'POST'   | '/paramYear'           | '{"array":[{"paramYear":"2100"}]}'                           | 'array[0].paramYear: must be between -999999999 and 2000'                                           |
      | 'PUT'    | '/paramYear'           | '{"array":[{"paramYear":"2100"}]}'                           | 'array[0].paramYear: must be between -999999999 and 2000'                                           |
      | 'PATCH'  | '/paramYear'           | '{"array":[{"paramYear":"2100"}]}'                           | 'array[0].paramYear: must be between -999999999 and 2000'                                           |
      | 'DELETE' | '/paramYear'           | '{"array":[{"paramYear":"2100"}]}'                           | 'array[0].paramYear: must be between -999999999 and 2000'                                           |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/constrained/named-nested-optional-dto-attribute<path> endpoint with the body <request body> (RequestBodyConstrainedNotNullRestApi.class)
    this endpoint receives the request body-attibute in a nested optional dto attribute from an array, both having the diferent names, a custom body-attibute name is defined by the endpoint
    and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/request-body/constrained/named-nested-optional-dto-attribute'<path>
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | request body                                                    | message                                                                                             |
      | 'POST'   | '/paramString'         | '{"array":[{"param-string":"ab"}]}'                             | 'array[0].paramString: size must be between 0 and 1'                                                |
      | 'PUT'    | '/paramString'         | '{"array":[{"param-string":"ab"}]}'                             | 'array[0].paramString: size must be between 0 and 1'                                                |
      | 'PATCH'  | '/paramString'         | '{"array":[{"param-string":"ab"}]}'                             | 'array[0].paramString: size must be between 0 and 1'                                                |
      | 'DELETE' | '/paramString'         | '{"array":[{"param-string":"ab"}]}'                             | 'array[0].paramString: size must be between 0 and 1'                                                |
      | 'POST'   | '/paramInteger'        | '{"array":[{"param-integer":10}]}'                              | 'array[0].paramInteger: must be less than or equal to 1'                                            |
      | 'PUT'    | '/paramInteger'        | '{"array":[{"param-integer":10}]}'                              | 'array[0].paramInteger: must be less than or equal to 1'                                            |
      | 'PATCH'  | '/paramInteger'        | '{"array":[{"param-integer":10}]}'                              | 'array[0].paramInteger: must be less than or equal to 1'                                            |
      | 'DELETE' | '/paramInteger'        | '{"array":[{"param-integer":10}]}'                              | 'array[0].paramInteger: must be less than or equal to 1'                                            |
      | 'POST'   | '/paramLong'           | '{"array":[{"param-long":10}]}'                                 | 'array[0].paramLong: must be less than or equal to 1'                                               |
      | 'PUT'    | '/paramLong'           | '{"array":[{"param-long":10}]}'                                 | 'array[0].paramLong: must be less than or equal to 1'                                               |
      | 'PATCH'  | '/paramLong'           | '{"array":[{"param-long":10}]}'                                 | 'array[0].paramLong: must be less than or equal to 1'                                               |
      | 'DELETE' | '/paramLong'           | '{"array":[{"param-long":10}]}'                                 | 'array[0].paramLong: must be less than or equal to 1'                                               |
      | 'POST'   | '/paramBigInteger'     | '{"array":[{"param-big-integer":10}]}'                          | 'array[0].paramBigInteger: must be less than or equal to 1'                                         |
      | 'PUT'    | '/paramBigInteger'     | '{"array":[{"param-big-integer":10}]}'                          | 'array[0].paramBigInteger: must be less than or equal to 1'                                         |
      | 'PATCH'  | '/paramBigInteger'     | '{"array":[{"param-big-integer":10}]}'                          | 'array[0].paramBigInteger: must be less than or equal to 1'                                         |
      | 'DELETE' | '/paramBigInteger'     | '{"array":[{"param-big-integer":10}]}'                          | 'array[0].paramBigInteger: must be less than or equal to 1'                                         |
      | 'POST'   | '/paramBigDecimal'     | '{"array":[{"param-big-decimal":10}]}'                          | 'array[0].paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PUT'    | '/paramBigDecimal'     | '{"array":[{"param-big-decimal":10}]}'                          | 'array[0].paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PATCH'  | '/paramBigDecimal'     | '{"array":[{"param-big-decimal":10}]}'                          | 'array[0].paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'DELETE' | '/paramBigDecimal'     | '{"array":[{"param-big-decimal":10}]}'                          | 'array[0].paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'POST'   | '/paramOffsetDateTime' | '{"array":[{"param-offset-date-time":"2100-01-01T00:00:00Z"}]}' | 'array[0].paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PUT'    | '/paramOffsetDateTime' | '{"array":[{"param-offset-date-time":"2100-01-01T00:00:00Z"}]}' | 'array[0].paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PATCH'  | '/paramOffsetDateTime' | '{"array":[{"param-offset-date-time":"2100-01-01T00:00:00Z"}]}' | 'array[0].paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'DELETE' | '/paramOffsetDateTime' | '{"array":[{"param-offset-date-time":"2100-01-01T00:00:00Z"}]}' | 'array[0].paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'POST'   | '/paramLocalDateTime'  | '{"array":[{"param-local-date-time":"2100-01-01T00:00:00Z"}]}'  | 'array[0].paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PUT'    | '/paramLocalDateTime'  | '{"array":[{"param-local-date-time":"2100-01-01T00:00:00Z"}]}'  | 'array[0].paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PATCH'  | '/paramLocalDateTime'  | '{"array":[{"param-local-date-time":"2100-01-01T00:00:00Z"}]}'  | 'array[0].paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'DELETE' | '/paramLocalDateTime'  | '{"array":[{"param-local-date-time":"2100-01-01T00:00:00Z"}]}'  | 'array[0].paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'POST'   | '/paramLocalDate'      | '{"array":[{"param-local-date":"2100-01-01"}]}'                 | 'array[0].paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PUT'    | '/paramLocalDate'      | '{"array":[{"param-local-date":"2100-01-01"}]}'                 | 'array[0].paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PATCH'  | '/paramLocalDate'      | '{"array":[{"param-local-date":"2100-01-01"}]}'                 | 'array[0].paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'DELETE' | '/paramLocalDate'      | '{"array":[{"param-local-date":"2100-01-01"}]}'                 | 'array[0].paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'POST'   | '/paramYear'           | '{"array":[{"param-year":"2100"}]}'                             | 'array[0].paramYear: must be between -999999999 and 2000'                                           |
      | 'PUT'    | '/paramYear'           | '{"array":[{"param-year":"2100"}]}'                             | 'array[0].paramYear: must be between -999999999 and 2000'                                           |
      | 'PATCH'  | '/paramYear'           | '{"array":[{"param-year":"2100"}]}'                             | 'array[0].paramYear: must be between -999999999 and 2000'                                           |
      | 'DELETE' | '/paramYear'           | '{"array":[{"param-year":"2100"}]}'                             | 'array[0].paramYear: must be between -999999999 and 2000'                                           |
