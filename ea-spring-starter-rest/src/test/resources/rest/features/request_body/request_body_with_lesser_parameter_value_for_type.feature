Feature: Request body with lesser parameter value for type
  
  The REST service receives data via the request body and cannot interpret the data because it is less than the minimum value allowed. The service responds with an error message.
  
   Rule: The conversion between data types is given by the following table
  
  | parameter type | java type      |
  |----------------|----------------|
  | string         | String         |
  | number         | Integer        |
  | number         | Long           |
  | number         | BigInteger     |
  | number         | Float          |
  | number         | Double         |
  | number         | BigDecimal     |
  | boolean        | Boolean        |
  | ISO-8601       | OffsetDateTime |
  | ISO-8601       | LocalDateTime  |
  | ISO-8601       | LocalDate      |
  | ISO-8601       | LocalTime      |
  | ISO-8601       | Year           |
  | string         | ENUM           |

  Background: 
    Given the default timezone 'America/Puerto_Rico'

  Scenario Outline: The <method> call is made to the /api/v1/request-body/dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in a dto attribute, both having the same name
    and the service cannot map the sent value to the declared data type, responding with an error message.

    Given the endpoint '/api/v1/request-body/dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | request body                                                | message                                    |
      | 'POST'   | '{"paramInteger":-2147483649}'                              | 'paramInteger: is not a valid type'        |
      | 'PUT'    | '{"paramInteger":-2147483649}'                              | 'paramInteger: is not a valid type'        |
      | 'PATCH'  | '{"paramInteger":-2147483649}'                              | 'paramInteger: is not a valid type'        |
      | 'DELETE' | '{"paramInteger":-2147483649}'                              | 'paramInteger: is not a valid type'        |
      | 'POST'   | '{"paramLong":-9223372036854775809}'                        | 'paramLong: is not a valid type'           |
      | 'PUT'    | '{"paramLong":-9223372036854775809}'                        | 'paramLong: is not a valid type'           |
      | 'PATCH'  | '{"paramLong":-9223372036854775809}'                        | 'paramLong: is not a valid type'           |
      | 'DELETE' | '{"paramLong":-9223372036854775809}'                        | 'paramLong: is not a valid type'           |
      | 'POST'   | '{"paramOffsetDateTime":"-1000000000-12-31T23:59:59.999Z"}' | 'paramOffsetDateTime: is not a valid type' |
      | 'PUT'    | '{"paramOffsetDateTime":"-1000000000-12-31T23:59:59.999Z"}' | 'paramOffsetDateTime: is not a valid type' |
      | 'PATCH'  | '{"paramOffsetDateTime":"-1000000000-12-31T23:59:59.999Z"}' | 'paramOffsetDateTime: is not a valid type' |
      | 'DELETE' | '{"paramOffsetDateTime":"-1000000000-12-31T23:59:59.999Z"}' | 'paramOffsetDateTime: is not a valid type' |
      | 'POST'   | '{"paramLocalDateTime":"-1000000000-12-31T23:59:59.999Z"}'  | 'paramLocalDateTime: is not a valid type'  |
      | 'PUT'    | '{"paramLocalDateTime":"-1000000000-12-31T23:59:59.999Z"}'  | 'paramLocalDateTime: is not a valid type'  |
      | 'PATCH'  | '{"paramLocalDateTime":"-1000000000-12-31T23:59:59.999Z"}'  | 'paramLocalDateTime: is not a valid type'  |
      | 'DELETE' | '{"paramLocalDateTime":"-1000000000-12-31T23:59:59.999Z"}'  | 'paramLocalDateTime: is not a valid type'  |
      | 'POST'   | '{"paramLocalDate":"-1000000000-12-31"}'                    | 'paramLocalDate: is not a valid type'      |
      | 'PUT'    | '{"paramLocalDate":"-1000000000-12-31"}'                    | 'paramLocalDate: is not a valid type'      |
      | 'PATCH'  | '{"paramLocalDate":"-1000000000-12-31"}'                    | 'paramLocalDate: is not a valid type'      |
      | 'DELETE' | '{"paramLocalDate":"-1000000000-12-31"}'                    | 'paramLocalDate: is not a valid type'      |
      | 'POST'   | '{"paramYear":"-1000000000"}'                               | 'paramYear: is not a valid type'           |
      | 'PUT'    | '{"paramYear":"-1000000000"}'                               | 'paramYear: is not a valid type'           |
      | 'PATCH'  | '{"paramYear":"-1000000000"}'                               | 'paramYear: is not a valid type'           |
      | 'DELETE' | '{"paramYear":"-1000000000"}'                               | 'paramYear: is not a valid type'           |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/named-dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in a dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint 
    and the service cannot map the sent value to the declared data type, responding with an error message.

    Given the endpoint '/api/v1/request-body/named-dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | request body                                                   | message                                       |
      | 'POST'   | '{"param-integer":-2147483649}'                                | 'param-integer: is not a valid type'          |
      | 'PUT'    | '{"param-integer":-2147483649}'                                | 'param-integer: is not a valid type'          |
      | 'PATCH'  | '{"param-integer":-2147483649}'                                | 'param-integer: is not a valid type'          |
      | 'DELETE' | '{"param-integer":-2147483649}'                                | 'param-integer: is not a valid type'          |
      | 'POST'   | '{"param-long":-9223372036854775809}'                          | 'param-long: is not a valid type'             |
      | 'PUT'    | '{"param-long":-9223372036854775809}'                          | 'param-long: is not a valid type'             |
      | 'PATCH'  | '{"param-long":-9223372036854775809}'                          | 'param-long: is not a valid type'             |
      | 'DELETE' | '{"param-long":-9223372036854775809}'                          | 'param-long: is not a valid type'             |
      | 'POST'   | '{"param-offset-date-time":"-1000000000-12-31T23:59:59.999Z"}' | 'param-offset-date-time: is not a valid type' |
      | 'PUT'    | '{"param-offset-date-time":"-1000000000-12-31T23:59:59.999Z"}' | 'param-offset-date-time: is not a valid type' |
      | 'PATCH'  | '{"param-offset-date-time":"-1000000000-12-31T23:59:59.999Z"}' | 'param-offset-date-time: is not a valid type' |
      | 'DELETE' | '{"param-offset-date-time":"-1000000000-12-31T23:59:59.999Z"}' | 'param-offset-date-time: is not a valid type' |
      | 'POST'   | '{"param-local-date-time":"-1000000000-12-31T23:59:59.999Z"}'  | 'param-local-date-time: is not a valid type'  |
      | 'PUT'    | '{"param-local-date-time":"-1000000000-12-31T23:59:59.999Z"}'  | 'param-local-date-time: is not a valid type'  |
      | 'PATCH'  | '{"param-local-date-time":"-1000000000-12-31T23:59:59.999Z"}'  | 'param-local-date-time: is not a valid type'  |
      | 'DELETE' | '{"param-local-date-time":"-1000000000-12-31T23:59:59.999Z"}'  | 'param-local-date-time: is not a valid type'  |
      | 'POST'   | '{"param-local-date":"-1000000000-12-31"}'                     | 'param-local-date: is not a valid type'       |
      | 'PUT'    | '{"param-local-date":"-1000000000-12-31"}'                     | 'param-local-date: is not a valid type'       |
      | 'PATCH'  | '{"param-local-date":"-1000000000-12-31"}'                     | 'param-local-date: is not a valid type'       |
      | 'DELETE' | '{"param-local-date":"-1000000000-12-31"}'                     | 'param-local-date: is not a valid type'       |
      | 'POST'   | '{"param-year":"-1000000000"}'                                 | 'param-year: is not a valid type'             |
      | 'PUT'    | '{"param-year":"-1000000000"}'                                 | 'param-year: is not a valid type'             |
      | 'PATCH'  | '{"param-year":"-1000000000"}'                                 | 'param-year: is not a valid type'             |
      | 'DELETE' | '{"param-year":"-1000000000"}'                                 | 'param-year: is not a valid type'             |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/optional-dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in an optional dto attribute, both having the same name
    and the service cannot map the sent value to the declared data type, responding with an error message.

    Given the endpoint '/api/v1/request-body/optional-dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | request body                                                | message                                    |
      | 'POST'   | '{"paramInteger":-2147483649}'                              | 'paramInteger: is not a valid type'        |
      | 'PUT'    | '{"paramInteger":-2147483649}'                              | 'paramInteger: is not a valid type'        |
      | 'PATCH'  | '{"paramInteger":-2147483649}'                              | 'paramInteger: is not a valid type'        |
      | 'DELETE' | '{"paramInteger":-2147483649}'                              | 'paramInteger: is not a valid type'        |
      | 'POST'   | '{"paramLong":-9223372036854775809}'                        | 'paramLong: is not a valid type'           |
      | 'PUT'    | '{"paramLong":-9223372036854775809}'                        | 'paramLong: is not a valid type'           |
      | 'PATCH'  | '{"paramLong":-9223372036854775809}'                        | 'paramLong: is not a valid type'           |
      | 'DELETE' | '{"paramLong":-9223372036854775809}'                        | 'paramLong: is not a valid type'           |
      | 'POST'   | '{"paramOffsetDateTime":"-1000000000-12-31T23:59:59.999Z"}' | 'paramOffsetDateTime: is not a valid type' |
      | 'PUT'    | '{"paramOffsetDateTime":"-1000000000-12-31T23:59:59.999Z"}' | 'paramOffsetDateTime: is not a valid type' |
      | 'PATCH'  | '{"paramOffsetDateTime":"-1000000000-12-31T23:59:59.999Z"}' | 'paramOffsetDateTime: is not a valid type' |
      | 'DELETE' | '{"paramOffsetDateTime":"-1000000000-12-31T23:59:59.999Z"}' | 'paramOffsetDateTime: is not a valid type' |
      | 'POST'   | '{"paramLocalDateTime":"-1000000000-12-31T23:59:59.999Z"}'  | 'paramLocalDateTime: is not a valid type'  |
      | 'PUT'    | '{"paramLocalDateTime":"-1000000000-12-31T23:59:59.999Z"}'  | 'paramLocalDateTime: is not a valid type'  |
      | 'PATCH'  | '{"paramLocalDateTime":"-1000000000-12-31T23:59:59.999Z"}'  | 'paramLocalDateTime: is not a valid type'  |
      | 'DELETE' | '{"paramLocalDateTime":"-1000000000-12-31T23:59:59.999Z"}'  | 'paramLocalDateTime: is not a valid type'  |
      | 'POST'   | '{"paramLocalDate":"-1000000000-12-31"}'                    | 'paramLocalDate: is not a valid type'      |
      | 'PUT'    | '{"paramLocalDate":"-1000000000-12-31"}'                    | 'paramLocalDate: is not a valid type'      |
      | 'PATCH'  | '{"paramLocalDate":"-1000000000-12-31"}'                    | 'paramLocalDate: is not a valid type'      |
      | 'DELETE' | '{"paramLocalDate":"-1000000000-12-31"}'                    | 'paramLocalDate: is not a valid type'      |
      | 'POST'   | '{"paramYear":"-1000000000"}'                               | 'paramYear: is not a valid type'           |
      | 'PUT'    | '{"paramYear":"-1000000000"}'                               | 'paramYear: is not a valid type'           |
      | 'PATCH'  | '{"paramYear":"-1000000000"}'                               | 'paramYear: is not a valid type'           |
      | 'DELETE' | '{"paramYear":"-1000000000"}'                               | 'paramYear: is not a valid type'           |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/named-optional-dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in an optional dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
    and the service cannot map the sent value to the declared data type, responding with an error message.

    Given the endpoint '/api/v1/request-body/named-optional-dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | request body                                                   | message                                       |
      | 'POST'   | '{"param-integer":-2147483649}'                                | 'param-integer: is not a valid type'          |
      | 'PUT'    | '{"param-integer":-2147483649}'                                | 'param-integer: is not a valid type'          |
      | 'PATCH'  | '{"param-integer":-2147483649}'                                | 'param-integer: is not a valid type'          |
      | 'DELETE' | '{"param-integer":-2147483649}'                                | 'param-integer: is not a valid type'          |
      | 'POST'   | '{"param-long":-9223372036854775809}'                          | 'param-long: is not a valid type'             |
      | 'PUT'    | '{"param-long":-9223372036854775809}'                          | 'param-long: is not a valid type'             |
      | 'PATCH'  | '{"param-long":-9223372036854775809}'                          | 'param-long: is not a valid type'             |
      | 'DELETE' | '{"param-long":-9223372036854775809}'                          | 'param-long: is not a valid type'             |
      | 'POST'   | '{"param-offset-date-time":"-1000000000-12-31T23:59:59.999Z"}' | 'param-offset-date-time: is not a valid type' |
      | 'PUT'    | '{"param-offset-date-time":"-1000000000-12-31T23:59:59.999Z"}' | 'param-offset-date-time: is not a valid type' |
      | 'PATCH'  | '{"param-offset-date-time":"-1000000000-12-31T23:59:59.999Z"}' | 'param-offset-date-time: is not a valid type' |
      | 'DELETE' | '{"param-offset-date-time":"-1000000000-12-31T23:59:59.999Z"}' | 'param-offset-date-time: is not a valid type' |
      | 'POST'   | '{"param-local-date-time":"-1000000000-12-31T23:59:59.999Z"}'  | 'param-local-date-time: is not a valid type'  |
      | 'PUT'    | '{"param-local-date-time":"-1000000000-12-31T23:59:59.999Z"}'  | 'param-local-date-time: is not a valid type'  |
      | 'PATCH'  | '{"param-local-date-time":"-1000000000-12-31T23:59:59.999Z"}'  | 'param-local-date-time: is not a valid type'  |
      | 'DELETE' | '{"param-local-date-time":"-1000000000-12-31T23:59:59.999Z"}'  | 'param-local-date-time: is not a valid type'  |
      | 'POST'   | '{"param-local-date":"-1000000000-12-31"}'                     | 'param-local-date: is not a valid type'       |
      | 'PUT'    | '{"param-local-date":"-1000000000-12-31"}'                     | 'param-local-date: is not a valid type'       |
      | 'PATCH'  | '{"param-local-date":"-1000000000-12-31"}'                     | 'param-local-date: is not a valid type'       |
      | 'DELETE' | '{"param-local-date":"-1000000000-12-31"}'                     | 'param-local-date: is not a valid type'       |
      | 'POST'   | '{"param-year":"-1000000000"}'                                 | 'param-year: is not a valid type'             |
      | 'PUT'    | '{"param-year":"-1000000000"}'                                 | 'param-year: is not a valid type'             |
      | 'PATCH'  | '{"param-year":"-1000000000"}'                                 | 'param-year: is not a valid type'             |
      | 'DELETE' | '{"param-year":"-1000000000"}'                                 | 'param-year: is not a valid type'             |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/nested-dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in a nested dto attribute, both having the same name
    and the service cannot map the sent value to the declared data type, responding with an error message.

    Given the endpoint '/api/v1/request-body/nested-dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | request body                                                           | message                                           |
      | 'POST'   | '{"nested":{"paramInteger":-2147483649}}'                              | 'nested.paramInteger: is not a valid type'        |
      | 'PUT'    | '{"nested":{"paramInteger":-2147483649}}'                              | 'nested.paramInteger: is not a valid type'        |
      | 'PATCH'  | '{"nested":{"paramInteger":-2147483649}}'                              | 'nested.paramInteger: is not a valid type'        |
      | 'DELETE' | '{"nested":{"paramInteger":-2147483649}}'                              | 'nested.paramInteger: is not a valid type'        |
      | 'POST'   | '{"nested":{"paramLong":-9223372036854775809}}'                        | 'nested.paramLong: is not a valid type'           |
      | 'PUT'    | '{"nested":{"paramLong":-9223372036854775809}}'                        | 'nested.paramLong: is not a valid type'           |
      | 'PATCH'  | '{"nested":{"paramLong":-9223372036854775809}}'                        | 'nested.paramLong: is not a valid type'           |
      | 'DELETE' | '{"nested":{"paramLong":-9223372036854775809}}'                        | 'nested.paramLong: is not a valid type'           |
      | 'POST'   | '{"nested":{"paramOffsetDateTime":"-1000000000-12-31T23:59:59.999Z"}}' | 'nested.paramOffsetDateTime: is not a valid type' |
      | 'PUT'    | '{"nested":{"paramOffsetDateTime":"-1000000000-12-31T23:59:59.999Z"}}' | 'nested.paramOffsetDateTime: is not a valid type' |
      | 'PATCH'  | '{"nested":{"paramOffsetDateTime":"-1000000000-12-31T23:59:59.999Z"}}' | 'nested.paramOffsetDateTime: is not a valid type' |
      | 'DELETE' | '{"nested":{"paramOffsetDateTime":"-1000000000-12-31T23:59:59.999Z"}}' | 'nested.paramOffsetDateTime: is not a valid type' |
      | 'POST'   | '{"nested":{"paramLocalDateTime":"-1000000000-12-31T23:59:59.999Z"}}'  | 'nested.paramLocalDateTime: is not a valid type'  |
      | 'PUT'    | '{"nested":{"paramLocalDateTime":"-1000000000-12-31T23:59:59.999Z"}}'  | 'nested.paramLocalDateTime: is not a valid type'  |
      | 'PATCH'  | '{"nested":{"paramLocalDateTime":"-1000000000-12-31T23:59:59.999Z"}}'  | 'nested.paramLocalDateTime: is not a valid type'  |
      | 'DELETE' | '{"nested":{"paramLocalDateTime":"-1000000000-12-31T23:59:59.999Z"}}'  | 'nested.paramLocalDateTime: is not a valid type'  |
      | 'POST'   | '{"nested":{"paramLocalDate":"-1000000000-12-31"}}'                    | 'nested.paramLocalDate: is not a valid type'      |
      | 'PUT'    | '{"nested":{"paramLocalDate":"-1000000000-12-31"}}'                    | 'nested.paramLocalDate: is not a valid type'      |
      | 'PATCH'  | '{"nested":{"paramLocalDate":"-1000000000-12-31"}}'                    | 'nested.paramLocalDate: is not a valid type'      |
      | 'DELETE' | '{"nested":{"paramLocalDate":"-1000000000-12-31"}}'                    | 'nested.paramLocalDate: is not a valid type'      |
      | 'POST'   | '{"nested":{"paramYear":"-1000000000"}}'                               | 'nested.paramYear: is not a valid type'           |
      | 'PUT'    | '{"nested":{"paramYear":"-1000000000"}}'                               | 'nested.paramYear: is not a valid type'           |
      | 'PATCH'  | '{"nested":{"paramYear":"-1000000000"}}'                               | 'nested.paramYear: is not a valid type'           |
      | 'DELETE' | '{"nested":{"paramYear":"-1000000000"}}'                               | 'nested.paramYear: is not a valid type'           |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/named-nested-dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in a nested dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
    and the service cannot map the sent value to the declared data type, responding with an error message.

    Given the endpoint '/api/v1/request-body/named-nested-dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | request body                                                              | message                                              |
      | 'POST'   | '{"nested":{"param-integer":-2147483649}}'                                | 'nested.param-integer: is not a valid type'          |
      | 'PUT'    | '{"nested":{"param-integer":-2147483649}}'                                | 'nested.param-integer: is not a valid type'          |
      | 'PATCH'  | '{"nested":{"param-integer":-2147483649}}'                                | 'nested.param-integer: is not a valid type'          |
      | 'DELETE' | '{"nested":{"param-integer":-2147483649}}'                                | 'nested.param-integer: is not a valid type'          |
      | 'POST'   | '{"nested":{"param-long":-9223372036854775809}}'                          | 'nested.param-long: is not a valid type'             |
      | 'PUT'    | '{"nested":{"param-long":-9223372036854775809}}'                          | 'nested.param-long: is not a valid type'             |
      | 'PATCH'  | '{"nested":{"param-long":-9223372036854775809}}'                          | 'nested.param-long: is not a valid type'             |
      | 'DELETE' | '{"nested":{"param-long":-9223372036854775809}}'                          | 'nested.param-long: is not a valid type'             |
      | 'POST'   | '{"nested":{"param-offset-date-time":"-1000000000-12-31T23:59:59.999Z"}}' | 'nested.param-offset-date-time: is not a valid type' |
      | 'PUT'    | '{"nested":{"param-offset-date-time":"-1000000000-12-31T23:59:59.999Z"}}' | 'nested.param-offset-date-time: is not a valid type' |
      | 'PATCH'  | '{"nested":{"param-offset-date-time":"-1000000000-12-31T23:59:59.999Z"}}' | 'nested.param-offset-date-time: is not a valid type' |
      | 'DELETE' | '{"nested":{"param-offset-date-time":"-1000000000-12-31T23:59:59.999Z"}}' | 'nested.param-offset-date-time: is not a valid type' |
      | 'POST'   | '{"nested":{"param-local-date-time":"-1000000000-12-31T23:59:59.999Z"}}'  | 'nested.param-local-date-time: is not a valid type'  |
      | 'PUT'    | '{"nested":{"param-local-date-time":"-1000000000-12-31T23:59:59.999Z"}}'  | 'nested.param-local-date-time: is not a valid type'  |
      | 'PATCH'  | '{"nested":{"param-local-date-time":"-1000000000-12-31T23:59:59.999Z"}}'  | 'nested.param-local-date-time: is not a valid type'  |
      | 'DELETE' | '{"nested":{"param-local-date-time":"-1000000000-12-31T23:59:59.999Z"}}'  | 'nested.param-local-date-time: is not a valid type'  |
      | 'POST'   | '{"nested":{"param-local-date":"-1000000000-12-31"}}'                     | 'nested.param-local-date: is not a valid type'       |
      | 'PUT'    | '{"nested":{"param-local-date":"-1000000000-12-31"}}'                     | 'nested.param-local-date: is not a valid type'       |
      | 'PATCH'  | '{"nested":{"param-local-date":"-1000000000-12-31"}}'                     | 'nested.param-local-date: is not a valid type'       |
      | 'DELETE' | '{"nested":{"param-local-date":"-1000000000-12-31"}}'                     | 'nested.param-local-date: is not a valid type'       |
      | 'POST'   | '{"nested":{"param-year":"-1000000000"}}'                                 | 'nested.param-year: is not a valid type'             |
      | 'PUT'    | '{"nested":{"param-year":"-1000000000"}}'                                 | 'nested.param-year: is not a valid type'             |
      | 'PATCH'  | '{"nested":{"param-year":"-1000000000"}}'                                 | 'nested.param-year: is not a valid type'             |
      | 'DELETE' | '{"nested":{"param-year":"-1000000000"}}'                                 | 'nested.param-year: is not a valid type'             |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/nested-optional-dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in a nested optional dto attribute, both having the same name
    and the service cannot map the sent value to the declared data type, responding with an error message.

    Given the endpoint '/api/v1/request-body/nested-optional-dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | request body                                                           | message                                           |
      | 'POST'   | '{"nested":{"paramInteger":-2147483649}}'                              | 'nested.paramInteger: is not a valid type'        |
      | 'PUT'    | '{"nested":{"paramInteger":-2147483649}}'                              | 'nested.paramInteger: is not a valid type'        |
      | 'PATCH'  | '{"nested":{"paramInteger":-2147483649}}'                              | 'nested.paramInteger: is not a valid type'        |
      | 'DELETE' | '{"nested":{"paramInteger":-2147483649}}'                              | 'nested.paramInteger: is not a valid type'        |
      | 'POST'   | '{"nested":{"paramLong":-9223372036854775809}}'                        | 'nested.paramLong: is not a valid type'           |
      | 'PUT'    | '{"nested":{"paramLong":-9223372036854775809}}'                        | 'nested.paramLong: is not a valid type'           |
      | 'PATCH'  | '{"nested":{"paramLong":-9223372036854775809}}'                        | 'nested.paramLong: is not a valid type'           |
      | 'DELETE' | '{"nested":{"paramLong":-9223372036854775809}}'                        | 'nested.paramLong: is not a valid type'           |
      | 'POST'   | '{"nested":{"paramOffsetDateTime":"-1000000000-12-31T23:59:59.999Z"}}' | 'nested.paramOffsetDateTime: is not a valid type' |
      | 'PUT'    | '{"nested":{"paramOffsetDateTime":"-1000000000-12-31T23:59:59.999Z"}}' | 'nested.paramOffsetDateTime: is not a valid type' |
      | 'PATCH'  | '{"nested":{"paramOffsetDateTime":"-1000000000-12-31T23:59:59.999Z"}}' | 'nested.paramOffsetDateTime: is not a valid type' |
      | 'DELETE' | '{"nested":{"paramOffsetDateTime":"-1000000000-12-31T23:59:59.999Z"}}' | 'nested.paramOffsetDateTime: is not a valid type' |
      | 'POST'   | '{"nested":{"paramLocalDateTime":"-1000000000-12-31T23:59:59.999Z"}}'  | 'nested.paramLocalDateTime: is not a valid type'  |
      | 'PUT'    | '{"nested":{"paramLocalDateTime":"-1000000000-12-31T23:59:59.999Z"}}'  | 'nested.paramLocalDateTime: is not a valid type'  |
      | 'PATCH'  | '{"nested":{"paramLocalDateTime":"-1000000000-12-31T23:59:59.999Z"}}'  | 'nested.paramLocalDateTime: is not a valid type'  |
      | 'DELETE' | '{"nested":{"paramLocalDateTime":"-1000000000-12-31T23:59:59.999Z"}}'  | 'nested.paramLocalDateTime: is not a valid type'  |
      | 'POST'   | '{"nested":{"paramLocalDate":"-1000000000-12-31"}}'                    | 'nested.paramLocalDate: is not a valid type'      |
      | 'PUT'    | '{"nested":{"paramLocalDate":"-1000000000-12-31"}}'                    | 'nested.paramLocalDate: is not a valid type'      |
      | 'PATCH'  | '{"nested":{"paramLocalDate":"-1000000000-12-31"}}'                    | 'nested.paramLocalDate: is not a valid type'      |
      | 'DELETE' | '{"nested":{"paramLocalDate":"-1000000000-12-31"}}'                    | 'nested.paramLocalDate: is not a valid type'      |
      | 'POST'   | '{"nested":{"paramYear":"-1000000000"}}'                               | 'nested.paramYear: is not a valid type'           |
      | 'PUT'    | '{"nested":{"paramYear":"-1000000000"}}'                               | 'nested.paramYear: is not a valid type'           |
      | 'PATCH'  | '{"nested":{"paramYear":"-1000000000"}}'                               | 'nested.paramYear: is not a valid type'           |
      | 'DELETE' | '{"nested":{"paramYear":"-1000000000"}}'                               | 'nested.paramYear: is not a valid type'           |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/named-nested-optional-dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in a nested optional dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
    and the service cannot map the sent value to the declared data type, responding with an error message.

    Given the endpoint '/api/v1/request-body/named-nested-optional-dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | request body                                                              | message                                              |
      | 'POST'   | '{"nested":{"param-integer":-2147483649}}'                                | 'nested.param-integer: is not a valid type'          |
      | 'PUT'    | '{"nested":{"param-integer":-2147483649}}'                                | 'nested.param-integer: is not a valid type'          |
      | 'PATCH'  | '{"nested":{"param-integer":-2147483649}}'                                | 'nested.param-integer: is not a valid type'          |
      | 'DELETE' | '{"nested":{"param-integer":-2147483649}}'                                | 'nested.param-integer: is not a valid type'          |
      | 'POST'   | '{"nested":{"param-long":-9223372036854775809}}'                          | 'nested.param-long: is not a valid type'             |
      | 'PUT'    | '{"nested":{"param-long":-9223372036854775809}}'                          | 'nested.param-long: is not a valid type'             |
      | 'PATCH'  | '{"nested":{"param-long":-9223372036854775809}}'                          | 'nested.param-long: is not a valid type'             |
      | 'DELETE' | '{"nested":{"param-long":-9223372036854775809}}'                          | 'nested.param-long: is not a valid type'             |
      | 'POST'   | '{"nested":{"param-offset-date-time":"-1000000000-12-31T23:59:59.999Z"}}' | 'nested.param-offset-date-time: is not a valid type' |
      | 'PUT'    | '{"nested":{"param-offset-date-time":"-1000000000-12-31T23:59:59.999Z"}}' | 'nested.param-offset-date-time: is not a valid type' |
      | 'PATCH'  | '{"nested":{"param-offset-date-time":"-1000000000-12-31T23:59:59.999Z"}}' | 'nested.param-offset-date-time: is not a valid type' |
      | 'DELETE' | '{"nested":{"param-offset-date-time":"-1000000000-12-31T23:59:59.999Z"}}' | 'nested.param-offset-date-time: is not a valid type' |
      | 'POST'   | '{"nested":{"param-local-date-time":"-1000000000-12-31T23:59:59.999Z"}}'  | 'nested.param-local-date-time: is not a valid type'  |
      | 'PUT'    | '{"nested":{"param-local-date-time":"-1000000000-12-31T23:59:59.999Z"}}'  | 'nested.param-local-date-time: is not a valid type'  |
      | 'PATCH'  | '{"nested":{"param-local-date-time":"-1000000000-12-31T23:59:59.999Z"}}'  | 'nested.param-local-date-time: is not a valid type'  |
      | 'DELETE' | '{"nested":{"param-local-date-time":"-1000000000-12-31T23:59:59.999Z"}}'  | 'nested.param-local-date-time: is not a valid type'  |
      | 'POST'   | '{"nested":{"param-local-date":"-1000000000-12-31"}}'                     | 'nested.param-local-date: is not a valid type'       |
      | 'PUT'    | '{"nested":{"param-local-date":"-1000000000-12-31"}}'                     | 'nested.param-local-date: is not a valid type'       |
      | 'PATCH'  | '{"nested":{"param-local-date":"-1000000000-12-31"}}'                     | 'nested.param-local-date: is not a valid type'       |
      | 'DELETE' | '{"nested":{"param-local-date":"-1000000000-12-31"}}'                     | 'nested.param-local-date: is not a valid type'       |
      | 'POST'   | '{"nested":{"param-year":"-1000000000"}}'                                 | 'nested.param-year: is not a valid type'             |
      | 'PUT'    | '{"nested":{"param-year":"-1000000000"}}'                                 | 'nested.param-year: is not a valid type'             |
      | 'PATCH'  | '{"nested":{"param-year":"-1000000000"}}'                                 | 'nested.param-year: is not a valid type'             |
      | 'DELETE' | '{"nested":{"param-year":"-1000000000"}}'                                 | 'nested.param-year: is not a valid type'             |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/nested-dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in a nested dto attribute from an array, both having the same name
    and the service cannot map the sent value to the declared data type, responding with an error message.

    Given the endpoint '/api/v1/request-body/nested-dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | request body                                                            | message                                             |
      | 'POST'   | '{"array":[{"paramInteger":-2147483649}]}'                              | 'array[0].paramInteger: is not a valid type'        |
      | 'PUT'    | '{"array":[{"paramInteger":-2147483649}]}'                              | 'array[0].paramInteger: is not a valid type'        |
      | 'PATCH'  | '{"array":[{"paramInteger":-2147483649}]}'                              | 'array[0].paramInteger: is not a valid type'        |
      | 'DELETE' | '{"array":[{"paramInteger":-2147483649}]}'                              | 'array[0].paramInteger: is not a valid type'        |
      | 'POST'   | '{"array":[{"paramLong":-9223372036854775809}]}'                        | 'array[0].paramLong: is not a valid type'           |
      | 'PUT'    | '{"array":[{"paramLong":-9223372036854775809}]}'                        | 'array[0].paramLong: is not a valid type'           |
      | 'PATCH'  | '{"array":[{"paramLong":-9223372036854775809}]}'                        | 'array[0].paramLong: is not a valid type'           |
      | 'DELETE' | '{"array":[{"paramLong":-9223372036854775809}]}'                        | 'array[0].paramLong: is not a valid type'           |
      | 'POST'   | '{"array":[{"paramOffsetDateTime":"-1000000000-12-31T23:59:59.999Z"}]}' | 'array[0].paramOffsetDateTime: is not a valid type' |
      | 'PUT'    | '{"array":[{"paramOffsetDateTime":"-1000000000-12-31T23:59:59.999Z"}]}' | 'array[0].paramOffsetDateTime: is not a valid type' |
      | 'PATCH'  | '{"array":[{"paramOffsetDateTime":"-1000000000-12-31T23:59:59.999Z"}]}' | 'array[0].paramOffsetDateTime: is not a valid type' |
      | 'DELETE' | '{"array":[{"paramOffsetDateTime":"-1000000000-12-31T23:59:59.999Z"}]}' | 'array[0].paramOffsetDateTime: is not a valid type' |
      | 'POST'   | '{"array":[{"paramLocalDateTime":"-1000000000-12-31T23:59:59.999Z"}]}'  | 'array[0].paramLocalDateTime: is not a valid type'  |
      | 'PUT'    | '{"array":[{"paramLocalDateTime":"-1000000000-12-31T23:59:59.999Z"}]}'  | 'array[0].paramLocalDateTime: is not a valid type'  |
      | 'PATCH'  | '{"array":[{"paramLocalDateTime":"-1000000000-12-31T23:59:59.999Z"}]}'  | 'array[0].paramLocalDateTime: is not a valid type'  |
      | 'DELETE' | '{"array":[{"paramLocalDateTime":"-1000000000-12-31T23:59:59.999Z"}]}'  | 'array[0].paramLocalDateTime: is not a valid type'  |
      | 'POST'   | '{"array":[{"paramLocalDate":"-1000000000-12-31"}]}'                    | 'array[0].paramLocalDate: is not a valid type'      |
      | 'PUT'    | '{"array":[{"paramLocalDate":"-1000000000-12-31"}]}'                    | 'array[0].paramLocalDate: is not a valid type'      |
      | 'PATCH'  | '{"array":[{"paramLocalDate":"-1000000000-12-31"}]}'                    | 'array[0].paramLocalDate: is not a valid type'      |
      | 'DELETE' | '{"array":[{"paramLocalDate":"-1000000000-12-31"}]}'                    | 'array[0].paramLocalDate: is not a valid type'      |
      | 'POST'   | '{"array":[{"paramYear":"-1000000000"}]}'                               | 'array[0].paramYear: is not a valid type'           |
      | 'PUT'    | '{"array":[{"paramYear":"-1000000000"}]}'                               | 'array[0].paramYear: is not a valid type'           |
      | 'PATCH'  | '{"array":[{"paramYear":"-1000000000"}]}'                               | 'array[0].paramYear: is not a valid type'           |
      | 'DELETE' | '{"array":[{"paramYear":"-1000000000"}]}'                               | 'array[0].paramYear: is not a valid type'           |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/named-nested-dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in a nested dto attribute from an array, both having the diferent names, a custom body-attibute name is defined by the endpoint
    and the service cannot map the sent value to the declared data type, responding with an error message.

    Given the endpoint '/api/v1/request-body/named-nested-dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | request body                                                               | message                                                |
      | 'POST'   | '{"array":[{"param-integer":-2147483649}]}'                                | 'array[0].param-integer: is not a valid type'          |
      | 'PUT'    | '{"array":[{"param-integer":-2147483649}]}'                                | 'array[0].param-integer: is not a valid type'          |
      | 'PATCH'  | '{"array":[{"param-integer":-2147483649}]}'                                | 'array[0].param-integer: is not a valid type'          |
      | 'DELETE' | '{"array":[{"param-integer":-2147483649}]}'                                | 'array[0].param-integer: is not a valid type'          |
      | 'POST'   | '{"array":[{"param-long":-9223372036854775809}]}'                          | 'array[0].param-long: is not a valid type'             |
      | 'PUT'    | '{"array":[{"param-long":-9223372036854775809}]}'                          | 'array[0].param-long: is not a valid type'             |
      | 'PATCH'  | '{"array":[{"param-long":-9223372036854775809}]}'                          | 'array[0].param-long: is not a valid type'             |
      | 'DELETE' | '{"array":[{"param-long":-9223372036854775809}]}'                          | 'array[0].param-long: is not a valid type'             |
      | 'POST'   | '{"array":[{"param-offset-date-time":"-1000000000-12-31T23:59:59.999Z"}]}' | 'array[0].param-offset-date-time: is not a valid type' |
      | 'PUT'    | '{"array":[{"param-offset-date-time":"-1000000000-12-31T23:59:59.999Z"}]}' | 'array[0].param-offset-date-time: is not a valid type' |
      | 'PATCH'  | '{"array":[{"param-offset-date-time":"-1000000000-12-31T23:59:59.999Z"}]}' | 'array[0].param-offset-date-time: is not a valid type' |
      | 'DELETE' | '{"array":[{"param-offset-date-time":"-1000000000-12-31T23:59:59.999Z"}]}' | 'array[0].param-offset-date-time: is not a valid type' |
      | 'POST'   | '{"array":[{"param-local-date-time":"-1000000000-12-31T23:59:59.999Z"}]}'  | 'array[0].param-local-date-time: is not a valid type'  |
      | 'PUT'    | '{"array":[{"param-local-date-time":"-1000000000-12-31T23:59:59.999Z"}]}'  | 'array[0].param-local-date-time: is not a valid type'  |
      | 'PATCH'  | '{"array":[{"param-local-date-time":"-1000000000-12-31T23:59:59.999Z"}]}'  | 'array[0].param-local-date-time: is not a valid type'  |
      | 'DELETE' | '{"array":[{"param-local-date-time":"-1000000000-12-31T23:59:59.999Z"}]}'  | 'array[0].param-local-date-time: is not a valid type'  |
      | 'POST'   | '{"array":[{"param-local-date":"-1000000000-12-31"}]}'                     | 'array[0].param-local-date: is not a valid type'       |
      | 'PUT'    | '{"array":[{"param-local-date":"-1000000000-12-31"}]}'                     | 'array[0].param-local-date: is not a valid type'       |
      | 'PATCH'  | '{"array":[{"param-local-date":"-1000000000-12-31"}]}'                     | 'array[0].param-local-date: is not a valid type'       |
      | 'DELETE' | '{"array":[{"param-local-date":"-1000000000-12-31"}]}'                     | 'array[0].param-local-date: is not a valid type'       |
      | 'POST'   | '{"array":[{"param-year":"-1000000000"}]}'                                 | 'array[0].param-year: is not a valid type'             |
      | 'PUT'    | '{"array":[{"param-year":"-1000000000"}]}'                                 | 'array[0].param-year: is not a valid type'             |
      | 'PATCH'  | '{"array":[{"param-year":"-1000000000"}]}'                                 | 'array[0].param-year: is not a valid type'             |
      | 'DELETE' | '{"array":[{"param-year":"-1000000000"}]}'                                 | 'array[0].param-year: is not a valid type'             |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/nested-optional-dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in a nested optional dto attribute from an array, both having the same name
    and the service cannot map the sent value to the declared data type, responding with an error message.

    Given the endpoint '/api/v1/request-body/nested-optional-dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | request body                                                            | message                                             |
      | 'POST'   | '{"array":[{"paramInteger":-2147483649}]}'                              | 'array[0].paramInteger: is not a valid type'        |
      | 'PUT'    | '{"array":[{"paramInteger":-2147483649}]}'                              | 'array[0].paramInteger: is not a valid type'        |
      | 'PATCH'  | '{"array":[{"paramInteger":-2147483649}]}'                              | 'array[0].paramInteger: is not a valid type'        |
      | 'DELETE' | '{"array":[{"paramInteger":-2147483649}]}'                              | 'array[0].paramInteger: is not a valid type'        |
      | 'POST'   | '{"array":[{"paramLong":-9223372036854775809}]}'                        | 'array[0].paramLong: is not a valid type'           |
      | 'PUT'    | '{"array":[{"paramLong":-9223372036854775809}]}'                        | 'array[0].paramLong: is not a valid type'           |
      | 'PATCH'  | '{"array":[{"paramLong":-9223372036854775809}]}'                        | 'array[0].paramLong: is not a valid type'           |
      | 'DELETE' | '{"array":[{"paramLong":-9223372036854775809}]}'                        | 'array[0].paramLong: is not a valid type'           |
      | 'POST'   | '{"array":[{"paramOffsetDateTime":"-1000000000-12-31T23:59:59.999Z"}]}' | 'array[0].paramOffsetDateTime: is not a valid type' |
      | 'PUT'    | '{"array":[{"paramOffsetDateTime":"-1000000000-12-31T23:59:59.999Z"}]}' | 'array[0].paramOffsetDateTime: is not a valid type' |
      | 'PATCH'  | '{"array":[{"paramOffsetDateTime":"-1000000000-12-31T23:59:59.999Z"}]}' | 'array[0].paramOffsetDateTime: is not a valid type' |
      | 'DELETE' | '{"array":[{"paramOffsetDateTime":"-1000000000-12-31T23:59:59.999Z"}]}' | 'array[0].paramOffsetDateTime: is not a valid type' |
      | 'POST'   | '{"array":[{"paramLocalDateTime":"-1000000000-12-31T23:59:59.999Z"}]}'  | 'array[0].paramLocalDateTime: is not a valid type'  |
      | 'PUT'    | '{"array":[{"paramLocalDateTime":"-1000000000-12-31T23:59:59.999Z"}]}'  | 'array[0].paramLocalDateTime: is not a valid type'  |
      | 'PATCH'  | '{"array":[{"paramLocalDateTime":"-1000000000-12-31T23:59:59.999Z"}]}'  | 'array[0].paramLocalDateTime: is not a valid type'  |
      | 'DELETE' | '{"array":[{"paramLocalDateTime":"-1000000000-12-31T23:59:59.999Z"}]}'  | 'array[0].paramLocalDateTime: is not a valid type'  |
      | 'POST'   | '{"array":[{"paramLocalDate":"-1000000000-12-31"}]}'                    | 'array[0].paramLocalDate: is not a valid type'      |
      | 'PUT'    | '{"array":[{"paramLocalDate":"-1000000000-12-31"}]}'                    | 'array[0].paramLocalDate: is not a valid type'      |
      | 'PATCH'  | '{"array":[{"paramLocalDate":"-1000000000-12-31"}]}'                    | 'array[0].paramLocalDate: is not a valid type'      |
      | 'DELETE' | '{"array":[{"paramLocalDate":"-1000000000-12-31"}]}'                    | 'array[0].paramLocalDate: is not a valid type'      |
      | 'POST'   | '{"array":[{"paramYear":"-1000000000"}]}'                               | 'array[0].paramYear: is not a valid type'           |
      | 'PUT'    | '{"array":[{"paramYear":"-1000000000"}]}'                               | 'array[0].paramYear: is not a valid type'           |
      | 'PATCH'  | '{"array":[{"paramYear":"-1000000000"}]}'                               | 'array[0].paramYear: is not a valid type'           |
      | 'DELETE' | '{"array":[{"paramYear":"-1000000000"}]}'                               | 'array[0].paramYear: is not a valid type'           |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/named-nested-optional-dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in a nested optional dto attribute from an array, both having the diferent names, a custom body-attibute name is defined by the endpoint
    and the service cannot map the sent value to the declared data type, responding with an error message.

    Given the endpoint '/api/v1/request-body/named-nested-optional-dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | request body                                                               | message                                                |
      | 'POST'   | '{"array":[{"param-integer":-2147483649}]}'                                | 'array[0].param-integer: is not a valid type'          |
      | 'PUT'    | '{"array":[{"param-integer":-2147483649}]}'                                | 'array[0].param-integer: is not a valid type'          |
      | 'PATCH'  | '{"array":[{"param-integer":-2147483649}]}'                                | 'array[0].param-integer: is not a valid type'          |
      | 'DELETE' | '{"array":[{"param-integer":-2147483649}]}'                                | 'array[0].param-integer: is not a valid type'          |
      | 'POST'   | '{"array":[{"param-long":-9223372036854775809}]}'                          | 'array[0].param-long: is not a valid type'             |
      | 'PUT'    | '{"array":[{"param-long":-9223372036854775809}]}'                          | 'array[0].param-long: is not a valid type'             |
      | 'PATCH'  | '{"array":[{"param-long":-9223372036854775809}]}'                          | 'array[0].param-long: is not a valid type'             |
      | 'DELETE' | '{"array":[{"param-long":-9223372036854775809}]}'                          | 'array[0].param-long: is not a valid type'             |
      | 'POST'   | '{"array":[{"param-offset-date-time":"-1000000000-12-31T23:59:59.999Z"}]}' | 'array[0].param-offset-date-time: is not a valid type' |
      | 'PUT'    | '{"array":[{"param-offset-date-time":"-1000000000-12-31T23:59:59.999Z"}]}' | 'array[0].param-offset-date-time: is not a valid type' |
      | 'PATCH'  | '{"array":[{"param-offset-date-time":"-1000000000-12-31T23:59:59.999Z"}]}' | 'array[0].param-offset-date-time: is not a valid type' |
      | 'DELETE' | '{"array":[{"param-offset-date-time":"-1000000000-12-31T23:59:59.999Z"}]}' | 'array[0].param-offset-date-time: is not a valid type' |
      | 'POST'   | '{"array":[{"param-local-date-time":"-1000000000-12-31T23:59:59.999Z"}]}'  | 'array[0].param-local-date-time: is not a valid type'  |
      | 'PUT'    | '{"array":[{"param-local-date-time":"-1000000000-12-31T23:59:59.999Z"}]}'  | 'array[0].param-local-date-time: is not a valid type'  |
      | 'PATCH'  | '{"array":[{"param-local-date-time":"-1000000000-12-31T23:59:59.999Z"}]}'  | 'array[0].param-local-date-time: is not a valid type'  |
      | 'DELETE' | '{"array":[{"param-local-date-time":"-1000000000-12-31T23:59:59.999Z"}]}'  | 'array[0].param-local-date-time: is not a valid type'  |
      | 'POST'   | '{"array":[{"param-local-date":"-1000000000-12-31"}]}'                     | 'array[0].param-local-date: is not a valid type'       |
      | 'PUT'    | '{"array":[{"param-local-date":"-1000000000-12-31"}]}'                     | 'array[0].param-local-date: is not a valid type'       |
      | 'PATCH'  | '{"array":[{"param-local-date":"-1000000000-12-31"}]}'                     | 'array[0].param-local-date: is not a valid type'       |
      | 'DELETE' | '{"array":[{"param-local-date":"-1000000000-12-31"}]}'                     | 'array[0].param-local-date: is not a valid type'       |
      | 'POST'   | '{"array":[{"param-year":"-1000000000"}]}'                                 | 'array[0].param-year: is not a valid type'             |
      | 'PUT'    | '{"array":[{"param-year":"-1000000000"}]}'                                 | 'array[0].param-year: is not a valid type'             |
      | 'PATCH'  | '{"array":[{"param-year":"-1000000000"}]}'                                 | 'array[0].param-year: is not a valid type'             |
      | 'DELETE' | '{"array":[{"param-year":"-1000000000"}]}'                                 | 'array[0].param-year: is not a valid type'             |
