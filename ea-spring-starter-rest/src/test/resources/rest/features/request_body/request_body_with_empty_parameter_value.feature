Feature: Request body with empty parameter value
  
  The REST service receives empty string data via the request body and interprets it as null for java-times and as an empty for strings. The service responds with a success message.
  There is an inconsistency in the java-time converter since when faced with an empty-string it should fail instead of returning null.
  
   Rule: The conversion between data types is given by the following table
  
  | parameter type | java type      |
  |----------------|----------------|
  | string         | String         |
  | number         | Integer        |
  | number         | Long           |
  | number         | BigInteger     |
  | number         | Float          |
  | number         | Double         |
  | number         | BigDecimal     |
  | boolean        | Boolean        |
  | ISO-8601       | OffsetDateTime |
  | ISO-8601       | LocalDateTime  |
  | ISO-8601       | LocalDate      |
  | ISO-8601       | LocalTime      |
  | ISO-8601       | Year           |
  | string         | ENUM           |

  Background: 
    Given the default timezone 'America/Puerto_Rico'

  Scenario Outline: The <method> call is made to the /api/v1/request-body/dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in a dto attribute, both having the same name
    and the service correctly maps the sent empty value with the declared data type, responding with a success message.

    Given the endpoint '/api/v1/request-body/dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | request body                 | received value       |
      | 'POST'   | '{"paramString":""}'         | '{"paramString":""}' |
      | 'PUT'    | '{"paramString":""}'         | '{"paramString":""}' |
      | 'PATCH'  | '{"paramString":""}'         | '{"paramString":""}' |
      | 'DELETE' | '{"paramString":""}'         | '{"paramString":""}' |
      | 'POST'   | '{"paramOffsetDateTime":""}' | '{}'                 |
      | 'PUT'    | '{"paramOffsetDateTime":""}' | '{}'                 |
      | 'PATCH'  | '{"paramOffsetDateTime":""}' | '{}'                 |
      | 'DELETE' | '{"paramOffsetDateTime":""}' | '{}'                 |
      | 'POST'   | '{"paramLocalDateTime":""}'  | '{}'                 |
      | 'PUT'    | '{"paramLocalDateTime":""}'  | '{}'                 |
      | 'PATCH'  | '{"paramLocalDateTime":""}'  | '{}'                 |
      | 'DELETE' | '{"paramLocalDateTime":""}'  | '{}'                 |
      | 'POST'   | '{"paramLocalDate":""}'      | '{}'                 |
      | 'PUT'    | '{"paramLocalDate":""}'      | '{}'                 |
      | 'PATCH'  | '{"paramLocalDate":""}'      | '{}'                 |
      | 'DELETE' | '{"paramLocalDate":""}'      | '{}'                 |
      | 'POST'   | '{"paramLocalTime":""}'      | '{}'                 |
      | 'PUT'    | '{"paramLocalTime":""}'      | '{}'                 |
      | 'PATCH'  | '{"paramLocalTime":""}'      | '{}'                 |
      | 'DELETE' | '{"paramLocalTime":""}'      | '{}'                 |
      | 'POST'   | '{"paramYear":""}'           | '{}'                 |
      | 'PUT'    | '{"paramYear":""}'           | '{}'                 |
      | 'PATCH'  | '{"paramYear":""}'           | '{}'                 |
      | 'DELETE' | '{"paramYear":""}'           | '{}'                 |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/named-dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in a dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint 
    and the service correctly maps the sent empty value with the declared data type, responding with a success message.

    Given the endpoint '/api/v1/request-body/named-dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | request body                    | received value       |
      | 'POST'   | '{"param-string":""}'           | '{"paramString":""}' |
      | 'PUT'    | '{"param-string":""}'           | '{"paramString":""}' |
      | 'PATCH'  | '{"param-string":""}'           | '{"paramString":""}' |
      | 'DELETE' | '{"param-string":""}'           | '{"paramString":""}' |
      | 'POST'   | '{"param-offset-date-time":""}' | '{}'                 |
      | 'PUT'    | '{"param-offset-date-time":""}' | '{}'                 |
      | 'PATCH'  | '{"param-offset-date-time":""}' | '{}'                 |
      | 'DELETE' | '{"param-offset-date-time":""}' | '{}'                 |
      | 'POST'   | '{"param-local-date-time":""}'  | '{}'                 |
      | 'PUT'    | '{"param-local-date-time":""}'  | '{}'                 |
      | 'PATCH'  | '{"param-local-date-time":""}'  | '{}'                 |
      | 'DELETE' | '{"param-local-date-time":""}'  | '{}'                 |
      | 'POST'   | '{"param-local-date":""}'       | '{}'                 |
      | 'PUT'    | '{"param-local-date":""}'       | '{}'                 |
      | 'PATCH'  | '{"param-local-date":""}'       | '{}'                 |
      | 'DELETE' | '{"param-local-date":""}'       | '{}'                 |
      | 'POST'   | '{"param-local-time":""}'       | '{}'                 |
      | 'PUT'    | '{"param-local-time":""}'       | '{}'                 |
      | 'PATCH'  | '{"param-local-time":""}'       | '{}'                 |
      | 'DELETE' | '{"param-local-time":""}'       | '{}'                 |
      | 'POST'   | '{"param-year":""}'             | '{}'                 |
      | 'PUT'    | '{"param-year":""}'             | '{}'                 |
      | 'PATCH'  | '{"param-year":""}'             | '{}'                 |
      | 'DELETE' | '{"param-year":""}'             | '{}'                 |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/optional-dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in an optional dto attribute, both having the same name
    and the service correctly maps the sent empty value with the declared data type as an optional-empty, responding with a success message.

    Given the endpoint '/api/v1/request-body/optional-dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | request body                 | received value                 |
      | 'POST'   | '{"paramString":""}'         | '{"paramString":""}'           |
      | 'PUT'    | '{"paramString":""}'         | '{"paramString":""}'           |
      | 'PATCH'  | '{"paramString":""}'         | '{"paramString":""}'           |
      | 'DELETE' | '{"paramString":""}'         | '{"paramString":""}'           |
      | 'POST'   | '{"paramOffsetDateTime":""}' | '{"paramOffsetDateTime":null}' |
      | 'PUT'    | '{"paramOffsetDateTime":""}' | '{"paramOffsetDateTime":null}' |
      | 'PATCH'  | '{"paramOffsetDateTime":""}' | '{"paramOffsetDateTime":null}' |
      | 'DELETE' | '{"paramOffsetDateTime":""}' | '{"paramOffsetDateTime":null}' |
      | 'POST'   | '{"paramLocalDateTime":""}'  | '{"paramLocalDateTime":null}'  |
      | 'PUT'    | '{"paramLocalDateTime":""}'  | '{"paramLocalDateTime":null}'  |
      | 'PATCH'  | '{"paramLocalDateTime":""}'  | '{"paramLocalDateTime":null}'  |
      | 'DELETE' | '{"paramLocalDateTime":""}'  | '{"paramLocalDateTime":null}'  |
      | 'POST'   | '{"paramLocalDate":""}'      | '{"paramLocalDate":null}'      |
      | 'PUT'    | '{"paramLocalDate":""}'      | '{"paramLocalDate":null}'      |
      | 'PATCH'  | '{"paramLocalDate":""}'      | '{"paramLocalDate":null}'      |
      | 'DELETE' | '{"paramLocalDate":""}'      | '{"paramLocalDate":null}'      |
      | 'POST'   | '{"paramLocalTime":""}'      | '{"paramLocalTime":null}'      |
      | 'PUT'    | '{"paramLocalTime":""}'      | '{"paramLocalTime":null}'      |
      | 'PATCH'  | '{"paramLocalTime":""}'      | '{"paramLocalTime":null}'      |
      | 'DELETE' | '{"paramLocalTime":""}'      | '{"paramLocalTime":null}'      |
      | 'POST'   | '{"paramYear":""}'           | '{"paramYear":null}'           |
      | 'PUT'    | '{"paramYear":""}'           | '{"paramYear":null}'           |
      | 'PATCH'  | '{"paramYear":""}'           | '{"paramYear":null}'           |
      | 'DELETE' | '{"paramYear":""}'           | '{"paramYear":null}'           |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/named-optional-dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in an optional dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
    and the service correctly maps the sent empty value with the declared data type as an optional-empty, responding with a success message.

    Given the endpoint '/api/v1/request-body/named-optional-dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | request body                    | received value                 |
      | 'POST'   | '{"param-string":""}'           | '{"paramString":""}'           |
      | 'PUT'    | '{"param-string":""}'           | '{"paramString":""}'           |
      | 'PATCH'  | '{"param-string":""}'           | '{"paramString":""}'           |
      | 'DELETE' | '{"param-string":""}'           | '{"paramString":""}'           |
      | 'POST'   | '{"param-offset-date-time":""}' | '{"paramOffsetDateTime":null}' |
      | 'PUT'    | '{"param-offset-date-time":""}' | '{"paramOffsetDateTime":null}' |
      | 'PATCH'  | '{"param-offset-date-time":""}' | '{"paramOffsetDateTime":null}' |
      | 'DELETE' | '{"param-offset-date-time":""}' | '{"paramOffsetDateTime":null}' |
      | 'POST'   | '{"param-local-date-time":""}'  | '{"paramLocalDateTime":null}'  |
      | 'PUT'    | '{"param-local-date-time":""}'  | '{"paramLocalDateTime":null}'  |
      | 'PATCH'  | '{"param-local-date-time":""}'  | '{"paramLocalDateTime":null}'  |
      | 'DELETE' | '{"param-local-date-time":""}'  | '{"paramLocalDateTime":null}'  |
      | 'POST'   | '{"param-local-date":""}'       | '{"paramLocalDate":null}'      |
      | 'PUT'    | '{"param-local-date":""}'       | '{"paramLocalDate":null}'      |
      | 'PATCH'  | '{"param-local-date":""}'       | '{"paramLocalDate":null}'      |
      | 'DELETE' | '{"param-local-date":""}'       | '{"paramLocalDate":null}'      |
      | 'POST'   | '{"param-local-time":""}'       | '{"paramLocalTime":null}'      |
      | 'PUT'    | '{"param-local-time":""}'       | '{"paramLocalTime":null}'      |
      | 'PATCH'  | '{"param-local-time":""}'       | '{"paramLocalTime":null}'      |
      | 'DELETE' | '{"param-local-time":""}'       | '{"paramLocalTime":null}'      |
      | 'POST'   | '{"param-year":""}'             | '{"paramYear":null}'           |
      | 'PUT'    | '{"param-year":""}'             | '{"paramYear":null}'           |
      | 'PATCH'  | '{"param-year":""}'             | '{"paramYear":null}'           |
      | 'DELETE' | '{"param-year":""}'             | '{"paramYear":null}'           |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/nested-dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in a nested dto attribute, both having the same name
    and the service correctly maps the sent empty value with the declared data type, responding with a success message.

    Given the endpoint '/api/v1/request-body/nested-dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | request body                            | received value                  |
      | 'POST'   | '{"nested":{"paramString":""}}'         | '{"nested":{"paramString":""}}' |
      | 'PUT'    | '{"nested":{"paramString":""}}'         | '{"nested":{"paramString":""}}' |
      | 'PATCH'  | '{"nested":{"paramString":""}}'         | '{"nested":{"paramString":""}}' |
      | 'DELETE' | '{"nested":{"paramString":""}}'         | '{"nested":{"paramString":""}}' |
      | 'POST'   | '{"nested":{"paramOffsetDateTime":""}}' | '{"nested":{}}'                 |
      | 'PUT'    | '{"nested":{"paramOffsetDateTime":""}}' | '{"nested":{}}'                 |
      | 'PATCH'  | '{"nested":{"paramOffsetDateTime":""}}' | '{"nested":{}}'                 |
      | 'DELETE' | '{"nested":{"paramOffsetDateTime":""}}' | '{"nested":{}}'                 |
      | 'POST'   | '{"nested":{"paramLocalDateTime":""}}'  | '{"nested":{}}'                 |
      | 'PUT'    | '{"nested":{"paramLocalDateTime":""}}'  | '{"nested":{}}'                 |
      | 'PATCH'  | '{"nested":{"paramLocalDateTime":""}}'  | '{"nested":{}}'                 |
      | 'DELETE' | '{"nested":{"paramLocalDateTime":""}}'  | '{"nested":{}}'                 |
      | 'POST'   | '{"nested":{"paramLocalDate":""}}'      | '{"nested":{}}'                 |
      | 'PUT'    | '{"nested":{"paramLocalDate":""}}'      | '{"nested":{}}'                 |
      | 'PATCH'  | '{"nested":{"paramLocalDate":""}}'      | '{"nested":{}}'                 |
      | 'DELETE' | '{"nested":{"paramLocalDate":""}}'      | '{"nested":{}}'                 |
      | 'POST'   | '{"nested":{"paramLocalTime":""}}'      | '{"nested":{}}'                 |
      | 'PUT'    | '{"nested":{"paramLocalTime":""}}'      | '{"nested":{}}'                 |
      | 'PATCH'  | '{"nested":{"paramLocalTime":""}}'      | '{"nested":{}}'                 |
      | 'DELETE' | '{"nested":{"paramLocalTime":""}}'      | '{"nested":{}}'                 |
      | 'POST'   | '{"nested":{"paramYear":""}}'           | '{"nested":{}}'                 |
      | 'PUT'    | '{"nested":{"paramYear":""}}'           | '{"nested":{}}'                 |
      | 'PATCH'  | '{"nested":{"paramYear":""}}'           | '{"nested":{}}'                 |
      | 'DELETE' | '{"nested":{"paramYear":""}}'           | '{"nested":{}}'                 |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/named-nested-dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in a nested dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
    and the service correctly maps the sent empty value with the declared data type, responding with a success message.

    Given the endpoint '/api/v1/request-body/named-nested-dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | request body                               | received value                  |
      | 'POST'   | '{"nested":{"param-string":""}}'           | '{"nested":{"paramString":""}}' |
      | 'PUT'    | '{"nested":{"param-string":""}}'           | '{"nested":{"paramString":""}}' |
      | 'PATCH'  | '{"nested":{"param-string":""}}'           | '{"nested":{"paramString":""}}' |
      | 'DELETE' | '{"nested":{"param-string":""}}'           | '{"nested":{"paramString":""}}' |
      | 'POST'   | '{"nested":{"param-offset-date-time":""}}' | '{"nested":{}}'                 |
      | 'PUT'    | '{"nested":{"param-offset-date-time":""}}' | '{"nested":{}}'                 |
      | 'PATCH'  | '{"nested":{"param-offset-date-time":""}}' | '{"nested":{}}'                 |
      | 'DELETE' | '{"nested":{"param-offset-date-time":""}}' | '{"nested":{}}'                 |
      | 'POST'   | '{"nested":{"param-local-date-time":""}}'  | '{"nested":{}}'                 |
      | 'PUT'    | '{"nested":{"param-local-date-time":""}}'  | '{"nested":{}}'                 |
      | 'PATCH'  | '{"nested":{"param-local-date-time":""}}'  | '{"nested":{}}'                 |
      | 'DELETE' | '{"nested":{"param-local-date-time":""}}'  | '{"nested":{}}'                 |
      | 'POST'   | '{"nested":{"param-local-date":""}}'       | '{"nested":{}}'                 |
      | 'PUT'    | '{"nested":{"param-local-date":""}}'       | '{"nested":{}}'                 |
      | 'PATCH'  | '{"nested":{"param-local-date":""}}'       | '{"nested":{}}'                 |
      | 'DELETE' | '{"nested":{"param-local-date":""}}'       | '{"nested":{}}'                 |
      | 'POST'   | '{"nested":{"param-local-time":""}}'       | '{"nested":{}}'                 |
      | 'PUT'    | '{"nested":{"param-local-time":""}}'       | '{"nested":{}}'                 |
      | 'PATCH'  | '{"nested":{"param-local-time":""}}'       | '{"nested":{}}'                 |
      | 'DELETE' | '{"nested":{"param-local-time":""}}'       | '{"nested":{}}'                 |
      | 'POST'   | '{"nested":{"param-year":""}}'             | '{"nested":{}}'                 |
      | 'PUT'    | '{"nested":{"param-year":""}}'             | '{"nested":{}}'                 |
      | 'PATCH'  | '{"nested":{"param-year":""}}'             | '{"nested":{}}'                 |
      | 'DELETE' | '{"nested":{"param-year":""}}'             | '{"nested":{}}'                 |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/nested-optional-dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in a nested optional dto attribute, both having the same name
    and the service correctly maps the sent empty value with the declared data type, responding with a success message.

    Given the endpoint '/api/v1/request-body/nested-optional-dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | request body                            | received value                            |
      | 'POST'   | '{"nested":{"paramString":""}}'         | '{"nested":{"paramString":""}}'           |
      | 'PUT'    | '{"nested":{"paramString":""}}'         | '{"nested":{"paramString":""}}'           |
      | 'PATCH'  | '{"nested":{"paramString":""}}'         | '{"nested":{"paramString":""}}'           |
      | 'DELETE' | '{"nested":{"paramString":""}}'         | '{"nested":{"paramString":""}}'           |
      | 'POST'   | '{"nested":{"paramOffsetDateTime":""}}' | '{"nested":{"paramOffsetDateTime":null}}' |
      | 'PUT'    | '{"nested":{"paramOffsetDateTime":""}}' | '{"nested":{"paramOffsetDateTime":null}}' |
      | 'PATCH'  | '{"nested":{"paramOffsetDateTime":""}}' | '{"nested":{"paramOffsetDateTime":null}}' |
      | 'DELETE' | '{"nested":{"paramOffsetDateTime":""}}' | '{"nested":{"paramOffsetDateTime":null}}' |
      | 'POST'   | '{"nested":{"paramLocalDateTime":""}}'  | '{"nested":{"paramLocalDateTime":null}}'  |
      | 'PUT'    | '{"nested":{"paramLocalDateTime":""}}'  | '{"nested":{"paramLocalDateTime":null}}'  |
      | 'PATCH'  | '{"nested":{"paramLocalDateTime":""}}'  | '{"nested":{"paramLocalDateTime":null}}'  |
      | 'DELETE' | '{"nested":{"paramLocalDateTime":""}}'  | '{"nested":{"paramLocalDateTime":null}}'  |
      | 'POST'   | '{"nested":{"paramLocalDate":""}}'      | '{"nested":{"paramLocalDate":null}}'      |
      | 'PUT'    | '{"nested":{"paramLocalDate":""}}'      | '{"nested":{"paramLocalDate":null}}'      |
      | 'PATCH'  | '{"nested":{"paramLocalDate":""}}'      | '{"nested":{"paramLocalDate":null}}'      |
      | 'DELETE' | '{"nested":{"paramLocalDate":""}}'      | '{"nested":{"paramLocalDate":null}}'      |
      | 'POST'   | '{"nested":{"paramLocalTime":""}}'      | '{"nested":{"paramLocalTime":null}}'      |
      | 'PUT'    | '{"nested":{"paramLocalTime":""}}'      | '{"nested":{"paramLocalTime":null}}'      |
      | 'PATCH'  | '{"nested":{"paramLocalTime":""}}'      | '{"nested":{"paramLocalTime":null}}'      |
      | 'DELETE' | '{"nested":{"paramLocalTime":""}}'      | '{"nested":{"paramLocalTime":null}}'      |
      | 'POST'   | '{"nested":{"paramYear":""}}'           | '{"nested":{"paramYear":null}}'           |
      | 'PUT'    | '{"nested":{"paramYear":""}}'           | '{"nested":{"paramYear":null}}'           |
      | 'PATCH'  | '{"nested":{"paramYear":""}}'           | '{"nested":{"paramYear":null}}'           |
      | 'DELETE' | '{"nested":{"paramYear":""}}'           | '{"nested":{"paramYear":null}}'           |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/named-nested-optional-dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in a nested dto optional attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
    and the service correctly maps the sent empty value with the declared data type, responding with a success message.

    Given the endpoint '/api/v1/request-body/named-nested-optional-dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | request body                               | received value                            |
      | 'POST'   | '{"nested":{"param-string":""}}'           | '{"nested":{"paramString":""}}'           |
      | 'PUT'    | '{"nested":{"param-string":""}}'           | '{"nested":{"paramString":""}}'           |
      | 'PATCH'  | '{"nested":{"param-string":""}}'           | '{"nested":{"paramString":""}}'           |
      | 'DELETE' | '{"nested":{"param-string":""}}'           | '{"nested":{"paramString":""}}'           |
      | 'POST'   | '{"nested":{"param-offset-date-time":""}}' | '{"nested":{"paramOffsetDateTime":null}}' |
      | 'PUT'    | '{"nested":{"param-offset-date-time":""}}' | '{"nested":{"paramOffsetDateTime":null}}' |
      | 'PATCH'  | '{"nested":{"param-offset-date-time":""}}' | '{"nested":{"paramOffsetDateTime":null}}' |
      | 'DELETE' | '{"nested":{"param-offset-date-time":""}}' | '{"nested":{"paramOffsetDateTime":null}}' |
      | 'POST'   | '{"nested":{"param-local-date-time":""}}'  | '{"nested":{"paramLocalDateTime":null}}'  |
      | 'PUT'    | '{"nested":{"param-local-date-time":""}}'  | '{"nested":{"paramLocalDateTime":null}}'  |
      | 'PATCH'  | '{"nested":{"param-local-date-time":""}}'  | '{"nested":{"paramLocalDateTime":null}}'  |
      | 'DELETE' | '{"nested":{"param-local-date-time":""}}'  | '{"nested":{"paramLocalDateTime":null}}'  |
      | 'POST'   | '{"nested":{"param-local-date":""}}'       | '{"nested":{"paramLocalDate":null}}'      |
      | 'PUT'    | '{"nested":{"param-local-date":""}}'       | '{"nested":{"paramLocalDate":null}}'      |
      | 'PATCH'  | '{"nested":{"param-local-date":""}}'       | '{"nested":{"paramLocalDate":null}}'      |
      | 'DELETE' | '{"nested":{"param-local-date":""}}'       | '{"nested":{"paramLocalDate":null}}'      |
      | 'POST'   | '{"nested":{"param-local-time":""}}'       | '{"nested":{"paramLocalTime":null}}'      |
      | 'PUT'    | '{"nested":{"param-local-time":""}}'       | '{"nested":{"paramLocalTime":null}}'      |
      | 'PATCH'  | '{"nested":{"param-local-time":""}}'       | '{"nested":{"paramLocalTime":null}}'      |
      | 'DELETE' | '{"nested":{"param-local-time":""}}'       | '{"nested":{"paramLocalTime":null}}'      |
      | 'POST'   | '{"nested":{"param-year":""}}'             | '{"nested":{"paramYear":null}}'           |
      | 'PUT'    | '{"nested":{"param-year":""}}'             | '{"nested":{"paramYear":null}}'           |
      | 'PATCH'  | '{"nested":{"param-year":""}}'             | '{"nested":{"paramYear":null}}'           |
      | 'DELETE' | '{"nested":{"param-year":""}}'             | '{"nested":{"paramYear":null}}'           |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/nested-dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in a nested dto attribute from an array, both having the same name
    and the service correctly maps the sent empty value with the declared data type, responding with a success message.

    Given the endpoint '/api/v1/request-body/nested-dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | request body                             | received value                   |
      | 'POST'   | '{"array":[{"paramString":""}]}'         | '{"array":[{"paramString":""}]}' |
      | 'PUT'    | '{"array":[{"paramString":""}]}'         | '{"array":[{"paramString":""}]}' |
      | 'PATCH'  | '{"array":[{"paramString":""}]}'         | '{"array":[{"paramString":""}]}' |
      | 'DELETE' | '{"array":[{"paramString":""}]}'         | '{"array":[{"paramString":""}]}' |
      | 'POST'   | '{"array":[{"paramOffsetDateTime":""}]}' | '{"array":[{}]}'                 |
      | 'PUT'    | '{"array":[{"paramOffsetDateTime":""}]}' | '{"array":[{}]}'                 |
      | 'PATCH'  | '{"array":[{"paramOffsetDateTime":""}]}' | '{"array":[{}]}'                 |
      | 'DELETE' | '{"array":[{"paramOffsetDateTime":""}]}' | '{"array":[{}]}'                 |
      | 'POST'   | '{"array":[{"paramLocalDateTime":""}]}'  | '{"array":[{}]}'                 |
      | 'PUT'    | '{"array":[{"paramLocalDateTime":""}]}'  | '{"array":[{}]}'                 |
      | 'PATCH'  | '{"array":[{"paramLocalDateTime":""}]}'  | '{"array":[{}]}'                 |
      | 'DELETE' | '{"array":[{"paramLocalDateTime":""}]}'  | '{"array":[{}]}'                 |
      | 'POST'   | '{"array":[{"paramLocalDate":""}]}'      | '{"array":[{}]}'                 |
      | 'PUT'    | '{"array":[{"paramLocalDate":""}]}'      | '{"array":[{}]}'                 |
      | 'PATCH'  | '{"array":[{"paramLocalDate":""}]}'      | '{"array":[{}]}'                 |
      | 'DELETE' | '{"array":[{"paramLocalDate":""}]}'      | '{"array":[{}]}'                 |
      | 'POST'   | '{"array":[{"paramLocalTime":""}]}'      | '{"array":[{}]}'                 |
      | 'PUT'    | '{"array":[{"paramLocalTime":""}]}'      | '{"array":[{}]}'                 |
      | 'PATCH'  | '{"array":[{"paramLocalTime":""}]}'      | '{"array":[{}]}'                 |
      | 'DELETE' | '{"array":[{"paramLocalTime":""}]}'      | '{"array":[{}]}'                 |
      | 'POST'   | '{"array":[{"paramYear":""}]}'           | '{"array":[{}]}'                 |
      | 'PUT'    | '{"array":[{"paramYear":""}]}'           | '{"array":[{}]}'                 |
      | 'PATCH'  | '{"array":[{"paramYear":""}]}'           | '{"array":[{}]}'                 |
      | 'DELETE' | '{"array":[{"paramYear":""}]}'           | '{"array":[{}]}'                 |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/named-nested-dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in a nested dto attribute from an array, both having the diferent names, a custom body-attibute name is defined by the endpoint
    and the service correctly maps the sent empty value with the declared data type, responding with a success message.

    Given the endpoint '/api/v1/request-body/named-nested-dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | request body                                | received value                   |
      | 'POST'   | '{"array":[{"param-string":""}]}'           | '{"array":[{"paramString":""}]}' |
      | 'PUT'    | '{"array":[{"param-string":""}]}'           | '{"array":[{"paramString":""}]}' |
      | 'PATCH'  | '{"array":[{"param-string":""}]}'           | '{"array":[{"paramString":""}]}' |
      | 'DELETE' | '{"array":[{"param-string":""}]}'           | '{"array":[{"paramString":""}]}' |
      | 'POST'   | '{"array":[{"param-offset-date-time":""}]}' | '{"array":[{}]}'                 |
      | 'PUT'    | '{"array":[{"param-offset-date-time":""}]}' | '{"array":[{}]}'                 |
      | 'PATCH'  | '{"array":[{"param-offset-date-time":""}]}' | '{"array":[{}]}'                 |
      | 'DELETE' | '{"array":[{"param-offset-date-time":""}]}' | '{"array":[{}]}'                 |
      | 'POST'   | '{"array":[{"param-local-date-time":""}]}'  | '{"array":[{}]}'                 |
      | 'PUT'    | '{"array":[{"param-local-date-time":""}]}'  | '{"array":[{}]}'                 |
      | 'PATCH'  | '{"array":[{"param-local-date-time":""}]}'  | '{"array":[{}]}'                 |
      | 'DELETE' | '{"array":[{"param-local-date-time":""}]}'  | '{"array":[{}]}'                 |
      | 'POST'   | '{"array":[{"param-local-date":""}]}'       | '{"array":[{}]}'                 |
      | 'PUT'    | '{"array":[{"param-local-date":""}]}'       | '{"array":[{}]}'                 |
      | 'PATCH'  | '{"array":[{"param-local-date":""}]}'       | '{"array":[{}]}'                 |
      | 'DELETE' | '{"array":[{"param-local-date":""}]}'       | '{"array":[{}]}'                 |
      | 'POST'   | '{"array":[{"param-local-time":""}]}'       | '{"array":[{}]}'                 |
      | 'PUT'    | '{"array":[{"param-local-time":""}]}'       | '{"array":[{}]}'                 |
      | 'PATCH'  | '{"array":[{"param-local-time":""}]}'       | '{"array":[{}]}'                 |
      | 'DELETE' | '{"array":[{"param-local-time":""}]}'       | '{"array":[{}]}'                 |
      | 'POST'   | '{"array":[{"param-year":""}]}'             | '{"array":[{}]}'                 |
      | 'PUT'    | '{"array":[{"param-year":""}]}'             | '{"array":[{}]}'                 |
      | 'PATCH'  | '{"array":[{"param-year":""}]}'             | '{"array":[{}]}'                 |
      | 'DELETE' | '{"array":[{"param-year":""}]}'             | '{"array":[{}]}'                 |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/nested-dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in a nested dto attribute from an array, both having the same name
    and the service correctly maps the sent empty value with the declared data type, responding with a success message.

    Given the endpoint '/api/v1/request-body/nested-dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | request body                             | received value                   |
      | 'POST'   | '{"array":[{"paramString":""}]}'         | '{"array":[{"paramString":""}]}' |
      | 'PUT'    | '{"array":[{"paramString":""}]}'         | '{"array":[{"paramString":""}]}' |
      | 'PATCH'  | '{"array":[{"paramString":""}]}'         | '{"array":[{"paramString":""}]}' |
      | 'DELETE' | '{"array":[{"paramString":""}]}'         | '{"array":[{"paramString":""}]}' |
      | 'POST'   | '{"array":[{"paramOffsetDateTime":""}]}' | '{"array":[{}]}'                 |
      | 'PUT'    | '{"array":[{"paramOffsetDateTime":""}]}' | '{"array":[{}]}'                 |
      | 'PATCH'  | '{"array":[{"paramOffsetDateTime":""}]}' | '{"array":[{}]}'                 |
      | 'DELETE' | '{"array":[{"paramOffsetDateTime":""}]}' | '{"array":[{}]}'                 |
      | 'POST'   | '{"array":[{"paramLocalDateTime":""}]}'  | '{"array":[{}]}'                 |
      | 'PUT'    | '{"array":[{"paramLocalDateTime":""}]}'  | '{"array":[{}]}'                 |
      | 'PATCH'  | '{"array":[{"paramLocalDateTime":""}]}'  | '{"array":[{}]}'                 |
      | 'DELETE' | '{"array":[{"paramLocalDateTime":""}]}'  | '{"array":[{}]}'                 |
      | 'POST'   | '{"array":[{"paramLocalDate":""}]}'      | '{"array":[{}]}'                 |
      | 'PUT'    | '{"array":[{"paramLocalDate":""}]}'      | '{"array":[{}]}'                 |
      | 'PATCH'  | '{"array":[{"paramLocalDate":""}]}'      | '{"array":[{}]}'                 |
      | 'DELETE' | '{"array":[{"paramLocalDate":""}]}'      | '{"array":[{}]}'                 |
      | 'POST'   | '{"array":[{"paramLocalTime":""}]}'      | '{"array":[{}]}'                 |
      | 'PUT'    | '{"array":[{"paramLocalTime":""}]}'      | '{"array":[{}]}'                 |
      | 'PATCH'  | '{"array":[{"paramLocalTime":""}]}'      | '{"array":[{}]}'                 |
      | 'DELETE' | '{"array":[{"paramLocalTime":""}]}'      | '{"array":[{}]}'                 |
      | 'POST'   | '{"array":[{"paramYear":""}]}'           | '{"array":[{}]}'                 |
      | 'PUT'    | '{"array":[{"paramYear":""}]}'           | '{"array":[{}]}'                 |
      | 'PATCH'  | '{"array":[{"paramYear":""}]}'           | '{"array":[{}]}'                 |
      | 'DELETE' | '{"array":[{"paramYear":""}]}'           | '{"array":[{}]}'                 |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/named-nested-optional-dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in a nested optional dto attribute from an array, both having the diferent names, a custom body-attibute name is defined by the endpoint
    and the service correctly maps the sent empty value with the declared data type, responding with a success message.

    Given the endpoint '/api/v1/request-body/named-nested-optional-dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | request body                                | received value                             |
      | 'POST'   | '{"array":[{"param-string":""}]}'           | '{"array":[{"paramString":""}]}'           |
      | 'PUT'    | '{"array":[{"param-string":""}]}'           | '{"array":[{"paramString":""}]}'           |
      | 'PATCH'  | '{"array":[{"param-string":""}]}'           | '{"array":[{"paramString":""}]}'           |
      | 'DELETE' | '{"array":[{"param-string":""}]}'           | '{"array":[{"paramString":""}]}'           |
      | 'POST'   | '{"array":[{"param-offset-date-time":""}]}' | '{"array":[{"paramOffsetDateTime":null}]}' |
      | 'PUT'    | '{"array":[{"param-offset-date-time":""}]}' | '{"array":[{"paramOffsetDateTime":null}]}' |
      | 'PATCH'  | '{"array":[{"param-offset-date-time":""}]}' | '{"array":[{"paramOffsetDateTime":null}]}' |
      | 'DELETE' | '{"array":[{"param-offset-date-time":""}]}' | '{"array":[{"paramOffsetDateTime":null}]}' |
      | 'POST'   | '{"array":[{"param-local-date-time":""}]}'  | '{"array":[{"paramLocalDateTime":null}]}'  |
      | 'PUT'    | '{"array":[{"param-local-date-time":""}]}'  | '{"array":[{"paramLocalDateTime":null}]}'  |
      | 'PATCH'  | '{"array":[{"param-local-date-time":""}]}'  | '{"array":[{"paramLocalDateTime":null}]}'  |
      | 'DELETE' | '{"array":[{"param-local-date-time":""}]}'  | '{"array":[{"paramLocalDateTime":null}]}'  |
      | 'POST'   | '{"array":[{"param-local-date":""}]}'       | '{"array":[{"paramLocalDate":null}]}'      |
      | 'PUT'    | '{"array":[{"param-local-date":""}]}'       | '{"array":[{"paramLocalDate":null}]}'      |
      | 'PATCH'  | '{"array":[{"param-local-date":""}]}'       | '{"array":[{"paramLocalDate":null}]}'      |
      | 'DELETE' | '{"array":[{"param-local-date":""}]}'       | '{"array":[{"paramLocalDate":null}]}'      |
      | 'POST'   | '{"array":[{"param-local-time":""}]}'       | '{"array":[{"paramLocalTime":null}]}'      |
      | 'PUT'    | '{"array":[{"param-local-time":""}]}'       | '{"array":[{"paramLocalTime":null}]}'      |
      | 'PATCH'  | '{"array":[{"param-local-time":""}]}'       | '{"array":[{"paramLocalTime":null}]}'      |
      | 'DELETE' | '{"array":[{"param-local-time":""}]}'       | '{"array":[{"paramLocalTime":null}]}'      |
      | 'POST'   | '{"array":[{"param-year":""}]}'             | '{"array":[{"paramYear":null}]}'           |
      | 'PUT'    | '{"array":[{"param-year":""}]}'             | '{"array":[{"paramYear":null}]}'           |
      | 'PATCH'  | '{"array":[{"param-year":""}]}'             | '{"array":[{"paramYear":null}]}'           |
      | 'DELETE' | '{"array":[{"param-year":""}]}'             | '{"array":[{"paramYear":null}]}'           |
