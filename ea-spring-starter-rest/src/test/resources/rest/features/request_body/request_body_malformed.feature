Feature: Request body malformed
  
  The REST service receives data via the request body some malformed bodies. The service responds with an error message.
  
   Rule: The conversion between data types is given by the following table
  
  | parameter type | java type      |
  |----------------|----------------|
  | string         | String         |
  | number         | Integer        |
  | number         | Long           |
  | number         | BigInteger     |
  | number         | Float          |
  | number         | Double         |
  | number         | BigDecimal     |
  | boolean        | Boolean        |
  | ISO-8601       | OffsetDateTime |
  | ISO-8601       | LocalDateTime  |
  | ISO-8601       | LocalDate      |
  | ISO-8601       | LocalTime      |
  | ISO-8601       | Year           |
  | string         | ENUM           |

  Background: 
    Given the default timezone 'America/Puerto_Rico'

  Scenario Outline: The <method> call is made to the /api/v1/request-body/dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives and malformed request body, responding with a error message.

    Given the endpoint '/api/v1/request-body/dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | request body            | message                            |
      | 'POST'   | '{"paramString":"id",}' | 'JSON parse error'                 |
      | 'PUT'    | '{"paramString":"id",}' | 'JSON parse error'                 |
      | 'PATCH'  | '{"paramString":"id",}' | 'JSON parse error'                 |
      | 'DELETE' | '{"paramString":"id",}' | 'JSON parse error'                 |
      | 'POST'   | '{"paramString":"id"'   | 'JSON parse error'                 |
      | 'PUT'    | '{"paramString":"id"'   | 'JSON parse error'                 |
      | 'PATCH'  | '{"paramString":"id"'   | 'JSON parse error'                 |
      | 'DELETE' | '{"paramString":"id"'   | 'JSON parse error'                 |
      | 'POST'   | 'QWERTY'                | 'JSON parse error'                 |
      | 'PUT'    | 'QWERTY'                | 'JSON parse error'                 |
      | 'PATCH'  | 'QWERTY'                | 'JSON parse error'                 |
      | 'DELETE' | 'QWERTY'                | 'JSON parse error'                 |
      | 'POST'   | ''                      | 'Required request body is missing' |
      | 'PUT'    | ''                      | 'Required request body is missing' |
      | 'PATCH'  | ''                      | 'Required request body is missing' |
      | 'DELETE' | ''                      | 'Required request body is missing' |
