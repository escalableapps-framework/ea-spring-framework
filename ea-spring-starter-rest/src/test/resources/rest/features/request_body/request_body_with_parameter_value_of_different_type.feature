Feature: Request body with parameter value of different type.
  
  The REST service receives data via the request body and cannot interpret the data because it is not the proper data type. The service responds with an error message.
  
   Rule: The conversion between data types is given by the following table
  
  | parameter type | java type      |
  |----------------|----------------|
  | string         | String         |
  | number         | Integer        |
  | number         | Long           |
  | number         | BigInteger     |
  | number         | Float          |
  | number         | Double         |
  | number         | BigDecimal     |
  | boolean        | Boolean        |
  | ISO-8601       | OffsetDateTime |
  | ISO-8601       | LocalDateTime  |
  | ISO-8601       | LocalDate      |
  | ISO-8601       | LocalTime      |
  | ISO-8601       | Year           |
  | string         | ENUM           |

  Background: 
    Given the default timezone 'America/Puerto_Rico'

  Scenario Outline: The <method> call is made to the /api/v1/request-body/dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in a dto attribute, both having the same name
    and the service cannot map the sent value to the declared data type, responding with an error message.

    Given the endpoint '/api/v1/request-body/dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | request body                  | message                                    |
      | 'POST'   | '{"paramInteger":"X"}'        | 'paramInteger: is not a valid type'        |
      | 'PUT'    | '{"paramInteger":"X"}'        | 'paramInteger: is not a valid type'        |
      | 'PATCH'  | '{"paramInteger":"X"}'        | 'paramInteger: is not a valid type'        |
      | 'DELETE' | '{"paramInteger":"X"}'        | 'paramInteger: is not a valid type'        |
      | 'POST'   | '{"paramLong":"X"}'           | 'paramLong: is not a valid type'           |
      | 'PUT'    | '{"paramLong":"X"}'           | 'paramLong: is not a valid type'           |
      | 'PATCH'  | '{"paramLong":"X"}'           | 'paramLong: is not a valid type'           |
      | 'DELETE' | '{"paramLong":"X"}'           | 'paramLong: is not a valid type'           |
      | 'POST'   | '{"paramBigInteger":"X"}'     | 'paramBigInteger: is not a valid type'     |
      | 'PUT'    | '{"paramBigInteger":"X"}'     | 'paramBigInteger: is not a valid type'     |
      | 'PATCH'  | '{"paramBigInteger":"X"}'     | 'paramBigInteger: is not a valid type'     |
      | 'DELETE' | '{"paramBigInteger":"X"}'     | 'paramBigInteger: is not a valid type'     |
      | 'POST'   | '{"paramFloat":"X"}'          | 'paramFloat: is not a valid type'          |
      | 'PUT'    | '{"paramFloat":"X"}'          | 'paramFloat: is not a valid type'          |
      | 'PATCH'  | '{"paramFloat":"X"}'          | 'paramFloat: is not a valid type'          |
      | 'DELETE' | '{"paramFloat":"X"}'          | 'paramFloat: is not a valid type'          |
      | 'POST'   | '{"paramDouble":"X"}'         | 'paramDouble: is not a valid type'         |
      | 'PUT'    | '{"paramDouble":"X"}'         | 'paramDouble: is not a valid type'         |
      | 'PATCH'  | '{"paramDouble":"X"}'         | 'paramDouble: is not a valid type'         |
      | 'DELETE' | '{"paramDouble":"X"}'         | 'paramDouble: is not a valid type'         |
      | 'POST'   | '{"paramBigDecimal":"X"}'     | 'paramBigDecimal: is not a valid type'     |
      | 'PUT'    | '{"paramBigDecimal":"X"}'     | 'paramBigDecimal: is not a valid type'     |
      | 'PATCH'  | '{"paramBigDecimal":"X"}'     | 'paramBigDecimal: is not a valid type'     |
      | 'DELETE' | '{"paramBigDecimal":"X"}'     | 'paramBigDecimal: is not a valid type'     |
      | 'POST'   | '{"paramBoolean":"X"}'        | 'paramBoolean: is not a valid type'        |
      | 'PUT'    | '{"paramBoolean":"X"}'        | 'paramBoolean: is not a valid type'        |
      | 'PATCH'  | '{"paramBoolean":"X"}'        | 'paramBoolean: is not a valid type'        |
      | 'DELETE' | '{"paramBoolean":"X"}'        | 'paramBoolean: is not a valid type'        |
      | 'POST'   | '{"paramOffsetDateTime":"X"}' | 'paramOffsetDateTime: is not a valid type' |
      | 'PUT'    | '{"paramOffsetDateTime":"X"}' | 'paramOffsetDateTime: is not a valid type' |
      | 'PATCH'  | '{"paramOffsetDateTime":"X"}' | 'paramOffsetDateTime: is not a valid type' |
      | 'DELETE' | '{"paramOffsetDateTime":"X"}' | 'paramOffsetDateTime: is not a valid type' |
      | 'POST'   | '{"paramLocalDateTime":"X"}'  | 'paramLocalDateTime: is not a valid type'  |
      | 'PUT'    | '{"paramLocalDateTime":"X"}'  | 'paramLocalDateTime: is not a valid type'  |
      | 'PATCH'  | '{"paramLocalDateTime":"X"}'  | 'paramLocalDateTime: is not a valid type'  |
      | 'DELETE' | '{"paramLocalDateTime":"X"}'  | 'paramLocalDateTime: is not a valid type'  |
      | 'POST'   | '{"paramLocalDate":"X"}'      | 'paramLocalDate: is not a valid type'      |
      | 'PUT'    | '{"paramLocalDate":"X"}'      | 'paramLocalDate: is not a valid type'      |
      | 'PATCH'  | '{"paramLocalDate":"X"}'      | 'paramLocalDate: is not a valid type'      |
      | 'DELETE' | '{"paramLocalDate":"X"}'      | 'paramLocalDate: is not a valid type'      |
      | 'POST'   | '{"paramLocalTime":"X"}'      | 'paramLocalTime: is not a valid type'      |
      | 'PUT'    | '{"paramLocalTime":"X"}'      | 'paramLocalTime: is not a valid type'      |
      | 'PATCH'  | '{"paramLocalTime":"X"}'      | 'paramLocalTime: is not a valid type'      |
      | 'DELETE' | '{"paramLocalTime":"X"}'      | 'paramLocalTime: is not a valid type'      |
      | 'POST'   | '{"paramYear":"X"}'           | 'paramYear: is not a valid type'           |
      | 'PUT'    | '{"paramYear":"X"}'           | 'paramYear: is not a valid type'           |
      | 'PATCH'  | '{"paramYear":"X"}'           | 'paramYear: is not a valid type'           |
      | 'DELETE' | '{"paramYear":"X"}'           | 'paramYear: is not a valid type'           |
      | 'POST'   | '{"paramEnumAB":"X"}'         | 'paramEnumAB: is not a valid type'         |
      | 'PUT'    | '{"paramEnumAB":"X"}'         | 'paramEnumAB: is not a valid type'         |
      | 'PATCH'  | '{"paramEnumAB":"X"}'         | 'paramEnumAB: is not a valid type'         |
      | 'DELETE' | '{"paramEnumAB":"X"}'         | 'paramEnumAB: is not a valid type'         |
      | 'POST'   | '{"paramEnumLowerAB":"X"}'    | 'paramEnumLowerAB: is not a valid type'    |
      | 'PUT'    | '{"paramEnumLowerAB":"X"}'    | 'paramEnumLowerAB: is not a valid type'    |
      | 'PATCH'  | '{"paramEnumLowerAB":"X"}'    | 'paramEnumLowerAB: is not a valid type'    |
      | 'DELETE' | '{"paramEnumLowerAB":"X"}'    | 'paramEnumLowerAB: is not a valid type'    |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/named-dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in a dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint 
    and the service cannot map the sent value to the declared data type, responding with an error message.

    Given the endpoint '/api/v1/request-body/named-dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | request body                     | message                                       |
      | 'POST'   | '{"param-integer":"X"}'          | 'param-integer: is not a valid type'          |
      | 'PUT'    | '{"param-integer":"X"}'          | 'param-integer: is not a valid type'          |
      | 'PATCH'  | '{"param-integer":"X"}'          | 'param-integer: is not a valid type'          |
      | 'DELETE' | '{"param-integer":"X"}'          | 'param-integer: is not a valid type'          |
      | 'POST'   | '{"param-long":"X"}'             | 'param-long: is not a valid type'             |
      | 'PUT'    | '{"param-long":"X"}'             | 'param-long: is not a valid type'             |
      | 'PATCH'  | '{"param-long":"X"}'             | 'param-long: is not a valid type'             |
      | 'DELETE' | '{"param-long":"X"}'             | 'param-long: is not a valid type'             |
      | 'POST'   | '{"param-big-integer":"X"}'      | 'param-big-integer: is not a valid type'      |
      | 'PUT'    | '{"param-big-integer":"X"}'      | 'param-big-integer: is not a valid type'      |
      | 'PATCH'  | '{"param-big-integer":"X"}'      | 'param-big-integer: is not a valid type'      |
      | 'DELETE' | '{"param-big-integer":"X"}'      | 'param-big-integer: is not a valid type'      |
      | 'POST'   | '{"param-float":"X"}'            | 'param-float: is not a valid type'            |
      | 'PUT'    | '{"param-float":"X"}'            | 'param-float: is not a valid type'            |
      | 'PATCH'  | '{"param-float":"X"}'            | 'param-float: is not a valid type'            |
      | 'DELETE' | '{"param-float":"X"}'            | 'param-float: is not a valid type'            |
      | 'POST'   | '{"param-double":"X"}'           | 'param-double: is not a valid type'           |
      | 'PUT'    | '{"param-double":"X"}'           | 'param-double: is not a valid type'           |
      | 'PATCH'  | '{"param-double":"X"}'           | 'param-double: is not a valid type'           |
      | 'DELETE' | '{"param-double":"X"}'           | 'param-double: is not a valid type'           |
      | 'POST'   | '{"param-big-decimal":"X"}'      | 'param-big-decimal: is not a valid type'      |
      | 'PUT'    | '{"param-big-decimal":"X"}'      | 'param-big-decimal: is not a valid type'      |
      | 'PATCH'  | '{"param-big-decimal":"X"}'      | 'param-big-decimal: is not a valid type'      |
      | 'DELETE' | '{"param-big-decimal":"X"}'      | 'param-big-decimal: is not a valid type'      |
      | 'POST'   | '{"param-boolean":"X"}'          | 'param-boolean: is not a valid type'          |
      | 'PUT'    | '{"param-boolean":"X"}'          | 'param-boolean: is not a valid type'          |
      | 'PATCH'  | '{"param-boolean":"X"}'          | 'param-boolean: is not a valid type'          |
      | 'DELETE' | '{"param-boolean":"X"}'          | 'param-boolean: is not a valid type'          |
      | 'POST'   | '{"param-offset-date-time":"X"}' | 'param-offset-date-time: is not a valid type' |
      | 'PUT'    | '{"param-offset-date-time":"X"}' | 'param-offset-date-time: is not a valid type' |
      | 'PATCH'  | '{"param-offset-date-time":"X"}' | 'param-offset-date-time: is not a valid type' |
      | 'DELETE' | '{"param-offset-date-time":"X"}' | 'param-offset-date-time: is not a valid type' |
      | 'POST'   | '{"param-local-date-time":"X"}'  | 'param-local-date-time: is not a valid type'  |
      | 'PUT'    | '{"param-local-date-time":"X"}'  | 'param-local-date-time: is not a valid type'  |
      | 'PATCH'  | '{"param-local-date-time":"X"}'  | 'param-local-date-time: is not a valid type'  |
      | 'DELETE' | '{"param-local-date-time":"X"}'  | 'param-local-date-time: is not a valid type'  |
      | 'POST'   | '{"param-local-date":"X"}'       | 'param-local-date: is not a valid type'       |
      | 'PUT'    | '{"param-local-date":"X"}'       | 'param-local-date: is not a valid type'       |
      | 'PATCH'  | '{"param-local-date":"X"}'       | 'param-local-date: is not a valid type'       |
      | 'DELETE' | '{"param-local-date":"X"}'       | 'param-local-date: is not a valid type'       |
      | 'POST'   | '{"param-local-time":"X"}'       | 'param-local-time: is not a valid type'       |
      | 'PUT'    | '{"param-local-time":"X"}'       | 'param-local-time: is not a valid type'       |
      | 'PATCH'  | '{"param-local-time":"X"}'       | 'param-local-time: is not a valid type'       |
      | 'DELETE' | '{"param-local-time":"X"}'       | 'param-local-time: is not a valid type'       |
      | 'POST'   | '{"param-year":"X"}'             | 'param-year: is not a valid type'             |
      | 'PUT'    | '{"param-year":"X"}'             | 'param-year: is not a valid type'             |
      | 'PATCH'  | '{"param-year":"X"}'             | 'param-year: is not a valid type'             |
      | 'DELETE' | '{"param-year":"X"}'             | 'param-year: is not a valid type'             |
      | 'POST'   | '{"param-enum-ab":"X"}'          | 'param-enum-ab: is not a valid type'          |
      | 'PUT'    | '{"param-enum-ab":"X"}'          | 'param-enum-ab: is not a valid type'          |
      | 'PATCH'  | '{"param-enum-ab":"X"}'          | 'param-enum-ab: is not a valid type'          |
      | 'DELETE' | '{"param-enum-ab":"X"}'          | 'param-enum-ab: is not a valid type'          |
      | 'POST'   | '{"param-enum-lower-ab":"X"}'    | 'param-enum-lower-ab: is not a valid type'    |
      | 'PUT'    | '{"param-enum-lower-ab":"X"}'    | 'param-enum-lower-ab: is not a valid type'    |
      | 'PATCH'  | '{"param-enum-lower-ab":"X"}'    | 'param-enum-lower-ab: is not a valid type'    |
      | 'DELETE' | '{"param-enum-lower-ab":"X"}'    | 'param-enum-lower-ab: is not a valid type'    |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/optional-dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in an optional dto attribute, both having the same name
    and the service cannot map the sent value to the declared data type, responding with an error message.

    Given the endpoint '/api/v1/request-body/optional-dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | request body                  | message                                    |
      | 'POST'   | '{"paramInteger":"X"}'        | 'paramInteger: is not a valid type'        |
      | 'PUT'    | '{"paramInteger":"X"}'        | 'paramInteger: is not a valid type'        |
      | 'PATCH'  | '{"paramInteger":"X"}'        | 'paramInteger: is not a valid type'        |
      | 'DELETE' | '{"paramInteger":"X"}'        | 'paramInteger: is not a valid type'        |
      | 'POST'   | '{"paramLong":"X"}'           | 'paramLong: is not a valid type'           |
      | 'PUT'    | '{"paramLong":"X"}'           | 'paramLong: is not a valid type'           |
      | 'PATCH'  | '{"paramLong":"X"}'           | 'paramLong: is not a valid type'           |
      | 'DELETE' | '{"paramLong":"X"}'           | 'paramLong: is not a valid type'           |
      | 'POST'   | '{"paramBigInteger":"X"}'     | 'paramBigInteger: is not a valid type'     |
      | 'PUT'    | '{"paramBigInteger":"X"}'     | 'paramBigInteger: is not a valid type'     |
      | 'PATCH'  | '{"paramBigInteger":"X"}'     | 'paramBigInteger: is not a valid type'     |
      | 'DELETE' | '{"paramBigInteger":"X"}'     | 'paramBigInteger: is not a valid type'     |
      | 'POST'   | '{"paramFloat":"X"}'          | 'paramFloat: is not a valid type'          |
      | 'PUT'    | '{"paramFloat":"X"}'          | 'paramFloat: is not a valid type'          |
      | 'PATCH'  | '{"paramFloat":"X"}'          | 'paramFloat: is not a valid type'          |
      | 'DELETE' | '{"paramFloat":"X"}'          | 'paramFloat: is not a valid type'          |
      | 'POST'   | '{"paramDouble":"X"}'         | 'paramDouble: is not a valid type'         |
      | 'PUT'    | '{"paramDouble":"X"}'         | 'paramDouble: is not a valid type'         |
      | 'PATCH'  | '{"paramDouble":"X"}'         | 'paramDouble: is not a valid type'         |
      | 'DELETE' | '{"paramDouble":"X"}'         | 'paramDouble: is not a valid type'         |
      | 'POST'   | '{"paramBigDecimal":"X"}'     | 'paramBigDecimal: is not a valid type'     |
      | 'PUT'    | '{"paramBigDecimal":"X"}'     | 'paramBigDecimal: is not a valid type'     |
      | 'PATCH'  | '{"paramBigDecimal":"X"}'     | 'paramBigDecimal: is not a valid type'     |
      | 'DELETE' | '{"paramBigDecimal":"X"}'     | 'paramBigDecimal: is not a valid type'     |
      | 'POST'   | '{"paramBoolean":"X"}'        | 'paramBoolean: is not a valid type'        |
      | 'PUT'    | '{"paramBoolean":"X"}'        | 'paramBoolean: is not a valid type'        |
      | 'PATCH'  | '{"paramBoolean":"X"}'        | 'paramBoolean: is not a valid type'        |
      | 'DELETE' | '{"paramBoolean":"X"}'        | 'paramBoolean: is not a valid type'        |
      | 'POST'   | '{"paramOffsetDateTime":"X"}' | 'paramOffsetDateTime: is not a valid type' |
      | 'PUT'    | '{"paramOffsetDateTime":"X"}' | 'paramOffsetDateTime: is not a valid type' |
      | 'PATCH'  | '{"paramOffsetDateTime":"X"}' | 'paramOffsetDateTime: is not a valid type' |
      | 'DELETE' | '{"paramOffsetDateTime":"X"}' | 'paramOffsetDateTime: is not a valid type' |
      | 'POST'   | '{"paramLocalDateTime":"X"}'  | 'paramLocalDateTime: is not a valid type'  |
      | 'PUT'    | '{"paramLocalDateTime":"X"}'  | 'paramLocalDateTime: is not a valid type'  |
      | 'PATCH'  | '{"paramLocalDateTime":"X"}'  | 'paramLocalDateTime: is not a valid type'  |
      | 'DELETE' | '{"paramLocalDateTime":"X"}'  | 'paramLocalDateTime: is not a valid type'  |
      | 'POST'   | '{"paramLocalDate":"X"}'      | 'paramLocalDate: is not a valid type'      |
      | 'PUT'    | '{"paramLocalDate":"X"}'      | 'paramLocalDate: is not a valid type'      |
      | 'PATCH'  | '{"paramLocalDate":"X"}'      | 'paramLocalDate: is not a valid type'      |
      | 'DELETE' | '{"paramLocalDate":"X"}'      | 'paramLocalDate: is not a valid type'      |
      | 'POST'   | '{"paramLocalTime":"X"}'      | 'paramLocalTime: is not a valid type'      |
      | 'PUT'    | '{"paramLocalTime":"X"}'      | 'paramLocalTime: is not a valid type'      |
      | 'PATCH'  | '{"paramLocalTime":"X"}'      | 'paramLocalTime: is not a valid type'      |
      | 'DELETE' | '{"paramLocalTime":"X"}'      | 'paramLocalTime: is not a valid type'      |
      | 'POST'   | '{"paramYear":"X"}'           | 'paramYear: is not a valid type'           |
      | 'PUT'    | '{"paramYear":"X"}'           | 'paramYear: is not a valid type'           |
      | 'PATCH'  | '{"paramYear":"X"}'           | 'paramYear: is not a valid type'           |
      | 'DELETE' | '{"paramYear":"X"}'           | 'paramYear: is not a valid type'           |
      | 'POST'   | '{"paramEnumAB":"X"}'         | 'paramEnumAB: is not a valid type'         |
      | 'PUT'    | '{"paramEnumAB":"X"}'         | 'paramEnumAB: is not a valid type'         |
      | 'PATCH'  | '{"paramEnumAB":"X"}'         | 'paramEnumAB: is not a valid type'         |
      | 'DELETE' | '{"paramEnumAB":"X"}'         | 'paramEnumAB: is not a valid type'         |
      | 'POST'   | '{"paramEnumLowerAB":"X"}'    | 'paramEnumLowerAB: is not a valid type'    |
      | 'PUT'    | '{"paramEnumLowerAB":"X"}'    | 'paramEnumLowerAB: is not a valid type'    |
      | 'PATCH'  | '{"paramEnumLowerAB":"X"}'    | 'paramEnumLowerAB: is not a valid type'    |
      | 'DELETE' | '{"paramEnumLowerAB":"X"}'    | 'paramEnumLowerAB: is not a valid type'    |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/named-optional-dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in an optional dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
    and the service cannot map the sent value to the declared data type, responding with an error message.

    Given the endpoint '/api/v1/request-body/named-optional-dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | request body                     | message                                       |
      | 'POST'   | '{"param-integer":"X"}'          | 'param-integer: is not a valid type'          |
      | 'PUT'    | '{"param-integer":"X"}'          | 'param-integer: is not a valid type'          |
      | 'PATCH'  | '{"param-integer":"X"}'          | 'param-integer: is not a valid type'          |
      | 'DELETE' | '{"param-integer":"X"}'          | 'param-integer: is not a valid type'          |
      | 'POST'   | '{"param-long":"X"}'             | 'param-long: is not a valid type'             |
      | 'PUT'    | '{"param-long":"X"}'             | 'param-long: is not a valid type'             |
      | 'PATCH'  | '{"param-long":"X"}'             | 'param-long: is not a valid type'             |
      | 'DELETE' | '{"param-long":"X"}'             | 'param-long: is not a valid type'             |
      | 'POST'   | '{"param-big-integer":"X"}'      | 'param-big-integer: is not a valid type'      |
      | 'PUT'    | '{"param-big-integer":"X"}'      | 'param-big-integer: is not a valid type'      |
      | 'PATCH'  | '{"param-big-integer":"X"}'      | 'param-big-integer: is not a valid type'      |
      | 'DELETE' | '{"param-big-integer":"X"}'      | 'param-big-integer: is not a valid type'      |
      | 'POST'   | '{"param-float":"X"}'            | 'param-float: is not a valid type'            |
      | 'PUT'    | '{"param-float":"X"}'            | 'param-float: is not a valid type'            |
      | 'PATCH'  | '{"param-float":"X"}'            | 'param-float: is not a valid type'            |
      | 'DELETE' | '{"param-float":"X"}'            | 'param-float: is not a valid type'            |
      | 'POST'   | '{"param-double":"X"}'           | 'param-double: is not a valid type'           |
      | 'PUT'    | '{"param-double":"X"}'           | 'param-double: is not a valid type'           |
      | 'PATCH'  | '{"param-double":"X"}'           | 'param-double: is not a valid type'           |
      | 'DELETE' | '{"param-double":"X"}'           | 'param-double: is not a valid type'           |
      | 'POST'   | '{"param-big-decimal":"X"}'      | 'param-big-decimal: is not a valid type'      |
      | 'PUT'    | '{"param-big-decimal":"X"}'      | 'param-big-decimal: is not a valid type'      |
      | 'PATCH'  | '{"param-big-decimal":"X"}'      | 'param-big-decimal: is not a valid type'      |
      | 'DELETE' | '{"param-big-decimal":"X"}'      | 'param-big-decimal: is not a valid type'      |
      | 'POST'   | '{"param-boolean":"X"}'          | 'param-boolean: is not a valid type'          |
      | 'PUT'    | '{"param-boolean":"X"}'          | 'param-boolean: is not a valid type'          |
      | 'PATCH'  | '{"param-boolean":"X"}'          | 'param-boolean: is not a valid type'          |
      | 'DELETE' | '{"param-boolean":"X"}'          | 'param-boolean: is not a valid type'          |
      | 'POST'   | '{"param-offset-date-time":"X"}' | 'param-offset-date-time: is not a valid type' |
      | 'PUT'    | '{"param-offset-date-time":"X"}' | 'param-offset-date-time: is not a valid type' |
      | 'PATCH'  | '{"param-offset-date-time":"X"}' | 'param-offset-date-time: is not a valid type' |
      | 'DELETE' | '{"param-offset-date-time":"X"}' | 'param-offset-date-time: is not a valid type' |
      | 'POST'   | '{"param-local-date-time":"X"}'  | 'param-local-date-time: is not a valid type'  |
      | 'PUT'    | '{"param-local-date-time":"X"}'  | 'param-local-date-time: is not a valid type'  |
      | 'PATCH'  | '{"param-local-date-time":"X"}'  | 'param-local-date-time: is not a valid type'  |
      | 'DELETE' | '{"param-local-date-time":"X"}'  | 'param-local-date-time: is not a valid type'  |
      | 'POST'   | '{"param-local-date":"X"}'       | 'param-local-date: is not a valid type'       |
      | 'PUT'    | '{"param-local-date":"X"}'       | 'param-local-date: is not a valid type'       |
      | 'PATCH'  | '{"param-local-date":"X"}'       | 'param-local-date: is not a valid type'       |
      | 'DELETE' | '{"param-local-date":"X"}'       | 'param-local-date: is not a valid type'       |
      | 'POST'   | '{"param-local-time":"X"}'       | 'param-local-time: is not a valid type'       |
      | 'PUT'    | '{"param-local-time":"X"}'       | 'param-local-time: is not a valid type'       |
      | 'PATCH'  | '{"param-local-time":"X"}'       | 'param-local-time: is not a valid type'       |
      | 'DELETE' | '{"param-local-time":"X"}'       | 'param-local-time: is not a valid type'       |
      | 'POST'   | '{"param-year":"X"}'             | 'param-year: is not a valid type'             |
      | 'PUT'    | '{"param-year":"X"}'             | 'param-year: is not a valid type'             |
      | 'PATCH'  | '{"param-year":"X"}'             | 'param-year: is not a valid type'             |
      | 'DELETE' | '{"param-year":"X"}'             | 'param-year: is not a valid type'             |
      | 'POST'   | '{"param-enum-ab":"X"}'          | 'param-enum-ab: is not a valid type'          |
      | 'PUT'    | '{"param-enum-ab":"X"}'          | 'param-enum-ab: is not a valid type'          |
      | 'PATCH'  | '{"param-enum-ab":"X"}'          | 'param-enum-ab: is not a valid type'          |
      | 'DELETE' | '{"param-enum-ab":"X"}'          | 'param-enum-ab: is not a valid type'          |
      | 'POST'   | '{"param-enum-lower-ab":"X"}'    | 'param-enum-lower-ab: is not a valid type'    |
      | 'PUT'    | '{"param-enum-lower-ab":"X"}'    | 'param-enum-lower-ab: is not a valid type'    |
      | 'PATCH'  | '{"param-enum-lower-ab":"X"}'    | 'param-enum-lower-ab: is not a valid type'    |
      | 'DELETE' | '{"param-enum-lower-ab":"X"}'    | 'param-enum-lower-ab: is not a valid type'    |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/nested-dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in a nested dto attribute, both having the same name
    and the service cannot map the sent value to the declared data type, responding with an error message.

    Given the endpoint '/api/v1/request-body/nested-dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | request body                             | message                                           |
      | 'POST'   | '{"nested":{"paramInteger":"X"}}'        | 'nested.paramInteger: is not a valid type'        |
      | 'PUT'    | '{"nested":{"paramInteger":"X"}}'        | 'nested.paramInteger: is not a valid type'        |
      | 'PATCH'  | '{"nested":{"paramInteger":"X"}}'        | 'nested.paramInteger: is not a valid type'        |
      | 'DELETE' | '{"nested":{"paramInteger":"X"}}'        | 'nested.paramInteger: is not a valid type'        |
      | 'POST'   | '{"nested":{"paramLong":"X"}}'           | 'nested.paramLong: is not a valid type'           |
      | 'PUT'    | '{"nested":{"paramLong":"X"}}'           | 'nested.paramLong: is not a valid type'           |
      | 'PATCH'  | '{"nested":{"paramLong":"X"}}'           | 'nested.paramLong: is not a valid type'           |
      | 'DELETE' | '{"nested":{"paramLong":"X"}}'           | 'nested.paramLong: is not a valid type'           |
      | 'POST'   | '{"nested":{"paramBigInteger":"X"}}'     | 'nested.paramBigInteger: is not a valid type'     |
      | 'PUT'    | '{"nested":{"paramBigInteger":"X"}}'     | 'nested.paramBigInteger: is not a valid type'     |
      | 'PATCH'  | '{"nested":{"paramBigInteger":"X"}}'     | 'nested.paramBigInteger: is not a valid type'     |
      | 'DELETE' | '{"nested":{"paramBigInteger":"X"}}'     | 'nested.paramBigInteger: is not a valid type'     |
      | 'POST'   | '{"nested":{"paramFloat":"X"}}'          | 'nested.paramFloat: is not a valid type'          |
      | 'PUT'    | '{"nested":{"paramFloat":"X"}}'          | 'nested.paramFloat: is not a valid type'          |
      | 'PATCH'  | '{"nested":{"paramFloat":"X"}}'          | 'nested.paramFloat: is not a valid type'          |
      | 'DELETE' | '{"nested":{"paramFloat":"X"}}'          | 'nested.paramFloat: is not a valid type'          |
      | 'POST'   | '{"nested":{"paramDouble":"X"}}'         | 'nested.paramDouble: is not a valid type'         |
      | 'PUT'    | '{"nested":{"paramDouble":"X"}}'         | 'nested.paramDouble: is not a valid type'         |
      | 'PATCH'  | '{"nested":{"paramDouble":"X"}}'         | 'nested.paramDouble: is not a valid type'         |
      | 'DELETE' | '{"nested":{"paramDouble":"X"}}'         | 'nested.paramDouble: is not a valid type'         |
      | 'POST'   | '{"nested":{"paramBigDecimal":"X"}}'     | 'nested.paramBigDecimal: is not a valid type'     |
      | 'PUT'    | '{"nested":{"paramBigDecimal":"X"}}'     | 'nested.paramBigDecimal: is not a valid type'     |
      | 'PATCH'  | '{"nested":{"paramBigDecimal":"X"}}'     | 'nested.paramBigDecimal: is not a valid type'     |
      | 'DELETE' | '{"nested":{"paramBigDecimal":"X"}}'     | 'nested.paramBigDecimal: is not a valid type'     |
      | 'POST'   | '{"nested":{"paramBoolean":"X"}}'        | 'nested.paramBoolean: is not a valid type'        |
      | 'PUT'    | '{"nested":{"paramBoolean":"X"}}'        | 'nested.paramBoolean: is not a valid type'        |
      | 'PATCH'  | '{"nested":{"paramBoolean":"X"}}'        | 'nested.paramBoolean: is not a valid type'        |
      | 'DELETE' | '{"nested":{"paramBoolean":"X"}}'        | 'nested.paramBoolean: is not a valid type'        |
      | 'POST'   | '{"nested":{"paramOffsetDateTime":"X"}}' | 'nested.paramOffsetDateTime: is not a valid type' |
      | 'PUT'    | '{"nested":{"paramOffsetDateTime":"X"}}' | 'nested.paramOffsetDateTime: is not a valid type' |
      | 'PATCH'  | '{"nested":{"paramOffsetDateTime":"X"}}' | 'nested.paramOffsetDateTime: is not a valid type' |
      | 'DELETE' | '{"nested":{"paramOffsetDateTime":"X"}}' | 'nested.paramOffsetDateTime: is not a valid type' |
      | 'POST'   | '{"nested":{"paramLocalDateTime":"X"}}'  | 'nested.paramLocalDateTime: is not a valid type'  |
      | 'PUT'    | '{"nested":{"paramLocalDateTime":"X"}}'  | 'nested.paramLocalDateTime: is not a valid type'  |
      | 'PATCH'  | '{"nested":{"paramLocalDateTime":"X"}}'  | 'nested.paramLocalDateTime: is not a valid type'  |
      | 'DELETE' | '{"nested":{"paramLocalDateTime":"X"}}'  | 'nested.paramLocalDateTime: is not a valid type'  |
      | 'POST'   | '{"nested":{"paramLocalDate":"X"}}'      | 'nested.paramLocalDate: is not a valid type'      |
      | 'PUT'    | '{"nested":{"paramLocalDate":"X"}}'      | 'nested.paramLocalDate: is not a valid type'      |
      | 'PATCH'  | '{"nested":{"paramLocalDate":"X"}}'      | 'nested.paramLocalDate: is not a valid type'      |
      | 'DELETE' | '{"nested":{"paramLocalDate":"X"}}'      | 'nested.paramLocalDate: is not a valid type'      |
      | 'POST'   | '{"nested":{"paramLocalTime":"X"}}'      | 'nested.paramLocalTime: is not a valid type'      |
      | 'PUT'    | '{"nested":{"paramLocalTime":"X"}}'      | 'nested.paramLocalTime: is not a valid type'      |
      | 'PATCH'  | '{"nested":{"paramLocalTime":"X"}}'      | 'nested.paramLocalTime: is not a valid type'      |
      | 'DELETE' | '{"nested":{"paramLocalTime":"X"}}'      | 'nested.paramLocalTime: is not a valid type'      |
      | 'POST'   | '{"nested":{"paramYear":"X"}}'           | 'nested.paramYear: is not a valid type'           |
      | 'PUT'    | '{"nested":{"paramYear":"X"}}'           | 'nested.paramYear: is not a valid type'           |
      | 'PATCH'  | '{"nested":{"paramYear":"X"}}'           | 'nested.paramYear: is not a valid type'           |
      | 'DELETE' | '{"nested":{"paramYear":"X"}}'           | 'nested.paramYear: is not a valid type'           |
      | 'POST'   | '{"nested":{"paramEnumAB":"X"}}'         | 'nested.paramEnumAB: is not a valid type'         |
      | 'PUT'    | '{"nested":{"paramEnumAB":"X"}}'         | 'nested.paramEnumAB: is not a valid type'         |
      | 'PATCH'  | '{"nested":{"paramEnumAB":"X"}}'         | 'nested.paramEnumAB: is not a valid type'         |
      | 'DELETE' | '{"nested":{"paramEnumAB":"X"}}'         | 'nested.paramEnumAB: is not a valid type'         |
      | 'POST'   | '{"nested":{"paramEnumLowerAB":"X"}}'    | 'nested.paramEnumLowerAB: is not a valid type'    |
      | 'PUT'    | '{"nested":{"paramEnumLowerAB":"X"}}'    | 'nested.paramEnumLowerAB: is not a valid type'    |
      | 'PATCH'  | '{"nested":{"paramEnumLowerAB":"X"}}'    | 'nested.paramEnumLowerAB: is not a valid type'    |
      | 'DELETE' | '{"nested":{"paramEnumLowerAB":"X"}}'    | 'nested.paramEnumLowerAB: is not a valid type'    |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/named-nested-dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in a nested dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
    and the service cannot map the sent value to the declared data type, responding with an error message.

    Given the endpoint '/api/v1/request-body/named-nested-dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | request body                                | message                                              |
      | 'POST'   | '{"nested":{"param-integer":"X"}}'          | 'nested.param-integer: is not a valid type'          |
      | 'PUT'    | '{"nested":{"param-integer":"X"}}'          | 'nested.param-integer: is not a valid type'          |
      | 'PATCH'  | '{"nested":{"param-integer":"X"}}'          | 'nested.param-integer: is not a valid type'          |
      | 'DELETE' | '{"nested":{"param-integer":"X"}}'          | 'nested.param-integer: is not a valid type'          |
      | 'POST'   | '{"nested":{"param-long":"X"}}'             | 'nested.param-long: is not a valid type'             |
      | 'PUT'    | '{"nested":{"param-long":"X"}}'             | 'nested.param-long: is not a valid type'             |
      | 'PATCH'  | '{"nested":{"param-long":"X"}}'             | 'nested.param-long: is not a valid type'             |
      | 'DELETE' | '{"nested":{"param-long":"X"}}'             | 'nested.param-long: is not a valid type'             |
      | 'POST'   | '{"nested":{"param-big-integer":"X"}}'      | 'nested.param-big-integer: is not a valid type'      |
      | 'PUT'    | '{"nested":{"param-big-integer":"X"}}'      | 'nested.param-big-integer: is not a valid type'      |
      | 'PATCH'  | '{"nested":{"param-big-integer":"X"}}'      | 'nested.param-big-integer: is not a valid type'      |
      | 'DELETE' | '{"nested":{"param-big-integer":"X"}}'      | 'nested.param-big-integer: is not a valid type'      |
      | 'POST'   | '{"nested":{"param-float":"X"}}'            | 'nested.param-float: is not a valid type'            |
      | 'PUT'    | '{"nested":{"param-float":"X"}}'            | 'nested.param-float: is not a valid type'            |
      | 'PATCH'  | '{"nested":{"param-float":"X"}}'            | 'nested.param-float: is not a valid type'            |
      | 'DELETE' | '{"nested":{"param-float":"X"}}'            | 'nested.param-float: is not a valid type'            |
      | 'POST'   | '{"nested":{"param-double":"X"}}'           | 'nested.param-double: is not a valid type'           |
      | 'PUT'    | '{"nested":{"param-double":"X"}}'           | 'nested.param-double: is not a valid type'           |
      | 'PATCH'  | '{"nested":{"param-double":"X"}}'           | 'nested.param-double: is not a valid type'           |
      | 'DELETE' | '{"nested":{"param-double":"X"}}'           | 'nested.param-double: is not a valid type'           |
      | 'POST'   | '{"nested":{"param-big-decimal":"X"}}'      | 'nested.param-big-decimal: is not a valid type'      |
      | 'PUT'    | '{"nested":{"param-big-decimal":"X"}}'      | 'nested.param-big-decimal: is not a valid type'      |
      | 'PATCH'  | '{"nested":{"param-big-decimal":"X"}}'      | 'nested.param-big-decimal: is not a valid type'      |
      | 'DELETE' | '{"nested":{"param-big-decimal":"X"}}'      | 'nested.param-big-decimal: is not a valid type'      |
      | 'POST'   | '{"nested":{"param-boolean":"X"}}'          | 'nested.param-boolean: is not a valid type'          |
      | 'PUT'    | '{"nested":{"param-boolean":"X"}}'          | 'nested.param-boolean: is not a valid type'          |
      | 'PATCH'  | '{"nested":{"param-boolean":"X"}}'          | 'nested.param-boolean: is not a valid type'          |
      | 'DELETE' | '{"nested":{"param-boolean":"X"}}'          | 'nested.param-boolean: is not a valid type'          |
      | 'POST'   | '{"nested":{"param-offset-date-time":"X"}}' | 'nested.param-offset-date-time: is not a valid type' |
      | 'PUT'    | '{"nested":{"param-offset-date-time":"X"}}' | 'nested.param-offset-date-time: is not a valid type' |
      | 'PATCH'  | '{"nested":{"param-offset-date-time":"X"}}' | 'nested.param-offset-date-time: is not a valid type' |
      | 'DELETE' | '{"nested":{"param-offset-date-time":"X"}}' | 'nested.param-offset-date-time: is not a valid type' |
      | 'POST'   | '{"nested":{"param-local-date-time":"X"}}'  | 'nested.param-local-date-time: is not a valid type'  |
      | 'PUT'    | '{"nested":{"param-local-date-time":"X"}}'  | 'nested.param-local-date-time: is not a valid type'  |
      | 'PATCH'  | '{"nested":{"param-local-date-time":"X"}}'  | 'nested.param-local-date-time: is not a valid type'  |
      | 'DELETE' | '{"nested":{"param-local-date-time":"X"}}'  | 'nested.param-local-date-time: is not a valid type'  |
      | 'POST'   | '{"nested":{"param-local-date":"X"}}'       | 'nested.param-local-date: is not a valid type'       |
      | 'PUT'    | '{"nested":{"param-local-date":"X"}}'       | 'nested.param-local-date: is not a valid type'       |
      | 'PATCH'  | '{"nested":{"param-local-date":"X"}}'       | 'nested.param-local-date: is not a valid type'       |
      | 'DELETE' | '{"nested":{"param-local-date":"X"}}'       | 'nested.param-local-date: is not a valid type'       |
      | 'POST'   | '{"nested":{"param-local-time":"X"}}'       | 'nested.param-local-time: is not a valid type'       |
      | 'PUT'    | '{"nested":{"param-local-time":"X"}}'       | 'nested.param-local-time: is not a valid type'       |
      | 'PATCH'  | '{"nested":{"param-local-time":"X"}}'       | 'nested.param-local-time: is not a valid type'       |
      | 'DELETE' | '{"nested":{"param-local-time":"X"}}'       | 'nested.param-local-time: is not a valid type'       |
      | 'POST'   | '{"nested":{"param-year":"X"}}'             | 'nested.param-year: is not a valid type'             |
      | 'PUT'    | '{"nested":{"param-year":"X"}}'             | 'nested.param-year: is not a valid type'             |
      | 'PATCH'  | '{"nested":{"param-year":"X"}}'             | 'nested.param-year: is not a valid type'             |
      | 'DELETE' | '{"nested":{"param-year":"X"}}'             | 'nested.param-year: is not a valid type'             |
      | 'POST'   | '{"nested":{"param-enum-ab":"X"}}'          | 'nested.param-enum-ab: is not a valid type'          |
      | 'PUT'    | '{"nested":{"param-enum-ab":"X"}}'          | 'nested.param-enum-ab: is not a valid type'          |
      | 'PATCH'  | '{"nested":{"param-enum-ab":"X"}}'          | 'nested.param-enum-ab: is not a valid type'          |
      | 'DELETE' | '{"nested":{"param-enum-ab":"X"}}'          | 'nested.param-enum-ab: is not a valid type'          |
      | 'POST'   | '{"nested":{"param-enum-lower-ab":"X"}}'    | 'nested.param-enum-lower-ab: is not a valid type'    |
      | 'PUT'    | '{"nested":{"param-enum-lower-ab":"X"}}'    | 'nested.param-enum-lower-ab: is not a valid type'    |
      | 'PATCH'  | '{"nested":{"param-enum-lower-ab":"X"}}'    | 'nested.param-enum-lower-ab: is not a valid type'    |
      | 'DELETE' | '{"nested":{"param-enum-lower-ab":"X"}}'    | 'nested.param-enum-lower-ab: is not a valid type'    |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/nested-optional-dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in a nested optional dto attribute, both having the same name
    and the service cannot map the sent value to the declared data type, responding with an error message.

    Given the endpoint '/api/v1/request-body/nested-optional-dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | request body                             | message                                           |
      | 'POST'   | '{"nested":{"paramInteger":"X"}}'        | 'nested.paramInteger: is not a valid type'        |
      | 'PUT'    | '{"nested":{"paramInteger":"X"}}'        | 'nested.paramInteger: is not a valid type'        |
      | 'PATCH'  | '{"nested":{"paramInteger":"X"}}'        | 'nested.paramInteger: is not a valid type'        |
      | 'DELETE' | '{"nested":{"paramInteger":"X"}}'        | 'nested.paramInteger: is not a valid type'        |
      | 'POST'   | '{"nested":{"paramLong":"X"}}'           | 'nested.paramLong: is not a valid type'           |
      | 'PUT'    | '{"nested":{"paramLong":"X"}}'           | 'nested.paramLong: is not a valid type'           |
      | 'PATCH'  | '{"nested":{"paramLong":"X"}}'           | 'nested.paramLong: is not a valid type'           |
      | 'DELETE' | '{"nested":{"paramLong":"X"}}'           | 'nested.paramLong: is not a valid type'           |
      | 'POST'   | '{"nested":{"paramBigInteger":"X"}}'     | 'nested.paramBigInteger: is not a valid type'     |
      | 'PUT'    | '{"nested":{"paramBigInteger":"X"}}'     | 'nested.paramBigInteger: is not a valid type'     |
      | 'PATCH'  | '{"nested":{"paramBigInteger":"X"}}'     | 'nested.paramBigInteger: is not a valid type'     |
      | 'DELETE' | '{"nested":{"paramBigInteger":"X"}}'     | 'nested.paramBigInteger: is not a valid type'     |
      | 'POST'   | '{"nested":{"paramFloat":"X"}}'          | 'nested.paramFloat: is not a valid type'          |
      | 'PUT'    | '{"nested":{"paramFloat":"X"}}'          | 'nested.paramFloat: is not a valid type'          |
      | 'PATCH'  | '{"nested":{"paramFloat":"X"}}'          | 'nested.paramFloat: is not a valid type'          |
      | 'DELETE' | '{"nested":{"paramFloat":"X"}}'          | 'nested.paramFloat: is not a valid type'          |
      | 'POST'   | '{"nested":{"paramDouble":"X"}}'         | 'nested.paramDouble: is not a valid type'         |
      | 'PUT'    | '{"nested":{"paramDouble":"X"}}'         | 'nested.paramDouble: is not a valid type'         |
      | 'PATCH'  | '{"nested":{"paramDouble":"X"}}'         | 'nested.paramDouble: is not a valid type'         |
      | 'DELETE' | '{"nested":{"paramDouble":"X"}}'         | 'nested.paramDouble: is not a valid type'         |
      | 'POST'   | '{"nested":{"paramBigDecimal":"X"}}'     | 'nested.paramBigDecimal: is not a valid type'     |
      | 'PUT'    | '{"nested":{"paramBigDecimal":"X"}}'     | 'nested.paramBigDecimal: is not a valid type'     |
      | 'PATCH'  | '{"nested":{"paramBigDecimal":"X"}}'     | 'nested.paramBigDecimal: is not a valid type'     |
      | 'DELETE' | '{"nested":{"paramBigDecimal":"X"}}'     | 'nested.paramBigDecimal: is not a valid type'     |
      | 'POST'   | '{"nested":{"paramBoolean":"X"}}'        | 'nested.paramBoolean: is not a valid type'        |
      | 'PUT'    | '{"nested":{"paramBoolean":"X"}}'        | 'nested.paramBoolean: is not a valid type'        |
      | 'PATCH'  | '{"nested":{"paramBoolean":"X"}}'        | 'nested.paramBoolean: is not a valid type'        |
      | 'DELETE' | '{"nested":{"paramBoolean":"X"}}'        | 'nested.paramBoolean: is not a valid type'        |
      | 'POST'   | '{"nested":{"paramOffsetDateTime":"X"}}' | 'nested.paramOffsetDateTime: is not a valid type' |
      | 'PUT'    | '{"nested":{"paramOffsetDateTime":"X"}}' | 'nested.paramOffsetDateTime: is not a valid type' |
      | 'PATCH'  | '{"nested":{"paramOffsetDateTime":"X"}}' | 'nested.paramOffsetDateTime: is not a valid type' |
      | 'DELETE' | '{"nested":{"paramOffsetDateTime":"X"}}' | 'nested.paramOffsetDateTime: is not a valid type' |
      | 'POST'   | '{"nested":{"paramLocalDateTime":"X"}}'  | 'nested.paramLocalDateTime: is not a valid type'  |
      | 'PUT'    | '{"nested":{"paramLocalDateTime":"X"}}'  | 'nested.paramLocalDateTime: is not a valid type'  |
      | 'PATCH'  | '{"nested":{"paramLocalDateTime":"X"}}'  | 'nested.paramLocalDateTime: is not a valid type'  |
      | 'DELETE' | '{"nested":{"paramLocalDateTime":"X"}}'  | 'nested.paramLocalDateTime: is not a valid type'  |
      | 'POST'   | '{"nested":{"paramLocalDate":"X"}}'      | 'nested.paramLocalDate: is not a valid type'      |
      | 'PUT'    | '{"nested":{"paramLocalDate":"X"}}'      | 'nested.paramLocalDate: is not a valid type'      |
      | 'PATCH'  | '{"nested":{"paramLocalDate":"X"}}'      | 'nested.paramLocalDate: is not a valid type'      |
      | 'DELETE' | '{"nested":{"paramLocalDate":"X"}}'      | 'nested.paramLocalDate: is not a valid type'      |
      | 'POST'   | '{"nested":{"paramLocalTime":"X"}}'      | 'nested.paramLocalTime: is not a valid type'      |
      | 'PUT'    | '{"nested":{"paramLocalTime":"X"}}'      | 'nested.paramLocalTime: is not a valid type'      |
      | 'PATCH'  | '{"nested":{"paramLocalTime":"X"}}'      | 'nested.paramLocalTime: is not a valid type'      |
      | 'DELETE' | '{"nested":{"paramLocalTime":"X"}}'      | 'nested.paramLocalTime: is not a valid type'      |
      | 'POST'   | '{"nested":{"paramYear":"X"}}'           | 'nested.paramYear: is not a valid type'           |
      | 'PUT'    | '{"nested":{"paramYear":"X"}}'           | 'nested.paramYear: is not a valid type'           |
      | 'PATCH'  | '{"nested":{"paramYear":"X"}}'           | 'nested.paramYear: is not a valid type'           |
      | 'DELETE' | '{"nested":{"paramYear":"X"}}'           | 'nested.paramYear: is not a valid type'           |
      | 'POST'   | '{"nested":{"paramEnumAB":"X"}}'         | 'nested.paramEnumAB: is not a valid type'         |
      | 'PUT'    | '{"nested":{"paramEnumAB":"X"}}'         | 'nested.paramEnumAB: is not a valid type'         |
      | 'PATCH'  | '{"nested":{"paramEnumAB":"X"}}'         | 'nested.paramEnumAB: is not a valid type'         |
      | 'DELETE' | '{"nested":{"paramEnumAB":"X"}}'         | 'nested.paramEnumAB: is not a valid type'         |
      | 'POST'   | '{"nested":{"paramEnumLowerAB":"X"}}'    | 'nested.paramEnumLowerAB: is not a valid type'    |
      | 'PUT'    | '{"nested":{"paramEnumLowerAB":"X"}}'    | 'nested.paramEnumLowerAB: is not a valid type'    |
      | 'PATCH'  | '{"nested":{"paramEnumLowerAB":"X"}}'    | 'nested.paramEnumLowerAB: is not a valid type'    |
      | 'DELETE' | '{"nested":{"paramEnumLowerAB":"X"}}'    | 'nested.paramEnumLowerAB: is not a valid type'    |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/named-nested-optional-dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in a nested optional dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
    and the service cannot map the sent value to the declared data type, responding with an error message.

    Given the endpoint '/api/v1/request-body/named-nested-optional-dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | request body                                | message                                              |
      | 'POST'   | '{"nested":{"param-integer":"X"}}'          | 'nested.param-integer: is not a valid type'          |
      | 'PUT'    | '{"nested":{"param-integer":"X"}}'          | 'nested.param-integer: is not a valid type'          |
      | 'PATCH'  | '{"nested":{"param-integer":"X"}}'          | 'nested.param-integer: is not a valid type'          |
      | 'DELETE' | '{"nested":{"param-integer":"X"}}'          | 'nested.param-integer: is not a valid type'          |
      | 'POST'   | '{"nested":{"param-long":"X"}}'             | 'nested.param-long: is not a valid type'             |
      | 'PUT'    | '{"nested":{"param-long":"X"}}'             | 'nested.param-long: is not a valid type'             |
      | 'PATCH'  | '{"nested":{"param-long":"X"}}'             | 'nested.param-long: is not a valid type'             |
      | 'DELETE' | '{"nested":{"param-long":"X"}}'             | 'nested.param-long: is not a valid type'             |
      | 'POST'   | '{"nested":{"param-big-integer":"X"}}'      | 'nested.param-big-integer: is not a valid type'      |
      | 'PUT'    | '{"nested":{"param-big-integer":"X"}}'      | 'nested.param-big-integer: is not a valid type'      |
      | 'PATCH'  | '{"nested":{"param-big-integer":"X"}}'      | 'nested.param-big-integer: is not a valid type'      |
      | 'DELETE' | '{"nested":{"param-big-integer":"X"}}'      | 'nested.param-big-integer: is not a valid type'      |
      | 'POST'   | '{"nested":{"param-float":"X"}}'            | 'nested.param-float: is not a valid type'            |
      | 'PUT'    | '{"nested":{"param-float":"X"}}'            | 'nested.param-float: is not a valid type'            |
      | 'PATCH'  | '{"nested":{"param-float":"X"}}'            | 'nested.param-float: is not a valid type'            |
      | 'DELETE' | '{"nested":{"param-float":"X"}}'            | 'nested.param-float: is not a valid type'            |
      | 'POST'   | '{"nested":{"param-double":"X"}}'           | 'nested.param-double: is not a valid type'           |
      | 'PUT'    | '{"nested":{"param-double":"X"}}'           | 'nested.param-double: is not a valid type'           |
      | 'PATCH'  | '{"nested":{"param-double":"X"}}'           | 'nested.param-double: is not a valid type'           |
      | 'DELETE' | '{"nested":{"param-double":"X"}}'           | 'nested.param-double: is not a valid type'           |
      | 'POST'   | '{"nested":{"param-big-decimal":"X"}}'      | 'nested.param-big-decimal: is not a valid type'      |
      | 'PUT'    | '{"nested":{"param-big-decimal":"X"}}'      | 'nested.param-big-decimal: is not a valid type'      |
      | 'PATCH'  | '{"nested":{"param-big-decimal":"X"}}'      | 'nested.param-big-decimal: is not a valid type'      |
      | 'DELETE' | '{"nested":{"param-big-decimal":"X"}}'      | 'nested.param-big-decimal: is not a valid type'      |
      | 'POST'   | '{"nested":{"param-boolean":"X"}}'          | 'nested.param-boolean: is not a valid type'          |
      | 'PUT'    | '{"nested":{"param-boolean":"X"}}'          | 'nested.param-boolean: is not a valid type'          |
      | 'PATCH'  | '{"nested":{"param-boolean":"X"}}'          | 'nested.param-boolean: is not a valid type'          |
      | 'DELETE' | '{"nested":{"param-boolean":"X"}}'          | 'nested.param-boolean: is not a valid type'          |
      | 'POST'   | '{"nested":{"param-offset-date-time":"X"}}' | 'nested.param-offset-date-time: is not a valid type' |
      | 'PUT'    | '{"nested":{"param-offset-date-time":"X"}}' | 'nested.param-offset-date-time: is not a valid type' |
      | 'PATCH'  | '{"nested":{"param-offset-date-time":"X"}}' | 'nested.param-offset-date-time: is not a valid type' |
      | 'DELETE' | '{"nested":{"param-offset-date-time":"X"}}' | 'nested.param-offset-date-time: is not a valid type' |
      | 'POST'   | '{"nested":{"param-local-date-time":"X"}}'  | 'nested.param-local-date-time: is not a valid type'  |
      | 'PUT'    | '{"nested":{"param-local-date-time":"X"}}'  | 'nested.param-local-date-time: is not a valid type'  |
      | 'PATCH'  | '{"nested":{"param-local-date-time":"X"}}'  | 'nested.param-local-date-time: is not a valid type'  |
      | 'DELETE' | '{"nested":{"param-local-date-time":"X"}}'  | 'nested.param-local-date-time: is not a valid type'  |
      | 'POST'   | '{"nested":{"param-local-date":"X"}}'       | 'nested.param-local-date: is not a valid type'       |
      | 'PUT'    | '{"nested":{"param-local-date":"X"}}'       | 'nested.param-local-date: is not a valid type'       |
      | 'PATCH'  | '{"nested":{"param-local-date":"X"}}'       | 'nested.param-local-date: is not a valid type'       |
      | 'DELETE' | '{"nested":{"param-local-date":"X"}}'       | 'nested.param-local-date: is not a valid type'       |
      | 'POST'   | '{"nested":{"param-local-time":"X"}}'       | 'nested.param-local-time: is not a valid type'       |
      | 'PUT'    | '{"nested":{"param-local-time":"X"}}'       | 'nested.param-local-time: is not a valid type'       |
      | 'PATCH'  | '{"nested":{"param-local-time":"X"}}'       | 'nested.param-local-time: is not a valid type'       |
      | 'DELETE' | '{"nested":{"param-local-time":"X"}}'       | 'nested.param-local-time: is not a valid type'       |
      | 'POST'   | '{"nested":{"param-year":"X"}}'             | 'nested.param-year: is not a valid type'             |
      | 'PUT'    | '{"nested":{"param-year":"X"}}'             | 'nested.param-year: is not a valid type'             |
      | 'PATCH'  | '{"nested":{"param-year":"X"}}'             | 'nested.param-year: is not a valid type'             |
      | 'DELETE' | '{"nested":{"param-year":"X"}}'             | 'nested.param-year: is not a valid type'             |
      | 'POST'   | '{"nested":{"param-enum-ab":"X"}}'          | 'nested.param-enum-ab: is not a valid type'          |
      | 'PUT'    | '{"nested":{"param-enum-ab":"X"}}'          | 'nested.param-enum-ab: is not a valid type'          |
      | 'PATCH'  | '{"nested":{"param-enum-ab":"X"}}'          | 'nested.param-enum-ab: is not a valid type'          |
      | 'DELETE' | '{"nested":{"param-enum-ab":"X"}}'          | 'nested.param-enum-ab: is not a valid type'          |
      | 'POST'   | '{"nested":{"param-enum-lower-ab":"X"}}'    | 'nested.param-enum-lower-ab: is not a valid type'    |
      | 'PUT'    | '{"nested":{"param-enum-lower-ab":"X"}}'    | 'nested.param-enum-lower-ab: is not a valid type'    |
      | 'PATCH'  | '{"nested":{"param-enum-lower-ab":"X"}}'    | 'nested.param-enum-lower-ab: is not a valid type'    |
      | 'DELETE' | '{"nested":{"param-enum-lower-ab":"X"}}'    | 'nested.param-enum-lower-ab: is not a valid type'    |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/nested-dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in a nested dto attribute from an array, both having the same name
    and the service cannot map the sent value to the declared data type, responding with an error message.

    Given the endpoint '/api/v1/request-body/nested-dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | request body                              | message                                             |
      | 'POST'   | '{"array":[{"paramInteger":"X"}]}'        | 'array[0].paramInteger: is not a valid type'        |
      | 'PUT'    | '{"array":[{"paramInteger":"X"}]}'        | 'array[0].paramInteger: is not a valid type'        |
      | 'PATCH'  | '{"array":[{"paramInteger":"X"}]}'        | 'array[0].paramInteger: is not a valid type'        |
      | 'DELETE' | '{"array":[{"paramInteger":"X"}]}'        | 'array[0].paramInteger: is not a valid type'        |
      | 'POST'   | '{"array":[{"paramLong":"X"}]}'           | 'array[0].paramLong: is not a valid type'           |
      | 'PUT'    | '{"array":[{"paramLong":"X"}]}'           | 'array[0].paramLong: is not a valid type'           |
      | 'PATCH'  | '{"array":[{"paramLong":"X"}]}'           | 'array[0].paramLong: is not a valid type'           |
      | 'DELETE' | '{"array":[{"paramLong":"X"}]}'           | 'array[0].paramLong: is not a valid type'           |
      | 'POST'   | '{"array":[{"paramBigInteger":"X"}]}'     | 'array[0].paramBigInteger: is not a valid type'     |
      | 'PUT'    | '{"array":[{"paramBigInteger":"X"}]}'     | 'array[0].paramBigInteger: is not a valid type'     |
      | 'PATCH'  | '{"array":[{"paramBigInteger":"X"}]}'     | 'array[0].paramBigInteger: is not a valid type'     |
      | 'DELETE' | '{"array":[{"paramBigInteger":"X"}]}'     | 'array[0].paramBigInteger: is not a valid type'     |
      | 'POST'   | '{"array":[{"paramFloat":"X"}]}'          | 'array[0].paramFloat: is not a valid type'          |
      | 'PUT'    | '{"array":[{"paramFloat":"X"}]}'          | 'array[0].paramFloat: is not a valid type'          |
      | 'PATCH'  | '{"array":[{"paramFloat":"X"}]}'          | 'array[0].paramFloat: is not a valid type'          |
      | 'DELETE' | '{"array":[{"paramFloat":"X"}]}'          | 'array[0].paramFloat: is not a valid type'          |
      | 'POST'   | '{"array":[{"paramDouble":"X"}]}'         | 'array[0].paramDouble: is not a valid type'         |
      | 'PUT'    | '{"array":[{"paramDouble":"X"}]}'         | 'array[0].paramDouble: is not a valid type'         |
      | 'PATCH'  | '{"array":[{"paramDouble":"X"}]}'         | 'array[0].paramDouble: is not a valid type'         |
      | 'DELETE' | '{"array":[{"paramDouble":"X"}]}'         | 'array[0].paramDouble: is not a valid type'         |
      | 'POST'   | '{"array":[{"paramBigDecimal":"X"}]}'     | 'array[0].paramBigDecimal: is not a valid type'     |
      | 'PUT'    | '{"array":[{"paramBigDecimal":"X"}]}'     | 'array[0].paramBigDecimal: is not a valid type'     |
      | 'PATCH'  | '{"array":[{"paramBigDecimal":"X"}]}'     | 'array[0].paramBigDecimal: is not a valid type'     |
      | 'DELETE' | '{"array":[{"paramBigDecimal":"X"}]}'     | 'array[0].paramBigDecimal: is not a valid type'     |
      | 'POST'   | '{"array":[{"paramBoolean":"X"}]}'        | 'array[0].paramBoolean: is not a valid type'        |
      | 'PUT'    | '{"array":[{"paramBoolean":"X"}]}'        | 'array[0].paramBoolean: is not a valid type'        |
      | 'PATCH'  | '{"array":[{"paramBoolean":"X"}]}'        | 'array[0].paramBoolean: is not a valid type'        |
      | 'DELETE' | '{"array":[{"paramBoolean":"X"}]}'        | 'array[0].paramBoolean: is not a valid type'        |
      | 'POST'   | '{"array":[{"paramOffsetDateTime":"X"}]}' | 'array[0].paramOffsetDateTime: is not a valid type' |
      | 'PUT'    | '{"array":[{"paramOffsetDateTime":"X"}]}' | 'array[0].paramOffsetDateTime: is not a valid type' |
      | 'PATCH'  | '{"array":[{"paramOffsetDateTime":"X"}]}' | 'array[0].paramOffsetDateTime: is not a valid type' |
      | 'DELETE' | '{"array":[{"paramOffsetDateTime":"X"}]}' | 'array[0].paramOffsetDateTime: is not a valid type' |
      | 'POST'   | '{"array":[{"paramLocalDateTime":"X"}]}'  | 'array[0].paramLocalDateTime: is not a valid type'  |
      | 'PUT'    | '{"array":[{"paramLocalDateTime":"X"}]}'  | 'array[0].paramLocalDateTime: is not a valid type'  |
      | 'PATCH'  | '{"array":[{"paramLocalDateTime":"X"}]}'  | 'array[0].paramLocalDateTime: is not a valid type'  |
      | 'DELETE' | '{"array":[{"paramLocalDateTime":"X"}]}'  | 'array[0].paramLocalDateTime: is not a valid type'  |
      | 'POST'   | '{"array":[{"paramLocalDate":"X"}]}'      | 'array[0].paramLocalDate: is not a valid type'      |
      | 'PUT'    | '{"array":[{"paramLocalDate":"X"}]}'      | 'array[0].paramLocalDate: is not a valid type'      |
      | 'PATCH'  | '{"array":[{"paramLocalDate":"X"}]}'      | 'array[0].paramLocalDate: is not a valid type'      |
      | 'DELETE' | '{"array":[{"paramLocalDate":"X"}]}'      | 'array[0].paramLocalDate: is not a valid type'      |
      | 'POST'   | '{"array":[{"paramLocalTime":"X"}]}'      | 'array[0].paramLocalTime: is not a valid type'      |
      | 'PUT'    | '{"array":[{"paramLocalTime":"X"}]}'      | 'array[0].paramLocalTime: is not a valid type'      |
      | 'PATCH'  | '{"array":[{"paramLocalTime":"X"}]}'      | 'array[0].paramLocalTime: is not a valid type'      |
      | 'DELETE' | '{"array":[{"paramLocalTime":"X"}]}'      | 'array[0].paramLocalTime: is not a valid type'      |
      | 'POST'   | '{"array":[{"paramYear":"X"}]}'           | 'array[0].paramYear: is not a valid type'           |
      | 'PUT'    | '{"array":[{"paramYear":"X"}]}'           | 'array[0].paramYear: is not a valid type'           |
      | 'PATCH'  | '{"array":[{"paramYear":"X"}]}'           | 'array[0].paramYear: is not a valid type'           |
      | 'DELETE' | '{"array":[{"paramYear":"X"}]}'           | 'array[0].paramYear: is not a valid type'           |
      | 'POST'   | '{"array":[{"paramEnumAB":"X"}]}'         | 'array[0].paramEnumAB: is not a valid type'         |
      | 'PUT'    | '{"array":[{"paramEnumAB":"X"}]}'         | 'array[0].paramEnumAB: is not a valid type'         |
      | 'PATCH'  | '{"array":[{"paramEnumAB":"X"}]}'         | 'array[0].paramEnumAB: is not a valid type'         |
      | 'DELETE' | '{"array":[{"paramEnumAB":"X"}]}'         | 'array[0].paramEnumAB: is not a valid type'         |
      | 'POST'   | '{"array":[{"paramEnumLowerAB":"X"}]}'    | 'array[0].paramEnumLowerAB: is not a valid type'    |
      | 'PUT'    | '{"array":[{"paramEnumLowerAB":"X"}]}'    | 'array[0].paramEnumLowerAB: is not a valid type'    |
      | 'PATCH'  | '{"array":[{"paramEnumLowerAB":"X"}]}'    | 'array[0].paramEnumLowerAB: is not a valid type'    |
      | 'DELETE' | '{"array":[{"paramEnumLowerAB":"X"}]}'    | 'array[0].paramEnumLowerAB: is not a valid type'    |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/named-nested-dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in a nested dto attribute from an array, both having the diferent names, a custom body-attibute name is defined by the endpoint
    and the service cannot map the sent value to the declared data type, responding with an error message.

    Given the endpoint '/api/v1/request-body/named-nested-dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | request body                                 | message                                                |
      | 'POST'   | '{"array":[{"param-integer":"X"}]}'          | 'array[0].param-integer: is not a valid type'          |
      | 'PUT'    | '{"array":[{"param-integer":"X"}]}'          | 'array[0].param-integer: is not a valid type'          |
      | 'PATCH'  | '{"array":[{"param-integer":"X"}]}'          | 'array[0].param-integer: is not a valid type'          |
      | 'DELETE' | '{"array":[{"param-integer":"X"}]}'          | 'array[0].param-integer: is not a valid type'          |
      | 'POST'   | '{"array":[{"param-long":"X"}]}'             | 'array[0].param-long: is not a valid type'             |
      | 'PUT'    | '{"array":[{"param-long":"X"}]}'             | 'array[0].param-long: is not a valid type'             |
      | 'PATCH'  | '{"array":[{"param-long":"X"}]}'             | 'array[0].param-long: is not a valid type'             |
      | 'DELETE' | '{"array":[{"param-long":"X"}]}'             | 'array[0].param-long: is not a valid type'             |
      | 'POST'   | '{"array":[{"param-big-integer":"X"}]}'      | 'array[0].param-big-integer: is not a valid type'      |
      | 'PUT'    | '{"array":[{"param-big-integer":"X"}]}'      | 'array[0].param-big-integer: is not a valid type'      |
      | 'PATCH'  | '{"array":[{"param-big-integer":"X"}]}'      | 'array[0].param-big-integer: is not a valid type'      |
      | 'DELETE' | '{"array":[{"param-big-integer":"X"}]}'      | 'array[0].param-big-integer: is not a valid type'      |
      | 'POST'   | '{"array":[{"param-float":"X"}]}'            | 'array[0].param-float: is not a valid type'            |
      | 'PUT'    | '{"array":[{"param-float":"X"}]}'            | 'array[0].param-float: is not a valid type'            |
      | 'PATCH'  | '{"array":[{"param-float":"X"}]}'            | 'array[0].param-float: is not a valid type'            |
      | 'DELETE' | '{"array":[{"param-float":"X"}]}'            | 'array[0].param-float: is not a valid type'            |
      | 'POST'   | '{"array":[{"param-double":"X"}]}'           | 'array[0].param-double: is not a valid type'           |
      | 'PUT'    | '{"array":[{"param-double":"X"}]}'           | 'array[0].param-double: is not a valid type'           |
      | 'PATCH'  | '{"array":[{"param-double":"X"}]}'           | 'array[0].param-double: is not a valid type'           |
      | 'DELETE' | '{"array":[{"param-double":"X"}]}'           | 'array[0].param-double: is not a valid type'           |
      | 'POST'   | '{"array":[{"param-big-decimal":"X"}]}'      | 'array[0].param-big-decimal: is not a valid type'      |
      | 'PUT'    | '{"array":[{"param-big-decimal":"X"}]}'      | 'array[0].param-big-decimal: is not a valid type'      |
      | 'PATCH'  | '{"array":[{"param-big-decimal":"X"}]}'      | 'array[0].param-big-decimal: is not a valid type'      |
      | 'DELETE' | '{"array":[{"param-big-decimal":"X"}]}'      | 'array[0].param-big-decimal: is not a valid type'      |
      | 'POST'   | '{"array":[{"param-boolean":"X"}]}'          | 'array[0].param-boolean: is not a valid type'          |
      | 'PUT'    | '{"array":[{"param-boolean":"X"}]}'          | 'array[0].param-boolean: is not a valid type'          |
      | 'PATCH'  | '{"array":[{"param-boolean":"X"}]}'          | 'array[0].param-boolean: is not a valid type'          |
      | 'DELETE' | '{"array":[{"param-boolean":"X"}]}'          | 'array[0].param-boolean: is not a valid type'          |
      | 'POST'   | '{"array":[{"param-offset-date-time":"X"}]}' | 'array[0].param-offset-date-time: is not a valid type' |
      | 'PUT'    | '{"array":[{"param-offset-date-time":"X"}]}' | 'array[0].param-offset-date-time: is not a valid type' |
      | 'PATCH'  | '{"array":[{"param-offset-date-time":"X"}]}' | 'array[0].param-offset-date-time: is not a valid type' |
      | 'DELETE' | '{"array":[{"param-offset-date-time":"X"}]}' | 'array[0].param-offset-date-time: is not a valid type' |
      | 'POST'   | '{"array":[{"param-local-date-time":"X"}]}'  | 'array[0].param-local-date-time: is not a valid type'  |
      | 'PUT'    | '{"array":[{"param-local-date-time":"X"}]}'  | 'array[0].param-local-date-time: is not a valid type'  |
      | 'PATCH'  | '{"array":[{"param-local-date-time":"X"}]}'  | 'array[0].param-local-date-time: is not a valid type'  |
      | 'DELETE' | '{"array":[{"param-local-date-time":"X"}]}'  | 'array[0].param-local-date-time: is not a valid type'  |
      | 'POST'   | '{"array":[{"param-local-date":"X"}]}'       | 'array[0].param-local-date: is not a valid type'       |
      | 'PUT'    | '{"array":[{"param-local-date":"X"}]}'       | 'array[0].param-local-date: is not a valid type'       |
      | 'PATCH'  | '{"array":[{"param-local-date":"X"}]}'       | 'array[0].param-local-date: is not a valid type'       |
      | 'DELETE' | '{"array":[{"param-local-date":"X"}]}'       | 'array[0].param-local-date: is not a valid type'       |
      | 'POST'   | '{"array":[{"param-local-time":"X"}]}'       | 'array[0].param-local-time: is not a valid type'       |
      | 'PUT'    | '{"array":[{"param-local-time":"X"}]}'       | 'array[0].param-local-time: is not a valid type'       |
      | 'PATCH'  | '{"array":[{"param-local-time":"X"}]}'       | 'array[0].param-local-time: is not a valid type'       |
      | 'DELETE' | '{"array":[{"param-local-time":"X"}]}'       | 'array[0].param-local-time: is not a valid type'       |
      | 'POST'   | '{"array":[{"param-year":"X"}]}'             | 'array[0].param-year: is not a valid type'             |
      | 'PUT'    | '{"array":[{"param-year":"X"}]}'             | 'array[0].param-year: is not a valid type'             |
      | 'PATCH'  | '{"array":[{"param-year":"X"}]}'             | 'array[0].param-year: is not a valid type'             |
      | 'DELETE' | '{"array":[{"param-year":"X"}]}'             | 'array[0].param-year: is not a valid type'             |
      | 'POST'   | '{"array":[{"param-enum-ab":"X"}]}'          | 'array[0].param-enum-ab: is not a valid type'          |
      | 'PUT'    | '{"array":[{"param-enum-ab":"X"}]}'          | 'array[0].param-enum-ab: is not a valid type'          |
      | 'PATCH'  | '{"array":[{"param-enum-ab":"X"}]}'          | 'array[0].param-enum-ab: is not a valid type'          |
      | 'DELETE' | '{"array":[{"param-enum-ab":"X"}]}'          | 'array[0].param-enum-ab: is not a valid type'          |
      | 'POST'   | '{"array":[{"param-enum-lower-ab":"X"}]}'    | 'array[0].param-enum-lower-ab: is not a valid type'    |
      | 'PUT'    | '{"array":[{"param-enum-lower-ab":"X"}]}'    | 'array[0].param-enum-lower-ab: is not a valid type'    |
      | 'PATCH'  | '{"array":[{"param-enum-lower-ab":"X"}]}'    | 'array[0].param-enum-lower-ab: is not a valid type'    |
      | 'DELETE' | '{"array":[{"param-enum-lower-ab":"X"}]}'    | 'array[0].param-enum-lower-ab: is not a valid type'    |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/nested-optional-dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in a nested optional dto attribute from an array, both having the same name
    and the service cannot map the sent value to the declared data type, responding with an error message.

    Given the endpoint '/api/v1/request-body/nested-optional-dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | request body                              | message                                             |
      | 'POST'   | '{"array":[{"paramInteger":"X"}]}'        | 'array[0].paramInteger: is not a valid type'        |
      | 'PUT'    | '{"array":[{"paramInteger":"X"}]}'        | 'array[0].paramInteger: is not a valid type'        |
      | 'PATCH'  | '{"array":[{"paramInteger":"X"}]}'        | 'array[0].paramInteger: is not a valid type'        |
      | 'DELETE' | '{"array":[{"paramInteger":"X"}]}'        | 'array[0].paramInteger: is not a valid type'        |
      | 'POST'   | '{"array":[{"paramLong":"X"}]}'           | 'array[0].paramLong: is not a valid type'           |
      | 'PUT'    | '{"array":[{"paramLong":"X"}]}'           | 'array[0].paramLong: is not a valid type'           |
      | 'PATCH'  | '{"array":[{"paramLong":"X"}]}'           | 'array[0].paramLong: is not a valid type'           |
      | 'DELETE' | '{"array":[{"paramLong":"X"}]}'           | 'array[0].paramLong: is not a valid type'           |
      | 'POST'   | '{"array":[{"paramBigInteger":"X"}]}'     | 'array[0].paramBigInteger: is not a valid type'     |
      | 'PUT'    | '{"array":[{"paramBigInteger":"X"}]}'     | 'array[0].paramBigInteger: is not a valid type'     |
      | 'PATCH'  | '{"array":[{"paramBigInteger":"X"}]}'     | 'array[0].paramBigInteger: is not a valid type'     |
      | 'DELETE' | '{"array":[{"paramBigInteger":"X"}]}'     | 'array[0].paramBigInteger: is not a valid type'     |
      | 'POST'   | '{"array":[{"paramFloat":"X"}]}'          | 'array[0].paramFloat: is not a valid type'          |
      | 'PUT'    | '{"array":[{"paramFloat":"X"}]}'          | 'array[0].paramFloat: is not a valid type'          |
      | 'PATCH'  | '{"array":[{"paramFloat":"X"}]}'          | 'array[0].paramFloat: is not a valid type'          |
      | 'DELETE' | '{"array":[{"paramFloat":"X"}]}'          | 'array[0].paramFloat: is not a valid type'          |
      | 'POST'   | '{"array":[{"paramDouble":"X"}]}'         | 'array[0].paramDouble: is not a valid type'         |
      | 'PUT'    | '{"array":[{"paramDouble":"X"}]}'         | 'array[0].paramDouble: is not a valid type'         |
      | 'PATCH'  | '{"array":[{"paramDouble":"X"}]}'         | 'array[0].paramDouble: is not a valid type'         |
      | 'DELETE' | '{"array":[{"paramDouble":"X"}]}'         | 'array[0].paramDouble: is not a valid type'         |
      | 'POST'   | '{"array":[{"paramBigDecimal":"X"}]}'     | 'array[0].paramBigDecimal: is not a valid type'     |
      | 'PUT'    | '{"array":[{"paramBigDecimal":"X"}]}'     | 'array[0].paramBigDecimal: is not a valid type'     |
      | 'PATCH'  | '{"array":[{"paramBigDecimal":"X"}]}'     | 'array[0].paramBigDecimal: is not a valid type'     |
      | 'DELETE' | '{"array":[{"paramBigDecimal":"X"}]}'     | 'array[0].paramBigDecimal: is not a valid type'     |
      | 'POST'   | '{"array":[{"paramBoolean":"X"}]}'        | 'array[0].paramBoolean: is not a valid type'        |
      | 'PUT'    | '{"array":[{"paramBoolean":"X"}]}'        | 'array[0].paramBoolean: is not a valid type'        |
      | 'PATCH'  | '{"array":[{"paramBoolean":"X"}]}'        | 'array[0].paramBoolean: is not a valid type'        |
      | 'DELETE' | '{"array":[{"paramBoolean":"X"}]}'        | 'array[0].paramBoolean: is not a valid type'        |
      | 'POST'   | '{"array":[{"paramOffsetDateTime":"X"}]}' | 'array[0].paramOffsetDateTime: is not a valid type' |
      | 'PUT'    | '{"array":[{"paramOffsetDateTime":"X"}]}' | 'array[0].paramOffsetDateTime: is not a valid type' |
      | 'PATCH'  | '{"array":[{"paramOffsetDateTime":"X"}]}' | 'array[0].paramOffsetDateTime: is not a valid type' |
      | 'DELETE' | '{"array":[{"paramOffsetDateTime":"X"}]}' | 'array[0].paramOffsetDateTime: is not a valid type' |
      | 'POST'   | '{"array":[{"paramLocalDateTime":"X"}]}'  | 'array[0].paramLocalDateTime: is not a valid type'  |
      | 'PUT'    | '{"array":[{"paramLocalDateTime":"X"}]}'  | 'array[0].paramLocalDateTime: is not a valid type'  |
      | 'PATCH'  | '{"array":[{"paramLocalDateTime":"X"}]}'  | 'array[0].paramLocalDateTime: is not a valid type'  |
      | 'DELETE' | '{"array":[{"paramLocalDateTime":"X"}]}'  | 'array[0].paramLocalDateTime: is not a valid type'  |
      | 'POST'   | '{"array":[{"paramLocalDate":"X"}]}'      | 'array[0].paramLocalDate: is not a valid type'      |
      | 'PUT'    | '{"array":[{"paramLocalDate":"X"}]}'      | 'array[0].paramLocalDate: is not a valid type'      |
      | 'PATCH'  | '{"array":[{"paramLocalDate":"X"}]}'      | 'array[0].paramLocalDate: is not a valid type'      |
      | 'DELETE' | '{"array":[{"paramLocalDate":"X"}]}'      | 'array[0].paramLocalDate: is not a valid type'      |
      | 'POST'   | '{"array":[{"paramLocalTime":"X"}]}'      | 'array[0].paramLocalTime: is not a valid type'      |
      | 'PUT'    | '{"array":[{"paramLocalTime":"X"}]}'      | 'array[0].paramLocalTime: is not a valid type'      |
      | 'PATCH'  | '{"array":[{"paramLocalTime":"X"}]}'      | 'array[0].paramLocalTime: is not a valid type'      |
      | 'DELETE' | '{"array":[{"paramLocalTime":"X"}]}'      | 'array[0].paramLocalTime: is not a valid type'      |
      | 'POST'   | '{"array":[{"paramYear":"X"}]}'           | 'array[0].paramYear: is not a valid type'           |
      | 'PUT'    | '{"array":[{"paramYear":"X"}]}'           | 'array[0].paramYear: is not a valid type'           |
      | 'PATCH'  | '{"array":[{"paramYear":"X"}]}'           | 'array[0].paramYear: is not a valid type'           |
      | 'DELETE' | '{"array":[{"paramYear":"X"}]}'           | 'array[0].paramYear: is not a valid type'           |
      | 'POST'   | '{"array":[{"paramEnumAB":"X"}]}'         | 'array[0].paramEnumAB: is not a valid type'         |
      | 'PUT'    | '{"array":[{"paramEnumAB":"X"}]}'         | 'array[0].paramEnumAB: is not a valid type'         |
      | 'PATCH'  | '{"array":[{"paramEnumAB":"X"}]}'         | 'array[0].paramEnumAB: is not a valid type'         |
      | 'DELETE' | '{"array":[{"paramEnumAB":"X"}]}'         | 'array[0].paramEnumAB: is not a valid type'         |
      | 'POST'   | '{"array":[{"paramEnumLowerAB":"X"}]}'    | 'array[0].paramEnumLowerAB: is not a valid type'    |
      | 'PUT'    | '{"array":[{"paramEnumLowerAB":"X"}]}'    | 'array[0].paramEnumLowerAB: is not a valid type'    |
      | 'PATCH'  | '{"array":[{"paramEnumLowerAB":"X"}]}'    | 'array[0].paramEnumLowerAB: is not a valid type'    |
      | 'DELETE' | '{"array":[{"paramEnumLowerAB":"X"}]}'    | 'array[0].paramEnumLowerAB: is not a valid type'    |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/named-nested-optional-dto-attribute endpoint with the body <request body> (RequestBodyUnconstrainedRestApi.class)
    this endpoint receives the request body-attibute in a nested optional dto attribute from an array, both having the diferent names, a custom body-attibute name is defined by the endpoint
    and the service cannot map the sent value to the declared data type, responding with an error message.

    Given the endpoint '/api/v1/request-body/named-nested-optional-dto-attribute'
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | request body                                 | message                                                |
      | 'POST'   | '{"array":[{"param-integer":"X"}]}'          | 'array[0].param-integer: is not a valid type'          |
      | 'PUT'    | '{"array":[{"param-integer":"X"}]}'          | 'array[0].param-integer: is not a valid type'          |
      | 'PATCH'  | '{"array":[{"param-integer":"X"}]}'          | 'array[0].param-integer: is not a valid type'          |
      | 'DELETE' | '{"array":[{"param-integer":"X"}]}'          | 'array[0].param-integer: is not a valid type'          |
      | 'POST'   | '{"array":[{"param-long":"X"}]}'             | 'array[0].param-long: is not a valid type'             |
      | 'PUT'    | '{"array":[{"param-long":"X"}]}'             | 'array[0].param-long: is not a valid type'             |
      | 'PATCH'  | '{"array":[{"param-long":"X"}]}'             | 'array[0].param-long: is not a valid type'             |
      | 'DELETE' | '{"array":[{"param-long":"X"}]}'             | 'array[0].param-long: is not a valid type'             |
      | 'POST'   | '{"array":[{"param-big-integer":"X"}]}'      | 'array[0].param-big-integer: is not a valid type'      |
      | 'PUT'    | '{"array":[{"param-big-integer":"X"}]}'      | 'array[0].param-big-integer: is not a valid type'      |
      | 'PATCH'  | '{"array":[{"param-big-integer":"X"}]}'      | 'array[0].param-big-integer: is not a valid type'      |
      | 'DELETE' | '{"array":[{"param-big-integer":"X"}]}'      | 'array[0].param-big-integer: is not a valid type'      |
      | 'POST'   | '{"array":[{"param-float":"X"}]}'            | 'array[0].param-float: is not a valid type'            |
      | 'PUT'    | '{"array":[{"param-float":"X"}]}'            | 'array[0].param-float: is not a valid type'            |
      | 'PATCH'  | '{"array":[{"param-float":"X"}]}'            | 'array[0].param-float: is not a valid type'            |
      | 'DELETE' | '{"array":[{"param-float":"X"}]}'            | 'array[0].param-float: is not a valid type'            |
      | 'POST'   | '{"array":[{"param-double":"X"}]}'           | 'array[0].param-double: is not a valid type'           |
      | 'PUT'    | '{"array":[{"param-double":"X"}]}'           | 'array[0].param-double: is not a valid type'           |
      | 'PATCH'  | '{"array":[{"param-double":"X"}]}'           | 'array[0].param-double: is not a valid type'           |
      | 'DELETE' | '{"array":[{"param-double":"X"}]}'           | 'array[0].param-double: is not a valid type'           |
      | 'POST'   | '{"array":[{"param-big-decimal":"X"}]}'      | 'array[0].param-big-decimal: is not a valid type'      |
      | 'PUT'    | '{"array":[{"param-big-decimal":"X"}]}'      | 'array[0].param-big-decimal: is not a valid type'      |
      | 'PATCH'  | '{"array":[{"param-big-decimal":"X"}]}'      | 'array[0].param-big-decimal: is not a valid type'      |
      | 'DELETE' | '{"array":[{"param-big-decimal":"X"}]}'      | 'array[0].param-big-decimal: is not a valid type'      |
      | 'POST'   | '{"array":[{"param-boolean":"X"}]}'          | 'array[0].param-boolean: is not a valid type'          |
      | 'PUT'    | '{"array":[{"param-boolean":"X"}]}'          | 'array[0].param-boolean: is not a valid type'          |
      | 'PATCH'  | '{"array":[{"param-boolean":"X"}]}'          | 'array[0].param-boolean: is not a valid type'          |
      | 'DELETE' | '{"array":[{"param-boolean":"X"}]}'          | 'array[0].param-boolean: is not a valid type'          |
      | 'POST'   | '{"array":[{"param-offset-date-time":"X"}]}' | 'array[0].param-offset-date-time: is not a valid type' |
      | 'PUT'    | '{"array":[{"param-offset-date-time":"X"}]}' | 'array[0].param-offset-date-time: is not a valid type' |
      | 'PATCH'  | '{"array":[{"param-offset-date-time":"X"}]}' | 'array[0].param-offset-date-time: is not a valid type' |
      | 'DELETE' | '{"array":[{"param-offset-date-time":"X"}]}' | 'array[0].param-offset-date-time: is not a valid type' |
      | 'POST'   | '{"array":[{"param-local-date-time":"X"}]}'  | 'array[0].param-local-date-time: is not a valid type'  |
      | 'PUT'    | '{"array":[{"param-local-date-time":"X"}]}'  | 'array[0].param-local-date-time: is not a valid type'  |
      | 'PATCH'  | '{"array":[{"param-local-date-time":"X"}]}'  | 'array[0].param-local-date-time: is not a valid type'  |
      | 'DELETE' | '{"array":[{"param-local-date-time":"X"}]}'  | 'array[0].param-local-date-time: is not a valid type'  |
      | 'POST'   | '{"array":[{"param-local-date":"X"}]}'       | 'array[0].param-local-date: is not a valid type'       |
      | 'PUT'    | '{"array":[{"param-local-date":"X"}]}'       | 'array[0].param-local-date: is not a valid type'       |
      | 'PATCH'  | '{"array":[{"param-local-date":"X"}]}'       | 'array[0].param-local-date: is not a valid type'       |
      | 'DELETE' | '{"array":[{"param-local-date":"X"}]}'       | 'array[0].param-local-date: is not a valid type'       |
      | 'POST'   | '{"array":[{"param-local-time":"X"}]}'       | 'array[0].param-local-time: is not a valid type'       |
      | 'PUT'    | '{"array":[{"param-local-time":"X"}]}'       | 'array[0].param-local-time: is not a valid type'       |
      | 'PATCH'  | '{"array":[{"param-local-time":"X"}]}'       | 'array[0].param-local-time: is not a valid type'       |
      | 'DELETE' | '{"array":[{"param-local-time":"X"}]}'       | 'array[0].param-local-time: is not a valid type'       |
      | 'POST'   | '{"array":[{"param-year":"X"}]}'             | 'array[0].param-year: is not a valid type'             |
      | 'PUT'    | '{"array":[{"param-year":"X"}]}'             | 'array[0].param-year: is not a valid type'             |
      | 'PATCH'  | '{"array":[{"param-year":"X"}]}'             | 'array[0].param-year: is not a valid type'             |
      | 'DELETE' | '{"array":[{"param-year":"X"}]}'             | 'array[0].param-year: is not a valid type'             |
      | 'POST'   | '{"array":[{"param-enum-ab":"X"}]}'          | 'array[0].param-enum-ab: is not a valid type'          |
      | 'PUT'    | '{"array":[{"param-enum-ab":"X"}]}'          | 'array[0].param-enum-ab: is not a valid type'          |
      | 'PATCH'  | '{"array":[{"param-enum-ab":"X"}]}'          | 'array[0].param-enum-ab: is not a valid type'          |
      | 'DELETE' | '{"array":[{"param-enum-ab":"X"}]}'          | 'array[0].param-enum-ab: is not a valid type'          |
      | 'POST'   | '{"array":[{"param-enum-lower-ab":"X"}]}'    | 'array[0].param-enum-lower-ab: is not a valid type'    |
      | 'PUT'    | '{"array":[{"param-enum-lower-ab":"X"}]}'    | 'array[0].param-enum-lower-ab: is not a valid type'    |
      | 'PATCH'  | '{"array":[{"param-enum-lower-ab":"X"}]}'    | 'array[0].param-enum-lower-ab: is not a valid type'    |
      | 'DELETE' | '{"array":[{"param-enum-lower-ab":"X"}]}'    | 'array[0].param-enum-lower-ab: is not a valid type'    |
