Feature: Request body with i18n-validation parameter value
  
  The REST service receives data via the request body and it does not accomplish with some constraint. The service responds with an error message.
  
   Rule: The conversion between data types is given by the following table
  
  | parameter type | java type      | validation              | validation                                   |
  |----------------|----------------|-------------------------|----------------------------------------------|
  | string         | String         | @Size                   | @Size(max = 1)                               |
  | number         | Integer        | @Min @Max               | @Max(1)                                      |
  | number         | Long           | @Min @Max               | @Max(1)                                      |
  | number         | BigInteger     | @DecimalMin @DecimalMax | @DecimalMax("1")                             |
  | number         | Float          |                         |                                              |
  | number         | Double         |                         |                                              |
  | number         | BigDecimal     | @DecimalMin @DecimalMax | @DecimalMax("9.9")                           |
  | boolean        | Boolean        |                         |                                              |
  | ISO-8601       | OffsetDateTime | @DatetimeRange          | @DatetimeRange(max = "2000-01-01T00:00:00Z") |
  | ISO-8601       | LocalDateTime  | @DatetimeRange          | @DatetimeRange(max = "2000-01-01T00:00:00Z") |
  | ISO-8601       | LocalDate      | @DateRange              | @DateRange(max = "2000-01-01")               |
  | ISO-8601       | LocalTime      |                         |                                              |
  | ISO-8601       | Year           | @YearRange              | @YearRange(max = "2000")                     |
  | string         | ENUM           |                         |                                              |

  Background: 
    Given the default timezone 'America/Puerto_Rico'

  Scenario Outline: The <method> call is made to the /api/v1/request-body/i18n-constrained/dto-attribute<path> endpoint with the body <request body> (RequestBodyConstrainedNotNullRestApi.class)
    this endpoint receives the request body-attibute in a dto attribute, both having the same name
    and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/request-body/i18n-constrained/dto-attribute'<path>
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | request body                                     | message                                               |
      | 'POST'   | '/paramString'         | '{"paramString":"ab"}'                           | 'paramString: is not a valid string'                  |
      | 'PUT'    | '/paramString'         | '{"paramString":"ab"}'                           | 'paramString: is not a valid string'                  |
      | 'PATCH'  | '/paramString'         | '{"paramString":"ab"}'                           | 'paramString: is not a valid string'                  |
      | 'DELETE' | '/paramString'         | '{"paramString":"ab"}'                           | 'paramString: is not a valid string'                  |
      | 'POST'   | '/paramInteger'        | '{"paramInteger":10}'                            | 'paramInteger: is not a valid integer'                |
      | 'PUT'    | '/paramInteger'        | '{"paramInteger":10}'                            | 'paramInteger: is not a valid integer'                |
      | 'PATCH'  | '/paramInteger'        | '{"paramInteger":10}'                            | 'paramInteger: is not a valid integer'                |
      | 'DELETE' | '/paramInteger'        | '{"paramInteger":10}'                            | 'paramInteger: is not a valid integer'                |
      | 'POST'   | '/paramLong'           | '{"paramLong":10}'                               | 'paramLong: is not a valid long'                      |
      | 'PUT'    | '/paramLong'           | '{"paramLong":10}'                               | 'paramLong: is not a valid long'                      |
      | 'PATCH'  | '/paramLong'           | '{"paramLong":10}'                               | 'paramLong: is not a valid long'                      |
      | 'DELETE' | '/paramLong'           | '{"paramLong":10}'                               | 'paramLong: is not a valid long'                      |
      | 'POST'   | '/paramBigInteger'     | '{"paramBigInteger":10}'                         | 'paramBigInteger: is not a valid big-integer'         |
      | 'PUT'    | '/paramBigInteger'     | '{"paramBigInteger":10}'                         | 'paramBigInteger: is not a valid big-integer'         |
      | 'PATCH'  | '/paramBigInteger'     | '{"paramBigInteger":10}'                         | 'paramBigInteger: is not a valid big-integer'         |
      | 'DELETE' | '/paramBigInteger'     | '{"paramBigInteger":10}'                         | 'paramBigInteger: is not a valid big-integer'         |
      | 'POST'   | '/paramBigDecimal'     | '{"paramBigDecimal":10}'                         | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'PUT'    | '/paramBigDecimal'     | '{"paramBigDecimal":10}'                         | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'PATCH'  | '/paramBigDecimal'     | '{"paramBigDecimal":10}'                         | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'DELETE' | '/paramBigDecimal'     | '{"paramBigDecimal":10}'                         | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'POST'   | '/paramOffsetDateTime' | '{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PUT'    | '/paramOffsetDateTime' | '{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PATCH'  | '/paramOffsetDateTime' | '{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'DELETE' | '/paramOffsetDateTime' | '{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'POST'   | '/paramLocalDateTime'  | '{"paramLocalDateTime":"2100-01-01T00:00:00Z"}'  | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'PUT'    | '/paramLocalDateTime'  | '{"paramLocalDateTime":"2100-01-01T00:00:00Z"}'  | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'PATCH'  | '/paramLocalDateTime'  | '{"paramLocalDateTime":"2100-01-01T00:00:00Z"}'  | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'DELETE' | '/paramLocalDateTime'  | '{"paramLocalDateTime":"2100-01-01T00:00:00Z"}'  | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'POST'   | '/paramLocalDate'      | '{"paramLocalDate":"2100-01-01"}'                | 'paramLocalDate: is not a valid date'                 |
      | 'PUT'    | '/paramLocalDate'      | '{"paramLocalDate":"2100-01-01"}'                | 'paramLocalDate: is not a valid date'                 |
      | 'PATCH'  | '/paramLocalDate'      | '{"paramLocalDate":"2100-01-01"}'                | 'paramLocalDate: is not a valid date'                 |
      | 'DELETE' | '/paramLocalDate'      | '{"paramLocalDate":"2100-01-01"}'                | 'paramLocalDate: is not a valid date'                 |
      | 'POST'   | '/paramYear'           | '{"paramYear":"2100"}'                           | 'paramYear: is not a valid year'                      |
      | 'PUT'    | '/paramYear'           | '{"paramYear":"2100"}'                           | 'paramYear: is not a valid year'                      |
      | 'PATCH'  | '/paramYear'           | '{"paramYear":"2100"}'                           | 'paramYear: is not a valid year'                      |
      | 'DELETE' | '/paramYear'           | '{"paramYear":"2100"}'                           | 'paramYear: is not a valid year'                      |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/i18n-constrained/named-dto-attribute<path> endpoint with the body <request body> (RequestBodyConstrainedNotNullRestApi.class)
    this endpoint receives the request body-attibute in a dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint 
    and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/request-body/i18n-constrained/named-dto-attribute'<path>
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | request body                                        | message                                               |
      | 'POST'   | '/paramString'         | '{"param-string":"ab"}'                             | 'paramString: is not a valid string'                  |
      | 'PUT'    | '/paramString'         | '{"param-string":"ab"}'                             | 'paramString: is not a valid string'                  |
      | 'PATCH'  | '/paramString'         | '{"param-string":"ab"}'                             | 'paramString: is not a valid string'                  |
      | 'DELETE' | '/paramString'         | '{"param-string":"ab"}'                             | 'paramString: is not a valid string'                  |
      | 'POST'   | '/paramInteger'        | '{"param-integer":10}'                              | 'paramInteger: is not a valid integer'                |
      | 'PUT'    | '/paramInteger'        | '{"param-integer":10}'                              | 'paramInteger: is not a valid integer'                |
      | 'PATCH'  | '/paramInteger'        | '{"param-integer":10}'                              | 'paramInteger: is not a valid integer'                |
      | 'DELETE' | '/paramInteger'        | '{"param-integer":10}'                              | 'paramInteger: is not a valid integer'                |
      | 'POST'   | '/paramLong'           | '{"param-long":10}'                                 | 'paramLong: is not a valid long'                      |
      | 'PUT'    | '/paramLong'           | '{"param-long":10}'                                 | 'paramLong: is not a valid long'                      |
      | 'PATCH'  | '/paramLong'           | '{"param-long":10}'                                 | 'paramLong: is not a valid long'                      |
      | 'DELETE' | '/paramLong'           | '{"param-long":10}'                                 | 'paramLong: is not a valid long'                      |
      | 'POST'   | '/paramBigInteger'     | '{"param-big-integer":10}'                          | 'paramBigInteger: is not a valid big-integer'         |
      | 'PUT'    | '/paramBigInteger'     | '{"param-big-integer":10}'                          | 'paramBigInteger: is not a valid big-integer'         |
      | 'PATCH'  | '/paramBigInteger'     | '{"param-big-integer":10}'                          | 'paramBigInteger: is not a valid big-integer'         |
      | 'DELETE' | '/paramBigInteger'     | '{"param-big-integer":10}'                          | 'paramBigInteger: is not a valid big-integer'         |
      | 'POST'   | '/paramBigDecimal'     | '{"param-big-decimal":10}'                          | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'PUT'    | '/paramBigDecimal'     | '{"param-big-decimal":10}'                          | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'PATCH'  | '/paramBigDecimal'     | '{"param-big-decimal":10}'                          | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'DELETE' | '/paramBigDecimal'     | '{"param-big-decimal":10}'                          | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'POST'   | '/paramOffsetDateTime' | '{"param-offset-date-time":"2100-01-01T00:00:00Z"}' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PUT'    | '/paramOffsetDateTime' | '{"param-offset-date-time":"2100-01-01T00:00:00Z"}' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PATCH'  | '/paramOffsetDateTime' | '{"param-offset-date-time":"2100-01-01T00:00:00Z"}' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'DELETE' | '/paramOffsetDateTime' | '{"param-offset-date-time":"2100-01-01T00:00:00Z"}' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'POST'   | '/paramLocalDateTime'  | '{"param-local-date-time":"2100-01-01T00:00:00Z"}'  | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'PUT'    | '/paramLocalDateTime'  | '{"param-local-date-time":"2100-01-01T00:00:00Z"}'  | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'PATCH'  | '/paramLocalDateTime'  | '{"param-local-date-time":"2100-01-01T00:00:00Z"}'  | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'DELETE' | '/paramLocalDateTime'  | '{"param-local-date-time":"2100-01-01T00:00:00Z"}'  | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'POST'   | '/paramLocalDate'      | '{"param-local-date":"2100-01-01"}'                 | 'paramLocalDate: is not a valid date'                 |
      | 'PUT'    | '/paramLocalDate'      | '{"param-local-date":"2100-01-01"}'                 | 'paramLocalDate: is not a valid date'                 |
      | 'PATCH'  | '/paramLocalDate'      | '{"param-local-date":"2100-01-01"}'                 | 'paramLocalDate: is not a valid date'                 |
      | 'DELETE' | '/paramLocalDate'      | '{"param-local-date":"2100-01-01"}'                 | 'paramLocalDate: is not a valid date'                 |
      | 'POST'   | '/paramYear'           | '{"param-year":"2100"}'                             | 'paramYear: is not a valid year'                      |
      | 'PUT'    | '/paramYear'           | '{"param-year":"2100"}'                             | 'paramYear: is not a valid year'                      |
      | 'PATCH'  | '/paramYear'           | '{"param-year":"2100"}'                             | 'paramYear: is not a valid year'                      |
      | 'DELETE' | '/paramYear'           | '{"param-year":"2100"}'                             | 'paramYear: is not a valid year'                      |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/i18n-constrained/optional-dto-attribute<path> endpoint with the body <request body> (RequestBodyConstrainedNotNullRestApi.class)
    this endpoint receives the request body-attibute in an optional dto attribute, both having the same name
    and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/request-body/i18n-constrained/optional-dto-attribute'<path>
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | request body                                     | message                                               |
      | 'POST'   | '/paramString'         | '{"paramString":"ab"}'                           | 'paramString: is not a valid string'                  |
      | 'PUT'    | '/paramString'         | '{"paramString":"ab"}'                           | 'paramString: is not a valid string'                  |
      | 'PATCH'  | '/paramString'         | '{"paramString":"ab"}'                           | 'paramString: is not a valid string'                  |
      | 'DELETE' | '/paramString'         | '{"paramString":"ab"}'                           | 'paramString: is not a valid string'                  |
      | 'POST'   | '/paramInteger'        | '{"paramInteger":10}'                            | 'paramInteger: is not a valid integer'                |
      | 'PUT'    | '/paramInteger'        | '{"paramInteger":10}'                            | 'paramInteger: is not a valid integer'                |
      | 'PATCH'  | '/paramInteger'        | '{"paramInteger":10}'                            | 'paramInteger: is not a valid integer'                |
      | 'DELETE' | '/paramInteger'        | '{"paramInteger":10}'                            | 'paramInteger: is not a valid integer'                |
      | 'POST'   | '/paramLong'           | '{"paramLong":10}'                               | 'paramLong: is not a valid long'                      |
      | 'PUT'    | '/paramLong'           | '{"paramLong":10}'                               | 'paramLong: is not a valid long'                      |
      | 'PATCH'  | '/paramLong'           | '{"paramLong":10}'                               | 'paramLong: is not a valid long'                      |
      | 'DELETE' | '/paramLong'           | '{"paramLong":10}'                               | 'paramLong: is not a valid long'                      |
      | 'POST'   | '/paramBigInteger'     | '{"paramBigInteger":10}'                         | 'paramBigInteger: is not a valid big-integer'         |
      | 'PUT'    | '/paramBigInteger'     | '{"paramBigInteger":10}'                         | 'paramBigInteger: is not a valid big-integer'         |
      | 'PATCH'  | '/paramBigInteger'     | '{"paramBigInteger":10}'                         | 'paramBigInteger: is not a valid big-integer'         |
      | 'DELETE' | '/paramBigInteger'     | '{"paramBigInteger":10}'                         | 'paramBigInteger: is not a valid big-integer'         |
      | 'POST'   | '/paramBigDecimal'     | '{"paramBigDecimal":10}'                         | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'PUT'    | '/paramBigDecimal'     | '{"paramBigDecimal":10}'                         | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'PATCH'  | '/paramBigDecimal'     | '{"paramBigDecimal":10}'                         | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'DELETE' | '/paramBigDecimal'     | '{"paramBigDecimal":10}'                         | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'POST'   | '/paramOffsetDateTime' | '{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PUT'    | '/paramOffsetDateTime' | '{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PATCH'  | '/paramOffsetDateTime' | '{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'DELETE' | '/paramOffsetDateTime' | '{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'POST'   | '/paramLocalDateTime'  | '{"paramLocalDateTime":"2100-01-01T00:00:00Z"}'  | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'PUT'    | '/paramLocalDateTime'  | '{"paramLocalDateTime":"2100-01-01T00:00:00Z"}'  | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'PATCH'  | '/paramLocalDateTime'  | '{"paramLocalDateTime":"2100-01-01T00:00:00Z"}'  | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'DELETE' | '/paramLocalDateTime'  | '{"paramLocalDateTime":"2100-01-01T00:00:00Z"}'  | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'POST'   | '/paramLocalDate'      | '{"paramLocalDate":"2100-01-01"}'                | 'paramLocalDate: is not a valid date'                 |
      | 'PUT'    | '/paramLocalDate'      | '{"paramLocalDate":"2100-01-01"}'                | 'paramLocalDate: is not a valid date'                 |
      | 'PATCH'  | '/paramLocalDate'      | '{"paramLocalDate":"2100-01-01"}'                | 'paramLocalDate: is not a valid date'                 |
      | 'DELETE' | '/paramLocalDate'      | '{"paramLocalDate":"2100-01-01"}'                | 'paramLocalDate: is not a valid date'                 |
      | 'POST'   | '/paramYear'           | '{"paramYear":"2100"}'                           | 'paramYear: is not a valid year'                      |
      | 'PUT'    | '/paramYear'           | '{"paramYear":"2100"}'                           | 'paramYear: is not a valid year'                      |
      | 'PATCH'  | '/paramYear'           | '{"paramYear":"2100"}'                           | 'paramYear: is not a valid year'                      |
      | 'DELETE' | '/paramYear'           | '{"paramYear":"2100"}'                           | 'paramYear: is not a valid year'                      |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/i18n-constrained/named-optional-dto-attribute<path> endpoint with the body <request body> (RequestBodyConstrainedNotNullRestApi.class)
    this endpoint receives the request body-attibute in an optional dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
    and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/request-body/i18n-constrained/named-optional-dto-attribute'<path>
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | request body                                        | message                                               |
      | 'POST'   | '/paramString'         | '{"param-string":"ab"}'                             | 'paramString: is not a valid string'                  |
      | 'PUT'    | '/paramString'         | '{"param-string":"ab"}'                             | 'paramString: is not a valid string'                  |
      | 'PATCH'  | '/paramString'         | '{"param-string":"ab"}'                             | 'paramString: is not a valid string'                  |
      | 'DELETE' | '/paramString'         | '{"param-string":"ab"}'                             | 'paramString: is not a valid string'                  |
      | 'POST'   | '/paramInteger'        | '{"param-integer":10}'                              | 'paramInteger: is not a valid integer'                |
      | 'PUT'    | '/paramInteger'        | '{"param-integer":10}'                              | 'paramInteger: is not a valid integer'                |
      | 'PATCH'  | '/paramInteger'        | '{"param-integer":10}'                              | 'paramInteger: is not a valid integer'                |
      | 'DELETE' | '/paramInteger'        | '{"param-integer":10}'                              | 'paramInteger: is not a valid integer'                |
      | 'POST'   | '/paramLong'           | '{"param-long":10}'                                 | 'paramLong: is not a valid long'                      |
      | 'PUT'    | '/paramLong'           | '{"param-long":10}'                                 | 'paramLong: is not a valid long'                      |
      | 'PATCH'  | '/paramLong'           | '{"param-long":10}'                                 | 'paramLong: is not a valid long'                      |
      | 'DELETE' | '/paramLong'           | '{"param-long":10}'                                 | 'paramLong: is not a valid long'                      |
      | 'POST'   | '/paramBigInteger'     | '{"param-big-integer":10}'                          | 'paramBigInteger: is not a valid big-integer'         |
      | 'PUT'    | '/paramBigInteger'     | '{"param-big-integer":10}'                          | 'paramBigInteger: is not a valid big-integer'         |
      | 'PATCH'  | '/paramBigInteger'     | '{"param-big-integer":10}'                          | 'paramBigInteger: is not a valid big-integer'         |
      | 'DELETE' | '/paramBigInteger'     | '{"param-big-integer":10}'                          | 'paramBigInteger: is not a valid big-integer'         |
      | 'POST'   | '/paramBigDecimal'     | '{"param-big-decimal":10}'                          | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'PUT'    | '/paramBigDecimal'     | '{"param-big-decimal":10}'                          | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'PATCH'  | '/paramBigDecimal'     | '{"param-big-decimal":10}'                          | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'DELETE' | '/paramBigDecimal'     | '{"param-big-decimal":10}'                          | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'POST'   | '/paramOffsetDateTime' | '{"param-offset-date-time":"2100-01-01T00:00:00Z"}' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PUT'    | '/paramOffsetDateTime' | '{"param-offset-date-time":"2100-01-01T00:00:00Z"}' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PATCH'  | '/paramOffsetDateTime' | '{"param-offset-date-time":"2100-01-01T00:00:00Z"}' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'DELETE' | '/paramOffsetDateTime' | '{"param-offset-date-time":"2100-01-01T00:00:00Z"}' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'POST'   | '/paramLocalDateTime'  | '{"param-local-date-time":"2100-01-01T00:00:00Z"}'  | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'PUT'    | '/paramLocalDateTime'  | '{"param-local-date-time":"2100-01-01T00:00:00Z"}'  | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'PATCH'  | '/paramLocalDateTime'  | '{"param-local-date-time":"2100-01-01T00:00:00Z"}'  | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'DELETE' | '/paramLocalDateTime'  | '{"param-local-date-time":"2100-01-01T00:00:00Z"}'  | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'POST'   | '/paramLocalDate'      | '{"param-local-date":"2100-01-01"}'                 | 'paramLocalDate: is not a valid date'                 |
      | 'PUT'    | '/paramLocalDate'      | '{"param-local-date":"2100-01-01"}'                 | 'paramLocalDate: is not a valid date'                 |
      | 'PATCH'  | '/paramLocalDate'      | '{"param-local-date":"2100-01-01"}'                 | 'paramLocalDate: is not a valid date'                 |
      | 'DELETE' | '/paramLocalDate'      | '{"param-local-date":"2100-01-01"}'                 | 'paramLocalDate: is not a valid date'                 |
      | 'POST'   | '/paramYear'           | '{"param-year":"2100"}'                             | 'paramYear: is not a valid year'                      |
      | 'PUT'    | '/paramYear'           | '{"param-year":"2100"}'                             | 'paramYear: is not a valid year'                      |
      | 'PATCH'  | '/paramYear'           | '{"param-year":"2100"}'                             | 'paramYear: is not a valid year'                      |
      | 'DELETE' | '/paramYear'           | '{"param-year":"2100"}'                             | 'paramYear: is not a valid year'                      |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/i18n-constrained/nested-dto-attribute<path> endpoint with the body <request body> (RequestBodyConstrainedNotNullRestApi.class)
    this endpoint receives the request body-attibute in a nested dto attribute, both having the same name
    and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/request-body/i18n-constrained/nested-dto-attribute'<path>
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | request body                                                | message                                                      |
      | 'POST'   | '/paramString'         | '{"nested":{"paramString":"ab"}}'                           | 'nested.paramString: is not a valid string'                  |
      | 'PUT'    | '/paramString'         | '{"nested":{"paramString":"ab"}}'                           | 'nested.paramString: is not a valid string'                  |
      | 'PATCH'  | '/paramString'         | '{"nested":{"paramString":"ab"}}'                           | 'nested.paramString: is not a valid string'                  |
      | 'DELETE' | '/paramString'         | '{"nested":{"paramString":"ab"}}'                           | 'nested.paramString: is not a valid string'                  |
      | 'POST'   | '/paramInteger'        | '{"nested":{"paramInteger":10}}'                            | 'nested.paramInteger: is not a valid integer'                |
      | 'PUT'    | '/paramInteger'        | '{"nested":{"paramInteger":10}}'                            | 'nested.paramInteger: is not a valid integer'                |
      | 'PATCH'  | '/paramInteger'        | '{"nested":{"paramInteger":10}}'                            | 'nested.paramInteger: is not a valid integer'                |
      | 'DELETE' | '/paramInteger'        | '{"nested":{"paramInteger":10}}'                            | 'nested.paramInteger: is not a valid integer'                |
      | 'POST'   | '/paramLong'           | '{"nested":{"paramLong":10}}'                               | 'nested.paramLong: is not a valid long'                      |
      | 'PUT'    | '/paramLong'           | '{"nested":{"paramLong":10}}'                               | 'nested.paramLong: is not a valid long'                      |
      | 'PATCH'  | '/paramLong'           | '{"nested":{"paramLong":10}}'                               | 'nested.paramLong: is not a valid long'                      |
      | 'DELETE' | '/paramLong'           | '{"nested":{"paramLong":10}}'                               | 'nested.paramLong: is not a valid long'                      |
      | 'POST'   | '/paramBigInteger'     | '{"nested":{"paramBigInteger":10}}'                         | 'nested.paramBigInteger: is not a valid big-integer'         |
      | 'PUT'    | '/paramBigInteger'     | '{"nested":{"paramBigInteger":10}}'                         | 'nested.paramBigInteger: is not a valid big-integer'         |
      | 'PATCH'  | '/paramBigInteger'     | '{"nested":{"paramBigInteger":10}}'                         | 'nested.paramBigInteger: is not a valid big-integer'         |
      | 'DELETE' | '/paramBigInteger'     | '{"nested":{"paramBigInteger":10}}'                         | 'nested.paramBigInteger: is not a valid big-integer'         |
      | 'POST'   | '/paramBigDecimal'     | '{"nested":{"paramBigDecimal":10}}'                         | 'nested.paramBigDecimal: is not a valid big-decimal'         |
      | 'PUT'    | '/paramBigDecimal'     | '{"nested":{"paramBigDecimal":10}}'                         | 'nested.paramBigDecimal: is not a valid big-decimal'         |
      | 'PATCH'  | '/paramBigDecimal'     | '{"nested":{"paramBigDecimal":10}}'                         | 'nested.paramBigDecimal: is not a valid big-decimal'         |
      | 'DELETE' | '/paramBigDecimal'     | '{"nested":{"paramBigDecimal":10}}'                         | 'nested.paramBigDecimal: is not a valid big-decimal'         |
      | 'POST'   | '/paramOffsetDateTime' | '{"nested":{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}}' | 'nested.paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PUT'    | '/paramOffsetDateTime' | '{"nested":{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}}' | 'nested.paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PATCH'  | '/paramOffsetDateTime' | '{"nested":{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}}' | 'nested.paramOffsetDateTime: is not a valid offset-datetime' |
      | 'DELETE' | '/paramOffsetDateTime' | '{"nested":{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}}' | 'nested.paramOffsetDateTime: is not a valid offset-datetime' |
      | 'POST'   | '/paramLocalDateTime'  | '{"nested":{"paramLocalDateTime":"2100-01-01T00:00:00Z"}}'  | 'nested.paramLocalDateTime: is not a valid local-datetime'   |
      | 'PUT'    | '/paramLocalDateTime'  | '{"nested":{"paramLocalDateTime":"2100-01-01T00:00:00Z"}}'  | 'nested.paramLocalDateTime: is not a valid local-datetime'   |
      | 'PATCH'  | '/paramLocalDateTime'  | '{"nested":{"paramLocalDateTime":"2100-01-01T00:00:00Z"}}'  | 'nested.paramLocalDateTime: is not a valid local-datetime'   |
      | 'DELETE' | '/paramLocalDateTime'  | '{"nested":{"paramLocalDateTime":"2100-01-01T00:00:00Z"}}'  | 'nested.paramLocalDateTime: is not a valid local-datetime'   |
      | 'POST'   | '/paramLocalDate'      | '{"nested":{"paramLocalDate":"2100-01-01"}}'                | 'nested.paramLocalDate: is not a valid date'                 |
      | 'PUT'    | '/paramLocalDate'      | '{"nested":{"paramLocalDate":"2100-01-01"}}'                | 'nested.paramLocalDate: is not a valid date'                 |
      | 'PATCH'  | '/paramLocalDate'      | '{"nested":{"paramLocalDate":"2100-01-01"}}'                | 'nested.paramLocalDate: is not a valid date'                 |
      | 'DELETE' | '/paramLocalDate'      | '{"nested":{"paramLocalDate":"2100-01-01"}}'                | 'nested.paramLocalDate: is not a valid date'                 |
      | 'POST'   | '/paramYear'           | '{"nested":{"paramYear":"2100"}}'                           | 'nested.paramYear: is not a valid year'                      |
      | 'PUT'    | '/paramYear'           | '{"nested":{"paramYear":"2100"}}'                           | 'nested.paramYear: is not a valid year'                      |
      | 'PATCH'  | '/paramYear'           | '{"nested":{"paramYear":"2100"}}'                           | 'nested.paramYear: is not a valid year'                      |
      | 'DELETE' | '/paramYear'           | '{"nested":{"paramYear":"2100"}}'                           | 'nested.paramYear: is not a valid year'                      |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/i18n-constrained/named-nested-dto-attribute<path> endpoint with the body <request body> (RequestBodyConstrainedNotNullRestApi.class)
    this endpoint receives the request body-attibute in a nested dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
    and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/request-body/i18n-constrained/named-nested-dto-attribute'<path>
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | request body                                                   | message                                                      |
      | 'POST'   | '/paramString'         | '{"nested":{"param-string":"ab"}}'                             | 'nested.paramString: is not a valid string'                  |
      | 'PUT'    | '/paramString'         | '{"nested":{"param-string":"ab"}}'                             | 'nested.paramString: is not a valid string'                  |
      | 'PATCH'  | '/paramString'         | '{"nested":{"param-string":"ab"}}'                             | 'nested.paramString: is not a valid string'                  |
      | 'DELETE' | '/paramString'         | '{"nested":{"param-string":"ab"}}'                             | 'nested.paramString: is not a valid string'                  |
      | 'POST'   | '/paramInteger'        | '{"nested":{"param-integer":10}}'                              | 'nested.paramInteger: is not a valid integer'                |
      | 'PUT'    | '/paramInteger'        | '{"nested":{"param-integer":10}}'                              | 'nested.paramInteger: is not a valid integer'                |
      | 'PATCH'  | '/paramInteger'        | '{"nested":{"param-integer":10}}'                              | 'nested.paramInteger: is not a valid integer'                |
      | 'DELETE' | '/paramInteger'        | '{"nested":{"param-integer":10}}'                              | 'nested.paramInteger: is not a valid integer'                |
      | 'POST'   | '/paramLong'           | '{"nested":{"param-long":10}}'                                 | 'nested.paramLong: is not a valid long'                      |
      | 'PUT'    | '/paramLong'           | '{"nested":{"param-long":10}}'                                 | 'nested.paramLong: is not a valid long'                      |
      | 'PATCH'  | '/paramLong'           | '{"nested":{"param-long":10}}'                                 | 'nested.paramLong: is not a valid long'                      |
      | 'DELETE' | '/paramLong'           | '{"nested":{"param-long":10}}'                                 | 'nested.paramLong: is not a valid long'                      |
      | 'POST'   | '/paramBigInteger'     | '{"nested":{"param-big-integer":10}}'                          | 'nested.paramBigInteger: is not a valid big-integer'         |
      | 'PUT'    | '/paramBigInteger'     | '{"nested":{"param-big-integer":10}}'                          | 'nested.paramBigInteger: is not a valid big-integer'         |
      | 'PATCH'  | '/paramBigInteger'     | '{"nested":{"param-big-integer":10}}'                          | 'nested.paramBigInteger: is not a valid big-integer'         |
      | 'DELETE' | '/paramBigInteger'     | '{"nested":{"param-big-integer":10}}'                          | 'nested.paramBigInteger: is not a valid big-integer'         |
      | 'POST'   | '/paramBigDecimal'     | '{"nested":{"param-big-decimal":10}}'                          | 'nested.paramBigDecimal: is not a valid big-decimal'         |
      | 'PUT'    | '/paramBigDecimal'     | '{"nested":{"param-big-decimal":10}}'                          | 'nested.paramBigDecimal: is not a valid big-decimal'         |
      | 'PATCH'  | '/paramBigDecimal'     | '{"nested":{"param-big-decimal":10}}'                          | 'nested.paramBigDecimal: is not a valid big-decimal'         |
      | 'DELETE' | '/paramBigDecimal'     | '{"nested":{"param-big-decimal":10}}'                          | 'nested.paramBigDecimal: is not a valid big-decimal'         |
      | 'POST'   | '/paramOffsetDateTime' | '{"nested":{"param-offset-date-time":"2100-01-01T00:00:00Z"}}' | 'nested.paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PUT'    | '/paramOffsetDateTime' | '{"nested":{"param-offset-date-time":"2100-01-01T00:00:00Z"}}' | 'nested.paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PATCH'  | '/paramOffsetDateTime' | '{"nested":{"param-offset-date-time":"2100-01-01T00:00:00Z"}}' | 'nested.paramOffsetDateTime: is not a valid offset-datetime' |
      | 'DELETE' | '/paramOffsetDateTime' | '{"nested":{"param-offset-date-time":"2100-01-01T00:00:00Z"}}' | 'nested.paramOffsetDateTime: is not a valid offset-datetime' |
      | 'POST'   | '/paramLocalDateTime'  | '{"nested":{"param-local-date-time":"2100-01-01T00:00:00Z"}}'  | 'nested.paramLocalDateTime: is not a valid local-datetime'   |
      | 'PUT'    | '/paramLocalDateTime'  | '{"nested":{"param-local-date-time":"2100-01-01T00:00:00Z"}}'  | 'nested.paramLocalDateTime: is not a valid local-datetime'   |
      | 'PATCH'  | '/paramLocalDateTime'  | '{"nested":{"param-local-date-time":"2100-01-01T00:00:00Z"}}'  | 'nested.paramLocalDateTime: is not a valid local-datetime'   |
      | 'DELETE' | '/paramLocalDateTime'  | '{"nested":{"param-local-date-time":"2100-01-01T00:00:00Z"}}'  | 'nested.paramLocalDateTime: is not a valid local-datetime'   |
      | 'POST'   | '/paramLocalDate'      | '{"nested":{"param-local-date":"2100-01-01"}}'                 | 'nested.paramLocalDate: is not a valid date'                 |
      | 'PUT'    | '/paramLocalDate'      | '{"nested":{"param-local-date":"2100-01-01"}}'                 | 'nested.paramLocalDate: is not a valid date'                 |
      | 'PATCH'  | '/paramLocalDate'      | '{"nested":{"param-local-date":"2100-01-01"}}'                 | 'nested.paramLocalDate: is not a valid date'                 |
      | 'DELETE' | '/paramLocalDate'      | '{"nested":{"param-local-date":"2100-01-01"}}'                 | 'nested.paramLocalDate: is not a valid date'                 |
      | 'POST'   | '/paramYear'           | '{"nested":{"param-year":"2100"}}'                             | 'nested.paramYear: is not a valid year'                      |
      | 'PUT'    | '/paramYear'           | '{"nested":{"param-year":"2100"}}'                             | 'nested.paramYear: is not a valid year'                      |
      | 'PATCH'  | '/paramYear'           | '{"nested":{"param-year":"2100"}}'                             | 'nested.paramYear: is not a valid year'                      |
      | 'DELETE' | '/paramYear'           | '{"nested":{"param-year":"2100"}}'                             | 'nested.paramYear: is not a valid year'                      |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/i18n-constrained/nested-optional-dto-attribute<path> endpoint with the body <request body> (RequestBodyConstrainedNotNullRestApi.class)
     this endpoint receives the request body-attibute in a nested optional dto attribute, both having the same name
     and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/request-body/i18n-constrained/nested-optional-dto-attribute'<path>
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | request body                                                | message                                                      |
      | 'POST'   | '/paramString'         | '{"nested":{"paramString":"ab"}}'                           | 'nested.paramString: is not a valid string'                  |
      | 'PUT'    | '/paramString'         | '{"nested":{"paramString":"ab"}}'                           | 'nested.paramString: is not a valid string'                  |
      | 'PATCH'  | '/paramString'         | '{"nested":{"paramString":"ab"}}'                           | 'nested.paramString: is not a valid string'                  |
      | 'DELETE' | '/paramString'         | '{"nested":{"paramString":"ab"}}'                           | 'nested.paramString: is not a valid string'                  |
      | 'POST'   | '/paramInteger'        | '{"nested":{"paramInteger":10}}'                            | 'nested.paramInteger: is not a valid integer'                |
      | 'PUT'    | '/paramInteger'        | '{"nested":{"paramInteger":10}}'                            | 'nested.paramInteger: is not a valid integer'                |
      | 'PATCH'  | '/paramInteger'        | '{"nested":{"paramInteger":10}}'                            | 'nested.paramInteger: is not a valid integer'                |
      | 'DELETE' | '/paramInteger'        | '{"nested":{"paramInteger":10}}'                            | 'nested.paramInteger: is not a valid integer'                |
      | 'POST'   | '/paramLong'           | '{"nested":{"paramLong":10}}'                               | 'nested.paramLong: is not a valid long'                      |
      | 'PUT'    | '/paramLong'           | '{"nested":{"paramLong":10}}'                               | 'nested.paramLong: is not a valid long'                      |
      | 'PATCH'  | '/paramLong'           | '{"nested":{"paramLong":10}}'                               | 'nested.paramLong: is not a valid long'                      |
      | 'DELETE' | '/paramLong'           | '{"nested":{"paramLong":10}}'                               | 'nested.paramLong: is not a valid long'                      |
      | 'POST'   | '/paramBigInteger'     | '{"nested":{"paramBigInteger":10}}'                         | 'nested.paramBigInteger: is not a valid big-integer'         |
      | 'PUT'    | '/paramBigInteger'     | '{"nested":{"paramBigInteger":10}}'                         | 'nested.paramBigInteger: is not a valid big-integer'         |
      | 'PATCH'  | '/paramBigInteger'     | '{"nested":{"paramBigInteger":10}}'                         | 'nested.paramBigInteger: is not a valid big-integer'         |
      | 'DELETE' | '/paramBigInteger'     | '{"nested":{"paramBigInteger":10}}'                         | 'nested.paramBigInteger: is not a valid big-integer'         |
      | 'POST'   | '/paramBigDecimal'     | '{"nested":{"paramBigDecimal":10}}'                         | 'nested.paramBigDecimal: is not a valid big-decimal'         |
      | 'PUT'    | '/paramBigDecimal'     | '{"nested":{"paramBigDecimal":10}}'                         | 'nested.paramBigDecimal: is not a valid big-decimal'         |
      | 'PATCH'  | '/paramBigDecimal'     | '{"nested":{"paramBigDecimal":10}}'                         | 'nested.paramBigDecimal: is not a valid big-decimal'         |
      | 'DELETE' | '/paramBigDecimal'     | '{"nested":{"paramBigDecimal":10}}'                         | 'nested.paramBigDecimal: is not a valid big-decimal'         |
      | 'POST'   | '/paramOffsetDateTime' | '{"nested":{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}}' | 'nested.paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PUT'    | '/paramOffsetDateTime' | '{"nested":{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}}' | 'nested.paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PATCH'  | '/paramOffsetDateTime' | '{"nested":{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}}' | 'nested.paramOffsetDateTime: is not a valid offset-datetime' |
      | 'DELETE' | '/paramOffsetDateTime' | '{"nested":{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}}' | 'nested.paramOffsetDateTime: is not a valid offset-datetime' |
      | 'POST'   | '/paramLocalDateTime'  | '{"nested":{"paramLocalDateTime":"2100-01-01T00:00:00Z"}}'  | 'nested.paramLocalDateTime: is not a valid local-datetime'   |
      | 'PUT'    | '/paramLocalDateTime'  | '{"nested":{"paramLocalDateTime":"2100-01-01T00:00:00Z"}}'  | 'nested.paramLocalDateTime: is not a valid local-datetime'   |
      | 'PATCH'  | '/paramLocalDateTime'  | '{"nested":{"paramLocalDateTime":"2100-01-01T00:00:00Z"}}'  | 'nested.paramLocalDateTime: is not a valid local-datetime'   |
      | 'DELETE' | '/paramLocalDateTime'  | '{"nested":{"paramLocalDateTime":"2100-01-01T00:00:00Z"}}'  | 'nested.paramLocalDateTime: is not a valid local-datetime'   |
      | 'POST'   | '/paramLocalDate'      | '{"nested":{"paramLocalDate":"2100-01-01"}}'                | 'nested.paramLocalDate: is not a valid date'                 |
      | 'PUT'    | '/paramLocalDate'      | '{"nested":{"paramLocalDate":"2100-01-01"}}'                | 'nested.paramLocalDate: is not a valid date'                 |
      | 'PATCH'  | '/paramLocalDate'      | '{"nested":{"paramLocalDate":"2100-01-01"}}'                | 'nested.paramLocalDate: is not a valid date'                 |
      | 'DELETE' | '/paramLocalDate'      | '{"nested":{"paramLocalDate":"2100-01-01"}}'                | 'nested.paramLocalDate: is not a valid date'                 |
      | 'POST'   | '/paramYear'           | '{"nested":{"paramYear":"2100"}}'                           | 'nested.paramYear: is not a valid year'                      |
      | 'PUT'    | '/paramYear'           | '{"nested":{"paramYear":"2100"}}'                           | 'nested.paramYear: is not a valid year'                      |
      | 'PATCH'  | '/paramYear'           | '{"nested":{"paramYear":"2100"}}'                           | 'nested.paramYear: is not a valid year'                      |
      | 'DELETE' | '/paramYear'           | '{"nested":{"paramYear":"2100"}}'                           | 'nested.paramYear: is not a valid year'                      |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/i18n-constrained/named-nested-optional-dto-attribute<path> endpoint with the body <request body> (RequestBodyConstrainedNotNullRestApi.class)
    this endpoint receives the request body-attibute in a nested optional dto attribute, both having the diferent names, a custom body-attibute name is defined by the endpoint
    and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/request-body/i18n-constrained/named-nested-optional-dto-attribute'<path>
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | request body                                                   | message                                                      |
      | 'POST'   | '/paramString'         | '{"nested":{"param-string":"ab"}}'                             | 'nested.paramString: is not a valid string'                  |
      | 'PUT'    | '/paramString'         | '{"nested":{"param-string":"ab"}}'                             | 'nested.paramString: is not a valid string'                  |
      | 'PATCH'  | '/paramString'         | '{"nested":{"param-string":"ab"}}'                             | 'nested.paramString: is not a valid string'                  |
      | 'DELETE' | '/paramString'         | '{"nested":{"param-string":"ab"}}'                             | 'nested.paramString: is not a valid string'                  |
      | 'POST'   | '/paramInteger'        | '{"nested":{"param-integer":10}}'                              | 'nested.paramInteger: is not a valid integer'                |
      | 'PUT'    | '/paramInteger'        | '{"nested":{"param-integer":10}}'                              | 'nested.paramInteger: is not a valid integer'                |
      | 'PATCH'  | '/paramInteger'        | '{"nested":{"param-integer":10}}'                              | 'nested.paramInteger: is not a valid integer'                |
      | 'DELETE' | '/paramInteger'        | '{"nested":{"param-integer":10}}'                              | 'nested.paramInteger: is not a valid integer'                |
      | 'POST'   | '/paramLong'           | '{"nested":{"param-long":10}}'                                 | 'nested.paramLong: is not a valid long'                      |
      | 'PUT'    | '/paramLong'           | '{"nested":{"param-long":10}}'                                 | 'nested.paramLong: is not a valid long'                      |
      | 'PATCH'  | '/paramLong'           | '{"nested":{"param-long":10}}'                                 | 'nested.paramLong: is not a valid long'                      |
      | 'DELETE' | '/paramLong'           | '{"nested":{"param-long":10}}'                                 | 'nested.paramLong: is not a valid long'                      |
      | 'POST'   | '/paramBigInteger'     | '{"nested":{"param-big-integer":10}}'                          | 'nested.paramBigInteger: is not a valid big-integer'         |
      | 'PUT'    | '/paramBigInteger'     | '{"nested":{"param-big-integer":10}}'                          | 'nested.paramBigInteger: is not a valid big-integer'         |
      | 'PATCH'  | '/paramBigInteger'     | '{"nested":{"param-big-integer":10}}'                          | 'nested.paramBigInteger: is not a valid big-integer'         |
      | 'DELETE' | '/paramBigInteger'     | '{"nested":{"param-big-integer":10}}'                          | 'nested.paramBigInteger: is not a valid big-integer'         |
      | 'POST'   | '/paramBigDecimal'     | '{"nested":{"param-big-decimal":10}}'                          | 'nested.paramBigDecimal: is not a valid big-decimal'         |
      | 'PUT'    | '/paramBigDecimal'     | '{"nested":{"param-big-decimal":10}}'                          | 'nested.paramBigDecimal: is not a valid big-decimal'         |
      | 'PATCH'  | '/paramBigDecimal'     | '{"nested":{"param-big-decimal":10}}'                          | 'nested.paramBigDecimal: is not a valid big-decimal'         |
      | 'DELETE' | '/paramBigDecimal'     | '{"nested":{"param-big-decimal":10}}'                          | 'nested.paramBigDecimal: is not a valid big-decimal'         |
      | 'POST'   | '/paramOffsetDateTime' | '{"nested":{"param-offset-date-time":"2100-01-01T00:00:00Z"}}' | 'nested.paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PUT'    | '/paramOffsetDateTime' | '{"nested":{"param-offset-date-time":"2100-01-01T00:00:00Z"}}' | 'nested.paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PATCH'  | '/paramOffsetDateTime' | '{"nested":{"param-offset-date-time":"2100-01-01T00:00:00Z"}}' | 'nested.paramOffsetDateTime: is not a valid offset-datetime' |
      | 'DELETE' | '/paramOffsetDateTime' | '{"nested":{"param-offset-date-time":"2100-01-01T00:00:00Z"}}' | 'nested.paramOffsetDateTime: is not a valid offset-datetime' |
      | 'POST'   | '/paramLocalDateTime'  | '{"nested":{"param-local-date-time":"2100-01-01T00:00:00Z"}}'  | 'nested.paramLocalDateTime: is not a valid local-datetime'   |
      | 'PUT'    | '/paramLocalDateTime'  | '{"nested":{"param-local-date-time":"2100-01-01T00:00:00Z"}}'  | 'nested.paramLocalDateTime: is not a valid local-datetime'   |
      | 'PATCH'  | '/paramLocalDateTime'  | '{"nested":{"param-local-date-time":"2100-01-01T00:00:00Z"}}'  | 'nested.paramLocalDateTime: is not a valid local-datetime'   |
      | 'DELETE' | '/paramLocalDateTime'  | '{"nested":{"param-local-date-time":"2100-01-01T00:00:00Z"}}'  | 'nested.paramLocalDateTime: is not a valid local-datetime'   |
      | 'POST'   | '/paramLocalDate'      | '{"nested":{"param-local-date":"2100-01-01"}}'                 | 'nested.paramLocalDate: is not a valid date'                 |
      | 'PUT'    | '/paramLocalDate'      | '{"nested":{"param-local-date":"2100-01-01"}}'                 | 'nested.paramLocalDate: is not a valid date'                 |
      | 'PATCH'  | '/paramLocalDate'      | '{"nested":{"param-local-date":"2100-01-01"}}'                 | 'nested.paramLocalDate: is not a valid date'                 |
      | 'DELETE' | '/paramLocalDate'      | '{"nested":{"param-local-date":"2100-01-01"}}'                 | 'nested.paramLocalDate: is not a valid date'                 |
      | 'POST'   | '/paramYear'           | '{"nested":{"param-year":"2100"}}'                             | 'nested.paramYear: is not a valid year'                      |
      | 'PUT'    | '/paramYear'           | '{"nested":{"param-year":"2100"}}'                             | 'nested.paramYear: is not a valid year'                      |
      | 'PATCH'  | '/paramYear'           | '{"nested":{"param-year":"2100"}}'                             | 'nested.paramYear: is not a valid year'                      |
      | 'DELETE' | '/paramYear'           | '{"nested":{"param-year":"2100"}}'                             | 'nested.paramYear: is not a valid year'                      |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/i18n-constrained/nested-dto-attribute<path> endpoint with the body <request body> (RequestBodyConstrainedNotNullRestApi.class)
    this endpoint receives the request body-attibute in a nested dto attribute from an array, both having the same name
    and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/request-body/i18n-constrained/nested-dto-attribute'<path>
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | request body                                                 | message                                                        |
      | 'POST'   | '/paramString'         | '{"array":[{"paramString":"ab"}]}'                           | 'array[0].paramString: is not a valid string'                  |
      | 'PUT'    | '/paramString'         | '{"array":[{"paramString":"ab"}]}'                           | 'array[0].paramString: is not a valid string'                  |
      | 'PATCH'  | '/paramString'         | '{"array":[{"paramString":"ab"}]}'                           | 'array[0].paramString: is not a valid string'                  |
      | 'DELETE' | '/paramString'         | '{"array":[{"paramString":"ab"}]}'                           | 'array[0].paramString: is not a valid string'                  |
      | 'POST'   | '/paramInteger'        | '{"array":[{"paramInteger":10}]}'                            | 'array[0].paramInteger: is not a valid integer'                |
      | 'PUT'    | '/paramInteger'        | '{"array":[{"paramInteger":10}]}'                            | 'array[0].paramInteger: is not a valid integer'                |
      | 'PATCH'  | '/paramInteger'        | '{"array":[{"paramInteger":10}]}'                            | 'array[0].paramInteger: is not a valid integer'                |
      | 'DELETE' | '/paramInteger'        | '{"array":[{"paramInteger":10}]}'                            | 'array[0].paramInteger: is not a valid integer'                |
      | 'POST'   | '/paramLong'           | '{"array":[{"paramLong":10}]}'                               | 'array[0].paramLong: is not a valid long'                      |
      | 'PUT'    | '/paramLong'           | '{"array":[{"paramLong":10}]}'                               | 'array[0].paramLong: is not a valid long'                      |
      | 'PATCH'  | '/paramLong'           | '{"array":[{"paramLong":10}]}'                               | 'array[0].paramLong: is not a valid long'                      |
      | 'DELETE' | '/paramLong'           | '{"array":[{"paramLong":10}]}'                               | 'array[0].paramLong: is not a valid long'                      |
      | 'POST'   | '/paramBigInteger'     | '{"array":[{"paramBigInteger":10}]}'                         | 'array[0].paramBigInteger: is not a valid big-integer'         |
      | 'PUT'    | '/paramBigInteger'     | '{"array":[{"paramBigInteger":10}]}'                         | 'array[0].paramBigInteger: is not a valid big-integer'         |
      | 'PATCH'  | '/paramBigInteger'     | '{"array":[{"paramBigInteger":10}]}'                         | 'array[0].paramBigInteger: is not a valid big-integer'         |
      | 'DELETE' | '/paramBigInteger'     | '{"array":[{"paramBigInteger":10}]}'                         | 'array[0].paramBigInteger: is not a valid big-integer'         |
      | 'POST'   | '/paramBigDecimal'     | '{"array":[{"paramBigDecimal":10}]}'                         | 'array[0].paramBigDecimal: is not a valid big-decimal'         |
      | 'PUT'    | '/paramBigDecimal'     | '{"array":[{"paramBigDecimal":10}]}'                         | 'array[0].paramBigDecimal: is not a valid big-decimal'         |
      | 'PATCH'  | '/paramBigDecimal'     | '{"array":[{"paramBigDecimal":10}]}'                         | 'array[0].paramBigDecimal: is not a valid big-decimal'         |
      | 'DELETE' | '/paramBigDecimal'     | '{"array":[{"paramBigDecimal":10}]}'                         | 'array[0].paramBigDecimal: is not a valid big-decimal'         |
      | 'POST'   | '/paramOffsetDateTime' | '{"array":[{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}]}' | 'array[0].paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PUT'    | '/paramOffsetDateTime' | '{"array":[{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}]}' | 'array[0].paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PATCH'  | '/paramOffsetDateTime' | '{"array":[{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}]}' | 'array[0].paramOffsetDateTime: is not a valid offset-datetime' |
      | 'DELETE' | '/paramOffsetDateTime' | '{"array":[{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}]}' | 'array[0].paramOffsetDateTime: is not a valid offset-datetime' |
      | 'POST'   | '/paramLocalDateTime'  | '{"array":[{"paramLocalDateTime":"2100-01-01T00:00:00Z"}]}'  | 'array[0].paramLocalDateTime: is not a valid local-datetime'   |
      | 'PUT'    | '/paramLocalDateTime'  | '{"array":[{"paramLocalDateTime":"2100-01-01T00:00:00Z"}]}'  | 'array[0].paramLocalDateTime: is not a valid local-datetime'   |
      | 'PATCH'  | '/paramLocalDateTime'  | '{"array":[{"paramLocalDateTime":"2100-01-01T00:00:00Z"}]}'  | 'array[0].paramLocalDateTime: is not a valid local-datetime'   |
      | 'DELETE' | '/paramLocalDateTime'  | '{"array":[{"paramLocalDateTime":"2100-01-01T00:00:00Z"}]}'  | 'array[0].paramLocalDateTime: is not a valid local-datetime'   |
      | 'POST'   | '/paramLocalDate'      | '{"array":[{"paramLocalDate":"2100-01-01"}]}'                | 'array[0].paramLocalDate: is not a valid date'                 |
      | 'PUT'    | '/paramLocalDate'      | '{"array":[{"paramLocalDate":"2100-01-01"}]}'                | 'array[0].paramLocalDate: is not a valid date'                 |
      | 'PATCH'  | '/paramLocalDate'      | '{"array":[{"paramLocalDate":"2100-01-01"}]}'                | 'array[0].paramLocalDate: is not a valid date'                 |
      | 'DELETE' | '/paramLocalDate'      | '{"array":[{"paramLocalDate":"2100-01-01"}]}'                | 'array[0].paramLocalDate: is not a valid date'                 |
      | 'POST'   | '/paramYear'           | '{"array":[{"paramYear":"2100"}]}'                           | 'array[0].paramYear: is not a valid year'                      |
      | 'PUT'    | '/paramYear'           | '{"array":[{"paramYear":"2100"}]}'                           | 'array[0].paramYear: is not a valid year'                      |
      | 'PATCH'  | '/paramYear'           | '{"array":[{"paramYear":"2100"}]}'                           | 'array[0].paramYear: is not a valid year'                      |
      | 'DELETE' | '/paramYear'           | '{"array":[{"paramYear":"2100"}]}'                           | 'array[0].paramYear: is not a valid year'                      |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/i18n-constrained/named-nested-dto-attribute<path> endpoint with the body <request body> (RequestBodyConstrainedNotNullRestApi.class)
    this endpoint receives the request body-attibute in a nested dto attribute from an array, both having the diferent names, a custom body-attibute name is defined by the endpoint
    and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/request-body/i18n-constrained/named-nested-dto-attribute'<path>
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | request body                                                    | message                                                        |
      | 'POST'   | '/paramString'         | '{"array":[{"param-string":"ab"}]}'                             | 'array[0].paramString: is not a valid string'                  |
      | 'PUT'    | '/paramString'         | '{"array":[{"param-string":"ab"}]}'                             | 'array[0].paramString: is not a valid string'                  |
      | 'PATCH'  | '/paramString'         | '{"array":[{"param-string":"ab"}]}'                             | 'array[0].paramString: is not a valid string'                  |
      | 'DELETE' | '/paramString'         | '{"array":[{"param-string":"ab"}]}'                             | 'array[0].paramString: is not a valid string'                  |
      | 'POST'   | '/paramInteger'        | '{"array":[{"param-integer":10}]}'                              | 'array[0].paramInteger: is not a valid integer'                |
      | 'PUT'    | '/paramInteger'        | '{"array":[{"param-integer":10}]}'                              | 'array[0].paramInteger: is not a valid integer'                |
      | 'PATCH'  | '/paramInteger'        | '{"array":[{"param-integer":10}]}'                              | 'array[0].paramInteger: is not a valid integer'                |
      | 'DELETE' | '/paramInteger'        | '{"array":[{"param-integer":10}]}'                              | 'array[0].paramInteger: is not a valid integer'                |
      | 'POST'   | '/paramLong'           | '{"array":[{"param-long":10}]}'                                 | 'array[0].paramLong: is not a valid long'                      |
      | 'PUT'    | '/paramLong'           | '{"array":[{"param-long":10}]}'                                 | 'array[0].paramLong: is not a valid long'                      |
      | 'PATCH'  | '/paramLong'           | '{"array":[{"param-long":10}]}'                                 | 'array[0].paramLong: is not a valid long'                      |
      | 'DELETE' | '/paramLong'           | '{"array":[{"param-long":10}]}'                                 | 'array[0].paramLong: is not a valid long'                      |
      | 'POST'   | '/paramBigInteger'     | '{"array":[{"param-big-integer":10}]}'                          | 'array[0].paramBigInteger: is not a valid big-integer'         |
      | 'PUT'    | '/paramBigInteger'     | '{"array":[{"param-big-integer":10}]}'                          | 'array[0].paramBigInteger: is not a valid big-integer'         |
      | 'PATCH'  | '/paramBigInteger'     | '{"array":[{"param-big-integer":10}]}'                          | 'array[0].paramBigInteger: is not a valid big-integer'         |
      | 'DELETE' | '/paramBigInteger'     | '{"array":[{"param-big-integer":10}]}'                          | 'array[0].paramBigInteger: is not a valid big-integer'         |
      | 'POST'   | '/paramBigDecimal'     | '{"array":[{"param-big-decimal":10}]}'                          | 'array[0].paramBigDecimal: is not a valid big-decimal'         |
      | 'PUT'    | '/paramBigDecimal'     | '{"array":[{"param-big-decimal":10}]}'                          | 'array[0].paramBigDecimal: is not a valid big-decimal'         |
      | 'PATCH'  | '/paramBigDecimal'     | '{"array":[{"param-big-decimal":10}]}'                          | 'array[0].paramBigDecimal: is not a valid big-decimal'         |
      | 'DELETE' | '/paramBigDecimal'     | '{"array":[{"param-big-decimal":10}]}'                          | 'array[0].paramBigDecimal: is not a valid big-decimal'         |
      | 'POST'   | '/paramOffsetDateTime' | '{"array":[{"param-offset-date-time":"2100-01-01T00:00:00Z"}]}' | 'array[0].paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PUT'    | '/paramOffsetDateTime' | '{"array":[{"param-offset-date-time":"2100-01-01T00:00:00Z"}]}' | 'array[0].paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PATCH'  | '/paramOffsetDateTime' | '{"array":[{"param-offset-date-time":"2100-01-01T00:00:00Z"}]}' | 'array[0].paramOffsetDateTime: is not a valid offset-datetime' |
      | 'DELETE' | '/paramOffsetDateTime' | '{"array":[{"param-offset-date-time":"2100-01-01T00:00:00Z"}]}' | 'array[0].paramOffsetDateTime: is not a valid offset-datetime' |
      | 'POST'   | '/paramLocalDateTime'  | '{"array":[{"param-local-date-time":"2100-01-01T00:00:00Z"}]}'  | 'array[0].paramLocalDateTime: is not a valid local-datetime'   |
      | 'PUT'    | '/paramLocalDateTime'  | '{"array":[{"param-local-date-time":"2100-01-01T00:00:00Z"}]}'  | 'array[0].paramLocalDateTime: is not a valid local-datetime'   |
      | 'PATCH'  | '/paramLocalDateTime'  | '{"array":[{"param-local-date-time":"2100-01-01T00:00:00Z"}]}'  | 'array[0].paramLocalDateTime: is not a valid local-datetime'   |
      | 'DELETE' | '/paramLocalDateTime'  | '{"array":[{"param-local-date-time":"2100-01-01T00:00:00Z"}]}'  | 'array[0].paramLocalDateTime: is not a valid local-datetime'   |
      | 'POST'   | '/paramLocalDate'      | '{"array":[{"param-local-date":"2100-01-01"}]}'                 | 'array[0].paramLocalDate: is not a valid date'                 |
      | 'PUT'    | '/paramLocalDate'      | '{"array":[{"param-local-date":"2100-01-01"}]}'                 | 'array[0].paramLocalDate: is not a valid date'                 |
      | 'PATCH'  | '/paramLocalDate'      | '{"array":[{"param-local-date":"2100-01-01"}]}'                 | 'array[0].paramLocalDate: is not a valid date'                 |
      | 'DELETE' | '/paramLocalDate'      | '{"array":[{"param-local-date":"2100-01-01"}]}'                 | 'array[0].paramLocalDate: is not a valid date'                 |
      | 'POST'   | '/paramYear'           | '{"array":[{"param-year":"2100"}]}'                             | 'array[0].paramYear: is not a valid year'                      |
      | 'PUT'    | '/paramYear'           | '{"array":[{"param-year":"2100"}]}'                             | 'array[0].paramYear: is not a valid year'                      |
      | 'PATCH'  | '/paramYear'           | '{"array":[{"param-year":"2100"}]}'                             | 'array[0].paramYear: is not a valid year'                      |
      | 'DELETE' | '/paramYear'           | '{"array":[{"param-year":"2100"}]}'                             | 'array[0].paramYear: is not a valid year'                      |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/i18n-constrained/nested-optional-dto-attribute<path> endpoint with the body <request body> (RequestBodyConstrainedNotNullRestApi.class)
    this endpoint receives the request body-attibute in a nested optional dto attribute from an array, both having the same name
    and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/request-body/i18n-constrained/nested-optional-dto-attribute'<path>
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | request body                                                 | message                                                        |
      | 'POST'   | '/paramString'         | '{"array":[{"paramString":"ab"}]}'                           | 'array[0].paramString: is not a valid string'                  |
      | 'PUT'    | '/paramString'         | '{"array":[{"paramString":"ab"}]}'                           | 'array[0].paramString: is not a valid string'                  |
      | 'PATCH'  | '/paramString'         | '{"array":[{"paramString":"ab"}]}'                           | 'array[0].paramString: is not a valid string'                  |
      | 'DELETE' | '/paramString'         | '{"array":[{"paramString":"ab"}]}'                           | 'array[0].paramString: is not a valid string'                  |
      | 'POST'   | '/paramInteger'        | '{"array":[{"paramInteger":10}]}'                            | 'array[0].paramInteger: is not a valid integer'                |
      | 'PUT'    | '/paramInteger'        | '{"array":[{"paramInteger":10}]}'                            | 'array[0].paramInteger: is not a valid integer'                |
      | 'PATCH'  | '/paramInteger'        | '{"array":[{"paramInteger":10}]}'                            | 'array[0].paramInteger: is not a valid integer'                |
      | 'DELETE' | '/paramInteger'        | '{"array":[{"paramInteger":10}]}'                            | 'array[0].paramInteger: is not a valid integer'                |
      | 'POST'   | '/paramLong'           | '{"array":[{"paramLong":10}]}'                               | 'array[0].paramLong: is not a valid long'                      |
      | 'PUT'    | '/paramLong'           | '{"array":[{"paramLong":10}]}'                               | 'array[0].paramLong: is not a valid long'                      |
      | 'PATCH'  | '/paramLong'           | '{"array":[{"paramLong":10}]}'                               | 'array[0].paramLong: is not a valid long'                      |
      | 'DELETE' | '/paramLong'           | '{"array":[{"paramLong":10}]}'                               | 'array[0].paramLong: is not a valid long'                      |
      | 'POST'   | '/paramBigInteger'     | '{"array":[{"paramBigInteger":10}]}'                         | 'array[0].paramBigInteger: is not a valid big-integer'         |
      | 'PUT'    | '/paramBigInteger'     | '{"array":[{"paramBigInteger":10}]}'                         | 'array[0].paramBigInteger: is not a valid big-integer'         |
      | 'PATCH'  | '/paramBigInteger'     | '{"array":[{"paramBigInteger":10}]}'                         | 'array[0].paramBigInteger: is not a valid big-integer'         |
      | 'DELETE' | '/paramBigInteger'     | '{"array":[{"paramBigInteger":10}]}'                         | 'array[0].paramBigInteger: is not a valid big-integer'         |
      | 'POST'   | '/paramBigDecimal'     | '{"array":[{"paramBigDecimal":10}]}'                         | 'array[0].paramBigDecimal: is not a valid big-decimal'         |
      | 'PUT'    | '/paramBigDecimal'     | '{"array":[{"paramBigDecimal":10}]}'                         | 'array[0].paramBigDecimal: is not a valid big-decimal'         |
      | 'PATCH'  | '/paramBigDecimal'     | '{"array":[{"paramBigDecimal":10}]}'                         | 'array[0].paramBigDecimal: is not a valid big-decimal'         |
      | 'DELETE' | '/paramBigDecimal'     | '{"array":[{"paramBigDecimal":10}]}'                         | 'array[0].paramBigDecimal: is not a valid big-decimal'         |
      | 'POST'   | '/paramOffsetDateTime' | '{"array":[{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}]}' | 'array[0].paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PUT'    | '/paramOffsetDateTime' | '{"array":[{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}]}' | 'array[0].paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PATCH'  | '/paramOffsetDateTime' | '{"array":[{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}]}' | 'array[0].paramOffsetDateTime: is not a valid offset-datetime' |
      | 'DELETE' | '/paramOffsetDateTime' | '{"array":[{"paramOffsetDateTime":"2100-01-01T00:00:00Z"}]}' | 'array[0].paramOffsetDateTime: is not a valid offset-datetime' |
      | 'POST'   | '/paramLocalDateTime'  | '{"array":[{"paramLocalDateTime":"2100-01-01T00:00:00Z"}]}'  | 'array[0].paramLocalDateTime: is not a valid local-datetime'   |
      | 'PUT'    | '/paramLocalDateTime'  | '{"array":[{"paramLocalDateTime":"2100-01-01T00:00:00Z"}]}'  | 'array[0].paramLocalDateTime: is not a valid local-datetime'   |
      | 'PATCH'  | '/paramLocalDateTime'  | '{"array":[{"paramLocalDateTime":"2100-01-01T00:00:00Z"}]}'  | 'array[0].paramLocalDateTime: is not a valid local-datetime'   |
      | 'DELETE' | '/paramLocalDateTime'  | '{"array":[{"paramLocalDateTime":"2100-01-01T00:00:00Z"}]}'  | 'array[0].paramLocalDateTime: is not a valid local-datetime'   |
      | 'POST'   | '/paramLocalDate'      | '{"array":[{"paramLocalDate":"2100-01-01"}]}'                | 'array[0].paramLocalDate: is not a valid date'                 |
      | 'PUT'    | '/paramLocalDate'      | '{"array":[{"paramLocalDate":"2100-01-01"}]}'                | 'array[0].paramLocalDate: is not a valid date'                 |
      | 'PATCH'  | '/paramLocalDate'      | '{"array":[{"paramLocalDate":"2100-01-01"}]}'                | 'array[0].paramLocalDate: is not a valid date'                 |
      | 'DELETE' | '/paramLocalDate'      | '{"array":[{"paramLocalDate":"2100-01-01"}]}'                | 'array[0].paramLocalDate: is not a valid date'                 |
      | 'POST'   | '/paramYear'           | '{"array":[{"paramYear":"2100"}]}'                           | 'array[0].paramYear: is not a valid year'                      |
      | 'PUT'    | '/paramYear'           | '{"array":[{"paramYear":"2100"}]}'                           | 'array[0].paramYear: is not a valid year'                      |
      | 'PATCH'  | '/paramYear'           | '{"array":[{"paramYear":"2100"}]}'                           | 'array[0].paramYear: is not a valid year'                      |
      | 'DELETE' | '/paramYear'           | '{"array":[{"paramYear":"2100"}]}'                           | 'array[0].paramYear: is not a valid year'                      |

  Scenario Outline: The <method> call is made to the /api/v1/request-body/i18n-constrained/named-nested-optional-dto-attribute<path> endpoint with the body <request body> (RequestBodyConstrainedNotNullRestApi.class)
    this endpoint receives the request body-attibute in a nested optional dto attribute from an array, both having the diferent names, a custom body-attibute name is defined by the endpoint
    and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/request-body/i18n-constrained/named-nested-optional-dto-attribute'<path>
    And the request with the <method> method
    And the request body is <request body>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | request body                                                    | message                                                        |
      | 'POST'   | '/paramString'         | '{"array":[{"param-string":"ab"}]}'                             | 'array[0].paramString: is not a valid string'                  |
      | 'PUT'    | '/paramString'         | '{"array":[{"param-string":"ab"}]}'                             | 'array[0].paramString: is not a valid string'                  |
      | 'PATCH'  | '/paramString'         | '{"array":[{"param-string":"ab"}]}'                             | 'array[0].paramString: is not a valid string'                  |
      | 'DELETE' | '/paramString'         | '{"array":[{"param-string":"ab"}]}'                             | 'array[0].paramString: is not a valid string'                  |
      | 'POST'   | '/paramInteger'        | '{"array":[{"param-integer":10}]}'                              | 'array[0].paramInteger: is not a valid integer'                |
      | 'PUT'    | '/paramInteger'        | '{"array":[{"param-integer":10}]}'                              | 'array[0].paramInteger: is not a valid integer'                |
      | 'PATCH'  | '/paramInteger'        | '{"array":[{"param-integer":10}]}'                              | 'array[0].paramInteger: is not a valid integer'                |
      | 'DELETE' | '/paramInteger'        | '{"array":[{"param-integer":10}]}'                              | 'array[0].paramInteger: is not a valid integer'                |
      | 'POST'   | '/paramLong'           | '{"array":[{"param-long":10}]}'                                 | 'array[0].paramLong: is not a valid long'                      |
      | 'PUT'    | '/paramLong'           | '{"array":[{"param-long":10}]}'                                 | 'array[0].paramLong: is not a valid long'                      |
      | 'PATCH'  | '/paramLong'           | '{"array":[{"param-long":10}]}'                                 | 'array[0].paramLong: is not a valid long'                      |
      | 'DELETE' | '/paramLong'           | '{"array":[{"param-long":10}]}'                                 | 'array[0].paramLong: is not a valid long'                      |
      | 'POST'   | '/paramBigInteger'     | '{"array":[{"param-big-integer":10}]}'                          | 'array[0].paramBigInteger: is not a valid big-integer'         |
      | 'PUT'    | '/paramBigInteger'     | '{"array":[{"param-big-integer":10}]}'                          | 'array[0].paramBigInteger: is not a valid big-integer'         |
      | 'PATCH'  | '/paramBigInteger'     | '{"array":[{"param-big-integer":10}]}'                          | 'array[0].paramBigInteger: is not a valid big-integer'         |
      | 'DELETE' | '/paramBigInteger'     | '{"array":[{"param-big-integer":10}]}'                          | 'array[0].paramBigInteger: is not a valid big-integer'         |
      | 'POST'   | '/paramBigDecimal'     | '{"array":[{"param-big-decimal":10}]}'                          | 'array[0].paramBigDecimal: is not a valid big-decimal'         |
      | 'PUT'    | '/paramBigDecimal'     | '{"array":[{"param-big-decimal":10}]}'                          | 'array[0].paramBigDecimal: is not a valid big-decimal'         |
      | 'PATCH'  | '/paramBigDecimal'     | '{"array":[{"param-big-decimal":10}]}'                          | 'array[0].paramBigDecimal: is not a valid big-decimal'         |
      | 'DELETE' | '/paramBigDecimal'     | '{"array":[{"param-big-decimal":10}]}'                          | 'array[0].paramBigDecimal: is not a valid big-decimal'         |
      | 'POST'   | '/paramOffsetDateTime' | '{"array":[{"param-offset-date-time":"2100-01-01T00:00:00Z"}]}' | 'array[0].paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PUT'    | '/paramOffsetDateTime' | '{"array":[{"param-offset-date-time":"2100-01-01T00:00:00Z"}]}' | 'array[0].paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PATCH'  | '/paramOffsetDateTime' | '{"array":[{"param-offset-date-time":"2100-01-01T00:00:00Z"}]}' | 'array[0].paramOffsetDateTime: is not a valid offset-datetime' |
      | 'DELETE' | '/paramOffsetDateTime' | '{"array":[{"param-offset-date-time":"2100-01-01T00:00:00Z"}]}' | 'array[0].paramOffsetDateTime: is not a valid offset-datetime' |
      | 'POST'   | '/paramLocalDateTime'  | '{"array":[{"param-local-date-time":"2100-01-01T00:00:00Z"}]}'  | 'array[0].paramLocalDateTime: is not a valid local-datetime'   |
      | 'PUT'    | '/paramLocalDateTime'  | '{"array":[{"param-local-date-time":"2100-01-01T00:00:00Z"}]}'  | 'array[0].paramLocalDateTime: is not a valid local-datetime'   |
      | 'PATCH'  | '/paramLocalDateTime'  | '{"array":[{"param-local-date-time":"2100-01-01T00:00:00Z"}]}'  | 'array[0].paramLocalDateTime: is not a valid local-datetime'   |
      | 'DELETE' | '/paramLocalDateTime'  | '{"array":[{"param-local-date-time":"2100-01-01T00:00:00Z"}]}'  | 'array[0].paramLocalDateTime: is not a valid local-datetime'   |
      | 'POST'   | '/paramLocalDate'      | '{"array":[{"param-local-date":"2100-01-01"}]}'                 | 'array[0].paramLocalDate: is not a valid date'                 |
      | 'PUT'    | '/paramLocalDate'      | '{"array":[{"param-local-date":"2100-01-01"}]}'                 | 'array[0].paramLocalDate: is not a valid date'                 |
      | 'PATCH'  | '/paramLocalDate'      | '{"array":[{"param-local-date":"2100-01-01"}]}'                 | 'array[0].paramLocalDate: is not a valid date'                 |
      | 'DELETE' | '/paramLocalDate'      | '{"array":[{"param-local-date":"2100-01-01"}]}'                 | 'array[0].paramLocalDate: is not a valid date'                 |
      | 'POST'   | '/paramYear'           | '{"array":[{"param-year":"2100"}]}'                             | 'array[0].paramYear: is not a valid year'                      |
      | 'PUT'    | '/paramYear'           | '{"array":[{"param-year":"2100"}]}'                             | 'array[0].paramYear: is not a valid year'                      |
      | 'PATCH'  | '/paramYear'           | '{"array":[{"param-year":"2100"}]}'                             | 'array[0].paramYear: is not a valid year'                      |
      | 'DELETE' | '/paramYear'           | '{"array":[{"param-year":"2100"}]}'                             | 'array[0].paramYear: is not a valid year'                      |
