Feature: Request parameter with valid parameter value
  
  The REST service receives data via the request parameter and successfully interprets the data for the declared data type. The service responds with a success message.
  
   Rule: The conversion between data types is given by the following table
  
  | parameter type | java type      |
  |----------------|----------------|
  | string         | String         |
  | number         | Integer        |
  | number         | Long           |
  | number         | BigInteger     |
  | number         | Float          |
  | number         | Double         |
  | number         | BigDecimal     |
  | boolean        | Boolean        |
  | ISO-8601       | OffsetDateTime |
  | ISO-8601       | LocalDateTime  |
  | ISO-8601       | LocalDate      |
  | ISO-8601       | LocalTime      |
  | ISO-8601       | Year           |
  | string         | ENUM           |

  Background: 
    Given the default timezone 'America/Puerto_Rico'

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/method-parameter?<param>=<value> endpoint (RequestParamUnconstrainedRestApi.class)
    this endpoint receives the request parameter in a method parameter, both having the same name
    and the service correctly maps the sent value with the declared data type, responding with a success message.

    Given the endpoint '/api/v1/request-parameter/method-parameter'
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | param                 | value                  | received value                                   |
      | 'GET'    | 'paramString'         | 'id'                   | '{"paramString":"id"}'                           |
      | 'HEAD'   | 'paramString'         | 'id'                   | '{"paramString":"id"}'                           |
      | 'POST'   | 'paramString'         | 'id'                   | '{"paramString":"id"}'                           |
      | 'PUT'    | 'paramString'         | 'id'                   | '{"paramString":"id"}'                           |
      | 'PATCH'  | 'paramString'         | 'id'                   | '{"paramString":"id"}'                           |
      | 'DELETE' | 'paramString'         | 'id'                   | '{"paramString":"id"}'                           |
      | 'GET'    | 'paramInteger'        | '2'                    | '{"paramInteger":2}'                             |
      | 'HEAD'   | 'paramInteger'        | '2'                    | '{"paramInteger":2}'                             |
      | 'POST'   | 'paramInteger'        | '2'                    | '{"paramInteger":2}'                             |
      | 'PUT'    | 'paramInteger'        | '2'                    | '{"paramInteger":2}'                             |
      | 'PATCH'  | 'paramInteger'        | '2'                    | '{"paramInteger":2}'                             |
      | 'DELETE' | 'paramInteger'        | '2'                    | '{"paramInteger":2}'                             |
      | 'GET'    | 'paramLong'           | '2'                    | '{"paramLong":2}'                                |
      | 'HEAD'   | 'paramLong'           | '2'                    | '{"paramLong":2}'                                |
      | 'POST'   | 'paramLong'           | '2'                    | '{"paramLong":2}'                                |
      | 'PUT'    | 'paramLong'           | '2'                    | '{"paramLong":2}'                                |
      | 'PATCH'  | 'paramLong'           | '2'                    | '{"paramLong":2}'                                |
      | 'DELETE' | 'paramLong'           | '2'                    | '{"paramLong":2}'                                |
      | 'GET'    | 'paramBigInteger'     | '2'                    | '{"paramBigInteger":2}'                          |
      | 'HEAD'   | 'paramBigInteger'     | '2'                    | '{"paramBigInteger":2}'                          |
      | 'POST'   | 'paramBigInteger'     | '2'                    | '{"paramBigInteger":2}'                          |
      | 'PUT'    | 'paramBigInteger'     | '2'                    | '{"paramBigInteger":2}'                          |
      | 'PATCH'  | 'paramBigInteger'     | '2'                    | '{"paramBigInteger":2}'                          |
      | 'DELETE' | 'paramBigInteger'     | '2'                    | '{"paramBigInteger":2}'                          |
      | 'GET'    | 'paramFloat'          | '2.1'                  | '{"paramFloat":2.1}'                             |
      | 'HEAD'   | 'paramFloat'          | '2.1'                  | '{"paramFloat":2.1}'                             |
      | 'POST'   | 'paramFloat'          | '2.1'                  | '{"paramFloat":2.1}'                             |
      | 'PUT'    | 'paramFloat'          | '2.1'                  | '{"paramFloat":2.1}'                             |
      | 'PATCH'  | 'paramFloat'          | '2.1'                  | '{"paramFloat":2.1}'                             |
      | 'DELETE' | 'paramFloat'          | '2.1'                  | '{"paramFloat":2.1}'                             |
      | 'GET'    | 'paramDouble'         | '2.1'                  | '{"paramDouble":2.1}'                            |
      | 'HEAD'   | 'paramDouble'         | '2.1'                  | '{"paramDouble":2.1}'                            |
      | 'POST'   | 'paramDouble'         | '2.1'                  | '{"paramDouble":2.1}'                            |
      | 'PUT'    | 'paramDouble'         | '2.1'                  | '{"paramDouble":2.1}'                            |
      | 'PATCH'  | 'paramDouble'         | '2.1'                  | '{"paramDouble":2.1}'                            |
      | 'DELETE' | 'paramDouble'         | '2.1'                  | '{"paramDouble":2.1}'                            |
      | 'GET'    | 'paramBigDecimal'     | '2.1'                  | '{"paramBigDecimal":2.1}'                        |
      | 'HEAD'   | 'paramBigDecimal'     | '2.1'                  | '{"paramBigDecimal":2.1}'                        |
      | 'POST'   | 'paramBigDecimal'     | '2.1'                  | '{"paramBigDecimal":2.1}'                        |
      | 'PUT'    | 'paramBigDecimal'     | '2.1'                  | '{"paramBigDecimal":2.1}'                        |
      | 'PATCH'  | 'paramBigDecimal'     | '2.1'                  | '{"paramBigDecimal":2.1}'                        |
      | 'DELETE' | 'paramBigDecimal'     | '2.1'                  | '{"paramBigDecimal":2.1}'                        |
      | 'GET'    | 'paramBoolean'        | 'true'                 | '{"paramBoolean":true}'                          |
      | 'HEAD'   | 'paramBoolean'        | 'true'                 | '{"paramBoolean":true}'                          |
      | 'POST'   | 'paramBoolean'        | 'true'                 | '{"paramBoolean":true}'                          |
      | 'PUT'    | 'paramBoolean'        | 'true'                 | '{"paramBoolean":true}'                          |
      | 'PATCH'  | 'paramBoolean'        | 'true'                 | '{"paramBoolean":true}'                          |
      | 'DELETE' | 'paramBoolean'        | 'true'                 | '{"paramBoolean":true}'                          |
      | 'GET'    | 'paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'HEAD'   | 'paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'POST'   | 'paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'PUT'    | 'paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'PATCH'  | 'paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'DELETE' | 'paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'GET'    | 'paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'HEAD'   | 'paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'POST'   | 'paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'PUT'    | 'paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'PATCH'  | 'paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'DELETE' | 'paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'GET'    | 'paramLocalDate'      | '2000-01-01'           | '{"paramLocalDate":"2000-01-01"}'                |
      | 'HEAD'   | 'paramLocalDate'      | '2000-01-01'           | '{"paramLocalDate":"2000-01-01"}'                |
      | 'POST'   | 'paramLocalDate'      | '2000-01-01'           | '{"paramLocalDate":"2000-01-01"}'                |
      | 'PUT'    | 'paramLocalDate'      | '2000-01-01'           | '{"paramLocalDate":"2000-01-01"}'                |
      | 'PATCH'  | 'paramLocalDate'      | '2000-01-01'           | '{"paramLocalDate":"2000-01-01"}'                |
      | 'DELETE' | 'paramLocalDate'      | '2000-01-01'           | '{"paramLocalDate":"2000-01-01"}'                |
      | 'GET'    | 'paramLocalTime'      | '13:00:00'             | '{"paramLocalTime":"13:00:00"}'                  |
      | 'HEAD'   | 'paramLocalTime'      | '13:00:00'             | '{"paramLocalTime":"13:00:00"}'                  |
      | 'POST'   | 'paramLocalTime'      | '13:00:00'             | '{"paramLocalTime":"13:00:00"}'                  |
      | 'PUT'    | 'paramLocalTime'      | '13:00:00'             | '{"paramLocalTime":"13:00:00"}'                  |
      | 'PATCH'  | 'paramLocalTime'      | '13:00:00'             | '{"paramLocalTime":"13:00:00"}'                  |
      | 'DELETE' | 'paramLocalTime'      | '13:00:00'             | '{"paramLocalTime":"13:00:00"}'                  |
      | 'GET'    | 'paramYear'           | '2000'                 | '{"paramYear":"2000"}'                           |
      | 'HEAD'   | 'paramYear'           | '2000'                 | '{"paramYear":"2000"}'                           |
      | 'POST'   | 'paramYear'           | '2000'                 | '{"paramYear":"2000"}'                           |
      | 'PUT'    | 'paramYear'           | '2000'                 | '{"paramYear":"2000"}'                           |
      | 'PATCH'  | 'paramYear'           | '2000'                 | '{"paramYear":"2000"}'                           |
      | 'DELETE' | 'paramYear'           | '2000'                 | '{"paramYear":"2000"}'                           |
      | 'GET'    | 'paramEnumAB'         | 'A'                    | '{"paramEnumAB":"A"}'                            |
      | 'HEAD'   | 'paramEnumAB'         | 'A'                    | '{"paramEnumAB":"A"}'                            |
      | 'POST'   | 'paramEnumAB'         | 'A'                    | '{"paramEnumAB":"A"}'                            |
      | 'PUT'    | 'paramEnumAB'         | 'A'                    | '{"paramEnumAB":"A"}'                            |
      | 'PATCH'  | 'paramEnumAB'         | 'A'                    | '{"paramEnumAB":"A"}'                            |
      | 'DELETE' | 'paramEnumAB'         | 'A'                    | '{"paramEnumAB":"A"}'                            |
      | 'GET'    | 'paramEnumLowerAB'    | 'b'                    | '{"paramEnumLowerAB":"b"}'                       |
      | 'HEAD'   | 'paramEnumLowerAB'    | 'b'                    | '{"paramEnumLowerAB":"b"}'                       |
      | 'POST'   | 'paramEnumLowerAB'    | 'b'                    | '{"paramEnumLowerAB":"b"}'                       |
      | 'PUT'    | 'paramEnumLowerAB'    | 'b'                    | '{"paramEnumLowerAB":"b"}'                       |
      | 'PATCH'  | 'paramEnumLowerAB'    | 'b'                    | '{"paramEnumLowerAB":"b"}'                       |
      | 'DELETE' | 'paramEnumLowerAB'    | 'b'                    | '{"paramEnumLowerAB":"b"}'                       |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/named-method-parameter?<param>=<value> endpoint (RequestParamUnconstrainedRestApi.class)
    this endpoint receives the request parameter in a method parameter, both having the diferent names, a custom request-parameter name is defined by the endpoint 
    and the service correctly maps the sent value with the declared data type, responding with a success message.

    Given the endpoint '/api/v1/request-parameter/named-method-parameter'
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | param                    | value                  | received value                                   |
      | 'GET'    | 'param-string'           | 'id'                   | '{"paramString":"id"}'                           |
      | 'HEAD'   | 'param-string'           | 'id'                   | '{"paramString":"id"}'                           |
      | 'POST'   | 'param-string'           | 'id'                   | '{"paramString":"id"}'                           |
      | 'PUT'    | 'param-string'           | 'id'                   | '{"paramString":"id"}'                           |
      | 'PATCH'  | 'param-string'           | 'id'                   | '{"paramString":"id"}'                           |
      | 'DELETE' | 'param-string'           | 'id'                   | '{"paramString":"id"}'                           |
      | 'GET'    | 'param-integer'          | '2'                    | '{"paramInteger":2}'                             |
      | 'HEAD'   | 'param-integer'          | '2'                    | '{"paramInteger":2}'                             |
      | 'POST'   | 'param-integer'          | '2'                    | '{"paramInteger":2}'                             |
      | 'PUT'    | 'param-integer'          | '2'                    | '{"paramInteger":2}'                             |
      | 'PATCH'  | 'param-integer'          | '2'                    | '{"paramInteger":2}'                             |
      | 'DELETE' | 'param-integer'          | '2'                    | '{"paramInteger":2}'                             |
      | 'GET'    | 'param-long'             | '2'                    | '{"paramLong":2}'                                |
      | 'HEAD'   | 'param-long'             | '2'                    | '{"paramLong":2}'                                |
      | 'POST'   | 'param-long'             | '2'                    | '{"paramLong":2}'                                |
      | 'PUT'    | 'param-long'             | '2'                    | '{"paramLong":2}'                                |
      | 'PATCH'  | 'param-long'             | '2'                    | '{"paramLong":2}'                                |
      | 'DELETE' | 'param-long'             | '2'                    | '{"paramLong":2}'                                |
      | 'GET'    | 'param-big-integer'      | '2'                    | '{"paramBigInteger":2}'                          |
      | 'HEAD'   | 'param-big-integer'      | '2'                    | '{"paramBigInteger":2}'                          |
      | 'POST'   | 'param-big-integer'      | '2'                    | '{"paramBigInteger":2}'                          |
      | 'PUT'    | 'param-big-integer'      | '2'                    | '{"paramBigInteger":2}'                          |
      | 'PATCH'  | 'param-big-integer'      | '2'                    | '{"paramBigInteger":2}'                          |
      | 'DELETE' | 'param-big-integer'      | '2'                    | '{"paramBigInteger":2}'                          |
      | 'GET'    | 'param-float'            | '2.1'                  | '{"paramFloat":2.1}'                             |
      | 'HEAD'   | 'param-float'            | '2.1'                  | '{"paramFloat":2.1}'                             |
      | 'POST'   | 'param-float'            | '2.1'                  | '{"paramFloat":2.1}'                             |
      | 'PUT'    | 'param-float'            | '2.1'                  | '{"paramFloat":2.1}'                             |
      | 'PATCH'  | 'param-float'            | '2.1'                  | '{"paramFloat":2.1}'                             |
      | 'DELETE' | 'param-float'            | '2.1'                  | '{"paramFloat":2.1}'                             |
      | 'GET'    | 'param-double'           | '2.1'                  | '{"paramDouble":2.1}'                            |
      | 'HEAD'   | 'param-double'           | '2.1'                  | '{"paramDouble":2.1}'                            |
      | 'POST'   | 'param-double'           | '2.1'                  | '{"paramDouble":2.1}'                            |
      | 'PUT'    | 'param-double'           | '2.1'                  | '{"paramDouble":2.1}'                            |
      | 'PATCH'  | 'param-double'           | '2.1'                  | '{"paramDouble":2.1}'                            |
      | 'DELETE' | 'param-double'           | '2.1'                  | '{"paramDouble":2.1}'                            |
      | 'GET'    | 'param-big-decimal'      | '2.1'                  | '{"paramBigDecimal":2.1}'                        |
      | 'HEAD'   | 'param-big-decimal'      | '2.1'                  | '{"paramBigDecimal":2.1}'                        |
      | 'POST'   | 'param-big-decimal'      | '2.1'                  | '{"paramBigDecimal":2.1}'                        |
      | 'PUT'    | 'param-big-decimal'      | '2.1'                  | '{"paramBigDecimal":2.1}'                        |
      | 'PATCH'  | 'param-big-decimal'      | '2.1'                  | '{"paramBigDecimal":2.1}'                        |
      | 'DELETE' | 'param-big-decimal'      | '2.1'                  | '{"paramBigDecimal":2.1}'                        |
      | 'GET'    | 'param-boolean'          | 'true'                 | '{"paramBoolean":true}'                          |
      | 'HEAD'   | 'param-boolean'          | 'true'                 | '{"paramBoolean":true}'                          |
      | 'POST'   | 'param-boolean'          | 'true'                 | '{"paramBoolean":true}'                          |
      | 'PUT'    | 'param-boolean'          | 'true'                 | '{"paramBoolean":true}'                          |
      | 'PATCH'  | 'param-boolean'          | 'true'                 | '{"paramBoolean":true}'                          |
      | 'DELETE' | 'param-boolean'          | 'true'                 | '{"paramBoolean":true}'                          |
      | 'GET'    | 'param-offset-date-time' | '2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'HEAD'   | 'param-offset-date-time' | '2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'POST'   | 'param-offset-date-time' | '2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'PUT'    | 'param-offset-date-time' | '2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'PATCH'  | 'param-offset-date-time' | '2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'DELETE' | 'param-offset-date-time' | '2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'GET'    | 'param-local-date-time'  | '2000-01-01T00:00:00Z' | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'HEAD'   | 'param-local-date-time'  | '2000-01-01T00:00:00Z' | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'POST'   | 'param-local-date-time'  | '2000-01-01T00:00:00Z' | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'PUT'    | 'param-local-date-time'  | '2000-01-01T00:00:00Z' | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'PATCH'  | 'param-local-date-time'  | '2000-01-01T00:00:00Z' | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'DELETE' | 'param-local-date-time'  | '2000-01-01T00:00:00Z' | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'GET'    | 'param-local-date'       | '2000-01-01'           | '{"paramLocalDate":"2000-01-01"}'                |
      | 'HEAD'   | 'param-local-date'       | '2000-01-01'           | '{"paramLocalDate":"2000-01-01"}'                |
      | 'POST'   | 'param-local-date'       | '2000-01-01'           | '{"paramLocalDate":"2000-01-01"}'                |
      | 'PUT'    | 'param-local-date'       | '2000-01-01'           | '{"paramLocalDate":"2000-01-01"}'                |
      | 'PATCH'  | 'param-local-date'       | '2000-01-01'           | '{"paramLocalDate":"2000-01-01"}'                |
      | 'DELETE' | 'param-local-date'       | '2000-01-01'           | '{"paramLocalDate":"2000-01-01"}'                |
      | 'GET'    | 'param-local-time'       | '13:00:00'             | '{"paramLocalTime":"13:00:00"}'                  |
      | 'HEAD'   | 'param-local-time'       | '13:00:00'             | '{"paramLocalTime":"13:00:00"}'                  |
      | 'POST'   | 'param-local-time'       | '13:00:00'             | '{"paramLocalTime":"13:00:00"}'                  |
      | 'PUT'    | 'param-local-time'       | '13:00:00'             | '{"paramLocalTime":"13:00:00"}'                  |
      | 'PATCH'  | 'param-local-time'       | '13:00:00'             | '{"paramLocalTime":"13:00:00"}'                  |
      | 'DELETE' | 'param-local-time'       | '13:00:00'             | '{"paramLocalTime":"13:00:00"}'                  |
      | 'GET'    | 'param-year'             | '2000'                 | '{"paramYear":"2000"}'                           |
      | 'HEAD'   | 'param-year'             | '2000'                 | '{"paramYear":"2000"}'                           |
      | 'POST'   | 'param-year'             | '2000'                 | '{"paramYear":"2000"}'                           |
      | 'PUT'    | 'param-year'             | '2000'                 | '{"paramYear":"2000"}'                           |
      | 'PATCH'  | 'param-year'             | '2000'                 | '{"paramYear":"2000"}'                           |
      | 'DELETE' | 'param-year'             | '2000'                 | '{"paramYear":"2000"}'                           |
      | 'GET'    | 'param-enum-ab'          | 'A'                    | '{"paramEnumAB":"A"}'                            |
      | 'HEAD'   | 'param-enum-ab'          | 'A'                    | '{"paramEnumAB":"A"}'                            |
      | 'POST'   | 'param-enum-ab'          | 'A'                    | '{"paramEnumAB":"A"}'                            |
      | 'PUT'    | 'param-enum-ab'          | 'A'                    | '{"paramEnumAB":"A"}'                            |
      | 'PATCH'  | 'param-enum-ab'          | 'A'                    | '{"paramEnumAB":"A"}'                            |
      | 'DELETE' | 'param-enum-ab'          | 'A'                    | '{"paramEnumAB":"A"}'                            |
      | 'GET'    | 'param-enum-lower-ab'    | 'b'                    | '{"paramEnumLowerAB":"b"}'                       |
      | 'HEAD'   | 'param-enum-lower-ab'    | 'b'                    | '{"paramEnumLowerAB":"b"}'                       |
      | 'POST'   | 'param-enum-lower-ab'    | 'b'                    | '{"paramEnumLowerAB":"b"}'                       |
      | 'PUT'    | 'param-enum-lower-ab'    | 'b'                    | '{"paramEnumLowerAB":"b"}'                       |
      | 'PATCH'  | 'param-enum-lower-ab'    | 'b'                    | '{"paramEnumLowerAB":"b"}'                       |
      | 'DELETE' | 'param-enum-lower-ab'    | 'b'                    | '{"paramEnumLowerAB":"b"}'                       |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/optional-method-parameter?<param>=<value> endpoint (RequestParamUnconstrainedRestApi.class)
    this endpoint receives the request parameter in an optional method parameter, both having the same name
    and the service correctly maps the sent value with the declared data type, responding with a success message.

    Given the endpoint '/api/v1/request-parameter/optional-method-parameter'
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | param                 | value                  | received value                                   |
      | 'GET'    | 'paramString'         | 'id'                   | '{"paramString":"id"}'                           |
      | 'HEAD'   | 'paramString'         | 'id'                   | '{"paramString":"id"}'                           |
      | 'POST'   | 'paramString'         | 'id'                   | '{"paramString":"id"}'                           |
      | 'PUT'    | 'paramString'         | 'id'                   | '{"paramString":"id"}'                           |
      | 'PATCH'  | 'paramString'         | 'id'                   | '{"paramString":"id"}'                           |
      | 'DELETE' | 'paramString'         | 'id'                   | '{"paramString":"id"}'                           |
      | 'GET'    | 'paramInteger'        | '2'                    | '{"paramInteger":2}'                             |
      | 'HEAD'   | 'paramInteger'        | '2'                    | '{"paramInteger":2}'                             |
      | 'POST'   | 'paramInteger'        | '2'                    | '{"paramInteger":2}'                             |
      | 'PUT'    | 'paramInteger'        | '2'                    | '{"paramInteger":2}'                             |
      | 'PATCH'  | 'paramInteger'        | '2'                    | '{"paramInteger":2}'                             |
      | 'DELETE' | 'paramInteger'        | '2'                    | '{"paramInteger":2}'                             |
      | 'GET'    | 'paramLong'           | '2'                    | '{"paramLong":2}'                                |
      | 'HEAD'   | 'paramLong'           | '2'                    | '{"paramLong":2}'                                |
      | 'POST'   | 'paramLong'           | '2'                    | '{"paramLong":2}'                                |
      | 'PUT'    | 'paramLong'           | '2'                    | '{"paramLong":2}'                                |
      | 'PATCH'  | 'paramLong'           | '2'                    | '{"paramLong":2}'                                |
      | 'DELETE' | 'paramLong'           | '2'                    | '{"paramLong":2}'                                |
      | 'GET'    | 'paramBigInteger'     | '2'                    | '{"paramBigInteger":2}'                          |
      | 'HEAD'   | 'paramBigInteger'     | '2'                    | '{"paramBigInteger":2}'                          |
      | 'POST'   | 'paramBigInteger'     | '2'                    | '{"paramBigInteger":2}'                          |
      | 'PUT'    | 'paramBigInteger'     | '2'                    | '{"paramBigInteger":2}'                          |
      | 'PATCH'  | 'paramBigInteger'     | '2'                    | '{"paramBigInteger":2}'                          |
      | 'DELETE' | 'paramBigInteger'     | '2'                    | '{"paramBigInteger":2}'                          |
      | 'GET'    | 'paramFloat'          | '2.1'                  | '{"paramFloat":2.1}'                             |
      | 'HEAD'   | 'paramFloat'          | '2.1'                  | '{"paramFloat":2.1}'                             |
      | 'POST'   | 'paramFloat'          | '2.1'                  | '{"paramFloat":2.1}'                             |
      | 'PUT'    | 'paramFloat'          | '2.1'                  | '{"paramFloat":2.1}'                             |
      | 'PATCH'  | 'paramFloat'          | '2.1'                  | '{"paramFloat":2.1}'                             |
      | 'DELETE' | 'paramFloat'          | '2.1'                  | '{"paramFloat":2.1}'                             |
      | 'GET'    | 'paramDouble'         | '2.1'                  | '{"paramDouble":2.1}'                            |
      | 'HEAD'   | 'paramDouble'         | '2.1'                  | '{"paramDouble":2.1}'                            |
      | 'POST'   | 'paramDouble'         | '2.1'                  | '{"paramDouble":2.1}'                            |
      | 'PUT'    | 'paramDouble'         | '2.1'                  | '{"paramDouble":2.1}'                            |
      | 'PATCH'  | 'paramDouble'         | '2.1'                  | '{"paramDouble":2.1}'                            |
      | 'DELETE' | 'paramDouble'         | '2.1'                  | '{"paramDouble":2.1}'                            |
      | 'GET'    | 'paramBigDecimal'     | '2.1'                  | '{"paramBigDecimal":2.1}'                        |
      | 'HEAD'   | 'paramBigDecimal'     | '2.1'                  | '{"paramBigDecimal":2.1}'                        |
      | 'POST'   | 'paramBigDecimal'     | '2.1'                  | '{"paramBigDecimal":2.1}'                        |
      | 'PUT'    | 'paramBigDecimal'     | '2.1'                  | '{"paramBigDecimal":2.1}'                        |
      | 'PATCH'  | 'paramBigDecimal'     | '2.1'                  | '{"paramBigDecimal":2.1}'                        |
      | 'DELETE' | 'paramBigDecimal'     | '2.1'                  | '{"paramBigDecimal":2.1}'                        |
      | 'GET'    | 'paramBoolean'        | 'true'                 | '{"paramBoolean":true}'                          |
      | 'HEAD'   | 'paramBoolean'        | 'true'                 | '{"paramBoolean":true}'                          |
      | 'POST'   | 'paramBoolean'        | 'true'                 | '{"paramBoolean":true}'                          |
      | 'PUT'    | 'paramBoolean'        | 'true'                 | '{"paramBoolean":true}'                          |
      | 'PATCH'  | 'paramBoolean'        | 'true'                 | '{"paramBoolean":true}'                          |
      | 'DELETE' | 'paramBoolean'        | 'true'                 | '{"paramBoolean":true}'                          |
      | 'GET'    | 'paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'HEAD'   | 'paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'POST'   | 'paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'PUT'    | 'paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'PATCH'  | 'paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'DELETE' | 'paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'GET'    | 'paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'HEAD'   | 'paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'POST'   | 'paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'PUT'    | 'paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'PATCH'  | 'paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'DELETE' | 'paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'GET'    | 'paramLocalDate'      | '2000-01-01'           | '{"paramLocalDate":"2000-01-01"}'                |
      | 'HEAD'   | 'paramLocalDate'      | '2000-01-01'           | '{"paramLocalDate":"2000-01-01"}'                |
      | 'POST'   | 'paramLocalDate'      | '2000-01-01'           | '{"paramLocalDate":"2000-01-01"}'                |
      | 'PUT'    | 'paramLocalDate'      | '2000-01-01'           | '{"paramLocalDate":"2000-01-01"}'                |
      | 'PATCH'  | 'paramLocalDate'      | '2000-01-01'           | '{"paramLocalDate":"2000-01-01"}'                |
      | 'DELETE' | 'paramLocalDate'      | '2000-01-01'           | '{"paramLocalDate":"2000-01-01"}'                |
      | 'GET'    | 'paramLocalTime'      | '13:00:00'             | '{"paramLocalTime":"13:00:00"}'                  |
      | 'HEAD'   | 'paramLocalTime'      | '13:00:00'             | '{"paramLocalTime":"13:00:00"}'                  |
      | 'POST'   | 'paramLocalTime'      | '13:00:00'             | '{"paramLocalTime":"13:00:00"}'                  |
      | 'PUT'    | 'paramLocalTime'      | '13:00:00'             | '{"paramLocalTime":"13:00:00"}'                  |
      | 'PATCH'  | 'paramLocalTime'      | '13:00:00'             | '{"paramLocalTime":"13:00:00"}'                  |
      | 'DELETE' | 'paramLocalTime'      | '13:00:00'             | '{"paramLocalTime":"13:00:00"}'                  |
      | 'GET'    | 'paramYear'           | '2000'                 | '{"paramYear":"2000"}'                           |
      | 'HEAD'   | 'paramYear'           | '2000'                 | '{"paramYear":"2000"}'                           |
      | 'POST'   | 'paramYear'           | '2000'                 | '{"paramYear":"2000"}'                           |
      | 'PUT'    | 'paramYear'           | '2000'                 | '{"paramYear":"2000"}'                           |
      | 'PATCH'  | 'paramYear'           | '2000'                 | '{"paramYear":"2000"}'                           |
      | 'DELETE' | 'paramYear'           | '2000'                 | '{"paramYear":"2000"}'                           |
      | 'GET'    | 'paramEnumAB'         | 'A'                    | '{"paramEnumAB":"A"}'                            |
      | 'HEAD'   | 'paramEnumAB'         | 'A'                    | '{"paramEnumAB":"A"}'                            |
      | 'POST'   | 'paramEnumAB'         | 'A'                    | '{"paramEnumAB":"A"}'                            |
      | 'PUT'    | 'paramEnumAB'         | 'A'                    | '{"paramEnumAB":"A"}'                            |
      | 'PATCH'  | 'paramEnumAB'         | 'A'                    | '{"paramEnumAB":"A"}'                            |
      | 'DELETE' | 'paramEnumAB'         | 'A'                    | '{"paramEnumAB":"A"}'                            |
      | 'GET'    | 'paramEnumLowerAB'    | 'b'                    | '{"paramEnumLowerAB":"b"}'                       |
      | 'HEAD'   | 'paramEnumLowerAB'    | 'b'                    | '{"paramEnumLowerAB":"b"}'                       |
      | 'POST'   | 'paramEnumLowerAB'    | 'b'                    | '{"paramEnumLowerAB":"b"}'                       |
      | 'PUT'    | 'paramEnumLowerAB'    | 'b'                    | '{"paramEnumLowerAB":"b"}'                       |
      | 'PATCH'  | 'paramEnumLowerAB'    | 'b'                    | '{"paramEnumLowerAB":"b"}'                       |
      | 'DELETE' | 'paramEnumLowerAB'    | 'b'                    | '{"paramEnumLowerAB":"b"}'                       |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/named-optional-method-parameter?<param>=<value> endpoint (RequestParamUnconstrainedRestApi.class)
    this endpoint receives the request parameter in an optional method parameter, both having the diferent names, a custom request-parameter name is defined by the endpoint 
    and the service correctly maps the sent value with the declared data type, responding with a success message.

    Given the endpoint '/api/v1/request-parameter/named-optional-method-parameter'
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | param                    | value                  | received value                                   |
      | 'GET'    | 'param-string'           | 'id'                   | '{"paramString":"id"}'                           |
      | 'HEAD'   | 'param-string'           | 'id'                   | '{"paramString":"id"}'                           |
      | 'POST'   | 'param-string'           | 'id'                   | '{"paramString":"id"}'                           |
      | 'PUT'    | 'param-string'           | 'id'                   | '{"paramString":"id"}'                           |
      | 'PATCH'  | 'param-string'           | 'id'                   | '{"paramString":"id"}'                           |
      | 'DELETE' | 'param-string'           | 'id'                   | '{"paramString":"id"}'                           |
      | 'GET'    | 'param-integer'          | '2'                    | '{"paramInteger":2}'                             |
      | 'HEAD'   | 'param-integer'          | '2'                    | '{"paramInteger":2}'                             |
      | 'POST'   | 'param-integer'          | '2'                    | '{"paramInteger":2}'                             |
      | 'PUT'    | 'param-integer'          | '2'                    | '{"paramInteger":2}'                             |
      | 'PATCH'  | 'param-integer'          | '2'                    | '{"paramInteger":2}'                             |
      | 'DELETE' | 'param-integer'          | '2'                    | '{"paramInteger":2}'                             |
      | 'GET'    | 'param-long'             | '2'                    | '{"paramLong":2}'                                |
      | 'HEAD'   | 'param-long'             | '2'                    | '{"paramLong":2}'                                |
      | 'POST'   | 'param-long'             | '2'                    | '{"paramLong":2}'                                |
      | 'PUT'    | 'param-long'             | '2'                    | '{"paramLong":2}'                                |
      | 'PATCH'  | 'param-long'             | '2'                    | '{"paramLong":2}'                                |
      | 'DELETE' | 'param-long'             | '2'                    | '{"paramLong":2}'                                |
      | 'GET'    | 'param-big-integer'      | '2'                    | '{"paramBigInteger":2}'                          |
      | 'HEAD'   | 'param-big-integer'      | '2'                    | '{"paramBigInteger":2}'                          |
      | 'POST'   | 'param-big-integer'      | '2'                    | '{"paramBigInteger":2}'                          |
      | 'PUT'    | 'param-big-integer'      | '2'                    | '{"paramBigInteger":2}'                          |
      | 'PATCH'  | 'param-big-integer'      | '2'                    | '{"paramBigInteger":2}'                          |
      | 'DELETE' | 'param-big-integer'      | '2'                    | '{"paramBigInteger":2}'                          |
      | 'GET'    | 'param-float'            | '2.1'                  | '{"paramFloat":2.1}'                             |
      | 'HEAD'   | 'param-float'            | '2.1'                  | '{"paramFloat":2.1}'                             |
      | 'POST'   | 'param-float'            | '2.1'                  | '{"paramFloat":2.1}'                             |
      | 'PUT'    | 'param-float'            | '2.1'                  | '{"paramFloat":2.1}'                             |
      | 'PATCH'  | 'param-float'            | '2.1'                  | '{"paramFloat":2.1}'                             |
      | 'DELETE' | 'param-float'            | '2.1'                  | '{"paramFloat":2.1}'                             |
      | 'GET'    | 'param-double'           | '2.1'                  | '{"paramDouble":2.1}'                            |
      | 'HEAD'   | 'param-double'           | '2.1'                  | '{"paramDouble":2.1}'                            |
      | 'POST'   | 'param-double'           | '2.1'                  | '{"paramDouble":2.1}'                            |
      | 'PUT'    | 'param-double'           | '2.1'                  | '{"paramDouble":2.1}'                            |
      | 'PATCH'  | 'param-double'           | '2.1'                  | '{"paramDouble":2.1}'                            |
      | 'DELETE' | 'param-double'           | '2.1'                  | '{"paramDouble":2.1}'                            |
      | 'GET'    | 'param-big-decimal'      | '2.1'                  | '{"paramBigDecimal":2.1}'                        |
      | 'HEAD'   | 'param-big-decimal'      | '2.1'                  | '{"paramBigDecimal":2.1}'                        |
      | 'POST'   | 'param-big-decimal'      | '2.1'                  | '{"paramBigDecimal":2.1}'                        |
      | 'PUT'    | 'param-big-decimal'      | '2.1'                  | '{"paramBigDecimal":2.1}'                        |
      | 'PATCH'  | 'param-big-decimal'      | '2.1'                  | '{"paramBigDecimal":2.1}'                        |
      | 'DELETE' | 'param-big-decimal'      | '2.1'                  | '{"paramBigDecimal":2.1}'                        |
      | 'GET'    | 'param-boolean'          | 'true'                 | '{"paramBoolean":true}'                          |
      | 'HEAD'   | 'param-boolean'          | 'true'                 | '{"paramBoolean":true}'                          |
      | 'POST'   | 'param-boolean'          | 'true'                 | '{"paramBoolean":true}'                          |
      | 'PUT'    | 'param-boolean'          | 'true'                 | '{"paramBoolean":true}'                          |
      | 'PATCH'  | 'param-boolean'          | 'true'                 | '{"paramBoolean":true}'                          |
      | 'DELETE' | 'param-boolean'          | 'true'                 | '{"paramBoolean":true}'                          |
      | 'GET'    | 'param-offset-date-time' | '2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'HEAD'   | 'param-offset-date-time' | '2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'POST'   | 'param-offset-date-time' | '2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'PUT'    | 'param-offset-date-time' | '2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'PATCH'  | 'param-offset-date-time' | '2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'DELETE' | 'param-offset-date-time' | '2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'GET'    | 'param-local-date-time'  | '2000-01-01T00:00:00Z' | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'HEAD'   | 'param-local-date-time'  | '2000-01-01T00:00:00Z' | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'POST'   | 'param-local-date-time'  | '2000-01-01T00:00:00Z' | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'PUT'    | 'param-local-date-time'  | '2000-01-01T00:00:00Z' | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'PATCH'  | 'param-local-date-time'  | '2000-01-01T00:00:00Z' | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'DELETE' | 'param-local-date-time'  | '2000-01-01T00:00:00Z' | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'GET'    | 'param-local-date'       | '2000-01-01'           | '{"paramLocalDate":"2000-01-01"}'                |
      | 'HEAD'   | 'param-local-date'       | '2000-01-01'           | '{"paramLocalDate":"2000-01-01"}'                |
      | 'POST'   | 'param-local-date'       | '2000-01-01'           | '{"paramLocalDate":"2000-01-01"}'                |
      | 'PUT'    | 'param-local-date'       | '2000-01-01'           | '{"paramLocalDate":"2000-01-01"}'                |
      | 'PATCH'  | 'param-local-date'       | '2000-01-01'           | '{"paramLocalDate":"2000-01-01"}'                |
      | 'DELETE' | 'param-local-date'       | '2000-01-01'           | '{"paramLocalDate":"2000-01-01"}'                |
      | 'GET'    | 'param-local-time'       | '13:00:00'             | '{"paramLocalTime":"13:00:00"}'                  |
      | 'HEAD'   | 'param-local-time'       | '13:00:00'             | '{"paramLocalTime":"13:00:00"}'                  |
      | 'POST'   | 'param-local-time'       | '13:00:00'             | '{"paramLocalTime":"13:00:00"}'                  |
      | 'PUT'    | 'param-local-time'       | '13:00:00'             | '{"paramLocalTime":"13:00:00"}'                  |
      | 'PATCH'  | 'param-local-time'       | '13:00:00'             | '{"paramLocalTime":"13:00:00"}'                  |
      | 'DELETE' | 'param-local-time'       | '13:00:00'             | '{"paramLocalTime":"13:00:00"}'                  |
      | 'GET'    | 'param-year'             | '2000'                 | '{"paramYear":"2000"}'                           |
      | 'HEAD'   | 'param-year'             | '2000'                 | '{"paramYear":"2000"}'                           |
      | 'POST'   | 'param-year'             | '2000'                 | '{"paramYear":"2000"}'                           |
      | 'PUT'    | 'param-year'             | '2000'                 | '{"paramYear":"2000"}'                           |
      | 'PATCH'  | 'param-year'             | '2000'                 | '{"paramYear":"2000"}'                           |
      | 'DELETE' | 'param-year'             | '2000'                 | '{"paramYear":"2000"}'                           |
      | 'GET'    | 'param-enum-ab'          | 'A'                    | '{"paramEnumAB":"A"}'                            |
      | 'HEAD'   | 'param-enum-ab'          | 'A'                    | '{"paramEnumAB":"A"}'                            |
      | 'POST'   | 'param-enum-ab'          | 'A'                    | '{"paramEnumAB":"A"}'                            |
      | 'PUT'    | 'param-enum-ab'          | 'A'                    | '{"paramEnumAB":"A"}'                            |
      | 'PATCH'  | 'param-enum-ab'          | 'A'                    | '{"paramEnumAB":"A"}'                            |
      | 'DELETE' | 'param-enum-ab'          | 'A'                    | '{"paramEnumAB":"A"}'                            |
      | 'GET'    | 'param-enum-lower-ab'    | 'b'                    | '{"paramEnumLowerAB":"b"}'                       |
      | 'HEAD'   | 'param-enum-lower-ab'    | 'b'                    | '{"paramEnumLowerAB":"b"}'                       |
      | 'POST'   | 'param-enum-lower-ab'    | 'b'                    | '{"paramEnumLowerAB":"b"}'                       |
      | 'PUT'    | 'param-enum-lower-ab'    | 'b'                    | '{"paramEnumLowerAB":"b"}'                       |
      | 'PATCH'  | 'param-enum-lower-ab'    | 'b'                    | '{"paramEnumLowerAB":"b"}'                       |
      | 'DELETE' | 'param-enum-lower-ab'    | 'b'                    | '{"paramEnumLowerAB":"b"}'                       |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/dto-attribute?<param>=<value> endpoint (RequestParamUnconstrainedRestApi.class)
    this endpoint receives the request parameter in a dto attribute, both having the same name
    and the service correctly maps the sent value with the declared data type, responding with a success message.

    Given the endpoint '/api/v1/request-parameter/dto-attribute'
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | param                 | value                  | received value                                   |
      | 'GET'    | 'paramString'         | 'id'                   | '{"paramString":"id"}'                           |
      | 'HEAD'   | 'paramString'         | 'id'                   | '{"paramString":"id"}'                           |
      | 'POST'   | 'paramString'         | 'id'                   | '{"paramString":"id"}'                           |
      | 'PUT'    | 'paramString'         | 'id'                   | '{"paramString":"id"}'                           |
      | 'PATCH'  | 'paramString'         | 'id'                   | '{"paramString":"id"}'                           |
      | 'DELETE' | 'paramString'         | 'id'                   | '{"paramString":"id"}'                           |
      | 'GET'    | 'paramInteger'        | '2'                    | '{"paramInteger":2}'                             |
      | 'HEAD'   | 'paramInteger'        | '2'                    | '{"paramInteger":2}'                             |
      | 'POST'   | 'paramInteger'        | '2'                    | '{"paramInteger":2}'                             |
      | 'PUT'    | 'paramInteger'        | '2'                    | '{"paramInteger":2}'                             |
      | 'PATCH'  | 'paramInteger'        | '2'                    | '{"paramInteger":2}'                             |
      | 'DELETE' | 'paramInteger'        | '2'                    | '{"paramInteger":2}'                             |
      | 'GET'    | 'paramLong'           | '2'                    | '{"paramLong":2}'                                |
      | 'HEAD'   | 'paramLong'           | '2'                    | '{"paramLong":2}'                                |
      | 'POST'   | 'paramLong'           | '2'                    | '{"paramLong":2}'                                |
      | 'PUT'    | 'paramLong'           | '2'                    | '{"paramLong":2}'                                |
      | 'PATCH'  | 'paramLong'           | '2'                    | '{"paramLong":2}'                                |
      | 'DELETE' | 'paramLong'           | '2'                    | '{"paramLong":2}'                                |
      | 'GET'    | 'paramBigInteger'     | '2'                    | '{"paramBigInteger":2}'                          |
      | 'HEAD'   | 'paramBigInteger'     | '2'                    | '{"paramBigInteger":2}'                          |
      | 'POST'   | 'paramBigInteger'     | '2'                    | '{"paramBigInteger":2}'                          |
      | 'PUT'    | 'paramBigInteger'     | '2'                    | '{"paramBigInteger":2}'                          |
      | 'PATCH'  | 'paramBigInteger'     | '2'                    | '{"paramBigInteger":2}'                          |
      | 'DELETE' | 'paramBigInteger'     | '2'                    | '{"paramBigInteger":2}'                          |
      | 'GET'    | 'paramFloat'          | '2.1'                  | '{"paramFloat":2.1}'                             |
      | 'HEAD'   | 'paramFloat'          | '2.1'                  | '{"paramFloat":2.1}'                             |
      | 'POST'   | 'paramFloat'          | '2.1'                  | '{"paramFloat":2.1}'                             |
      | 'PUT'    | 'paramFloat'          | '2.1'                  | '{"paramFloat":2.1}'                             |
      | 'PATCH'  | 'paramFloat'          | '2.1'                  | '{"paramFloat":2.1}'                             |
      | 'DELETE' | 'paramFloat'          | '2.1'                  | '{"paramFloat":2.1}'                             |
      | 'GET'    | 'paramDouble'         | '2.1'                  | '{"paramDouble":2.1}'                            |
      | 'HEAD'   | 'paramDouble'         | '2.1'                  | '{"paramDouble":2.1}'                            |
      | 'POST'   | 'paramDouble'         | '2.1'                  | '{"paramDouble":2.1}'                            |
      | 'PUT'    | 'paramDouble'         | '2.1'                  | '{"paramDouble":2.1}'                            |
      | 'PATCH'  | 'paramDouble'         | '2.1'                  | '{"paramDouble":2.1}'                            |
      | 'DELETE' | 'paramDouble'         | '2.1'                  | '{"paramDouble":2.1}'                            |
      | 'GET'    | 'paramBigDecimal'     | '2.1'                  | '{"paramBigDecimal":2.1}'                        |
      | 'HEAD'   | 'paramBigDecimal'     | '2.1'                  | '{"paramBigDecimal":2.1}'                        |
      | 'POST'   | 'paramBigDecimal'     | '2.1'                  | '{"paramBigDecimal":2.1}'                        |
      | 'PUT'    | 'paramBigDecimal'     | '2.1'                  | '{"paramBigDecimal":2.1}'                        |
      | 'PATCH'  | 'paramBigDecimal'     | '2.1'                  | '{"paramBigDecimal":2.1}'                        |
      | 'DELETE' | 'paramBigDecimal'     | '2.1'                  | '{"paramBigDecimal":2.1}'                        |
      | 'GET'    | 'paramBoolean'        | 'true'                 | '{"paramBoolean":true}'                          |
      | 'HEAD'   | 'paramBoolean'        | 'true'                 | '{"paramBoolean":true}'                          |
      | 'POST'   | 'paramBoolean'        | 'true'                 | '{"paramBoolean":true}'                          |
      | 'PUT'    | 'paramBoolean'        | 'true'                 | '{"paramBoolean":true}'                          |
      | 'PATCH'  | 'paramBoolean'        | 'true'                 | '{"paramBoolean":true}'                          |
      | 'DELETE' | 'paramBoolean'        | 'true'                 | '{"paramBoolean":true}'                          |
      | 'GET'    | 'paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'HEAD'   | 'paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'POST'   | 'paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'PUT'    | 'paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'PATCH'  | 'paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'DELETE' | 'paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'GET'    | 'paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'HEAD'   | 'paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'POST'   | 'paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'PUT'    | 'paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'PATCH'  | 'paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'DELETE' | 'paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'GET'    | 'paramLocalDate'      | '2000-01-01'           | '{"paramLocalDate":"2000-01-01"}'                |
      | 'HEAD'   | 'paramLocalDate'      | '2000-01-01'           | '{"paramLocalDate":"2000-01-01"}'                |
      | 'POST'   | 'paramLocalDate'      | '2000-01-01'           | '{"paramLocalDate":"2000-01-01"}'                |
      | 'PUT'    | 'paramLocalDate'      | '2000-01-01'           | '{"paramLocalDate":"2000-01-01"}'                |
      | 'PATCH'  | 'paramLocalDate'      | '2000-01-01'           | '{"paramLocalDate":"2000-01-01"}'                |
      | 'DELETE' | 'paramLocalDate'      | '2000-01-01'           | '{"paramLocalDate":"2000-01-01"}'                |
      | 'GET'    | 'paramLocalTime'      | '13:00:00'             | '{"paramLocalTime":"13:00:00"}'                  |
      | 'HEAD'   | 'paramLocalTime'      | '13:00:00'             | '{"paramLocalTime":"13:00:00"}'                  |
      | 'POST'   | 'paramLocalTime'      | '13:00:00'             | '{"paramLocalTime":"13:00:00"}'                  |
      | 'PUT'    | 'paramLocalTime'      | '13:00:00'             | '{"paramLocalTime":"13:00:00"}'                  |
      | 'PATCH'  | 'paramLocalTime'      | '13:00:00'             | '{"paramLocalTime":"13:00:00"}'                  |
      | 'DELETE' | 'paramLocalTime'      | '13:00:00'             | '{"paramLocalTime":"13:00:00"}'                  |
      | 'GET'    | 'paramYear'           | '2000'                 | '{"paramYear":"2000"}'                           |
      | 'HEAD'   | 'paramYear'           | '2000'                 | '{"paramYear":"2000"}'                           |
      | 'POST'   | 'paramYear'           | '2000'                 | '{"paramYear":"2000"}'                           |
      | 'PUT'    | 'paramYear'           | '2000'                 | '{"paramYear":"2000"}'                           |
      | 'PATCH'  | 'paramYear'           | '2000'                 | '{"paramYear":"2000"}'                           |
      | 'DELETE' | 'paramYear'           | '2000'                 | '{"paramYear":"2000"}'                           |
      | 'GET'    | 'paramEnumAB'         | 'A'                    | '{"paramEnumAB":"A"}'                            |
      | 'HEAD'   | 'paramEnumAB'         | 'A'                    | '{"paramEnumAB":"A"}'                            |
      | 'POST'   | 'paramEnumAB'         | 'A'                    | '{"paramEnumAB":"A"}'                            |
      | 'PUT'    | 'paramEnumAB'         | 'A'                    | '{"paramEnumAB":"A"}'                            |
      | 'PATCH'  | 'paramEnumAB'         | 'A'                    | '{"paramEnumAB":"A"}'                            |
      | 'DELETE' | 'paramEnumAB'         | 'A'                    | '{"paramEnumAB":"A"}'                            |
      | 'GET'    | 'paramEnumLowerAB'    | 'b'                    | '{"paramEnumLowerAB":"b"}'                       |
      | 'HEAD'   | 'paramEnumLowerAB'    | 'b'                    | '{"paramEnumLowerAB":"b"}'                       |
      | 'POST'   | 'paramEnumLowerAB'    | 'b'                    | '{"paramEnumLowerAB":"b"}'                       |
      | 'PUT'    | 'paramEnumLowerAB'    | 'b'                    | '{"paramEnumLowerAB":"b"}'                       |
      | 'PATCH'  | 'paramEnumLowerAB'    | 'b'                    | '{"paramEnumLowerAB":"b"}'                       |
      | 'DELETE' | 'paramEnumLowerAB'    | 'b'                    | '{"paramEnumLowerAB":"b"}'                       |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/optional-dto-attribute?<param>=<value> endpoint (RequestParamUnconstrainedRestApi.class)
    this endpoint receives the request parameter in an optional dto attribute, both having the same name
    and the service correctly maps the sent value with the declared data type, responding with a success message.

    Given the endpoint '/api/v1/request-parameter/optional-dto-attribute'
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | param                 | value                  | received value                                   |
      | 'GET'    | 'paramString'         | 'id'                   | '{"paramString":"id"}'                           |
      | 'HEAD'   | 'paramString'         | 'id'                   | '{"paramString":"id"}'                           |
      | 'POST'   | 'paramString'         | 'id'                   | '{"paramString":"id"}'                           |
      | 'PUT'    | 'paramString'         | 'id'                   | '{"paramString":"id"}'                           |
      | 'PATCH'  | 'paramString'         | 'id'                   | '{"paramString":"id"}'                           |
      | 'DELETE' | 'paramString'         | 'id'                   | '{"paramString":"id"}'                           |
      | 'GET'    | 'paramInteger'        | '2'                    | '{"paramInteger":2}'                             |
      | 'HEAD'   | 'paramInteger'        | '2'                    | '{"paramInteger":2}'                             |
      | 'POST'   | 'paramInteger'        | '2'                    | '{"paramInteger":2}'                             |
      | 'PUT'    | 'paramInteger'        | '2'                    | '{"paramInteger":2}'                             |
      | 'PATCH'  | 'paramInteger'        | '2'                    | '{"paramInteger":2}'                             |
      | 'DELETE' | 'paramInteger'        | '2'                    | '{"paramInteger":2}'                             |
      | 'GET'    | 'paramLong'           | '2'                    | '{"paramLong":2}'                                |
      | 'HEAD'   | 'paramLong'           | '2'                    | '{"paramLong":2}'                                |
      | 'POST'   | 'paramLong'           | '2'                    | '{"paramLong":2}'                                |
      | 'PUT'    | 'paramLong'           | '2'                    | '{"paramLong":2}'                                |
      | 'PATCH'  | 'paramLong'           | '2'                    | '{"paramLong":2}'                                |
      | 'DELETE' | 'paramLong'           | '2'                    | '{"paramLong":2}'                                |
      | 'GET'    | 'paramBigInteger'     | '2'                    | '{"paramBigInteger":2}'                          |
      | 'HEAD'   | 'paramBigInteger'     | '2'                    | '{"paramBigInteger":2}'                          |
      | 'POST'   | 'paramBigInteger'     | '2'                    | '{"paramBigInteger":2}'                          |
      | 'PUT'    | 'paramBigInteger'     | '2'                    | '{"paramBigInteger":2}'                          |
      | 'PATCH'  | 'paramBigInteger'     | '2'                    | '{"paramBigInteger":2}'                          |
      | 'DELETE' | 'paramBigInteger'     | '2'                    | '{"paramBigInteger":2}'                          |
      | 'GET'    | 'paramFloat'          | '2.1'                  | '{"paramFloat":2.1}'                             |
      | 'HEAD'   | 'paramFloat'          | '2.1'                  | '{"paramFloat":2.1}'                             |
      | 'POST'   | 'paramFloat'          | '2.1'                  | '{"paramFloat":2.1}'                             |
      | 'PUT'    | 'paramFloat'          | '2.1'                  | '{"paramFloat":2.1}'                             |
      | 'PATCH'  | 'paramFloat'          | '2.1'                  | '{"paramFloat":2.1}'                             |
      | 'DELETE' | 'paramFloat'          | '2.1'                  | '{"paramFloat":2.1}'                             |
      | 'GET'    | 'paramDouble'         | '2.1'                  | '{"paramDouble":2.1}'                            |
      | 'HEAD'   | 'paramDouble'         | '2.1'                  | '{"paramDouble":2.1}'                            |
      | 'POST'   | 'paramDouble'         | '2.1'                  | '{"paramDouble":2.1}'                            |
      | 'PUT'    | 'paramDouble'         | '2.1'                  | '{"paramDouble":2.1}'                            |
      | 'PATCH'  | 'paramDouble'         | '2.1'                  | '{"paramDouble":2.1}'                            |
      | 'DELETE' | 'paramDouble'         | '2.1'                  | '{"paramDouble":2.1}'                            |
      | 'GET'    | 'paramBigDecimal'     | '2.1'                  | '{"paramBigDecimal":2.1}'                        |
      | 'HEAD'   | 'paramBigDecimal'     | '2.1'                  | '{"paramBigDecimal":2.1}'                        |
      | 'POST'   | 'paramBigDecimal'     | '2.1'                  | '{"paramBigDecimal":2.1}'                        |
      | 'PUT'    | 'paramBigDecimal'     | '2.1'                  | '{"paramBigDecimal":2.1}'                        |
      | 'PATCH'  | 'paramBigDecimal'     | '2.1'                  | '{"paramBigDecimal":2.1}'                        |
      | 'DELETE' | 'paramBigDecimal'     | '2.1'                  | '{"paramBigDecimal":2.1}'                        |
      | 'GET'    | 'paramBoolean'        | 'true'                 | '{"paramBoolean":true}'                          |
      | 'HEAD'   | 'paramBoolean'        | 'true'                 | '{"paramBoolean":true}'                          |
      | 'POST'   | 'paramBoolean'        | 'true'                 | '{"paramBoolean":true}'                          |
      | 'PUT'    | 'paramBoolean'        | 'true'                 | '{"paramBoolean":true}'                          |
      | 'PATCH'  | 'paramBoolean'        | 'true'                 | '{"paramBoolean":true}'                          |
      | 'DELETE' | 'paramBoolean'        | 'true'                 | '{"paramBoolean":true}'                          |
      | 'GET'    | 'paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'HEAD'   | 'paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'POST'   | 'paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'PUT'    | 'paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'PATCH'  | 'paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'DELETE' | 'paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}' |
      | 'GET'    | 'paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'HEAD'   | 'paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'POST'   | 'paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'PUT'    | 'paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'PATCH'  | 'paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'DELETE' | 'paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"paramLocalDateTime":"2000-01-01T00:00:00Z"}'  |
      | 'GET'    | 'paramLocalDate'      | '2000-01-01'           | '{"paramLocalDate":"2000-01-01"}'                |
      | 'HEAD'   | 'paramLocalDate'      | '2000-01-01'           | '{"paramLocalDate":"2000-01-01"}'                |
      | 'POST'   | 'paramLocalDate'      | '2000-01-01'           | '{"paramLocalDate":"2000-01-01"}'                |
      | 'PUT'    | 'paramLocalDate'      | '2000-01-01'           | '{"paramLocalDate":"2000-01-01"}'                |
      | 'PATCH'  | 'paramLocalDate'      | '2000-01-01'           | '{"paramLocalDate":"2000-01-01"}'                |
      | 'DELETE' | 'paramLocalDate'      | '2000-01-01'           | '{"paramLocalDate":"2000-01-01"}'                |
      | 'GET'    | 'paramLocalTime'      | '13:00:00'             | '{"paramLocalTime":"13:00:00"}'                  |
      | 'HEAD'   | 'paramLocalTime'      | '13:00:00'             | '{"paramLocalTime":"13:00:00"}'                  |
      | 'POST'   | 'paramLocalTime'      | '13:00:00'             | '{"paramLocalTime":"13:00:00"}'                  |
      | 'PUT'    | 'paramLocalTime'      | '13:00:00'             | '{"paramLocalTime":"13:00:00"}'                  |
      | 'PATCH'  | 'paramLocalTime'      | '13:00:00'             | '{"paramLocalTime":"13:00:00"}'                  |
      | 'DELETE' | 'paramLocalTime'      | '13:00:00'             | '{"paramLocalTime":"13:00:00"}'                  |
      | 'GET'    | 'paramYear'           | '2000'                 | '{"paramYear":"2000"}'                           |
      | 'HEAD'   | 'paramYear'           | '2000'                 | '{"paramYear":"2000"}'                           |
      | 'POST'   | 'paramYear'           | '2000'                 | '{"paramYear":"2000"}'                           |
      | 'PUT'    | 'paramYear'           | '2000'                 | '{"paramYear":"2000"}'                           |
      | 'PATCH'  | 'paramYear'           | '2000'                 | '{"paramYear":"2000"}'                           |
      | 'DELETE' | 'paramYear'           | '2000'                 | '{"paramYear":"2000"}'                           |
      | 'GET'    | 'paramEnumAB'         | 'A'                    | '{"paramEnumAB":"A"}'                            |
      | 'HEAD'   | 'paramEnumAB'         | 'A'                    | '{"paramEnumAB":"A"}'                            |
      | 'POST'   | 'paramEnumAB'         | 'A'                    | '{"paramEnumAB":"A"}'                            |
      | 'PUT'    | 'paramEnumAB'         | 'A'                    | '{"paramEnumAB":"A"}'                            |
      | 'PATCH'  | 'paramEnumAB'         | 'A'                    | '{"paramEnumAB":"A"}'                            |
      | 'DELETE' | 'paramEnumAB'         | 'A'                    | '{"paramEnumAB":"A"}'                            |
      | 'GET'    | 'paramEnumLowerAB'    | 'b'                    | '{"paramEnumLowerAB":"b"}'                       |
      | 'HEAD'   | 'paramEnumLowerAB'    | 'b'                    | '{"paramEnumLowerAB":"b"}'                       |
      | 'POST'   | 'paramEnumLowerAB'    | 'b'                    | '{"paramEnumLowerAB":"b"}'                       |
      | 'PUT'    | 'paramEnumLowerAB'    | 'b'                    | '{"paramEnumLowerAB":"b"}'                       |
      | 'PATCH'  | 'paramEnumLowerAB'    | 'b'                    | '{"paramEnumLowerAB":"b"}'                       |
      | 'DELETE' | 'paramEnumLowerAB'    | 'b'                    | '{"paramEnumLowerAB":"b"}'                       |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/nested-dto-attribute?<param>=<value> endpoint (RequestParamUnconstrainedRestApi.class)
    this endpoint receives the request parameter in a nested dto attribute, both having the same name
    and the service correctly maps the sent value with the declared data type, responding with a success message.

    Given the endpoint '/api/v1/request-parameter/nested-dto-attribute'
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | param                        | value                  | received value                                              |
      | 'GET'    | 'nested.paramString'         | 'id'                   | '{"nested":{"paramString":"id"}}'                           |
      | 'HEAD'   | 'nested.paramString'         | 'id'                   | '{"nested":{"paramString":"id"}}'                           |
      | 'POST'   | 'nested.paramString'         | 'id'                   | '{"nested":{"paramString":"id"}}'                           |
      | 'PUT'    | 'nested.paramString'         | 'id'                   | '{"nested":{"paramString":"id"}}'                           |
      | 'PATCH'  | 'nested.paramString'         | 'id'                   | '{"nested":{"paramString":"id"}}'                           |
      | 'DELETE' | 'nested.paramString'         | 'id'                   | '{"nested":{"paramString":"id"}}'                           |
      | 'GET'    | 'nested.paramInteger'        | '2'                    | '{"nested":{"paramInteger":2}}'                             |
      | 'HEAD'   | 'nested.paramInteger'        | '2'                    | '{"nested":{"paramInteger":2}}'                             |
      | 'POST'   | 'nested.paramInteger'        | '2'                    | '{"nested":{"paramInteger":2}}'                             |
      | 'PUT'    | 'nested.paramInteger'        | '2'                    | '{"nested":{"paramInteger":2}}'                             |
      | 'PATCH'  | 'nested.paramInteger'        | '2'                    | '{"nested":{"paramInteger":2}}'                             |
      | 'DELETE' | 'nested.paramInteger'        | '2'                    | '{"nested":{"paramInteger":2}}'                             |
      | 'GET'    | 'nested.paramLong'           | '2'                    | '{"nested":{"paramLong":2}}'                                |
      | 'HEAD'   | 'nested.paramLong'           | '2'                    | '{"nested":{"paramLong":2}}'                                |
      | 'POST'   | 'nested.paramLong'           | '2'                    | '{"nested":{"paramLong":2}}'                                |
      | 'PUT'    | 'nested.paramLong'           | '2'                    | '{"nested":{"paramLong":2}}'                                |
      | 'PATCH'  | 'nested.paramLong'           | '2'                    | '{"nested":{"paramLong":2}}'                                |
      | 'DELETE' | 'nested.paramLong'           | '2'                    | '{"nested":{"paramLong":2}}'                                |
      | 'GET'    | 'nested.paramBigInteger'     | '2'                    | '{"nested":{"paramBigInteger":2}}'                          |
      | 'HEAD'   | 'nested.paramBigInteger'     | '2'                    | '{"nested":{"paramBigInteger":2}}'                          |
      | 'POST'   | 'nested.paramBigInteger'     | '2'                    | '{"nested":{"paramBigInteger":2}}'                          |
      | 'PUT'    | 'nested.paramBigInteger'     | '2'                    | '{"nested":{"paramBigInteger":2}}'                          |
      | 'PATCH'  | 'nested.paramBigInteger'     | '2'                    | '{"nested":{"paramBigInteger":2}}'                          |
      | 'DELETE' | 'nested.paramBigInteger'     | '2'                    | '{"nested":{"paramBigInteger":2}}'                          |
      | 'GET'    | 'nested.paramFloat'          | '2.1'                  | '{"nested":{"paramFloat":2.1}}'                             |
      | 'HEAD'   | 'nested.paramFloat'          | '2.1'                  | '{"nested":{"paramFloat":2.1}}'                             |
      | 'POST'   | 'nested.paramFloat'          | '2.1'                  | '{"nested":{"paramFloat":2.1}}'                             |
      | 'PUT'    | 'nested.paramFloat'          | '2.1'                  | '{"nested":{"paramFloat":2.1}}'                             |
      | 'PATCH'  | 'nested.paramFloat'          | '2.1'                  | '{"nested":{"paramFloat":2.1}}'                             |
      | 'DELETE' | 'nested.paramFloat'          | '2.1'                  | '{"nested":{"paramFloat":2.1}}'                             |
      | 'GET'    | 'nested.paramDouble'         | '2.1'                  | '{"nested":{"paramDouble":2.1}}'                            |
      | 'HEAD'   | 'nested.paramDouble'         | '2.1'                  | '{"nested":{"paramDouble":2.1}}'                            |
      | 'POST'   | 'nested.paramDouble'         | '2.1'                  | '{"nested":{"paramDouble":2.1}}'                            |
      | 'PUT'    | 'nested.paramDouble'         | '2.1'                  | '{"nested":{"paramDouble":2.1}}'                            |
      | 'PATCH'  | 'nested.paramDouble'         | '2.1'                  | '{"nested":{"paramDouble":2.1}}'                            |
      | 'DELETE' | 'nested.paramDouble'         | '2.1'                  | '{"nested":{"paramDouble":2.1}}'                            |
      | 'GET'    | 'nested.paramBigDecimal'     | '2.1'                  | '{"nested":{"paramBigDecimal":2.1}}'                        |
      | 'HEAD'   | 'nested.paramBigDecimal'     | '2.1'                  | '{"nested":{"paramBigDecimal":2.1}}'                        |
      | 'POST'   | 'nested.paramBigDecimal'     | '2.1'                  | '{"nested":{"paramBigDecimal":2.1}}'                        |
      | 'PUT'    | 'nested.paramBigDecimal'     | '2.1'                  | '{"nested":{"paramBigDecimal":2.1}}'                        |
      | 'PATCH'  | 'nested.paramBigDecimal'     | '2.1'                  | '{"nested":{"paramBigDecimal":2.1}}'                        |
      | 'DELETE' | 'nested.paramBigDecimal'     | '2.1'                  | '{"nested":{"paramBigDecimal":2.1}}'                        |
      | 'GET'    | 'nested.paramBoolean'        | 'true'                 | '{"nested":{"paramBoolean":true}}'                          |
      | 'HEAD'   | 'nested.paramBoolean'        | 'true'                 | '{"nested":{"paramBoolean":true}}'                          |
      | 'POST'   | 'nested.paramBoolean'        | 'true'                 | '{"nested":{"paramBoolean":true}}'                          |
      | 'PUT'    | 'nested.paramBoolean'        | 'true'                 | '{"nested":{"paramBoolean":true}}'                          |
      | 'PATCH'  | 'nested.paramBoolean'        | 'true'                 | '{"nested":{"paramBoolean":true}}'                          |
      | 'DELETE' | 'nested.paramBoolean'        | 'true'                 | '{"nested":{"paramBoolean":true}}'                          |
      | 'GET'    | 'nested.paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"nested":{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}}' |
      | 'HEAD'   | 'nested.paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"nested":{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}}' |
      | 'POST'   | 'nested.paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"nested":{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}}' |
      | 'PUT'    | 'nested.paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"nested":{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}}' |
      | 'PATCH'  | 'nested.paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"nested":{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}}' |
      | 'DELETE' | 'nested.paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"nested":{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}}' |
      | 'GET'    | 'nested.paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"nested":{"paramLocalDateTime":"2000-01-01T00:00:00Z"}}'  |
      | 'HEAD'   | 'nested.paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"nested":{"paramLocalDateTime":"2000-01-01T00:00:00Z"}}'  |
      | 'POST'   | 'nested.paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"nested":{"paramLocalDateTime":"2000-01-01T00:00:00Z"}}'  |
      | 'PUT'    | 'nested.paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"nested":{"paramLocalDateTime":"2000-01-01T00:00:00Z"}}'  |
      | 'PATCH'  | 'nested.paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"nested":{"paramLocalDateTime":"2000-01-01T00:00:00Z"}}'  |
      | 'DELETE' | 'nested.paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"nested":{"paramLocalDateTime":"2000-01-01T00:00:00Z"}}'  |
      | 'GET'    | 'nested.paramLocalDate'      | '2000-01-01'           | '{"nested":{"paramLocalDate":"2000-01-01"}}'                |
      | 'HEAD'   | 'nested.paramLocalDate'      | '2000-01-01'           | '{"nested":{"paramLocalDate":"2000-01-01"}}'                |
      | 'POST'   | 'nested.paramLocalDate'      | '2000-01-01'           | '{"nested":{"paramLocalDate":"2000-01-01"}}'                |
      | 'PUT'    | 'nested.paramLocalDate'      | '2000-01-01'           | '{"nested":{"paramLocalDate":"2000-01-01"}}'                |
      | 'PATCH'  | 'nested.paramLocalDate'      | '2000-01-01'           | '{"nested":{"paramLocalDate":"2000-01-01"}}'                |
      | 'DELETE' | 'nested.paramLocalDate'      | '2000-01-01'           | '{"nested":{"paramLocalDate":"2000-01-01"}}'                |
      | 'GET'    | 'nested.paramLocalTime'      | '13:00:00'             | '{"nested":{"paramLocalTime":"13:00:00"}}'                  |
      | 'HEAD'   | 'nested.paramLocalTime'      | '13:00:00'             | '{"nested":{"paramLocalTime":"13:00:00"}}'                  |
      | 'POST'   | 'nested.paramLocalTime'      | '13:00:00'             | '{"nested":{"paramLocalTime":"13:00:00"}}'                  |
      | 'PUT'    | 'nested.paramLocalTime'      | '13:00:00'             | '{"nested":{"paramLocalTime":"13:00:00"}}'                  |
      | 'PATCH'  | 'nested.paramLocalTime'      | '13:00:00'             | '{"nested":{"paramLocalTime":"13:00:00"}}'                  |
      | 'DELETE' | 'nested.paramLocalTime'      | '13:00:00'             | '{"nested":{"paramLocalTime":"13:00:00"}}'                  |
      | 'GET'    | 'nested.paramYear'           | '2000'                 | '{"nested":{"paramYear":"2000"}}'                           |
      | 'HEAD'   | 'nested.paramYear'           | '2000'                 | '{"nested":{"paramYear":"2000"}}'                           |
      | 'POST'   | 'nested.paramYear'           | '2000'                 | '{"nested":{"paramYear":"2000"}}'                           |
      | 'PUT'    | 'nested.paramYear'           | '2000'                 | '{"nested":{"paramYear":"2000"}}'                           |
      | 'PATCH'  | 'nested.paramYear'           | '2000'                 | '{"nested":{"paramYear":"2000"}}'                           |
      | 'DELETE' | 'nested.paramYear'           | '2000'                 | '{"nested":{"paramYear":"2000"}}'                           |
      | 'GET'    | 'nested.paramEnumAB'         | 'A'                    | '{"nested":{"paramEnumAB":"A"}}'                            |
      | 'HEAD'   | 'nested.paramEnumAB'         | 'A'                    | '{"nested":{"paramEnumAB":"A"}}'                            |
      | 'POST'   | 'nested.paramEnumAB'         | 'A'                    | '{"nested":{"paramEnumAB":"A"}}'                            |
      | 'PUT'    | 'nested.paramEnumAB'         | 'A'                    | '{"nested":{"paramEnumAB":"A"}}'                            |
      | 'PATCH'  | 'nested.paramEnumAB'         | 'A'                    | '{"nested":{"paramEnumAB":"A"}}'                            |
      | 'DELETE' | 'nested.paramEnumAB'         | 'A'                    | '{"nested":{"paramEnumAB":"A"}}'                            |
      | 'GET'    | 'nested.paramEnumLowerAB'    | 'b'                    | '{"nested":{"paramEnumLowerAB":"b"}}'                       |
      | 'HEAD'   | 'nested.paramEnumLowerAB'    | 'b'                    | '{"nested":{"paramEnumLowerAB":"b"}}'                       |
      | 'POST'   | 'nested.paramEnumLowerAB'    | 'b'                    | '{"nested":{"paramEnumLowerAB":"b"}}'                       |
      | 'PUT'    | 'nested.paramEnumLowerAB'    | 'b'                    | '{"nested":{"paramEnumLowerAB":"b"}}'                       |
      | 'PATCH'  | 'nested.paramEnumLowerAB'    | 'b'                    | '{"nested":{"paramEnumLowerAB":"b"}}'                       |
      | 'DELETE' | 'nested.paramEnumLowerAB'    | 'b'                    | '{"nested":{"paramEnumLowerAB":"b"}}'                       |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/nested-optional-dto-attribute?<param>=<value> endpoint (RequestParamUnconstrainedRestApi.class)
     this endpoint receives the request parameter in a nested optional dto attribute, both having the same name
     and the service correctly maps the sent value with the declared data type, responding with a success message.

    Given the endpoint '/api/v1/request-parameter/nested-optional-dto-attribute'
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | param                        | value                  | received value                                              |
      | 'GET'    | 'nested.paramString'         | 'id'                   | '{"nested":{"paramString":"id"}}'                           |
      | 'HEAD'   | 'nested.paramString'         | 'id'                   | '{"nested":{"paramString":"id"}}'                           |
      | 'POST'   | 'nested.paramString'         | 'id'                   | '{"nested":{"paramString":"id"}}'                           |
      | 'PUT'    | 'nested.paramString'         | 'id'                   | '{"nested":{"paramString":"id"}}'                           |
      | 'PATCH'  | 'nested.paramString'         | 'id'                   | '{"nested":{"paramString":"id"}}'                           |
      | 'DELETE' | 'nested.paramString'         | 'id'                   | '{"nested":{"paramString":"id"}}'                           |
      | 'GET'    | 'nested.paramInteger'        | '2'                    | '{"nested":{"paramInteger":2}}'                             |
      | 'HEAD'   | 'nested.paramInteger'        | '2'                    | '{"nested":{"paramInteger":2}}'                             |
      | 'POST'   | 'nested.paramInteger'        | '2'                    | '{"nested":{"paramInteger":2}}'                             |
      | 'PUT'    | 'nested.paramInteger'        | '2'                    | '{"nested":{"paramInteger":2}}'                             |
      | 'PATCH'  | 'nested.paramInteger'        | '2'                    | '{"nested":{"paramInteger":2}}'                             |
      | 'DELETE' | 'nested.paramInteger'        | '2'                    | '{"nested":{"paramInteger":2}}'                             |
      | 'GET'    | 'nested.paramLong'           | '2'                    | '{"nested":{"paramLong":2}}'                                |
      | 'HEAD'   | 'nested.paramLong'           | '2'                    | '{"nested":{"paramLong":2}}'                                |
      | 'POST'   | 'nested.paramLong'           | '2'                    | '{"nested":{"paramLong":2}}'                                |
      | 'PUT'    | 'nested.paramLong'           | '2'                    | '{"nested":{"paramLong":2}}'                                |
      | 'PATCH'  | 'nested.paramLong'           | '2'                    | '{"nested":{"paramLong":2}}'                                |
      | 'DELETE' | 'nested.paramLong'           | '2'                    | '{"nested":{"paramLong":2}}'                                |
      | 'GET'    | 'nested.paramBigInteger'     | '2'                    | '{"nested":{"paramBigInteger":2}}'                          |
      | 'HEAD'   | 'nested.paramBigInteger'     | '2'                    | '{"nested":{"paramBigInteger":2}}'                          |
      | 'POST'   | 'nested.paramBigInteger'     | '2'                    | '{"nested":{"paramBigInteger":2}}'                          |
      | 'PUT'    | 'nested.paramBigInteger'     | '2'                    | '{"nested":{"paramBigInteger":2}}'                          |
      | 'PATCH'  | 'nested.paramBigInteger'     | '2'                    | '{"nested":{"paramBigInteger":2}}'                          |
      | 'DELETE' | 'nested.paramBigInteger'     | '2'                    | '{"nested":{"paramBigInteger":2}}'                          |
      | 'GET'    | 'nested.paramFloat'          | '2.1'                  | '{"nested":{"paramFloat":2.1}}'                             |
      | 'HEAD'   | 'nested.paramFloat'          | '2.1'                  | '{"nested":{"paramFloat":2.1}}'                             |
      | 'POST'   | 'nested.paramFloat'          | '2.1'                  | '{"nested":{"paramFloat":2.1}}'                             |
      | 'PUT'    | 'nested.paramFloat'          | '2.1'                  | '{"nested":{"paramFloat":2.1}}'                             |
      | 'PATCH'  | 'nested.paramFloat'          | '2.1'                  | '{"nested":{"paramFloat":2.1}}'                             |
      | 'DELETE' | 'nested.paramFloat'          | '2.1'                  | '{"nested":{"paramFloat":2.1}}'                             |
      | 'GET'    | 'nested.paramDouble'         | '2.1'                  | '{"nested":{"paramDouble":2.1}}'                            |
      | 'HEAD'   | 'nested.paramDouble'         | '2.1'                  | '{"nested":{"paramDouble":2.1}}'                            |
      | 'POST'   | 'nested.paramDouble'         | '2.1'                  | '{"nested":{"paramDouble":2.1}}'                            |
      | 'PUT'    | 'nested.paramDouble'         | '2.1'                  | '{"nested":{"paramDouble":2.1}}'                            |
      | 'PATCH'  | 'nested.paramDouble'         | '2.1'                  | '{"nested":{"paramDouble":2.1}}'                            |
      | 'DELETE' | 'nested.paramDouble'         | '2.1'                  | '{"nested":{"paramDouble":2.1}}'                            |
      | 'GET'    | 'nested.paramBigDecimal'     | '2.1'                  | '{"nested":{"paramBigDecimal":2.1}}'                        |
      | 'HEAD'   | 'nested.paramBigDecimal'     | '2.1'                  | '{"nested":{"paramBigDecimal":2.1}}'                        |
      | 'POST'   | 'nested.paramBigDecimal'     | '2.1'                  | '{"nested":{"paramBigDecimal":2.1}}'                        |
      | 'PUT'    | 'nested.paramBigDecimal'     | '2.1'                  | '{"nested":{"paramBigDecimal":2.1}}'                        |
      | 'PATCH'  | 'nested.paramBigDecimal'     | '2.1'                  | '{"nested":{"paramBigDecimal":2.1}}'                        |
      | 'DELETE' | 'nested.paramBigDecimal'     | '2.1'                  | '{"nested":{"paramBigDecimal":2.1}}'                        |
      | 'GET'    | 'nested.paramBoolean'        | 'true'                 | '{"nested":{"paramBoolean":true}}'                          |
      | 'HEAD'   | 'nested.paramBoolean'        | 'true'                 | '{"nested":{"paramBoolean":true}}'                          |
      | 'POST'   | 'nested.paramBoolean'        | 'true'                 | '{"nested":{"paramBoolean":true}}'                          |
      | 'PUT'    | 'nested.paramBoolean'        | 'true'                 | '{"nested":{"paramBoolean":true}}'                          |
      | 'PATCH'  | 'nested.paramBoolean'        | 'true'                 | '{"nested":{"paramBoolean":true}}'                          |
      | 'DELETE' | 'nested.paramBoolean'        | 'true'                 | '{"nested":{"paramBoolean":true}}'                          |
      | 'GET'    | 'nested.paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"nested":{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}}' |
      | 'HEAD'   | 'nested.paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"nested":{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}}' |
      | 'POST'   | 'nested.paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"nested":{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}}' |
      | 'PUT'    | 'nested.paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"nested":{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}}' |
      | 'PATCH'  | 'nested.paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"nested":{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}}' |
      | 'DELETE' | 'nested.paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"nested":{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}}' |
      | 'GET'    | 'nested.paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"nested":{"paramLocalDateTime":"2000-01-01T00:00:00Z"}}'  |
      | 'HEAD'   | 'nested.paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"nested":{"paramLocalDateTime":"2000-01-01T00:00:00Z"}}'  |
      | 'POST'   | 'nested.paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"nested":{"paramLocalDateTime":"2000-01-01T00:00:00Z"}}'  |
      | 'PUT'    | 'nested.paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"nested":{"paramLocalDateTime":"2000-01-01T00:00:00Z"}}'  |
      | 'PATCH'  | 'nested.paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"nested":{"paramLocalDateTime":"2000-01-01T00:00:00Z"}}'  |
      | 'DELETE' | 'nested.paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"nested":{"paramLocalDateTime":"2000-01-01T00:00:00Z"}}'  |
      | 'GET'    | 'nested.paramLocalDate'      | '2000-01-01'           | '{"nested":{"paramLocalDate":"2000-01-01"}}'                |
      | 'HEAD'   | 'nested.paramLocalDate'      | '2000-01-01'           | '{"nested":{"paramLocalDate":"2000-01-01"}}'                |
      | 'POST'   | 'nested.paramLocalDate'      | '2000-01-01'           | '{"nested":{"paramLocalDate":"2000-01-01"}}'                |
      | 'PUT'    | 'nested.paramLocalDate'      | '2000-01-01'           | '{"nested":{"paramLocalDate":"2000-01-01"}}'                |
      | 'PATCH'  | 'nested.paramLocalDate'      | '2000-01-01'           | '{"nested":{"paramLocalDate":"2000-01-01"}}'                |
      | 'DELETE' | 'nested.paramLocalDate'      | '2000-01-01'           | '{"nested":{"paramLocalDate":"2000-01-01"}}'                |
      | 'GET'    | 'nested.paramLocalTime'      | '13:00:00'             | '{"nested":{"paramLocalTime":"13:00:00"}}'                  |
      | 'HEAD'   | 'nested.paramLocalTime'      | '13:00:00'             | '{"nested":{"paramLocalTime":"13:00:00"}}'                  |
      | 'POST'   | 'nested.paramLocalTime'      | '13:00:00'             | '{"nested":{"paramLocalTime":"13:00:00"}}'                  |
      | 'PUT'    | 'nested.paramLocalTime'      | '13:00:00'             | '{"nested":{"paramLocalTime":"13:00:00"}}'                  |
      | 'PATCH'  | 'nested.paramLocalTime'      | '13:00:00'             | '{"nested":{"paramLocalTime":"13:00:00"}}'                  |
      | 'DELETE' | 'nested.paramLocalTime'      | '13:00:00'             | '{"nested":{"paramLocalTime":"13:00:00"}}'                  |
      | 'GET'    | 'nested.paramYear'           | '2000'                 | '{"nested":{"paramYear":"2000"}}'                           |
      | 'HEAD'   | 'nested.paramYear'           | '2000'                 | '{"nested":{"paramYear":"2000"}}'                           |
      | 'POST'   | 'nested.paramYear'           | '2000'                 | '{"nested":{"paramYear":"2000"}}'                           |
      | 'PUT'    | 'nested.paramYear'           | '2000'                 | '{"nested":{"paramYear":"2000"}}'                           |
      | 'PATCH'  | 'nested.paramYear'           | '2000'                 | '{"nested":{"paramYear":"2000"}}'                           |
      | 'DELETE' | 'nested.paramYear'           | '2000'                 | '{"nested":{"paramYear":"2000"}}'                           |
      | 'GET'    | 'nested.paramEnumAB'         | 'A'                    | '{"nested":{"paramEnumAB":"A"}}'                            |
      | 'HEAD'   | 'nested.paramEnumAB'         | 'A'                    | '{"nested":{"paramEnumAB":"A"}}'                            |
      | 'POST'   | 'nested.paramEnumAB'         | 'A'                    | '{"nested":{"paramEnumAB":"A"}}'                            |
      | 'PUT'    | 'nested.paramEnumAB'         | 'A'                    | '{"nested":{"paramEnumAB":"A"}}'                            |
      | 'PATCH'  | 'nested.paramEnumAB'         | 'A'                    | '{"nested":{"paramEnumAB":"A"}}'                            |
      | 'DELETE' | 'nested.paramEnumAB'         | 'A'                    | '{"nested":{"paramEnumAB":"A"}}'                            |
      | 'GET'    | 'nested.paramEnumLowerAB'    | 'b'                    | '{"nested":{"paramEnumLowerAB":"b"}}'                       |
      | 'HEAD'   | 'nested.paramEnumLowerAB'    | 'b'                    | '{"nested":{"paramEnumLowerAB":"b"}}'                       |
      | 'POST'   | 'nested.paramEnumLowerAB'    | 'b'                    | '{"nested":{"paramEnumLowerAB":"b"}}'                       |
      | 'PUT'    | 'nested.paramEnumLowerAB'    | 'b'                    | '{"nested":{"paramEnumLowerAB":"b"}}'                       |
      | 'PATCH'  | 'nested.paramEnumLowerAB'    | 'b'                    | '{"nested":{"paramEnumLowerAB":"b"}}'                       |
      | 'DELETE' | 'nested.paramEnumLowerAB'    | 'b'                    | '{"nested":{"paramEnumLowerAB":"b"}}'                       |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/nested-dto-attribute?<param>=<value> endpoint (RequestParamUnconstrainedRestApi.class)
    this endpoint receives the request parameter in a nested dto attribute from an array, both having the same name
    and the service correctly maps the sent value with the declared data type, responding with a success message.

    Given the endpoint '/api/v1/request-parameter/nested-dto-attribute'
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | param                          | value                  | received value                                               |
      | 'GET'    | 'array[0].paramString'         | 'id'                   | '{"array":[{"paramString":"id"}]}'                           |
      | 'HEAD'   | 'array[0].paramString'         | 'id'                   | '{"array":[{"paramString":"id"}]}'                           |
      | 'POST'   | 'array[0].paramString'         | 'id'                   | '{"array":[{"paramString":"id"}]}'                           |
      | 'PUT'    | 'array[0].paramString'         | 'id'                   | '{"array":[{"paramString":"id"}]}'                           |
      | 'PATCH'  | 'array[0].paramString'         | 'id'                   | '{"array":[{"paramString":"id"}]}'                           |
      | 'DELETE' | 'array[0].paramString'         | 'id'                   | '{"array":[{"paramString":"id"}]}'                           |
      | 'GET'    | 'array[0].paramInteger'        | '2'                    | '{"array":[{"paramInteger":2}]}'                             |
      | 'HEAD'   | 'array[0].paramInteger'        | '2'                    | '{"array":[{"paramInteger":2}]}'                             |
      | 'POST'   | 'array[0].paramInteger'        | '2'                    | '{"array":[{"paramInteger":2}]}'                             |
      | 'PUT'    | 'array[0].paramInteger'        | '2'                    | '{"array":[{"paramInteger":2}]}'                             |
      | 'PATCH'  | 'array[0].paramInteger'        | '2'                    | '{"array":[{"paramInteger":2}]}'                             |
      | 'DELETE' | 'array[0].paramInteger'        | '2'                    | '{"array":[{"paramInteger":2}]}'                             |
      | 'GET'    | 'array[0].paramLong'           | '2'                    | '{"array":[{"paramLong":2}]}'                                |
      | 'HEAD'   | 'array[0].paramLong'           | '2'                    | '{"array":[{"paramLong":2}]}'                                |
      | 'POST'   | 'array[0].paramLong'           | '2'                    | '{"array":[{"paramLong":2}]}'                                |
      | 'PUT'    | 'array[0].paramLong'           | '2'                    | '{"array":[{"paramLong":2}]}'                                |
      | 'PATCH'  | 'array[0].paramLong'           | '2'                    | '{"array":[{"paramLong":2}]}'                                |
      | 'DELETE' | 'array[0].paramLong'           | '2'                    | '{"array":[{"paramLong":2}]}'                                |
      | 'GET'    | 'array[0].paramBigInteger'     | '2'                    | '{"array":[{"paramBigInteger":2}]}'                          |
      | 'HEAD'   | 'array[0].paramBigInteger'     | '2'                    | '{"array":[{"paramBigInteger":2}]}'                          |
      | 'POST'   | 'array[0].paramBigInteger'     | '2'                    | '{"array":[{"paramBigInteger":2}]}'                          |
      | 'PUT'    | 'array[0].paramBigInteger'     | '2'                    | '{"array":[{"paramBigInteger":2}]}'                          |
      | 'PATCH'  | 'array[0].paramBigInteger'     | '2'                    | '{"array":[{"paramBigInteger":2}]}'                          |
      | 'DELETE' | 'array[0].paramBigInteger'     | '2'                    | '{"array":[{"paramBigInteger":2}]}'                          |
      | 'GET'    | 'array[0].paramFloat'          | '2.1'                  | '{"array":[{"paramFloat":2.1}]}'                             |
      | 'HEAD'   | 'array[0].paramFloat'          | '2.1'                  | '{"array":[{"paramFloat":2.1}]}'                             |
      | 'POST'   | 'array[0].paramFloat'          | '2.1'                  | '{"array":[{"paramFloat":2.1}]}'                             |
      | 'PUT'    | 'array[0].paramFloat'          | '2.1'                  | '{"array":[{"paramFloat":2.1}]}'                             |
      | 'PATCH'  | 'array[0].paramFloat'          | '2.1'                  | '{"array":[{"paramFloat":2.1}]}'                             |
      | 'DELETE' | 'array[0].paramFloat'          | '2.1'                  | '{"array":[{"paramFloat":2.1}]}'                             |
      | 'GET'    | 'array[0].paramDouble'         | '2.1'                  | '{"array":[{"paramDouble":2.1}]}'                            |
      | 'HEAD'   | 'array[0].paramDouble'         | '2.1'                  | '{"array":[{"paramDouble":2.1}]}'                            |
      | 'POST'   | 'array[0].paramDouble'         | '2.1'                  | '{"array":[{"paramDouble":2.1}]}'                            |
      | 'PUT'    | 'array[0].paramDouble'         | '2.1'                  | '{"array":[{"paramDouble":2.1}]}'                            |
      | 'PATCH'  | 'array[0].paramDouble'         | '2.1'                  | '{"array":[{"paramDouble":2.1}]}'                            |
      | 'DELETE' | 'array[0].paramDouble'         | '2.1'                  | '{"array":[{"paramDouble":2.1}]}'                            |
      | 'GET'    | 'array[0].paramBigDecimal'     | '2.1'                  | '{"array":[{"paramBigDecimal":2.1}]}'                        |
      | 'HEAD'   | 'array[0].paramBigDecimal'     | '2.1'                  | '{"array":[{"paramBigDecimal":2.1}]}'                        |
      | 'POST'   | 'array[0].paramBigDecimal'     | '2.1'                  | '{"array":[{"paramBigDecimal":2.1}]}'                        |
      | 'PUT'    | 'array[0].paramBigDecimal'     | '2.1'                  | '{"array":[{"paramBigDecimal":2.1}]}'                        |
      | 'PATCH'  | 'array[0].paramBigDecimal'     | '2.1'                  | '{"array":[{"paramBigDecimal":2.1}]}'                        |
      | 'DELETE' | 'array[0].paramBigDecimal'     | '2.1'                  | '{"array":[{"paramBigDecimal":2.1}]}'                        |
      | 'GET'    | 'array[0].paramBoolean'        | 'true'                 | '{"array":[{"paramBoolean":true}]}'                          |
      | 'HEAD'   | 'array[0].paramBoolean'        | 'true'                 | '{"array":[{"paramBoolean":true}]}'                          |
      | 'POST'   | 'array[0].paramBoolean'        | 'true'                 | '{"array":[{"paramBoolean":true}]}'                          |
      | 'PUT'    | 'array[0].paramBoolean'        | 'true'                 | '{"array":[{"paramBoolean":true}]}'                          |
      | 'PATCH'  | 'array[0].paramBoolean'        | 'true'                 | '{"array":[{"paramBoolean":true}]}'                          |
      | 'DELETE' | 'array[0].paramBoolean'        | 'true'                 | '{"array":[{"paramBoolean":true}]}'                          |
      | 'GET'    | 'array[0].paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"array":[{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}]}' |
      | 'HEAD'   | 'array[0].paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"array":[{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}]}' |
      | 'POST'   | 'array[0].paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"array":[{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}]}' |
      | 'PUT'    | 'array[0].paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"array":[{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}]}' |
      | 'PATCH'  | 'array[0].paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"array":[{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}]}' |
      | 'DELETE' | 'array[0].paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"array":[{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}]}' |
      | 'GET'    | 'array[0].paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"array":[{"paramLocalDateTime":"2000-01-01T00:00:00Z"}]}'  |
      | 'HEAD'   | 'array[0].paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"array":[{"paramLocalDateTime":"2000-01-01T00:00:00Z"}]}'  |
      | 'POST'   | 'array[0].paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"array":[{"paramLocalDateTime":"2000-01-01T00:00:00Z"}]}'  |
      | 'PUT'    | 'array[0].paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"array":[{"paramLocalDateTime":"2000-01-01T00:00:00Z"}]}'  |
      | 'PATCH'  | 'array[0].paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"array":[{"paramLocalDateTime":"2000-01-01T00:00:00Z"}]}'  |
      | 'DELETE' | 'array[0].paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"array":[{"paramLocalDateTime":"2000-01-01T00:00:00Z"}]}'  |
      | 'GET'    | 'array[0].paramLocalDate'      | '2000-01-01'           | '{"array":[{"paramLocalDate":"2000-01-01"}]}'                |
      | 'HEAD'   | 'array[0].paramLocalDate'      | '2000-01-01'           | '{"array":[{"paramLocalDate":"2000-01-01"}]}'                |
      | 'POST'   | 'array[0].paramLocalDate'      | '2000-01-01'           | '{"array":[{"paramLocalDate":"2000-01-01"}]}'                |
      | 'PUT'    | 'array[0].paramLocalDate'      | '2000-01-01'           | '{"array":[{"paramLocalDate":"2000-01-01"}]}'                |
      | 'PATCH'  | 'array[0].paramLocalDate'      | '2000-01-01'           | '{"array":[{"paramLocalDate":"2000-01-01"}]}'                |
      | 'DELETE' | 'array[0].paramLocalDate'      | '2000-01-01'           | '{"array":[{"paramLocalDate":"2000-01-01"}]}'                |
      | 'GET'    | 'array[0].paramLocalTime'      | '13:00:00'             | '{"array":[{"paramLocalTime":"13:00:00"}]}'                  |
      | 'HEAD'   | 'array[0].paramLocalTime'      | '13:00:00'             | '{"array":[{"paramLocalTime":"13:00:00"}]}'                  |
      | 'POST'   | 'array[0].paramLocalTime'      | '13:00:00'             | '{"array":[{"paramLocalTime":"13:00:00"}]}'                  |
      | 'PUT'    | 'array[0].paramLocalTime'      | '13:00:00'             | '{"array":[{"paramLocalTime":"13:00:00"}]}'                  |
      | 'PATCH'  | 'array[0].paramLocalTime'      | '13:00:00'             | '{"array":[{"paramLocalTime":"13:00:00"}]}'                  |
      | 'DELETE' | 'array[0].paramLocalTime'      | '13:00:00'             | '{"array":[{"paramLocalTime":"13:00:00"}]}'                  |
      | 'GET'    | 'array[0].paramYear'           | '2000'                 | '{"array":[{"paramYear":"2000"}]}'                           |
      | 'HEAD'   | 'array[0].paramYear'           | '2000'                 | '{"array":[{"paramYear":"2000"}]}'                           |
      | 'POST'   | 'array[0].paramYear'           | '2000'                 | '{"array":[{"paramYear":"2000"}]}'                           |
      | 'PUT'    | 'array[0].paramYear'           | '2000'                 | '{"array":[{"paramYear":"2000"}]}'                           |
      | 'PATCH'  | 'array[0].paramYear'           | '2000'                 | '{"array":[{"paramYear":"2000"}]}'                           |
      | 'DELETE' | 'array[0].paramYear'           | '2000'                 | '{"array":[{"paramYear":"2000"}]}'                           |
      | 'GET'    | 'array[0].paramEnumAB'         | 'A'                    | '{"array":[{"paramEnumAB":"A"}]}'                            |
      | 'HEAD'   | 'array[0].paramEnumAB'         | 'A'                    | '{"array":[{"paramEnumAB":"A"}]}'                            |
      | 'POST'   | 'array[0].paramEnumAB'         | 'A'                    | '{"array":[{"paramEnumAB":"A"}]}'                            |
      | 'PUT'    | 'array[0].paramEnumAB'         | 'A'                    | '{"array":[{"paramEnumAB":"A"}]}'                            |
      | 'PATCH'  | 'array[0].paramEnumAB'         | 'A'                    | '{"array":[{"paramEnumAB":"A"}]}'                            |
      | 'DELETE' | 'array[0].paramEnumAB'         | 'A'                    | '{"array":[{"paramEnumAB":"A"}]}'                            |
      | 'GET'    | 'array[0].paramEnumLowerAB'    | 'b'                    | '{"array":[{"paramEnumLowerAB":"b"}]}'                       |
      | 'HEAD'   | 'array[0].paramEnumLowerAB'    | 'b'                    | '{"array":[{"paramEnumLowerAB":"b"}]}'                       |
      | 'POST'   | 'array[0].paramEnumLowerAB'    | 'b'                    | '{"array":[{"paramEnumLowerAB":"b"}]}'                       |
      | 'PUT'    | 'array[0].paramEnumLowerAB'    | 'b'                    | '{"array":[{"paramEnumLowerAB":"b"}]}'                       |
      | 'PATCH'  | 'array[0].paramEnumLowerAB'    | 'b'                    | '{"array":[{"paramEnumLowerAB":"b"}]}'                       |
      | 'DELETE' | 'array[0].paramEnumLowerAB'    | 'b'                    | '{"array":[{"paramEnumLowerAB":"b"}]}'                       |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/nested-optional-dto-attribute?<param>=<value> endpoint (RequestParamUnconstrainedRestApi.class)
    this endpoint receives the request parameter in a nested optional dto attribute from an array, both having the same name
    and the service correctly maps the sent value with the declared data type, responding with a success message.

    Given the endpoint '/api/v1/request-parameter/nested-optional-dto-attribute'
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | param                          | value                  | received value                                               |
      | 'GET'    | 'array[0].paramString'         | 'id'                   | '{"array":[{"paramString":"id"}]}'                           |
      | 'HEAD'   | 'array[0].paramString'         | 'id'                   | '{"array":[{"paramString":"id"}]}'                           |
      | 'POST'   | 'array[0].paramString'         | 'id'                   | '{"array":[{"paramString":"id"}]}'                           |
      | 'PUT'    | 'array[0].paramString'         | 'id'                   | '{"array":[{"paramString":"id"}]}'                           |
      | 'PATCH'  | 'array[0].paramString'         | 'id'                   | '{"array":[{"paramString":"id"}]}'                           |
      | 'DELETE' | 'array[0].paramString'         | 'id'                   | '{"array":[{"paramString":"id"}]}'                           |
      | 'GET'    | 'array[0].paramInteger'        | '2'                    | '{"array":[{"paramInteger":2}]}'                             |
      | 'HEAD'   | 'array[0].paramInteger'        | '2'                    | '{"array":[{"paramInteger":2}]}'                             |
      | 'POST'   | 'array[0].paramInteger'        | '2'                    | '{"array":[{"paramInteger":2}]}'                             |
      | 'PUT'    | 'array[0].paramInteger'        | '2'                    | '{"array":[{"paramInteger":2}]}'                             |
      | 'PATCH'  | 'array[0].paramInteger'        | '2'                    | '{"array":[{"paramInteger":2}]}'                             |
      | 'DELETE' | 'array[0].paramInteger'        | '2'                    | '{"array":[{"paramInteger":2}]}'                             |
      | 'GET'    | 'array[0].paramLong'           | '2'                    | '{"array":[{"paramLong":2}]}'                                |
      | 'HEAD'   | 'array[0].paramLong'           | '2'                    | '{"array":[{"paramLong":2}]}'                                |
      | 'POST'   | 'array[0].paramLong'           | '2'                    | '{"array":[{"paramLong":2}]}'                                |
      | 'PUT'    | 'array[0].paramLong'           | '2'                    | '{"array":[{"paramLong":2}]}'                                |
      | 'PATCH'  | 'array[0].paramLong'           | '2'                    | '{"array":[{"paramLong":2}]}'                                |
      | 'DELETE' | 'array[0].paramLong'           | '2'                    | '{"array":[{"paramLong":2}]}'                                |
      | 'GET'    | 'array[0].paramBigInteger'     | '2'                    | '{"array":[{"paramBigInteger":2}]}'                          |
      | 'HEAD'   | 'array[0].paramBigInteger'     | '2'                    | '{"array":[{"paramBigInteger":2}]}'                          |
      | 'POST'   | 'array[0].paramBigInteger'     | '2'                    | '{"array":[{"paramBigInteger":2}]}'                          |
      | 'PUT'    | 'array[0].paramBigInteger'     | '2'                    | '{"array":[{"paramBigInteger":2}]}'                          |
      | 'PATCH'  | 'array[0].paramBigInteger'     | '2'                    | '{"array":[{"paramBigInteger":2}]}'                          |
      | 'DELETE' | 'array[0].paramBigInteger'     | '2'                    | '{"array":[{"paramBigInteger":2}]}'                          |
      | 'GET'    | 'array[0].paramFloat'          | '2.1'                  | '{"array":[{"paramFloat":2.1}]}'                             |
      | 'HEAD'   | 'array[0].paramFloat'          | '2.1'                  | '{"array":[{"paramFloat":2.1}]}'                             |
      | 'POST'   | 'array[0].paramFloat'          | '2.1'                  | '{"array":[{"paramFloat":2.1}]}'                             |
      | 'PUT'    | 'array[0].paramFloat'          | '2.1'                  | '{"array":[{"paramFloat":2.1}]}'                             |
      | 'PATCH'  | 'array[0].paramFloat'          | '2.1'                  | '{"array":[{"paramFloat":2.1}]}'                             |
      | 'DELETE' | 'array[0].paramFloat'          | '2.1'                  | '{"array":[{"paramFloat":2.1}]}'                             |
      | 'GET'    | 'array[0].paramDouble'         | '2.1'                  | '{"array":[{"paramDouble":2.1}]}'                            |
      | 'HEAD'   | 'array[0].paramDouble'         | '2.1'                  | '{"array":[{"paramDouble":2.1}]}'                            |
      | 'POST'   | 'array[0].paramDouble'         | '2.1'                  | '{"array":[{"paramDouble":2.1}]}'                            |
      | 'PUT'    | 'array[0].paramDouble'         | '2.1'                  | '{"array":[{"paramDouble":2.1}]}'                            |
      | 'PATCH'  | 'array[0].paramDouble'         | '2.1'                  | '{"array":[{"paramDouble":2.1}]}'                            |
      | 'DELETE' | 'array[0].paramDouble'         | '2.1'                  | '{"array":[{"paramDouble":2.1}]}'                            |
      | 'GET'    | 'array[0].paramBigDecimal'     | '2.1'                  | '{"array":[{"paramBigDecimal":2.1}]}'                        |
      | 'HEAD'   | 'array[0].paramBigDecimal'     | '2.1'                  | '{"array":[{"paramBigDecimal":2.1}]}'                        |
      | 'POST'   | 'array[0].paramBigDecimal'     | '2.1'                  | '{"array":[{"paramBigDecimal":2.1}]}'                        |
      | 'PUT'    | 'array[0].paramBigDecimal'     | '2.1'                  | '{"array":[{"paramBigDecimal":2.1}]}'                        |
      | 'PATCH'  | 'array[0].paramBigDecimal'     | '2.1'                  | '{"array":[{"paramBigDecimal":2.1}]}'                        |
      | 'DELETE' | 'array[0].paramBigDecimal'     | '2.1'                  | '{"array":[{"paramBigDecimal":2.1}]}'                        |
      | 'GET'    | 'array[0].paramBoolean'        | 'true'                 | '{"array":[{"paramBoolean":true}]}'                          |
      | 'HEAD'   | 'array[0].paramBoolean'        | 'true'                 | '{"array":[{"paramBoolean":true}]}'                          |
      | 'POST'   | 'array[0].paramBoolean'        | 'true'                 | '{"array":[{"paramBoolean":true}]}'                          |
      | 'PUT'    | 'array[0].paramBoolean'        | 'true'                 | '{"array":[{"paramBoolean":true}]}'                          |
      | 'PATCH'  | 'array[0].paramBoolean'        | 'true'                 | '{"array":[{"paramBoolean":true}]}'                          |
      | 'DELETE' | 'array[0].paramBoolean'        | 'true'                 | '{"array":[{"paramBoolean":true}]}'                          |
      | 'GET'    | 'array[0].paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"array":[{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}]}' |
      | 'HEAD'   | 'array[0].paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"array":[{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}]}' |
      | 'POST'   | 'array[0].paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"array":[{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}]}' |
      | 'PUT'    | 'array[0].paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"array":[{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}]}' |
      | 'PATCH'  | 'array[0].paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"array":[{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}]}' |
      | 'DELETE' | 'array[0].paramOffsetDateTime' | '2000-01-01T00:00:00Z' | '{"array":[{"paramOffsetDateTime":"2000-01-01T00:00:00Z"}]}' |
      | 'GET'    | 'array[0].paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"array":[{"paramLocalDateTime":"2000-01-01T00:00:00Z"}]}'  |
      | 'HEAD'   | 'array[0].paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"array":[{"paramLocalDateTime":"2000-01-01T00:00:00Z"}]}'  |
      | 'POST'   | 'array[0].paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"array":[{"paramLocalDateTime":"2000-01-01T00:00:00Z"}]}'  |
      | 'PUT'    | 'array[0].paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"array":[{"paramLocalDateTime":"2000-01-01T00:00:00Z"}]}'  |
      | 'PATCH'  | 'array[0].paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"array":[{"paramLocalDateTime":"2000-01-01T00:00:00Z"}]}'  |
      | 'DELETE' | 'array[0].paramLocalDateTime'  | '2000-01-01T00:00:00Z' | '{"array":[{"paramLocalDateTime":"2000-01-01T00:00:00Z"}]}'  |
      | 'GET'    | 'array[0].paramLocalDate'      | '2000-01-01'           | '{"array":[{"paramLocalDate":"2000-01-01"}]}'                |
      | 'HEAD'   | 'array[0].paramLocalDate'      | '2000-01-01'           | '{"array":[{"paramLocalDate":"2000-01-01"}]}'                |
      | 'POST'   | 'array[0].paramLocalDate'      | '2000-01-01'           | '{"array":[{"paramLocalDate":"2000-01-01"}]}'                |
      | 'PUT'    | 'array[0].paramLocalDate'      | '2000-01-01'           | '{"array":[{"paramLocalDate":"2000-01-01"}]}'                |
      | 'PATCH'  | 'array[0].paramLocalDate'      | '2000-01-01'           | '{"array":[{"paramLocalDate":"2000-01-01"}]}'                |
      | 'DELETE' | 'array[0].paramLocalDate'      | '2000-01-01'           | '{"array":[{"paramLocalDate":"2000-01-01"}]}'                |
      | 'GET'    | 'array[0].paramLocalTime'      | '13:00:00'             | '{"array":[{"paramLocalTime":"13:00:00"}]}'                  |
      | 'HEAD'   | 'array[0].paramLocalTime'      | '13:00:00'             | '{"array":[{"paramLocalTime":"13:00:00"}]}'                  |
      | 'POST'   | 'array[0].paramLocalTime'      | '13:00:00'             | '{"array":[{"paramLocalTime":"13:00:00"}]}'                  |
      | 'PUT'    | 'array[0].paramLocalTime'      | '13:00:00'             | '{"array":[{"paramLocalTime":"13:00:00"}]}'                  |
      | 'PATCH'  | 'array[0].paramLocalTime'      | '13:00:00'             | '{"array":[{"paramLocalTime":"13:00:00"}]}'                  |
      | 'DELETE' | 'array[0].paramLocalTime'      | '13:00:00'             | '{"array":[{"paramLocalTime":"13:00:00"}]}'                  |
      | 'GET'    | 'array[0].paramYear'           | '2000'                 | '{"array":[{"paramYear":"2000"}]}'                           |
      | 'HEAD'   | 'array[0].paramYear'           | '2000'                 | '{"array":[{"paramYear":"2000"}]}'                           |
      | 'POST'   | 'array[0].paramYear'           | '2000'                 | '{"array":[{"paramYear":"2000"}]}'                           |
      | 'PUT'    | 'array[0].paramYear'           | '2000'                 | '{"array":[{"paramYear":"2000"}]}'                           |
      | 'PATCH'  | 'array[0].paramYear'           | '2000'                 | '{"array":[{"paramYear":"2000"}]}'                           |
      | 'DELETE' | 'array[0].paramYear'           | '2000'                 | '{"array":[{"paramYear":"2000"}]}'                           |
      | 'GET'    | 'array[0].paramEnumAB'         | 'A'                    | '{"array":[{"paramEnumAB":"A"}]}'                            |
      | 'HEAD'   | 'array[0].paramEnumAB'         | 'A'                    | '{"array":[{"paramEnumAB":"A"}]}'                            |
      | 'POST'   | 'array[0].paramEnumAB'         | 'A'                    | '{"array":[{"paramEnumAB":"A"}]}'                            |
      | 'PUT'    | 'array[0].paramEnumAB'         | 'A'                    | '{"array":[{"paramEnumAB":"A"}]}'                            |
      | 'PATCH'  | 'array[0].paramEnumAB'         | 'A'                    | '{"array":[{"paramEnumAB":"A"}]}'                            |
      | 'DELETE' | 'array[0].paramEnumAB'         | 'A'                    | '{"array":[{"paramEnumAB":"A"}]}'                            |
      | 'GET'    | 'array[0].paramEnumLowerAB'    | 'b'                    | '{"array":[{"paramEnumLowerAB":"b"}]}'                       |
      | 'HEAD'   | 'array[0].paramEnumLowerAB'    | 'b'                    | '{"array":[{"paramEnumLowerAB":"b"}]}'                       |
      | 'POST'   | 'array[0].paramEnumLowerAB'    | 'b'                    | '{"array":[{"paramEnumLowerAB":"b"}]}'                       |
      | 'PUT'    | 'array[0].paramEnumLowerAB'    | 'b'                    | '{"array":[{"paramEnumLowerAB":"b"}]}'                       |
      | 'PATCH'  | 'array[0].paramEnumLowerAB'    | 'b'                    | '{"array":[{"paramEnumLowerAB":"b"}]}'                       |
      | 'DELETE' | 'array[0].paramEnumLowerAB'    | 'b'                    | '{"array":[{"paramEnumLowerAB":"b"}]}'                       |
