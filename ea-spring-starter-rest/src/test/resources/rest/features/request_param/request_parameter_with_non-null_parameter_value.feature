Feature: Request parameter with non-null parameter value
  
  The REST service receives empty data via the request parameter and interprets it as null.  The service responds with an error message.
  
   Rule: The conversion between data types is given by the following table
  
  | parameter type | java type      |
  |----------------|----------------|
  | string         | String         |
  | number         | Integer        |
  | number         | Long           |
  | number         | BigInteger     |
  | number         | Float          |
  | number         | Double         |
  | number         | BigDecimal     |
  | boolean        | Boolean        |
  | ISO-8601       | OffsetDateTime |
  | ISO-8601       | LocalDateTime  |
  | ISO-8601       | LocalDate      |
  | ISO-8601       | LocalTime      |
  | ISO-8601       | Year           |
  | string         | ENUM           |

  Background: 
    Given the default timezone 'America/Puerto_Rico'

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/not-null/method-parameter<path>?<param>=<value> endpoint (RequestParamConstrainedNotNullRestApi.class)
    this endpoint receives the request parameter in a method parameter, both having the same name
    and the service maps the sent empty value to null, but service responding with an error message since data is must not be null.

    Given the endpoint '/api/v1/request-parameter/not-null/method-parameter'<path>
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | param                 | value | message                                 |
      | 'GET'    | '/paramString'         | 'paramString'         | ''    | 'paramString: must not be null'         |
      | 'HEAD'   | '/paramString'         | 'paramString'         | ''    |                                         |
      | 'POST'   | '/paramString'         | 'paramString'         | ''    | 'paramString: must not be null'         |
      | 'PUT'    | '/paramString'         | 'paramString'         | ''    | 'paramString: must not be null'         |
      | 'PATCH'  | '/paramString'         | 'paramString'         | ''    | 'paramString: must not be null'         |
      | 'DELETE' | '/paramString'         | 'paramString'         | ''    | 'paramString: must not be null'         |
      | 'GET'    | '/paramInteger'        | 'paramInteger'        | ''    | 'paramInteger: must not be null'        |
      | 'HEAD'   | '/paramInteger'        | 'paramInteger'        | ''    |                                         |
      | 'POST'   | '/paramInteger'        | 'paramInteger'        | ''    | 'paramInteger: must not be null'        |
      | 'PUT'    | '/paramInteger'        | 'paramInteger'        | ''    | 'paramInteger: must not be null'        |
      | 'PATCH'  | '/paramInteger'        | 'paramInteger'        | ''    | 'paramInteger: must not be null'        |
      | 'DELETE' | '/paramInteger'        | 'paramInteger'        | ''    | 'paramInteger: must not be null'        |
      | 'GET'    | '/paramLong'           | 'paramLong'           | ''    | 'paramLong: must not be null'           |
      | 'HEAD'   | '/paramLong'           | 'paramLong'           | ''    |                                         |
      | 'POST'   | '/paramLong'           | 'paramLong'           | ''    | 'paramLong: must not be null'           |
      | 'PUT'    | '/paramLong'           | 'paramLong'           | ''    | 'paramLong: must not be null'           |
      | 'PATCH'  | '/paramLong'           | 'paramLong'           | ''    | 'paramLong: must not be null'           |
      | 'DELETE' | '/paramLong'           | 'paramLong'           | ''    | 'paramLong: must not be null'           |
      | 'GET'    | '/paramBigInteger'     | 'paramBigInteger'     | ''    | 'paramBigInteger: must not be null'     |
      | 'HEAD'   | '/paramBigInteger'     | 'paramBigInteger'     | ''    |                                         |
      | 'POST'   | '/paramBigInteger'     | 'paramBigInteger'     | ''    | 'paramBigInteger: must not be null'     |
      | 'PUT'    | '/paramBigInteger'     | 'paramBigInteger'     | ''    | 'paramBigInteger: must not be null'     |
      | 'PATCH'  | '/paramBigInteger'     | 'paramBigInteger'     | ''    | 'paramBigInteger: must not be null'     |
      | 'DELETE' | '/paramBigInteger'     | 'paramBigInteger'     | ''    | 'paramBigInteger: must not be null'     |
      | 'GET'    | '/paramFloat'          | 'paramFloat'          | ''    | 'paramFloat: must not be null'          |
      | 'HEAD'   | '/paramFloat'          | 'paramFloat'          | ''    |                                         |
      | 'POST'   | '/paramFloat'          | 'paramFloat'          | ''    | 'paramFloat: must not be null'          |
      | 'PUT'    | '/paramFloat'          | 'paramFloat'          | ''    | 'paramFloat: must not be null'          |
      | 'PATCH'  | '/paramFloat'          | 'paramFloat'          | ''    | 'paramFloat: must not be null'          |
      | 'DELETE' | '/paramFloat'          | 'paramFloat'          | ''    | 'paramFloat: must not be null'          |
      | 'GET'    | '/paramDouble'         | 'paramDouble'         | ''    | 'paramDouble: must not be null'         |
      | 'HEAD'   | '/paramDouble'         | 'paramDouble'         | ''    |                                         |
      | 'POST'   | '/paramDouble'         | 'paramDouble'         | ''    | 'paramDouble: must not be null'         |
      | 'PUT'    | '/paramDouble'         | 'paramDouble'         | ''    | 'paramDouble: must not be null'         |
      | 'PATCH'  | '/paramDouble'         | 'paramDouble'         | ''    | 'paramDouble: must not be null'         |
      | 'DELETE' | '/paramDouble'         | 'paramDouble'         | ''    | 'paramDouble: must not be null'         |
      | 'GET'    | '/paramBigDecimal'     | 'paramBigDecimal'     | ''    | 'paramBigDecimal: must not be null'     |
      | 'HEAD'   | '/paramBigDecimal'     | 'paramBigDecimal'     | ''    |                                         |
      | 'POST'   | '/paramBigDecimal'     | 'paramBigDecimal'     | ''    | 'paramBigDecimal: must not be null'     |
      | 'PUT'    | '/paramBigDecimal'     | 'paramBigDecimal'     | ''    | 'paramBigDecimal: must not be null'     |
      | 'PATCH'  | '/paramBigDecimal'     | 'paramBigDecimal'     | ''    | 'paramBigDecimal: must not be null'     |
      | 'DELETE' | '/paramBigDecimal'     | 'paramBigDecimal'     | ''    | 'paramBigDecimal: must not be null'     |
      | 'GET'    | '/paramBoolean'        | 'paramBoolean'        | ''    | 'paramBoolean: must not be null'        |
      | 'HEAD'   | '/paramBoolean'        | 'paramBoolean'        | ''    |                                         |
      | 'POST'   | '/paramBoolean'        | 'paramBoolean'        | ''    | 'paramBoolean: must not be null'        |
      | 'PUT'    | '/paramBoolean'        | 'paramBoolean'        | ''    | 'paramBoolean: must not be null'        |
      | 'PATCH'  | '/paramBoolean'        | 'paramBoolean'        | ''    | 'paramBoolean: must not be null'        |
      | 'DELETE' | '/paramBoolean'        | 'paramBoolean'        | ''    | 'paramBoolean: must not be null'        |
      | 'GET'    | '/paramOffsetDateTime' | 'paramOffsetDateTime' | ''    | 'paramOffsetDateTime: must not be null' |
      | 'HEAD'   | '/paramOffsetDateTime' | 'paramOffsetDateTime' | ''    |                                         |
      | 'POST'   | '/paramOffsetDateTime' | 'paramOffsetDateTime' | ''    | 'paramOffsetDateTime: must not be null' |
      | 'PUT'    | '/paramOffsetDateTime' | 'paramOffsetDateTime' | ''    | 'paramOffsetDateTime: must not be null' |
      | 'PATCH'  | '/paramOffsetDateTime' | 'paramOffsetDateTime' | ''    | 'paramOffsetDateTime: must not be null' |
      | 'DELETE' | '/paramOffsetDateTime' | 'paramOffsetDateTime' | ''    | 'paramOffsetDateTime: must not be null' |
      | 'GET'    | '/paramLocalDateTime'  | 'paramLocalDateTime'  | ''    | 'paramLocalDateTime: must not be null'  |
      | 'HEAD'   | '/paramLocalDateTime'  | 'paramLocalDateTime'  | ''    |                                         |
      | 'POST'   | '/paramLocalDateTime'  | 'paramLocalDateTime'  | ''    | 'paramLocalDateTime: must not be null'  |
      | 'PUT'    | '/paramLocalDateTime'  | 'paramLocalDateTime'  | ''    | 'paramLocalDateTime: must not be null'  |
      | 'PATCH'  | '/paramLocalDateTime'  | 'paramLocalDateTime'  | ''    | 'paramLocalDateTime: must not be null'  |
      | 'DELETE' | '/paramLocalDateTime'  | 'paramLocalDateTime'  | ''    | 'paramLocalDateTime: must not be null'  |
      | 'GET'    | '/paramLocalDate'      | 'paramLocalDate'      | ''    | 'paramLocalDate: must not be null'      |
      | 'HEAD'   | '/paramLocalDate'      | 'paramLocalDate'      | ''    |                                         |
      | 'POST'   | '/paramLocalDate'      | 'paramLocalDate'      | ''    | 'paramLocalDate: must not be null'      |
      | 'PUT'    | '/paramLocalDate'      | 'paramLocalDate'      | ''    | 'paramLocalDate: must not be null'      |
      | 'PATCH'  | '/paramLocalDate'      | 'paramLocalDate'      | ''    | 'paramLocalDate: must not be null'      |
      | 'DELETE' | '/paramLocalDate'      | 'paramLocalDate'      | ''    | 'paramLocalDate: must not be null'      |
      | 'GET'    | '/paramLocalTime'      | 'paramLocalTime'      | ''    | 'paramLocalTime: must not be null'      |
      | 'HEAD'   | '/paramLocalTime'      | 'paramLocalTime'      | ''    |                                         |
      | 'POST'   | '/paramLocalTime'      | 'paramLocalTime'      | ''    | 'paramLocalTime: must not be null'      |
      | 'PUT'    | '/paramLocalTime'      | 'paramLocalTime'      | ''    | 'paramLocalTime: must not be null'      |
      | 'PATCH'  | '/paramLocalTime'      | 'paramLocalTime'      | ''    | 'paramLocalTime: must not be null'      |
      | 'DELETE' | '/paramLocalTime'      | 'paramLocalTime'      | ''    | 'paramLocalTime: must not be null'      |
      | 'GET'    | '/paramYear'           | 'paramYear'           | ''    | 'paramYear: must not be null'           |
      | 'HEAD'   | '/paramYear'           | 'paramYear'           | ''    |                                         |
      | 'POST'   | '/paramYear'           | 'paramYear'           | ''    | 'paramYear: must not be null'           |
      | 'PUT'    | '/paramYear'           | 'paramYear'           | ''    | 'paramYear: must not be null'           |
      | 'PATCH'  | '/paramYear'           | 'paramYear'           | ''    | 'paramYear: must not be null'           |
      | 'DELETE' | '/paramYear'           | 'paramYear'           | ''    | 'paramYear: must not be null'           |
      | 'GET'    | '/paramEnumAB'         | 'paramEnumAB'         | ''    | 'paramEnumAB: must not be null'         |
      | 'HEAD'   | '/paramEnumAB'         | 'paramEnumAB'         | ''    |                                         |
      | 'POST'   | '/paramEnumAB'         | 'paramEnumAB'         | ''    | 'paramEnumAB: must not be null'         |
      | 'PUT'    | '/paramEnumAB'         | 'paramEnumAB'         | ''    | 'paramEnumAB: must not be null'         |
      | 'PATCH'  | '/paramEnumAB'         | 'paramEnumAB'         | ''    | 'paramEnumAB: must not be null'         |
      | 'DELETE' | '/paramEnumAB'         | 'paramEnumAB'         | ''    | 'paramEnumAB: must not be null'         |
      | 'GET'    | '/paramEnumLowerAB'    | 'paramEnumLowerAB'    | ''    | 'paramEnumLowerAB: must not be null'    |
      | 'HEAD'   | '/paramEnumLowerAB'    | 'paramEnumLowerAB'    | ''    |                                         |
      | 'POST'   | '/paramEnumLowerAB'    | 'paramEnumLowerAB'    | ''    | 'paramEnumLowerAB: must not be null'    |
      | 'PUT'    | '/paramEnumLowerAB'    | 'paramEnumLowerAB'    | ''    | 'paramEnumLowerAB: must not be null'    |
      | 'PATCH'  | '/paramEnumLowerAB'    | 'paramEnumLowerAB'    | ''    | 'paramEnumLowerAB: must not be null'    |
      | 'DELETE' | '/paramEnumLowerAB'    | 'paramEnumLowerAB'    | ''    | 'paramEnumLowerAB: must not be null'    |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/not-null/named-method-parameter<path>?<param>=<value> endpoint (RequestParamConstrainedNotNullRestApi.class)
    this endpoint receives the request parameter in a method parameter, both having the diferent names, a custom request-parameter name is defined by the endpoint 
    and the service maps the sent empty value to null, but service responding with an error message since data is must not be null.

    Given the endpoint '/api/v1/request-parameter/not-null/named-method-parameter'<path>
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | param                    | value | message                                    |
      | 'GET'    | '/paramString'         | 'param-string'           | ''    | 'param-string: must not be null'           |
      | 'HEAD'   | '/paramString'         | 'param-string'           | ''    |                                            |
      | 'POST'   | '/paramString'         | 'param-string'           | ''    | 'param-string: must not be null'           |
      | 'PUT'    | '/paramString'         | 'param-string'           | ''    | 'param-string: must not be null'           |
      | 'PATCH'  | '/paramString'         | 'param-string'           | ''    | 'param-string: must not be null'           |
      | 'DELETE' | '/paramString'         | 'param-string'           | ''    | 'param-string: must not be null'           |
      | 'GET'    | '/paramInteger'        | 'param-integer'          | ''    | 'param-integer: must not be null'          |
      | 'HEAD'   | '/paramInteger'        | 'param-integer'          | ''    |                                            |
      | 'POST'   | '/paramInteger'        | 'param-integer'          | ''    | 'param-integer: must not be null'          |
      | 'PUT'    | '/paramInteger'        | 'param-integer'          | ''    | 'param-integer: must not be null'          |
      | 'PATCH'  | '/paramInteger'        | 'param-integer'          | ''    | 'param-integer: must not be null'          |
      | 'DELETE' | '/paramInteger'        | 'param-integer'          | ''    | 'param-integer: must not be null'          |
      | 'GET'    | '/paramLong'           | 'param-long'             | ''    | 'param-long: must not be null'             |
      | 'HEAD'   | '/paramLong'           | 'param-long'             | ''    |                                            |
      | 'POST'   | '/paramLong'           | 'param-long'             | ''    | 'param-long: must not be null'             |
      | 'PUT'    | '/paramLong'           | 'param-long'             | ''    | 'param-long: must not be null'             |
      | 'PATCH'  | '/paramLong'           | 'param-long'             | ''    | 'param-long: must not be null'             |
      | 'DELETE' | '/paramLong'           | 'param-long'             | ''    | 'param-long: must not be null'             |
      | 'GET'    | '/paramBigInteger'     | 'param-big-integer'      | ''    | 'param-big-integer: must not be null'      |
      | 'HEAD'   | '/paramBigInteger'     | 'param-big-integer'      | ''    |                                            |
      | 'POST'   | '/paramBigInteger'     | 'param-big-integer'      | ''    | 'param-big-integer: must not be null'      |
      | 'PUT'    | '/paramBigInteger'     | 'param-big-integer'      | ''    | 'param-big-integer: must not be null'      |
      | 'PATCH'  | '/paramBigInteger'     | 'param-big-integer'      | ''    | 'param-big-integer: must not be null'      |
      | 'DELETE' | '/paramBigInteger'     | 'param-big-integer'      | ''    | 'param-big-integer: must not be null'      |
      | 'GET'    | '/paramFloat'          | 'param-float'            | ''    | 'param-float: must not be null'            |
      | 'HEAD'   | '/paramFloat'          | 'param-float'            | ''    |                                            |
      | 'POST'   | '/paramFloat'          | 'param-float'            | ''    | 'param-float: must not be null'            |
      | 'PUT'    | '/paramFloat'          | 'param-float'            | ''    | 'param-float: must not be null'            |
      | 'PATCH'  | '/paramFloat'          | 'param-float'            | ''    | 'param-float: must not be null'            |
      | 'DELETE' | '/paramFloat'          | 'param-float'            | ''    | 'param-float: must not be null'            |
      | 'GET'    | '/paramDouble'         | 'param-double'           | ''    | 'param-double: must not be null'           |
      | 'HEAD'   | '/paramDouble'         | 'param-double'           | ''    |                                            |
      | 'POST'   | '/paramDouble'         | 'param-double'           | ''    | 'param-double: must not be null'           |
      | 'PUT'    | '/paramDouble'         | 'param-double'           | ''    | 'param-double: must not be null'           |
      | 'PATCH'  | '/paramDouble'         | 'param-double'           | ''    | 'param-double: must not be null'           |
      | 'DELETE' | '/paramDouble'         | 'param-double'           | ''    | 'param-double: must not be null'           |
      | 'GET'    | '/paramBigDecimal'     | 'param-big-decimal'      | ''    | 'param-big-decimal: must not be null'      |
      | 'HEAD'   | '/paramBigDecimal'     | 'param-big-decimal'      | ''    |                                            |
      | 'POST'   | '/paramBigDecimal'     | 'param-big-decimal'      | ''    | 'param-big-decimal: must not be null'      |
      | 'PUT'    | '/paramBigDecimal'     | 'param-big-decimal'      | ''    | 'param-big-decimal: must not be null'      |
      | 'PATCH'  | '/paramBigDecimal'     | 'param-big-decimal'      | ''    | 'param-big-decimal: must not be null'      |
      | 'DELETE' | '/paramBigDecimal'     | 'param-big-decimal'      | ''    | 'param-big-decimal: must not be null'      |
      | 'GET'    | '/paramBoolean'        | 'param-boolean'          | ''    | 'param-boolean: must not be null'          |
      | 'HEAD'   | '/paramBoolean'        | 'param-boolean'          | ''    |                                            |
      | 'POST'   | '/paramBoolean'        | 'param-boolean'          | ''    | 'param-boolean: must not be null'          |
      | 'PUT'    | '/paramBoolean'        | 'param-boolean'          | ''    | 'param-boolean: must not be null'          |
      | 'PATCH'  | '/paramBoolean'        | 'param-boolean'          | ''    | 'param-boolean: must not be null'          |
      | 'DELETE' | '/paramBoolean'        | 'param-boolean'          | ''    | 'param-boolean: must not be null'          |
      | 'GET'    | '/paramOffsetDateTime' | 'param-offset-date-time' | ''    | 'param-offset-date-time: must not be null' |
      | 'HEAD'   | '/paramOffsetDateTime' | 'param-offset-date-time' | ''    |                                            |
      | 'POST'   | '/paramOffsetDateTime' | 'param-offset-date-time' | ''    | 'param-offset-date-time: must not be null' |
      | 'PUT'    | '/paramOffsetDateTime' | 'param-offset-date-time' | ''    | 'param-offset-date-time: must not be null' |
      | 'PATCH'  | '/paramOffsetDateTime' | 'param-offset-date-time' | ''    | 'param-offset-date-time: must not be null' |
      | 'DELETE' | '/paramOffsetDateTime' | 'param-offset-date-time' | ''    | 'param-offset-date-time: must not be null' |
      | 'GET'    | '/paramLocalDateTime'  | 'param-local-date-time'  | ''    | 'param-local-date-time: must not be null'  |
      | 'HEAD'   | '/paramLocalDateTime'  | 'param-local-date-time'  | ''    |                                            |
      | 'POST'   | '/paramLocalDateTime'  | 'param-local-date-time'  | ''    | 'param-local-date-time: must not be null'  |
      | 'PUT'    | '/paramLocalDateTime'  | 'param-local-date-time'  | ''    | 'param-local-date-time: must not be null'  |
      | 'PATCH'  | '/paramLocalDateTime'  | 'param-local-date-time'  | ''    | 'param-local-date-time: must not be null'  |
      | 'DELETE' | '/paramLocalDateTime'  | 'param-local-date-time'  | ''    | 'param-local-date-time: must not be null'  |
      | 'GET'    | '/paramLocalDate'      | 'param-local-date'       | ''    | 'param-local-date: must not be null'       |
      | 'HEAD'   | '/paramLocalDate'      | 'param-local-date'       | ''    |                                            |
      | 'POST'   | '/paramLocalDate'      | 'param-local-date'       | ''    | 'param-local-date: must not be null'       |
      | 'PUT'    | '/paramLocalDate'      | 'param-local-date'       | ''    | 'param-local-date: must not be null'       |
      | 'PATCH'  | '/paramLocalDate'      | 'param-local-date'       | ''    | 'param-local-date: must not be null'       |
      | 'DELETE' | '/paramLocalDate'      | 'param-local-date'       | ''    | 'param-local-date: must not be null'       |
      | 'GET'    | '/paramLocalTime'      | 'param-local-time'       | ''    | 'param-local-time: must not be null'       |
      | 'HEAD'   | '/paramLocalTime'      | 'param-local-time'       | ''    |                                            |
      | 'POST'   | '/paramLocalTime'      | 'param-local-time'       | ''    | 'param-local-time: must not be null'       |
      | 'PUT'    | '/paramLocalTime'      | 'param-local-time'       | ''    | 'param-local-time: must not be null'       |
      | 'PATCH'  | '/paramLocalTime'      | 'param-local-time'       | ''    | 'param-local-time: must not be null'       |
      | 'DELETE' | '/paramLocalTime'      | 'param-local-time'       | ''    | 'param-local-time: must not be null'       |
      | 'GET'    | '/paramYear'           | 'param-year'             | ''    | 'param-year: must not be null'             |
      | 'HEAD'   | '/paramYear'           | 'param-year'             | ''    |                                            |
      | 'POST'   | '/paramYear'           | 'param-year'             | ''    | 'param-year: must not be null'             |
      | 'PUT'    | '/paramYear'           | 'param-year'             | ''    | 'param-year: must not be null'             |
      | 'PATCH'  | '/paramYear'           | 'param-year'             | ''    | 'param-year: must not be null'             |
      | 'DELETE' | '/paramYear'           | 'param-year'             | ''    | 'param-year: must not be null'             |
      | 'GET'    | '/paramEnumAB'         | 'param-enum-ab'          | ''    | 'param-enum-ab: must not be null'          |
      | 'HEAD'   | '/paramEnumAB'         | 'param-enum-ab'          | ''    |                                            |
      | 'POST'   | '/paramEnumAB'         | 'param-enum-ab'          | ''    | 'param-enum-ab: must not be null'          |
      | 'PUT'    | '/paramEnumAB'         | 'param-enum-ab'          | ''    | 'param-enum-ab: must not be null'          |
      | 'PATCH'  | '/paramEnumAB'         | 'param-enum-ab'          | ''    | 'param-enum-ab: must not be null'          |
      | 'DELETE' | '/paramEnumAB'         | 'param-enum-ab'          | ''    | 'param-enum-ab: must not be null'          |
      | 'GET'    | '/paramEnumLowerAB'    | 'param-enum-lower-ab'    | ''    | 'param-enum-lower-ab: must not be null'    |
      | 'HEAD'   | '/paramEnumLowerAB'    | 'param-enum-lower-ab'    | ''    |                                            |
      | 'POST'   | '/paramEnumLowerAB'    | 'param-enum-lower-ab'    | ''    | 'param-enum-lower-ab: must not be null'    |
      | 'PUT'    | '/paramEnumLowerAB'    | 'param-enum-lower-ab'    | ''    | 'param-enum-lower-ab: must not be null'    |
      | 'PATCH'  | '/paramEnumLowerAB'    | 'param-enum-lower-ab'    | ''    | 'param-enum-lower-ab: must not be null'    |
      | 'DELETE' | '/paramEnumLowerAB'    | 'param-enum-lower-ab'    | ''    | 'param-enum-lower-ab: must not be null'    |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/not-null/optional-method-parameter<path>?<param>=<value> endpoint (RequestParamConstrainedNotNullRestApi.class)
    this endpoint receives the request parameter in an optional method parameter, both having the same name
    and the service maps the sent empty value to null, but service responding with an error message since data is must not be null.

    Given the endpoint '/api/v1/request-parameter/not-null/optional-method-parameter'<path>
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | param                 | value | message                                 |
      | 'GET'    | '/paramString'         | 'paramString'         | ''    | 'paramString: must not be null'         |
      | 'HEAD'   | '/paramString'         | 'paramString'         | ''    |                                         |
      | 'POST'   | '/paramString'         | 'paramString'         | ''    | 'paramString: must not be null'         |
      | 'PUT'    | '/paramString'         | 'paramString'         | ''    | 'paramString: must not be null'         |
      | 'PATCH'  | '/paramString'         | 'paramString'         | ''    | 'paramString: must not be null'         |
      | 'DELETE' | '/paramString'         | 'paramString'         | ''    | 'paramString: must not be null'         |
      | 'GET'    | '/paramInteger'        | 'paramInteger'        | ''    | 'paramInteger: must not be null'        |
      | 'HEAD'   | '/paramInteger'        | 'paramInteger'        | ''    |                                         |
      | 'POST'   | '/paramInteger'        | 'paramInteger'        | ''    | 'paramInteger: must not be null'        |
      | 'PUT'    | '/paramInteger'        | 'paramInteger'        | ''    | 'paramInteger: must not be null'        |
      | 'PATCH'  | '/paramInteger'        | 'paramInteger'        | ''    | 'paramInteger: must not be null'        |
      | 'DELETE' | '/paramInteger'        | 'paramInteger'        | ''    | 'paramInteger: must not be null'        |
      | 'GET'    | '/paramLong'           | 'paramLong'           | ''    | 'paramLong: must not be null'           |
      | 'HEAD'   | '/paramLong'           | 'paramLong'           | ''    |                                         |
      | 'POST'   | '/paramLong'           | 'paramLong'           | ''    | 'paramLong: must not be null'           |
      | 'PUT'    | '/paramLong'           | 'paramLong'           | ''    | 'paramLong: must not be null'           |
      | 'PATCH'  | '/paramLong'           | 'paramLong'           | ''    | 'paramLong: must not be null'           |
      | 'DELETE' | '/paramLong'           | 'paramLong'           | ''    | 'paramLong: must not be null'           |
      | 'GET'    | '/paramBigInteger'     | 'paramBigInteger'     | ''    | 'paramBigInteger: must not be null'     |
      | 'HEAD'   | '/paramBigInteger'     | 'paramBigInteger'     | ''    |                                         |
      | 'POST'   | '/paramBigInteger'     | 'paramBigInteger'     | ''    | 'paramBigInteger: must not be null'     |
      | 'PUT'    | '/paramBigInteger'     | 'paramBigInteger'     | ''    | 'paramBigInteger: must not be null'     |
      | 'PATCH'  | '/paramBigInteger'     | 'paramBigInteger'     | ''    | 'paramBigInteger: must not be null'     |
      | 'DELETE' | '/paramBigInteger'     | 'paramBigInteger'     | ''    | 'paramBigInteger: must not be null'     |
      | 'GET'    | '/paramFloat'          | 'paramFloat'          | ''    | 'paramFloat: must not be null'          |
      | 'HEAD'   | '/paramFloat'          | 'paramFloat'          | ''    |                                         |
      | 'POST'   | '/paramFloat'          | 'paramFloat'          | ''    | 'paramFloat: must not be null'          |
      | 'PUT'    | '/paramFloat'          | 'paramFloat'          | ''    | 'paramFloat: must not be null'          |
      | 'PATCH'  | '/paramFloat'          | 'paramFloat'          | ''    | 'paramFloat: must not be null'          |
      | 'DELETE' | '/paramFloat'          | 'paramFloat'          | ''    | 'paramFloat: must not be null'          |
      | 'GET'    | '/paramDouble'         | 'paramDouble'         | ''    | 'paramDouble: must not be null'         |
      | 'HEAD'   | '/paramDouble'         | 'paramDouble'         | ''    |                                         |
      | 'POST'   | '/paramDouble'         | 'paramDouble'         | ''    | 'paramDouble: must not be null'         |
      | 'PUT'    | '/paramDouble'         | 'paramDouble'         | ''    | 'paramDouble: must not be null'         |
      | 'PATCH'  | '/paramDouble'         | 'paramDouble'         | ''    | 'paramDouble: must not be null'         |
      | 'DELETE' | '/paramDouble'         | 'paramDouble'         | ''    | 'paramDouble: must not be null'         |
      | 'GET'    | '/paramBigDecimal'     | 'paramBigDecimal'     | ''    | 'paramBigDecimal: must not be null'     |
      | 'HEAD'   | '/paramBigDecimal'     | 'paramBigDecimal'     | ''    |                                         |
      | 'POST'   | '/paramBigDecimal'     | 'paramBigDecimal'     | ''    | 'paramBigDecimal: must not be null'     |
      | 'PUT'    | '/paramBigDecimal'     | 'paramBigDecimal'     | ''    | 'paramBigDecimal: must not be null'     |
      | 'PATCH'  | '/paramBigDecimal'     | 'paramBigDecimal'     | ''    | 'paramBigDecimal: must not be null'     |
      | 'DELETE' | '/paramBigDecimal'     | 'paramBigDecimal'     | ''    | 'paramBigDecimal: must not be null'     |
      | 'GET'    | '/paramBoolean'        | 'paramBoolean'        | ''    | 'paramBoolean: must not be null'        |
      | 'HEAD'   | '/paramBoolean'        | 'paramBoolean'        | ''    |                                         |
      | 'POST'   | '/paramBoolean'        | 'paramBoolean'        | ''    | 'paramBoolean: must not be null'        |
      | 'PUT'    | '/paramBoolean'        | 'paramBoolean'        | ''    | 'paramBoolean: must not be null'        |
      | 'PATCH'  | '/paramBoolean'        | 'paramBoolean'        | ''    | 'paramBoolean: must not be null'        |
      | 'DELETE' | '/paramBoolean'        | 'paramBoolean'        | ''    | 'paramBoolean: must not be null'        |
      | 'GET'    | '/paramOffsetDateTime' | 'paramOffsetDateTime' | ''    | 'paramOffsetDateTime: must not be null' |
      | 'HEAD'   | '/paramOffsetDateTime' | 'paramOffsetDateTime' | ''    |                                         |
      | 'POST'   | '/paramOffsetDateTime' | 'paramOffsetDateTime' | ''    | 'paramOffsetDateTime: must not be null' |
      | 'PUT'    | '/paramOffsetDateTime' | 'paramOffsetDateTime' | ''    | 'paramOffsetDateTime: must not be null' |
      | 'PATCH'  | '/paramOffsetDateTime' | 'paramOffsetDateTime' | ''    | 'paramOffsetDateTime: must not be null' |
      | 'DELETE' | '/paramOffsetDateTime' | 'paramOffsetDateTime' | ''    | 'paramOffsetDateTime: must not be null' |
      | 'GET'    | '/paramLocalDateTime'  | 'paramLocalDateTime'  | ''    | 'paramLocalDateTime: must not be null'  |
      | 'HEAD'   | '/paramLocalDateTime'  | 'paramLocalDateTime'  | ''    |                                         |
      | 'POST'   | '/paramLocalDateTime'  | 'paramLocalDateTime'  | ''    | 'paramLocalDateTime: must not be null'  |
      | 'PUT'    | '/paramLocalDateTime'  | 'paramLocalDateTime'  | ''    | 'paramLocalDateTime: must not be null'  |
      | 'PATCH'  | '/paramLocalDateTime'  | 'paramLocalDateTime'  | ''    | 'paramLocalDateTime: must not be null'  |
      | 'DELETE' | '/paramLocalDateTime'  | 'paramLocalDateTime'  | ''    | 'paramLocalDateTime: must not be null'  |
      | 'GET'    | '/paramLocalDate'      | 'paramLocalDate'      | ''    | 'paramLocalDate: must not be null'      |
      | 'HEAD'   | '/paramLocalDate'      | 'paramLocalDate'      | ''    |                                         |
      | 'POST'   | '/paramLocalDate'      | 'paramLocalDate'      | ''    | 'paramLocalDate: must not be null'      |
      | 'PUT'    | '/paramLocalDate'      | 'paramLocalDate'      | ''    | 'paramLocalDate: must not be null'      |
      | 'PATCH'  | '/paramLocalDate'      | 'paramLocalDate'      | ''    | 'paramLocalDate: must not be null'      |
      | 'DELETE' | '/paramLocalDate'      | 'paramLocalDate'      | ''    | 'paramLocalDate: must not be null'      |
      | 'GET'    | '/paramLocalTime'      | 'paramLocalTime'      | ''    | 'paramLocalTime: must not be null'      |
      | 'HEAD'   | '/paramLocalTime'      | 'paramLocalTime'      | ''    |                                         |
      | 'POST'   | '/paramLocalTime'      | 'paramLocalTime'      | ''    | 'paramLocalTime: must not be null'      |
      | 'PUT'    | '/paramLocalTime'      | 'paramLocalTime'      | ''    | 'paramLocalTime: must not be null'      |
      | 'PATCH'  | '/paramLocalTime'      | 'paramLocalTime'      | ''    | 'paramLocalTime: must not be null'      |
      | 'DELETE' | '/paramLocalTime'      | 'paramLocalTime'      | ''    | 'paramLocalTime: must not be null'      |
      | 'GET'    | '/paramYear'           | 'paramYear'           | ''    | 'paramYear: must not be null'           |
      | 'HEAD'   | '/paramYear'           | 'paramYear'           | ''    |                                         |
      | 'POST'   | '/paramYear'           | 'paramYear'           | ''    | 'paramYear: must not be null'           |
      | 'PUT'    | '/paramYear'           | 'paramYear'           | ''    | 'paramYear: must not be null'           |
      | 'PATCH'  | '/paramYear'           | 'paramYear'           | ''    | 'paramYear: must not be null'           |
      | 'DELETE' | '/paramYear'           | 'paramYear'           | ''    | 'paramYear: must not be null'           |
      | 'GET'    | '/paramEnumAB'         | 'paramEnumAB'         | ''    | 'paramEnumAB: must not be null'         |
      | 'HEAD'   | '/paramEnumAB'         | 'paramEnumAB'         | ''    |                                         |
      | 'POST'   | '/paramEnumAB'         | 'paramEnumAB'         | ''    | 'paramEnumAB: must not be null'         |
      | 'PUT'    | '/paramEnumAB'         | 'paramEnumAB'         | ''    | 'paramEnumAB: must not be null'         |
      | 'PATCH'  | '/paramEnumAB'         | 'paramEnumAB'         | ''    | 'paramEnumAB: must not be null'         |
      | 'DELETE' | '/paramEnumAB'         | 'paramEnumAB'         | ''    | 'paramEnumAB: must not be null'         |
      | 'GET'    | '/paramEnumLowerAB'    | 'paramEnumLowerAB'    | ''    | 'paramEnumLowerAB: must not be null'    |
      | 'HEAD'   | '/paramEnumLowerAB'    | 'paramEnumLowerAB'    | ''    |                                         |
      | 'POST'   | '/paramEnumLowerAB'    | 'paramEnumLowerAB'    | ''    | 'paramEnumLowerAB: must not be null'    |
      | 'PUT'    | '/paramEnumLowerAB'    | 'paramEnumLowerAB'    | ''    | 'paramEnumLowerAB: must not be null'    |
      | 'PATCH'  | '/paramEnumLowerAB'    | 'paramEnumLowerAB'    | ''    | 'paramEnumLowerAB: must not be null'    |
      | 'DELETE' | '/paramEnumLowerAB'    | 'paramEnumLowerAB'    | ''    | 'paramEnumLowerAB: must not be null'    |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/not-null/named-optional-method-parameter<path>?<param>=<value> endpoint (RequestParamConstrainedNotNullRestApi.class)
    this endpoint receives the request parameter in an optional method parameter, both having the diferent names, a custom request-parameter name is defined by the endpoint 
    and the service maps the sent empty value to null, but service responding with an error message since data is must not be null.

    Given the endpoint '/api/v1/request-parameter/not-null/named-optional-method-parameter'<path>
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | param                    | value | message                                 |
      | 'GET'    | '/paramString'         | 'param-string'           | ''    | 'paramString: must not be null'         |
      | 'HEAD'   | '/paramString'         | 'param-string'           | ''    |                                         |
      | 'POST'   | '/paramString'         | 'param-string'           | ''    | 'paramString: must not be null'         |
      | 'PUT'    | '/paramString'         | 'param-string'           | ''    | 'paramString: must not be null'         |
      | 'PATCH'  | '/paramString'         | 'param-string'           | ''    | 'paramString: must not be null'         |
      | 'DELETE' | '/paramString'         | 'param-string'           | ''    | 'paramString: must not be null'         |
      | 'GET'    | '/paramInteger'        | 'param-integer'          | ''    | 'paramInteger: must not be null'        |
      | 'HEAD'   | '/paramInteger'        | 'param-integer'          | ''    |                                         |
      | 'POST'   | '/paramInteger'        | 'param-integer'          | ''    | 'paramInteger: must not be null'        |
      | 'PUT'    | '/paramInteger'        | 'param-integer'          | ''    | 'paramInteger: must not be null'        |
      | 'PATCH'  | '/paramInteger'        | 'param-integer'          | ''    | 'paramInteger: must not be null'        |
      | 'DELETE' | '/paramInteger'        | 'param-integer'          | ''    | 'paramInteger: must not be null'        |
      | 'GET'    | '/paramLong'           | 'param-long'             | ''    | 'paramLong: must not be null'           |
      | 'HEAD'   | '/paramLong'           | 'param-long'             | ''    |                                         |
      | 'POST'   | '/paramLong'           | 'param-long'             | ''    | 'paramLong: must not be null'           |
      | 'PUT'    | '/paramLong'           | 'param-long'             | ''    | 'paramLong: must not be null'           |
      | 'PATCH'  | '/paramLong'           | 'param-long'             | ''    | 'paramLong: must not be null'           |
      | 'DELETE' | '/paramLong'           | 'param-long'             | ''    | 'paramLong: must not be null'           |
      | 'GET'    | '/paramBigInteger'     | 'param-big-integer'      | ''    | 'paramBigInteger: must not be null'     |
      | 'HEAD'   | '/paramBigInteger'     | 'param-big-integer'      | ''    |                                         |
      | 'POST'   | '/paramBigInteger'     | 'param-big-integer'      | ''    | 'paramBigInteger: must not be null'     |
      | 'PUT'    | '/paramBigInteger'     | 'param-big-integer'      | ''    | 'paramBigInteger: must not be null'     |
      | 'PATCH'  | '/paramBigInteger'     | 'param-big-integer'      | ''    | 'paramBigInteger: must not be null'     |
      | 'DELETE' | '/paramBigInteger'     | 'param-big-integer'      | ''    | 'paramBigInteger: must not be null'     |
      | 'GET'    | '/paramFloat'          | 'param-float'            | ''    | 'paramFloat: must not be null'          |
      | 'HEAD'   | '/paramFloat'          | 'param-float'            | ''    |                                         |
      | 'POST'   | '/paramFloat'          | 'param-float'            | ''    | 'paramFloat: must not be null'          |
      | 'PUT'    | '/paramFloat'          | 'param-float'            | ''    | 'paramFloat: must not be null'          |
      | 'PATCH'  | '/paramFloat'          | 'param-float'            | ''    | 'paramFloat: must not be null'          |
      | 'DELETE' | '/paramFloat'          | 'param-float'            | ''    | 'paramFloat: must not be null'          |
      | 'GET'    | '/paramDouble'         | 'param-double'           | ''    | 'paramDouble: must not be null'         |
      | 'HEAD'   | '/paramDouble'         | 'param-double'           | ''    |                                         |
      | 'POST'   | '/paramDouble'         | 'param-double'           | ''    | 'paramDouble: must not be null'         |
      | 'PUT'    | '/paramDouble'         | 'param-double'           | ''    | 'paramDouble: must not be null'         |
      | 'PATCH'  | '/paramDouble'         | 'param-double'           | ''    | 'paramDouble: must not be null'         |
      | 'DELETE' | '/paramDouble'         | 'param-double'           | ''    | 'paramDouble: must not be null'         |
      | 'GET'    | '/paramBigDecimal'     | 'param-big-decimal'      | ''    | 'paramBigDecimal: must not be null'     |
      | 'HEAD'   | '/paramBigDecimal'     | 'param-big-decimal'      | ''    |                                         |
      | 'POST'   | '/paramBigDecimal'     | 'param-big-decimal'      | ''    | 'paramBigDecimal: must not be null'     |
      | 'PUT'    | '/paramBigDecimal'     | 'param-big-decimal'      | ''    | 'paramBigDecimal: must not be null'     |
      | 'PATCH'  | '/paramBigDecimal'     | 'param-big-decimal'      | ''    | 'paramBigDecimal: must not be null'     |
      | 'DELETE' | '/paramBigDecimal'     | 'param-big-decimal'      | ''    | 'paramBigDecimal: must not be null'     |
      | 'GET'    | '/paramBoolean'        | 'param-boolean'          | ''    | 'paramBoolean: must not be null'        |
      | 'HEAD'   | '/paramBoolean'        | 'param-boolean'          | ''    |                                         |
      | 'POST'   | '/paramBoolean'        | 'param-boolean'          | ''    | 'paramBoolean: must not be null'        |
      | 'PUT'    | '/paramBoolean'        | 'param-boolean'          | ''    | 'paramBoolean: must not be null'        |
      | 'PATCH'  | '/paramBoolean'        | 'param-boolean'          | ''    | 'paramBoolean: must not be null'        |
      | 'DELETE' | '/paramBoolean'        | 'param-boolean'          | ''    | 'paramBoolean: must not be null'        |
      | 'GET'    | '/paramOffsetDateTime' | 'param-offset-date-time' | ''    | 'paramOffsetDateTime: must not be null' |
      | 'HEAD'   | '/paramOffsetDateTime' | 'param-offset-date-time' | ''    |                                         |
      | 'POST'   | '/paramOffsetDateTime' | 'param-offset-date-time' | ''    | 'paramOffsetDateTime: must not be null' |
      | 'PUT'    | '/paramOffsetDateTime' | 'param-offset-date-time' | ''    | 'paramOffsetDateTime: must not be null' |
      | 'PATCH'  | '/paramOffsetDateTime' | 'param-offset-date-time' | ''    | 'paramOffsetDateTime: must not be null' |
      | 'DELETE' | '/paramOffsetDateTime' | 'param-offset-date-time' | ''    | 'paramOffsetDateTime: must not be null' |
      | 'GET'    | '/paramLocalDateTime'  | 'param-local-date-time'  | ''    | 'paramLocalDateTime: must not be null'  |
      | 'HEAD'   | '/paramLocalDateTime'  | 'param-local-date-time'  | ''    |                                         |
      | 'POST'   | '/paramLocalDateTime'  | 'param-local-date-time'  | ''    | 'paramLocalDateTime: must not be null'  |
      | 'PUT'    | '/paramLocalDateTime'  | 'param-local-date-time'  | ''    | 'paramLocalDateTime: must not be null'  |
      | 'PATCH'  | '/paramLocalDateTime'  | 'param-local-date-time'  | ''    | 'paramLocalDateTime: must not be null'  |
      | 'DELETE' | '/paramLocalDateTime'  | 'param-local-date-time'  | ''    | 'paramLocalDateTime: must not be null'  |
      | 'GET'    | '/paramLocalDate'      | 'param-local-date'       | ''    | 'paramLocalDate: must not be null'      |
      | 'HEAD'   | '/paramLocalDate'      | 'param-local-date'       | ''    |                                         |
      | 'POST'   | '/paramLocalDate'      | 'param-local-date'       | ''    | 'paramLocalDate: must not be null'      |
      | 'PUT'    | '/paramLocalDate'      | 'param-local-date'       | ''    | 'paramLocalDate: must not be null'      |
      | 'PATCH'  | '/paramLocalDate'      | 'param-local-date'       | ''    | 'paramLocalDate: must not be null'      |
      | 'DELETE' | '/paramLocalDate'      | 'param-local-date'       | ''    | 'paramLocalDate: must not be null'      |
      | 'GET'    | '/paramLocalTime'      | 'param-local-time'       | ''    | 'paramLocalTime: must not be null'      |
      | 'HEAD'   | '/paramLocalTime'      | 'param-local-time'       | ''    |                                         |
      | 'POST'   | '/paramLocalTime'      | 'param-local-time'       | ''    | 'paramLocalTime: must not be null'      |
      | 'PUT'    | '/paramLocalTime'      | 'param-local-time'       | ''    | 'paramLocalTime: must not be null'      |
      | 'PATCH'  | '/paramLocalTime'      | 'param-local-time'       | ''    | 'paramLocalTime: must not be null'      |
      | 'DELETE' | '/paramLocalTime'      | 'param-local-time'       | ''    | 'paramLocalTime: must not be null'      |
      | 'GET'    | '/paramYear'           | 'param-year'             | ''    | 'paramYear: must not be null'           |
      | 'HEAD'   | '/paramYear'           | 'param-year'             | ''    |                                         |
      | 'POST'   | '/paramYear'           | 'param-year'             | ''    | 'paramYear: must not be null'           |
      | 'PUT'    | '/paramYear'           | 'param-year'             | ''    | 'paramYear: must not be null'           |
      | 'PATCH'  | '/paramYear'           | 'param-year'             | ''    | 'paramYear: must not be null'           |
      | 'DELETE' | '/paramYear'           | 'param-year'             | ''    | 'paramYear: must not be null'           |
      | 'GET'    | '/paramEnumAB'         | 'param-enum-ab'          | ''    | 'paramEnumAB: must not be null'         |
      | 'HEAD'   | '/paramEnumAB'         | 'param-enum-ab'          | ''    |                                         |
      | 'POST'   | '/paramEnumAB'         | 'param-enum-ab'          | ''    | 'paramEnumAB: must not be null'         |
      | 'PUT'    | '/paramEnumAB'         | 'param-enum-ab'          | ''    | 'paramEnumAB: must not be null'         |
      | 'PATCH'  | '/paramEnumAB'         | 'param-enum-ab'          | ''    | 'paramEnumAB: must not be null'         |
      | 'DELETE' | '/paramEnumAB'         | 'param-enum-ab'          | ''    | 'paramEnumAB: must not be null'         |
      | 'GET'    | '/paramEnumLowerAB'    | 'param-enum-lower-ab'    | ''    | 'paramEnumLowerAB: must not be null'    |
      | 'HEAD'   | '/paramEnumLowerAB'    | 'param-enum-lower-ab'    | ''    |                                         |
      | 'POST'   | '/paramEnumLowerAB'    | 'param-enum-lower-ab'    | ''    | 'paramEnumLowerAB: must not be null'    |
      | 'PUT'    | '/paramEnumLowerAB'    | 'param-enum-lower-ab'    | ''    | 'paramEnumLowerAB: must not be null'    |
      | 'PATCH'  | '/paramEnumLowerAB'    | 'param-enum-lower-ab'    | ''    | 'paramEnumLowerAB: must not be null'    |
      | 'DELETE' | '/paramEnumLowerAB'    | 'param-enum-lower-ab'    | ''    | 'paramEnumLowerAB: must not be null'    |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/not-null/dto-attribute<path>?<param>=<value> endpoint (RequestParamConstrainedNotNullRestApi.class)
    this endpoint receives the request parameter in a dto attribute, both having the same name
    and the service maps the sent empty value to null, but service responding with an error message since data is must not be null.

    Given the endpoint '/api/v1/request-parameter/not-null/dto-attribute'<path>
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | param                 | value | message                                 |
      | 'GET'    | '/paramString'         | 'paramString'         | ''    | 'paramString: must not be null'         |
      | 'HEAD'   | '/paramString'         | 'paramString'         | ''    |                                         |
      | 'POST'   | '/paramString'         | 'paramString'         | ''    | 'paramString: must not be null'         |
      | 'PUT'    | '/paramString'         | 'paramString'         | ''    | 'paramString: must not be null'         |
      | 'PATCH'  | '/paramString'         | 'paramString'         | ''    | 'paramString: must not be null'         |
      | 'DELETE' | '/paramString'         | 'paramString'         | ''    | 'paramString: must not be null'         |
      | 'GET'    | '/paramInteger'        | 'paramInteger'        | ''    | 'paramInteger: must not be null'        |
      | 'HEAD'   | '/paramInteger'        | 'paramInteger'        | ''    |                                         |
      | 'POST'   | '/paramInteger'        | 'paramInteger'        | ''    | 'paramInteger: must not be null'        |
      | 'PUT'    | '/paramInteger'        | 'paramInteger'        | ''    | 'paramInteger: must not be null'        |
      | 'PATCH'  | '/paramInteger'        | 'paramInteger'        | ''    | 'paramInteger: must not be null'        |
      | 'DELETE' | '/paramInteger'        | 'paramInteger'        | ''    | 'paramInteger: must not be null'        |
      | 'GET'    | '/paramLong'           | 'paramLong'           | ''    | 'paramLong: must not be null'           |
      | 'HEAD'   | '/paramLong'           | 'paramLong'           | ''    |                                         |
      | 'POST'   | '/paramLong'           | 'paramLong'           | ''    | 'paramLong: must not be null'           |
      | 'PUT'    | '/paramLong'           | 'paramLong'           | ''    | 'paramLong: must not be null'           |
      | 'PATCH'  | '/paramLong'           | 'paramLong'           | ''    | 'paramLong: must not be null'           |
      | 'DELETE' | '/paramLong'           | 'paramLong'           | ''    | 'paramLong: must not be null'           |
      | 'GET'    | '/paramBigInteger'     | 'paramBigInteger'     | ''    | 'paramBigInteger: must not be null'     |
      | 'HEAD'   | '/paramBigInteger'     | 'paramBigInteger'     | ''    |                                         |
      | 'POST'   | '/paramBigInteger'     | 'paramBigInteger'     | ''    | 'paramBigInteger: must not be null'     |
      | 'PUT'    | '/paramBigInteger'     | 'paramBigInteger'     | ''    | 'paramBigInteger: must not be null'     |
      | 'PATCH'  | '/paramBigInteger'     | 'paramBigInteger'     | ''    | 'paramBigInteger: must not be null'     |
      | 'DELETE' | '/paramBigInteger'     | 'paramBigInteger'     | ''    | 'paramBigInteger: must not be null'     |
      | 'GET'    | '/paramFloat'          | 'paramFloat'          | ''    | 'paramFloat: must not be null'          |
      | 'HEAD'   | '/paramFloat'          | 'paramFloat'          | ''    |                                         |
      | 'POST'   | '/paramFloat'          | 'paramFloat'          | ''    | 'paramFloat: must not be null'          |
      | 'PUT'    | '/paramFloat'          | 'paramFloat'          | ''    | 'paramFloat: must not be null'          |
      | 'PATCH'  | '/paramFloat'          | 'paramFloat'          | ''    | 'paramFloat: must not be null'          |
      | 'DELETE' | '/paramFloat'          | 'paramFloat'          | ''    | 'paramFloat: must not be null'          |
      | 'GET'    | '/paramDouble'         | 'paramDouble'         | ''    | 'paramDouble: must not be null'         |
      | 'HEAD'   | '/paramDouble'         | 'paramDouble'         | ''    |                                         |
      | 'POST'   | '/paramDouble'         | 'paramDouble'         | ''    | 'paramDouble: must not be null'         |
      | 'PUT'    | '/paramDouble'         | 'paramDouble'         | ''    | 'paramDouble: must not be null'         |
      | 'PATCH'  | '/paramDouble'         | 'paramDouble'         | ''    | 'paramDouble: must not be null'         |
      | 'DELETE' | '/paramDouble'         | 'paramDouble'         | ''    | 'paramDouble: must not be null'         |
      | 'GET'    | '/paramBigDecimal'     | 'paramBigDecimal'     | ''    | 'paramBigDecimal: must not be null'     |
      | 'HEAD'   | '/paramBigDecimal'     | 'paramBigDecimal'     | ''    |                                         |
      | 'POST'   | '/paramBigDecimal'     | 'paramBigDecimal'     | ''    | 'paramBigDecimal: must not be null'     |
      | 'PUT'    | '/paramBigDecimal'     | 'paramBigDecimal'     | ''    | 'paramBigDecimal: must not be null'     |
      | 'PATCH'  | '/paramBigDecimal'     | 'paramBigDecimal'     | ''    | 'paramBigDecimal: must not be null'     |
      | 'DELETE' | '/paramBigDecimal'     | 'paramBigDecimal'     | ''    | 'paramBigDecimal: must not be null'     |
      | 'GET'    | '/paramBoolean'        | 'paramBoolean'        | ''    | 'paramBoolean: must not be null'        |
      | 'HEAD'   | '/paramBoolean'        | 'paramBoolean'        | ''    |                                         |
      | 'POST'   | '/paramBoolean'        | 'paramBoolean'        | ''    | 'paramBoolean: must not be null'        |
      | 'PUT'    | '/paramBoolean'        | 'paramBoolean'        | ''    | 'paramBoolean: must not be null'        |
      | 'PATCH'  | '/paramBoolean'        | 'paramBoolean'        | ''    | 'paramBoolean: must not be null'        |
      | 'DELETE' | '/paramBoolean'        | 'paramBoolean'        | ''    | 'paramBoolean: must not be null'        |
      | 'GET'    | '/paramOffsetDateTime' | 'paramOffsetDateTime' | ''    | 'paramOffsetDateTime: must not be null' |
      | 'HEAD'   | '/paramOffsetDateTime' | 'paramOffsetDateTime' | ''    |                                         |
      | 'POST'   | '/paramOffsetDateTime' | 'paramOffsetDateTime' | ''    | 'paramOffsetDateTime: must not be null' |
      | 'PUT'    | '/paramOffsetDateTime' | 'paramOffsetDateTime' | ''    | 'paramOffsetDateTime: must not be null' |
      | 'PATCH'  | '/paramOffsetDateTime' | 'paramOffsetDateTime' | ''    | 'paramOffsetDateTime: must not be null' |
      | 'DELETE' | '/paramOffsetDateTime' | 'paramOffsetDateTime' | ''    | 'paramOffsetDateTime: must not be null' |
      | 'GET'    | '/paramLocalDateTime'  | 'paramLocalDateTime'  | ''    | 'paramLocalDateTime: must not be null'  |
      | 'HEAD'   | '/paramLocalDateTime'  | 'paramLocalDateTime'  | ''    |                                         |
      | 'POST'   | '/paramLocalDateTime'  | 'paramLocalDateTime'  | ''    | 'paramLocalDateTime: must not be null'  |
      | 'PUT'    | '/paramLocalDateTime'  | 'paramLocalDateTime'  | ''    | 'paramLocalDateTime: must not be null'  |
      | 'PATCH'  | '/paramLocalDateTime'  | 'paramLocalDateTime'  | ''    | 'paramLocalDateTime: must not be null'  |
      | 'DELETE' | '/paramLocalDateTime'  | 'paramLocalDateTime'  | ''    | 'paramLocalDateTime: must not be null'  |
      | 'GET'    | '/paramLocalDate'      | 'paramLocalDate'      | ''    | 'paramLocalDate: must not be null'      |
      | 'HEAD'   | '/paramLocalDate'      | 'paramLocalDate'      | ''    |                                         |
      | 'POST'   | '/paramLocalDate'      | 'paramLocalDate'      | ''    | 'paramLocalDate: must not be null'      |
      | 'PUT'    | '/paramLocalDate'      | 'paramLocalDate'      | ''    | 'paramLocalDate: must not be null'      |
      | 'PATCH'  | '/paramLocalDate'      | 'paramLocalDate'      | ''    | 'paramLocalDate: must not be null'      |
      | 'DELETE' | '/paramLocalDate'      | 'paramLocalDate'      | ''    | 'paramLocalDate: must not be null'      |
      | 'GET'    | '/paramLocalTime'      | 'paramLocalTime'      | ''    | 'paramLocalTime: must not be null'      |
      | 'HEAD'   | '/paramLocalTime'      | 'paramLocalTime'      | ''    |                                         |
      | 'POST'   | '/paramLocalTime'      | 'paramLocalTime'      | ''    | 'paramLocalTime: must not be null'      |
      | 'PUT'    | '/paramLocalTime'      | 'paramLocalTime'      | ''    | 'paramLocalTime: must not be null'      |
      | 'PATCH'  | '/paramLocalTime'      | 'paramLocalTime'      | ''    | 'paramLocalTime: must not be null'      |
      | 'DELETE' | '/paramLocalTime'      | 'paramLocalTime'      | ''    | 'paramLocalTime: must not be null'      |
      | 'GET'    | '/paramYear'           | 'paramYear'           | ''    | 'paramYear: must not be null'           |
      | 'HEAD'   | '/paramYear'           | 'paramYear'           | ''    |                                         |
      | 'POST'   | '/paramYear'           | 'paramYear'           | ''    | 'paramYear: must not be null'           |
      | 'PUT'    | '/paramYear'           | 'paramYear'           | ''    | 'paramYear: must not be null'           |
      | 'PATCH'  | '/paramYear'           | 'paramYear'           | ''    | 'paramYear: must not be null'           |
      | 'DELETE' | '/paramYear'           | 'paramYear'           | ''    | 'paramYear: must not be null'           |
      | 'GET'    | '/paramEnumAB'         | 'paramEnumAB'         | ''    | 'paramEnumAB: must not be null'         |
      | 'HEAD'   | '/paramEnumAB'         | 'paramEnumAB'         | ''    |                                         |
      | 'POST'   | '/paramEnumAB'         | 'paramEnumAB'         | ''    | 'paramEnumAB: must not be null'         |
      | 'PUT'    | '/paramEnumAB'         | 'paramEnumAB'         | ''    | 'paramEnumAB: must not be null'         |
      | 'PATCH'  | '/paramEnumAB'         | 'paramEnumAB'         | ''    | 'paramEnumAB: must not be null'         |
      | 'DELETE' | '/paramEnumAB'         | 'paramEnumAB'         | ''    | 'paramEnumAB: must not be null'         |
      | 'GET'    | '/paramEnumLowerAB'    | 'paramEnumLowerAB'    | ''    | 'paramEnumLowerAB: must not be null'    |
      | 'HEAD'   | '/paramEnumLowerAB'    | 'paramEnumLowerAB'    | ''    |                                         |
      | 'POST'   | '/paramEnumLowerAB'    | 'paramEnumLowerAB'    | ''    | 'paramEnumLowerAB: must not be null'    |
      | 'PUT'    | '/paramEnumLowerAB'    | 'paramEnumLowerAB'    | ''    | 'paramEnumLowerAB: must not be null'    |
      | 'PATCH'  | '/paramEnumLowerAB'    | 'paramEnumLowerAB'    | ''    | 'paramEnumLowerAB: must not be null'    |
      | 'DELETE' | '/paramEnumLowerAB'    | 'paramEnumLowerAB'    | ''    | 'paramEnumLowerAB: must not be null'    |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/not-null/optional-dto-attribute<path>?<param>=<value> endpoint (RequestParamConstrainedNotNullRestApi.class)
    this endpoint receives the request parameter in an optional dto attribute, both having the same name
    and the service maps the sent empty value to null, but service responding with an error message since data is must not be null.

    Given the endpoint '/api/v1/request-parameter/not-null/optional-dto-attribute'<path>
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | param                 | value | message                                 |
      | 'GET'    | '/paramString'         | 'paramString'         | ''    | 'paramString: must not be null'         |
      | 'HEAD'   | '/paramString'         | 'paramString'         | ''    |                                         |
      | 'POST'   | '/paramString'         | 'paramString'         | ''    | 'paramString: must not be null'         |
      | 'PUT'    | '/paramString'         | 'paramString'         | ''    | 'paramString: must not be null'         |
      | 'PATCH'  | '/paramString'         | 'paramString'         | ''    | 'paramString: must not be null'         |
      | 'DELETE' | '/paramString'         | 'paramString'         | ''    | 'paramString: must not be null'         |
      | 'GET'    | '/paramInteger'        | 'paramInteger'        | ''    | 'paramInteger: must not be null'        |
      | 'HEAD'   | '/paramInteger'        | 'paramInteger'        | ''    |                                         |
      | 'POST'   | '/paramInteger'        | 'paramInteger'        | ''    | 'paramInteger: must not be null'        |
      | 'PUT'    | '/paramInteger'        | 'paramInteger'        | ''    | 'paramInteger: must not be null'        |
      | 'PATCH'  | '/paramInteger'        | 'paramInteger'        | ''    | 'paramInteger: must not be null'        |
      | 'DELETE' | '/paramInteger'        | 'paramInteger'        | ''    | 'paramInteger: must not be null'        |
      | 'GET'    | '/paramLong'           | 'paramLong'           | ''    | 'paramLong: must not be null'           |
      | 'HEAD'   | '/paramLong'           | 'paramLong'           | ''    |                                         |
      | 'POST'   | '/paramLong'           | 'paramLong'           | ''    | 'paramLong: must not be null'           |
      | 'PUT'    | '/paramLong'           | 'paramLong'           | ''    | 'paramLong: must not be null'           |
      | 'PATCH'  | '/paramLong'           | 'paramLong'           | ''    | 'paramLong: must not be null'           |
      | 'DELETE' | '/paramLong'           | 'paramLong'           | ''    | 'paramLong: must not be null'           |
      | 'GET'    | '/paramBigInteger'     | 'paramBigInteger'     | ''    | 'paramBigInteger: must not be null'     |
      | 'HEAD'   | '/paramBigInteger'     | 'paramBigInteger'     | ''    |                                         |
      | 'POST'   | '/paramBigInteger'     | 'paramBigInteger'     | ''    | 'paramBigInteger: must not be null'     |
      | 'PUT'    | '/paramBigInteger'     | 'paramBigInteger'     | ''    | 'paramBigInteger: must not be null'     |
      | 'PATCH'  | '/paramBigInteger'     | 'paramBigInteger'     | ''    | 'paramBigInteger: must not be null'     |
      | 'DELETE' | '/paramBigInteger'     | 'paramBigInteger'     | ''    | 'paramBigInteger: must not be null'     |
      | 'GET'    | '/paramFloat'          | 'paramFloat'          | ''    | 'paramFloat: must not be null'          |
      | 'HEAD'   | '/paramFloat'          | 'paramFloat'          | ''    |                                         |
      | 'POST'   | '/paramFloat'          | 'paramFloat'          | ''    | 'paramFloat: must not be null'          |
      | 'PUT'    | '/paramFloat'          | 'paramFloat'          | ''    | 'paramFloat: must not be null'          |
      | 'PATCH'  | '/paramFloat'          | 'paramFloat'          | ''    | 'paramFloat: must not be null'          |
      | 'DELETE' | '/paramFloat'          | 'paramFloat'          | ''    | 'paramFloat: must not be null'          |
      | 'GET'    | '/paramDouble'         | 'paramDouble'         | ''    | 'paramDouble: must not be null'         |
      | 'HEAD'   | '/paramDouble'         | 'paramDouble'         | ''    |                                         |
      | 'POST'   | '/paramDouble'         | 'paramDouble'         | ''    | 'paramDouble: must not be null'         |
      | 'PUT'    | '/paramDouble'         | 'paramDouble'         | ''    | 'paramDouble: must not be null'         |
      | 'PATCH'  | '/paramDouble'         | 'paramDouble'         | ''    | 'paramDouble: must not be null'         |
      | 'DELETE' | '/paramDouble'         | 'paramDouble'         | ''    | 'paramDouble: must not be null'         |
      | 'GET'    | '/paramBigDecimal'     | 'paramBigDecimal'     | ''    | 'paramBigDecimal: must not be null'     |
      | 'HEAD'   | '/paramBigDecimal'     | 'paramBigDecimal'     | ''    |                                         |
      | 'POST'   | '/paramBigDecimal'     | 'paramBigDecimal'     | ''    | 'paramBigDecimal: must not be null'     |
      | 'PUT'    | '/paramBigDecimal'     | 'paramBigDecimal'     | ''    | 'paramBigDecimal: must not be null'     |
      | 'PATCH'  | '/paramBigDecimal'     | 'paramBigDecimal'     | ''    | 'paramBigDecimal: must not be null'     |
      | 'DELETE' | '/paramBigDecimal'     | 'paramBigDecimal'     | ''    | 'paramBigDecimal: must not be null'     |
      | 'GET'    | '/paramBoolean'        | 'paramBoolean'        | ''    | 'paramBoolean: must not be null'        |
      | 'HEAD'   | '/paramBoolean'        | 'paramBoolean'        | ''    |                                         |
      | 'POST'   | '/paramBoolean'        | 'paramBoolean'        | ''    | 'paramBoolean: must not be null'        |
      | 'PUT'    | '/paramBoolean'        | 'paramBoolean'        | ''    | 'paramBoolean: must not be null'        |
      | 'PATCH'  | '/paramBoolean'        | 'paramBoolean'        | ''    | 'paramBoolean: must not be null'        |
      | 'DELETE' | '/paramBoolean'        | 'paramBoolean'        | ''    | 'paramBoolean: must not be null'        |
      | 'GET'    | '/paramOffsetDateTime' | 'paramOffsetDateTime' | ''    | 'paramOffsetDateTime: must not be null' |
      | 'HEAD'   | '/paramOffsetDateTime' | 'paramOffsetDateTime' | ''    |                                         |
      | 'POST'   | '/paramOffsetDateTime' | 'paramOffsetDateTime' | ''    | 'paramOffsetDateTime: must not be null' |
      | 'PUT'    | '/paramOffsetDateTime' | 'paramOffsetDateTime' | ''    | 'paramOffsetDateTime: must not be null' |
      | 'PATCH'  | '/paramOffsetDateTime' | 'paramOffsetDateTime' | ''    | 'paramOffsetDateTime: must not be null' |
      | 'DELETE' | '/paramOffsetDateTime' | 'paramOffsetDateTime' | ''    | 'paramOffsetDateTime: must not be null' |
      | 'GET'    | '/paramLocalDateTime'  | 'paramLocalDateTime'  | ''    | 'paramLocalDateTime: must not be null'  |
      | 'HEAD'   | '/paramLocalDateTime'  | 'paramLocalDateTime'  | ''    |                                         |
      | 'POST'   | '/paramLocalDateTime'  | 'paramLocalDateTime'  | ''    | 'paramLocalDateTime: must not be null'  |
      | 'PUT'    | '/paramLocalDateTime'  | 'paramLocalDateTime'  | ''    | 'paramLocalDateTime: must not be null'  |
      | 'PATCH'  | '/paramLocalDateTime'  | 'paramLocalDateTime'  | ''    | 'paramLocalDateTime: must not be null'  |
      | 'DELETE' | '/paramLocalDateTime'  | 'paramLocalDateTime'  | ''    | 'paramLocalDateTime: must not be null'  |
      | 'GET'    | '/paramLocalDate'      | 'paramLocalDate'      | ''    | 'paramLocalDate: must not be null'      |
      | 'HEAD'   | '/paramLocalDate'      | 'paramLocalDate'      | ''    |                                         |
      | 'POST'   | '/paramLocalDate'      | 'paramLocalDate'      | ''    | 'paramLocalDate: must not be null'      |
      | 'PUT'    | '/paramLocalDate'      | 'paramLocalDate'      | ''    | 'paramLocalDate: must not be null'      |
      | 'PATCH'  | '/paramLocalDate'      | 'paramLocalDate'      | ''    | 'paramLocalDate: must not be null'      |
      | 'DELETE' | '/paramLocalDate'      | 'paramLocalDate'      | ''    | 'paramLocalDate: must not be null'      |
      | 'GET'    | '/paramLocalTime'      | 'paramLocalTime'      | ''    | 'paramLocalTime: must not be null'      |
      | 'HEAD'   | '/paramLocalTime'      | 'paramLocalTime'      | ''    |                                         |
      | 'POST'   | '/paramLocalTime'      | 'paramLocalTime'      | ''    | 'paramLocalTime: must not be null'      |
      | 'PUT'    | '/paramLocalTime'      | 'paramLocalTime'      | ''    | 'paramLocalTime: must not be null'      |
      | 'PATCH'  | '/paramLocalTime'      | 'paramLocalTime'      | ''    | 'paramLocalTime: must not be null'      |
      | 'DELETE' | '/paramLocalTime'      | 'paramLocalTime'      | ''    | 'paramLocalTime: must not be null'      |
      | 'GET'    | '/paramYear'           | 'paramYear'           | ''    | 'paramYear: must not be null'           |
      | 'HEAD'   | '/paramYear'           | 'paramYear'           | ''    |                                         |
      | 'POST'   | '/paramYear'           | 'paramYear'           | ''    | 'paramYear: must not be null'           |
      | 'PUT'    | '/paramYear'           | 'paramYear'           | ''    | 'paramYear: must not be null'           |
      | 'PATCH'  | '/paramYear'           | 'paramYear'           | ''    | 'paramYear: must not be null'           |
      | 'DELETE' | '/paramYear'           | 'paramYear'           | ''    | 'paramYear: must not be null'           |
      | 'GET'    | '/paramEnumAB'         | 'paramEnumAB'         | ''    | 'paramEnumAB: must not be null'         |
      | 'HEAD'   | '/paramEnumAB'         | 'paramEnumAB'         | ''    |                                         |
      | 'POST'   | '/paramEnumAB'         | 'paramEnumAB'         | ''    | 'paramEnumAB: must not be null'         |
      | 'PUT'    | '/paramEnumAB'         | 'paramEnumAB'         | ''    | 'paramEnumAB: must not be null'         |
      | 'PATCH'  | '/paramEnumAB'         | 'paramEnumAB'         | ''    | 'paramEnumAB: must not be null'         |
      | 'DELETE' | '/paramEnumAB'         | 'paramEnumAB'         | ''    | 'paramEnumAB: must not be null'         |
      | 'GET'    | '/paramEnumLowerAB'    | 'paramEnumLowerAB'    | ''    | 'paramEnumLowerAB: must not be null'    |
      | 'HEAD'   | '/paramEnumLowerAB'    | 'paramEnumLowerAB'    | ''    |                                         |
      | 'POST'   | '/paramEnumLowerAB'    | 'paramEnumLowerAB'    | ''    | 'paramEnumLowerAB: must not be null'    |
      | 'PUT'    | '/paramEnumLowerAB'    | 'paramEnumLowerAB'    | ''    | 'paramEnumLowerAB: must not be null'    |
      | 'PATCH'  | '/paramEnumLowerAB'    | 'paramEnumLowerAB'    | ''    | 'paramEnumLowerAB: must not be null'    |
      | 'DELETE' | '/paramEnumLowerAB'    | 'paramEnumLowerAB'    | ''    | 'paramEnumLowerAB: must not be null'    |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/not-null/nested-dto-attribute<path>?<param>=<value> endpoint (RequestParamConstrainedNotNullRestApi.class)
    this endpoint receives the request parameter in a nested dto attribute, both having the same name
    and the service maps the sent empty value to null, but service responding with an error message since data is must not be null.

    Given the endpoint '/api/v1/request-parameter/not-null/nested-dto-attribute'<path>
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | param                        | value | message                                        |
      | 'GET'    | '/paramString'         | 'nested.paramString'         | ''    | 'nested.paramString: must not be null'         |
      | 'HEAD'   | '/paramString'         | 'nested.paramString'         | ''    |                                                |
      | 'POST'   | '/paramString'         | 'nested.paramString'         | ''    | 'nested.paramString: must not be null'         |
      | 'PUT'    | '/paramString'         | 'nested.paramString'         | ''    | 'nested.paramString: must not be null'         |
      | 'PATCH'  | '/paramString'         | 'nested.paramString'         | ''    | 'nested.paramString: must not be null'         |
      | 'DELETE' | '/paramString'         | 'nested.paramString'         | ''    | 'nested.paramString: must not be null'         |
      | 'GET'    | '/paramInteger'        | 'nested.paramInteger'        | ''    | 'nested.paramInteger: must not be null'        |
      | 'HEAD'   | '/paramInteger'        | 'nested.paramInteger'        | ''    |                                                |
      | 'POST'   | '/paramInteger'        | 'nested.paramInteger'        | ''    | 'nested.paramInteger: must not be null'        |
      | 'PUT'    | '/paramInteger'        | 'nested.paramInteger'        | ''    | 'nested.paramInteger: must not be null'        |
      | 'PATCH'  | '/paramInteger'        | 'nested.paramInteger'        | ''    | 'nested.paramInteger: must not be null'        |
      | 'DELETE' | '/paramInteger'        | 'nested.paramInteger'        | ''    | 'nested.paramInteger: must not be null'        |
      | 'GET'    | '/paramLong'           | 'nested.paramLong'           | ''    | 'nested.paramLong: must not be null'           |
      | 'HEAD'   | '/paramLong'           | 'nested.paramLong'           | ''    |                                                |
      | 'POST'   | '/paramLong'           | 'nested.paramLong'           | ''    | 'nested.paramLong: must not be null'           |
      | 'PUT'    | '/paramLong'           | 'nested.paramLong'           | ''    | 'nested.paramLong: must not be null'           |
      | 'PATCH'  | '/paramLong'           | 'nested.paramLong'           | ''    | 'nested.paramLong: must not be null'           |
      | 'DELETE' | '/paramLong'           | 'nested.paramLong'           | ''    | 'nested.paramLong: must not be null'           |
      | 'GET'    | '/paramBigInteger'     | 'nested.paramBigInteger'     | ''    | 'nested.paramBigInteger: must not be null'     |
      | 'HEAD'   | '/paramBigInteger'     | 'nested.paramBigInteger'     | ''    |                                                |
      | 'POST'   | '/paramBigInteger'     | 'nested.paramBigInteger'     | ''    | 'nested.paramBigInteger: must not be null'     |
      | 'PUT'    | '/paramBigInteger'     | 'nested.paramBigInteger'     | ''    | 'nested.paramBigInteger: must not be null'     |
      | 'PATCH'  | '/paramBigInteger'     | 'nested.paramBigInteger'     | ''    | 'nested.paramBigInteger: must not be null'     |
      | 'DELETE' | '/paramBigInteger'     | 'nested.paramBigInteger'     | ''    | 'nested.paramBigInteger: must not be null'     |
      | 'GET'    | '/paramFloat'          | 'nested.paramFloat'          | ''    | 'nested.paramFloat: must not be null'          |
      | 'HEAD'   | '/paramFloat'          | 'nested.paramFloat'          | ''    |                                                |
      | 'POST'   | '/paramFloat'          | 'nested.paramFloat'          | ''    | 'nested.paramFloat: must not be null'          |
      | 'PUT'    | '/paramFloat'          | 'nested.paramFloat'          | ''    | 'nested.paramFloat: must not be null'          |
      | 'PATCH'  | '/paramFloat'          | 'nested.paramFloat'          | ''    | 'nested.paramFloat: must not be null'          |
      | 'DELETE' | '/paramFloat'          | 'nested.paramFloat'          | ''    | 'nested.paramFloat: must not be null'          |
      | 'GET'    | '/paramDouble'         | 'nested.paramDouble'         | ''    | 'nested.paramDouble: must not be null'         |
      | 'HEAD'   | '/paramDouble'         | 'nested.paramDouble'         | ''    |                                                |
      | 'POST'   | '/paramDouble'         | 'nested.paramDouble'         | ''    | 'nested.paramDouble: must not be null'         |
      | 'PUT'    | '/paramDouble'         | 'nested.paramDouble'         | ''    | 'nested.paramDouble: must not be null'         |
      | 'PATCH'  | '/paramDouble'         | 'nested.paramDouble'         | ''    | 'nested.paramDouble: must not be null'         |
      | 'DELETE' | '/paramDouble'         | 'nested.paramDouble'         | ''    | 'nested.paramDouble: must not be null'         |
      | 'GET'    | '/paramBigDecimal'     | 'nested.paramBigDecimal'     | ''    | 'nested.paramBigDecimal: must not be null'     |
      | 'HEAD'   | '/paramBigDecimal'     | 'nested.paramBigDecimal'     | ''    |                                                |
      | 'POST'   | '/paramBigDecimal'     | 'nested.paramBigDecimal'     | ''    | 'nested.paramBigDecimal: must not be null'     |
      | 'PUT'    | '/paramBigDecimal'     | 'nested.paramBigDecimal'     | ''    | 'nested.paramBigDecimal: must not be null'     |
      | 'PATCH'  | '/paramBigDecimal'     | 'nested.paramBigDecimal'     | ''    | 'nested.paramBigDecimal: must not be null'     |
      | 'DELETE' | '/paramBigDecimal'     | 'nested.paramBigDecimal'     | ''    | 'nested.paramBigDecimal: must not be null'     |
      | 'GET'    | '/paramBoolean'        | 'nested.paramBoolean'        | ''    | 'nested.paramBoolean: must not be null'        |
      | 'HEAD'   | '/paramBoolean'        | 'nested.paramBoolean'        | ''    |                                                |
      | 'POST'   | '/paramBoolean'        | 'nested.paramBoolean'        | ''    | 'nested.paramBoolean: must not be null'        |
      | 'PUT'    | '/paramBoolean'        | 'nested.paramBoolean'        | ''    | 'nested.paramBoolean: must not be null'        |
      | 'PATCH'  | '/paramBoolean'        | 'nested.paramBoolean'        | ''    | 'nested.paramBoolean: must not be null'        |
      | 'DELETE' | '/paramBoolean'        | 'nested.paramBoolean'        | ''    | 'nested.paramBoolean: must not be null'        |
      | 'GET'    | '/paramOffsetDateTime' | 'nested.paramOffsetDateTime' | ''    | 'nested.paramOffsetDateTime: must not be null' |
      | 'HEAD'   | '/paramOffsetDateTime' | 'nested.paramOffsetDateTime' | ''    |                                                |
      | 'POST'   | '/paramOffsetDateTime' | 'nested.paramOffsetDateTime' | ''    | 'nested.paramOffsetDateTime: must not be null' |
      | 'PUT'    | '/paramOffsetDateTime' | 'nested.paramOffsetDateTime' | ''    | 'nested.paramOffsetDateTime: must not be null' |
      | 'PATCH'  | '/paramOffsetDateTime' | 'nested.paramOffsetDateTime' | ''    | 'nested.paramOffsetDateTime: must not be null' |
      | 'DELETE' | '/paramOffsetDateTime' | 'nested.paramOffsetDateTime' | ''    | 'nested.paramOffsetDateTime: must not be null' |
      | 'GET'    | '/paramLocalDateTime'  | 'nested.paramLocalDateTime'  | ''    | 'nested.paramLocalDateTime: must not be null'  |
      | 'HEAD'   | '/paramLocalDateTime'  | 'nested.paramLocalDateTime'  | ''    |                                                |
      | 'POST'   | '/paramLocalDateTime'  | 'nested.paramLocalDateTime'  | ''    | 'nested.paramLocalDateTime: must not be null'  |
      | 'PUT'    | '/paramLocalDateTime'  | 'nested.paramLocalDateTime'  | ''    | 'nested.paramLocalDateTime: must not be null'  |
      | 'PATCH'  | '/paramLocalDateTime'  | 'nested.paramLocalDateTime'  | ''    | 'nested.paramLocalDateTime: must not be null'  |
      | 'DELETE' | '/paramLocalDateTime'  | 'nested.paramLocalDateTime'  | ''    | 'nested.paramLocalDateTime: must not be null'  |
      | 'GET'    | '/paramLocalDate'      | 'nested.paramLocalDate'      | ''    | 'nested.paramLocalDate: must not be null'      |
      | 'HEAD'   | '/paramLocalDate'      | 'nested.paramLocalDate'      | ''    |                                                |
      | 'POST'   | '/paramLocalDate'      | 'nested.paramLocalDate'      | ''    | 'nested.paramLocalDate: must not be null'      |
      | 'PUT'    | '/paramLocalDate'      | 'nested.paramLocalDate'      | ''    | 'nested.paramLocalDate: must not be null'      |
      | 'PATCH'  | '/paramLocalDate'      | 'nested.paramLocalDate'      | ''    | 'nested.paramLocalDate: must not be null'      |
      | 'DELETE' | '/paramLocalDate'      | 'nested.paramLocalDate'      | ''    | 'nested.paramLocalDate: must not be null'      |
      | 'GET'    | '/paramLocalTime'      | 'nested.paramLocalTime'      | ''    | 'nested.paramLocalTime: must not be null'      |
      | 'HEAD'   | '/paramLocalTime'      | 'nested.paramLocalTime'      | ''    |                                                |
      | 'POST'   | '/paramLocalTime'      | 'nested.paramLocalTime'      | ''    | 'nested.paramLocalTime: must not be null'      |
      | 'PUT'    | '/paramLocalTime'      | 'nested.paramLocalTime'      | ''    | 'nested.paramLocalTime: must not be null'      |
      | 'PATCH'  | '/paramLocalTime'      | 'nested.paramLocalTime'      | ''    | 'nested.paramLocalTime: must not be null'      |
      | 'DELETE' | '/paramLocalTime'      | 'nested.paramLocalTime'      | ''    | 'nested.paramLocalTime: must not be null'      |
      | 'GET'    | '/paramYear'           | 'nested.paramYear'           | ''    | 'nested.paramYear: must not be null'           |
      | 'HEAD'   | '/paramYear'           | 'nested.paramYear'           | ''    |                                                |
      | 'POST'   | '/paramYear'           | 'nested.paramYear'           | ''    | 'nested.paramYear: must not be null'           |
      | 'PUT'    | '/paramYear'           | 'nested.paramYear'           | ''    | 'nested.paramYear: must not be null'           |
      | 'PATCH'  | '/paramYear'           | 'nested.paramYear'           | ''    | 'nested.paramYear: must not be null'           |
      | 'DELETE' | '/paramYear'           | 'nested.paramYear'           | ''    | 'nested.paramYear: must not be null'           |
      | 'GET'    | '/paramEnumAB'         | 'nested.paramEnumAB'         | ''    | 'nested.paramEnumAB: must not be null'         |
      | 'HEAD'   | '/paramEnumAB'         | 'nested.paramEnumAB'         | ''    |                                                |
      | 'POST'   | '/paramEnumAB'         | 'nested.paramEnumAB'         | ''    | 'nested.paramEnumAB: must not be null'         |
      | 'PUT'    | '/paramEnumAB'         | 'nested.paramEnumAB'         | ''    | 'nested.paramEnumAB: must not be null'         |
      | 'PATCH'  | '/paramEnumAB'         | 'nested.paramEnumAB'         | ''    | 'nested.paramEnumAB: must not be null'         |
      | 'DELETE' | '/paramEnumAB'         | 'nested.paramEnumAB'         | ''    | 'nested.paramEnumAB: must not be null'         |
      | 'GET'    | '/paramEnumLowerAB'    | 'nested.paramEnumLowerAB'    | ''    | 'nested.paramEnumLowerAB: must not be null'    |
      | 'HEAD'   | '/paramEnumLowerAB'    | 'nested.paramEnumLowerAB'    | ''    |                                                |
      | 'POST'   | '/paramEnumLowerAB'    | 'nested.paramEnumLowerAB'    | ''    | 'nested.paramEnumLowerAB: must not be null'    |
      | 'PUT'    | '/paramEnumLowerAB'    | 'nested.paramEnumLowerAB'    | ''    | 'nested.paramEnumLowerAB: must not be null'    |
      | 'PATCH'  | '/paramEnumLowerAB'    | 'nested.paramEnumLowerAB'    | ''    | 'nested.paramEnumLowerAB: must not be null'    |
      | 'DELETE' | '/paramEnumLowerAB'    | 'nested.paramEnumLowerAB'    | ''    | 'nested.paramEnumLowerAB: must not be null'    |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/not-null/nested-optional-dto-attribute<path>?<param>=<value> endpoint (RequestParamConstrainedNotNullRestApi.class)
    this endpoint receives the request parameter in a nested optional dto attribute, both having the same name
    and the service maps the sent empty value to null, but service responding with an error message since data is must not be null.

    Given the endpoint '/api/v1/request-parameter/not-null/nested-optional-dto-attribute'<path>
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | param                        | value | message                                        |
      | 'GET'    | '/paramString'         | 'nested.paramString'         | ''    | 'nested.paramString: must not be null'         |
      | 'HEAD'   | '/paramString'         | 'nested.paramString'         | ''    |                                                |
      | 'POST'   | '/paramString'         | 'nested.paramString'         | ''    | 'nested.paramString: must not be null'         |
      | 'PUT'    | '/paramString'         | 'nested.paramString'         | ''    | 'nested.paramString: must not be null'         |
      | 'PATCH'  | '/paramString'         | 'nested.paramString'         | ''    | 'nested.paramString: must not be null'         |
      | 'DELETE' | '/paramString'         | 'nested.paramString'         | ''    | 'nested.paramString: must not be null'         |
      | 'GET'    | '/paramInteger'        | 'nested.paramInteger'        | ''    | 'nested.paramInteger: must not be null'        |
      | 'HEAD'   | '/paramInteger'        | 'nested.paramInteger'        | ''    |                                                |
      | 'POST'   | '/paramInteger'        | 'nested.paramInteger'        | ''    | 'nested.paramInteger: must not be null'        |
      | 'PUT'    | '/paramInteger'        | 'nested.paramInteger'        | ''    | 'nested.paramInteger: must not be null'        |
      | 'PATCH'  | '/paramInteger'        | 'nested.paramInteger'        | ''    | 'nested.paramInteger: must not be null'        |
      | 'DELETE' | '/paramInteger'        | 'nested.paramInteger'        | ''    | 'nested.paramInteger: must not be null'        |
      | 'GET'    | '/paramLong'           | 'nested.paramLong'           | ''    | 'nested.paramLong: must not be null'           |
      | 'HEAD'   | '/paramLong'           | 'nested.paramLong'           | ''    |                                                |
      | 'POST'   | '/paramLong'           | 'nested.paramLong'           | ''    | 'nested.paramLong: must not be null'           |
      | 'PUT'    | '/paramLong'           | 'nested.paramLong'           | ''    | 'nested.paramLong: must not be null'           |
      | 'PATCH'  | '/paramLong'           | 'nested.paramLong'           | ''    | 'nested.paramLong: must not be null'           |
      | 'DELETE' | '/paramLong'           | 'nested.paramLong'           | ''    | 'nested.paramLong: must not be null'           |
      | 'GET'    | '/paramBigInteger'     | 'nested.paramBigInteger'     | ''    | 'nested.paramBigInteger: must not be null'     |
      | 'HEAD'   | '/paramBigInteger'     | 'nested.paramBigInteger'     | ''    |                                                |
      | 'POST'   | '/paramBigInteger'     | 'nested.paramBigInteger'     | ''    | 'nested.paramBigInteger: must not be null'     |
      | 'PUT'    | '/paramBigInteger'     | 'nested.paramBigInteger'     | ''    | 'nested.paramBigInteger: must not be null'     |
      | 'PATCH'  | '/paramBigInteger'     | 'nested.paramBigInteger'     | ''    | 'nested.paramBigInteger: must not be null'     |
      | 'DELETE' | '/paramBigInteger'     | 'nested.paramBigInteger'     | ''    | 'nested.paramBigInteger: must not be null'     |
      | 'GET'    | '/paramFloat'          | 'nested.paramFloat'          | ''    | 'nested.paramFloat: must not be null'          |
      | 'HEAD'   | '/paramFloat'          | 'nested.paramFloat'          | ''    |                                                |
      | 'POST'   | '/paramFloat'          | 'nested.paramFloat'          | ''    | 'nested.paramFloat: must not be null'          |
      | 'PUT'    | '/paramFloat'          | 'nested.paramFloat'          | ''    | 'nested.paramFloat: must not be null'          |
      | 'PATCH'  | '/paramFloat'          | 'nested.paramFloat'          | ''    | 'nested.paramFloat: must not be null'          |
      | 'DELETE' | '/paramFloat'          | 'nested.paramFloat'          | ''    | 'nested.paramFloat: must not be null'          |
      | 'GET'    | '/paramDouble'         | 'nested.paramDouble'         | ''    | 'nested.paramDouble: must not be null'         |
      | 'HEAD'   | '/paramDouble'         | 'nested.paramDouble'         | ''    |                                                |
      | 'POST'   | '/paramDouble'         | 'nested.paramDouble'         | ''    | 'nested.paramDouble: must not be null'         |
      | 'PUT'    | '/paramDouble'         | 'nested.paramDouble'         | ''    | 'nested.paramDouble: must not be null'         |
      | 'PATCH'  | '/paramDouble'         | 'nested.paramDouble'         | ''    | 'nested.paramDouble: must not be null'         |
      | 'DELETE' | '/paramDouble'         | 'nested.paramDouble'         | ''    | 'nested.paramDouble: must not be null'         |
      | 'GET'    | '/paramBigDecimal'     | 'nested.paramBigDecimal'     | ''    | 'nested.paramBigDecimal: must not be null'     |
      | 'HEAD'   | '/paramBigDecimal'     | 'nested.paramBigDecimal'     | ''    |                                                |
      | 'POST'   | '/paramBigDecimal'     | 'nested.paramBigDecimal'     | ''    | 'nested.paramBigDecimal: must not be null'     |
      | 'PUT'    | '/paramBigDecimal'     | 'nested.paramBigDecimal'     | ''    | 'nested.paramBigDecimal: must not be null'     |
      | 'PATCH'  | '/paramBigDecimal'     | 'nested.paramBigDecimal'     | ''    | 'nested.paramBigDecimal: must not be null'     |
      | 'DELETE' | '/paramBigDecimal'     | 'nested.paramBigDecimal'     | ''    | 'nested.paramBigDecimal: must not be null'     |
      | 'GET'    | '/paramBoolean'        | 'nested.paramBoolean'        | ''    | 'nested.paramBoolean: must not be null'        |
      | 'HEAD'   | '/paramBoolean'        | 'nested.paramBoolean'        | ''    |                                                |
      | 'POST'   | '/paramBoolean'        | 'nested.paramBoolean'        | ''    | 'nested.paramBoolean: must not be null'        |
      | 'PUT'    | '/paramBoolean'        | 'nested.paramBoolean'        | ''    | 'nested.paramBoolean: must not be null'        |
      | 'PATCH'  | '/paramBoolean'        | 'nested.paramBoolean'        | ''    | 'nested.paramBoolean: must not be null'        |
      | 'DELETE' | '/paramBoolean'        | 'nested.paramBoolean'        | ''    | 'nested.paramBoolean: must not be null'        |
      | 'GET'    | '/paramOffsetDateTime' | 'nested.paramOffsetDateTime' | ''    | 'nested.paramOffsetDateTime: must not be null' |
      | 'HEAD'   | '/paramOffsetDateTime' | 'nested.paramOffsetDateTime' | ''    |                                                |
      | 'POST'   | '/paramOffsetDateTime' | 'nested.paramOffsetDateTime' | ''    | 'nested.paramOffsetDateTime: must not be null' |
      | 'PUT'    | '/paramOffsetDateTime' | 'nested.paramOffsetDateTime' | ''    | 'nested.paramOffsetDateTime: must not be null' |
      | 'PATCH'  | '/paramOffsetDateTime' | 'nested.paramOffsetDateTime' | ''    | 'nested.paramOffsetDateTime: must not be null' |
      | 'DELETE' | '/paramOffsetDateTime' | 'nested.paramOffsetDateTime' | ''    | 'nested.paramOffsetDateTime: must not be null' |
      | 'GET'    | '/paramLocalDateTime'  | 'nested.paramLocalDateTime'  | ''    | 'nested.paramLocalDateTime: must not be null'  |
      | 'HEAD'   | '/paramLocalDateTime'  | 'nested.paramLocalDateTime'  | ''    |                                                |
      | 'POST'   | '/paramLocalDateTime'  | 'nested.paramLocalDateTime'  | ''    | 'nested.paramLocalDateTime: must not be null'  |
      | 'PUT'    | '/paramLocalDateTime'  | 'nested.paramLocalDateTime'  | ''    | 'nested.paramLocalDateTime: must not be null'  |
      | 'PATCH'  | '/paramLocalDateTime'  | 'nested.paramLocalDateTime'  | ''    | 'nested.paramLocalDateTime: must not be null'  |
      | 'DELETE' | '/paramLocalDateTime'  | 'nested.paramLocalDateTime'  | ''    | 'nested.paramLocalDateTime: must not be null'  |
      | 'GET'    | '/paramLocalDate'      | 'nested.paramLocalDate'      | ''    | 'nested.paramLocalDate: must not be null'      |
      | 'HEAD'   | '/paramLocalDate'      | 'nested.paramLocalDate'      | ''    |                                                |
      | 'POST'   | '/paramLocalDate'      | 'nested.paramLocalDate'      | ''    | 'nested.paramLocalDate: must not be null'      |
      | 'PUT'    | '/paramLocalDate'      | 'nested.paramLocalDate'      | ''    | 'nested.paramLocalDate: must not be null'      |
      | 'PATCH'  | '/paramLocalDate'      | 'nested.paramLocalDate'      | ''    | 'nested.paramLocalDate: must not be null'      |
      | 'DELETE' | '/paramLocalDate'      | 'nested.paramLocalDate'      | ''    | 'nested.paramLocalDate: must not be null'      |
      | 'GET'    | '/paramLocalTime'      | 'nested.paramLocalTime'      | ''    | 'nested.paramLocalTime: must not be null'      |
      | 'HEAD'   | '/paramLocalTime'      | 'nested.paramLocalTime'      | ''    |                                                |
      | 'POST'   | '/paramLocalTime'      | 'nested.paramLocalTime'      | ''    | 'nested.paramLocalTime: must not be null'      |
      | 'PUT'    | '/paramLocalTime'      | 'nested.paramLocalTime'      | ''    | 'nested.paramLocalTime: must not be null'      |
      | 'PATCH'  | '/paramLocalTime'      | 'nested.paramLocalTime'      | ''    | 'nested.paramLocalTime: must not be null'      |
      | 'DELETE' | '/paramLocalTime'      | 'nested.paramLocalTime'      | ''    | 'nested.paramLocalTime: must not be null'      |
      | 'GET'    | '/paramYear'           | 'nested.paramYear'           | ''    | 'nested.paramYear: must not be null'           |
      | 'HEAD'   | '/paramYear'           | 'nested.paramYear'           | ''    |                                                |
      | 'POST'   | '/paramYear'           | 'nested.paramYear'           | ''    | 'nested.paramYear: must not be null'           |
      | 'PUT'    | '/paramYear'           | 'nested.paramYear'           | ''    | 'nested.paramYear: must not be null'           |
      | 'PATCH'  | '/paramYear'           | 'nested.paramYear'           | ''    | 'nested.paramYear: must not be null'           |
      | 'DELETE' | '/paramYear'           | 'nested.paramYear'           | ''    | 'nested.paramYear: must not be null'           |
      | 'GET'    | '/paramEnumAB'         | 'nested.paramEnumAB'         | ''    | 'nested.paramEnumAB: must not be null'         |
      | 'HEAD'   | '/paramEnumAB'         | 'nested.paramEnumAB'         | ''    |                                                |
      | 'POST'   | '/paramEnumAB'         | 'nested.paramEnumAB'         | ''    | 'nested.paramEnumAB: must not be null'         |
      | 'PUT'    | '/paramEnumAB'         | 'nested.paramEnumAB'         | ''    | 'nested.paramEnumAB: must not be null'         |
      | 'PATCH'  | '/paramEnumAB'         | 'nested.paramEnumAB'         | ''    | 'nested.paramEnumAB: must not be null'         |
      | 'DELETE' | '/paramEnumAB'         | 'nested.paramEnumAB'         | ''    | 'nested.paramEnumAB: must not be null'         |
      | 'GET'    | '/paramEnumLowerAB'    | 'nested.paramEnumLowerAB'    | ''    | 'nested.paramEnumLowerAB: must not be null'    |
      | 'HEAD'   | '/paramEnumLowerAB'    | 'nested.paramEnumLowerAB'    | ''    |                                                |
      | 'POST'   | '/paramEnumLowerAB'    | 'nested.paramEnumLowerAB'    | ''    | 'nested.paramEnumLowerAB: must not be null'    |
      | 'PUT'    | '/paramEnumLowerAB'    | 'nested.paramEnumLowerAB'    | ''    | 'nested.paramEnumLowerAB: must not be null'    |
      | 'PATCH'  | '/paramEnumLowerAB'    | 'nested.paramEnumLowerAB'    | ''    | 'nested.paramEnumLowerAB: must not be null'    |
      | 'DELETE' | '/paramEnumLowerAB'    | 'nested.paramEnumLowerAB'    | ''    | 'nested.paramEnumLowerAB: must not be null'    |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/not-null/nested-dto-attribute<path>?<param>=<value> endpoint (RequestParamConstrainedNotNullRestApi.class)
    this endpoint receives the request parameter in a nested dto attribute from an array, both having the same name
    and the service maps the sent empty value to null, but service responding with an error message since data is must not be null.

    Given the endpoint '/api/v1/request-parameter/not-null/nested-dto-attribute'<path>
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | param                          | value | message                                          |
      | 'GET'    | '/paramString'         | 'array[0].paramString'         | ''    | 'array[0].paramString: must not be null'         |
      | 'HEAD'   | '/paramString'         | 'array[0].paramString'         | ''    |                                                  |
      | 'POST'   | '/paramString'         | 'array[0].paramString'         | ''    | 'array[0].paramString: must not be null'         |
      | 'PUT'    | '/paramString'         | 'array[0].paramString'         | ''    | 'array[0].paramString: must not be null'         |
      | 'PATCH'  | '/paramString'         | 'array[0].paramString'         | ''    | 'array[0].paramString: must not be null'         |
      | 'DELETE' | '/paramString'         | 'array[0].paramString'         | ''    | 'array[0].paramString: must not be null'         |
      | 'GET'    | '/paramInteger'        | 'array[0].paramInteger'        | ''    | 'array[0].paramInteger: must not be null'        |
      | 'HEAD'   | '/paramInteger'        | 'array[0].paramInteger'        | ''    |                                                  |
      | 'POST'   | '/paramInteger'        | 'array[0].paramInteger'        | ''    | 'array[0].paramInteger: must not be null'        |
      | 'PUT'    | '/paramInteger'        | 'array[0].paramInteger'        | ''    | 'array[0].paramInteger: must not be null'        |
      | 'PATCH'  | '/paramInteger'        | 'array[0].paramInteger'        | ''    | 'array[0].paramInteger: must not be null'        |
      | 'DELETE' | '/paramInteger'        | 'array[0].paramInteger'        | ''    | 'array[0].paramInteger: must not be null'        |
      | 'GET'    | '/paramLong'           | 'array[0].paramLong'           | ''    | 'array[0].paramLong: must not be null'           |
      | 'HEAD'   | '/paramLong'           | 'array[0].paramLong'           | ''    |                                                  |
      | 'POST'   | '/paramLong'           | 'array[0].paramLong'           | ''    | 'array[0].paramLong: must not be null'           |
      | 'PUT'    | '/paramLong'           | 'array[0].paramLong'           | ''    | 'array[0].paramLong: must not be null'           |
      | 'PATCH'  | '/paramLong'           | 'array[0].paramLong'           | ''    | 'array[0].paramLong: must not be null'           |
      | 'DELETE' | '/paramLong'           | 'array[0].paramLong'           | ''    | 'array[0].paramLong: must not be null'           |
      | 'GET'    | '/paramBigInteger'     | 'array[0].paramBigInteger'     | ''    | 'array[0].paramBigInteger: must not be null'     |
      | 'HEAD'   | '/paramBigInteger'     | 'array[0].paramBigInteger'     | ''    |                                                  |
      | 'POST'   | '/paramBigInteger'     | 'array[0].paramBigInteger'     | ''    | 'array[0].paramBigInteger: must not be null'     |
      | 'PUT'    | '/paramBigInteger'     | 'array[0].paramBigInteger'     | ''    | 'array[0].paramBigInteger: must not be null'     |
      | 'PATCH'  | '/paramBigInteger'     | 'array[0].paramBigInteger'     | ''    | 'array[0].paramBigInteger: must not be null'     |
      | 'DELETE' | '/paramBigInteger'     | 'array[0].paramBigInteger'     | ''    | 'array[0].paramBigInteger: must not be null'     |
      | 'GET'    | '/paramFloat'          | 'array[0].paramFloat'          | ''    | 'array[0].paramFloat: must not be null'          |
      | 'HEAD'   | '/paramFloat'          | 'array[0].paramFloat'          | ''    |                                                  |
      | 'POST'   | '/paramFloat'          | 'array[0].paramFloat'          | ''    | 'array[0].paramFloat: must not be null'          |
      | 'PUT'    | '/paramFloat'          | 'array[0].paramFloat'          | ''    | 'array[0].paramFloat: must not be null'          |
      | 'PATCH'  | '/paramFloat'          | 'array[0].paramFloat'          | ''    | 'array[0].paramFloat: must not be null'          |
      | 'DELETE' | '/paramFloat'          | 'array[0].paramFloat'          | ''    | 'array[0].paramFloat: must not be null'          |
      | 'GET'    | '/paramDouble'         | 'array[0].paramDouble'         | ''    | 'array[0].paramDouble: must not be null'         |
      | 'HEAD'   | '/paramDouble'         | 'array[0].paramDouble'         | ''    |                                                  |
      | 'POST'   | '/paramDouble'         | 'array[0].paramDouble'         | ''    | 'array[0].paramDouble: must not be null'         |
      | 'PUT'    | '/paramDouble'         | 'array[0].paramDouble'         | ''    | 'array[0].paramDouble: must not be null'         |
      | 'PATCH'  | '/paramDouble'         | 'array[0].paramDouble'         | ''    | 'array[0].paramDouble: must not be null'         |
      | 'DELETE' | '/paramDouble'         | 'array[0].paramDouble'         | ''    | 'array[0].paramDouble: must not be null'         |
      | 'GET'    | '/paramBigDecimal'     | 'array[0].paramBigDecimal'     | ''    | 'array[0].paramBigDecimal: must not be null'     |
      | 'HEAD'   | '/paramBigDecimal'     | 'array[0].paramBigDecimal'     | ''    |                                                  |
      | 'POST'   | '/paramBigDecimal'     | 'array[0].paramBigDecimal'     | ''    | 'array[0].paramBigDecimal: must not be null'     |
      | 'PUT'    | '/paramBigDecimal'     | 'array[0].paramBigDecimal'     | ''    | 'array[0].paramBigDecimal: must not be null'     |
      | 'PATCH'  | '/paramBigDecimal'     | 'array[0].paramBigDecimal'     | ''    | 'array[0].paramBigDecimal: must not be null'     |
      | 'DELETE' | '/paramBigDecimal'     | 'array[0].paramBigDecimal'     | ''    | 'array[0].paramBigDecimal: must not be null'     |
      | 'GET'    | '/paramBoolean'        | 'array[0].paramBoolean'        | ''    | 'array[0].paramBoolean: must not be null'        |
      | 'HEAD'   | '/paramBoolean'        | 'array[0].paramBoolean'        | ''    |                                                  |
      | 'POST'   | '/paramBoolean'        | 'array[0].paramBoolean'        | ''    | 'array[0].paramBoolean: must not be null'        |
      | 'PUT'    | '/paramBoolean'        | 'array[0].paramBoolean'        | ''    | 'array[0].paramBoolean: must not be null'        |
      | 'PATCH'  | '/paramBoolean'        | 'array[0].paramBoolean'        | ''    | 'array[0].paramBoolean: must not be null'        |
      | 'DELETE' | '/paramBoolean'        | 'array[0].paramBoolean'        | ''    | 'array[0].paramBoolean: must not be null'        |
      | 'GET'    | '/paramOffsetDateTime' | 'array[0].paramOffsetDateTime' | ''    | 'array[0].paramOffsetDateTime: must not be null' |
      | 'HEAD'   | '/paramOffsetDateTime' | 'array[0].paramOffsetDateTime' | ''    |                                                  |
      | 'POST'   | '/paramOffsetDateTime' | 'array[0].paramOffsetDateTime' | ''    | 'array[0].paramOffsetDateTime: must not be null' |
      | 'PUT'    | '/paramOffsetDateTime' | 'array[0].paramOffsetDateTime' | ''    | 'array[0].paramOffsetDateTime: must not be null' |
      | 'PATCH'  | '/paramOffsetDateTime' | 'array[0].paramOffsetDateTime' | ''    | 'array[0].paramOffsetDateTime: must not be null' |
      | 'DELETE' | '/paramOffsetDateTime' | 'array[0].paramOffsetDateTime' | ''    | 'array[0].paramOffsetDateTime: must not be null' |
      | 'GET'    | '/paramLocalDateTime'  | 'array[0].paramLocalDateTime'  | ''    | 'array[0].paramLocalDateTime: must not be null'  |
      | 'HEAD'   | '/paramLocalDateTime'  | 'array[0].paramLocalDateTime'  | ''    |                                                  |
      | 'POST'   | '/paramLocalDateTime'  | 'array[0].paramLocalDateTime'  | ''    | 'array[0].paramLocalDateTime: must not be null'  |
      | 'PUT'    | '/paramLocalDateTime'  | 'array[0].paramLocalDateTime'  | ''    | 'array[0].paramLocalDateTime: must not be null'  |
      | 'PATCH'  | '/paramLocalDateTime'  | 'array[0].paramLocalDateTime'  | ''    | 'array[0].paramLocalDateTime: must not be null'  |
      | 'DELETE' | '/paramLocalDateTime'  | 'array[0].paramLocalDateTime'  | ''    | 'array[0].paramLocalDateTime: must not be null'  |
      | 'GET'    | '/paramLocalDate'      | 'array[0].paramLocalDate'      | ''    | 'array[0].paramLocalDate: must not be null'      |
      | 'HEAD'   | '/paramLocalDate'      | 'array[0].paramLocalDate'      | ''    |                                                  |
      | 'POST'   | '/paramLocalDate'      | 'array[0].paramLocalDate'      | ''    | 'array[0].paramLocalDate: must not be null'      |
      | 'PUT'    | '/paramLocalDate'      | 'array[0].paramLocalDate'      | ''    | 'array[0].paramLocalDate: must not be null'      |
      | 'PATCH'  | '/paramLocalDate'      | 'array[0].paramLocalDate'      | ''    | 'array[0].paramLocalDate: must not be null'      |
      | 'DELETE' | '/paramLocalDate'      | 'array[0].paramLocalDate'      | ''    | 'array[0].paramLocalDate: must not be null'      |
      | 'GET'    | '/paramLocalTime'      | 'array[0].paramLocalTime'      | ''    | 'array[0].paramLocalTime: must not be null'      |
      | 'HEAD'   | '/paramLocalTime'      | 'array[0].paramLocalTime'      | ''    |                                                  |
      | 'POST'   | '/paramLocalTime'      | 'array[0].paramLocalTime'      | ''    | 'array[0].paramLocalTime: must not be null'      |
      | 'PUT'    | '/paramLocalTime'      | 'array[0].paramLocalTime'      | ''    | 'array[0].paramLocalTime: must not be null'      |
      | 'PATCH'  | '/paramLocalTime'      | 'array[0].paramLocalTime'      | ''    | 'array[0].paramLocalTime: must not be null'      |
      | 'DELETE' | '/paramLocalTime'      | 'array[0].paramLocalTime'      | ''    | 'array[0].paramLocalTime: must not be null'      |
      | 'GET'    | '/paramYear'           | 'array[0].paramYear'           | ''    | 'array[0].paramYear: must not be null'           |
      | 'HEAD'   | '/paramYear'           | 'array[0].paramYear'           | ''    |                                                  |
      | 'POST'   | '/paramYear'           | 'array[0].paramYear'           | ''    | 'array[0].paramYear: must not be null'           |
      | 'PUT'    | '/paramYear'           | 'array[0].paramYear'           | ''    | 'array[0].paramYear: must not be null'           |
      | 'PATCH'  | '/paramYear'           | 'array[0].paramYear'           | ''    | 'array[0].paramYear: must not be null'           |
      | 'DELETE' | '/paramYear'           | 'array[0].paramYear'           | ''    | 'array[0].paramYear: must not be null'           |
      | 'GET'    | '/paramEnumAB'         | 'array[0].paramEnumAB'         | ''    | 'array[0].paramEnumAB: must not be null'         |
      | 'HEAD'   | '/paramEnumAB'         | 'array[0].paramEnumAB'         | ''    |                                                  |
      | 'POST'   | '/paramEnumAB'         | 'array[0].paramEnumAB'         | ''    | 'array[0].paramEnumAB: must not be null'         |
      | 'PUT'    | '/paramEnumAB'         | 'array[0].paramEnumAB'         | ''    | 'array[0].paramEnumAB: must not be null'         |
      | 'PATCH'  | '/paramEnumAB'         | 'array[0].paramEnumAB'         | ''    | 'array[0].paramEnumAB: must not be null'         |
      | 'DELETE' | '/paramEnumAB'         | 'array[0].paramEnumAB'         | ''    | 'array[0].paramEnumAB: must not be null'         |
      | 'GET'    | '/paramEnumLowerAB'    | 'array[0].paramEnumLowerAB'    | ''    | 'array[0].paramEnumLowerAB: must not be null'    |
      | 'HEAD'   | '/paramEnumLowerAB'    | 'array[0].paramEnumLowerAB'    | ''    |                                                  |
      | 'POST'   | '/paramEnumLowerAB'    | 'array[0].paramEnumLowerAB'    | ''    | 'array[0].paramEnumLowerAB: must not be null'    |
      | 'PUT'    | '/paramEnumLowerAB'    | 'array[0].paramEnumLowerAB'    | ''    | 'array[0].paramEnumLowerAB: must not be null'    |
      | 'PATCH'  | '/paramEnumLowerAB'    | 'array[0].paramEnumLowerAB'    | ''    | 'array[0].paramEnumLowerAB: must not be null'    |
      | 'DELETE' | '/paramEnumLowerAB'    | 'array[0].paramEnumLowerAB'    | ''    | 'array[0].paramEnumLowerAB: must not be null'    |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/not-null/nested-optional-dto-attribute<path>?<param>=<value> endpoint (RequestParamConstrainedNotNullRestApi.class)
    this endpoint receives the request parameter in a nested optional dto attribute from an array, both having the same name
    and the service maps the sent empty value to null, but service responding with an error message since data is must not be null.

    Given the endpoint '/api/v1/request-parameter/not-null/nested-optional-dto-attribute'<path>
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | param                          | value | message                                          |
      | 'GET'    | '/paramString'         | 'array[0].paramString'         | ''    | 'array[0].paramString: must not be null'         |
      | 'HEAD'   | '/paramString'         | 'array[0].paramString'         | ''    |                                                  |
      | 'POST'   | '/paramString'         | 'array[0].paramString'         | ''    | 'array[0].paramString: must not be null'         |
      | 'PUT'    | '/paramString'         | 'array[0].paramString'         | ''    | 'array[0].paramString: must not be null'         |
      | 'PATCH'  | '/paramString'         | 'array[0].paramString'         | ''    | 'array[0].paramString: must not be null'         |
      | 'DELETE' | '/paramString'         | 'array[0].paramString'         | ''    | 'array[0].paramString: must not be null'         |
      | 'GET'    | '/paramInteger'        | 'array[0].paramInteger'        | ''    | 'array[0].paramInteger: must not be null'        |
      | 'HEAD'   | '/paramInteger'        | 'array[0].paramInteger'        | ''    |                                                  |
      | 'POST'   | '/paramInteger'        | 'array[0].paramInteger'        | ''    | 'array[0].paramInteger: must not be null'        |
      | 'PUT'    | '/paramInteger'        | 'array[0].paramInteger'        | ''    | 'array[0].paramInteger: must not be null'        |
      | 'PATCH'  | '/paramInteger'        | 'array[0].paramInteger'        | ''    | 'array[0].paramInteger: must not be null'        |
      | 'DELETE' | '/paramInteger'        | 'array[0].paramInteger'        | ''    | 'array[0].paramInteger: must not be null'        |
      | 'GET'    | '/paramLong'           | 'array[0].paramLong'           | ''    | 'array[0].paramLong: must not be null'           |
      | 'HEAD'   | '/paramLong'           | 'array[0].paramLong'           | ''    |                                                  |
      | 'POST'   | '/paramLong'           | 'array[0].paramLong'           | ''    | 'array[0].paramLong: must not be null'           |
      | 'PUT'    | '/paramLong'           | 'array[0].paramLong'           | ''    | 'array[0].paramLong: must not be null'           |
      | 'PATCH'  | '/paramLong'           | 'array[0].paramLong'           | ''    | 'array[0].paramLong: must not be null'           |
      | 'DELETE' | '/paramLong'           | 'array[0].paramLong'           | ''    | 'array[0].paramLong: must not be null'           |
      | 'GET'    | '/paramBigInteger'     | 'array[0].paramBigInteger'     | ''    | 'array[0].paramBigInteger: must not be null'     |
      | 'HEAD'   | '/paramBigInteger'     | 'array[0].paramBigInteger'     | ''    |                                                  |
      | 'POST'   | '/paramBigInteger'     | 'array[0].paramBigInteger'     | ''    | 'array[0].paramBigInteger: must not be null'     |
      | 'PUT'    | '/paramBigInteger'     | 'array[0].paramBigInteger'     | ''    | 'array[0].paramBigInteger: must not be null'     |
      | 'PATCH'  | '/paramBigInteger'     | 'array[0].paramBigInteger'     | ''    | 'array[0].paramBigInteger: must not be null'     |
      | 'DELETE' | '/paramBigInteger'     | 'array[0].paramBigInteger'     | ''    | 'array[0].paramBigInteger: must not be null'     |
      | 'GET'    | '/paramFloat'          | 'array[0].paramFloat'          | ''    | 'array[0].paramFloat: must not be null'          |
      | 'HEAD'   | '/paramFloat'          | 'array[0].paramFloat'          | ''    |                                                  |
      | 'POST'   | '/paramFloat'          | 'array[0].paramFloat'          | ''    | 'array[0].paramFloat: must not be null'          |
      | 'PUT'    | '/paramFloat'          | 'array[0].paramFloat'          | ''    | 'array[0].paramFloat: must not be null'          |
      | 'PATCH'  | '/paramFloat'          | 'array[0].paramFloat'          | ''    | 'array[0].paramFloat: must not be null'          |
      | 'DELETE' | '/paramFloat'          | 'array[0].paramFloat'          | ''    | 'array[0].paramFloat: must not be null'          |
      | 'GET'    | '/paramDouble'         | 'array[0].paramDouble'         | ''    | 'array[0].paramDouble: must not be null'         |
      | 'HEAD'   | '/paramDouble'         | 'array[0].paramDouble'         | ''    |                                                  |
      | 'POST'   | '/paramDouble'         | 'array[0].paramDouble'         | ''    | 'array[0].paramDouble: must not be null'         |
      | 'PUT'    | '/paramDouble'         | 'array[0].paramDouble'         | ''    | 'array[0].paramDouble: must not be null'         |
      | 'PATCH'  | '/paramDouble'         | 'array[0].paramDouble'         | ''    | 'array[0].paramDouble: must not be null'         |
      | 'DELETE' | '/paramDouble'         | 'array[0].paramDouble'         | ''    | 'array[0].paramDouble: must not be null'         |
      | 'GET'    | '/paramBigDecimal'     | 'array[0].paramBigDecimal'     | ''    | 'array[0].paramBigDecimal: must not be null'     |
      | 'HEAD'   | '/paramBigDecimal'     | 'array[0].paramBigDecimal'     | ''    |                                                  |
      | 'POST'   | '/paramBigDecimal'     | 'array[0].paramBigDecimal'     | ''    | 'array[0].paramBigDecimal: must not be null'     |
      | 'PUT'    | '/paramBigDecimal'     | 'array[0].paramBigDecimal'     | ''    | 'array[0].paramBigDecimal: must not be null'     |
      | 'PATCH'  | '/paramBigDecimal'     | 'array[0].paramBigDecimal'     | ''    | 'array[0].paramBigDecimal: must not be null'     |
      | 'DELETE' | '/paramBigDecimal'     | 'array[0].paramBigDecimal'     | ''    | 'array[0].paramBigDecimal: must not be null'     |
      | 'GET'    | '/paramBoolean'        | 'array[0].paramBoolean'        | ''    | 'array[0].paramBoolean: must not be null'        |
      | 'HEAD'   | '/paramBoolean'        | 'array[0].paramBoolean'        | ''    |                                                  |
      | 'POST'   | '/paramBoolean'        | 'array[0].paramBoolean'        | ''    | 'array[0].paramBoolean: must not be null'        |
      | 'PUT'    | '/paramBoolean'        | 'array[0].paramBoolean'        | ''    | 'array[0].paramBoolean: must not be null'        |
      | 'PATCH'  | '/paramBoolean'        | 'array[0].paramBoolean'        | ''    | 'array[0].paramBoolean: must not be null'        |
      | 'DELETE' | '/paramBoolean'        | 'array[0].paramBoolean'        | ''    | 'array[0].paramBoolean: must not be null'        |
      | 'GET'    | '/paramOffsetDateTime' | 'array[0].paramOffsetDateTime' | ''    | 'array[0].paramOffsetDateTime: must not be null' |
      | 'HEAD'   | '/paramOffsetDateTime' | 'array[0].paramOffsetDateTime' | ''    |                                                  |
      | 'POST'   | '/paramOffsetDateTime' | 'array[0].paramOffsetDateTime' | ''    | 'array[0].paramOffsetDateTime: must not be null' |
      | 'PUT'    | '/paramOffsetDateTime' | 'array[0].paramOffsetDateTime' | ''    | 'array[0].paramOffsetDateTime: must not be null' |
      | 'PATCH'  | '/paramOffsetDateTime' | 'array[0].paramOffsetDateTime' | ''    | 'array[0].paramOffsetDateTime: must not be null' |
      | 'DELETE' | '/paramOffsetDateTime' | 'array[0].paramOffsetDateTime' | ''    | 'array[0].paramOffsetDateTime: must not be null' |
      | 'GET'    | '/paramLocalDateTime'  | 'array[0].paramLocalDateTime'  | ''    | 'array[0].paramLocalDateTime: must not be null'  |
      | 'HEAD'   | '/paramLocalDateTime'  | 'array[0].paramLocalDateTime'  | ''    |                                                  |
      | 'POST'   | '/paramLocalDateTime'  | 'array[0].paramLocalDateTime'  | ''    | 'array[0].paramLocalDateTime: must not be null'  |
      | 'PUT'    | '/paramLocalDateTime'  | 'array[0].paramLocalDateTime'  | ''    | 'array[0].paramLocalDateTime: must not be null'  |
      | 'PATCH'  | '/paramLocalDateTime'  | 'array[0].paramLocalDateTime'  | ''    | 'array[0].paramLocalDateTime: must not be null'  |
      | 'DELETE' | '/paramLocalDateTime'  | 'array[0].paramLocalDateTime'  | ''    | 'array[0].paramLocalDateTime: must not be null'  |
      | 'GET'    | '/paramLocalDate'      | 'array[0].paramLocalDate'      | ''    | 'array[0].paramLocalDate: must not be null'      |
      | 'HEAD'   | '/paramLocalDate'      | 'array[0].paramLocalDate'      | ''    |                                                  |
      | 'POST'   | '/paramLocalDate'      | 'array[0].paramLocalDate'      | ''    | 'array[0].paramLocalDate: must not be null'      |
      | 'PUT'    | '/paramLocalDate'      | 'array[0].paramLocalDate'      | ''    | 'array[0].paramLocalDate: must not be null'      |
      | 'PATCH'  | '/paramLocalDate'      | 'array[0].paramLocalDate'      | ''    | 'array[0].paramLocalDate: must not be null'      |
      | 'DELETE' | '/paramLocalDate'      | 'array[0].paramLocalDate'      | ''    | 'array[0].paramLocalDate: must not be null'      |
      | 'GET'    | '/paramLocalTime'      | 'array[0].paramLocalTime'      | ''    | 'array[0].paramLocalTime: must not be null'      |
      | 'HEAD'   | '/paramLocalTime'      | 'array[0].paramLocalTime'      | ''    |                                                  |
      | 'POST'   | '/paramLocalTime'      | 'array[0].paramLocalTime'      | ''    | 'array[0].paramLocalTime: must not be null'      |
      | 'PUT'    | '/paramLocalTime'      | 'array[0].paramLocalTime'      | ''    | 'array[0].paramLocalTime: must not be null'      |
      | 'PATCH'  | '/paramLocalTime'      | 'array[0].paramLocalTime'      | ''    | 'array[0].paramLocalTime: must not be null'      |
      | 'DELETE' | '/paramLocalTime'      | 'array[0].paramLocalTime'      | ''    | 'array[0].paramLocalTime: must not be null'      |
      | 'GET'    | '/paramYear'           | 'array[0].paramYear'           | ''    | 'array[0].paramYear: must not be null'           |
      | 'HEAD'   | '/paramYear'           | 'array[0].paramYear'           | ''    |                                                  |
      | 'POST'   | '/paramYear'           | 'array[0].paramYear'           | ''    | 'array[0].paramYear: must not be null'           |
      | 'PUT'    | '/paramYear'           | 'array[0].paramYear'           | ''    | 'array[0].paramYear: must not be null'           |
      | 'PATCH'  | '/paramYear'           | 'array[0].paramYear'           | ''    | 'array[0].paramYear: must not be null'           |
      | 'DELETE' | '/paramYear'           | 'array[0].paramYear'           | ''    | 'array[0].paramYear: must not be null'           |
      | 'GET'    | '/paramEnumAB'         | 'array[0].paramEnumAB'         | ''    | 'array[0].paramEnumAB: must not be null'         |
      | 'HEAD'   | '/paramEnumAB'         | 'array[0].paramEnumAB'         | ''    |                                                  |
      | 'POST'   | '/paramEnumAB'         | 'array[0].paramEnumAB'         | ''    | 'array[0].paramEnumAB: must not be null'         |
      | 'PUT'    | '/paramEnumAB'         | 'array[0].paramEnumAB'         | ''    | 'array[0].paramEnumAB: must not be null'         |
      | 'PATCH'  | '/paramEnumAB'         | 'array[0].paramEnumAB'         | ''    | 'array[0].paramEnumAB: must not be null'         |
      | 'DELETE' | '/paramEnumAB'         | 'array[0].paramEnumAB'         | ''    | 'array[0].paramEnumAB: must not be null'         |
      | 'GET'    | '/paramEnumLowerAB'    | 'array[0].paramEnumLowerAB'    | ''    | 'array[0].paramEnumLowerAB: must not be null'    |
      | 'HEAD'   | '/paramEnumLowerAB'    | 'array[0].paramEnumLowerAB'    | ''    |                                                  |
      | 'POST'   | '/paramEnumLowerAB'    | 'array[0].paramEnumLowerAB'    | ''    | 'array[0].paramEnumLowerAB: must not be null'    |
      | 'PUT'    | '/paramEnumLowerAB'    | 'array[0].paramEnumLowerAB'    | ''    | 'array[0].paramEnumLowerAB: must not be null'    |
      | 'PATCH'  | '/paramEnumLowerAB'    | 'array[0].paramEnumLowerAB'    | ''    | 'array[0].paramEnumLowerAB: must not be null'    |
      | 'DELETE' | '/paramEnumLowerAB'    | 'array[0].paramEnumLowerAB'    | ''    | 'array[0].paramEnumLowerAB: must not be null'    |
