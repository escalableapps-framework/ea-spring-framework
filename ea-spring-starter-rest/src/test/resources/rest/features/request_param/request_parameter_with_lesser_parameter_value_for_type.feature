Feature: Request parameter with lesser parameter value for type
  
  The REST service receives data via the request parameters and cannot interpret the data because it is less than the minimum value allowed. The service responds with an error message.
  
   Rule: The conversion between data types is given by the following table
  
  | parameter type | java type      |
  |----------------|----------------|
  | string         | String         |
  | number         | Integer        |
  | number         | Long           |
  | number         | BigInteger     |
  | number         | Float          |
  | number         | Double         |
  | number         | BigDecimal     |
  | boolean        | Boolean        |
  | ISO-8601       | OffsetDateTime |
  | ISO-8601       | LocalDateTime  |
  | ISO-8601       | LocalDate      |
  | ISO-8601       | LocalTime      |
  | ISO-8601       | Year           |
  | string         | ENUM           |

  Background: 
    Given the default timezone 'America/Puerto_Rico'

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/method-parameter?<param>=<value> endpoint (RequestParamUnconstrainedRestApi.class)
    this endpoint receives the request parameter in a method parameter, both having the same name
    and the service cannot map the sent value to the declared data type, responding with an error message.

    Given the endpoint '/api/v1/request-parameter/method-parameter'
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | param                 | value                             | message                                    |
      | 'GET'    | 'paramInteger'        | '-2147483649'                     | 'paramInteger: is not a valid type'        |
      | 'HEAD'   | 'paramInteger'        | '-2147483649'                     |                                            |
      | 'POST'   | 'paramInteger'        | '-2147483649'                     | 'paramInteger: is not a valid type'        |
      | 'PUT'    | 'paramInteger'        | '-2147483649'                     | 'paramInteger: is not a valid type'        |
      | 'PATCH'  | 'paramInteger'        | '-2147483649'                     | 'paramInteger: is not a valid type'        |
      | 'DELETE' | 'paramInteger'        | '-2147483649'                     | 'paramInteger: is not a valid type'        |
      | 'GET'    | 'paramLong'           | '-9223372036854775809'            | 'paramLong: is not a valid type'           |
      | 'HEAD'   | 'paramLong'           | '-9223372036854775809'            |                                            |
      | 'POST'   | 'paramLong'           | '-9223372036854775809'            | 'paramLong: is not a valid type'           |
      | 'PUT'    | 'paramLong'           | '-9223372036854775809'            | 'paramLong: is not a valid type'           |
      | 'PATCH'  | 'paramLong'           | '-9223372036854775809'            | 'paramLong: is not a valid type'           |
      | 'DELETE' | 'paramLong'           | '-9223372036854775809'            | 'paramLong: is not a valid type'           |
      | 'GET'    | 'paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' | 'paramOffsetDateTime: is not a valid type' |
      | 'HEAD'   | 'paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' |                                            |
      | 'POST'   | 'paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' | 'paramOffsetDateTime: is not a valid type' |
      | 'PUT'    | 'paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' | 'paramOffsetDateTime: is not a valid type' |
      | 'PATCH'  | 'paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' | 'paramOffsetDateTime: is not a valid type' |
      | 'DELETE' | 'paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' | 'paramOffsetDateTime: is not a valid type' |
      | 'GET'    | 'paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' | 'paramLocalDateTime: is not a valid type'  |
      | 'HEAD'   | 'paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' |                                            |
      | 'POST'   | 'paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' | 'paramLocalDateTime: is not a valid type'  |
      | 'PUT'    | 'paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' | 'paramLocalDateTime: is not a valid type'  |
      | 'PATCH'  | 'paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' | 'paramLocalDateTime: is not a valid type'  |
      | 'DELETE' | 'paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' | 'paramLocalDateTime: is not a valid type'  |
      | 'GET'    | 'paramLocalDate'      | '-1000000000-12-31'               | 'paramLocalDate: is not a valid type'      |
      | 'HEAD'   | 'paramLocalDate'      | '-1000000000-12-31'               |                                            |
      | 'POST'   | 'paramLocalDate'      | '-1000000000-12-31'               | 'paramLocalDate: is not a valid type'      |
      | 'PUT'    | 'paramLocalDate'      | '-1000000000-12-31'               | 'paramLocalDate: is not a valid type'      |
      | 'PATCH'  | 'paramLocalDate'      | '-1000000000-12-31'               | 'paramLocalDate: is not a valid type'      |
      | 'DELETE' | 'paramLocalDate'      | '-1000000000-12-31'               | 'paramLocalDate: is not a valid type'      |
      | 'GET'    | 'paramYear'           | '-1000000000'                     | 'paramYear: is not a valid type'           |
      | 'HEAD'   | 'paramYear'           | '-1000000000'                     |                                            |
      | 'POST'   | 'paramYear'           | '-1000000000'                     | 'paramYear: is not a valid type'           |
      | 'PUT'    | 'paramYear'           | '-1000000000'                     | 'paramYear: is not a valid type'           |
      | 'PATCH'  | 'paramYear'           | '-1000000000'                     | 'paramYear: is not a valid type'           |
      | 'DELETE' | 'paramYear'           | '-1000000000'                     | 'paramYear: is not a valid type'           |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/named-method-parameter?<param>=<value> endpoint (RequestParamUnconstrainedRestApi.class)
    this endpoint receives the request parameter in a method parameter, both having the diferent names, a custom request-parameter name is defined by the endpoint 
    and the service cannot map the sent value to the declared data type, responding with an error message.

    Given the endpoint '/api/v1/request-parameter/named-method-parameter'
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | param                    | value                             | message                                       |
      | 'GET'    | 'param-integer'          | '-2147483649'                     | 'param-integer: is not a valid type'          |
      | 'HEAD'   | 'param-integer'          | '-2147483649'                     |                                               |
      | 'POST'   | 'param-integer'          | '-2147483649'                     | 'param-integer: is not a valid type'          |
      | 'PUT'    | 'param-integer'          | '-2147483649'                     | 'param-integer: is not a valid type'          |
      | 'PATCH'  | 'param-integer'          | '-2147483649'                     | 'param-integer: is not a valid type'          |
      | 'DELETE' | 'param-integer'          | '-2147483649'                     | 'param-integer: is not a valid type'          |
      | 'GET'    | 'param-long'             | '-9223372036854775809'            | 'param-long: is not a valid type'             |
      | 'HEAD'   | 'param-long'             | '-9223372036854775809'            |                                               |
      | 'POST'   | 'param-long'             | '-9223372036854775809'            | 'param-long: is not a valid type'             |
      | 'PUT'    | 'param-long'             | '-9223372036854775809'            | 'param-long: is not a valid type'             |
      | 'PATCH'  | 'param-long'             | '-9223372036854775809'            | 'param-long: is not a valid type'             |
      | 'DELETE' | 'param-long'             | '-9223372036854775809'            | 'param-long: is not a valid type'             |
      | 'GET'    | 'param-offset-date-time' | '-1000000000-12-31T23:59:59.999Z' | 'param-offset-date-time: is not a valid type' |
      | 'HEAD'   | 'param-offset-date-time' | '-1000000000-12-31T23:59:59.999Z' |                                               |
      | 'POST'   | 'param-offset-date-time' | '-1000000000-12-31T23:59:59.999Z' | 'param-offset-date-time: is not a valid type' |
      | 'PUT'    | 'param-offset-date-time' | '-1000000000-12-31T23:59:59.999Z' | 'param-offset-date-time: is not a valid type' |
      | 'PATCH'  | 'param-offset-date-time' | '-1000000000-12-31T23:59:59.999Z' | 'param-offset-date-time: is not a valid type' |
      | 'DELETE' | 'param-offset-date-time' | '-1000000000-12-31T23:59:59.999Z' | 'param-offset-date-time: is not a valid type' |
      | 'GET'    | 'param-offset-date-time' | '-1000000000-12-31T23:59:59.999Z' | 'param-offset-date-time: is not a valid type' |
      | 'HEAD'   | 'param-offset-date-time' | '-1000000000-12-31T23:59:59.999Z' |                                               |
      | 'POST'   | 'param-offset-date-time' | '-1000000000-12-31T23:59:59.999Z' | 'param-offset-date-time: is not a valid type' |
      | 'PUT'    | 'param-offset-date-time' | '-1000000000-12-31T23:59:59.999Z' | 'param-offset-date-time: is not a valid type' |
      | 'PATCH'  | 'param-offset-date-time' | '-1000000000-12-31T23:59:59.999Z' | 'param-offset-date-time: is not a valid type' |
      | 'DELETE' | 'param-offset-date-time' | '-1000000000-12-31T23:59:59.999Z' | 'param-offset-date-time: is not a valid type' |
      | 'GET'    | 'param-local-date'       | '-1000000000-12-31'               | 'param-local-date: is not a valid type'       |
      | 'HEAD'   | 'param-local-date'       | '-1000000000-12-31'               |                                               |
      | 'POST'   | 'param-local-date'       | '-1000000000-12-31'               | 'param-local-date: is not a valid type'       |
      | 'PUT'    | 'param-local-date'       | '-1000000000-12-31'               | 'param-local-date: is not a valid type'       |
      | 'PATCH'  | 'param-local-date'       | '-1000000000-12-31'               | 'param-local-date: is not a valid type'       |
      | 'DELETE' | 'param-local-date'       | '-1000000000-12-31'               | 'param-local-date: is not a valid type'       |
      | 'GET'    | 'param-year'             | '-1000000000'                     | 'param-year: is not a valid type'             |
      | 'HEAD'   | 'param-year'             | '-1000000000'                     |                                               |
      | 'POST'   | 'param-year'             | '-1000000000'                     | 'param-year: is not a valid type'             |
      | 'PUT'    | 'param-year'             | '-1000000000'                     | 'param-year: is not a valid type'             |
      | 'PATCH'  | 'param-year'             | '-1000000000'                     | 'param-year: is not a valid type'             |
      | 'DELETE' | 'param-year'             | '-1000000000'                     | 'param-year: is not a valid type'             |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/optional-method-parameter?<param>=<value> endpoint (RequestParamUnconstrainedRestApi.class)
    this endpoint receives the request parameter in an optional method parameter, both having the same name
    and the service cannot map the sent value to the declared data type, responding with an error message.

    Given the endpoint '/api/v1/request-parameter/optional-method-parameter'
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | param                 | value                             | message                                    |
      | 'GET'    | 'paramInteger'        | '-2147483649'                     | 'paramInteger: is not a valid type'        |
      | 'HEAD'   | 'paramInteger'        | '-2147483649'                     |                                            |
      | 'POST'   | 'paramInteger'        | '-2147483649'                     | 'paramInteger: is not a valid type'        |
      | 'PUT'    | 'paramInteger'        | '-2147483649'                     | 'paramInteger: is not a valid type'        |
      | 'PATCH'  | 'paramInteger'        | '-2147483649'                     | 'paramInteger: is not a valid type'        |
      | 'DELETE' | 'paramInteger'        | '-2147483649'                     | 'paramInteger: is not a valid type'        |
      | 'GET'    | 'paramLong'           | '-9223372036854775809'            | 'paramLong: is not a valid type'           |
      | 'HEAD'   | 'paramLong'           | '-9223372036854775809'            |                                            |
      | 'POST'   | 'paramLong'           | '-9223372036854775809'            | 'paramLong: is not a valid type'           |
      | 'PUT'    | 'paramLong'           | '-9223372036854775809'            | 'paramLong: is not a valid type'           |
      | 'PATCH'  | 'paramLong'           | '-9223372036854775809'            | 'paramLong: is not a valid type'           |
      | 'DELETE' | 'paramLong'           | '-9223372036854775809'            | 'paramLong: is not a valid type'           |
      | 'GET'    | 'paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' | 'paramOffsetDateTime: is not a valid type' |
      | 'HEAD'   | 'paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' |                                            |
      | 'POST'   | 'paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' | 'paramOffsetDateTime: is not a valid type' |
      | 'PUT'    | 'paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' | 'paramOffsetDateTime: is not a valid type' |
      | 'PATCH'  | 'paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' | 'paramOffsetDateTime: is not a valid type' |
      | 'DELETE' | 'paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' | 'paramOffsetDateTime: is not a valid type' |
      | 'GET'    | 'paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' | 'paramLocalDateTime: is not a valid type'  |
      | 'HEAD'   | 'paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' |                                            |
      | 'POST'   | 'paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' | 'paramLocalDateTime: is not a valid type'  |
      | 'PUT'    | 'paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' | 'paramLocalDateTime: is not a valid type'  |
      | 'PATCH'  | 'paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' | 'paramLocalDateTime: is not a valid type'  |
      | 'DELETE' | 'paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' | 'paramLocalDateTime: is not a valid type'  |
      | 'GET'    | 'paramLocalDate'      | '-1000000000-12-31'               | 'paramLocalDate: is not a valid type'      |
      | 'HEAD'   | 'paramLocalDate'      | '-1000000000-12-31'               |                                            |
      | 'POST'   | 'paramLocalDate'      | '-1000000000-12-31'               | 'paramLocalDate: is not a valid type'      |
      | 'PUT'    | 'paramLocalDate'      | '-1000000000-12-31'               | 'paramLocalDate: is not a valid type'      |
      | 'PATCH'  | 'paramLocalDate'      | '-1000000000-12-31'               | 'paramLocalDate: is not a valid type'      |
      | 'DELETE' | 'paramLocalDate'      | '-1000000000-12-31'               | 'paramLocalDate: is not a valid type'      |
      | 'GET'    | 'paramYear'           | '-1000000000'                     | 'paramYear: is not a valid type'           |
      | 'HEAD'   | 'paramYear'           | '-1000000000'                     |                                            |
      | 'POST'   | 'paramYear'           | '-1000000000'                     | 'paramYear: is not a valid type'           |
      | 'PUT'    | 'paramYear'           | '-1000000000'                     | 'paramYear: is not a valid type'           |
      | 'PATCH'  | 'paramYear'           | '-1000000000'                     | 'paramYear: is not a valid type'           |
      | 'DELETE' | 'paramYear'           | '-1000000000'                     | 'paramYear: is not a valid type'           |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/named-optional-method-parameter?<param>=<value> endpoint (RequestParamUnconstrainedRestApi.class)
    this endpoint receives the request parameter in an optional method parameter, both having the diferent names, a custom request-parameter name is defined by the endpoint 
    and the service cannot map the sent value to the declared data type, responding with an error message.

    Given the endpoint '/api/v1/request-parameter/named-optional-method-parameter'
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | param                    | value                             | message                                       |
      | 'GET'    | 'param-integer'          | '-2147483649'                     | 'param-integer: is not a valid type'          |
      | 'HEAD'   | 'param-integer'          | '-2147483649'                     |                                               |
      | 'POST'   | 'param-integer'          | '-2147483649'                     | 'param-integer: is not a valid type'          |
      | 'PUT'    | 'param-integer'          | '-2147483649'                     | 'param-integer: is not a valid type'          |
      | 'PATCH'  | 'param-integer'          | '-2147483649'                     | 'param-integer: is not a valid type'          |
      | 'DELETE' | 'param-integer'          | '-2147483649'                     | 'param-integer: is not a valid type'          |
      | 'GET'    | 'param-long'             | '-9223372036854775809'            | 'param-long: is not a valid type'             |
      | 'HEAD'   | 'param-long'             | '-9223372036854775809'            |                                               |
      | 'POST'   | 'param-long'             | '-9223372036854775809'            | 'param-long: is not a valid type'             |
      | 'PUT'    | 'param-long'             | '-9223372036854775809'            | 'param-long: is not a valid type'             |
      | 'PATCH'  | 'param-long'             | '-9223372036854775809'            | 'param-long: is not a valid type'             |
      | 'DELETE' | 'param-long'             | '-9223372036854775809'            | 'param-long: is not a valid type'             |
      | 'GET'    | 'param-offset-date-time' | '-1000000000-12-31T23:59:59.999Z' | 'param-offset-date-time: is not a valid type' |
      | 'HEAD'   | 'param-offset-date-time' | '-1000000000-12-31T23:59:59.999Z' |                                               |
      | 'POST'   | 'param-offset-date-time' | '-1000000000-12-31T23:59:59.999Z' | 'param-offset-date-time: is not a valid type' |
      | 'PUT'    | 'param-offset-date-time' | '-1000000000-12-31T23:59:59.999Z' | 'param-offset-date-time: is not a valid type' |
      | 'PATCH'  | 'param-offset-date-time' | '-1000000000-12-31T23:59:59.999Z' | 'param-offset-date-time: is not a valid type' |
      | 'DELETE' | 'param-offset-date-time' | '-1000000000-12-31T23:59:59.999Z' | 'param-offset-date-time: is not a valid type' |
      | 'GET'    | 'param-offset-date-time' | '-1000000000-12-31T23:59:59.999Z' | 'param-offset-date-time: is not a valid type' |
      | 'HEAD'   | 'param-offset-date-time' | '-1000000000-12-31T23:59:59.999Z' |                                               |
      | 'POST'   | 'param-offset-date-time' | '-1000000000-12-31T23:59:59.999Z' | 'param-offset-date-time: is not a valid type' |
      | 'PUT'    | 'param-offset-date-time' | '-1000000000-12-31T23:59:59.999Z' | 'param-offset-date-time: is not a valid type' |
      | 'PATCH'  | 'param-offset-date-time' | '-1000000000-12-31T23:59:59.999Z' | 'param-offset-date-time: is not a valid type' |
      | 'DELETE' | 'param-offset-date-time' | '-1000000000-12-31T23:59:59.999Z' | 'param-offset-date-time: is not a valid type' |
      | 'GET'    | 'param-local-date'       | '-1000000000-12-31'               | 'param-local-date: is not a valid type'       |
      | 'HEAD'   | 'param-local-date'       | '-1000000000-12-31'               |                                               |
      | 'POST'   | 'param-local-date'       | '-1000000000-12-31'               | 'param-local-date: is not a valid type'       |
      | 'PUT'    | 'param-local-date'       | '-1000000000-12-31'               | 'param-local-date: is not a valid type'       |
      | 'PATCH'  | 'param-local-date'       | '-1000000000-12-31'               | 'param-local-date: is not a valid type'       |
      | 'DELETE' | 'param-local-date'       | '-1000000000-12-31'               | 'param-local-date: is not a valid type'       |
      | 'GET'    | 'param-year'             | '-1000000000'                     | 'param-year: is not a valid type'             |
      | 'HEAD'   | 'param-year'             | '-1000000000'                     |                                               |
      | 'POST'   | 'param-year'             | '-1000000000'                     | 'param-year: is not a valid type'             |
      | 'PUT'    | 'param-year'             | '-1000000000'                     | 'param-year: is not a valid type'             |
      | 'PATCH'  | 'param-year'             | '-1000000000'                     | 'param-year: is not a valid type'             |
      | 'DELETE' | 'param-year'             | '-1000000000'                     | 'param-year: is not a valid type'             |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/dto-attribute?<param>=<value> endpoint (RequestParamUnconstrainedRestApi.class)
    this endpoint receives the request parameter in a dto attribute, both having the same name
    and the service cannot map the sent value to the declared data type, responding with an error message.

    Given the endpoint '/api/v1/request-parameter/dto-attribute'
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | param                 | value                             | message                                    |
      | 'GET'    | 'paramInteger'        | '-2147483649'                     | 'paramInteger: is not a valid type'        |
      | 'HEAD'   | 'paramInteger'        | '-2147483649'                     |                                            |
      | 'POST'   | 'paramInteger'        | '-2147483649'                     | 'paramInteger: is not a valid type'        |
      | 'PUT'    | 'paramInteger'        | '-2147483649'                     | 'paramInteger: is not a valid type'        |
      | 'PATCH'  | 'paramInteger'        | '-2147483649'                     | 'paramInteger: is not a valid type'        |
      | 'DELETE' | 'paramInteger'        | '-2147483649'                     | 'paramInteger: is not a valid type'        |
      | 'GET'    | 'paramLong'           | '-9223372036854775809'            | 'paramLong: is not a valid type'           |
      | 'HEAD'   | 'paramLong'           | '-9223372036854775809'            |                                            |
      | 'POST'   | 'paramLong'           | '-9223372036854775809'            | 'paramLong: is not a valid type'           |
      | 'PUT'    | 'paramLong'           | '-9223372036854775809'            | 'paramLong: is not a valid type'           |
      | 'PATCH'  | 'paramLong'           | '-9223372036854775809'            | 'paramLong: is not a valid type'           |
      | 'DELETE' | 'paramLong'           | '-9223372036854775809'            | 'paramLong: is not a valid type'           |
      | 'GET'    | 'paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' | 'paramOffsetDateTime: is not a valid type' |
      | 'HEAD'   | 'paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' |                                            |
      | 'POST'   | 'paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' | 'paramOffsetDateTime: is not a valid type' |
      | 'PUT'    | 'paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' | 'paramOffsetDateTime: is not a valid type' |
      | 'PATCH'  | 'paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' | 'paramOffsetDateTime: is not a valid type' |
      | 'DELETE' | 'paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' | 'paramOffsetDateTime: is not a valid type' |
      | 'GET'    | 'paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' | 'paramLocalDateTime: is not a valid type'  |
      | 'HEAD'   | 'paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' |                                            |
      | 'POST'   | 'paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' | 'paramLocalDateTime: is not a valid type'  |
      | 'PUT'    | 'paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' | 'paramLocalDateTime: is not a valid type'  |
      | 'PATCH'  | 'paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' | 'paramLocalDateTime: is not a valid type'  |
      | 'DELETE' | 'paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' | 'paramLocalDateTime: is not a valid type'  |
      | 'GET'    | 'paramLocalDate'      | '-1000000000-12-31'               | 'paramLocalDate: is not a valid type'      |
      | 'HEAD'   | 'paramLocalDate'      | '-1000000000-12-31'               |                                            |
      | 'POST'   | 'paramLocalDate'      | '-1000000000-12-31'               | 'paramLocalDate: is not a valid type'      |
      | 'PUT'    | 'paramLocalDate'      | '-1000000000-12-31'               | 'paramLocalDate: is not a valid type'      |
      | 'PATCH'  | 'paramLocalDate'      | '-1000000000-12-31'               | 'paramLocalDate: is not a valid type'      |
      | 'DELETE' | 'paramLocalDate'      | '-1000000000-12-31'               | 'paramLocalDate: is not a valid type'      |
      | 'GET'    | 'paramYear'           | '-1000000000'                     | 'paramYear: is not a valid type'           |
      | 'HEAD'   | 'paramYear'           | '-1000000000'                     |                                            |
      | 'POST'   | 'paramYear'           | '-1000000000'                     | 'paramYear: is not a valid type'           |
      | 'PUT'    | 'paramYear'           | '-1000000000'                     | 'paramYear: is not a valid type'           |
      | 'PATCH'  | 'paramYear'           | '-1000000000'                     | 'paramYear: is not a valid type'           |
      | 'DELETE' | 'paramYear'           | '-1000000000'                     | 'paramYear: is not a valid type'           |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/optional-dto-attribute?<param>=<value> endpoint (RequestParamUnconstrainedRestApi.class)
    this endpoint receives the request parameter in an optional dto attribute, both having the same name
    and the service cannot map the sent value to the declared data type, responding with an error message.

    Given the endpoint '/api/v1/request-parameter/optional-dto-attribute'
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | param                 | value                             | message                                    |
      | 'GET'    | 'paramInteger'        | '-2147483649'                     | 'paramInteger: is not a valid type'        |
      | 'HEAD'   | 'paramInteger'        | '-2147483649'                     |                                            |
      | 'POST'   | 'paramInteger'        | '-2147483649'                     | 'paramInteger: is not a valid type'        |
      | 'PUT'    | 'paramInteger'        | '-2147483649'                     | 'paramInteger: is not a valid type'        |
      | 'PATCH'  | 'paramInteger'        | '-2147483649'                     | 'paramInteger: is not a valid type'        |
      | 'DELETE' | 'paramInteger'        | '-2147483649'                     | 'paramInteger: is not a valid type'        |
      | 'GET'    | 'paramLong'           | '-9223372036854775809'            | 'paramLong: is not a valid type'           |
      | 'HEAD'   | 'paramLong'           | '-9223372036854775809'            |                                            |
      | 'POST'   | 'paramLong'           | '-9223372036854775809'            | 'paramLong: is not a valid type'           |
      | 'PUT'    | 'paramLong'           | '-9223372036854775809'            | 'paramLong: is not a valid type'           |
      | 'PATCH'  | 'paramLong'           | '-9223372036854775809'            | 'paramLong: is not a valid type'           |
      | 'DELETE' | 'paramLong'           | '-9223372036854775809'            | 'paramLong: is not a valid type'           |
      | 'GET'    | 'paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' | 'paramOffsetDateTime: is not a valid type' |
      | 'HEAD'   | 'paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' |                                            |
      | 'POST'   | 'paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' | 'paramOffsetDateTime: is not a valid type' |
      | 'PUT'    | 'paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' | 'paramOffsetDateTime: is not a valid type' |
      | 'PATCH'  | 'paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' | 'paramOffsetDateTime: is not a valid type' |
      | 'DELETE' | 'paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' | 'paramOffsetDateTime: is not a valid type' |
      | 'GET'    | 'paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' | 'paramLocalDateTime: is not a valid type'  |
      | 'HEAD'   | 'paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' |                                            |
      | 'POST'   | 'paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' | 'paramLocalDateTime: is not a valid type'  |
      | 'PUT'    | 'paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' | 'paramLocalDateTime: is not a valid type'  |
      | 'PATCH'  | 'paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' | 'paramLocalDateTime: is not a valid type'  |
      | 'DELETE' | 'paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' | 'paramLocalDateTime: is not a valid type'  |
      | 'GET'    | 'paramLocalDate'      | '-1000000000-12-31'               | 'paramLocalDate: is not a valid type'      |
      | 'HEAD'   | 'paramLocalDate'      | '-1000000000-12-31'               |                                            |
      | 'POST'   | 'paramLocalDate'      | '-1000000000-12-31'               | 'paramLocalDate: is not a valid type'      |
      | 'PUT'    | 'paramLocalDate'      | '-1000000000-12-31'               | 'paramLocalDate: is not a valid type'      |
      | 'PATCH'  | 'paramLocalDate'      | '-1000000000-12-31'               | 'paramLocalDate: is not a valid type'      |
      | 'DELETE' | 'paramLocalDate'      | '-1000000000-12-31'               | 'paramLocalDate: is not a valid type'      |
      | 'GET'    | 'paramYear'           | '-1000000000'                     | 'paramYear: is not a valid type'           |
      | 'HEAD'   | 'paramYear'           | '-1000000000'                     |                                            |
      | 'POST'   | 'paramYear'           | '-1000000000'                     | 'paramYear: is not a valid type'           |
      | 'PUT'    | 'paramYear'           | '-1000000000'                     | 'paramYear: is not a valid type'           |
      | 'PATCH'  | 'paramYear'           | '-1000000000'                     | 'paramYear: is not a valid type'           |
      | 'DELETE' | 'paramYear'           | '-1000000000'                     | 'paramYear: is not a valid type'           |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/nested-dto-attribute?<param>=<value> endpoint (RequestParamUnconstrainedRestApi.class)
    this endpoint receives the request parameter in a nested dto attribute, both having the same name
    and the service cannot map the sent value to the declared data type, responding with an error message.

    Given the endpoint '/api/v1/request-parameter/nested-dto-attribute'
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | param                        | value                             | message                                           |
      | 'GET'    | 'nested.paramInteger'        | '-2147483649'                     | 'nested.paramInteger: is not a valid type'        |
      | 'HEAD'   | 'nested.paramInteger'        | '-2147483649'                     |                                                   |
      | 'POST'   | 'nested.paramInteger'        | '-2147483649'                     | 'nested.paramInteger: is not a valid type'        |
      | 'PUT'    | 'nested.paramInteger'        | '-2147483649'                     | 'nested.paramInteger: is not a valid type'        |
      | 'PATCH'  | 'nested.paramInteger'        | '-2147483649'                     | 'nested.paramInteger: is not a valid type'        |
      | 'DELETE' | 'nested.paramInteger'        | '-2147483649'                     | 'nested.paramInteger: is not a valid type'        |
      | 'GET'    | 'nested.paramLong'           | '-9223372036854775809'            | 'nested.paramLong: is not a valid type'           |
      | 'HEAD'   | 'nested.paramLong'           | '-9223372036854775809'            |                                                   |
      | 'POST'   | 'nested.paramLong'           | '-9223372036854775809'            | 'nested.paramLong: is not a valid type'           |
      | 'PUT'    | 'nested.paramLong'           | '-9223372036854775809'            | 'nested.paramLong: is not a valid type'           |
      | 'PATCH'  | 'nested.paramLong'           | '-9223372036854775809'            | 'nested.paramLong: is not a valid type'           |
      | 'DELETE' | 'nested.paramLong'           | '-9223372036854775809'            | 'nested.paramLong: is not a valid type'           |
      | 'GET'    | 'nested.paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' | 'nested.paramOffsetDateTime: is not a valid type' |
      | 'HEAD'   | 'nested.paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' |                                                   |
      | 'POST'   | 'nested.paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' | 'nested.paramOffsetDateTime: is not a valid type' |
      | 'PUT'    | 'nested.paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' | 'nested.paramOffsetDateTime: is not a valid type' |
      | 'PATCH'  | 'nested.paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' | 'nested.paramOffsetDateTime: is not a valid type' |
      | 'DELETE' | 'nested.paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' | 'nested.paramOffsetDateTime: is not a valid type' |
      | 'GET'    | 'nested.paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' | 'nested.paramLocalDateTime: is not a valid type'  |
      | 'HEAD'   | 'nested.paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' |                                                   |
      | 'POST'   | 'nested.paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' | 'nested.paramLocalDateTime: is not a valid type'  |
      | 'PUT'    | 'nested.paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' | 'nested.paramLocalDateTime: is not a valid type'  |
      | 'PATCH'  | 'nested.paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' | 'nested.paramLocalDateTime: is not a valid type'  |
      | 'DELETE' | 'nested.paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' | 'nested.paramLocalDateTime: is not a valid type'  |
      | 'GET'    | 'nested.paramLocalDate'      | '-1000000000-12-31'               | 'nested.paramLocalDate: is not a valid type'      |
      | 'HEAD'   | 'nested.paramLocalDate'      | '-1000000000-12-31'               |                                                   |
      | 'POST'   | 'nested.paramLocalDate'      | '-1000000000-12-31'               | 'nested.paramLocalDate: is not a valid type'      |
      | 'PUT'    | 'nested.paramLocalDate'      | '-1000000000-12-31'               | 'nested.paramLocalDate: is not a valid type'      |
      | 'PATCH'  | 'nested.paramLocalDate'      | '-1000000000-12-31'               | 'nested.paramLocalDate: is not a valid type'      |
      | 'DELETE' | 'nested.paramLocalDate'      | '-1000000000-12-31'               | 'nested.paramLocalDate: is not a valid type'      |
      | 'GET'    | 'nested.paramYear'           | '-1000000000'                     | 'nested.paramYear: is not a valid type'           |
      | 'HEAD'   | 'nested.paramYear'           | '-1000000000'                     |                                                   |
      | 'POST'   | 'nested.paramYear'           | '-1000000000'                     | 'nested.paramYear: is not a valid type'           |
      | 'PUT'    | 'nested.paramYear'           | '-1000000000'                     | 'nested.paramYear: is not a valid type'           |
      | 'PATCH'  | 'nested.paramYear'           | '-1000000000'                     | 'nested.paramYear: is not a valid type'           |
      | 'DELETE' | 'nested.paramYear'           | '-1000000000'                     | 'nested.paramYear: is not a valid type'           |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/nested-optional-dto-attribute?<param>=<value> endpoint (RequestParamUnconstrainedRestApi.class)
    this endpoint receives the request parameter in a nested optional dto attribute, both having the same name
    and the service cannot map the sent value to the declared data type, responding with an error message.

    Given the endpoint '/api/v1/request-parameter/nested-optional-dto-attribute'
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | param                        | value                             | message                                           |
      | 'GET'    | 'nested.paramInteger'        | '-2147483649'                     | 'nested.paramInteger: is not a valid type'        |
      | 'HEAD'   | 'nested.paramInteger'        | '-2147483649'                     |                                                   |
      | 'POST'   | 'nested.paramInteger'        | '-2147483649'                     | 'nested.paramInteger: is not a valid type'        |
      | 'PUT'    | 'nested.paramInteger'        | '-2147483649'                     | 'nested.paramInteger: is not a valid type'        |
      | 'PATCH'  | 'nested.paramInteger'        | '-2147483649'                     | 'nested.paramInteger: is not a valid type'        |
      | 'DELETE' | 'nested.paramInteger'        | '-2147483649'                     | 'nested.paramInteger: is not a valid type'        |
      | 'GET'    | 'nested.paramLong'           | '-9223372036854775809'            | 'nested.paramLong: is not a valid type'           |
      | 'HEAD'   | 'nested.paramLong'           | '-9223372036854775809'            |                                                   |
      | 'POST'   | 'nested.paramLong'           | '-9223372036854775809'            | 'nested.paramLong: is not a valid type'           |
      | 'PUT'    | 'nested.paramLong'           | '-9223372036854775809'            | 'nested.paramLong: is not a valid type'           |
      | 'PATCH'  | 'nested.paramLong'           | '-9223372036854775809'            | 'nested.paramLong: is not a valid type'           |
      | 'DELETE' | 'nested.paramLong'           | '-9223372036854775809'            | 'nested.paramLong: is not a valid type'           |
      | 'GET'    | 'nested.paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' | 'nested.paramOffsetDateTime: is not a valid type' |
      | 'HEAD'   | 'nested.paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' |                                                   |
      | 'POST'   | 'nested.paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' | 'nested.paramOffsetDateTime: is not a valid type' |
      | 'PUT'    | 'nested.paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' | 'nested.paramOffsetDateTime: is not a valid type' |
      | 'PATCH'  | 'nested.paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' | 'nested.paramOffsetDateTime: is not a valid type' |
      | 'DELETE' | 'nested.paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' | 'nested.paramOffsetDateTime: is not a valid type' |
      | 'GET'    | 'nested.paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' | 'nested.paramLocalDateTime: is not a valid type'  |
      | 'HEAD'   | 'nested.paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' |                                                   |
      | 'POST'   | 'nested.paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' | 'nested.paramLocalDateTime: is not a valid type'  |
      | 'PUT'    | 'nested.paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' | 'nested.paramLocalDateTime: is not a valid type'  |
      | 'PATCH'  | 'nested.paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' | 'nested.paramLocalDateTime: is not a valid type'  |
      | 'DELETE' | 'nested.paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' | 'nested.paramLocalDateTime: is not a valid type'  |
      | 'GET'    | 'nested.paramLocalDate'      | '-1000000000-12-31'               | 'nested.paramLocalDate: is not a valid type'      |
      | 'HEAD'   | 'nested.paramLocalDate'      | '-1000000000-12-31'               |                                                   |
      | 'POST'   | 'nested.paramLocalDate'      | '-1000000000-12-31'               | 'nested.paramLocalDate: is not a valid type'      |
      | 'PUT'    | 'nested.paramLocalDate'      | '-1000000000-12-31'               | 'nested.paramLocalDate: is not a valid type'      |
      | 'PATCH'  | 'nested.paramLocalDate'      | '-1000000000-12-31'               | 'nested.paramLocalDate: is not a valid type'      |
      | 'DELETE' | 'nested.paramLocalDate'      | '-1000000000-12-31'               | 'nested.paramLocalDate: is not a valid type'      |
      | 'GET'    | 'nested.paramYear'           | '-1000000000'                     | 'nested.paramYear: is not a valid type'           |
      | 'HEAD'   | 'nested.paramYear'           | '-1000000000'                     |                                                   |
      | 'POST'   | 'nested.paramYear'           | '-1000000000'                     | 'nested.paramYear: is not a valid type'           |
      | 'PUT'    | 'nested.paramYear'           | '-1000000000'                     | 'nested.paramYear: is not a valid type'           |
      | 'PATCH'  | 'nested.paramYear'           | '-1000000000'                     | 'nested.paramYear: is not a valid type'           |
      | 'DELETE' | 'nested.paramYear'           | '-1000000000'                     | 'nested.paramYear: is not a valid type'           |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/nested-dto-attribute?<param>=<value> endpoint (RequestParamUnconstrainedRestApi.class)
    this endpoint receives the request parameter in a nested dto attribute from an array, both having the same name
    and the service cannot map the sent value to the declared data type, responding with an error message.

    Given the endpoint '/api/v1/request-parameter/nested-dto-attribute'
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | param                          | value                             | message                                             |
      | 'GET'    | 'array[0].paramInteger'        | '-2147483649'                     | 'array[0].paramInteger: is not a valid type'        |
      | 'HEAD'   | 'array[0].paramInteger'        | '-2147483649'                     |                                                     |
      | 'POST'   | 'array[0].paramInteger'        | '-2147483649'                     | 'array[0].paramInteger: is not a valid type'        |
      | 'PUT'    | 'array[0].paramInteger'        | '-2147483649'                     | 'array[0].paramInteger: is not a valid type'        |
      | 'PATCH'  | 'array[0].paramInteger'        | '-2147483649'                     | 'array[0].paramInteger: is not a valid type'        |
      | 'DELETE' | 'array[0].paramInteger'        | '-2147483649'                     | 'array[0].paramInteger: is not a valid type'        |
      | 'GET'    | 'array[0].paramLong'           | '-9223372036854775809'            | 'array[0].paramLong: is not a valid type'           |
      | 'HEAD'   | 'array[0].paramLong'           | '-9223372036854775809'            |                                                     |
      | 'POST'   | 'array[0].paramLong'           | '-9223372036854775809'            | 'array[0].paramLong: is not a valid type'           |
      | 'PUT'    | 'array[0].paramLong'           | '-9223372036854775809'            | 'array[0].paramLong: is not a valid type'           |
      | 'PATCH'  | 'array[0].paramLong'           | '-9223372036854775809'            | 'array[0].paramLong: is not a valid type'           |
      | 'DELETE' | 'array[0].paramLong'           | '-9223372036854775809'            | 'array[0].paramLong: is not a valid type'           |
      | 'GET'    | 'array[0].paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' | 'array[0].paramOffsetDateTime: is not a valid type' |
      | 'HEAD'   | 'array[0].paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' |                                                     |
      | 'POST'   | 'array[0].paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' | 'array[0].paramOffsetDateTime: is not a valid type' |
      | 'PUT'    | 'array[0].paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' | 'array[0].paramOffsetDateTime: is not a valid type' |
      | 'PATCH'  | 'array[0].paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' | 'array[0].paramOffsetDateTime: is not a valid type' |
      | 'DELETE' | 'array[0].paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' | 'array[0].paramOffsetDateTime: is not a valid type' |
      | 'GET'    | 'array[0].paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' | 'array[0].paramLocalDateTime: is not a valid type'  |
      | 'HEAD'   | 'array[0].paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' |                                                     |
      | 'POST'   | 'array[0].paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' | 'array[0].paramLocalDateTime: is not a valid type'  |
      | 'PUT'    | 'array[0].paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' | 'array[0].paramLocalDateTime: is not a valid type'  |
      | 'PATCH'  | 'array[0].paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' | 'array[0].paramLocalDateTime: is not a valid type'  |
      | 'DELETE' | 'array[0].paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' | 'array[0].paramLocalDateTime: is not a valid type'  |
      | 'GET'    | 'array[0].paramLocalDate'      | '-1000000000-12-31'               | 'array[0].paramLocalDate: is not a valid type'      |
      | 'HEAD'   | 'array[0].paramLocalDate'      | '-1000000000-12-31'               |                                                     |
      | 'POST'   | 'array[0].paramLocalDate'      | '-1000000000-12-31'               | 'array[0].paramLocalDate: is not a valid type'      |
      | 'PUT'    | 'array[0].paramLocalDate'      | '-1000000000-12-31'               | 'array[0].paramLocalDate: is not a valid type'      |
      | 'PATCH'  | 'array[0].paramLocalDate'      | '-1000000000-12-31'               | 'array[0].paramLocalDate: is not a valid type'      |
      | 'DELETE' | 'array[0].paramLocalDate'      | '-1000000000-12-31'               | 'array[0].paramLocalDate: is not a valid type'      |
      | 'GET'    | 'array[0].paramYear'           | '-1000000000'                     | 'array[0].paramYear: is not a valid type'           |
      | 'HEAD'   | 'array[0].paramYear'           | '-1000000000'                     |                                                     |
      | 'POST'   | 'array[0].paramYear'           | '-1000000000'                     | 'array[0].paramYear: is not a valid type'           |
      | 'PUT'    | 'array[0].paramYear'           | '-1000000000'                     | 'array[0].paramYear: is not a valid type'           |
      | 'PATCH'  | 'array[0].paramYear'           | '-1000000000'                     | 'array[0].paramYear: is not a valid type'           |
      | 'DELETE' | 'array[0].paramYear'           | '-1000000000'                     | 'array[0].paramYear: is not a valid type'           |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/nested-optional-dto-attribute?<param>=<value> endpoint (RequestParamUnconstrainedRestApi.class)
    this endpoint receives the request parameter in a nested optional dto attribute from an array, both having the same name
    and the service cannot map the sent value to the declared data type, responding with an error message.

    Given the endpoint '/api/v1/request-parameter/nested-optional-dto-attribute'
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | param                          | value                             | message                                             |
      | 'GET'    | 'array[0].paramInteger'        | '-2147483649'                     | 'array[0].paramInteger: is not a valid type'        |
      | 'HEAD'   | 'array[0].paramInteger'        | '-2147483649'                     |                                                     |
      | 'POST'   | 'array[0].paramInteger'        | '-2147483649'                     | 'array[0].paramInteger: is not a valid type'        |
      | 'PUT'    | 'array[0].paramInteger'        | '-2147483649'                     | 'array[0].paramInteger: is not a valid type'        |
      | 'PATCH'  | 'array[0].paramInteger'        | '-2147483649'                     | 'array[0].paramInteger: is not a valid type'        |
      | 'DELETE' | 'array[0].paramInteger'        | '-2147483649'                     | 'array[0].paramInteger: is not a valid type'        |
      | 'GET'    | 'array[0].paramLong'           | '-9223372036854775809'            | 'array[0].paramLong: is not a valid type'           |
      | 'HEAD'   | 'array[0].paramLong'           | '-9223372036854775809'            |                                                     |
      | 'POST'   | 'array[0].paramLong'           | '-9223372036854775809'            | 'array[0].paramLong: is not a valid type'           |
      | 'PUT'    | 'array[0].paramLong'           | '-9223372036854775809'            | 'array[0].paramLong: is not a valid type'           |
      | 'PATCH'  | 'array[0].paramLong'           | '-9223372036854775809'            | 'array[0].paramLong: is not a valid type'           |
      | 'DELETE' | 'array[0].paramLong'           | '-9223372036854775809'            | 'array[0].paramLong: is not a valid type'           |
      | 'GET'    | 'array[0].paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' | 'array[0].paramOffsetDateTime: is not a valid type' |
      | 'HEAD'   | 'array[0].paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' |                                                     |
      | 'POST'   | 'array[0].paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' | 'array[0].paramOffsetDateTime: is not a valid type' |
      | 'PUT'    | 'array[0].paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' | 'array[0].paramOffsetDateTime: is not a valid type' |
      | 'PATCH'  | 'array[0].paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' | 'array[0].paramOffsetDateTime: is not a valid type' |
      | 'DELETE' | 'array[0].paramOffsetDateTime' | '-1000000000-12-31T23:59:59.999Z' | 'array[0].paramOffsetDateTime: is not a valid type' |
      | 'GET'    | 'array[0].paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' | 'array[0].paramLocalDateTime: is not a valid type'  |
      | 'HEAD'   | 'array[0].paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' |                                                     |
      | 'POST'   | 'array[0].paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' | 'array[0].paramLocalDateTime: is not a valid type'  |
      | 'PUT'    | 'array[0].paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' | 'array[0].paramLocalDateTime: is not a valid type'  |
      | 'PATCH'  | 'array[0].paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' | 'array[0].paramLocalDateTime: is not a valid type'  |
      | 'DELETE' | 'array[0].paramLocalDateTime'  | '-1000000000-12-31T23:59:59.999Z' | 'array[0].paramLocalDateTime: is not a valid type'  |
      | 'GET'    | 'array[0].paramLocalDate'      | '-1000000000-12-31'               | 'array[0].paramLocalDate: is not a valid type'      |
      | 'HEAD'   | 'array[0].paramLocalDate'      | '-1000000000-12-31'               |                                                     |
      | 'POST'   | 'array[0].paramLocalDate'      | '-1000000000-12-31'               | 'array[0].paramLocalDate: is not a valid type'      |
      | 'PUT'    | 'array[0].paramLocalDate'      | '-1000000000-12-31'               | 'array[0].paramLocalDate: is not a valid type'      |
      | 'PATCH'  | 'array[0].paramLocalDate'      | '-1000000000-12-31'               | 'array[0].paramLocalDate: is not a valid type'      |
      | 'DELETE' | 'array[0].paramLocalDate'      | '-1000000000-12-31'               | 'array[0].paramLocalDate: is not a valid type'      |
      | 'GET'    | 'array[0].paramYear'           | '-1000000000'                     | 'array[0].paramYear: is not a valid type'           |
      | 'HEAD'   | 'array[0].paramYear'           | '-1000000000'                     |                                                     |
      | 'POST'   | 'array[0].paramYear'           | '-1000000000'                     | 'array[0].paramYear: is not a valid type'           |
      | 'PUT'    | 'array[0].paramYear'           | '-1000000000'                     | 'array[0].paramYear: is not a valid type'           |
      | 'PATCH'  | 'array[0].paramYear'           | '-1000000000'                     | 'array[0].paramYear: is not a valid type'           |
      | 'DELETE' | 'array[0].paramYear'           | '-1000000000'                     | 'array[0].paramYear: is not a valid type'           |
