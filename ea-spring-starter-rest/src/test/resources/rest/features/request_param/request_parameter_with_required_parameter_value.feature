Feature: Request parameter with required parameter value
  
  The REST service receives empty data via the request parameter and interprets it as null. If method parameter is optional the service responds with a success message else responds with an error message.
  
   Rule: The conversion between data types is given by the following table
  
  | parameter type | java type      |
  |----------------|----------------|
  | string         | String         |
  | number         | Integer        |
  | number         | Long           |
  | number         | BigInteger     |
  | number         | Float          |
  | number         | Double         |
  | number         | BigDecimal     |
  | boolean        | Boolean        |
  | ISO-8601       | OffsetDateTime |
  | ISO-8601       | LocalDateTime  |
  | ISO-8601       | LocalDate      |
  | ISO-8601       | LocalTime      |
  | ISO-8601       | Year           |
  | string         | ENUM           |

  Background: 
    Given the default timezone 'America/Puerto_Rico'

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/required/method-parameter<path>?<param>=<value> endpoint (RequestParamRequiredRestApi.class)
    this endpoint receives the request parameter in a required method parameter, both having the same name
    and the service maps the sent empty value to null, but service responding with an error message since data is requited.

    Given the endpoint '/api/v1/request-parameter/required/method-parameter'<path>
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | param                 | value | message                                 |
      | 'GET'    | '/paramString'         | 'paramString'         | ''    | 'paramString: must not be null'         |
      | 'HEAD'   | '/paramString'         | 'paramString'         | ''    |                                         |
      | 'POST'   | '/paramString'         | 'paramString'         | ''    | 'paramString: must not be null'         |
      | 'PUT'    | '/paramString'         | 'paramString'         | ''    | 'paramString: must not be null'         |
      | 'PATCH'  | '/paramString'         | 'paramString'         | ''    | 'paramString: must not be null'         |
      | 'DELETE' | '/paramString'         | 'paramString'         | ''    | 'paramString: must not be null'         |
      | 'GET'    | '/paramInteger'        | 'paramInteger'        | ''    | 'paramInteger: must not be null'        |
      | 'HEAD'   | '/paramInteger'        | 'paramInteger'        | ''    |                                         |
      | 'POST'   | '/paramInteger'        | 'paramInteger'        | ''    | 'paramInteger: must not be null'        |
      | 'PUT'    | '/paramInteger'        | 'paramInteger'        | ''    | 'paramInteger: must not be null'        |
      | 'PATCH'  | '/paramInteger'        | 'paramInteger'        | ''    | 'paramInteger: must not be null'        |
      | 'DELETE' | '/paramInteger'        | 'paramInteger'        | ''    | 'paramInteger: must not be null'        |
      | 'GET'    | '/paramLong'           | 'paramLong'           | ''    | 'paramLong: must not be null'           |
      | 'HEAD'   | '/paramLong'           | 'paramLong'           | ''    |                                         |
      | 'POST'   | '/paramLong'           | 'paramLong'           | ''    | 'paramLong: must not be null'           |
      | 'PUT'    | '/paramLong'           | 'paramLong'           | ''    | 'paramLong: must not be null'           |
      | 'PATCH'  | '/paramLong'           | 'paramLong'           | ''    | 'paramLong: must not be null'           |
      | 'DELETE' | '/paramLong'           | 'paramLong'           | ''    | 'paramLong: must not be null'           |
      | 'GET'    | '/paramBigInteger'     | 'paramBigInteger'     | ''    | 'paramBigInteger: must not be null'     |
      | 'HEAD'   | '/paramBigInteger'     | 'paramBigInteger'     | ''    |                                         |
      | 'POST'   | '/paramBigInteger'     | 'paramBigInteger'     | ''    | 'paramBigInteger: must not be null'     |
      | 'PUT'    | '/paramBigInteger'     | 'paramBigInteger'     | ''    | 'paramBigInteger: must not be null'     |
      | 'PATCH'  | '/paramBigInteger'     | 'paramBigInteger'     | ''    | 'paramBigInteger: must not be null'     |
      | 'DELETE' | '/paramBigInteger'     | 'paramBigInteger'     | ''    | 'paramBigInteger: must not be null'     |
      | 'GET'    | '/paramFloat'          | 'paramFloat'          | ''    | 'paramFloat: must not be null'          |
      | 'HEAD'   | '/paramFloat'          | 'paramFloat'          | ''    |                                         |
      | 'POST'   | '/paramFloat'          | 'paramFloat'          | ''    | 'paramFloat: must not be null'          |
      | 'PUT'    | '/paramFloat'          | 'paramFloat'          | ''    | 'paramFloat: must not be null'          |
      | 'PATCH'  | '/paramFloat'          | 'paramFloat'          | ''    | 'paramFloat: must not be null'          |
      | 'DELETE' | '/paramFloat'          | 'paramFloat'          | ''    | 'paramFloat: must not be null'          |
      | 'GET'    | '/paramDouble'         | 'paramDouble'         | ''    | 'paramDouble: must not be null'         |
      | 'HEAD'   | '/paramDouble'         | 'paramDouble'         | ''    |                                         |
      | 'POST'   | '/paramDouble'         | 'paramDouble'         | ''    | 'paramDouble: must not be null'         |
      | 'PUT'    | '/paramDouble'         | 'paramDouble'         | ''    | 'paramDouble: must not be null'         |
      | 'PATCH'  | '/paramDouble'         | 'paramDouble'         | ''    | 'paramDouble: must not be null'         |
      | 'DELETE' | '/paramDouble'         | 'paramDouble'         | ''    | 'paramDouble: must not be null'         |
      | 'GET'    | '/paramBigDecimal'     | 'paramBigDecimal'     | ''    | 'paramBigDecimal: must not be null'     |
      | 'HEAD'   | '/paramBigDecimal'     | 'paramBigDecimal'     | ''    |                                         |
      | 'POST'   | '/paramBigDecimal'     | 'paramBigDecimal'     | ''    | 'paramBigDecimal: must not be null'     |
      | 'PUT'    | '/paramBigDecimal'     | 'paramBigDecimal'     | ''    | 'paramBigDecimal: must not be null'     |
      | 'PATCH'  | '/paramBigDecimal'     | 'paramBigDecimal'     | ''    | 'paramBigDecimal: must not be null'     |
      | 'DELETE' | '/paramBigDecimal'     | 'paramBigDecimal'     | ''    | 'paramBigDecimal: must not be null'     |
      | 'GET'    | '/paramBoolean'        | 'paramBoolean'        | ''    | 'paramBoolean: must not be null'        |
      | 'HEAD'   | '/paramBoolean'        | 'paramBoolean'        | ''    |                                         |
      | 'POST'   | '/paramBoolean'        | 'paramBoolean'        | ''    | 'paramBoolean: must not be null'        |
      | 'PUT'    | '/paramBoolean'        | 'paramBoolean'        | ''    | 'paramBoolean: must not be null'        |
      | 'PATCH'  | '/paramBoolean'        | 'paramBoolean'        | ''    | 'paramBoolean: must not be null'        |
      | 'DELETE' | '/paramBoolean'        | 'paramBoolean'        | ''    | 'paramBoolean: must not be null'        |
      | 'GET'    | '/paramOffsetDateTime' | 'paramOffsetDateTime' | ''    | 'paramOffsetDateTime: must not be null' |
      | 'HEAD'   | '/paramOffsetDateTime' | 'paramOffsetDateTime' | ''    |                                         |
      | 'POST'   | '/paramOffsetDateTime' | 'paramOffsetDateTime' | ''    | 'paramOffsetDateTime: must not be null' |
      | 'PUT'    | '/paramOffsetDateTime' | 'paramOffsetDateTime' | ''    | 'paramOffsetDateTime: must not be null' |
      | 'PATCH'  | '/paramOffsetDateTime' | 'paramOffsetDateTime' | ''    | 'paramOffsetDateTime: must not be null' |
      | 'DELETE' | '/paramOffsetDateTime' | 'paramOffsetDateTime' | ''    | 'paramOffsetDateTime: must not be null' |
      | 'GET'    | '/paramLocalDateTime'  | 'paramLocalDateTime'  | ''    | 'paramLocalDateTime: must not be null'  |
      | 'HEAD'   | '/paramLocalDateTime'  | 'paramLocalDateTime'  | ''    |                                         |
      | 'POST'   | '/paramLocalDateTime'  | 'paramLocalDateTime'  | ''    | 'paramLocalDateTime: must not be null'  |
      | 'PUT'    | '/paramLocalDateTime'  | 'paramLocalDateTime'  | ''    | 'paramLocalDateTime: must not be null'  |
      | 'PATCH'  | '/paramLocalDateTime'  | 'paramLocalDateTime'  | ''    | 'paramLocalDateTime: must not be null'  |
      | 'DELETE' | '/paramLocalDateTime'  | 'paramLocalDateTime'  | ''    | 'paramLocalDateTime: must not be null'  |
      | 'GET'    | '/paramLocalDate'      | 'paramLocalDate'      | ''    | 'paramLocalDate: must not be null'      |
      | 'HEAD'   | '/paramLocalDate'      | 'paramLocalDate'      | ''    |                                         |
      | 'POST'   | '/paramLocalDate'      | 'paramLocalDate'      | ''    | 'paramLocalDate: must not be null'      |
      | 'PUT'    | '/paramLocalDate'      | 'paramLocalDate'      | ''    | 'paramLocalDate: must not be null'      |
      | 'PATCH'  | '/paramLocalDate'      | 'paramLocalDate'      | ''    | 'paramLocalDate: must not be null'      |
      | 'DELETE' | '/paramLocalDate'      | 'paramLocalDate'      | ''    | 'paramLocalDate: must not be null'      |
      | 'GET'    | '/paramLocalTime'      | 'paramLocalTime'      | ''    | 'paramLocalTime: must not be null'      |
      | 'HEAD'   | '/paramLocalTime'      | 'paramLocalTime'      | ''    |                                         |
      | 'POST'   | '/paramLocalTime'      | 'paramLocalTime'      | ''    | 'paramLocalTime: must not be null'      |
      | 'PUT'    | '/paramLocalTime'      | 'paramLocalTime'      | ''    | 'paramLocalTime: must not be null'      |
      | 'PATCH'  | '/paramLocalTime'      | 'paramLocalTime'      | ''    | 'paramLocalTime: must not be null'      |
      | 'DELETE' | '/paramLocalTime'      | 'paramLocalTime'      | ''    | 'paramLocalTime: must not be null'      |
      | 'GET'    | '/paramYear'           | 'paramYear'           | ''    | 'paramYear: must not be null'           |
      | 'HEAD'   | '/paramYear'           | 'paramYear'           | ''    |                                         |
      | 'POST'   | '/paramYear'           | 'paramYear'           | ''    | 'paramYear: must not be null'           |
      | 'PUT'    | '/paramYear'           | 'paramYear'           | ''    | 'paramYear: must not be null'           |
      | 'PATCH'  | '/paramYear'           | 'paramYear'           | ''    | 'paramYear: must not be null'           |
      | 'DELETE' | '/paramYear'           | 'paramYear'           | ''    | 'paramYear: must not be null'           |
      | 'GET'    | '/paramEnumAB'         | 'paramEnumAB'         | ''    | 'paramEnumAB: must not be null'         |
      | 'HEAD'   | '/paramEnumAB'         | 'paramEnumAB'         | ''    |                                         |
      | 'POST'   | '/paramEnumAB'         | 'paramEnumAB'         | ''    | 'paramEnumAB: must not be null'         |
      | 'PUT'    | '/paramEnumAB'         | 'paramEnumAB'         | ''    | 'paramEnumAB: must not be null'         |
      | 'PATCH'  | '/paramEnumAB'         | 'paramEnumAB'         | ''    | 'paramEnumAB: must not be null'         |
      | 'DELETE' | '/paramEnumAB'         | 'paramEnumAB'         | ''    | 'paramEnumAB: must not be null'         |
      | 'GET'    | '/paramEnumLowerAB'    | 'paramEnumLowerAB'    | ''    | 'paramEnumLowerAB: must not be null'    |
      | 'HEAD'   | '/paramEnumLowerAB'    | 'paramEnumLowerAB'    | ''    |                                         |
      | 'POST'   | '/paramEnumLowerAB'    | 'paramEnumLowerAB'    | ''    | 'paramEnumLowerAB: must not be null'    |
      | 'PUT'    | '/paramEnumLowerAB'    | 'paramEnumLowerAB'    | ''    | 'paramEnumLowerAB: must not be null'    |
      | 'PATCH'  | '/paramEnumLowerAB'    | 'paramEnumLowerAB'    | ''    | 'paramEnumLowerAB: must not be null'    |
      | 'DELETE' | '/paramEnumLowerAB'    | 'paramEnumLowerAB'    | ''    | 'paramEnumLowerAB: must not be null'    |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/required/named-method-parameter<path>?<param>=<value> endpoint (RequestParamRequiredRestApi.class)
    this endpoint receives the request parameter in a required method parameter, both having the diferent names, a custom request-parameter name is defined by the endpoint 
    and the service maps the sent empty value to null, but service responding with an error message since data is requited.

    Given the endpoint '/api/v1/request-parameter/required/named-method-parameter'<path>
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | param                    | value | message                                    |
      | 'GET'    | '/paramString'         | 'param-string'           | ''    | 'param-string: must not be null'           |
      | 'HEAD'   | '/paramString'         | 'param-string'           | ''    |                                            |
      | 'POST'   | '/paramString'         | 'param-string'           | ''    | 'param-string: must not be null'           |
      | 'PUT'    | '/paramString'         | 'param-string'           | ''    | 'param-string: must not be null'           |
      | 'PATCH'  | '/paramString'         | 'param-string'           | ''    | 'param-string: must not be null'           |
      | 'DELETE' | '/paramString'         | 'param-string'           | ''    | 'param-string: must not be null'           |
      | 'GET'    | '/paramInteger'        | 'param-integer'          | ''    | 'param-integer: must not be null'          |
      | 'HEAD'   | '/paramInteger'        | 'param-integer'          | ''    |                                            |
      | 'POST'   | '/paramInteger'        | 'param-integer'          | ''    | 'param-integer: must not be null'          |
      | 'PUT'    | '/paramInteger'        | 'param-integer'          | ''    | 'param-integer: must not be null'          |
      | 'PATCH'  | '/paramInteger'        | 'param-integer'          | ''    | 'param-integer: must not be null'          |
      | 'DELETE' | '/paramInteger'        | 'param-integer'          | ''    | 'param-integer: must not be null'          |
      | 'GET'    | '/paramLong'           | 'param-long'             | ''    | 'param-long: must not be null'             |
      | 'HEAD'   | '/paramLong'           | 'param-long'             | ''    |                                            |
      | 'POST'   | '/paramLong'           | 'param-long'             | ''    | 'param-long: must not be null'             |
      | 'PUT'    | '/paramLong'           | 'param-long'             | ''    | 'param-long: must not be null'             |
      | 'PATCH'  | '/paramLong'           | 'param-long'             | ''    | 'param-long: must not be null'             |
      | 'DELETE' | '/paramLong'           | 'param-long'             | ''    | 'param-long: must not be null'             |
      | 'GET'    | '/paramBigInteger'     | 'param-big-integer'      | ''    | 'param-big-integer: must not be null'      |
      | 'HEAD'   | '/paramBigInteger'     | 'param-big-integer'      | ''    |                                            |
      | 'POST'   | '/paramBigInteger'     | 'param-big-integer'      | ''    | 'param-big-integer: must not be null'      |
      | 'PUT'    | '/paramBigInteger'     | 'param-big-integer'      | ''    | 'param-big-integer: must not be null'      |
      | 'PATCH'  | '/paramBigInteger'     | 'param-big-integer'      | ''    | 'param-big-integer: must not be null'      |
      | 'DELETE' | '/paramBigInteger'     | 'param-big-integer'      | ''    | 'param-big-integer: must not be null'      |
      | 'GET'    | '/paramFloat'          | 'param-float'            | ''    | 'param-float: must not be null'            |
      | 'HEAD'   | '/paramFloat'          | 'param-float'            | ''    |                                            |
      | 'POST'   | '/paramFloat'          | 'param-float'            | ''    | 'param-float: must not be null'            |
      | 'PUT'    | '/paramFloat'          | 'param-float'            | ''    | 'param-float: must not be null'            |
      | 'PATCH'  | '/paramFloat'          | 'param-float'            | ''    | 'param-float: must not be null'            |
      | 'DELETE' | '/paramFloat'          | 'param-float'            | ''    | 'param-float: must not be null'            |
      | 'GET'    | '/paramDouble'         | 'param-double'           | ''    | 'param-double: must not be null'           |
      | 'HEAD'   | '/paramDouble'         | 'param-double'           | ''    |                                            |
      | 'POST'   | '/paramDouble'         | 'param-double'           | ''    | 'param-double: must not be null'           |
      | 'PUT'    | '/paramDouble'         | 'param-double'           | ''    | 'param-double: must not be null'           |
      | 'PATCH'  | '/paramDouble'         | 'param-double'           | ''    | 'param-double: must not be null'           |
      | 'DELETE' | '/paramDouble'         | 'param-double'           | ''    | 'param-double: must not be null'           |
      | 'GET'    | '/paramBigDecimal'     | 'param-big-decimal'      | ''    | 'param-big-decimal: must not be null'      |
      | 'HEAD'   | '/paramBigDecimal'     | 'param-big-decimal'      | ''    |                                            |
      | 'POST'   | '/paramBigDecimal'     | 'param-big-decimal'      | ''    | 'param-big-decimal: must not be null'      |
      | 'PUT'    | '/paramBigDecimal'     | 'param-big-decimal'      | ''    | 'param-big-decimal: must not be null'      |
      | 'PATCH'  | '/paramBigDecimal'     | 'param-big-decimal'      | ''    | 'param-big-decimal: must not be null'      |
      | 'DELETE' | '/paramBigDecimal'     | 'param-big-decimal'      | ''    | 'param-big-decimal: must not be null'      |
      | 'GET'    | '/paramBoolean'        | 'param-boolean'          | ''    | 'param-boolean: must not be null'          |
      | 'HEAD'   | '/paramBoolean'        | 'param-boolean'          | ''    |                                            |
      | 'POST'   | '/paramBoolean'        | 'param-boolean'          | ''    | 'param-boolean: must not be null'          |
      | 'PUT'    | '/paramBoolean'        | 'param-boolean'          | ''    | 'param-boolean: must not be null'          |
      | 'PATCH'  | '/paramBoolean'        | 'param-boolean'          | ''    | 'param-boolean: must not be null'          |
      | 'DELETE' | '/paramBoolean'        | 'param-boolean'          | ''    | 'param-boolean: must not be null'          |
      | 'GET'    | '/paramOffsetDateTime' | 'param-offset-date-time' | ''    | 'param-offset-date-time: must not be null' |
      | 'HEAD'   | '/paramOffsetDateTime' | 'param-offset-date-time' | ''    |                                            |
      | 'POST'   | '/paramOffsetDateTime' | 'param-offset-date-time' | ''    | 'param-offset-date-time: must not be null' |
      | 'PUT'    | '/paramOffsetDateTime' | 'param-offset-date-time' | ''    | 'param-offset-date-time: must not be null' |
      | 'PATCH'  | '/paramOffsetDateTime' | 'param-offset-date-time' | ''    | 'param-offset-date-time: must not be null' |
      | 'DELETE' | '/paramOffsetDateTime' | 'param-offset-date-time' | ''    | 'param-offset-date-time: must not be null' |
      | 'GET'    | '/paramLocalDateTime'  | 'param-local-date-time'  | ''    | 'param-local-date-time: must not be null'  |
      | 'HEAD'   | '/paramLocalDateTime'  | 'param-local-date-time'  | ''    |                                            |
      | 'POST'   | '/paramLocalDateTime'  | 'param-local-date-time'  | ''    | 'param-local-date-time: must not be null'  |
      | 'PUT'    | '/paramLocalDateTime'  | 'param-local-date-time'  | ''    | 'param-local-date-time: must not be null'  |
      | 'PATCH'  | '/paramLocalDateTime'  | 'param-local-date-time'  | ''    | 'param-local-date-time: must not be null'  |
      | 'DELETE' | '/paramLocalDateTime'  | 'param-local-date-time'  | ''    | 'param-local-date-time: must not be null'  |
      | 'GET'    | '/paramLocalDate'      | 'param-local-date'       | ''    | 'param-local-date: must not be null'       |
      | 'HEAD'   | '/paramLocalDate'      | 'param-local-date'       | ''    |                                            |
      | 'POST'   | '/paramLocalDate'      | 'param-local-date'       | ''    | 'param-local-date: must not be null'       |
      | 'PUT'    | '/paramLocalDate'      | 'param-local-date'       | ''    | 'param-local-date: must not be null'       |
      | 'PATCH'  | '/paramLocalDate'      | 'param-local-date'       | ''    | 'param-local-date: must not be null'       |
      | 'DELETE' | '/paramLocalDate'      | 'param-local-date'       | ''    | 'param-local-date: must not be null'       |
      | 'GET'    | '/paramLocalTime'      | 'param-local-time'       | ''    | 'param-local-time: must not be null'       |
      | 'HEAD'   | '/paramLocalTime'      | 'param-local-time'       | ''    |                                            |
      | 'POST'   | '/paramLocalTime'      | 'param-local-time'       | ''    | 'param-local-time: must not be null'       |
      | 'PUT'    | '/paramLocalTime'      | 'param-local-time'       | ''    | 'param-local-time: must not be null'       |
      | 'PATCH'  | '/paramLocalTime'      | 'param-local-time'       | ''    | 'param-local-time: must not be null'       |
      | 'DELETE' | '/paramLocalTime'      | 'param-local-time'       | ''    | 'param-local-time: must not be null'       |
      | 'GET'    | '/paramYear'           | 'param-year'             | ''    | 'param-year: must not be null'             |
      | 'HEAD'   | '/paramYear'           | 'param-year'             | ''    |                                            |
      | 'POST'   | '/paramYear'           | 'param-year'             | ''    | 'param-year: must not be null'             |
      | 'PUT'    | '/paramYear'           | 'param-year'             | ''    | 'param-year: must not be null'             |
      | 'PATCH'  | '/paramYear'           | 'param-year'             | ''    | 'param-year: must not be null'             |
      | 'DELETE' | '/paramYear'           | 'param-year'             | ''    | 'param-year: must not be null'             |
      | 'GET'    | '/paramEnumAB'         | 'param-enum-ab'          | ''    | 'param-enum-ab: must not be null'          |
      | 'HEAD'   | '/paramEnumAB'         | 'param-enum-ab'          | ''    |                                            |
      | 'POST'   | '/paramEnumAB'         | 'param-enum-ab'          | ''    | 'param-enum-ab: must not be null'          |
      | 'PUT'    | '/paramEnumAB'         | 'param-enum-ab'          | ''    | 'param-enum-ab: must not be null'          |
      | 'PATCH'  | '/paramEnumAB'         | 'param-enum-ab'          | ''    | 'param-enum-ab: must not be null'          |
      | 'DELETE' | '/paramEnumAB'         | 'param-enum-ab'          | ''    | 'param-enum-ab: must not be null'          |
      | 'GET'    | '/paramEnumLowerAB'    | 'param-enum-lower-ab'    | ''    | 'param-enum-lower-ab: must not be null'    |
      | 'HEAD'   | '/paramEnumLowerAB'    | 'param-enum-lower-ab'    | ''    |                                            |
      | 'POST'   | '/paramEnumLowerAB'    | 'param-enum-lower-ab'    | ''    | 'param-enum-lower-ab: must not be null'    |
      | 'PUT'    | '/paramEnumLowerAB'    | 'param-enum-lower-ab'    | ''    | 'param-enum-lower-ab: must not be null'    |
      | 'PATCH'  | '/paramEnumLowerAB'    | 'param-enum-lower-ab'    | ''    | 'param-enum-lower-ab: must not be null'    |
      | 'DELETE' | '/paramEnumLowerAB'    | 'param-enum-lower-ab'    | ''    | 'param-enum-lower-ab: must not be null'    |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/required/optional-method-parameter<path>?<param>=<value> endpoint (RequestParamRequiredRestApi.class)
    this endpoint receives the request parameter in a required but optional method parameter, both having the same name
    and the service correctly maps the sent empty value with the declared data type as an optional-empty, responding with a success message.

    Given the endpoint '/api/v1/request-parameter/required/optional-method-parameter'<path>
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | path                   | param                 | value | received value                 |
      | 'GET'    | '/paramString'         | 'paramString'         | ''    | '{"paramString":null}'         |
      | 'HEAD'   | '/paramString'         | 'paramString'         | ''    | '{"paramString":null}'         |
      | 'POST'   | '/paramString'         | 'paramString'         | ''    | '{"paramString":null}'         |
      | 'PUT'    | '/paramString'         | 'paramString'         | ''    | '{"paramString":null}'         |
      | 'PATCH'  | '/paramString'         | 'paramString'         | ''    | '{"paramString":null}'         |
      | 'DELETE' | '/paramString'         | 'paramString'         | ''    | '{"paramString":null}'         |
      | 'GET'    | '/paramInteger'        | 'paramInteger'        | ''    | '{"paramInteger":null}'        |
      | 'HEAD'   | '/paramInteger'        | 'paramInteger'        | ''    | '{"paramInteger":null}'        |
      | 'POST'   | '/paramInteger'        | 'paramInteger'        | ''    | '{"paramInteger":null}'        |
      | 'PUT'    | '/paramInteger'        | 'paramInteger'        | ''    | '{"paramInteger":null}'        |
      | 'PATCH'  | '/paramInteger'        | 'paramInteger'        | ''    | '{"paramInteger":null}'        |
      | 'DELETE' | '/paramInteger'        | 'paramInteger'        | ''    | '{"paramInteger":null}'        |
      | 'GET'    | '/paramLong'           | 'paramLong'           | ''    | '{"paramLong":null}'           |
      | 'HEAD'   | '/paramLong'           | 'paramLong'           | ''    | '{"paramLong":null}'           |
      | 'POST'   | '/paramLong'           | 'paramLong'           | ''    | '{"paramLong":null}'           |
      | 'PUT'    | '/paramLong'           | 'paramLong'           | ''    | '{"paramLong":null}'           |
      | 'PATCH'  | '/paramLong'           | 'paramLong'           | ''    | '{"paramLong":null}'           |
      | 'DELETE' | '/paramLong'           | 'paramLong'           | ''    | '{"paramLong":null}'           |
      | 'GET'    | '/paramBigInteger'     | 'paramBigInteger'     | ''    | '{"paramBigInteger":null}'     |
      | 'HEAD'   | '/paramBigInteger'     | 'paramBigInteger'     | ''    | '{"paramBigInteger":null}'     |
      | 'POST'   | '/paramBigInteger'     | 'paramBigInteger'     | ''    | '{"paramBigInteger":null}'     |
      | 'PUT'    | '/paramBigInteger'     | 'paramBigInteger'     | ''    | '{"paramBigInteger":null}'     |
      | 'PATCH'  | '/paramBigInteger'     | 'paramBigInteger'     | ''    | '{"paramBigInteger":null}'     |
      | 'DELETE' | '/paramBigInteger'     | 'paramBigInteger'     | ''    | '{"paramBigInteger":null}'     |
      | 'GET'    | '/paramFloat'          | 'paramFloat'          | ''    | '{"paramFloat":null}'          |
      | 'HEAD'   | '/paramFloat'          | 'paramFloat'          | ''    | '{"paramFloat":null}'          |
      | 'POST'   | '/paramFloat'          | 'paramFloat'          | ''    | '{"paramFloat":null}'          |
      | 'PUT'    | '/paramFloat'          | 'paramFloat'          | ''    | '{"paramFloat":null}'          |
      | 'PATCH'  | '/paramFloat'          | 'paramFloat'          | ''    | '{"paramFloat":null}'          |
      | 'DELETE' | '/paramFloat'          | 'paramFloat'          | ''    | '{"paramFloat":null}'          |
      | 'GET'    | '/paramDouble'         | 'paramDouble'         | ''    | '{"paramDouble":null}'         |
      | 'HEAD'   | '/paramDouble'         | 'paramDouble'         | ''    | '{"paramDouble":null}'         |
      | 'POST'   | '/paramDouble'         | 'paramDouble'         | ''    | '{"paramDouble":null}'         |
      | 'PUT'    | '/paramDouble'         | 'paramDouble'         | ''    | '{"paramDouble":null}'         |
      | 'PATCH'  | '/paramDouble'         | 'paramDouble'         | ''    | '{"paramDouble":null}'         |
      | 'DELETE' | '/paramDouble'         | 'paramDouble'         | ''    | '{"paramDouble":null}'         |
      | 'GET'    | '/paramBigDecimal'     | 'paramBigDecimal'     | ''    | '{"paramBigDecimal":null}'     |
      | 'HEAD'   | '/paramBigDecimal'     | 'paramBigDecimal'     | ''    | '{"paramBigDecimal":null}'     |
      | 'POST'   | '/paramBigDecimal'     | 'paramBigDecimal'     | ''    | '{"paramBigDecimal":null}'     |
      | 'PUT'    | '/paramBigDecimal'     | 'paramBigDecimal'     | ''    | '{"paramBigDecimal":null}'     |
      | 'PATCH'  | '/paramBigDecimal'     | 'paramBigDecimal'     | ''    | '{"paramBigDecimal":null}'     |
      | 'DELETE' | '/paramBigDecimal'     | 'paramBigDecimal'     | ''    | '{"paramBigDecimal":null}'     |
      | 'GET'    | '/paramBoolean'        | 'paramBoolean'        | ''    | '{"paramBoolean":null}'        |
      | 'HEAD'   | '/paramBoolean'        | 'paramBoolean'        | ''    | '{"paramBoolean":null}'        |
      | 'POST'   | '/paramBoolean'        | 'paramBoolean'        | ''    | '{"paramBoolean":null}'        |
      | 'PUT'    | '/paramBoolean'        | 'paramBoolean'        | ''    | '{"paramBoolean":null}'        |
      | 'PATCH'  | '/paramBoolean'        | 'paramBoolean'        | ''    | '{"paramBoolean":null}'        |
      | 'DELETE' | '/paramBoolean'        | 'paramBoolean'        | ''    | '{"paramBoolean":null}'        |
      | 'GET'    | '/paramOffsetDateTime' | 'paramOffsetDateTime' | ''    | '{"paramOffsetDateTime":null}' |
      | 'HEAD'   | '/paramOffsetDateTime' | 'paramOffsetDateTime' | ''    | '{"paramOffsetDateTime":null}' |
      | 'POST'   | '/paramOffsetDateTime' | 'paramOffsetDateTime' | ''    | '{"paramOffsetDateTime":null}' |
      | 'PUT'    | '/paramOffsetDateTime' | 'paramOffsetDateTime' | ''    | '{"paramOffsetDateTime":null}' |
      | 'PATCH'  | '/paramOffsetDateTime' | 'paramOffsetDateTime' | ''    | '{"paramOffsetDateTime":null}' |
      | 'DELETE' | '/paramOffsetDateTime' | 'paramOffsetDateTime' | ''    | '{"paramOffsetDateTime":null}' |
      | 'GET'    | '/paramLocalDateTime'  | 'paramLocalDateTime'  | ''    | '{"paramLocalDateTime":null}'  |
      | 'HEAD'   | '/paramLocalDateTime'  | 'paramLocalDateTime'  | ''    | '{"paramLocalDateTime":null}'  |
      | 'POST'   | '/paramLocalDateTime'  | 'paramLocalDateTime'  | ''    | '{"paramLocalDateTime":null}'  |
      | 'PUT'    | '/paramLocalDateTime'  | 'paramLocalDateTime'  | ''    | '{"paramLocalDateTime":null}'  |
      | 'PATCH'  | '/paramLocalDateTime'  | 'paramLocalDateTime'  | ''    | '{"paramLocalDateTime":null}'  |
      | 'DELETE' | '/paramLocalDateTime'  | 'paramLocalDateTime'  | ''    | '{"paramLocalDateTime":null}'  |
      | 'GET'    | '/paramLocalDate'      | 'paramLocalDate'      | ''    | '{"paramLocalDate":null}'      |
      | 'HEAD'   | '/paramLocalDate'      | 'paramLocalDate'      | ''    | '{"paramLocalDate":null}'      |
      | 'POST'   | '/paramLocalDate'      | 'paramLocalDate'      | ''    | '{"paramLocalDate":null}'      |
      | 'PUT'    | '/paramLocalDate'      | 'paramLocalDate'      | ''    | '{"paramLocalDate":null}'      |
      | 'PATCH'  | '/paramLocalDate'      | 'paramLocalDate'      | ''    | '{"paramLocalDate":null}'      |
      | 'DELETE' | '/paramLocalDate'      | 'paramLocalDate'      | ''    | '{"paramLocalDate":null}'      |
      | 'GET'    | '/paramLocalTime'      | 'paramLocalTime'      | ''    | '{"paramLocalTime":null}'      |
      | 'HEAD'   | '/paramLocalTime'      | 'paramLocalTime'      | ''    | '{"paramLocalTime":null}'      |
      | 'POST'   | '/paramLocalTime'      | 'paramLocalTime'      | ''    | '{"paramLocalTime":null}'      |
      | 'PUT'    | '/paramLocalTime'      | 'paramLocalTime'      | ''    | '{"paramLocalTime":null}'      |
      | 'PATCH'  | '/paramLocalTime'      | 'paramLocalTime'      | ''    | '{"paramLocalTime":null}'      |
      | 'DELETE' | '/paramLocalTime'      | 'paramLocalTime'      | ''    | '{"paramLocalTime":null}'      |
      | 'GET'    | '/paramYear'           | 'paramYear'           | ''    | '{"paramYear":null}'           |
      | 'HEAD'   | '/paramYear'           | 'paramYear'           | ''    | '{"paramYear":null}'           |
      | 'POST'   | '/paramYear'           | 'paramYear'           | ''    | '{"paramYear":null}'           |
      | 'PUT'    | '/paramYear'           | 'paramYear'           | ''    | '{"paramYear":null}'           |
      | 'PATCH'  | '/paramYear'           | 'paramYear'           | ''    | '{"paramYear":null}'           |
      | 'DELETE' | '/paramYear'           | 'paramYear'           | ''    | '{"paramYear":null}'           |
      | 'GET'    | '/paramEnumAB'         | 'paramEnumAB'         | ''    | '{"paramEnumAB":null}'         |
      | 'HEAD'   | '/paramEnumAB'         | 'paramEnumAB'         | ''    | '{"paramEnumAB":null}'         |
      | 'POST'   | '/paramEnumAB'         | 'paramEnumAB'         | ''    | '{"paramEnumAB":null}'         |
      | 'PUT'    | '/paramEnumAB'         | 'paramEnumAB'         | ''    | '{"paramEnumAB":null}'         |
      | 'PATCH'  | '/paramEnumAB'         | 'paramEnumAB'         | ''    | '{"paramEnumAB":null}'         |
      | 'DELETE' | '/paramEnumAB'         | 'paramEnumAB'         | ''    | '{"paramEnumAB":null}'         |
      | 'GET'    | '/paramEnumLowerAB'    | 'paramEnumLowerAB'    | ''    | '{"paramEnumLowerAB":null}'    |
      | 'HEAD'   | '/paramEnumLowerAB'    | 'paramEnumLowerAB'    | ''    | '{"paramEnumLowerAB":null}'    |
      | 'POST'   | '/paramEnumLowerAB'    | 'paramEnumLowerAB'    | ''    | '{"paramEnumLowerAB":null}'    |
      | 'PUT'    | '/paramEnumLowerAB'    | 'paramEnumLowerAB'    | ''    | '{"paramEnumLowerAB":null}'    |
      | 'PATCH'  | '/paramEnumLowerAB'    | 'paramEnumLowerAB'    | ''    | '{"paramEnumLowerAB":null}'    |
      | 'DELETE' | '/paramEnumLowerAB'    | 'paramEnumLowerAB'    | ''    | '{"paramEnumLowerAB":null}'    |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/required/named-optional-method-parameter<path>?<param>=<value> endpoint (RequestParamRequiredRestApi.class)
    this endpoint receives the request parameter in a required but optional method parameter, both having the diferent names, a custom request-parameter name is defined by the endpoint 
    and the service correctly maps the sent empty value with the declared data type as an optional-empty, responding with a success message.

    Given the endpoint '/api/v1/request-parameter/required/named-optional-method-parameter'<path>
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | path                   | param                    | value | received value |
      | 'GET'    | '/paramString'         | 'param-string'           | ''    | '{}'           |
      | 'HEAD'   | '/paramString'         | 'param-string'           | ''    | '{}'           |
      | 'POST'   | '/paramString'         | 'param-string'           | ''    | '{}'           |
      | 'PUT'    | '/paramString'         | 'param-string'           | ''    | '{}'           |
      | 'PATCH'  | '/paramString'         | 'param-string'           | ''    | '{}'           |
      | 'DELETE' | '/paramString'         | 'param-string'           | ''    | '{}'           |
      | 'GET'    | '/paramInteger'        | 'param-integer'          | ''    | '{}'           |
      | 'HEAD'   | '/paramInteger'        | 'param-integer'          | ''    | '{}'           |
      | 'POST'   | '/paramInteger'        | 'param-integer'          | ''    | '{}'           |
      | 'PUT'    | '/paramInteger'        | 'param-integer'          | ''    | '{}'           |
      | 'PATCH'  | '/paramInteger'        | 'param-integer'          | ''    | '{}'           |
      | 'DELETE' | '/paramInteger'        | 'param-integer'          | ''    | '{}'           |
      | 'GET'    | '/paramLong'           | 'param-long'             | ''    | '{}'           |
      | 'HEAD'   | '/paramLong'           | 'param-long'             | ''    | '{}'           |
      | 'POST'   | '/paramLong'           | 'param-long'             | ''    | '{}'           |
      | 'PUT'    | '/paramLong'           | 'param-long'             | ''    | '{}'           |
      | 'PATCH'  | '/paramLong'           | 'param-long'             | ''    | '{}'           |
      | 'DELETE' | '/paramLong'           | 'param-long'             | ''    | '{}'           |
      | 'GET'    | '/paramBigInteger'     | 'param-big-integer'      | ''    | '{}'           |
      | 'HEAD'   | '/paramBigInteger'     | 'param-big-integer'      | ''    | '{}'           |
      | 'POST'   | '/paramBigInteger'     | 'param-big-integer'      | ''    | '{}'           |
      | 'PUT'    | '/paramBigInteger'     | 'param-big-integer'      | ''    | '{}'           |
      | 'PATCH'  | '/paramBigInteger'     | 'param-big-integer'      | ''    | '{}'           |
      | 'DELETE' | '/paramBigInteger'     | 'param-big-integer'      | ''    | '{}'           |
      | 'GET'    | '/paramFloat'          | 'param-float'            | ''    | '{}'           |
      | 'HEAD'   | '/paramFloat'          | 'param-float'            | ''    | '{}'           |
      | 'POST'   | '/paramFloat'          | 'param-float'            | ''    | '{}'           |
      | 'PUT'    | '/paramFloat'          | 'param-float'            | ''    | '{}'           |
      | 'PATCH'  | '/paramFloat'          | 'param-float'            | ''    | '{}'           |
      | 'DELETE' | '/paramFloat'          | 'param-float'            | ''    | '{}'           |
      | 'GET'    | '/paramDouble'         | 'param-double'           | ''    | '{}'           |
      | 'HEAD'   | '/paramDouble'         | 'param-double'           | ''    | '{}'           |
      | 'POST'   | '/paramDouble'         | 'param-double'           | ''    | '{}'           |
      | 'PUT'    | '/paramDouble'         | 'param-double'           | ''    | '{}'           |
      | 'PATCH'  | '/paramDouble'         | 'param-double'           | ''    | '{}'           |
      | 'DELETE' | '/paramDouble'         | 'param-double'           | ''    | '{}'           |
      | 'GET'    | '/paramBigDecimal'     | 'param-big-decimal'      | ''    | '{}'           |
      | 'HEAD'   | '/paramBigDecimal'     | 'param-big-decimal'      | ''    | '{}'           |
      | 'POST'   | '/paramBigDecimal'     | 'param-big-decimal'      | ''    | '{}'           |
      | 'PUT'    | '/paramBigDecimal'     | 'param-big-decimal'      | ''    | '{}'           |
      | 'PATCH'  | '/paramBigDecimal'     | 'param-big-decimal'      | ''    | '{}'           |
      | 'DELETE' | '/paramBigDecimal'     | 'param-big-decimal'      | ''    | '{}'           |
      | 'GET'    | '/paramBoolean'        | 'param-boolean'          | ''    | '{}'           |
      | 'HEAD'   | '/paramBoolean'        | 'param-boolean'          | ''    | '{}'           |
      | 'POST'   | '/paramBoolean'        | 'param-boolean'          | ''    | '{}'           |
      | 'PUT'    | '/paramBoolean'        | 'param-boolean'          | ''    | '{}'           |
      | 'PATCH'  | '/paramBoolean'        | 'param-boolean'          | ''    | '{}'           |
      | 'DELETE' | '/paramBoolean'        | 'param-boolean'          | ''    | '{}'           |
      | 'GET'    | '/paramOffsetDateTime' | 'param-offset-date-time' | ''    | '{}'           |
      | 'HEAD'   | '/paramOffsetDateTime' | 'param-offset-date-time' | ''    | '{}'           |
      | 'POST'   | '/paramOffsetDateTime' | 'param-offset-date-time' | ''    | '{}'           |
      | 'PUT'    | '/paramOffsetDateTime' | 'param-offset-date-time' | ''    | '{}'           |
      | 'PATCH'  | '/paramOffsetDateTime' | 'param-offset-date-time' | ''    | '{}'           |
      | 'DELETE' | '/paramOffsetDateTime' | 'param-offset-date-time' | ''    | '{}'           |
      | 'GET'    | '/paramLocalDateTime'  | 'param-local-date-time'  | ''    | '{}'           |
      | 'HEAD'   | '/paramLocalDateTime'  | 'param-local-date-time'  | ''    | '{}'           |
      | 'POST'   | '/paramLocalDateTime'  | 'param-local-date-time'  | ''    | '{}'           |
      | 'PUT'    | '/paramLocalDateTime'  | 'param-local-date-time'  | ''    | '{}'           |
      | 'PATCH'  | '/paramLocalDateTime'  | 'param-local-date-time'  | ''    | '{}'           |
      | 'DELETE' | '/paramLocalDateTime'  | 'param-local-date-time'  | ''    | '{}'           |
      | 'GET'    | '/paramLocalDate'      | 'param-local-date'       | ''    | '{}'           |
      | 'HEAD'   | '/paramLocalDate'      | 'param-local-date'       | ''    | '{}'           |
      | 'POST'   | '/paramLocalDate'      | 'param-local-date'       | ''    | '{}'           |
      | 'PUT'    | '/paramLocalDate'      | 'param-local-date'       | ''    | '{}'           |
      | 'PATCH'  | '/paramLocalDate'      | 'param-local-date'       | ''    | '{}'           |
      | 'DELETE' | '/paramLocalDate'      | 'param-local-date'       | ''    | '{}'           |
      | 'GET'    | '/paramLocalTime'      | 'param-local-time'       | ''    | '{}'           |
      | 'HEAD'   | '/paramLocalTime'      | 'param-local-time'       | ''    | '{}'           |
      | 'POST'   | '/paramLocalTime'      | 'param-local-time'       | ''    | '{}'           |
      | 'PUT'    | '/paramLocalTime'      | 'param-local-time'       | ''    | '{}'           |
      | 'PATCH'  | '/paramLocalTime'      | 'param-local-time'       | ''    | '{}'           |
      | 'DELETE' | '/paramLocalTime'      | 'param-local-time'       | ''    | '{}'           |
      | 'GET'    | '/paramYear'           | 'param-year'             | ''    | '{}'           |
      | 'HEAD'   | '/paramYear'           | 'param-year'             | ''    | '{}'           |
      | 'POST'   | '/paramYear'           | 'param-year'             | ''    | '{}'           |
      | 'PUT'    | '/paramYear'           | 'param-year'             | ''    | '{}'           |
      | 'PATCH'  | '/paramYear'           | 'param-year'             | ''    | '{}'           |
      | 'DELETE' | '/paramYear'           | 'param-year'             | ''    | '{}'           |
      | 'GET'    | '/paramEnumAB'         | 'param-enum-ab'          | ''    | '{}'           |
      | 'HEAD'   | '/paramEnumAB'         | 'param-enum-ab'          | ''    | '{}'           |
      | 'POST'   | '/paramEnumAB'         | 'param-enum-ab'          | ''    | '{}'           |
      | 'PUT'    | '/paramEnumAB'         | 'param-enum-ab'          | ''    | '{}'           |
      | 'PATCH'  | '/paramEnumAB'         | 'param-enum-ab'          | ''    | '{}'           |
      | 'DELETE' | '/paramEnumAB'         | 'param-enum-ab'          | ''    | '{}'           |
      | 'GET'    | '/paramEnumLowerAB'    | 'param-enum-lower-ab'    | ''    | '{}'           |
      | 'HEAD'   | '/paramEnumLowerAB'    | 'param-enum-lower-ab'    | ''    | '{}'           |
      | 'POST'   | '/paramEnumLowerAB'    | 'param-enum-lower-ab'    | ''    | '{}'           |
      | 'PUT'    | '/paramEnumLowerAB'    | 'param-enum-lower-ab'    | ''    | '{}'           |
      | 'PATCH'  | '/paramEnumLowerAB'    | 'param-enum-lower-ab'    | ''    | '{}'           |
      | 'DELETE' | '/paramEnumLowerAB'    | 'param-enum-lower-ab'    | ''    | '{}'           |
