Feature: Request parameter with empty parameter value
  
  The REST service receives empty data via the request parameter and interprets it as null. The service responds with a success message.
  
   Rule: The conversion between data types is given by the following table
  
  | parameter type | java type      |
  |----------------|----------------|
  | string         | String         |
  | number         | Integer        |
  | number         | Long           |
  | number         | BigInteger     |
  | number         | Float          |
  | number         | Double         |
  | number         | BigDecimal     |
  | boolean        | Boolean        |
  | ISO-8601       | OffsetDateTime |
  | ISO-8601       | LocalDateTime  |
  | ISO-8601       | LocalDate      |
  | ISO-8601       | LocalTime      |
  | ISO-8601       | Year           |
  | string         | ENUM           |

  Background: 
    Given the default timezone 'America/Puerto_Rico'

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/method-parameter?<param>=<value> endpoint (RequestParamUnconstrainedRestApi.class)
    this endpoint receives the request parameter in a method parameter, both having the same name
    and the service correctly maps the sent empty value with the declared data type by setting it to null, responding with a success message.

    Given the endpoint '/api/v1/request-parameter/method-parameter'
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | param                 | value | received value |
      | 'GET'    | 'paramString'         | ''    | '{}'           |
      | 'HEAD'   | 'paramString'         | ''    | '{}'           |
      | 'POST'   | 'paramString'         | ''    | '{}'           |
      | 'PUT'    | 'paramString'         | ''    | '{}'           |
      | 'PATCH'  | 'paramString'         | ''    | '{}'           |
      | 'DELETE' | 'paramString'         | ''    | '{}'           |
      | 'GET'    | 'paramInteger'        | ''    | '{}'           |
      | 'HEAD'   | 'paramInteger'        | ''    | '{}'           |
      | 'POST'   | 'paramInteger'        | ''    | '{}'           |
      | 'PUT'    | 'paramInteger'        | ''    | '{}'           |
      | 'PATCH'  | 'paramInteger'        | ''    | '{}'           |
      | 'DELETE' | 'paramInteger'        | ''    | '{}'           |
      | 'GET'    | 'paramLong'           | ''    | '{}'           |
      | 'HEAD'   | 'paramLong'           | ''    | '{}'           |
      | 'POST'   | 'paramLong'           | ''    | '{}'           |
      | 'PUT'    | 'paramLong'           | ''    | '{}'           |
      | 'PATCH'  | 'paramLong'           | ''    | '{}'           |
      | 'DELETE' | 'paramLong'           | ''    | '{}'           |
      | 'GET'    | 'paramBigInteger'     | ''    | '{}'           |
      | 'HEAD'   | 'paramBigInteger'     | ''    | '{}'           |
      | 'POST'   | 'paramBigInteger'     | ''    | '{}'           |
      | 'PUT'    | 'paramBigInteger'     | ''    | '{}'           |
      | 'PATCH'  | 'paramBigInteger'     | ''    | '{}'           |
      | 'DELETE' | 'paramBigInteger'     | ''    | '{}'           |
      | 'GET'    | 'paramFloat'          | ''    | '{}'           |
      | 'HEAD'   | 'paramFloat'          | ''    | '{}'           |
      | 'POST'   | 'paramFloat'          | ''    | '{}'           |
      | 'PUT'    | 'paramFloat'          | ''    | '{}'           |
      | 'PATCH'  | 'paramFloat'          | ''    | '{}'           |
      | 'DELETE' | 'paramFloat'          | ''    | '{}'           |
      | 'GET'    | 'paramDouble'         | ''    | '{}'           |
      | 'HEAD'   | 'paramDouble'         | ''    | '{}'           |
      | 'POST'   | 'paramDouble'         | ''    | '{}'           |
      | 'PUT'    | 'paramDouble'         | ''    | '{}'           |
      | 'PATCH'  | 'paramDouble'         | ''    | '{}'           |
      | 'DELETE' | 'paramDouble'         | ''    | '{}'           |
      | 'GET'    | 'paramBigDecimal'     | ''    | '{}'           |
      | 'HEAD'   | 'paramBigDecimal'     | ''    | '{}'           |
      | 'POST'   | 'paramBigDecimal'     | ''    | '{}'           |
      | 'PUT'    | 'paramBigDecimal'     | ''    | '{}'           |
      | 'PATCH'  | 'paramBigDecimal'     | ''    | '{}'           |
      | 'DELETE' | 'paramBigDecimal'     | ''    | '{}'           |
      | 'GET'    | 'paramBoolean'        | ''    | '{}'           |
      | 'HEAD'   | 'paramBoolean'        | ''    | '{}'           |
      | 'POST'   | 'paramBoolean'        | ''    | '{}'           |
      | 'PUT'    | 'paramBoolean'        | ''    | '{}'           |
      | 'PATCH'  | 'paramBoolean'        | ''    | '{}'           |
      | 'DELETE' | 'paramBoolean'        | ''    | '{}'           |
      | 'GET'    | 'paramOffsetDateTime' | ''    | '{}'           |
      | 'HEAD'   | 'paramOffsetDateTime' | ''    | '{}'           |
      | 'POST'   | 'paramOffsetDateTime' | ''    | '{}'           |
      | 'PUT'    | 'paramOffsetDateTime' | ''    | '{}'           |
      | 'PATCH'  | 'paramOffsetDateTime' | ''    | '{}'           |
      | 'DELETE' | 'paramOffsetDateTime' | ''    | '{}'           |
      | 'GET'    | 'paramLocalDateTime'  | ''    | '{}'           |
      | 'HEAD'   | 'paramLocalDateTime'  | ''    | '{}'           |
      | 'POST'   | 'paramLocalDateTime'  | ''    | '{}'           |
      | 'PUT'    | 'paramLocalDateTime'  | ''    | '{}'           |
      | 'PATCH'  | 'paramLocalDateTime'  | ''    | '{}'           |
      | 'DELETE' | 'paramLocalDateTime'  | ''    | '{}'           |
      | 'GET'    | 'paramLocalDate'      | ''    | '{}'           |
      | 'HEAD'   | 'paramLocalDate'      | ''    | '{}'           |
      | 'POST'   | 'paramLocalDate'      | ''    | '{}'           |
      | 'PUT'    | 'paramLocalDate'      | ''    | '{}'           |
      | 'PATCH'  | 'paramLocalDate'      | ''    | '{}'           |
      | 'DELETE' | 'paramLocalDate'      | ''    | '{}'           |
      | 'GET'    | 'paramLocalTime'      | ''    | '{}'           |
      | 'HEAD'   | 'paramLocalTime'      | ''    | '{}'           |
      | 'POST'   | 'paramLocalTime'      | ''    | '{}'           |
      | 'PUT'    | 'paramLocalTime'      | ''    | '{}'           |
      | 'PATCH'  | 'paramLocalTime'      | ''    | '{}'           |
      | 'DELETE' | 'paramLocalTime'      | ''    | '{}'           |
      | 'GET'    | 'paramYear'           | ''    | '{}'           |
      | 'HEAD'   | 'paramYear'           | ''    | '{}'           |
      | 'POST'   | 'paramYear'           | ''    | '{}'           |
      | 'PUT'    | 'paramYear'           | ''    | '{}'           |
      | 'PATCH'  | 'paramYear'           | ''    | '{}'           |
      | 'DELETE' | 'paramYear'           | ''    | '{}'           |
      | 'GET'    | 'paramEnumAB'         | ''    | '{}'           |
      | 'HEAD'   | 'paramEnumAB'         | ''    | '{}'           |
      | 'POST'   | 'paramEnumAB'         | ''    | '{}'           |
      | 'PUT'    | 'paramEnumAB'         | ''    | '{}'           |
      | 'PATCH'  | 'paramEnumAB'         | ''    | '{}'           |
      | 'DELETE' | 'paramEnumAB'         | ''    | '{}'           |
      | 'GET'    | 'paramEnumLowerAB'    | ''    | '{}'           |
      | 'HEAD'   | 'paramEnumLowerAB'    | ''    | '{}'           |
      | 'POST'   | 'paramEnumLowerAB'    | ''    | '{}'           |
      | 'PUT'    | 'paramEnumLowerAB'    | ''    | '{}'           |
      | 'PATCH'  | 'paramEnumLowerAB'    | ''    | '{}'           |
      | 'DELETE' | 'paramEnumLowerAB'    | ''    | '{}'           |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/named-method-parameter?<param>=<value> endpoint (RequestParamUnconstrainedRestApi.class)
    this endpoint receives the request parameter in a method parameter, both having the diferent names, a custom request-parameter name is defined by the endpoint 
    and the service correctly maps the sent empty value with the declared data type by setting it to null, responding with a success message.

    Given the endpoint '/api/v1/request-parameter/named-method-parameter'
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | param                    | value | received value |
      | 'GET'    | 'param-string'           | ''    | '{}'           |
      | 'HEAD'   | 'param-string'           | ''    | '{}'           |
      | 'POST'   | 'param-string'           | ''    | '{}'           |
      | 'PUT'    | 'param-string'           | ''    | '{}'           |
      | 'PATCH'  | 'param-string'           | ''    | '{}'           |
      | 'DELETE' | 'param-string'           | ''    | '{}'           |
      | 'GET'    | 'param-integer'          | ''    | '{}'           |
      | 'HEAD'   | 'param-integer'          | ''    | '{}'           |
      | 'POST'   | 'param-integer'          | ''    | '{}'           |
      | 'PUT'    | 'param-integer'          | ''    | '{}'           |
      | 'PATCH'  | 'param-integer'          | ''    | '{}'           |
      | 'DELETE' | 'param-integer'          | ''    | '{}'           |
      | 'GET'    | 'param-long'             | ''    | '{}'           |
      | 'HEAD'   | 'param-long'             | ''    | '{}'           |
      | 'POST'   | 'param-long'             | ''    | '{}'           |
      | 'PUT'    | 'param-long'             | ''    | '{}'           |
      | 'PATCH'  | 'param-long'             | ''    | '{}'           |
      | 'DELETE' | 'param-long'             | ''    | '{}'           |
      | 'GET'    | 'param-big-integer'      | ''    | '{}'           |
      | 'HEAD'   | 'param-big-integer'      | ''    | '{}'           |
      | 'POST'   | 'param-big-integer'      | ''    | '{}'           |
      | 'PUT'    | 'param-big-integer'      | ''    | '{}'           |
      | 'PATCH'  | 'param-big-integer'      | ''    | '{}'           |
      | 'DELETE' | 'param-big-integer'      | ''    | '{}'           |
      | 'GET'    | 'param-float'            | ''    | '{}'           |
      | 'HEAD'   | 'param-float'            | ''    | '{}'           |
      | 'POST'   | 'param-float'            | ''    | '{}'           |
      | 'PUT'    | 'param-float'            | ''    | '{}'           |
      | 'PATCH'  | 'param-float'            | ''    | '{}'           |
      | 'DELETE' | 'param-float'            | ''    | '{}'           |
      | 'GET'    | 'param-double'           | ''    | '{}'           |
      | 'HEAD'   | 'param-double'           | ''    | '{}'           |
      | 'POST'   | 'param-double'           | ''    | '{}'           |
      | 'PUT'    | 'param-double'           | ''    | '{}'           |
      | 'PATCH'  | 'param-double'           | ''    | '{}'           |
      | 'DELETE' | 'param-double'           | ''    | '{}'           |
      | 'GET'    | 'param-big-decimal'      | ''    | '{}'           |
      | 'HEAD'   | 'param-big-decimal'      | ''    | '{}'           |
      | 'POST'   | 'param-big-decimal'      | ''    | '{}'           |
      | 'PUT'    | 'param-big-decimal'      | ''    | '{}'           |
      | 'PATCH'  | 'param-big-decimal'      | ''    | '{}'           |
      | 'DELETE' | 'param-big-decimal'      | ''    | '{}'           |
      | 'GET'    | 'param-boolean'          | ''    | '{}'           |
      | 'HEAD'   | 'param-boolean'          | ''    | '{}'           |
      | 'POST'   | 'param-boolean'          | ''    | '{}'           |
      | 'PUT'    | 'param-boolean'          | ''    | '{}'           |
      | 'PATCH'  | 'param-boolean'          | ''    | '{}'           |
      | 'DELETE' | 'param-boolean'          | ''    | '{}'           |
      | 'GET'    | 'param-offset-date-time' | ''    | '{}'           |
      | 'HEAD'   | 'param-offset-date-time' | ''    | '{}'           |
      | 'POST'   | 'param-offset-date-time' | ''    | '{}'           |
      | 'PUT'    | 'param-offset-date-time' | ''    | '{}'           |
      | 'PATCH'  | 'param-offset-date-time' | ''    | '{}'           |
      | 'DELETE' | 'param-offset-date-time' | ''    | '{}'           |
      | 'GET'    | 'param-local-date-time'  | ''    | '{}'           |
      | 'HEAD'   | 'param-local-date-time'  | ''    | '{}'           |
      | 'POST'   | 'param-local-date-time'  | ''    | '{}'           |
      | 'PUT'    | 'param-local-date-time'  | ''    | '{}'           |
      | 'PATCH'  | 'param-local-date-time'  | ''    | '{}'           |
      | 'DELETE' | 'param-local-date-time'  | ''    | '{}'           |
      | 'GET'    | 'param-local-date'       | ''    | '{}'           |
      | 'HEAD'   | 'param-local-date'       | ''    | '{}'           |
      | 'POST'   | 'param-local-date'       | ''    | '{}'           |
      | 'PUT'    | 'param-local-date'       | ''    | '{}'           |
      | 'PATCH'  | 'param-local-date'       | ''    | '{}'           |
      | 'DELETE' | 'param-local-date'       | ''    | '{}'           |
      | 'GET'    | 'param-local-time'       | ''    | '{}'           |
      | 'HEAD'   | 'param-local-time'       | ''    | '{}'           |
      | 'POST'   | 'param-local-time'       | ''    | '{}'           |
      | 'PUT'    | 'param-local-time'       | ''    | '{}'           |
      | 'PATCH'  | 'param-local-time'       | ''    | '{}'           |
      | 'DELETE' | 'param-local-time'       | ''    | '{}'           |
      | 'GET'    | 'param-year'             | ''    | '{}'           |
      | 'HEAD'   | 'param-year'             | ''    | '{}'           |
      | 'POST'   | 'param-year'             | ''    | '{}'           |
      | 'PUT'    | 'param-year'             | ''    | '{}'           |
      | 'PATCH'  | 'param-year'             | ''    | '{}'           |
      | 'DELETE' | 'param-year'             | ''    | '{}'           |
      | 'GET'    | 'param-enum-ab'          | ''    | '{}'           |
      | 'HEAD'   | 'param-enum-ab'          | ''    | '{}'           |
      | 'POST'   | 'param-enum-ab'          | ''    | '{}'           |
      | 'PUT'    | 'param-enum-ab'          | ''    | '{}'           |
      | 'PATCH'  | 'param-enum-ab'          | ''    | '{}'           |
      | 'DELETE' | 'param-enum-ab'          | ''    | '{}'           |
      | 'GET'    | 'param-enum-lower-ab'    | ''    | '{}'           |
      | 'HEAD'   | 'param-enum-lower-ab'    | ''    | '{}'           |
      | 'POST'   | 'param-enum-lower-ab'    | ''    | '{}'           |
      | 'PUT'    | 'param-enum-lower-ab'    | ''    | '{}'           |
      | 'PATCH'  | 'param-enum-lower-ab'    | ''    | '{}'           |
      | 'DELETE' | 'param-enum-lower-ab'    | ''    | '{}'           |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/optional-method-parameter?<param>=<value> endpoint (RequestParamUnconstrainedRestApi.class)
    this endpoint receives the request parameter in an optional method parameter, both having the same name
    and the service correctly maps the sent empty value with the declared data type as an optional-empty, responding with a success message.

    Given the endpoint '/api/v1/request-parameter/optional-method-parameter'
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | param                 | value | received value |
      | 'GET'    | 'paramString'         | ''    | '{}'           |
      | 'HEAD'   | 'paramString'         | ''    | '{}'           |
      | 'POST'   | 'paramString'         | ''    | '{}'           |
      | 'PUT'    | 'paramString'         | ''    | '{}'           |
      | 'PATCH'  | 'paramString'         | ''    | '{}'           |
      | 'DELETE' | 'paramString'         | ''    | '{}'           |
      | 'GET'    | 'paramInteger'        | ''    | '{}'           |
      | 'HEAD'   | 'paramInteger'        | ''    | '{}'           |
      | 'POST'   | 'paramInteger'        | ''    | '{}'           |
      | 'PUT'    | 'paramInteger'        | ''    | '{}'           |
      | 'PATCH'  | 'paramInteger'        | ''    | '{}'           |
      | 'DELETE' | 'paramInteger'        | ''    | '{}'           |
      | 'GET'    | 'paramLong'           | ''    | '{}'           |
      | 'HEAD'   | 'paramLong'           | ''    | '{}'           |
      | 'POST'   | 'paramLong'           | ''    | '{}'           |
      | 'PUT'    | 'paramLong'           | ''    | '{}'           |
      | 'PATCH'  | 'paramLong'           | ''    | '{}'           |
      | 'DELETE' | 'paramLong'           | ''    | '{}'           |
      | 'GET'    | 'paramBigInteger'     | ''    | '{}'           |
      | 'HEAD'   | 'paramBigInteger'     | ''    | '{}'           |
      | 'POST'   | 'paramBigInteger'     | ''    | '{}'           |
      | 'PUT'    | 'paramBigInteger'     | ''    | '{}'           |
      | 'PATCH'  | 'paramBigInteger'     | ''    | '{}'           |
      | 'DELETE' | 'paramBigInteger'     | ''    | '{}'           |
      | 'GET'    | 'paramFloat'          | ''    | '{}'           |
      | 'HEAD'   | 'paramFloat'          | ''    | '{}'           |
      | 'POST'   | 'paramFloat'          | ''    | '{}'           |
      | 'PUT'    | 'paramFloat'          | ''    | '{}'           |
      | 'PATCH'  | 'paramFloat'          | ''    | '{}'           |
      | 'DELETE' | 'paramFloat'          | ''    | '{}'           |
      | 'GET'    | 'paramDouble'         | ''    | '{}'           |
      | 'HEAD'   | 'paramDouble'         | ''    | '{}'           |
      | 'POST'   | 'paramDouble'         | ''    | '{}'           |
      | 'PUT'    | 'paramDouble'         | ''    | '{}'           |
      | 'PATCH'  | 'paramDouble'         | ''    | '{}'           |
      | 'DELETE' | 'paramDouble'         | ''    | '{}'           |
      | 'GET'    | 'paramBigDecimal'     | ''    | '{}'           |
      | 'HEAD'   | 'paramBigDecimal'     | ''    | '{}'           |
      | 'POST'   | 'paramBigDecimal'     | ''    | '{}'           |
      | 'PUT'    | 'paramBigDecimal'     | ''    | '{}'           |
      | 'PATCH'  | 'paramBigDecimal'     | ''    | '{}'           |
      | 'DELETE' | 'paramBigDecimal'     | ''    | '{}'           |
      | 'GET'    | 'paramBoolean'        | ''    | '{}'           |
      | 'HEAD'   | 'paramBoolean'        | ''    | '{}'           |
      | 'POST'   | 'paramBoolean'        | ''    | '{}'           |
      | 'PUT'    | 'paramBoolean'        | ''    | '{}'           |
      | 'PATCH'  | 'paramBoolean'        | ''    | '{}'           |
      | 'DELETE' | 'paramBoolean'        | ''    | '{}'           |
      | 'GET'    | 'paramOffsetDateTime' | ''    | '{}'           |
      | 'HEAD'   | 'paramOffsetDateTime' | ''    | '{}'           |
      | 'POST'   | 'paramOffsetDateTime' | ''    | '{}'           |
      | 'PUT'    | 'paramOffsetDateTime' | ''    | '{}'           |
      | 'PATCH'  | 'paramOffsetDateTime' | ''    | '{}'           |
      | 'DELETE' | 'paramOffsetDateTime' | ''    | '{}'           |
      | 'GET'    | 'paramLocalDateTime'  | ''    | '{}'           |
      | 'HEAD'   | 'paramLocalDateTime'  | ''    | '{}'           |
      | 'POST'   | 'paramLocalDateTime'  | ''    | '{}'           |
      | 'PUT'    | 'paramLocalDateTime'  | ''    | '{}'           |
      | 'PATCH'  | 'paramLocalDateTime'  | ''    | '{}'           |
      | 'DELETE' | 'paramLocalDateTime'  | ''    | '{}'           |
      | 'GET'    | 'paramLocalDate'      | ''    | '{}'           |
      | 'HEAD'   | 'paramLocalDate'      | ''    | '{}'           |
      | 'POST'   | 'paramLocalDate'      | ''    | '{}'           |
      | 'PUT'    | 'paramLocalDate'      | ''    | '{}'           |
      | 'PATCH'  | 'paramLocalDate'      | ''    | '{}'           |
      | 'DELETE' | 'paramLocalDate'      | ''    | '{}'           |
      | 'GET'    | 'paramLocalTime'      | ''    | '{}'           |
      | 'HEAD'   | 'paramLocalTime'      | ''    | '{}'           |
      | 'POST'   | 'paramLocalTime'      | ''    | '{}'           |
      | 'PUT'    | 'paramLocalTime'      | ''    | '{}'           |
      | 'PATCH'  | 'paramLocalTime'      | ''    | '{}'           |
      | 'DELETE' | 'paramLocalTime'      | ''    | '{}'           |
      | 'GET'    | 'paramYear'           | ''    | '{}'           |
      | 'HEAD'   | 'paramYear'           | ''    | '{}'           |
      | 'POST'   | 'paramYear'           | ''    | '{}'           |
      | 'PUT'    | 'paramYear'           | ''    | '{}'           |
      | 'PATCH'  | 'paramYear'           | ''    | '{}'           |
      | 'DELETE' | 'paramYear'           | ''    | '{}'           |
      | 'GET'    | 'paramEnumAB'         | ''    | '{}'           |
      | 'HEAD'   | 'paramEnumAB'         | ''    | '{}'           |
      | 'POST'   | 'paramEnumAB'         | ''    | '{}'           |
      | 'PUT'    | 'paramEnumAB'         | ''    | '{}'           |
      | 'PATCH'  | 'paramEnumAB'         | ''    | '{}'           |
      | 'DELETE' | 'paramEnumAB'         | ''    | '{}'           |
      | 'GET'    | 'paramEnumLowerAB'    | ''    | '{}'           |
      | 'HEAD'   | 'paramEnumLowerAB'    | ''    | '{}'           |
      | 'POST'   | 'paramEnumLowerAB'    | ''    | '{}'           |
      | 'PUT'    | 'paramEnumLowerAB'    | ''    | '{}'           |
      | 'PATCH'  | 'paramEnumLowerAB'    | ''    | '{}'           |
      | 'DELETE' | 'paramEnumLowerAB'    | ''    | '{}'           |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/named-optional-method-parameter?<param>=<value> endpoint (RequestParamUnconstrainedRestApi.class)
    this endpoint receives the request parameter in an optional method parameter, both having the diferent names, a custom request-parameter name is defined by the endpoint 
    and the service correctly maps the sent empty value with the declared data type as an optional-empty, responding with a success message.

    Given the endpoint '/api/v1/request-parameter/named-optional-method-parameter'
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | param                    | value | received value |
      | 'GET'    | 'param-string'           | ''    | '{}'           |
      | 'HEAD'   | 'param-string'           | ''    | '{}'           |
      | 'POST'   | 'param-string'           | ''    | '{}'           |
      | 'PUT'    | 'param-string'           | ''    | '{}'           |
      | 'PATCH'  | 'param-string'           | ''    | '{}'           |
      | 'DELETE' | 'param-string'           | ''    | '{}'           |
      | 'GET'    | 'param-integer'          | ''    | '{}'           |
      | 'HEAD'   | 'param-integer'          | ''    | '{}'           |
      | 'POST'   | 'param-integer'          | ''    | '{}'           |
      | 'PUT'    | 'param-integer'          | ''    | '{}'           |
      | 'PATCH'  | 'param-integer'          | ''    | '{}'           |
      | 'DELETE' | 'param-integer'          | ''    | '{}'           |
      | 'GET'    | 'param-long'             | ''    | '{}'           |
      | 'HEAD'   | 'param-long'             | ''    | '{}'           |
      | 'POST'   | 'param-long'             | ''    | '{}'           |
      | 'PUT'    | 'param-long'             | ''    | '{}'           |
      | 'PATCH'  | 'param-long'             | ''    | '{}'           |
      | 'DELETE' | 'param-long'             | ''    | '{}'           |
      | 'GET'    | 'param-big-integer'      | ''    | '{}'           |
      | 'HEAD'   | 'param-big-integer'      | ''    | '{}'           |
      | 'POST'   | 'param-big-integer'      | ''    | '{}'           |
      | 'PUT'    | 'param-big-integer'      | ''    | '{}'           |
      | 'PATCH'  | 'param-big-integer'      | ''    | '{}'           |
      | 'DELETE' | 'param-big-integer'      | ''    | '{}'           |
      | 'GET'    | 'param-float'            | ''    | '{}'           |
      | 'HEAD'   | 'param-float'            | ''    | '{}'           |
      | 'POST'   | 'param-float'            | ''    | '{}'           |
      | 'PUT'    | 'param-float'            | ''    | '{}'           |
      | 'PATCH'  | 'param-float'            | ''    | '{}'           |
      | 'DELETE' | 'param-float'            | ''    | '{}'           |
      | 'GET'    | 'param-double'           | ''    | '{}'           |
      | 'HEAD'   | 'param-double'           | ''    | '{}'           |
      | 'POST'   | 'param-double'           | ''    | '{}'           |
      | 'PUT'    | 'param-double'           | ''    | '{}'           |
      | 'PATCH'  | 'param-double'           | ''    | '{}'           |
      | 'DELETE' | 'param-double'           | ''    | '{}'           |
      | 'GET'    | 'param-big-decimal'      | ''    | '{}'           |
      | 'HEAD'   | 'param-big-decimal'      | ''    | '{}'           |
      | 'POST'   | 'param-big-decimal'      | ''    | '{}'           |
      | 'PUT'    | 'param-big-decimal'      | ''    | '{}'           |
      | 'PATCH'  | 'param-big-decimal'      | ''    | '{}'           |
      | 'DELETE' | 'param-big-decimal'      | ''    | '{}'           |
      | 'GET'    | 'param-boolean'          | ''    | '{}'           |
      | 'HEAD'   | 'param-boolean'          | ''    | '{}'           |
      | 'POST'   | 'param-boolean'          | ''    | '{}'           |
      | 'PUT'    | 'param-boolean'          | ''    | '{}'           |
      | 'PATCH'  | 'param-boolean'          | ''    | '{}'           |
      | 'DELETE' | 'param-boolean'          | ''    | '{}'           |
      | 'GET'    | 'param-offset-date-time' | ''    | '{}'           |
      | 'HEAD'   | 'param-offset-date-time' | ''    | '{}'           |
      | 'POST'   | 'param-offset-date-time' | ''    | '{}'           |
      | 'PUT'    | 'param-offset-date-time' | ''    | '{}'           |
      | 'PATCH'  | 'param-offset-date-time' | ''    | '{}'           |
      | 'DELETE' | 'param-offset-date-time' | ''    | '{}'           |
      | 'GET'    | 'param-local-date-time'  | ''    | '{}'           |
      | 'HEAD'   | 'param-local-date-time'  | ''    | '{}'           |
      | 'POST'   | 'param-local-date-time'  | ''    | '{}'           |
      | 'PUT'    | 'param-local-date-time'  | ''    | '{}'           |
      | 'PATCH'  | 'param-local-date-time'  | ''    | '{}'           |
      | 'DELETE' | 'param-local-date-time'  | ''    | '{}'           |
      | 'GET'    | 'param-local-date'       | ''    | '{}'           |
      | 'HEAD'   | 'param-local-date'       | ''    | '{}'           |
      | 'POST'   | 'param-local-date'       | ''    | '{}'           |
      | 'PUT'    | 'param-local-date'       | ''    | '{}'           |
      | 'PATCH'  | 'param-local-date'       | ''    | '{}'           |
      | 'DELETE' | 'param-local-date'       | ''    | '{}'           |
      | 'GET'    | 'param-local-time'       | ''    | '{}'           |
      | 'HEAD'   | 'param-local-time'       | ''    | '{}'           |
      | 'POST'   | 'param-local-time'       | ''    | '{}'           |
      | 'PUT'    | 'param-local-time'       | ''    | '{}'           |
      | 'PATCH'  | 'param-local-time'       | ''    | '{}'           |
      | 'DELETE' | 'param-local-time'       | ''    | '{}'           |
      | 'GET'    | 'param-year'             | ''    | '{}'           |
      | 'HEAD'   | 'param-year'             | ''    | '{}'           |
      | 'POST'   | 'param-year'             | ''    | '{}'           |
      | 'PUT'    | 'param-year'             | ''    | '{}'           |
      | 'PATCH'  | 'param-year'             | ''    | '{}'           |
      | 'DELETE' | 'param-year'             | ''    | '{}'           |
      | 'GET'    | 'param-enum-ab'          | ''    | '{}'           |
      | 'HEAD'   | 'param-enum-ab'          | ''    | '{}'           |
      | 'POST'   | 'param-enum-ab'          | ''    | '{}'           |
      | 'PUT'    | 'param-enum-ab'          | ''    | '{}'           |
      | 'PATCH'  | 'param-enum-ab'          | ''    | '{}'           |
      | 'DELETE' | 'param-enum-ab'          | ''    | '{}'           |
      | 'GET'    | 'param-enum-lower-ab'    | ''    | '{}'           |
      | 'HEAD'   | 'param-enum-lower-ab'    | ''    | '{}'           |
      | 'POST'   | 'param-enum-lower-ab'    | ''    | '{}'           |
      | 'PUT'    | 'param-enum-lower-ab'    | ''    | '{}'           |
      | 'PATCH'  | 'param-enum-lower-ab'    | ''    | '{}'           |
      | 'DELETE' | 'param-enum-lower-ab'    | ''    | '{}'           |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/dto-attribute?<param>=<value> endpoint (RequestParamUnconstrainedRestApi.class)
    this endpoint receives the request parameter in a dto attribute, both having the same name
    and the service correctly maps the sent empty value with the declared data type by setting it to null, responding with a success message.

    Given the endpoint '/api/v1/request-parameter/dto-attribute'
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | param                 | value | received value |
      | 'GET'    | 'paramString'         | ''    | '{}'           |
      | 'HEAD'   | 'paramString'         | ''    | '{}'           |
      | 'POST'   | 'paramString'         | ''    | '{}'           |
      | 'PUT'    | 'paramString'         | ''    | '{}'           |
      | 'PATCH'  | 'paramString'         | ''    | '{}'           |
      | 'DELETE' | 'paramString'         | ''    | '{}'           |
      | 'GET'    | 'paramInteger'        | ''    | '{}'           |
      | 'HEAD'   | 'paramInteger'        | ''    | '{}'           |
      | 'POST'   | 'paramInteger'        | ''    | '{}'           |
      | 'PUT'    | 'paramInteger'        | ''    | '{}'           |
      | 'PATCH'  | 'paramInteger'        | ''    | '{}'           |
      | 'DELETE' | 'paramInteger'        | ''    | '{}'           |
      | 'GET'    | 'paramLong'           | ''    | '{}'           |
      | 'HEAD'   | 'paramLong'           | ''    | '{}'           |
      | 'POST'   | 'paramLong'           | ''    | '{}'           |
      | 'PUT'    | 'paramLong'           | ''    | '{}'           |
      | 'PATCH'  | 'paramLong'           | ''    | '{}'           |
      | 'DELETE' | 'paramLong'           | ''    | '{}'           |
      | 'GET'    | 'paramBigInteger'     | ''    | '{}'           |
      | 'HEAD'   | 'paramBigInteger'     | ''    | '{}'           |
      | 'POST'   | 'paramBigInteger'     | ''    | '{}'           |
      | 'PUT'    | 'paramBigInteger'     | ''    | '{}'           |
      | 'PATCH'  | 'paramBigInteger'     | ''    | '{}'           |
      | 'DELETE' | 'paramBigInteger'     | ''    | '{}'           |
      | 'GET'    | 'paramFloat'          | ''    | '{}'           |
      | 'HEAD'   | 'paramFloat'          | ''    | '{}'           |
      | 'POST'   | 'paramFloat'          | ''    | '{}'           |
      | 'PUT'    | 'paramFloat'          | ''    | '{}'           |
      | 'PATCH'  | 'paramFloat'          | ''    | '{}'           |
      | 'DELETE' | 'paramFloat'          | ''    | '{}'           |
      | 'GET'    | 'paramDouble'         | ''    | '{}'           |
      | 'HEAD'   | 'paramDouble'         | ''    | '{}'           |
      | 'POST'   | 'paramDouble'         | ''    | '{}'           |
      | 'PUT'    | 'paramDouble'         | ''    | '{}'           |
      | 'PATCH'  | 'paramDouble'         | ''    | '{}'           |
      | 'DELETE' | 'paramDouble'         | ''    | '{}'           |
      | 'GET'    | 'paramBigDecimal'     | ''    | '{}'           |
      | 'HEAD'   | 'paramBigDecimal'     | ''    | '{}'           |
      | 'POST'   | 'paramBigDecimal'     | ''    | '{}'           |
      | 'PUT'    | 'paramBigDecimal'     | ''    | '{}'           |
      | 'PATCH'  | 'paramBigDecimal'     | ''    | '{}'           |
      | 'DELETE' | 'paramBigDecimal'     | ''    | '{}'           |
      | 'GET'    | 'paramBoolean'        | ''    | '{}'           |
      | 'HEAD'   | 'paramBoolean'        | ''    | '{}'           |
      | 'POST'   | 'paramBoolean'        | ''    | '{}'           |
      | 'PUT'    | 'paramBoolean'        | ''    | '{}'           |
      | 'PATCH'  | 'paramBoolean'        | ''    | '{}'           |
      | 'DELETE' | 'paramBoolean'        | ''    | '{}'           |
      | 'GET'    | 'paramOffsetDateTime' | ''    | '{}'           |
      | 'HEAD'   | 'paramOffsetDateTime' | ''    | '{}'           |
      | 'POST'   | 'paramOffsetDateTime' | ''    | '{}'           |
      | 'PUT'    | 'paramOffsetDateTime' | ''    | '{}'           |
      | 'PATCH'  | 'paramOffsetDateTime' | ''    | '{}'           |
      | 'DELETE' | 'paramOffsetDateTime' | ''    | '{}'           |
      | 'GET'    | 'paramLocalDateTime'  | ''    | '{}'           |
      | 'HEAD'   | 'paramLocalDateTime'  | ''    | '{}'           |
      | 'POST'   | 'paramLocalDateTime'  | ''    | '{}'           |
      | 'PUT'    | 'paramLocalDateTime'  | ''    | '{}'           |
      | 'PATCH'  | 'paramLocalDateTime'  | ''    | '{}'           |
      | 'DELETE' | 'paramLocalDateTime'  | ''    | '{}'           |
      | 'GET'    | 'paramLocalDate'      | ''    | '{}'           |
      | 'HEAD'   | 'paramLocalDate'      | ''    | '{}'           |
      | 'POST'   | 'paramLocalDate'      | ''    | '{}'           |
      | 'PUT'    | 'paramLocalDate'      | ''    | '{}'           |
      | 'PATCH'  | 'paramLocalDate'      | ''    | '{}'           |
      | 'DELETE' | 'paramLocalDate'      | ''    | '{}'           |
      | 'GET'    | 'paramLocalTime'      | ''    | '{}'           |
      | 'HEAD'   | 'paramLocalTime'      | ''    | '{}'           |
      | 'POST'   | 'paramLocalTime'      | ''    | '{}'           |
      | 'PUT'    | 'paramLocalTime'      | ''    | '{}'           |
      | 'PATCH'  | 'paramLocalTime'      | ''    | '{}'           |
      | 'DELETE' | 'paramLocalTime'      | ''    | '{}'           |
      | 'GET'    | 'paramYear'           | ''    | '{}'           |
      | 'HEAD'   | 'paramYear'           | ''    | '{}'           |
      | 'POST'   | 'paramYear'           | ''    | '{}'           |
      | 'PUT'    | 'paramYear'           | ''    | '{}'           |
      | 'PATCH'  | 'paramYear'           | ''    | '{}'           |
      | 'DELETE' | 'paramYear'           | ''    | '{}'           |
      | 'GET'    | 'paramEnumAB'         | ''    | '{}'           |
      | 'HEAD'   | 'paramEnumAB'         | ''    | '{}'           |
      | 'POST'   | 'paramEnumAB'         | ''    | '{}'           |
      | 'PUT'    | 'paramEnumAB'         | ''    | '{}'           |
      | 'PATCH'  | 'paramEnumAB'         | ''    | '{}'           |
      | 'DELETE' | 'paramEnumAB'         | ''    | '{}'           |
      | 'GET'    | 'paramEnumLowerAB'    | ''    | '{}'           |
      | 'HEAD'   | 'paramEnumLowerAB'    | ''    | '{}'           |
      | 'POST'   | 'paramEnumLowerAB'    | ''    | '{}'           |
      | 'PUT'    | 'paramEnumLowerAB'    | ''    | '{}'           |
      | 'PATCH'  | 'paramEnumLowerAB'    | ''    | '{}'           |
      | 'DELETE' | 'paramEnumLowerAB'    | ''    | '{}'           |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/optional-dto-attribute?<param>=<value> endpoint (RequestParamUnconstrainedRestApi.class)
    this endpoint receives the request parameter in an optional dto attribute, both having the same name
      and the service correctly maps the sent empty value with the declared data type as an optional-empty, responding with a success message.

    Given the endpoint '/api/v1/request-parameter/optional-dto-attribute'
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | param                 | value | received value                 |
      | 'GET'    | 'paramString'         | ''    | '{"paramString":null}'         |
      | 'HEAD'   | 'paramString'         | ''    | '{"paramString":null}'         |
      | 'POST'   | 'paramString'         | ''    | '{"paramString":null}'         |
      | 'PUT'    | 'paramString'         | ''    | '{"paramString":null}'         |
      | 'PATCH'  | 'paramString'         | ''    | '{"paramString":null}'         |
      | 'DELETE' | 'paramString'         | ''    | '{"paramString":null}'         |
      | 'GET'    | 'paramInteger'        | ''    | '{"paramInteger":null}'        |
      | 'HEAD'   | 'paramInteger'        | ''    | '{"paramInteger":null}'        |
      | 'POST'   | 'paramInteger'        | ''    | '{"paramInteger":null}'        |
      | 'PUT'    | 'paramInteger'        | ''    | '{"paramInteger":null}'        |
      | 'PATCH'  | 'paramInteger'        | ''    | '{"paramInteger":null}'        |
      | 'DELETE' | 'paramInteger'        | ''    | '{"paramInteger":null}'        |
      | 'GET'    | 'paramLong'           | ''    | '{"paramLong":null}'           |
      | 'HEAD'   | 'paramLong'           | ''    | '{"paramLong":null}'           |
      | 'POST'   | 'paramLong'           | ''    | '{"paramLong":null}'           |
      | 'PUT'    | 'paramLong'           | ''    | '{"paramLong":null}'           |
      | 'PATCH'  | 'paramLong'           | ''    | '{"paramLong":null}'           |
      | 'DELETE' | 'paramLong'           | ''    | '{"paramLong":null}'           |
      | 'GET'    | 'paramBigInteger'     | ''    | '{"paramBigInteger":null}'     |
      | 'HEAD'   | 'paramBigInteger'     | ''    | '{"paramBigInteger":null}'     |
      | 'POST'   | 'paramBigInteger'     | ''    | '{"paramBigInteger":null}'     |
      | 'PUT'    | 'paramBigInteger'     | ''    | '{"paramBigInteger":null}'     |
      | 'PATCH'  | 'paramBigInteger'     | ''    | '{"paramBigInteger":null}'     |
      | 'DELETE' | 'paramBigInteger'     | ''    | '{"paramBigInteger":null}'     |
      | 'GET'    | 'paramFloat'          | ''    | '{"paramFloat":null}'          |
      | 'HEAD'   | 'paramFloat'          | ''    | '{"paramFloat":null}'          |
      | 'POST'   | 'paramFloat'          | ''    | '{"paramFloat":null}'          |
      | 'PUT'    | 'paramFloat'          | ''    | '{"paramFloat":null}'          |
      | 'PATCH'  | 'paramFloat'          | ''    | '{"paramFloat":null}'          |
      | 'DELETE' | 'paramFloat'          | ''    | '{"paramFloat":null}'          |
      | 'GET'    | 'paramDouble'         | ''    | '{"paramDouble":null}'         |
      | 'HEAD'   | 'paramDouble'         | ''    | '{"paramDouble":null}'         |
      | 'POST'   | 'paramDouble'         | ''    | '{"paramDouble":null}'         |
      | 'PUT'    | 'paramDouble'         | ''    | '{"paramDouble":null}'         |
      | 'PATCH'  | 'paramDouble'         | ''    | '{"paramDouble":null}'         |
      | 'DELETE' | 'paramDouble'         | ''    | '{"paramDouble":null}'         |
      | 'GET'    | 'paramBigDecimal'     | ''    | '{"paramBigDecimal":null}'     |
      | 'HEAD'   | 'paramBigDecimal'     | ''    | '{"paramBigDecimal":null}'     |
      | 'POST'   | 'paramBigDecimal'     | ''    | '{"paramBigDecimal":null}'     |
      | 'PUT'    | 'paramBigDecimal'     | ''    | '{"paramBigDecimal":null}'     |
      | 'PATCH'  | 'paramBigDecimal'     | ''    | '{"paramBigDecimal":null}'     |
      | 'DELETE' | 'paramBigDecimal'     | ''    | '{"paramBigDecimal":null}'     |
      | 'GET'    | 'paramBoolean'        | ''    | '{"paramBoolean":null}'        |
      | 'HEAD'   | 'paramBoolean'        | ''    | '{"paramBoolean":null}'        |
      | 'POST'   | 'paramBoolean'        | ''    | '{"paramBoolean":null}'        |
      | 'PUT'    | 'paramBoolean'        | ''    | '{"paramBoolean":null}'        |
      | 'PATCH'  | 'paramBoolean'        | ''    | '{"paramBoolean":null}'        |
      | 'DELETE' | 'paramBoolean'        | ''    | '{"paramBoolean":null}'        |
      | 'GET'    | 'paramOffsetDateTime' | ''    | '{"paramOffsetDateTime":null}' |
      | 'HEAD'   | 'paramOffsetDateTime' | ''    | '{"paramOffsetDateTime":null}' |
      | 'POST'   | 'paramOffsetDateTime' | ''    | '{"paramOffsetDateTime":null}' |
      | 'PUT'    | 'paramOffsetDateTime' | ''    | '{"paramOffsetDateTime":null}' |
      | 'PATCH'  | 'paramOffsetDateTime' | ''    | '{"paramOffsetDateTime":null}' |
      | 'DELETE' | 'paramOffsetDateTime' | ''    | '{"paramOffsetDateTime":null}' |
      | 'GET'    | 'paramLocalDateTime'  | ''    | '{"paramLocalDateTime":null}'  |
      | 'HEAD'   | 'paramLocalDateTime'  | ''    | '{"paramLocalDateTime":null}'  |
      | 'POST'   | 'paramLocalDateTime'  | ''    | '{"paramLocalDateTime":null}'  |
      | 'PUT'    | 'paramLocalDateTime'  | ''    | '{"paramLocalDateTime":null}'  |
      | 'PATCH'  | 'paramLocalDateTime'  | ''    | '{"paramLocalDateTime":null}'  |
      | 'DELETE' | 'paramLocalDateTime'  | ''    | '{"paramLocalDateTime":null}'  |
      | 'GET'    | 'paramLocalDate'      | ''    | '{"paramLocalDate":null}'      |
      | 'HEAD'   | 'paramLocalDate'      | ''    | '{"paramLocalDate":null}'      |
      | 'POST'   | 'paramLocalDate'      | ''    | '{"paramLocalDate":null}'      |
      | 'PUT'    | 'paramLocalDate'      | ''    | '{"paramLocalDate":null}'      |
      | 'PATCH'  | 'paramLocalDate'      | ''    | '{"paramLocalDate":null}'      |
      | 'DELETE' | 'paramLocalDate'      | ''    | '{"paramLocalDate":null}'      |
      | 'GET'    | 'paramLocalTime'      | ''    | '{"paramLocalTime":null}'      |
      | 'HEAD'   | 'paramLocalTime'      | ''    | '{"paramLocalTime":null}'      |
      | 'POST'   | 'paramLocalTime'      | ''    | '{"paramLocalTime":null}'      |
      | 'PUT'    | 'paramLocalTime'      | ''    | '{"paramLocalTime":null}'      |
      | 'PATCH'  | 'paramLocalTime'      | ''    | '{"paramLocalTime":null}'      |
      | 'DELETE' | 'paramLocalTime'      | ''    | '{"paramLocalTime":null}'      |
      | 'GET'    | 'paramYear'           | ''    | '{"paramYear":null}'           |
      | 'HEAD'   | 'paramYear'           | ''    | '{"paramYear":null}'           |
      | 'POST'   | 'paramYear'           | ''    | '{"paramYear":null}'           |
      | 'PUT'    | 'paramYear'           | ''    | '{"paramYear":null}'           |
      | 'PATCH'  | 'paramYear'           | ''    | '{"paramYear":null}'           |
      | 'DELETE' | 'paramYear'           | ''    | '{"paramYear":null}'           |
      | 'GET'    | 'paramEnumAB'         | ''    | '{"paramEnumAB":null}'         |
      | 'HEAD'   | 'paramEnumAB'         | ''    | '{"paramEnumAB":null}'         |
      | 'POST'   | 'paramEnumAB'         | ''    | '{"paramEnumAB":null}'         |
      | 'PUT'    | 'paramEnumAB'         | ''    | '{"paramEnumAB":null}'         |
      | 'PATCH'  | 'paramEnumAB'         | ''    | '{"paramEnumAB":null}'         |
      | 'DELETE' | 'paramEnumAB'         | ''    | '{"paramEnumAB":null}'         |
      | 'GET'    | 'paramEnumLowerAB'    | ''    | '{"paramEnumLowerAB":null}'    |
      | 'HEAD'   | 'paramEnumLowerAB'    | ''    | '{"paramEnumLowerAB":null}'    |
      | 'POST'   | 'paramEnumLowerAB'    | ''    | '{"paramEnumLowerAB":null}'    |
      | 'PUT'    | 'paramEnumLowerAB'    | ''    | '{"paramEnumLowerAB":null}'    |
      | 'PATCH'  | 'paramEnumLowerAB'    | ''    | '{"paramEnumLowerAB":null}'    |
      | 'DELETE' | 'paramEnumLowerAB'    | ''    | '{"paramEnumLowerAB":null}'    |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/nested-dto-attribute?<param>=<value> endpoint (RequestParamUnconstrainedRestApi.class)
    this endpoint receives the request parameter in a nested dto attribute, both having the same name
      and the service correctly maps the sent empty value with the declared data type by setting it to null, responding with a success message.

    Given the endpoint '/api/v1/request-parameter/nested-dto-attribute'
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | param                        | value | received value  |
      | 'GET'    | 'nested.paramString'         | ''    | '{"nested":{}}' |
      | 'HEAD'   | 'nested.paramString'         | ''    | '{"nested":{}}' |
      | 'POST'   | 'nested.paramString'         | ''    | '{"nested":{}}' |
      | 'PUT'    | 'nested.paramString'         | ''    | '{"nested":{}}' |
      | 'PATCH'  | 'nested.paramString'         | ''    | '{"nested":{}}' |
      | 'DELETE' | 'nested.paramString'         | ''    | '{"nested":{}}' |
      | 'GET'    | 'nested.paramInteger'        | ''    | '{"nested":{}}' |
      | 'HEAD'   | 'nested.paramInteger'        | ''    | '{"nested":{}}' |
      | 'POST'   | 'nested.paramInteger'        | ''    | '{"nested":{}}' |
      | 'PUT'    | 'nested.paramInteger'        | ''    | '{"nested":{}}' |
      | 'PATCH'  | 'nested.paramInteger'        | ''    | '{"nested":{}}' |
      | 'DELETE' | 'nested.paramInteger'        | ''    | '{"nested":{}}' |
      | 'GET'    | 'nested.paramLong'           | ''    | '{"nested":{}}' |
      | 'HEAD'   | 'nested.paramLong'           | ''    | '{"nested":{}}' |
      | 'POST'   | 'nested.paramLong'           | ''    | '{"nested":{}}' |
      | 'PUT'    | 'nested.paramLong'           | ''    | '{"nested":{}}' |
      | 'PATCH'  | 'nested.paramLong'           | ''    | '{"nested":{}}' |
      | 'DELETE' | 'nested.paramLong'           | ''    | '{"nested":{}}' |
      | 'GET'    | 'nested.paramBigInteger'     | ''    | '{"nested":{}}' |
      | 'HEAD'   | 'nested.paramBigInteger'     | ''    | '{"nested":{}}' |
      | 'POST'   | 'nested.paramBigInteger'     | ''    | '{"nested":{}}' |
      | 'PUT'    | 'nested.paramBigInteger'     | ''    | '{"nested":{}}' |
      | 'PATCH'  | 'nested.paramBigInteger'     | ''    | '{"nested":{}}' |
      | 'DELETE' | 'nested.paramBigInteger'     | ''    | '{"nested":{}}' |
      | 'GET'    | 'nested.paramFloat'          | ''    | '{"nested":{}}' |
      | 'HEAD'   | 'nested.paramFloat'          | ''    | '{"nested":{}}' |
      | 'POST'   | 'nested.paramFloat'          | ''    | '{"nested":{}}' |
      | 'PUT'    | 'nested.paramFloat'          | ''    | '{"nested":{}}' |
      | 'PATCH'  | 'nested.paramFloat'          | ''    | '{"nested":{}}' |
      | 'DELETE' | 'nested.paramFloat'          | ''    | '{"nested":{}}' |
      | 'GET'    | 'nested.paramDouble'         | ''    | '{"nested":{}}' |
      | 'HEAD'   | 'nested.paramDouble'         | ''    | '{"nested":{}}' |
      | 'POST'   | 'nested.paramDouble'         | ''    | '{"nested":{}}' |
      | 'PUT'    | 'nested.paramDouble'         | ''    | '{"nested":{}}' |
      | 'PATCH'  | 'nested.paramDouble'         | ''    | '{"nested":{}}' |
      | 'DELETE' | 'nested.paramDouble'         | ''    | '{"nested":{}}' |
      | 'GET'    | 'nested.paramBigDecimal'     | ''    | '{"nested":{}}' |
      | 'HEAD'   | 'nested.paramBigDecimal'     | ''    | '{"nested":{}}' |
      | 'POST'   | 'nested.paramBigDecimal'     | ''    | '{"nested":{}}' |
      | 'PUT'    | 'nested.paramBigDecimal'     | ''    | '{"nested":{}}' |
      | 'PATCH'  | 'nested.paramBigDecimal'     | ''    | '{"nested":{}}' |
      | 'DELETE' | 'nested.paramBigDecimal'     | ''    | '{"nested":{}}' |
      | 'GET'    | 'nested.paramBoolean'        | ''    | '{"nested":{}}' |
      | 'HEAD'   | 'nested.paramBoolean'        | ''    | '{"nested":{}}' |
      | 'POST'   | 'nested.paramBoolean'        | ''    | '{"nested":{}}' |
      | 'PUT'    | 'nested.paramBoolean'        | ''    | '{"nested":{}}' |
      | 'PATCH'  | 'nested.paramBoolean'        | ''    | '{"nested":{}}' |
      | 'DELETE' | 'nested.paramBoolean'        | ''    | '{"nested":{}}' |
      | 'GET'    | 'nested.paramOffsetDateTime' | ''    | '{"nested":{}}' |
      | 'HEAD'   | 'nested.paramOffsetDateTime' | ''    | '{"nested":{}}' |
      | 'POST'   | 'nested.paramOffsetDateTime' | ''    | '{"nested":{}}' |
      | 'PUT'    | 'nested.paramOffsetDateTime' | ''    | '{"nested":{}}' |
      | 'PATCH'  | 'nested.paramOffsetDateTime' | ''    | '{"nested":{}}' |
      | 'DELETE' | 'nested.paramOffsetDateTime' | ''    | '{"nested":{}}' |
      | 'GET'    | 'nested.paramLocalDateTime'  | ''    | '{"nested":{}}' |
      | 'HEAD'   | 'nested.paramLocalDateTime'  | ''    | '{"nested":{}}' |
      | 'POST'   | 'nested.paramLocalDateTime'  | ''    | '{"nested":{}}' |
      | 'PUT'    | 'nested.paramLocalDateTime'  | ''    | '{"nested":{}}' |
      | 'PATCH'  | 'nested.paramLocalDateTime'  | ''    | '{"nested":{}}' |
      | 'DELETE' | 'nested.paramLocalDateTime'  | ''    | '{"nested":{}}' |
      | 'GET'    | 'nested.paramLocalDate'      | ''    | '{"nested":{}}' |
      | 'HEAD'   | 'nested.paramLocalDate'      | ''    | '{"nested":{}}' |
      | 'POST'   | 'nested.paramLocalDate'      | ''    | '{"nested":{}}' |
      | 'PUT'    | 'nested.paramLocalDate'      | ''    | '{"nested":{}}' |
      | 'PATCH'  | 'nested.paramLocalDate'      | ''    | '{"nested":{}}' |
      | 'DELETE' | 'nested.paramLocalDate'      | ''    | '{"nested":{}}' |
      | 'GET'    | 'nested.paramLocalTime'      | ''    | '{"nested":{}}' |
      | 'HEAD'   | 'nested.paramLocalTime'      | ''    | '{"nested":{}}' |
      | 'POST'   | 'nested.paramLocalTime'      | ''    | '{"nested":{}}' |
      | 'PUT'    | 'nested.paramLocalTime'      | ''    | '{"nested":{}}' |
      | 'PATCH'  | 'nested.paramLocalTime'      | ''    | '{"nested":{}}' |
      | 'DELETE' | 'nested.paramLocalTime'      | ''    | '{"nested":{}}' |
      | 'GET'    | 'nested.paramYear'           | ''    | '{"nested":{}}' |
      | 'HEAD'   | 'nested.paramYear'           | ''    | '{"nested":{}}' |
      | 'POST'   | 'nested.paramYear'           | ''    | '{"nested":{}}' |
      | 'PUT'    | 'nested.paramYear'           | ''    | '{"nested":{}}' |
      | 'PATCH'  | 'nested.paramYear'           | ''    | '{"nested":{}}' |
      | 'DELETE' | 'nested.paramYear'           | ''    | '{"nested":{}}' |
      | 'GET'    | 'nested.paramEnumAB'         | ''    | '{"nested":{}}' |
      | 'HEAD'   | 'nested.paramEnumAB'         | ''    | '{"nested":{}}' |
      | 'POST'   | 'nested.paramEnumAB'         | ''    | '{"nested":{}}' |
      | 'PUT'    | 'nested.paramEnumAB'         | ''    | '{"nested":{}}' |
      | 'PATCH'  | 'nested.paramEnumAB'         | ''    | '{"nested":{}}' |
      | 'DELETE' | 'nested.paramEnumAB'         | ''    | '{"nested":{}}' |
      | 'GET'    | 'nested.paramEnumLowerAB'    | ''    | '{"nested":{}}' |
      | 'HEAD'   | 'nested.paramEnumLowerAB'    | ''    | '{"nested":{}}' |
      | 'POST'   | 'nested.paramEnumLowerAB'    | ''    | '{"nested":{}}' |
      | 'PUT'    | 'nested.paramEnumLowerAB'    | ''    | '{"nested":{}}' |
      | 'PATCH'  | 'nested.paramEnumLowerAB'    | ''    | '{"nested":{}}' |
      | 'DELETE' | 'nested.paramEnumLowerAB'    | ''    | '{"nested":{}}' |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/nested-optional-dto-attribute?<param>=<value> endpoint (RequestParamUnconstrainedRestApi.class)
     this endpoint receives the request parameter in a nested optional dto attribute, both having the same name
       and the service correctly maps the sent empty value with the declared data type by setting it to null, responding with a success message.

    Given the endpoint '/api/v1/request-parameter/nested-optional-dto-attribute'
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | param                        | value | received value                            |
      | 'GET'    | 'nested.paramString'         | ''    | '{"nested":{"paramString":null}}'         |
      | 'HEAD'   | 'nested.paramString'         | ''    | '{"nested":{"paramString":null}}'         |
      | 'POST'   | 'nested.paramString'         | ''    | '{"nested":{"paramString":null}}'         |
      | 'PUT'    | 'nested.paramString'         | ''    | '{"nested":{"paramString":null}}'         |
      | 'PATCH'  | 'nested.paramString'         | ''    | '{"nested":{"paramString":null}}'         |
      | 'DELETE' | 'nested.paramString'         | ''    | '{"nested":{"paramString":null}}'         |
      | 'GET'    | 'nested.paramInteger'        | ''    | '{"nested":{"paramInteger":null}}'        |
      | 'HEAD'   | 'nested.paramInteger'        | ''    | '{"nested":{"paramInteger":null}}'        |
      | 'POST'   | 'nested.paramInteger'        | ''    | '{"nested":{"paramInteger":null}}'        |
      | 'PUT'    | 'nested.paramInteger'        | ''    | '{"nested":{"paramInteger":null}}'        |
      | 'PATCH'  | 'nested.paramInteger'        | ''    | '{"nested":{"paramInteger":null}}'        |
      | 'DELETE' | 'nested.paramInteger'        | ''    | '{"nested":{"paramInteger":null}}'        |
      | 'GET'    | 'nested.paramLong'           | ''    | '{"nested":{"paramLong":null}}'           |
      | 'HEAD'   | 'nested.paramLong'           | ''    | '{"nested":{"paramLong":null}}'           |
      | 'POST'   | 'nested.paramLong'           | ''    | '{"nested":{"paramLong":null}}'           |
      | 'PUT'    | 'nested.paramLong'           | ''    | '{"nested":{"paramLong":null}}'           |
      | 'PATCH'  | 'nested.paramLong'           | ''    | '{"nested":{"paramLong":null}}'           |
      | 'DELETE' | 'nested.paramLong'           | ''    | '{"nested":{"paramLong":null}}'           |
      | 'GET'    | 'nested.paramBigInteger'     | ''    | '{"nested":{"paramBigInteger":null}}'     |
      | 'HEAD'   | 'nested.paramBigInteger'     | ''    | '{"nested":{"paramBigInteger":null}}'     |
      | 'POST'   | 'nested.paramBigInteger'     | ''    | '{"nested":{"paramBigInteger":null}}'     |
      | 'PUT'    | 'nested.paramBigInteger'     | ''    | '{"nested":{"paramBigInteger":null}}'     |
      | 'PATCH'  | 'nested.paramBigInteger'     | ''    | '{"nested":{"paramBigInteger":null}}'     |
      | 'DELETE' | 'nested.paramBigInteger'     | ''    | '{"nested":{"paramBigInteger":null}}'     |
      | 'GET'    | 'nested.paramFloat'          | ''    | '{"nested":{"paramFloat":null}}'          |
      | 'HEAD'   | 'nested.paramFloat'          | ''    | '{"nested":{"paramFloat":null}}'          |
      | 'POST'   | 'nested.paramFloat'          | ''    | '{"nested":{"paramFloat":null}}'          |
      | 'PUT'    | 'nested.paramFloat'          | ''    | '{"nested":{"paramFloat":null}}'          |
      | 'PATCH'  | 'nested.paramFloat'          | ''    | '{"nested":{"paramFloat":null}}'          |
      | 'DELETE' | 'nested.paramFloat'          | ''    | '{"nested":{"paramFloat":null}}'          |
      | 'GET'    | 'nested.paramDouble'         | ''    | '{"nested":{"paramDouble":null}}'         |
      | 'HEAD'   | 'nested.paramDouble'         | ''    | '{"nested":{"paramDouble":null}}'         |
      | 'POST'   | 'nested.paramDouble'         | ''    | '{"nested":{"paramDouble":null}}'         |
      | 'PUT'    | 'nested.paramDouble'         | ''    | '{"nested":{"paramDouble":null}}'         |
      | 'PATCH'  | 'nested.paramDouble'         | ''    | '{"nested":{"paramDouble":null}}'         |
      | 'DELETE' | 'nested.paramDouble'         | ''    | '{"nested":{"paramDouble":null}}'         |
      | 'GET'    | 'nested.paramBigDecimal'     | ''    | '{"nested":{"paramBigDecimal":null}}'     |
      | 'HEAD'   | 'nested.paramBigDecimal'     | ''    | '{"nested":{"paramBigDecimal":null}}'     |
      | 'POST'   | 'nested.paramBigDecimal'     | ''    | '{"nested":{"paramBigDecimal":null}}'     |
      | 'PUT'    | 'nested.paramBigDecimal'     | ''    | '{"nested":{"paramBigDecimal":null}}'     |
      | 'PATCH'  | 'nested.paramBigDecimal'     | ''    | '{"nested":{"paramBigDecimal":null}}'     |
      | 'DELETE' | 'nested.paramBigDecimal'     | ''    | '{"nested":{"paramBigDecimal":null}}'     |
      | 'GET'    | 'nested.paramBoolean'        | ''    | '{"nested":{"paramBoolean":null}}'        |
      | 'HEAD'   | 'nested.paramBoolean'        | ''    | '{"nested":{"paramBoolean":null}}'        |
      | 'POST'   | 'nested.paramBoolean'        | ''    | '{"nested":{"paramBoolean":null}}'        |
      | 'PUT'    | 'nested.paramBoolean'        | ''    | '{"nested":{"paramBoolean":null}}'        |
      | 'PATCH'  | 'nested.paramBoolean'        | ''    | '{"nested":{"paramBoolean":null}}'        |
      | 'DELETE' | 'nested.paramBoolean'        | ''    | '{"nested":{"paramBoolean":null}}'        |
      | 'GET'    | 'nested.paramOffsetDateTime' | ''    | '{"nested":{"paramOffsetDateTime":null}}' |
      | 'HEAD'   | 'nested.paramOffsetDateTime' | ''    | '{"nested":{"paramOffsetDateTime":null}}' |
      | 'POST'   | 'nested.paramOffsetDateTime' | ''    | '{"nested":{"paramOffsetDateTime":null}}' |
      | 'PUT'    | 'nested.paramOffsetDateTime' | ''    | '{"nested":{"paramOffsetDateTime":null}}' |
      | 'PATCH'  | 'nested.paramOffsetDateTime' | ''    | '{"nested":{"paramOffsetDateTime":null}}' |
      | 'DELETE' | 'nested.paramOffsetDateTime' | ''    | '{"nested":{"paramOffsetDateTime":null}}' |
      | 'GET'    | 'nested.paramLocalDateTime'  | ''    | '{"nested":{"paramLocalDateTime":null}}'  |
      | 'HEAD'   | 'nested.paramLocalDateTime'  | ''    | '{"nested":{"paramLocalDateTime":null}}'  |
      | 'POST'   | 'nested.paramLocalDateTime'  | ''    | '{"nested":{"paramLocalDateTime":null}}'  |
      | 'PUT'    | 'nested.paramLocalDateTime'  | ''    | '{"nested":{"paramLocalDateTime":null}}'  |
      | 'PATCH'  | 'nested.paramLocalDateTime'  | ''    | '{"nested":{"paramLocalDateTime":null}}'  |
      | 'DELETE' | 'nested.paramLocalDateTime'  | ''    | '{"nested":{"paramLocalDateTime":null}}'  |
      | 'GET'    | 'nested.paramLocalDate'      | ''    | '{"nested":{"paramLocalDate":null}}'      |
      | 'HEAD'   | 'nested.paramLocalDate'      | ''    | '{"nested":{"paramLocalDate":null}}'      |
      | 'POST'   | 'nested.paramLocalDate'      | ''    | '{"nested":{"paramLocalDate":null}}'      |
      | 'PUT'    | 'nested.paramLocalDate'      | ''    | '{"nested":{"paramLocalDate":null}}'      |
      | 'PATCH'  | 'nested.paramLocalDate'      | ''    | '{"nested":{"paramLocalDate":null}}'      |
      | 'DELETE' | 'nested.paramLocalDate'      | ''    | '{"nested":{"paramLocalDate":null}}'      |
      | 'GET'    | 'nested.paramLocalTime'      | ''    | '{"nested":{"paramLocalTime":null}}'      |
      | 'HEAD'   | 'nested.paramLocalTime'      | ''    | '{"nested":{"paramLocalTime":null}}'      |
      | 'POST'   | 'nested.paramLocalTime'      | ''    | '{"nested":{"paramLocalTime":null}}'      |
      | 'PUT'    | 'nested.paramLocalTime'      | ''    | '{"nested":{"paramLocalTime":null}}'      |
      | 'PATCH'  | 'nested.paramLocalTime'      | ''    | '{"nested":{"paramLocalTime":null}}'      |
      | 'DELETE' | 'nested.paramLocalTime'      | ''    | '{"nested":{"paramLocalTime":null}}'      |
      | 'GET'    | 'nested.paramYear'           | ''    | '{"nested":{"paramYear":null}}'           |
      | 'HEAD'   | 'nested.paramYear'           | ''    | '{"nested":{"paramYear":null}}'           |
      | 'POST'   | 'nested.paramYear'           | ''    | '{"nested":{"paramYear":null}}'           |
      | 'PUT'    | 'nested.paramYear'           | ''    | '{"nested":{"paramYear":null}}'           |
      | 'PATCH'  | 'nested.paramYear'           | ''    | '{"nested":{"paramYear":null}}'           |
      | 'DELETE' | 'nested.paramYear'           | ''    | '{"nested":{"paramYear":null}}'           |
      | 'GET'    | 'nested.paramEnumAB'         | ''    | '{"nested":{"paramEnumAB":null}}'         |
      | 'HEAD'   | 'nested.paramEnumAB'         | ''    | '{"nested":{"paramEnumAB":null}}'         |
      | 'POST'   | 'nested.paramEnumAB'         | ''    | '{"nested":{"paramEnumAB":null}}'         |
      | 'PUT'    | 'nested.paramEnumAB'         | ''    | '{"nested":{"paramEnumAB":null}}'         |
      | 'PATCH'  | 'nested.paramEnumAB'         | ''    | '{"nested":{"paramEnumAB":null}}'         |
      | 'DELETE' | 'nested.paramEnumAB'         | ''    | '{"nested":{"paramEnumAB":null}}'         |
      | 'GET'    | 'nested.paramEnumLowerAB'    | ''    | '{"nested":{"paramEnumLowerAB":null}}'    |
      | 'HEAD'   | 'nested.paramEnumLowerAB'    | ''    | '{"nested":{"paramEnumLowerAB":null}}'    |
      | 'POST'   | 'nested.paramEnumLowerAB'    | ''    | '{"nested":{"paramEnumLowerAB":null}}'    |
      | 'PUT'    | 'nested.paramEnumLowerAB'    | ''    | '{"nested":{"paramEnumLowerAB":null}}'    |
      | 'PATCH'  | 'nested.paramEnumLowerAB'    | ''    | '{"nested":{"paramEnumLowerAB":null}}'    |
      | 'DELETE' | 'nested.paramEnumLowerAB'    | ''    | '{"nested":{"paramEnumLowerAB":null}}'    |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/nested-dto-attribute?<param>=<value> endpoint (RequestParamUnconstrainedRestApi.class)
    this endpoint receives the request parameter in a nested dto attribute from an array, both having the same name
      and the service correctly maps the sent empty value with the declared data type by setting it to null, responding with a success message.

    Given the endpoint '/api/v1/request-parameter/nested-dto-attribute'
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | param                          | value | received value   |
      | 'GET'    | 'array[0].paramString'         | ''    | '{"array":[{}]}' |
      | 'HEAD'   | 'array[0].paramString'         | ''    | '{"array":[{}]}' |
      | 'POST'   | 'array[0].paramString'         | ''    | '{"array":[{}]}' |
      | 'PUT'    | 'array[0].paramString'         | ''    | '{"array":[{}]}' |
      | 'PATCH'  | 'array[0].paramString'         | ''    | '{"array":[{}]}' |
      | 'DELETE' | 'array[0].paramString'         | ''    | '{"array":[{}]}' |
      | 'GET'    | 'array[0].paramInteger'        | ''    | '{"array":[{}]}' |
      | 'HEAD'   | 'array[0].paramInteger'        | ''    | '{"array":[{}]}' |
      | 'POST'   | 'array[0].paramInteger'        | ''    | '{"array":[{}]}' |
      | 'PUT'    | 'array[0].paramInteger'        | ''    | '{"array":[{}]}' |
      | 'PATCH'  | 'array[0].paramInteger'        | ''    | '{"array":[{}]}' |
      | 'DELETE' | 'array[0].paramInteger'        | ''    | '{"array":[{}]}' |
      | 'GET'    | 'array[0].paramLong'           | ''    | '{"array":[{}]}' |
      | 'HEAD'   | 'array[0].paramLong'           | ''    | '{"array":[{}]}' |
      | 'POST'   | 'array[0].paramLong'           | ''    | '{"array":[{}]}' |
      | 'PUT'    | 'array[0].paramLong'           | ''    | '{"array":[{}]}' |
      | 'PATCH'  | 'array[0].paramLong'           | ''    | '{"array":[{}]}' |
      | 'DELETE' | 'array[0].paramLong'           | ''    | '{"array":[{}]}' |
      | 'GET'    | 'array[0].paramBigInteger'     | ''    | '{"array":[{}]}' |
      | 'HEAD'   | 'array[0].paramBigInteger'     | ''    | '{"array":[{}]}' |
      | 'POST'   | 'array[0].paramBigInteger'     | ''    | '{"array":[{}]}' |
      | 'PUT'    | 'array[0].paramBigInteger'     | ''    | '{"array":[{}]}' |
      | 'PATCH'  | 'array[0].paramBigInteger'     | ''    | '{"array":[{}]}' |
      | 'DELETE' | 'array[0].paramBigInteger'     | ''    | '{"array":[{}]}' |
      | 'GET'    | 'array[0].paramFloat'          | ''    | '{"array":[{}]}' |
      | 'HEAD'   | 'array[0].paramFloat'          | ''    | '{"array":[{}]}' |
      | 'POST'   | 'array[0].paramFloat'          | ''    | '{"array":[{}]}' |
      | 'PUT'    | 'array[0].paramFloat'          | ''    | '{"array":[{}]}' |
      | 'PATCH'  | 'array[0].paramFloat'          | ''    | '{"array":[{}]}' |
      | 'DELETE' | 'array[0].paramFloat'          | ''    | '{"array":[{}]}' |
      | 'GET'    | 'array[0].paramDouble'         | ''    | '{"array":[{}]}' |
      | 'HEAD'   | 'array[0].paramDouble'         | ''    | '{"array":[{}]}' |
      | 'POST'   | 'array[0].paramDouble'         | ''    | '{"array":[{}]}' |
      | 'PUT'    | 'array[0].paramDouble'         | ''    | '{"array":[{}]}' |
      | 'PATCH'  | 'array[0].paramDouble'         | ''    | '{"array":[{}]}' |
      | 'DELETE' | 'array[0].paramDouble'         | ''    | '{"array":[{}]}' |
      | 'GET'    | 'array[0].paramBigDecimal'     | ''    | '{"array":[{}]}' |
      | 'HEAD'   | 'array[0].paramBigDecimal'     | ''    | '{"array":[{}]}' |
      | 'POST'   | 'array[0].paramBigDecimal'     | ''    | '{"array":[{}]}' |
      | 'PUT'    | 'array[0].paramBigDecimal'     | ''    | '{"array":[{}]}' |
      | 'PATCH'  | 'array[0].paramBigDecimal'     | ''    | '{"array":[{}]}' |
      | 'DELETE' | 'array[0].paramBigDecimal'     | ''    | '{"array":[{}]}' |
      | 'GET'    | 'array[0].paramBoolean'        | ''    | '{"array":[{}]}' |
      | 'HEAD'   | 'array[0].paramBoolean'        | ''    | '{"array":[{}]}' |
      | 'POST'   | 'array[0].paramBoolean'        | ''    | '{"array":[{}]}' |
      | 'PUT'    | 'array[0].paramBoolean'        | ''    | '{"array":[{}]}' |
      | 'PATCH'  | 'array[0].paramBoolean'        | ''    | '{"array":[{}]}' |
      | 'DELETE' | 'array[0].paramBoolean'        | ''    | '{"array":[{}]}' |
      | 'GET'    | 'array[0].paramOffsetDateTime' | ''    | '{"array":[{}]}' |
      | 'HEAD'   | 'array[0].paramOffsetDateTime' | ''    | '{"array":[{}]}' |
      | 'POST'   | 'array[0].paramOffsetDateTime' | ''    | '{"array":[{}]}' |
      | 'PUT'    | 'array[0].paramOffsetDateTime' | ''    | '{"array":[{}]}' |
      | 'PATCH'  | 'array[0].paramOffsetDateTime' | ''    | '{"array":[{}]}' |
      | 'DELETE' | 'array[0].paramOffsetDateTime' | ''    | '{"array":[{}]}' |
      | 'GET'    | 'array[0].paramLocalDateTime'  | ''    | '{"array":[{}]}' |
      | 'HEAD'   | 'array[0].paramLocalDateTime'  | ''    | '{"array":[{}]}' |
      | 'POST'   | 'array[0].paramLocalDateTime'  | ''    | '{"array":[{}]}' |
      | 'PUT'    | 'array[0].paramLocalDateTime'  | ''    | '{"array":[{}]}' |
      | 'PATCH'  | 'array[0].paramLocalDateTime'  | ''    | '{"array":[{}]}' |
      | 'DELETE' | 'array[0].paramLocalDateTime'  | ''    | '{"array":[{}]}' |
      | 'GET'    | 'array[0].paramLocalDate'      | ''    | '{"array":[{}]}' |
      | 'HEAD'   | 'array[0].paramLocalDate'      | ''    | '{"array":[{}]}' |
      | 'POST'   | 'array[0].paramLocalDate'      | ''    | '{"array":[{}]}' |
      | 'PUT'    | 'array[0].paramLocalDate'      | ''    | '{"array":[{}]}' |
      | 'PATCH'  | 'array[0].paramLocalDate'      | ''    | '{"array":[{}]}' |
      | 'DELETE' | 'array[0].paramLocalDate'      | ''    | '{"array":[{}]}' |
      | 'GET'    | 'array[0].paramLocalTime'      | ''    | '{"array":[{}]}' |
      | 'HEAD'   | 'array[0].paramLocalTime'      | ''    | '{"array":[{}]}' |
      | 'POST'   | 'array[0].paramLocalTime'      | ''    | '{"array":[{}]}' |
      | 'PUT'    | 'array[0].paramLocalTime'      | ''    | '{"array":[{}]}' |
      | 'PATCH'  | 'array[0].paramLocalTime'      | ''    | '{"array":[{}]}' |
      | 'DELETE' | 'array[0].paramLocalTime'      | ''    | '{"array":[{}]}' |
      | 'GET'    | 'array[0].paramYear'           | ''    | '{"array":[{}]}' |
      | 'HEAD'   | 'array[0].paramYear'           | ''    | '{"array":[{}]}' |
      | 'POST'   | 'array[0].paramYear'           | ''    | '{"array":[{}]}' |
      | 'PUT'    | 'array[0].paramYear'           | ''    | '{"array":[{}]}' |
      | 'PATCH'  | 'array[0].paramYear'           | ''    | '{"array":[{}]}' |
      | 'DELETE' | 'array[0].paramYear'           | ''    | '{"array":[{}]}' |
      | 'GET'    | 'array[0].paramEnumAB'         | ''    | '{"array":[{}]}' |
      | 'HEAD'   | 'array[0].paramEnumAB'         | ''    | '{"array":[{}]}' |
      | 'POST'   | 'array[0].paramEnumAB'         | ''    | '{"array":[{}]}' |
      | 'PUT'    | 'array[0].paramEnumAB'         | ''    | '{"array":[{}]}' |
      | 'PATCH'  | 'array[0].paramEnumAB'         | ''    | '{"array":[{}]}' |
      | 'DELETE' | 'array[0].paramEnumAB'         | ''    | '{"array":[{}]}' |
      | 'GET'    | 'array[0].paramEnumLowerAB'    | ''    | '{"array":[{}]}' |
      | 'HEAD'   | 'array[0].paramEnumLowerAB'    | ''    | '{"array":[{}]}' |
      | 'POST'   | 'array[0].paramEnumLowerAB'    | ''    | '{"array":[{}]}' |
      | 'PUT'    | 'array[0].paramEnumLowerAB'    | ''    | '{"array":[{}]}' |
      | 'PATCH'  | 'array[0].paramEnumLowerAB'    | ''    | '{"array":[{}]}' |
      | 'DELETE' | 'array[0].paramEnumLowerAB'    | ''    | '{"array":[{}]}' |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/nested-optional-dto-attribute?<param>=<value> endpoint (RequestParamUnconstrainedRestApi.class)
    this endpoint receives the request parameter in a nested optional dto attribute from an array, both having the same name
      and the service correctly maps the sent empty value with the declared data type by setting it to null, responding with a success message.

    Given the endpoint '/api/v1/request-parameter/nested-optional-dto-attribute'
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 204
    And the received value is <received value>

    Examples: 
      | method   | param                          | value | received value                             |
      | 'GET'    | 'array[0].paramString'         | ''    | '{"array":[{"paramString":null}]}'         |
      | 'HEAD'   | 'array[0].paramString'         | ''    | '{"array":[{"paramString":null}]}'         |
      | 'POST'   | 'array[0].paramString'         | ''    | '{"array":[{"paramString":null}]}'         |
      | 'PUT'    | 'array[0].paramString'         | ''    | '{"array":[{"paramString":null}]}'         |
      | 'PATCH'  | 'array[0].paramString'         | ''    | '{"array":[{"paramString":null}]}'         |
      | 'DELETE' | 'array[0].paramString'         | ''    | '{"array":[{"paramString":null}]}'         |
      | 'GET'    | 'array[0].paramInteger'        | ''    | '{"array":[{"paramInteger":null}]}'        |
      | 'HEAD'   | 'array[0].paramInteger'        | ''    | '{"array":[{"paramInteger":null}]}'        |
      | 'POST'   | 'array[0].paramInteger'        | ''    | '{"array":[{"paramInteger":null}]}'        |
      | 'PUT'    | 'array[0].paramInteger'        | ''    | '{"array":[{"paramInteger":null}]}'        |
      | 'PATCH'  | 'array[0].paramInteger'        | ''    | '{"array":[{"paramInteger":null}]}'        |
      | 'DELETE' | 'array[0].paramInteger'        | ''    | '{"array":[{"paramInteger":null}]}'        |
      | 'GET'    | 'array[0].paramLong'           | ''    | '{"array":[{"paramLong":null}]}'           |
      | 'HEAD'   | 'array[0].paramLong'           | ''    | '{"array":[{"paramLong":null}]}'           |
      | 'POST'   | 'array[0].paramLong'           | ''    | '{"array":[{"paramLong":null}]}'           |
      | 'PUT'    | 'array[0].paramLong'           | ''    | '{"array":[{"paramLong":null}]}'           |
      | 'PATCH'  | 'array[0].paramLong'           | ''    | '{"array":[{"paramLong":null}]}'           |
      | 'DELETE' | 'array[0].paramLong'           | ''    | '{"array":[{"paramLong":null}]}'           |
      | 'GET'    | 'array[0].paramBigInteger'     | ''    | '{"array":[{"paramBigInteger":null}]}'     |
      | 'HEAD'   | 'array[0].paramBigInteger'     | ''    | '{"array":[{"paramBigInteger":null}]}'     |
      | 'POST'   | 'array[0].paramBigInteger'     | ''    | '{"array":[{"paramBigInteger":null}]}'     |
      | 'PUT'    | 'array[0].paramBigInteger'     | ''    | '{"array":[{"paramBigInteger":null}]}'     |
      | 'PATCH'  | 'array[0].paramBigInteger'     | ''    | '{"array":[{"paramBigInteger":null}]}'     |
      | 'DELETE' | 'array[0].paramBigInteger'     | ''    | '{"array":[{"paramBigInteger":null}]}'     |
      | 'GET'    | 'array[0].paramFloat'          | ''    | '{"array":[{"paramFloat":null}]}'          |
      | 'HEAD'   | 'array[0].paramFloat'          | ''    | '{"array":[{"paramFloat":null}]}'          |
      | 'POST'   | 'array[0].paramFloat'          | ''    | '{"array":[{"paramFloat":null}]}'          |
      | 'PUT'    | 'array[0].paramFloat'          | ''    | '{"array":[{"paramFloat":null}]}'          |
      | 'PATCH'  | 'array[0].paramFloat'          | ''    | '{"array":[{"paramFloat":null}]}'          |
      | 'DELETE' | 'array[0].paramFloat'          | ''    | '{"array":[{"paramFloat":null}]}'          |
      | 'GET'    | 'array[0].paramDouble'         | ''    | '{"array":[{"paramDouble":null}]}'         |
      | 'HEAD'   | 'array[0].paramDouble'         | ''    | '{"array":[{"paramDouble":null}]}'         |
      | 'POST'   | 'array[0].paramDouble'         | ''    | '{"array":[{"paramDouble":null}]}'         |
      | 'PUT'    | 'array[0].paramDouble'         | ''    | '{"array":[{"paramDouble":null}]}'         |
      | 'PATCH'  | 'array[0].paramDouble'         | ''    | '{"array":[{"paramDouble":null}]}'         |
      | 'DELETE' | 'array[0].paramDouble'         | ''    | '{"array":[{"paramDouble":null}]}'         |
      | 'GET'    | 'array[0].paramBigDecimal'     | ''    | '{"array":[{"paramBigDecimal":null}]}'     |
      | 'HEAD'   | 'array[0].paramBigDecimal'     | ''    | '{"array":[{"paramBigDecimal":null}]}'     |
      | 'POST'   | 'array[0].paramBigDecimal'     | ''    | '{"array":[{"paramBigDecimal":null}]}'     |
      | 'PUT'    | 'array[0].paramBigDecimal'     | ''    | '{"array":[{"paramBigDecimal":null}]}'     |
      | 'PATCH'  | 'array[0].paramBigDecimal'     | ''    | '{"array":[{"paramBigDecimal":null}]}'     |
      | 'DELETE' | 'array[0].paramBigDecimal'     | ''    | '{"array":[{"paramBigDecimal":null}]}'     |
      | 'GET'    | 'array[0].paramBoolean'        | ''    | '{"array":[{"paramBoolean":null}]}'        |
      | 'HEAD'   | 'array[0].paramBoolean'        | ''    | '{"array":[{"paramBoolean":null}]}'        |
      | 'POST'   | 'array[0].paramBoolean'        | ''    | '{"array":[{"paramBoolean":null}]}'        |
      | 'PUT'    | 'array[0].paramBoolean'        | ''    | '{"array":[{"paramBoolean":null}]}'        |
      | 'PATCH'  | 'array[0].paramBoolean'        | ''    | '{"array":[{"paramBoolean":null}]}'        |
      | 'DELETE' | 'array[0].paramBoolean'        | ''    | '{"array":[{"paramBoolean":null}]}'        |
      | 'GET'    | 'array[0].paramOffsetDateTime' | ''    | '{"array":[{"paramOffsetDateTime":null}]}' |
      | 'HEAD'   | 'array[0].paramOffsetDateTime' | ''    | '{"array":[{"paramOffsetDateTime":null}]}' |
      | 'POST'   | 'array[0].paramOffsetDateTime' | ''    | '{"array":[{"paramOffsetDateTime":null}]}' |
      | 'PUT'    | 'array[0].paramOffsetDateTime' | ''    | '{"array":[{"paramOffsetDateTime":null}]}' |
      | 'PATCH'  | 'array[0].paramOffsetDateTime' | ''    | '{"array":[{"paramOffsetDateTime":null}]}' |
      | 'DELETE' | 'array[0].paramOffsetDateTime' | ''    | '{"array":[{"paramOffsetDateTime":null}]}' |
      | 'GET'    | 'array[0].paramLocalDateTime'  | ''    | '{"array":[{"paramLocalDateTime":null}]}'  |
      | 'HEAD'   | 'array[0].paramLocalDateTime'  | ''    | '{"array":[{"paramLocalDateTime":null}]}'  |
      | 'POST'   | 'array[0].paramLocalDateTime'  | ''    | '{"array":[{"paramLocalDateTime":null}]}'  |
      | 'PUT'    | 'array[0].paramLocalDateTime'  | ''    | '{"array":[{"paramLocalDateTime":null}]}'  |
      | 'PATCH'  | 'array[0].paramLocalDateTime'  | ''    | '{"array":[{"paramLocalDateTime":null}]}'  |
      | 'DELETE' | 'array[0].paramLocalDateTime'  | ''    | '{"array":[{"paramLocalDateTime":null}]}'  |
      | 'GET'    | 'array[0].paramLocalDate'      | ''    | '{"array":[{"paramLocalDate":null}]}'      |
      | 'HEAD'   | 'array[0].paramLocalDate'      | ''    | '{"array":[{"paramLocalDate":null}]}'      |
      | 'POST'   | 'array[0].paramLocalDate'      | ''    | '{"array":[{"paramLocalDate":null}]}'      |
      | 'PUT'    | 'array[0].paramLocalDate'      | ''    | '{"array":[{"paramLocalDate":null}]}'      |
      | 'PATCH'  | 'array[0].paramLocalDate'      | ''    | '{"array":[{"paramLocalDate":null}]}'      |
      | 'DELETE' | 'array[0].paramLocalDate'      | ''    | '{"array":[{"paramLocalDate":null}]}'      |
      | 'GET'    | 'array[0].paramLocalTime'      | ''    | '{"array":[{"paramLocalTime":null}]}'      |
      | 'HEAD'   | 'array[0].paramLocalTime'      | ''    | '{"array":[{"paramLocalTime":null}]}'      |
      | 'POST'   | 'array[0].paramLocalTime'      | ''    | '{"array":[{"paramLocalTime":null}]}'      |
      | 'PUT'    | 'array[0].paramLocalTime'      | ''    | '{"array":[{"paramLocalTime":null}]}'      |
      | 'PATCH'  | 'array[0].paramLocalTime'      | ''    | '{"array":[{"paramLocalTime":null}]}'      |
      | 'DELETE' | 'array[0].paramLocalTime'      | ''    | '{"array":[{"paramLocalTime":null}]}'      |
      | 'GET'    | 'array[0].paramYear'           | ''    | '{"array":[{"paramYear":null}]}'           |
      | 'HEAD'   | 'array[0].paramYear'           | ''    | '{"array":[{"paramYear":null}]}'           |
      | 'POST'   | 'array[0].paramYear'           | ''    | '{"array":[{"paramYear":null}]}'           |
      | 'PUT'    | 'array[0].paramYear'           | ''    | '{"array":[{"paramYear":null}]}'           |
      | 'PATCH'  | 'array[0].paramYear'           | ''    | '{"array":[{"paramYear":null}]}'           |
      | 'DELETE' | 'array[0].paramYear'           | ''    | '{"array":[{"paramYear":null}]}'           |
      | 'GET'    | 'array[0].paramEnumAB'         | ''    | '{"array":[{"paramEnumAB":null}]}'         |
      | 'HEAD'   | 'array[0].paramEnumAB'         | ''    | '{"array":[{"paramEnumAB":null}]}'         |
      | 'POST'   | 'array[0].paramEnumAB'         | ''    | '{"array":[{"paramEnumAB":null}]}'         |
      | 'PUT'    | 'array[0].paramEnumAB'         | ''    | '{"array":[{"paramEnumAB":null}]}'         |
      | 'PATCH'  | 'array[0].paramEnumAB'         | ''    | '{"array":[{"paramEnumAB":null}]}'         |
      | 'DELETE' | 'array[0].paramEnumAB'         | ''    | '{"array":[{"paramEnumAB":null}]}'         |
      | 'GET'    | 'array[0].paramEnumLowerAB'    | ''    | '{"array":[{"paramEnumLowerAB":null}]}'    |
      | 'HEAD'   | 'array[0].paramEnumLowerAB'    | ''    | '{"array":[{"paramEnumLowerAB":null}]}'    |
      | 'POST'   | 'array[0].paramEnumLowerAB'    | ''    | '{"array":[{"paramEnumLowerAB":null}]}'    |
      | 'PUT'    | 'array[0].paramEnumLowerAB'    | ''    | '{"array":[{"paramEnumLowerAB":null}]}'    |
      | 'PATCH'  | 'array[0].paramEnumLowerAB'    | ''    | '{"array":[{"paramEnumLowerAB":null}]}'    |
      | 'DELETE' | 'array[0].paramEnumLowerAB'    | ''    | '{"array":[{"paramEnumLowerAB":null}]}'    |
