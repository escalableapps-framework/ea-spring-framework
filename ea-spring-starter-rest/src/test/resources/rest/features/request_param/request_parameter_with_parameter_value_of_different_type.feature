Feature: Request parameter with parameter value of different type.
  
  The REST service receives data via the request parameters and cannot interpret the data because it is not the proper data type. The service responds with an error message.
  
   Rule: The conversion between data types is given by the following table
  
  | parameter type | java type      |
  |----------------|----------------|
  | string         | String         |
  | number         | Integer        |
  | number         | Long           |
  | number         | BigInteger     |
  | number         | Float          |
  | number         | Double         |
  | number         | BigDecimal     |
  | boolean        | Boolean        |
  | ISO-8601       | OffsetDateTime |
  | ISO-8601       | LocalDateTime  |
  | ISO-8601       | LocalDate      |
  | ISO-8601       | LocalTime      |
  | ISO-8601       | Year           |
  | string         | ENUM           |

  Background: 
    Given the default timezone 'America/Puerto_Rico'

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/method-parameter?<param>=<value> endpoint (RequestParamUnconstrainedRestApi.class)
    this endpoint receives the request parameter in a method parameter, both having the same name
    and the service cannot map the sent value to the declared data type, responding with an error message.

    Given the endpoint '/api/v1/request-parameter/method-parameter'
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | param                 | value | message                                    |
      | 'GET'    | 'paramInteger'        | 'X'   | 'paramInteger: is not a valid type'        |
      | 'HEAD'   | 'paramInteger'        | 'X'   |                                            |
      | 'POST'   | 'paramInteger'        | 'X'   | 'paramInteger: is not a valid type'        |
      | 'PUT'    | 'paramInteger'        | 'X'   | 'paramInteger: is not a valid type'        |
      | 'PATCH'  | 'paramInteger'        | 'X'   | 'paramInteger: is not a valid type'        |
      | 'DELETE' | 'paramInteger'        | 'X'   | 'paramInteger: is not a valid type'        |
      | 'GET'    | 'paramLong'           | 'X'   | 'paramLong: is not a valid type'           |
      | 'HEAD'   | 'paramLong'           | 'X'   |                                            |
      | 'POST'   | 'paramLong'           | 'X'   | 'paramLong: is not a valid type'           |
      | 'PUT'    | 'paramLong'           | 'X'   | 'paramLong: is not a valid type'           |
      | 'PATCH'  | 'paramLong'           | 'X'   | 'paramLong: is not a valid type'           |
      | 'DELETE' | 'paramLong'           | 'X'   | 'paramLong: is not a valid type'           |
      | 'GET'    | 'paramBigInteger'     | 'X'   | 'paramBigInteger: is not a valid type'     |
      | 'HEAD'   | 'paramBigInteger'     | 'X'   |                                            |
      | 'POST'   | 'paramBigInteger'     | 'X'   | 'paramBigInteger: is not a valid type'     |
      | 'PUT'    | 'paramBigInteger'     | 'X'   | 'paramBigInteger: is not a valid type'     |
      | 'PATCH'  | 'paramBigInteger'     | 'X'   | 'paramBigInteger: is not a valid type'     |
      | 'DELETE' | 'paramBigInteger'     | 'X'   | 'paramBigInteger: is not a valid type'     |
      | 'GET'    | 'paramFloat'          | 'X'   | 'paramFloat: is not a valid type'          |
      | 'HEAD'   | 'paramFloat'          | 'X'   |                                            |
      | 'POST'   | 'paramFloat'          | 'X'   | 'paramFloat: is not a valid type'          |
      | 'PUT'    | 'paramFloat'          | 'X'   | 'paramFloat: is not a valid type'          |
      | 'PATCH'  | 'paramFloat'          | 'X'   | 'paramFloat: is not a valid type'          |
      | 'DELETE' | 'paramFloat'          | 'X'   | 'paramFloat: is not a valid type'          |
      | 'GET'    | 'paramDouble'         | 'X'   | 'paramDouble: is not a valid type'         |
      | 'HEAD'   | 'paramDouble'         | 'X'   |                                            |
      | 'POST'   | 'paramDouble'         | 'X'   | 'paramDouble: is not a valid type'         |
      | 'PUT'    | 'paramDouble'         | 'X'   | 'paramDouble: is not a valid type'         |
      | 'PATCH'  | 'paramDouble'         | 'X'   | 'paramDouble: is not a valid type'         |
      | 'DELETE' | 'paramDouble'         | 'X'   | 'paramDouble: is not a valid type'         |
      | 'GET'    | 'paramBigDecimal'     | 'X'   | 'paramBigDecimal: is not a valid type'     |
      | 'HEAD'   | 'paramBigDecimal'     | 'X'   |                                            |
      | 'POST'   | 'paramBigDecimal'     | 'X'   | 'paramBigDecimal: is not a valid type'     |
      | 'PUT'    | 'paramBigDecimal'     | 'X'   | 'paramBigDecimal: is not a valid type'     |
      | 'PATCH'  | 'paramBigDecimal'     | 'X'   | 'paramBigDecimal: is not a valid type'     |
      | 'DELETE' | 'paramBigDecimal'     | 'X'   | 'paramBigDecimal: is not a valid type'     |
      | 'GET'    | 'paramBoolean'        | 'X'   | 'paramBoolean: is not a valid type'        |
      | 'HEAD'   | 'paramBoolean'        | 'X'   |                                            |
      | 'POST'   | 'paramBoolean'        | 'X'   | 'paramBoolean: is not a valid type'        |
      | 'PUT'    | 'paramBoolean'        | 'X'   | 'paramBoolean: is not a valid type'        |
      | 'PATCH'  | 'paramBoolean'        | 'X'   | 'paramBoolean: is not a valid type'        |
      | 'DELETE' | 'paramBoolean'        | 'X'   | 'paramBoolean: is not a valid type'        |
      | 'GET'    | 'paramOffsetDateTime' | 'X'   | 'paramOffsetDateTime: is not a valid type' |
      | 'HEAD'   | 'paramOffsetDateTime' | 'X'   |                                            |
      | 'POST'   | 'paramOffsetDateTime' | 'X'   | 'paramOffsetDateTime: is not a valid type' |
      | 'PUT'    | 'paramOffsetDateTime' | 'X'   | 'paramOffsetDateTime: is not a valid type' |
      | 'PATCH'  | 'paramOffsetDateTime' | 'X'   | 'paramOffsetDateTime: is not a valid type' |
      | 'DELETE' | 'paramOffsetDateTime' | 'X'   | 'paramOffsetDateTime: is not a valid type' |
      | 'GET'    | 'paramLocalDateTime'  | 'X'   | 'paramLocalDateTime: is not a valid type'  |
      | 'HEAD'   | 'paramLocalDateTime'  | 'X'   |                                            |
      | 'POST'   | 'paramLocalDateTime'  | 'X'   | 'paramLocalDateTime: is not a valid type'  |
      | 'PUT'    | 'paramLocalDateTime'  | 'X'   | 'paramLocalDateTime: is not a valid type'  |
      | 'PATCH'  | 'paramLocalDateTime'  | 'X'   | 'paramLocalDateTime: is not a valid type'  |
      | 'DELETE' | 'paramLocalDateTime'  | 'X'   | 'paramLocalDateTime: is not a valid type'  |
      | 'GET'    | 'paramLocalDate'      | 'X'   | 'paramLocalDate: is not a valid type'      |
      | 'HEAD'   | 'paramLocalDate'      | 'X'   |                                            |
      | 'POST'   | 'paramLocalDate'      | 'X'   | 'paramLocalDate: is not a valid type'      |
      | 'PUT'    | 'paramLocalDate'      | 'X'   | 'paramLocalDate: is not a valid type'      |
      | 'PATCH'  | 'paramLocalDate'      | 'X'   | 'paramLocalDate: is not a valid type'      |
      | 'DELETE' | 'paramLocalDate'      | 'X'   | 'paramLocalDate: is not a valid type'      |
      | 'GET'    | 'paramLocalTime'      | 'X'   | 'paramLocalTime: is not a valid type'      |
      | 'HEAD'   | 'paramLocalTime'      | 'X'   |                                            |
      | 'POST'   | 'paramLocalTime'      | 'X'   | 'paramLocalTime: is not a valid type'      |
      | 'PUT'    | 'paramLocalTime'      | 'X'   | 'paramLocalTime: is not a valid type'      |
      | 'PATCH'  | 'paramLocalTime'      | 'X'   | 'paramLocalTime: is not a valid type'      |
      | 'DELETE' | 'paramLocalTime'      | 'X'   | 'paramLocalTime: is not a valid type'      |
      | 'GET'    | 'paramYear'           | 'X'   | 'paramYear: is not a valid type'           |
      | 'HEAD'   | 'paramYear'           | 'X'   |                                            |
      | 'POST'   | 'paramYear'           | 'X'   | 'paramYear: is not a valid type'           |
      | 'PUT'    | 'paramYear'           | 'X'   | 'paramYear: is not a valid type'           |
      | 'PATCH'  | 'paramYear'           | 'X'   | 'paramYear: is not a valid type'           |
      | 'DELETE' | 'paramYear'           | 'X'   | 'paramYear: is not a valid type'           |
      | 'GET'    | 'paramEnumAB'         | 'X'   | 'paramEnumAB: is not a valid type'         |
      | 'HEAD'   | 'paramEnumAB'         | 'X'   |                                            |
      | 'POST'   | 'paramEnumAB'         | 'X'   | 'paramEnumAB: is not a valid type'         |
      | 'PUT'    | 'paramEnumAB'         | 'X'   | 'paramEnumAB: is not a valid type'         |
      | 'PATCH'  | 'paramEnumAB'         | 'X'   | 'paramEnumAB: is not a valid type'         |
      | 'DELETE' | 'paramEnumAB'         | 'X'   | 'paramEnumAB: is not a valid type'         |
      | 'GET'    | 'paramEnumLowerAB'    | 'X'   | 'paramEnumLowerAB: is not a valid type'    |
      | 'HEAD'   | 'paramEnumLowerAB'    | 'X'   |                                            |
      | 'POST'   | 'paramEnumLowerAB'    | 'X'   | 'paramEnumLowerAB: is not a valid type'    |
      | 'PUT'    | 'paramEnumLowerAB'    | 'X'   | 'paramEnumLowerAB: is not a valid type'    |
      | 'PATCH'  | 'paramEnumLowerAB'    | 'X'   | 'paramEnumLowerAB: is not a valid type'    |
      | 'DELETE' | 'paramEnumLowerAB'    | 'X'   | 'paramEnumLowerAB: is not a valid type'    |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/named-method-parameter?<param>=<value> endpoint (RequestParamUnconstrainedRestApi.class)
    this endpoint receives the request parameter in a method parameter, both having the diferent names, a custom request-parameter name is defined by the endpoint 
    and the service cannot map the sent value to the declared data type, responding with an error message.

    Given the endpoint '/api/v1/request-parameter/named-method-parameter'
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | param                    | value | message                                       |
      | 'GET'    | 'param-integer'          | 'X'   | 'param-integer: is not a valid type'          |
      | 'HEAD'   | 'param-integer'          | 'X'   |                                               |
      | 'POST'   | 'param-integer'          | 'X'   | 'param-integer: is not a valid type'          |
      | 'PUT'    | 'param-integer'          | 'X'   | 'param-integer: is not a valid type'          |
      | 'PATCH'  | 'param-integer'          | 'X'   | 'param-integer: is not a valid type'          |
      | 'DELETE' | 'param-integer'          | 'X'   | 'param-integer: is not a valid type'          |
      | 'GET'    | 'param-long'             | 'X'   | 'param-long: is not a valid type'             |
      | 'HEAD'   | 'param-long'             | 'X'   |                                               |
      | 'POST'   | 'param-long'             | 'X'   | 'param-long: is not a valid type'             |
      | 'PUT'    | 'param-long'             | 'X'   | 'param-long: is not a valid type'             |
      | 'PATCH'  | 'param-long'             | 'X'   | 'param-long: is not a valid type'             |
      | 'DELETE' | 'param-long'             | 'X'   | 'param-long: is not a valid type'             |
      | 'GET'    | 'param-big-integer'      | 'X'   | 'param-big-integer: is not a valid type'      |
      | 'HEAD'   | 'param-big-integer'      | 'X'   |                                               |
      | 'POST'   | 'param-big-integer'      | 'X'   | 'param-big-integer: is not a valid type'      |
      | 'PUT'    | 'param-big-integer'      | 'X'   | 'param-big-integer: is not a valid type'      |
      | 'PATCH'  | 'param-big-integer'      | 'X'   | 'param-big-integer: is not a valid type'      |
      | 'DELETE' | 'param-big-integer'      | 'X'   | 'param-big-integer: is not a valid type'      |
      | 'GET'    | 'param-float'            | 'X'   | 'param-float: is not a valid type'            |
      | 'HEAD'   | 'param-float'            | 'X'   |                                               |
      | 'POST'   | 'param-float'            | 'X'   | 'param-float: is not a valid type'            |
      | 'PUT'    | 'param-float'            | 'X'   | 'param-float: is not a valid type'            |
      | 'PATCH'  | 'param-float'            | 'X'   | 'param-float: is not a valid type'            |
      | 'DELETE' | 'param-float'            | 'X'   | 'param-float: is not a valid type'            |
      | 'GET'    | 'param-double'           | 'X'   | 'param-double: is not a valid type'           |
      | 'HEAD'   | 'param-double'           | 'X'   |                                               |
      | 'POST'   | 'param-double'           | 'X'   | 'param-double: is not a valid type'           |
      | 'PUT'    | 'param-double'           | 'X'   | 'param-double: is not a valid type'           |
      | 'PATCH'  | 'param-double'           | 'X'   | 'param-double: is not a valid type'           |
      | 'DELETE' | 'param-double'           | 'X'   | 'param-double: is not a valid type'           |
      | 'GET'    | 'param-big-decimal'      | 'X'   | 'param-big-decimal: is not a valid type'      |
      | 'HEAD'   | 'param-big-decimal'      | 'X'   |                                               |
      | 'POST'   | 'param-big-decimal'      | 'X'   | 'param-big-decimal: is not a valid type'      |
      | 'PUT'    | 'param-big-decimal'      | 'X'   | 'param-big-decimal: is not a valid type'      |
      | 'PATCH'  | 'param-big-decimal'      | 'X'   | 'param-big-decimal: is not a valid type'      |
      | 'DELETE' | 'param-big-decimal'      | 'X'   | 'param-big-decimal: is not a valid type'      |
      | 'GET'    | 'param-boolean'          | 'X'   | 'param-boolean: is not a valid type'          |
      | 'HEAD'   | 'param-boolean'          | 'X'   |                                               |
      | 'POST'   | 'param-boolean'          | 'X'   | 'param-boolean: is not a valid type'          |
      | 'PUT'    | 'param-boolean'          | 'X'   | 'param-boolean: is not a valid type'          |
      | 'PATCH'  | 'param-boolean'          | 'X'   | 'param-boolean: is not a valid type'          |
      | 'DELETE' | 'param-boolean'          | 'X'   | 'param-boolean: is not a valid type'          |
      | 'GET'    | 'param-offset-date-time' | 'X'   | 'param-offset-date-time: is not a valid type' |
      | 'HEAD'   | 'param-offset-date-time' | 'X'   |                                               |
      | 'POST'   | 'param-offset-date-time' | 'X'   | 'param-offset-date-time: is not a valid type' |
      | 'PUT'    | 'param-offset-date-time' | 'X'   | 'param-offset-date-time: is not a valid type' |
      | 'PATCH'  | 'param-offset-date-time' | 'X'   | 'param-offset-date-time: is not a valid type' |
      | 'DELETE' | 'param-offset-date-time' | 'X'   | 'param-offset-date-time: is not a valid type' |
      | 'GET'    | 'param-local-date-time'  | 'X'   | 'param-local-date-time: is not a valid type'  |
      | 'HEAD'   | 'param-local-date-time'  | 'X'   |                                               |
      | 'POST'   | 'param-local-date-time'  | 'X'   | 'param-local-date-time: is not a valid type'  |
      | 'PUT'    | 'param-local-date-time'  | 'X'   | 'param-local-date-time: is not a valid type'  |
      | 'PATCH'  | 'param-local-date-time'  | 'X'   | 'param-local-date-time: is not a valid type'  |
      | 'DELETE' | 'param-local-date-time'  | 'X'   | 'param-local-date-time: is not a valid type'  |
      | 'GET'    | 'param-local-date'       | 'X'   | 'param-local-date: is not a valid type'       |
      | 'HEAD'   | 'param-local-date'       | 'X'   |                                               |
      | 'POST'   | 'param-local-date'       | 'X'   | 'param-local-date: is not a valid type'       |
      | 'PUT'    | 'param-local-date'       | 'X'   | 'param-local-date: is not a valid type'       |
      | 'PATCH'  | 'param-local-date'       | 'X'   | 'param-local-date: is not a valid type'       |
      | 'DELETE' | 'param-local-date'       | 'X'   | 'param-local-date: is not a valid type'       |
      | 'GET'    | 'param-local-time'       | 'X'   | 'param-local-time: is not a valid type'       |
      | 'HEAD'   | 'param-local-time'       | 'X'   |                                               |
      | 'POST'   | 'param-local-time'       | 'X'   | 'param-local-time: is not a valid type'       |
      | 'PUT'    | 'param-local-time'       | 'X'   | 'param-local-time: is not a valid type'       |
      | 'PATCH'  | 'param-local-time'       | 'X'   | 'param-local-time: is not a valid type'       |
      | 'DELETE' | 'param-local-time'       | 'X'   | 'param-local-time: is not a valid type'       |
      | 'GET'    | 'param-year'             | 'X'   | 'param-year: is not a valid type'             |
      | 'HEAD'   | 'param-year'             | 'X'   |                                               |
      | 'POST'   | 'param-year'             | 'X'   | 'param-year: is not a valid type'             |
      | 'PUT'    | 'param-year'             | 'X'   | 'param-year: is not a valid type'             |
      | 'PATCH'  | 'param-year'             | 'X'   | 'param-year: is not a valid type'             |
      | 'DELETE' | 'param-year'             | 'X'   | 'param-year: is not a valid type'             |
      | 'GET'    | 'param-enum-ab'          | 'X'   | 'param-enum-ab: is not a valid type'          |
      | 'HEAD'   | 'param-enum-ab'          | 'X'   |                                               |
      | 'POST'   | 'param-enum-ab'          | 'X'   | 'param-enum-ab: is not a valid type'          |
      | 'PUT'    | 'param-enum-ab'          | 'X'   | 'param-enum-ab: is not a valid type'          |
      | 'PATCH'  | 'param-enum-ab'          | 'X'   | 'param-enum-ab: is not a valid type'          |
      | 'DELETE' | 'param-enum-ab'          | 'X'   | 'param-enum-ab: is not a valid type'          |
      | 'GET'    | 'param-enum-lower-ab'    | 'X'   | 'param-enum-lower-ab: is not a valid type'    |
      | 'HEAD'   | 'param-enum-lower-ab'    | 'X'   |                                               |
      | 'POST'   | 'param-enum-lower-ab'    | 'X'   | 'param-enum-lower-ab: is not a valid type'    |
      | 'PUT'    | 'param-enum-lower-ab'    | 'X'   | 'param-enum-lower-ab: is not a valid type'    |
      | 'PATCH'  | 'param-enum-lower-ab'    | 'X'   | 'param-enum-lower-ab: is not a valid type'    |
      | 'DELETE' | 'param-enum-lower-ab'    | 'X'   | 'param-enum-lower-ab: is not a valid type'    |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/optional-method-parameter?<param>=<value> endpoint (RequestParamUnconstrainedRestApi.class)
    this endpoint receives the request parameter in an optional method parameter, both having the same name
    and the service cannot map the sent value to the declared data type, responding with an error message.

    Given the endpoint '/api/v1/request-parameter/optional-method-parameter'
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | param                 | value | message                                    |
      | 'GET'    | 'paramInteger'        | 'X'   | 'paramInteger: is not a valid type'        |
      | 'HEAD'   | 'paramInteger'        | 'X'   |                                            |
      | 'POST'   | 'paramInteger'        | 'X'   | 'paramInteger: is not a valid type'        |
      | 'PUT'    | 'paramInteger'        | 'X'   | 'paramInteger: is not a valid type'        |
      | 'PATCH'  | 'paramInteger'        | 'X'   | 'paramInteger: is not a valid type'        |
      | 'DELETE' | 'paramInteger'        | 'X'   | 'paramInteger: is not a valid type'        |
      | 'GET'    | 'paramLong'           | 'X'   | 'paramLong: is not a valid type'           |
      | 'HEAD'   | 'paramLong'           | 'X'   |                                            |
      | 'POST'   | 'paramLong'           | 'X'   | 'paramLong: is not a valid type'           |
      | 'PUT'    | 'paramLong'           | 'X'   | 'paramLong: is not a valid type'           |
      | 'PATCH'  | 'paramLong'           | 'X'   | 'paramLong: is not a valid type'           |
      | 'DELETE' | 'paramLong'           | 'X'   | 'paramLong: is not a valid type'           |
      | 'GET'    | 'paramBigInteger'     | 'X'   | 'paramBigInteger: is not a valid type'     |
      | 'HEAD'   | 'paramBigInteger'     | 'X'   |                                            |
      | 'POST'   | 'paramBigInteger'     | 'X'   | 'paramBigInteger: is not a valid type'     |
      | 'PUT'    | 'paramBigInteger'     | 'X'   | 'paramBigInteger: is not a valid type'     |
      | 'PATCH'  | 'paramBigInteger'     | 'X'   | 'paramBigInteger: is not a valid type'     |
      | 'DELETE' | 'paramBigInteger'     | 'X'   | 'paramBigInteger: is not a valid type'     |
      | 'GET'    | 'paramFloat'          | 'X'   | 'paramFloat: is not a valid type'          |
      | 'HEAD'   | 'paramFloat'          | 'X'   |                                            |
      | 'POST'   | 'paramFloat'          | 'X'   | 'paramFloat: is not a valid type'          |
      | 'PUT'    | 'paramFloat'          | 'X'   | 'paramFloat: is not a valid type'          |
      | 'PATCH'  | 'paramFloat'          | 'X'   | 'paramFloat: is not a valid type'          |
      | 'DELETE' | 'paramFloat'          | 'X'   | 'paramFloat: is not a valid type'          |
      | 'GET'    | 'paramDouble'         | 'X'   | 'paramDouble: is not a valid type'         |
      | 'HEAD'   | 'paramDouble'         | 'X'   |                                            |
      | 'POST'   | 'paramDouble'         | 'X'   | 'paramDouble: is not a valid type'         |
      | 'PUT'    | 'paramDouble'         | 'X'   | 'paramDouble: is not a valid type'         |
      | 'PATCH'  | 'paramDouble'         | 'X'   | 'paramDouble: is not a valid type'         |
      | 'DELETE' | 'paramDouble'         | 'X'   | 'paramDouble: is not a valid type'         |
      | 'GET'    | 'paramBigDecimal'     | 'X'   | 'paramBigDecimal: is not a valid type'     |
      | 'HEAD'   | 'paramBigDecimal'     | 'X'   |                                            |
      | 'POST'   | 'paramBigDecimal'     | 'X'   | 'paramBigDecimal: is not a valid type'     |
      | 'PUT'    | 'paramBigDecimal'     | 'X'   | 'paramBigDecimal: is not a valid type'     |
      | 'PATCH'  | 'paramBigDecimal'     | 'X'   | 'paramBigDecimal: is not a valid type'     |
      | 'DELETE' | 'paramBigDecimal'     | 'X'   | 'paramBigDecimal: is not a valid type'     |
      | 'GET'    | 'paramBoolean'        | 'X'   | 'paramBoolean: is not a valid type'        |
      | 'HEAD'   | 'paramBoolean'        | 'X'   |                                            |
      | 'POST'   | 'paramBoolean'        | 'X'   | 'paramBoolean: is not a valid type'        |
      | 'PUT'    | 'paramBoolean'        | 'X'   | 'paramBoolean: is not a valid type'        |
      | 'PATCH'  | 'paramBoolean'        | 'X'   | 'paramBoolean: is not a valid type'        |
      | 'DELETE' | 'paramBoolean'        | 'X'   | 'paramBoolean: is not a valid type'        |
      | 'GET'    | 'paramOffsetDateTime' | 'X'   | 'paramOffsetDateTime: is not a valid type' |
      | 'HEAD'   | 'paramOffsetDateTime' | 'X'   |                                            |
      | 'POST'   | 'paramOffsetDateTime' | 'X'   | 'paramOffsetDateTime: is not a valid type' |
      | 'PUT'    | 'paramOffsetDateTime' | 'X'   | 'paramOffsetDateTime: is not a valid type' |
      | 'PATCH'  | 'paramOffsetDateTime' | 'X'   | 'paramOffsetDateTime: is not a valid type' |
      | 'DELETE' | 'paramOffsetDateTime' | 'X'   | 'paramOffsetDateTime: is not a valid type' |
      | 'GET'    | 'paramLocalDateTime'  | 'X'   | 'paramLocalDateTime: is not a valid type'  |
      | 'HEAD'   | 'paramLocalDateTime'  | 'X'   |                                            |
      | 'POST'   | 'paramLocalDateTime'  | 'X'   | 'paramLocalDateTime: is not a valid type'  |
      | 'PUT'    | 'paramLocalDateTime'  | 'X'   | 'paramLocalDateTime: is not a valid type'  |
      | 'PATCH'  | 'paramLocalDateTime'  | 'X'   | 'paramLocalDateTime: is not a valid type'  |
      | 'DELETE' | 'paramLocalDateTime'  | 'X'   | 'paramLocalDateTime: is not a valid type'  |
      | 'GET'    | 'paramLocalDate'      | 'X'   | 'paramLocalDate: is not a valid type'      |
      | 'HEAD'   | 'paramLocalDate'      | 'X'   |                                            |
      | 'POST'   | 'paramLocalDate'      | 'X'   | 'paramLocalDate: is not a valid type'      |
      | 'PUT'    | 'paramLocalDate'      | 'X'   | 'paramLocalDate: is not a valid type'      |
      | 'PATCH'  | 'paramLocalDate'      | 'X'   | 'paramLocalDate: is not a valid type'      |
      | 'DELETE' | 'paramLocalDate'      | 'X'   | 'paramLocalDate: is not a valid type'      |
      | 'GET'    | 'paramLocalTime'      | 'X'   | 'paramLocalTime: is not a valid type'      |
      | 'HEAD'   | 'paramLocalTime'      | 'X'   |                                            |
      | 'POST'   | 'paramLocalTime'      | 'X'   | 'paramLocalTime: is not a valid type'      |
      | 'PUT'    | 'paramLocalTime'      | 'X'   | 'paramLocalTime: is not a valid type'      |
      | 'PATCH'  | 'paramLocalTime'      | 'X'   | 'paramLocalTime: is not a valid type'      |
      | 'DELETE' | 'paramLocalTime'      | 'X'   | 'paramLocalTime: is not a valid type'      |
      | 'GET'    | 'paramYear'           | 'X'   | 'paramYear: is not a valid type'           |
      | 'HEAD'   | 'paramYear'           | 'X'   |                                            |
      | 'POST'   | 'paramYear'           | 'X'   | 'paramYear: is not a valid type'           |
      | 'PUT'    | 'paramYear'           | 'X'   | 'paramYear: is not a valid type'           |
      | 'PATCH'  | 'paramYear'           | 'X'   | 'paramYear: is not a valid type'           |
      | 'DELETE' | 'paramYear'           | 'X'   | 'paramYear: is not a valid type'           |
      | 'GET'    | 'paramEnumAB'         | 'X'   | 'paramEnumAB: is not a valid type'         |
      | 'HEAD'   | 'paramEnumAB'         | 'X'   |                                            |
      | 'POST'   | 'paramEnumAB'         | 'X'   | 'paramEnumAB: is not a valid type'         |
      | 'PUT'    | 'paramEnumAB'         | 'X'   | 'paramEnumAB: is not a valid type'         |
      | 'PATCH'  | 'paramEnumAB'         | 'X'   | 'paramEnumAB: is not a valid type'         |
      | 'DELETE' | 'paramEnumAB'         | 'X'   | 'paramEnumAB: is not a valid type'         |
      | 'GET'    | 'paramEnumLowerAB'    | 'X'   | 'paramEnumLowerAB: is not a valid type'    |
      | 'HEAD'   | 'paramEnumLowerAB'    | 'X'   |                                            |
      | 'POST'   | 'paramEnumLowerAB'    | 'X'   | 'paramEnumLowerAB: is not a valid type'    |
      | 'PUT'    | 'paramEnumLowerAB'    | 'X'   | 'paramEnumLowerAB: is not a valid type'    |
      | 'PATCH'  | 'paramEnumLowerAB'    | 'X'   | 'paramEnumLowerAB: is not a valid type'    |
      | 'DELETE' | 'paramEnumLowerAB'    | 'X'   | 'paramEnumLowerAB: is not a valid type'    |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/named-optional-method-parameter?<param>=<value> endpoint (RequestParamUnconstrainedRestApi.class)
    this endpoint receives the request parameter in an optional method parameter, both having the diferent names, a custom request-parameter name is defined by the endpoint 
    and the service cannot map the sent value to the declared data type, responding with an error message.

    Given the endpoint '/api/v1/request-parameter/named-optional-method-parameter'
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | param                    | value | message                                       |
      | 'GET'    | 'param-integer'          | 'X'   | 'param-integer: is not a valid type'          |
      | 'HEAD'   | 'param-integer'          | 'X'   |                                               |
      | 'POST'   | 'param-integer'          | 'X'   | 'param-integer: is not a valid type'          |
      | 'PUT'    | 'param-integer'          | 'X'   | 'param-integer: is not a valid type'          |
      | 'PATCH'  | 'param-integer'          | 'X'   | 'param-integer: is not a valid type'          |
      | 'DELETE' | 'param-integer'          | 'X'   | 'param-integer: is not a valid type'          |
      | 'GET'    | 'param-long'             | 'X'   | 'param-long: is not a valid type'             |
      | 'HEAD'   | 'param-long'             | 'X'   |                                               |
      | 'POST'   | 'param-long'             | 'X'   | 'param-long: is not a valid type'             |
      | 'PUT'    | 'param-long'             | 'X'   | 'param-long: is not a valid type'             |
      | 'PATCH'  | 'param-long'             | 'X'   | 'param-long: is not a valid type'             |
      | 'DELETE' | 'param-long'             | 'X'   | 'param-long: is not a valid type'             |
      | 'GET'    | 'param-big-integer'      | 'X'   | 'param-big-integer: is not a valid type'      |
      | 'HEAD'   | 'param-big-integer'      | 'X'   |                                               |
      | 'POST'   | 'param-big-integer'      | 'X'   | 'param-big-integer: is not a valid type'      |
      | 'PUT'    | 'param-big-integer'      | 'X'   | 'param-big-integer: is not a valid type'      |
      | 'PATCH'  | 'param-big-integer'      | 'X'   | 'param-big-integer: is not a valid type'      |
      | 'DELETE' | 'param-big-integer'      | 'X'   | 'param-big-integer: is not a valid type'      |
      | 'GET'    | 'param-float'            | 'X'   | 'param-float: is not a valid type'            |
      | 'HEAD'   | 'param-float'            | 'X'   |                                               |
      | 'POST'   | 'param-float'            | 'X'   | 'param-float: is not a valid type'            |
      | 'PUT'    | 'param-float'            | 'X'   | 'param-float: is not a valid type'            |
      | 'PATCH'  | 'param-float'            | 'X'   | 'param-float: is not a valid type'            |
      | 'DELETE' | 'param-float'            | 'X'   | 'param-float: is not a valid type'            |
      | 'GET'    | 'param-double'           | 'X'   | 'param-double: is not a valid type'           |
      | 'HEAD'   | 'param-double'           | 'X'   |                                               |
      | 'POST'   | 'param-double'           | 'X'   | 'param-double: is not a valid type'           |
      | 'PUT'    | 'param-double'           | 'X'   | 'param-double: is not a valid type'           |
      | 'PATCH'  | 'param-double'           | 'X'   | 'param-double: is not a valid type'           |
      | 'DELETE' | 'param-double'           | 'X'   | 'param-double: is not a valid type'           |
      | 'GET'    | 'param-big-decimal'      | 'X'   | 'param-big-decimal: is not a valid type'      |
      | 'HEAD'   | 'param-big-decimal'      | 'X'   |                                               |
      | 'POST'   | 'param-big-decimal'      | 'X'   | 'param-big-decimal: is not a valid type'      |
      | 'PUT'    | 'param-big-decimal'      | 'X'   | 'param-big-decimal: is not a valid type'      |
      | 'PATCH'  | 'param-big-decimal'      | 'X'   | 'param-big-decimal: is not a valid type'      |
      | 'DELETE' | 'param-big-decimal'      | 'X'   | 'param-big-decimal: is not a valid type'      |
      | 'GET'    | 'param-boolean'          | 'X'   | 'param-boolean: is not a valid type'          |
      | 'HEAD'   | 'param-boolean'          | 'X'   |                                               |
      | 'POST'   | 'param-boolean'          | 'X'   | 'param-boolean: is not a valid type'          |
      | 'PUT'    | 'param-boolean'          | 'X'   | 'param-boolean: is not a valid type'          |
      | 'PATCH'  | 'param-boolean'          | 'X'   | 'param-boolean: is not a valid type'          |
      | 'DELETE' | 'param-boolean'          | 'X'   | 'param-boolean: is not a valid type'          |
      | 'GET'    | 'param-offset-date-time' | 'X'   | 'param-offset-date-time: is not a valid type' |
      | 'HEAD'   | 'param-offset-date-time' | 'X'   |                                               |
      | 'POST'   | 'param-offset-date-time' | 'X'   | 'param-offset-date-time: is not a valid type' |
      | 'PUT'    | 'param-offset-date-time' | 'X'   | 'param-offset-date-time: is not a valid type' |
      | 'PATCH'  | 'param-offset-date-time' | 'X'   | 'param-offset-date-time: is not a valid type' |
      | 'DELETE' | 'param-offset-date-time' | 'X'   | 'param-offset-date-time: is not a valid type' |
      | 'GET'    | 'param-local-date-time'  | 'X'   | 'param-local-date-time: is not a valid type'  |
      | 'HEAD'   | 'param-local-date-time'  | 'X'   |                                               |
      | 'POST'   | 'param-local-date-time'  | 'X'   | 'param-local-date-time: is not a valid type'  |
      | 'PUT'    | 'param-local-date-time'  | 'X'   | 'param-local-date-time: is not a valid type'  |
      | 'PATCH'  | 'param-local-date-time'  | 'X'   | 'param-local-date-time: is not a valid type'  |
      | 'DELETE' | 'param-local-date-time'  | 'X'   | 'param-local-date-time: is not a valid type'  |
      | 'GET'    | 'param-local-date'       | 'X'   | 'param-local-date: is not a valid type'       |
      | 'HEAD'   | 'param-local-date'       | 'X'   |                                               |
      | 'POST'   | 'param-local-date'       | 'X'   | 'param-local-date: is not a valid type'       |
      | 'PUT'    | 'param-local-date'       | 'X'   | 'param-local-date: is not a valid type'       |
      | 'PATCH'  | 'param-local-date'       | 'X'   | 'param-local-date: is not a valid type'       |
      | 'DELETE' | 'param-local-date'       | 'X'   | 'param-local-date: is not a valid type'       |
      | 'GET'    | 'param-local-time'       | 'X'   | 'param-local-time: is not a valid type'       |
      | 'HEAD'   | 'param-local-time'       | 'X'   |                                               |
      | 'POST'   | 'param-local-time'       | 'X'   | 'param-local-time: is not a valid type'       |
      | 'PUT'    | 'param-local-time'       | 'X'   | 'param-local-time: is not a valid type'       |
      | 'PATCH'  | 'param-local-time'       | 'X'   | 'param-local-time: is not a valid type'       |
      | 'DELETE' | 'param-local-time'       | 'X'   | 'param-local-time: is not a valid type'       |
      | 'GET'    | 'param-year'             | 'X'   | 'param-year: is not a valid type'             |
      | 'HEAD'   | 'param-year'             | 'X'   |                                               |
      | 'POST'   | 'param-year'             | 'X'   | 'param-year: is not a valid type'             |
      | 'PUT'    | 'param-year'             | 'X'   | 'param-year: is not a valid type'             |
      | 'PATCH'  | 'param-year'             | 'X'   | 'param-year: is not a valid type'             |
      | 'DELETE' | 'param-year'             | 'X'   | 'param-year: is not a valid type'             |
      | 'GET'    | 'param-enum-ab'          | 'X'   | 'param-enum-ab: is not a valid type'          |
      | 'HEAD'   | 'param-enum-ab'          | 'X'   |                                               |
      | 'POST'   | 'param-enum-ab'          | 'X'   | 'param-enum-ab: is not a valid type'          |
      | 'PUT'    | 'param-enum-ab'          | 'X'   | 'param-enum-ab: is not a valid type'          |
      | 'PATCH'  | 'param-enum-ab'          | 'X'   | 'param-enum-ab: is not a valid type'          |
      | 'DELETE' | 'param-enum-ab'          | 'X'   | 'param-enum-ab: is not a valid type'          |
      | 'GET'    | 'param-enum-lower-ab'    | 'X'   | 'param-enum-lower-ab: is not a valid type'    |
      | 'HEAD'   | 'param-enum-lower-ab'    | 'X'   |                                               |
      | 'POST'   | 'param-enum-lower-ab'    | 'X'   | 'param-enum-lower-ab: is not a valid type'    |
      | 'PUT'    | 'param-enum-lower-ab'    | 'X'   | 'param-enum-lower-ab: is not a valid type'    |
      | 'PATCH'  | 'param-enum-lower-ab'    | 'X'   | 'param-enum-lower-ab: is not a valid type'    |
      | 'DELETE' | 'param-enum-lower-ab'    | 'X'   | 'param-enum-lower-ab: is not a valid type'    |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/dto-attribute?<param>=<value> endpoint (RequestParamUnconstrainedRestApi.class)
    this endpoint receives the request parameter in a dto attribute, both having the same name
    and the service cannot map the sent value to the declared data type, responding with an error message.

    Given the endpoint '/api/v1/request-parameter/dto-attribute'
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | param                 | value | message                                    |
      | 'GET'    | 'paramInteger'        | 'X'   | 'paramInteger: is not a valid type'        |
      | 'HEAD'   | 'paramInteger'        | 'X'   |                                            |
      | 'POST'   | 'paramInteger'        | 'X'   | 'paramInteger: is not a valid type'        |
      | 'PUT'    | 'paramInteger'        | 'X'   | 'paramInteger: is not a valid type'        |
      | 'PATCH'  | 'paramInteger'        | 'X'   | 'paramInteger: is not a valid type'        |
      | 'DELETE' | 'paramInteger'        | 'X'   | 'paramInteger: is not a valid type'        |
      | 'GET'    | 'paramLong'           | 'X'   | 'paramLong: is not a valid type'           |
      | 'HEAD'   | 'paramLong'           | 'X'   |                                            |
      | 'POST'   | 'paramLong'           | 'X'   | 'paramLong: is not a valid type'           |
      | 'PUT'    | 'paramLong'           | 'X'   | 'paramLong: is not a valid type'           |
      | 'PATCH'  | 'paramLong'           | 'X'   | 'paramLong: is not a valid type'           |
      | 'DELETE' | 'paramLong'           | 'X'   | 'paramLong: is not a valid type'           |
      | 'GET'    | 'paramBigInteger'     | 'X'   | 'paramBigInteger: is not a valid type'     |
      | 'HEAD'   | 'paramBigInteger'     | 'X'   |                                            |
      | 'POST'   | 'paramBigInteger'     | 'X'   | 'paramBigInteger: is not a valid type'     |
      | 'PUT'    | 'paramBigInteger'     | 'X'   | 'paramBigInteger: is not a valid type'     |
      | 'PATCH'  | 'paramBigInteger'     | 'X'   | 'paramBigInteger: is not a valid type'     |
      | 'DELETE' | 'paramBigInteger'     | 'X'   | 'paramBigInteger: is not a valid type'     |
      | 'GET'    | 'paramFloat'          | 'X'   | 'paramFloat: is not a valid type'          |
      | 'HEAD'   | 'paramFloat'          | 'X'   |                                            |
      | 'POST'   | 'paramFloat'          | 'X'   | 'paramFloat: is not a valid type'          |
      | 'PUT'    | 'paramFloat'          | 'X'   | 'paramFloat: is not a valid type'          |
      | 'PATCH'  | 'paramFloat'          | 'X'   | 'paramFloat: is not a valid type'          |
      | 'DELETE' | 'paramFloat'          | 'X'   | 'paramFloat: is not a valid type'          |
      | 'GET'    | 'paramDouble'         | 'X'   | 'paramDouble: is not a valid type'         |
      | 'HEAD'   | 'paramDouble'         | 'X'   |                                            |
      | 'POST'   | 'paramDouble'         | 'X'   | 'paramDouble: is not a valid type'         |
      | 'PUT'    | 'paramDouble'         | 'X'   | 'paramDouble: is not a valid type'         |
      | 'PATCH'  | 'paramDouble'         | 'X'   | 'paramDouble: is not a valid type'         |
      | 'DELETE' | 'paramDouble'         | 'X'   | 'paramDouble: is not a valid type'         |
      | 'GET'    | 'paramBigDecimal'     | 'X'   | 'paramBigDecimal: is not a valid type'     |
      | 'HEAD'   | 'paramBigDecimal'     | 'X'   |                                            |
      | 'POST'   | 'paramBigDecimal'     | 'X'   | 'paramBigDecimal: is not a valid type'     |
      | 'PUT'    | 'paramBigDecimal'     | 'X'   | 'paramBigDecimal: is not a valid type'     |
      | 'PATCH'  | 'paramBigDecimal'     | 'X'   | 'paramBigDecimal: is not a valid type'     |
      | 'DELETE' | 'paramBigDecimal'     | 'X'   | 'paramBigDecimal: is not a valid type'     |
      | 'GET'    | 'paramBoolean'        | 'X'   | 'paramBoolean: is not a valid type'        |
      | 'HEAD'   | 'paramBoolean'        | 'X'   |                                            |
      | 'POST'   | 'paramBoolean'        | 'X'   | 'paramBoolean: is not a valid type'        |
      | 'PUT'    | 'paramBoolean'        | 'X'   | 'paramBoolean: is not a valid type'        |
      | 'PATCH'  | 'paramBoolean'        | 'X'   | 'paramBoolean: is not a valid type'        |
      | 'DELETE' | 'paramBoolean'        | 'X'   | 'paramBoolean: is not a valid type'        |
      | 'GET'    | 'paramOffsetDateTime' | 'X'   | 'paramOffsetDateTime: is not a valid type' |
      | 'HEAD'   | 'paramOffsetDateTime' | 'X'   |                                            |
      | 'POST'   | 'paramOffsetDateTime' | 'X'   | 'paramOffsetDateTime: is not a valid type' |
      | 'PUT'    | 'paramOffsetDateTime' | 'X'   | 'paramOffsetDateTime: is not a valid type' |
      | 'PATCH'  | 'paramOffsetDateTime' | 'X'   | 'paramOffsetDateTime: is not a valid type' |
      | 'DELETE' | 'paramOffsetDateTime' | 'X'   | 'paramOffsetDateTime: is not a valid type' |
      | 'GET'    | 'paramLocalDateTime'  | 'X'   | 'paramLocalDateTime: is not a valid type'  |
      | 'HEAD'   | 'paramLocalDateTime'  | 'X'   |                                            |
      | 'POST'   | 'paramLocalDateTime'  | 'X'   | 'paramLocalDateTime: is not a valid type'  |
      | 'PUT'    | 'paramLocalDateTime'  | 'X'   | 'paramLocalDateTime: is not a valid type'  |
      | 'PATCH'  | 'paramLocalDateTime'  | 'X'   | 'paramLocalDateTime: is not a valid type'  |
      | 'DELETE' | 'paramLocalDateTime'  | 'X'   | 'paramLocalDateTime: is not a valid type'  |
      | 'GET'    | 'paramLocalDate'      | 'X'   | 'paramLocalDate: is not a valid type'      |
      | 'HEAD'   | 'paramLocalDate'      | 'X'   |                                            |
      | 'POST'   | 'paramLocalDate'      | 'X'   | 'paramLocalDate: is not a valid type'      |
      | 'PUT'    | 'paramLocalDate'      | 'X'   | 'paramLocalDate: is not a valid type'      |
      | 'PATCH'  | 'paramLocalDate'      | 'X'   | 'paramLocalDate: is not a valid type'      |
      | 'DELETE' | 'paramLocalDate'      | 'X'   | 'paramLocalDate: is not a valid type'      |
      | 'GET'    | 'paramLocalTime'      | 'X'   | 'paramLocalTime: is not a valid type'      |
      | 'HEAD'   | 'paramLocalTime'      | 'X'   |                                            |
      | 'POST'   | 'paramLocalTime'      | 'X'   | 'paramLocalTime: is not a valid type'      |
      | 'PUT'    | 'paramLocalTime'      | 'X'   | 'paramLocalTime: is not a valid type'      |
      | 'PATCH'  | 'paramLocalTime'      | 'X'   | 'paramLocalTime: is not a valid type'      |
      | 'DELETE' | 'paramLocalTime'      | 'X'   | 'paramLocalTime: is not a valid type'      |
      | 'GET'    | 'paramYear'           | 'X'   | 'paramYear: is not a valid type'           |
      | 'HEAD'   | 'paramYear'           | 'X'   |                                            |
      | 'POST'   | 'paramYear'           | 'X'   | 'paramYear: is not a valid type'           |
      | 'PUT'    | 'paramYear'           | 'X'   | 'paramYear: is not a valid type'           |
      | 'PATCH'  | 'paramYear'           | 'X'   | 'paramYear: is not a valid type'           |
      | 'DELETE' | 'paramYear'           | 'X'   | 'paramYear: is not a valid type'           |
      | 'GET'    | 'paramEnumAB'         | 'X'   | 'paramEnumAB: is not a valid type'         |
      | 'HEAD'   | 'paramEnumAB'         | 'X'   |                                            |
      | 'POST'   | 'paramEnumAB'         | 'X'   | 'paramEnumAB: is not a valid type'         |
      | 'PUT'    | 'paramEnumAB'         | 'X'   | 'paramEnumAB: is not a valid type'         |
      | 'PATCH'  | 'paramEnumAB'         | 'X'   | 'paramEnumAB: is not a valid type'         |
      | 'DELETE' | 'paramEnumAB'         | 'X'   | 'paramEnumAB: is not a valid type'         |
      | 'GET'    | 'paramEnumLowerAB'    | 'X'   | 'paramEnumLowerAB: is not a valid type'    |
      | 'HEAD'   | 'paramEnumLowerAB'    | 'X'   |                                            |
      | 'POST'   | 'paramEnumLowerAB'    | 'X'   | 'paramEnumLowerAB: is not a valid type'    |
      | 'PUT'    | 'paramEnumLowerAB'    | 'X'   | 'paramEnumLowerAB: is not a valid type'    |
      | 'PATCH'  | 'paramEnumLowerAB'    | 'X'   | 'paramEnumLowerAB: is not a valid type'    |
      | 'DELETE' | 'paramEnumLowerAB'    | 'X'   | 'paramEnumLowerAB: is not a valid type'    |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/optional-dto-attribute?<param>=<value> endpoint (RequestParamUnconstrainedRestApi.class)
    this endpoint receives the request parameter in an optional dto attribute, both having the same name
    and the service cannot map the sent value to the declared data type, responding with an error message.

    Given the endpoint '/api/v1/request-parameter/optional-dto-attribute'
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | param                 | value | message                                    |
      | 'GET'    | 'paramInteger'        | 'X'   | 'paramInteger: is not a valid type'        |
      | 'HEAD'   | 'paramInteger'        | 'X'   |                                            |
      | 'POST'   | 'paramInteger'        | 'X'   | 'paramInteger: is not a valid type'        |
      | 'PUT'    | 'paramInteger'        | 'X'   | 'paramInteger: is not a valid type'        |
      | 'PATCH'  | 'paramInteger'        | 'X'   | 'paramInteger: is not a valid type'        |
      | 'DELETE' | 'paramInteger'        | 'X'   | 'paramInteger: is not a valid type'        |
      | 'GET'    | 'paramLong'           | 'X'   | 'paramLong: is not a valid type'           |
      | 'HEAD'   | 'paramLong'           | 'X'   |                                            |
      | 'POST'   | 'paramLong'           | 'X'   | 'paramLong: is not a valid type'           |
      | 'PUT'    | 'paramLong'           | 'X'   | 'paramLong: is not a valid type'           |
      | 'PATCH'  | 'paramLong'           | 'X'   | 'paramLong: is not a valid type'           |
      | 'DELETE' | 'paramLong'           | 'X'   | 'paramLong: is not a valid type'           |
      | 'GET'    | 'paramBigInteger'     | 'X'   | 'paramBigInteger: is not a valid type'     |
      | 'HEAD'   | 'paramBigInteger'     | 'X'   |                                            |
      | 'POST'   | 'paramBigInteger'     | 'X'   | 'paramBigInteger: is not a valid type'     |
      | 'PUT'    | 'paramBigInteger'     | 'X'   | 'paramBigInteger: is not a valid type'     |
      | 'PATCH'  | 'paramBigInteger'     | 'X'   | 'paramBigInteger: is not a valid type'     |
      | 'DELETE' | 'paramBigInteger'     | 'X'   | 'paramBigInteger: is not a valid type'     |
      | 'GET'    | 'paramFloat'          | 'X'   | 'paramFloat: is not a valid type'          |
      | 'HEAD'   | 'paramFloat'          | 'X'   |                                            |
      | 'POST'   | 'paramFloat'          | 'X'   | 'paramFloat: is not a valid type'          |
      | 'PUT'    | 'paramFloat'          | 'X'   | 'paramFloat: is not a valid type'          |
      | 'PATCH'  | 'paramFloat'          | 'X'   | 'paramFloat: is not a valid type'          |
      | 'DELETE' | 'paramFloat'          | 'X'   | 'paramFloat: is not a valid type'          |
      | 'GET'    | 'paramDouble'         | 'X'   | 'paramDouble: is not a valid type'         |
      | 'HEAD'   | 'paramDouble'         | 'X'   |                                            |
      | 'POST'   | 'paramDouble'         | 'X'   | 'paramDouble: is not a valid type'         |
      | 'PUT'    | 'paramDouble'         | 'X'   | 'paramDouble: is not a valid type'         |
      | 'PATCH'  | 'paramDouble'         | 'X'   | 'paramDouble: is not a valid type'         |
      | 'DELETE' | 'paramDouble'         | 'X'   | 'paramDouble: is not a valid type'         |
      | 'GET'    | 'paramBigDecimal'     | 'X'   | 'paramBigDecimal: is not a valid type'     |
      | 'HEAD'   | 'paramBigDecimal'     | 'X'   |                                            |
      | 'POST'   | 'paramBigDecimal'     | 'X'   | 'paramBigDecimal: is not a valid type'     |
      | 'PUT'    | 'paramBigDecimal'     | 'X'   | 'paramBigDecimal: is not a valid type'     |
      | 'PATCH'  | 'paramBigDecimal'     | 'X'   | 'paramBigDecimal: is not a valid type'     |
      | 'DELETE' | 'paramBigDecimal'     | 'X'   | 'paramBigDecimal: is not a valid type'     |
      | 'GET'    | 'paramBoolean'        | 'X'   | 'paramBoolean: is not a valid type'        |
      | 'HEAD'   | 'paramBoolean'        | 'X'   |                                            |
      | 'POST'   | 'paramBoolean'        | 'X'   | 'paramBoolean: is not a valid type'        |
      | 'PUT'    | 'paramBoolean'        | 'X'   | 'paramBoolean: is not a valid type'        |
      | 'PATCH'  | 'paramBoolean'        | 'X'   | 'paramBoolean: is not a valid type'        |
      | 'DELETE' | 'paramBoolean'        | 'X'   | 'paramBoolean: is not a valid type'        |
      | 'GET'    | 'paramOffsetDateTime' | 'X'   | 'paramOffsetDateTime: is not a valid type' |
      | 'HEAD'   | 'paramOffsetDateTime' | 'X'   |                                            |
      | 'POST'   | 'paramOffsetDateTime' | 'X'   | 'paramOffsetDateTime: is not a valid type' |
      | 'PUT'    | 'paramOffsetDateTime' | 'X'   | 'paramOffsetDateTime: is not a valid type' |
      | 'PATCH'  | 'paramOffsetDateTime' | 'X'   | 'paramOffsetDateTime: is not a valid type' |
      | 'DELETE' | 'paramOffsetDateTime' | 'X'   | 'paramOffsetDateTime: is not a valid type' |
      | 'GET'    | 'paramLocalDateTime'  | 'X'   | 'paramLocalDateTime: is not a valid type'  |
      | 'HEAD'   | 'paramLocalDateTime'  | 'X'   |                                            |
      | 'POST'   | 'paramLocalDateTime'  | 'X'   | 'paramLocalDateTime: is not a valid type'  |
      | 'PUT'    | 'paramLocalDateTime'  | 'X'   | 'paramLocalDateTime: is not a valid type'  |
      | 'PATCH'  | 'paramLocalDateTime'  | 'X'   | 'paramLocalDateTime: is not a valid type'  |
      | 'DELETE' | 'paramLocalDateTime'  | 'X'   | 'paramLocalDateTime: is not a valid type'  |
      | 'GET'    | 'paramLocalDate'      | 'X'   | 'paramLocalDate: is not a valid type'      |
      | 'HEAD'   | 'paramLocalDate'      | 'X'   |                                            |
      | 'POST'   | 'paramLocalDate'      | 'X'   | 'paramLocalDate: is not a valid type'      |
      | 'PUT'    | 'paramLocalDate'      | 'X'   | 'paramLocalDate: is not a valid type'      |
      | 'PATCH'  | 'paramLocalDate'      | 'X'   | 'paramLocalDate: is not a valid type'      |
      | 'DELETE' | 'paramLocalDate'      | 'X'   | 'paramLocalDate: is not a valid type'      |
      | 'GET'    | 'paramLocalTime'      | 'X'   | 'paramLocalTime: is not a valid type'      |
      | 'HEAD'   | 'paramLocalTime'      | 'X'   |                                            |
      | 'POST'   | 'paramLocalTime'      | 'X'   | 'paramLocalTime: is not a valid type'      |
      | 'PUT'    | 'paramLocalTime'      | 'X'   | 'paramLocalTime: is not a valid type'      |
      | 'PATCH'  | 'paramLocalTime'      | 'X'   | 'paramLocalTime: is not a valid type'      |
      | 'DELETE' | 'paramLocalTime'      | 'X'   | 'paramLocalTime: is not a valid type'      |
      | 'GET'    | 'paramYear'           | 'X'   | 'paramYear: is not a valid type'           |
      | 'HEAD'   | 'paramYear'           | 'X'   |                                            |
      | 'POST'   | 'paramYear'           | 'X'   | 'paramYear: is not a valid type'           |
      | 'PUT'    | 'paramYear'           | 'X'   | 'paramYear: is not a valid type'           |
      | 'PATCH'  | 'paramYear'           | 'X'   | 'paramYear: is not a valid type'           |
      | 'DELETE' | 'paramYear'           | 'X'   | 'paramYear: is not a valid type'           |
      | 'GET'    | 'paramEnumAB'         | 'X'   | 'paramEnumAB: is not a valid type'         |
      | 'HEAD'   | 'paramEnumAB'         | 'X'   |                                            |
      | 'POST'   | 'paramEnumAB'         | 'X'   | 'paramEnumAB: is not a valid type'         |
      | 'PUT'    | 'paramEnumAB'         | 'X'   | 'paramEnumAB: is not a valid type'         |
      | 'PATCH'  | 'paramEnumAB'         | 'X'   | 'paramEnumAB: is not a valid type'         |
      | 'DELETE' | 'paramEnumAB'         | 'X'   | 'paramEnumAB: is not a valid type'         |
      | 'GET'    | 'paramEnumLowerAB'    | 'X'   | 'paramEnumLowerAB: is not a valid type'    |
      | 'HEAD'   | 'paramEnumLowerAB'    | 'X'   |                                            |
      | 'POST'   | 'paramEnumLowerAB'    | 'X'   | 'paramEnumLowerAB: is not a valid type'    |
      | 'PUT'    | 'paramEnumLowerAB'    | 'X'   | 'paramEnumLowerAB: is not a valid type'    |
      | 'PATCH'  | 'paramEnumLowerAB'    | 'X'   | 'paramEnumLowerAB: is not a valid type'    |
      | 'DELETE' | 'paramEnumLowerAB'    | 'X'   | 'paramEnumLowerAB: is not a valid type'    |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/nested-dto-attribute?<param>=<value> endpoint (RequestParamUnconstrainedRestApi.class)
    this endpoint receives the request parameter in a nested dto attribute, both having the same name
    and the service cannot map the sent value to the declared data type, responding with an error message.

    Given the endpoint '/api/v1/request-parameter/nested-dto-attribute'
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | param                        | value | message                                           |
      | 'GET'    | 'nested.paramInteger'        | 'X'   | 'nested.paramInteger: is not a valid type'        |
      | 'HEAD'   | 'nested.paramInteger'        | 'X'   |                                                   |
      | 'POST'   | 'nested.paramInteger'        | 'X'   | 'nested.paramInteger: is not a valid type'        |
      | 'PUT'    | 'nested.paramInteger'        | 'X'   | 'nested.paramInteger: is not a valid type'        |
      | 'PATCH'  | 'nested.paramInteger'        | 'X'   | 'nested.paramInteger: is not a valid type'        |
      | 'DELETE' | 'nested.paramInteger'        | 'X'   | 'nested.paramInteger: is not a valid type'        |
      | 'GET'    | 'nested.paramLong'           | 'X'   | 'nested.paramLong: is not a valid type'           |
      | 'HEAD'   | 'nested.paramLong'           | 'X'   |                                                   |
      | 'POST'   | 'nested.paramLong'           | 'X'   | 'nested.paramLong: is not a valid type'           |
      | 'PUT'    | 'nested.paramLong'           | 'X'   | 'nested.paramLong: is not a valid type'           |
      | 'PATCH'  | 'nested.paramLong'           | 'X'   | 'nested.paramLong: is not a valid type'           |
      | 'DELETE' | 'nested.paramLong'           | 'X'   | 'nested.paramLong: is not a valid type'           |
      | 'GET'    | 'nested.paramBigInteger'     | 'X'   | 'nested.paramBigInteger: is not a valid type'     |
      | 'HEAD'   | 'nested.paramBigInteger'     | 'X'   |                                                   |
      | 'POST'   | 'nested.paramBigInteger'     | 'X'   | 'nested.paramBigInteger: is not a valid type'     |
      | 'PUT'    | 'nested.paramBigInteger'     | 'X'   | 'nested.paramBigInteger: is not a valid type'     |
      | 'PATCH'  | 'nested.paramBigInteger'     | 'X'   | 'nested.paramBigInteger: is not a valid type'     |
      | 'DELETE' | 'nested.paramBigInteger'     | 'X'   | 'nested.paramBigInteger: is not a valid type'     |
      | 'GET'    | 'nested.paramFloat'          | 'X'   | 'nested.paramFloat: is not a valid type'          |
      | 'HEAD'   | 'nested.paramFloat'          | 'X'   |                                                   |
      | 'POST'   | 'nested.paramFloat'          | 'X'   | 'nested.paramFloat: is not a valid type'          |
      | 'PUT'    | 'nested.paramFloat'          | 'X'   | 'nested.paramFloat: is not a valid type'          |
      | 'PATCH'  | 'nested.paramFloat'          | 'X'   | 'nested.paramFloat: is not a valid type'          |
      | 'DELETE' | 'nested.paramFloat'          | 'X'   | 'nested.paramFloat: is not a valid type'          |
      | 'GET'    | 'nested.paramDouble'         | 'X'   | 'nested.paramDouble: is not a valid type'         |
      | 'HEAD'   | 'nested.paramDouble'         | 'X'   |                                                   |
      | 'POST'   | 'nested.paramDouble'         | 'X'   | 'nested.paramDouble: is not a valid type'         |
      | 'PUT'    | 'nested.paramDouble'         | 'X'   | 'nested.paramDouble: is not a valid type'         |
      | 'PATCH'  | 'nested.paramDouble'         | 'X'   | 'nested.paramDouble: is not a valid type'         |
      | 'DELETE' | 'nested.paramDouble'         | 'X'   | 'nested.paramDouble: is not a valid type'         |
      | 'GET'    | 'nested.paramBigDecimal'     | 'X'   | 'nested.paramBigDecimal: is not a valid type'     |
      | 'HEAD'   | 'nested.paramBigDecimal'     | 'X'   |                                                   |
      | 'POST'   | 'nested.paramBigDecimal'     | 'X'   | 'nested.paramBigDecimal: is not a valid type'     |
      | 'PUT'    | 'nested.paramBigDecimal'     | 'X'   | 'nested.paramBigDecimal: is not a valid type'     |
      | 'PATCH'  | 'nested.paramBigDecimal'     | 'X'   | 'nested.paramBigDecimal: is not a valid type'     |
      | 'DELETE' | 'nested.paramBigDecimal'     | 'X'   | 'nested.paramBigDecimal: is not a valid type'     |
      | 'GET'    | 'nested.paramBoolean'        | 'X'   | 'nested.paramBoolean: is not a valid type'        |
      | 'HEAD'   | 'nested.paramBoolean'        | 'X'   |                                                   |
      | 'POST'   | 'nested.paramBoolean'        | 'X'   | 'nested.paramBoolean: is not a valid type'        |
      | 'PUT'    | 'nested.paramBoolean'        | 'X'   | 'nested.paramBoolean: is not a valid type'        |
      | 'PATCH'  | 'nested.paramBoolean'        | 'X'   | 'nested.paramBoolean: is not a valid type'        |
      | 'DELETE' | 'nested.paramBoolean'        | 'X'   | 'nested.paramBoolean: is not a valid type'        |
      | 'GET'    | 'nested.paramOffsetDateTime' | 'X'   | 'nested.paramOffsetDateTime: is not a valid type' |
      | 'HEAD'   | 'nested.paramOffsetDateTime' | 'X'   |                                                   |
      | 'POST'   | 'nested.paramOffsetDateTime' | 'X'   | 'nested.paramOffsetDateTime: is not a valid type' |
      | 'PUT'    | 'nested.paramOffsetDateTime' | 'X'   | 'nested.paramOffsetDateTime: is not a valid type' |
      | 'PATCH'  | 'nested.paramOffsetDateTime' | 'X'   | 'nested.paramOffsetDateTime: is not a valid type' |
      | 'DELETE' | 'nested.paramOffsetDateTime' | 'X'   | 'nested.paramOffsetDateTime: is not a valid type' |
      | 'GET'    | 'nested.paramLocalDateTime'  | 'X'   | 'nested.paramLocalDateTime: is not a valid type'  |
      | 'HEAD'   | 'nested.paramLocalDateTime'  | 'X'   |                                                   |
      | 'POST'   | 'nested.paramLocalDateTime'  | 'X'   | 'nested.paramLocalDateTime: is not a valid type'  |
      | 'PUT'    | 'nested.paramLocalDateTime'  | 'X'   | 'nested.paramLocalDateTime: is not a valid type'  |
      | 'PATCH'  | 'nested.paramLocalDateTime'  | 'X'   | 'nested.paramLocalDateTime: is not a valid type'  |
      | 'DELETE' | 'nested.paramLocalDateTime'  | 'X'   | 'nested.paramLocalDateTime: is not a valid type'  |
      | 'GET'    | 'nested.paramLocalDate'      | 'X'   | 'nested.paramLocalDate: is not a valid type'      |
      | 'HEAD'   | 'nested.paramLocalDate'      | 'X'   |                                                   |
      | 'POST'   | 'nested.paramLocalDate'      | 'X'   | 'nested.paramLocalDate: is not a valid type'      |
      | 'PUT'    | 'nested.paramLocalDate'      | 'X'   | 'nested.paramLocalDate: is not a valid type'      |
      | 'PATCH'  | 'nested.paramLocalDate'      | 'X'   | 'nested.paramLocalDate: is not a valid type'      |
      | 'DELETE' | 'nested.paramLocalDate'      | 'X'   | 'nested.paramLocalDate: is not a valid type'      |
      | 'GET'    | 'nested.paramLocalTime'      | 'X'   | 'nested.paramLocalTime: is not a valid type'      |
      | 'HEAD'   | 'nested.paramLocalTime'      | 'X'   |                                                   |
      | 'POST'   | 'nested.paramLocalTime'      | 'X'   | 'nested.paramLocalTime: is not a valid type'      |
      | 'PUT'    | 'nested.paramLocalTime'      | 'X'   | 'nested.paramLocalTime: is not a valid type'      |
      | 'PATCH'  | 'nested.paramLocalTime'      | 'X'   | 'nested.paramLocalTime: is not a valid type'      |
      | 'DELETE' | 'nested.paramLocalTime'      | 'X'   | 'nested.paramLocalTime: is not a valid type'      |
      | 'GET'    | 'nested.paramYear'           | 'X'   | 'nested.paramYear: is not a valid type'           |
      | 'HEAD'   | 'nested.paramYear'           | 'X'   |                                                   |
      | 'POST'   | 'nested.paramYear'           | 'X'   | 'nested.paramYear: is not a valid type'           |
      | 'PUT'    | 'nested.paramYear'           | 'X'   | 'nested.paramYear: is not a valid type'           |
      | 'PATCH'  | 'nested.paramYear'           | 'X'   | 'nested.paramYear: is not a valid type'           |
      | 'DELETE' | 'nested.paramYear'           | 'X'   | 'nested.paramYear: is not a valid type'           |
      | 'GET'    | 'nested.paramEnumAB'         | 'X'   | 'nested.paramEnumAB: is not a valid type'         |
      | 'HEAD'   | 'nested.paramEnumAB'         | 'X'   |                                                   |
      | 'POST'   | 'nested.paramEnumAB'         | 'X'   | 'nested.paramEnumAB: is not a valid type'         |
      | 'PUT'    | 'nested.paramEnumAB'         | 'X'   | 'nested.paramEnumAB: is not a valid type'         |
      | 'PATCH'  | 'nested.paramEnumAB'         | 'X'   | 'nested.paramEnumAB: is not a valid type'         |
      | 'DELETE' | 'nested.paramEnumAB'         | 'X'   | 'nested.paramEnumAB: is not a valid type'         |
      | 'GET'    | 'nested.paramEnumLowerAB'    | 'X'   | 'nested.paramEnumLowerAB: is not a valid type'    |
      | 'HEAD'   | 'nested.paramEnumLowerAB'    | 'X'   |                                                   |
      | 'POST'   | 'nested.paramEnumLowerAB'    | 'X'   | 'nested.paramEnumLowerAB: is not a valid type'    |
      | 'PUT'    | 'nested.paramEnumLowerAB'    | 'X'   | 'nested.paramEnumLowerAB: is not a valid type'    |
      | 'PATCH'  | 'nested.paramEnumLowerAB'    | 'X'   | 'nested.paramEnumLowerAB: is not a valid type'    |
      | 'DELETE' | 'nested.paramEnumLowerAB'    | 'X'   | 'nested.paramEnumLowerAB: is not a valid type'    |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/nested-optional-dto-attribute?<param>=<value> endpoint (RequestParamUnconstrainedRestApi.class)
    this endpoint receives the request parameter in a nested optional dto attribute, both having the same name
    and the service cannot map the sent value to the declared data type, responding with an error message.

    Given the endpoint '/api/v1/request-parameter/nested-optional-dto-attribute'
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | param                        | value | message                                           |
      | 'GET'    | 'nested.paramInteger'        | 'X'   | 'nested.paramInteger: is not a valid type'        |
      | 'HEAD'   | 'nested.paramInteger'        | 'X'   |                                                   |
      | 'POST'   | 'nested.paramInteger'        | 'X'   | 'nested.paramInteger: is not a valid type'        |
      | 'PUT'    | 'nested.paramInteger'        | 'X'   | 'nested.paramInteger: is not a valid type'        |
      | 'PATCH'  | 'nested.paramInteger'        | 'X'   | 'nested.paramInteger: is not a valid type'        |
      | 'DELETE' | 'nested.paramInteger'        | 'X'   | 'nested.paramInteger: is not a valid type'        |
      | 'GET'    | 'nested.paramLong'           | 'X'   | 'nested.paramLong: is not a valid type'           |
      | 'HEAD'   | 'nested.paramLong'           | 'X'   |                                                   |
      | 'POST'   | 'nested.paramLong'           | 'X'   | 'nested.paramLong: is not a valid type'           |
      | 'PUT'    | 'nested.paramLong'           | 'X'   | 'nested.paramLong: is not a valid type'           |
      | 'PATCH'  | 'nested.paramLong'           | 'X'   | 'nested.paramLong: is not a valid type'           |
      | 'DELETE' | 'nested.paramLong'           | 'X'   | 'nested.paramLong: is not a valid type'           |
      | 'GET'    | 'nested.paramBigInteger'     | 'X'   | 'nested.paramBigInteger: is not a valid type'     |
      | 'HEAD'   | 'nested.paramBigInteger'     | 'X'   |                                                   |
      | 'POST'   | 'nested.paramBigInteger'     | 'X'   | 'nested.paramBigInteger: is not a valid type'     |
      | 'PUT'    | 'nested.paramBigInteger'     | 'X'   | 'nested.paramBigInteger: is not a valid type'     |
      | 'PATCH'  | 'nested.paramBigInteger'     | 'X'   | 'nested.paramBigInteger: is not a valid type'     |
      | 'DELETE' | 'nested.paramBigInteger'     | 'X'   | 'nested.paramBigInteger: is not a valid type'     |
      | 'GET'    | 'nested.paramFloat'          | 'X'   | 'nested.paramFloat: is not a valid type'          |
      | 'HEAD'   | 'nested.paramFloat'          | 'X'   |                                                   |
      | 'POST'   | 'nested.paramFloat'          | 'X'   | 'nested.paramFloat: is not a valid type'          |
      | 'PUT'    | 'nested.paramFloat'          | 'X'   | 'nested.paramFloat: is not a valid type'          |
      | 'PATCH'  | 'nested.paramFloat'          | 'X'   | 'nested.paramFloat: is not a valid type'          |
      | 'DELETE' | 'nested.paramFloat'          | 'X'   | 'nested.paramFloat: is not a valid type'          |
      | 'GET'    | 'nested.paramDouble'         | 'X'   | 'nested.paramDouble: is not a valid type'         |
      | 'HEAD'   | 'nested.paramDouble'         | 'X'   |                                                   |
      | 'POST'   | 'nested.paramDouble'         | 'X'   | 'nested.paramDouble: is not a valid type'         |
      | 'PUT'    | 'nested.paramDouble'         | 'X'   | 'nested.paramDouble: is not a valid type'         |
      | 'PATCH'  | 'nested.paramDouble'         | 'X'   | 'nested.paramDouble: is not a valid type'         |
      | 'DELETE' | 'nested.paramDouble'         | 'X'   | 'nested.paramDouble: is not a valid type'         |
      | 'GET'    | 'nested.paramBigDecimal'     | 'X'   | 'nested.paramBigDecimal: is not a valid type'     |
      | 'HEAD'   | 'nested.paramBigDecimal'     | 'X'   |                                                   |
      | 'POST'   | 'nested.paramBigDecimal'     | 'X'   | 'nested.paramBigDecimal: is not a valid type'     |
      | 'PUT'    | 'nested.paramBigDecimal'     | 'X'   | 'nested.paramBigDecimal: is not a valid type'     |
      | 'PATCH'  | 'nested.paramBigDecimal'     | 'X'   | 'nested.paramBigDecimal: is not a valid type'     |
      | 'DELETE' | 'nested.paramBigDecimal'     | 'X'   | 'nested.paramBigDecimal: is not a valid type'     |
      | 'GET'    | 'nested.paramBoolean'        | 'X'   | 'nested.paramBoolean: is not a valid type'        |
      | 'HEAD'   | 'nested.paramBoolean'        | 'X'   |                                                   |
      | 'POST'   | 'nested.paramBoolean'        | 'X'   | 'nested.paramBoolean: is not a valid type'        |
      | 'PUT'    | 'nested.paramBoolean'        | 'X'   | 'nested.paramBoolean: is not a valid type'        |
      | 'PATCH'  | 'nested.paramBoolean'        | 'X'   | 'nested.paramBoolean: is not a valid type'        |
      | 'DELETE' | 'nested.paramBoolean'        | 'X'   | 'nested.paramBoolean: is not a valid type'        |
      | 'GET'    | 'nested.paramOffsetDateTime' | 'X'   | 'nested.paramOffsetDateTime: is not a valid type' |
      | 'HEAD'   | 'nested.paramOffsetDateTime' | 'X'   |                                                   |
      | 'POST'   | 'nested.paramOffsetDateTime' | 'X'   | 'nested.paramOffsetDateTime: is not a valid type' |
      | 'PUT'    | 'nested.paramOffsetDateTime' | 'X'   | 'nested.paramOffsetDateTime: is not a valid type' |
      | 'PATCH'  | 'nested.paramOffsetDateTime' | 'X'   | 'nested.paramOffsetDateTime: is not a valid type' |
      | 'DELETE' | 'nested.paramOffsetDateTime' | 'X'   | 'nested.paramOffsetDateTime: is not a valid type' |
      | 'GET'    | 'nested.paramLocalDateTime'  | 'X'   | 'nested.paramLocalDateTime: is not a valid type'  |
      | 'HEAD'   | 'nested.paramLocalDateTime'  | 'X'   |                                                   |
      | 'POST'   | 'nested.paramLocalDateTime'  | 'X'   | 'nested.paramLocalDateTime: is not a valid type'  |
      | 'PUT'    | 'nested.paramLocalDateTime'  | 'X'   | 'nested.paramLocalDateTime: is not a valid type'  |
      | 'PATCH'  | 'nested.paramLocalDateTime'  | 'X'   | 'nested.paramLocalDateTime: is not a valid type'  |
      | 'DELETE' | 'nested.paramLocalDateTime'  | 'X'   | 'nested.paramLocalDateTime: is not a valid type'  |
      | 'GET'    | 'nested.paramLocalDate'      | 'X'   | 'nested.paramLocalDate: is not a valid type'      |
      | 'HEAD'   | 'nested.paramLocalDate'      | 'X'   |                                                   |
      | 'POST'   | 'nested.paramLocalDate'      | 'X'   | 'nested.paramLocalDate: is not a valid type'      |
      | 'PUT'    | 'nested.paramLocalDate'      | 'X'   | 'nested.paramLocalDate: is not a valid type'      |
      | 'PATCH'  | 'nested.paramLocalDate'      | 'X'   | 'nested.paramLocalDate: is not a valid type'      |
      | 'DELETE' | 'nested.paramLocalDate'      | 'X'   | 'nested.paramLocalDate: is not a valid type'      |
      | 'GET'    | 'nested.paramLocalTime'      | 'X'   | 'nested.paramLocalTime: is not a valid type'      |
      | 'HEAD'   | 'nested.paramLocalTime'      | 'X'   |                                                   |
      | 'POST'   | 'nested.paramLocalTime'      | 'X'   | 'nested.paramLocalTime: is not a valid type'      |
      | 'PUT'    | 'nested.paramLocalTime'      | 'X'   | 'nested.paramLocalTime: is not a valid type'      |
      | 'PATCH'  | 'nested.paramLocalTime'      | 'X'   | 'nested.paramLocalTime: is not a valid type'      |
      | 'DELETE' | 'nested.paramLocalTime'      | 'X'   | 'nested.paramLocalTime: is not a valid type'      |
      | 'GET'    | 'nested.paramYear'           | 'X'   | 'nested.paramYear: is not a valid type'           |
      | 'HEAD'   | 'nested.paramYear'           | 'X'   |                                                   |
      | 'POST'   | 'nested.paramYear'           | 'X'   | 'nested.paramYear: is not a valid type'           |
      | 'PUT'    | 'nested.paramYear'           | 'X'   | 'nested.paramYear: is not a valid type'           |
      | 'PATCH'  | 'nested.paramYear'           | 'X'   | 'nested.paramYear: is not a valid type'           |
      | 'DELETE' | 'nested.paramYear'           | 'X'   | 'nested.paramYear: is not a valid type'           |
      | 'GET'    | 'nested.paramEnumAB'         | 'X'   | 'nested.paramEnumAB: is not a valid type'         |
      | 'HEAD'   | 'nested.paramEnumAB'         | 'X'   |                                                   |
      | 'POST'   | 'nested.paramEnumAB'         | 'X'   | 'nested.paramEnumAB: is not a valid type'         |
      | 'PUT'    | 'nested.paramEnumAB'         | 'X'   | 'nested.paramEnumAB: is not a valid type'         |
      | 'PATCH'  | 'nested.paramEnumAB'         | 'X'   | 'nested.paramEnumAB: is not a valid type'         |
      | 'DELETE' | 'nested.paramEnumAB'         | 'X'   | 'nested.paramEnumAB: is not a valid type'         |
      | 'GET'    | 'nested.paramEnumLowerAB'    | 'X'   | 'nested.paramEnumLowerAB: is not a valid type'    |
      | 'HEAD'   | 'nested.paramEnumLowerAB'    | 'X'   |                                                   |
      | 'POST'   | 'nested.paramEnumLowerAB'    | 'X'   | 'nested.paramEnumLowerAB: is not a valid type'    |
      | 'PUT'    | 'nested.paramEnumLowerAB'    | 'X'   | 'nested.paramEnumLowerAB: is not a valid type'    |
      | 'PATCH'  | 'nested.paramEnumLowerAB'    | 'X'   | 'nested.paramEnumLowerAB: is not a valid type'    |
      | 'DELETE' | 'nested.paramEnumLowerAB'    | 'X'   | 'nested.paramEnumLowerAB: is not a valid type'    |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/nested-dto-attribute?<param>=<value> endpoint (RequestParamUnconstrainedRestApi.class)
    this endpoint receives the request parameter in a nested dto attribute from an array, both having the same name
    and the service cannot map the sent value to the declared data type, responding with an error message.

    Given the endpoint '/api/v1/request-parameter/nested-dto-attribute'
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | param                          | value | message                                             |
      | 'GET'    | 'array[0].paramInteger'        | 'X'   | 'array[0].paramInteger: is not a valid type'        |
      | 'HEAD'   | 'array[0].paramInteger'        | 'X'   |                                                     |
      | 'POST'   | 'array[0].paramInteger'        | 'X'   | 'array[0].paramInteger: is not a valid type'        |
      | 'PUT'    | 'array[0].paramInteger'        | 'X'   | 'array[0].paramInteger: is not a valid type'        |
      | 'PATCH'  | 'array[0].paramInteger'        | 'X'   | 'array[0].paramInteger: is not a valid type'        |
      | 'DELETE' | 'array[0].paramInteger'        | 'X'   | 'array[0].paramInteger: is not a valid type'        |
      | 'GET'    | 'array[0].paramLong'           | 'X'   | 'array[0].paramLong: is not a valid type'           |
      | 'HEAD'   | 'array[0].paramLong'           | 'X'   |                                                     |
      | 'POST'   | 'array[0].paramLong'           | 'X'   | 'array[0].paramLong: is not a valid type'           |
      | 'PUT'    | 'array[0].paramLong'           | 'X'   | 'array[0].paramLong: is not a valid type'           |
      | 'PATCH'  | 'array[0].paramLong'           | 'X'   | 'array[0].paramLong: is not a valid type'           |
      | 'DELETE' | 'array[0].paramLong'           | 'X'   | 'array[0].paramLong: is not a valid type'           |
      | 'GET'    | 'array[0].paramBigInteger'     | 'X'   | 'array[0].paramBigInteger: is not a valid type'     |
      | 'HEAD'   | 'array[0].paramBigInteger'     | 'X'   |                                                     |
      | 'POST'   | 'array[0].paramBigInteger'     | 'X'   | 'array[0].paramBigInteger: is not a valid type'     |
      | 'PUT'    | 'array[0].paramBigInteger'     | 'X'   | 'array[0].paramBigInteger: is not a valid type'     |
      | 'PATCH'  | 'array[0].paramBigInteger'     | 'X'   | 'array[0].paramBigInteger: is not a valid type'     |
      | 'DELETE' | 'array[0].paramBigInteger'     | 'X'   | 'array[0].paramBigInteger: is not a valid type'     |
      | 'GET'    | 'array[0].paramFloat'          | 'X'   | 'array[0].paramFloat: is not a valid type'          |
      | 'HEAD'   | 'array[0].paramFloat'          | 'X'   |                                                     |
      | 'POST'   | 'array[0].paramFloat'          | 'X'   | 'array[0].paramFloat: is not a valid type'          |
      | 'PUT'    | 'array[0].paramFloat'          | 'X'   | 'array[0].paramFloat: is not a valid type'          |
      | 'PATCH'  | 'array[0].paramFloat'          | 'X'   | 'array[0].paramFloat: is not a valid type'          |
      | 'DELETE' | 'array[0].paramFloat'          | 'X'   | 'array[0].paramFloat: is not a valid type'          |
      | 'GET'    | 'array[0].paramDouble'         | 'X'   | 'array[0].paramDouble: is not a valid type'         |
      | 'HEAD'   | 'array[0].paramDouble'         | 'X'   |                                                     |
      | 'POST'   | 'array[0].paramDouble'         | 'X'   | 'array[0].paramDouble: is not a valid type'         |
      | 'PUT'    | 'array[0].paramDouble'         | 'X'   | 'array[0].paramDouble: is not a valid type'         |
      | 'PATCH'  | 'array[0].paramDouble'         | 'X'   | 'array[0].paramDouble: is not a valid type'         |
      | 'DELETE' | 'array[0].paramDouble'         | 'X'   | 'array[0].paramDouble: is not a valid type'         |
      | 'GET'    | 'array[0].paramBigDecimal'     | 'X'   | 'array[0].paramBigDecimal: is not a valid type'     |
      | 'HEAD'   | 'array[0].paramBigDecimal'     | 'X'   |                                                     |
      | 'POST'   | 'array[0].paramBigDecimal'     | 'X'   | 'array[0].paramBigDecimal: is not a valid type'     |
      | 'PUT'    | 'array[0].paramBigDecimal'     | 'X'   | 'array[0].paramBigDecimal: is not a valid type'     |
      | 'PATCH'  | 'array[0].paramBigDecimal'     | 'X'   | 'array[0].paramBigDecimal: is not a valid type'     |
      | 'DELETE' | 'array[0].paramBigDecimal'     | 'X'   | 'array[0].paramBigDecimal: is not a valid type'     |
      | 'GET'    | 'array[0].paramBoolean'        | 'X'   | 'array[0].paramBoolean: is not a valid type'        |
      | 'HEAD'   | 'array[0].paramBoolean'        | 'X'   |                                                     |
      | 'POST'   | 'array[0].paramBoolean'        | 'X'   | 'array[0].paramBoolean: is not a valid type'        |
      | 'PUT'    | 'array[0].paramBoolean'        | 'X'   | 'array[0].paramBoolean: is not a valid type'        |
      | 'PATCH'  | 'array[0].paramBoolean'        | 'X'   | 'array[0].paramBoolean: is not a valid type'        |
      | 'DELETE' | 'array[0].paramBoolean'        | 'X'   | 'array[0].paramBoolean: is not a valid type'        |
      | 'GET'    | 'array[0].paramOffsetDateTime' | 'X'   | 'array[0].paramOffsetDateTime: is not a valid type' |
      | 'HEAD'   | 'array[0].paramOffsetDateTime' | 'X'   |                                                     |
      | 'POST'   | 'array[0].paramOffsetDateTime' | 'X'   | 'array[0].paramOffsetDateTime: is not a valid type' |
      | 'PUT'    | 'array[0].paramOffsetDateTime' | 'X'   | 'array[0].paramOffsetDateTime: is not a valid type' |
      | 'PATCH'  | 'array[0].paramOffsetDateTime' | 'X'   | 'array[0].paramOffsetDateTime: is not a valid type' |
      | 'DELETE' | 'array[0].paramOffsetDateTime' | 'X'   | 'array[0].paramOffsetDateTime: is not a valid type' |
      | 'GET'    | 'array[0].paramLocalDateTime'  | 'X'   | 'array[0].paramLocalDateTime: is not a valid type'  |
      | 'HEAD'   | 'array[0].paramLocalDateTime'  | 'X'   |                                                     |
      | 'POST'   | 'array[0].paramLocalDateTime'  | 'X'   | 'array[0].paramLocalDateTime: is not a valid type'  |
      | 'PUT'    | 'array[0].paramLocalDateTime'  | 'X'   | 'array[0].paramLocalDateTime: is not a valid type'  |
      | 'PATCH'  | 'array[0].paramLocalDateTime'  | 'X'   | 'array[0].paramLocalDateTime: is not a valid type'  |
      | 'DELETE' | 'array[0].paramLocalDateTime'  | 'X'   | 'array[0].paramLocalDateTime: is not a valid type'  |
      | 'GET'    | 'array[0].paramLocalDate'      | 'X'   | 'array[0].paramLocalDate: is not a valid type'      |
      | 'HEAD'   | 'array[0].paramLocalDate'      | 'X'   |                                                     |
      | 'POST'   | 'array[0].paramLocalDate'      | 'X'   | 'array[0].paramLocalDate: is not a valid type'      |
      | 'PUT'    | 'array[0].paramLocalDate'      | 'X'   | 'array[0].paramLocalDate: is not a valid type'      |
      | 'PATCH'  | 'array[0].paramLocalDate'      | 'X'   | 'array[0].paramLocalDate: is not a valid type'      |
      | 'DELETE' | 'array[0].paramLocalDate'      | 'X'   | 'array[0].paramLocalDate: is not a valid type'      |
      | 'GET'    | 'array[0].paramLocalTime'      | 'X'   | 'array[0].paramLocalTime: is not a valid type'      |
      | 'HEAD'   | 'array[0].paramLocalTime'      | 'X'   |                                                     |
      | 'POST'   | 'array[0].paramLocalTime'      | 'X'   | 'array[0].paramLocalTime: is not a valid type'      |
      | 'PUT'    | 'array[0].paramLocalTime'      | 'X'   | 'array[0].paramLocalTime: is not a valid type'      |
      | 'PATCH'  | 'array[0].paramLocalTime'      | 'X'   | 'array[0].paramLocalTime: is not a valid type'      |
      | 'DELETE' | 'array[0].paramLocalTime'      | 'X'   | 'array[0].paramLocalTime: is not a valid type'      |
      | 'GET'    | 'array[0].paramYear'           | 'X'   | 'array[0].paramYear: is not a valid type'           |
      | 'HEAD'   | 'array[0].paramYear'           | 'X'   |                                                     |
      | 'POST'   | 'array[0].paramYear'           | 'X'   | 'array[0].paramYear: is not a valid type'           |
      | 'PUT'    | 'array[0].paramYear'           | 'X'   | 'array[0].paramYear: is not a valid type'           |
      | 'PATCH'  | 'array[0].paramYear'           | 'X'   | 'array[0].paramYear: is not a valid type'           |
      | 'DELETE' | 'array[0].paramYear'           | 'X'   | 'array[0].paramYear: is not a valid type'           |
      | 'GET'    | 'array[0].paramEnumAB'         | 'X'   | 'array[0].paramEnumAB: is not a valid type'         |
      | 'HEAD'   | 'array[0].paramEnumAB'         | 'X'   |                                                     |
      | 'POST'   | 'array[0].paramEnumAB'         | 'X'   | 'array[0].paramEnumAB: is not a valid type'         |
      | 'PUT'    | 'array[0].paramEnumAB'         | 'X'   | 'array[0].paramEnumAB: is not a valid type'         |
      | 'PATCH'  | 'array[0].paramEnumAB'         | 'X'   | 'array[0].paramEnumAB: is not a valid type'         |
      | 'DELETE' | 'array[0].paramEnumAB'         | 'X'   | 'array[0].paramEnumAB: is not a valid type'         |
      | 'GET'    | 'array[0].paramEnumLowerAB'    | 'X'   | 'array[0].paramEnumLowerAB: is not a valid type'    |
      | 'HEAD'   | 'array[0].paramEnumLowerAB'    | 'X'   |                                                     |
      | 'POST'   | 'array[0].paramEnumLowerAB'    | 'X'   | 'array[0].paramEnumLowerAB: is not a valid type'    |
      | 'PUT'    | 'array[0].paramEnumLowerAB'    | 'X'   | 'array[0].paramEnumLowerAB: is not a valid type'    |
      | 'PATCH'  | 'array[0].paramEnumLowerAB'    | 'X'   | 'array[0].paramEnumLowerAB: is not a valid type'    |
      | 'DELETE' | 'array[0].paramEnumLowerAB'    | 'X'   | 'array[0].paramEnumLowerAB: is not a valid type'    |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/nested-optional-dto-attribute?<param>=<value> endpoint (RequestParamUnconstrainedRestApi.class)
    this endpoint receives the request parameter in a nested optional dto attribute from an array, both having the same name
    and the service cannot map the sent value to the declared data type, responding with an error message.

    Given the endpoint '/api/v1/request-parameter/nested-optional-dto-attribute'
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | param                          | value | message                                             |
      | 'GET'    | 'array[0].paramInteger'        | 'X'   | 'array[0].paramInteger: is not a valid type'        |
      | 'HEAD'   | 'array[0].paramInteger'        | 'X'   |                                                     |
      | 'POST'   | 'array[0].paramInteger'        | 'X'   | 'array[0].paramInteger: is not a valid type'        |
      | 'PUT'    | 'array[0].paramInteger'        | 'X'   | 'array[0].paramInteger: is not a valid type'        |
      | 'PATCH'  | 'array[0].paramInteger'        | 'X'   | 'array[0].paramInteger: is not a valid type'        |
      | 'DELETE' | 'array[0].paramInteger'        | 'X'   | 'array[0].paramInteger: is not a valid type'        |
      | 'GET'    | 'array[0].paramLong'           | 'X'   | 'array[0].paramLong: is not a valid type'           |
      | 'HEAD'   | 'array[0].paramLong'           | 'X'   |                                                     |
      | 'POST'   | 'array[0].paramLong'           | 'X'   | 'array[0].paramLong: is not a valid type'           |
      | 'PUT'    | 'array[0].paramLong'           | 'X'   | 'array[0].paramLong: is not a valid type'           |
      | 'PATCH'  | 'array[0].paramLong'           | 'X'   | 'array[0].paramLong: is not a valid type'           |
      | 'DELETE' | 'array[0].paramLong'           | 'X'   | 'array[0].paramLong: is not a valid type'           |
      | 'GET'    | 'array[0].paramBigInteger'     | 'X'   | 'array[0].paramBigInteger: is not a valid type'     |
      | 'HEAD'   | 'array[0].paramBigInteger'     | 'X'   |                                                     |
      | 'POST'   | 'array[0].paramBigInteger'     | 'X'   | 'array[0].paramBigInteger: is not a valid type'     |
      | 'PUT'    | 'array[0].paramBigInteger'     | 'X'   | 'array[0].paramBigInteger: is not a valid type'     |
      | 'PATCH'  | 'array[0].paramBigInteger'     | 'X'   | 'array[0].paramBigInteger: is not a valid type'     |
      | 'DELETE' | 'array[0].paramBigInteger'     | 'X'   | 'array[0].paramBigInteger: is not a valid type'     |
      | 'GET'    | 'array[0].paramFloat'          | 'X'   | 'array[0].paramFloat: is not a valid type'          |
      | 'HEAD'   | 'array[0].paramFloat'          | 'X'   |                                                     |
      | 'POST'   | 'array[0].paramFloat'          | 'X'   | 'array[0].paramFloat: is not a valid type'          |
      | 'PUT'    | 'array[0].paramFloat'          | 'X'   | 'array[0].paramFloat: is not a valid type'          |
      | 'PATCH'  | 'array[0].paramFloat'          | 'X'   | 'array[0].paramFloat: is not a valid type'          |
      | 'DELETE' | 'array[0].paramFloat'          | 'X'   | 'array[0].paramFloat: is not a valid type'          |
      | 'GET'    | 'array[0].paramDouble'         | 'X'   | 'array[0].paramDouble: is not a valid type'         |
      | 'HEAD'   | 'array[0].paramDouble'         | 'X'   |                                                     |
      | 'POST'   | 'array[0].paramDouble'         | 'X'   | 'array[0].paramDouble: is not a valid type'         |
      | 'PUT'    | 'array[0].paramDouble'         | 'X'   | 'array[0].paramDouble: is not a valid type'         |
      | 'PATCH'  | 'array[0].paramDouble'         | 'X'   | 'array[0].paramDouble: is not a valid type'         |
      | 'DELETE' | 'array[0].paramDouble'         | 'X'   | 'array[0].paramDouble: is not a valid type'         |
      | 'GET'    | 'array[0].paramBigDecimal'     | 'X'   | 'array[0].paramBigDecimal: is not a valid type'     |
      | 'HEAD'   | 'array[0].paramBigDecimal'     | 'X'   |                                                     |
      | 'POST'   | 'array[0].paramBigDecimal'     | 'X'   | 'array[0].paramBigDecimal: is not a valid type'     |
      | 'PUT'    | 'array[0].paramBigDecimal'     | 'X'   | 'array[0].paramBigDecimal: is not a valid type'     |
      | 'PATCH'  | 'array[0].paramBigDecimal'     | 'X'   | 'array[0].paramBigDecimal: is not a valid type'     |
      | 'DELETE' | 'array[0].paramBigDecimal'     | 'X'   | 'array[0].paramBigDecimal: is not a valid type'     |
      | 'GET'    | 'array[0].paramBoolean'        | 'X'   | 'array[0].paramBoolean: is not a valid type'        |
      | 'HEAD'   | 'array[0].paramBoolean'        | 'X'   |                                                     |
      | 'POST'   | 'array[0].paramBoolean'        | 'X'   | 'array[0].paramBoolean: is not a valid type'        |
      | 'PUT'    | 'array[0].paramBoolean'        | 'X'   | 'array[0].paramBoolean: is not a valid type'        |
      | 'PATCH'  | 'array[0].paramBoolean'        | 'X'   | 'array[0].paramBoolean: is not a valid type'        |
      | 'DELETE' | 'array[0].paramBoolean'        | 'X'   | 'array[0].paramBoolean: is not a valid type'        |
      | 'GET'    | 'array[0].paramOffsetDateTime' | 'X'   | 'array[0].paramOffsetDateTime: is not a valid type' |
      | 'HEAD'   | 'array[0].paramOffsetDateTime' | 'X'   |                                                     |
      | 'POST'   | 'array[0].paramOffsetDateTime' | 'X'   | 'array[0].paramOffsetDateTime: is not a valid type' |
      | 'PUT'    | 'array[0].paramOffsetDateTime' | 'X'   | 'array[0].paramOffsetDateTime: is not a valid type' |
      | 'PATCH'  | 'array[0].paramOffsetDateTime' | 'X'   | 'array[0].paramOffsetDateTime: is not a valid type' |
      | 'DELETE' | 'array[0].paramOffsetDateTime' | 'X'   | 'array[0].paramOffsetDateTime: is not a valid type' |
      | 'GET'    | 'array[0].paramLocalDateTime'  | 'X'   | 'array[0].paramLocalDateTime: is not a valid type'  |
      | 'HEAD'   | 'array[0].paramLocalDateTime'  | 'X'   |                                                     |
      | 'POST'   | 'array[0].paramLocalDateTime'  | 'X'   | 'array[0].paramLocalDateTime: is not a valid type'  |
      | 'PUT'    | 'array[0].paramLocalDateTime'  | 'X'   | 'array[0].paramLocalDateTime: is not a valid type'  |
      | 'PATCH'  | 'array[0].paramLocalDateTime'  | 'X'   | 'array[0].paramLocalDateTime: is not a valid type'  |
      | 'DELETE' | 'array[0].paramLocalDateTime'  | 'X'   | 'array[0].paramLocalDateTime: is not a valid type'  |
      | 'GET'    | 'array[0].paramLocalDate'      | 'X'   | 'array[0].paramLocalDate: is not a valid type'      |
      | 'HEAD'   | 'array[0].paramLocalDate'      | 'X'   |                                                     |
      | 'POST'   | 'array[0].paramLocalDate'      | 'X'   | 'array[0].paramLocalDate: is not a valid type'      |
      | 'PUT'    | 'array[0].paramLocalDate'      | 'X'   | 'array[0].paramLocalDate: is not a valid type'      |
      | 'PATCH'  | 'array[0].paramLocalDate'      | 'X'   | 'array[0].paramLocalDate: is not a valid type'      |
      | 'DELETE' | 'array[0].paramLocalDate'      | 'X'   | 'array[0].paramLocalDate: is not a valid type'      |
      | 'GET'    | 'array[0].paramLocalTime'      | 'X'   | 'array[0].paramLocalTime: is not a valid type'      |
      | 'HEAD'   | 'array[0].paramLocalTime'      | 'X'   |                                                     |
      | 'POST'   | 'array[0].paramLocalTime'      | 'X'   | 'array[0].paramLocalTime: is not a valid type'      |
      | 'PUT'    | 'array[0].paramLocalTime'      | 'X'   | 'array[0].paramLocalTime: is not a valid type'      |
      | 'PATCH'  | 'array[0].paramLocalTime'      | 'X'   | 'array[0].paramLocalTime: is not a valid type'      |
      | 'DELETE' | 'array[0].paramLocalTime'      | 'X'   | 'array[0].paramLocalTime: is not a valid type'      |
      | 'GET'    | 'array[0].paramYear'           | 'X'   | 'array[0].paramYear: is not a valid type'           |
      | 'HEAD'   | 'array[0].paramYear'           | 'X'   |                                                     |
      | 'POST'   | 'array[0].paramYear'           | 'X'   | 'array[0].paramYear: is not a valid type'           |
      | 'PUT'    | 'array[0].paramYear'           | 'X'   | 'array[0].paramYear: is not a valid type'           |
      | 'PATCH'  | 'array[0].paramYear'           | 'X'   | 'array[0].paramYear: is not a valid type'           |
      | 'DELETE' | 'array[0].paramYear'           | 'X'   | 'array[0].paramYear: is not a valid type'           |
      | 'GET'    | 'array[0].paramEnumAB'         | 'X'   | 'array[0].paramEnumAB: is not a valid type'         |
      | 'HEAD'   | 'array[0].paramEnumAB'         | 'X'   |                                                     |
      | 'POST'   | 'array[0].paramEnumAB'         | 'X'   | 'array[0].paramEnumAB: is not a valid type'         |
      | 'PUT'    | 'array[0].paramEnumAB'         | 'X'   | 'array[0].paramEnumAB: is not a valid type'         |
      | 'PATCH'  | 'array[0].paramEnumAB'         | 'X'   | 'array[0].paramEnumAB: is not a valid type'         |
      | 'DELETE' | 'array[0].paramEnumAB'         | 'X'   | 'array[0].paramEnumAB: is not a valid type'         |
      | 'GET'    | 'array[0].paramEnumLowerAB'    | 'X'   | 'array[0].paramEnumLowerAB: is not a valid type'    |
      | 'HEAD'   | 'array[0].paramEnumLowerAB'    | 'X'   |                                                     |
      | 'POST'   | 'array[0].paramEnumLowerAB'    | 'X'   | 'array[0].paramEnumLowerAB: is not a valid type'    |
      | 'PUT'    | 'array[0].paramEnumLowerAB'    | 'X'   | 'array[0].paramEnumLowerAB: is not a valid type'    |
      | 'PATCH'  | 'array[0].paramEnumLowerAB'    | 'X'   | 'array[0].paramEnumLowerAB: is not a valid type'    |
      | 'DELETE' | 'array[0].paramEnumLowerAB'    | 'X'   | 'array[0].paramEnumLowerAB: is not a valid type'    |
