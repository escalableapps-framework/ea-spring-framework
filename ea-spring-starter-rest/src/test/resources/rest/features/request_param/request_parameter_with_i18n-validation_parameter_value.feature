Feature: Request parameter with i18n-validation for parameter value
  
  The REST service receives data via the request parameter and it does not accomplish with some constraint. The service responds with an error message.
  
   Rule: The conversion between data types is given by the following table
  
  | parameter type | java type      | validation              | validation                                   |
  |----------------|----------------|-------------------------|----------------------------------------------|
  | string         | String         | @Size                   | @Size(max = 1)                               |
  | number         | Integer        | @Min @Max               | @Max(1)                                      |
  | number         | Long           | @Min @Max               | @Max(1)                                      |
  | number         | BigInteger     | @DecimalMin @DecimalMax | @DecimalMax("1")                             |
  | number         | Float          |                         |                                              |
  | number         | Double         |                         |                                              |
  | number         | BigDecimal     | @DecimalMin @DecimalMax | @DecimalMax("9.9")                           |
  | boolean        | Boolean        |                         |                                              |
  | ISO-8601       | OffsetDateTime | @DatetimeRange          | @DatetimeRange(max = "2000-01-01T00:00:00Z") |
  | ISO-8601       | LocalDateTime  | @DatetimeRange          | @DatetimeRange(max = "2000-01-01T00:00:00Z") |
  | ISO-8601       | LocalDate      | @DateRange              | @DateRange(max = "2000-01-01")               |
  | ISO-8601       | LocalTime      |                         |                                              |
  | ISO-8601       | Year           | @YearRange              | @YearRange(max = "2000")                     |
  | string         | ENUM           |                         |                                              |

  Background: 
    Given the default timezone 'America/Puerto_Rico'

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/i18n-constrained/method-parameter<path>?<param>=<value> endpoint (RequestParamI18nConstrainedRestApi.class)
    this endpoint receives the request parameter in a method parameter, both having the same name
    and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/request-parameter/i18n-constrained/method-parameter'<path>
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | param                 | value                  | message                                               |
      | 'GET'    | '/paramString'         | 'paramString'         | 'ab'                   | 'paramString: is not a valid string'                  |
      | 'HEAD'   | '/paramString'         | 'paramString'         | 'ab'                   |                                                       |
      | 'POST'   | '/paramString'         | 'paramString'         | 'ab'                   | 'paramString: is not a valid string'                  |
      | 'PUT'    | '/paramString'         | 'paramString'         | 'ab'                   | 'paramString: is not a valid string'                  |
      | 'PATCH'  | '/paramString'         | 'paramString'         | 'ab'                   | 'paramString: is not a valid string'                  |
      | 'DELETE' | '/paramString'         | 'paramString'         | 'ab'                   | 'paramString: is not a valid string'                  |
      | 'GET'    | '/paramInteger'        | 'paramInteger'        | '10'                   | 'paramInteger: is not a valid integer'                |
      | 'HEAD'   | '/paramInteger'        | 'paramInteger'        | '10'                   |                                                       |
      | 'POST'   | '/paramInteger'        | 'paramInteger'        | '10'                   | 'paramInteger: is not a valid integer'                |
      | 'PUT'    | '/paramInteger'        | 'paramInteger'        | '10'                   | 'paramInteger: is not a valid integer'                |
      | 'PATCH'  | '/paramInteger'        | 'paramInteger'        | '10'                   | 'paramInteger: is not a valid integer'                |
      | 'DELETE' | '/paramInteger'        | 'paramInteger'        | '10'                   | 'paramInteger: is not a valid integer'                |
      | 'GET'    | '/paramLong'           | 'paramLong'           | '10'                   | 'paramLong: is not a valid long'                      |
      | 'HEAD'   | '/paramLong'           | 'paramLong'           | '10'                   |                                                       |
      | 'POST'   | '/paramLong'           | 'paramLong'           | '10'                   | 'paramLong: is not a valid long'                      |
      | 'PUT'    | '/paramLong'           | 'paramLong'           | '10'                   | 'paramLong: is not a valid long'                      |
      | 'PATCH'  | '/paramLong'           | 'paramLong'           | '10'                   | 'paramLong: is not a valid long'                      |
      | 'DELETE' | '/paramLong'           | 'paramLong'           | '10'                   | 'paramLong: is not a valid long'                      |
      | 'GET'    | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   | 'paramBigInteger: is not a valid big-integer'         |
      | 'HEAD'   | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   |                                                       |
      | 'POST'   | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   | 'paramBigInteger: is not a valid big-integer'         |
      | 'PUT'    | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   | 'paramBigInteger: is not a valid big-integer'         |
      | 'PATCH'  | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   | 'paramBigInteger: is not a valid big-integer'         |
      | 'DELETE' | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   | 'paramBigInteger: is not a valid big-integer'         |
      | 'GET'    | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'HEAD'   | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   |                                                       |
      | 'POST'   | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'PUT'    | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'PATCH'  | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'DELETE' | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'GET'    | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'HEAD'   | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' |                                                       |
      | 'POST'   | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PUT'    | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PATCH'  | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'DELETE' | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'GET'    | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'HEAD'   | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' |                                                       |
      | 'POST'   | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'PUT'    | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'PATCH'  | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'DELETE' | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'GET'    | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           | 'paramLocalDate: is not a valid date'                 |
      | 'HEAD'   | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           |                                                       |
      | 'POST'   | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           | 'paramLocalDate: is not a valid date'                 |
      | 'PUT'    | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           | 'paramLocalDate: is not a valid date'                 |
      | 'PATCH'  | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           | 'paramLocalDate: is not a valid date'                 |
      | 'DELETE' | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           | 'paramLocalDate: is not a valid date'                 |
      | 'GET'    | '/paramYear'           | 'paramYear'           | '2100'                 | 'paramYear: is not a valid year'                      |
      | 'HEAD'   | '/paramYear'           | 'paramYear'           | '2100'                 |                                                       |
      | 'POST'   | '/paramYear'           | 'paramYear'           | '2100'                 | 'paramYear: is not a valid year'                      |
      | 'PUT'    | '/paramYear'           | 'paramYear'           | '2100'                 | 'paramYear: is not a valid year'                      |
      | 'PATCH'  | '/paramYear'           | 'paramYear'           | '2100'                 | 'paramYear: is not a valid year'                      |
      | 'DELETE' | '/paramYear'           | 'paramYear'           | '2100'                 | 'paramYear: is not a valid year'                      |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/i18n-constrained/named-method-parameter<path>?<param>=<value> endpoint (RequestParamI18nConstrainedRestApi.class)
    this endpoint receives the request parameter in a method parameter, both having the diferent names, a custom request-parameter name is defined by the endpoint 
    and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/request-parameter/i18n-constrained/named-method-parameter'<path>
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | param                    | value                  | message                                               |
      | 'GET'    | '/paramString'         | 'param-string'           | 'ab'                   | 'paramString: is not a valid string'                  |
      | 'HEAD'   | '/paramString'         | 'param-string'           | 'ab'                   |                                                       |
      | 'POST'   | '/paramString'         | 'param-string'           | 'ab'                   | 'paramString: is not a valid string'                  |
      | 'PUT'    | '/paramString'         | 'param-string'           | 'ab'                   | 'paramString: is not a valid string'                  |
      | 'PATCH'  | '/paramString'         | 'param-string'           | 'ab'                   | 'paramString: is not a valid string'                  |
      | 'DELETE' | '/paramString'         | 'param-string'           | 'ab'                   | 'paramString: is not a valid string'                  |
      | 'GET'    | '/paramInteger'        | 'param-integer'          | '10'                   | 'paramInteger: is not a valid integer'                |
      | 'HEAD'   | '/paramInteger'        | 'param-integer'          | '10'                   |                                                       |
      | 'POST'   | '/paramInteger'        | 'param-integer'          | '10'                   | 'paramInteger: is not a valid integer'                |
      | 'PUT'    | '/paramInteger'        | 'param-integer'          | '10'                   | 'paramInteger: is not a valid integer'                |
      | 'PATCH'  | '/paramInteger'        | 'param-integer'          | '10'                   | 'paramInteger: is not a valid integer'                |
      | 'DELETE' | '/paramInteger'        | 'param-integer'          | '10'                   | 'paramInteger: is not a valid integer'                |
      | 'GET'    | '/paramLong'           | 'param-long'             | '10'                   | 'paramLong: is not a valid long'                      |
      | 'HEAD'   | '/paramLong'           | 'param-long'             | '10'                   |                                                       |
      | 'POST'   | '/paramLong'           | 'param-long'             | '10'                   | 'paramLong: is not a valid long'                      |
      | 'PUT'    | '/paramLong'           | 'param-long'             | '10'                   | 'paramLong: is not a valid long'                      |
      | 'PATCH'  | '/paramLong'           | 'param-long'             | '10'                   | 'paramLong: is not a valid long'                      |
      | 'DELETE' | '/paramLong'           | 'param-long'             | '10'                   | 'paramLong: is not a valid long'                      |
      | 'GET'    | '/paramBigInteger'     | 'param-big-integer'      | '10'                   | 'paramBigInteger: is not a valid big-integer'         |
      | 'HEAD'   | '/paramBigInteger'     | 'param-big-integer'      | '10'                   |                                                       |
      | 'POST'   | '/paramBigInteger'     | 'param-big-integer'      | '10'                   | 'paramBigInteger: is not a valid big-integer'         |
      | 'PUT'    | '/paramBigInteger'     | 'param-big-integer'      | '10'                   | 'paramBigInteger: is not a valid big-integer'         |
      | 'PATCH'  | '/paramBigInteger'     | 'param-big-integer'      | '10'                   | 'paramBigInteger: is not a valid big-integer'         |
      | 'DELETE' | '/paramBigInteger'     | 'param-big-integer'      | '10'                   | 'paramBigInteger: is not a valid big-integer'         |
      | 'GET'    | '/paramBigDecimal'     | 'param-big-decimal'      | '10'                   | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'HEAD'   | '/paramBigDecimal'     | 'param-big-decimal'      | '10'                   |                                                       |
      | 'POST'   | '/paramBigDecimal'     | 'param-big-decimal'      | '10'                   | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'PUT'    | '/paramBigDecimal'     | 'param-big-decimal'      | '10'                   | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'PATCH'  | '/paramBigDecimal'     | 'param-big-decimal'      | '10'                   | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'DELETE' | '/paramBigDecimal'     | 'param-big-decimal'      | '10'                   | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'GET'    | '/paramOffsetDateTime' | 'param-offset-date-time' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'HEAD'   | '/paramOffsetDateTime' | 'param-offset-date-time' | '2100-01-01T00:00:00Z' |                                                       |
      | 'POST'   | '/paramOffsetDateTime' | 'param-offset-date-time' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PUT'    | '/paramOffsetDateTime' | 'param-offset-date-time' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PATCH'  | '/paramOffsetDateTime' | 'param-offset-date-time' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'DELETE' | '/paramOffsetDateTime' | 'param-offset-date-time' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'GET'    | '/paramLocalDateTime'  | 'param-local-date-time'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'HEAD'   | '/paramLocalDateTime'  | 'param-local-date-time'  | '2100-01-01T00:00:00Z' |                                                       |
      | 'POST'   | '/paramLocalDateTime'  | 'param-local-date-time'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'PUT'    | '/paramLocalDateTime'  | 'param-local-date-time'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'PATCH'  | '/paramLocalDateTime'  | 'param-local-date-time'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'DELETE' | '/paramLocalDateTime'  | 'param-local-date-time'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'GET'    | '/paramLocalDate'      | 'param-local-date'       | '2100-01-01'           | 'paramLocalDate: is not a valid date'                 |
      | 'HEAD'   | '/paramLocalDate'      | 'param-local-date'       | '2100-01-01'           |                                                       |
      | 'POST'   | '/paramLocalDate'      | 'param-local-date'       | '2100-01-01'           | 'paramLocalDate: is not a valid date'                 |
      | 'PUT'    | '/paramLocalDate'      | 'param-local-date'       | '2100-01-01'           | 'paramLocalDate: is not a valid date'                 |
      | 'PATCH'  | '/paramLocalDate'      | 'param-local-date'       | '2100-01-01'           | 'paramLocalDate: is not a valid date'                 |
      | 'DELETE' | '/paramLocalDate'      | 'param-local-date'       | '2100-01-01'           | 'paramLocalDate: is not a valid date'                 |
      | 'GET'    | '/paramYear'           | 'param-year'             | '2100'                 | 'paramYear: is not a valid year'                      |
      | 'HEAD'   | '/paramYear'           | 'param-year'             | '2100'                 |                                                       |
      | 'POST'   | '/paramYear'           | 'param-year'             | '2100'                 | 'paramYear: is not a valid year'                      |
      | 'PUT'    | '/paramYear'           | 'param-year'             | '2100'                 | 'paramYear: is not a valid year'                      |
      | 'PATCH'  | '/paramYear'           | 'param-year'             | '2100'                 | 'paramYear: is not a valid year'                      |
      | 'DELETE' | '/paramYear'           | 'param-year'             | '2100'                 | 'paramYear: is not a valid year'                      |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/i18n-constrained/optional-method-parameter<path>?<param>=<value> endpoint (RequestParamI18nConstrainedRestApi.class)
    this endpoint receives the request parameter in an optional method parameter, both having the same name
    and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/request-parameter/i18n-constrained/optional-method-parameter'<path>
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | param                 | value                  | message                                               |
      | 'GET'    | '/paramString'         | 'paramString'         | 'ab'                   | 'paramString: is not a valid string'                  |
      | 'HEAD'   | '/paramString'         | 'paramString'         | 'ab'                   |                                                       |
      | 'POST'   | '/paramString'         | 'paramString'         | 'ab'                   | 'paramString: is not a valid string'                  |
      | 'PUT'    | '/paramString'         | 'paramString'         | 'ab'                   | 'paramString: is not a valid string'                  |
      | 'PATCH'  | '/paramString'         | 'paramString'         | 'ab'                   | 'paramString: is not a valid string'                  |
      | 'DELETE' | '/paramString'         | 'paramString'         | 'ab'                   | 'paramString: is not a valid string'                  |
      | 'GET'    | '/paramInteger'        | 'paramInteger'        | '10'                   | 'paramInteger: is not a valid integer'                |
      | 'HEAD'   | '/paramInteger'        | 'paramInteger'        | '10'                   |                                                       |
      | 'POST'   | '/paramInteger'        | 'paramInteger'        | '10'                   | 'paramInteger: is not a valid integer'                |
      | 'PUT'    | '/paramInteger'        | 'paramInteger'        | '10'                   | 'paramInteger: is not a valid integer'                |
      | 'PATCH'  | '/paramInteger'        | 'paramInteger'        | '10'                   | 'paramInteger: is not a valid integer'                |
      | 'DELETE' | '/paramInteger'        | 'paramInteger'        | '10'                   | 'paramInteger: is not a valid integer'                |
      | 'GET'    | '/paramLong'           | 'paramLong'           | '10'                   | 'paramLong: is not a valid long'                      |
      | 'HEAD'   | '/paramLong'           | 'paramLong'           | '10'                   |                                                       |
      | 'POST'   | '/paramLong'           | 'paramLong'           | '10'                   | 'paramLong: is not a valid long'                      |
      | 'PUT'    | '/paramLong'           | 'paramLong'           | '10'                   | 'paramLong: is not a valid long'                      |
      | 'PATCH'  | '/paramLong'           | 'paramLong'           | '10'                   | 'paramLong: is not a valid long'                      |
      | 'DELETE' | '/paramLong'           | 'paramLong'           | '10'                   | 'paramLong: is not a valid long'                      |
      | 'GET'    | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   | 'paramBigInteger: is not a valid big-integer'         |
      | 'HEAD'   | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   |                                                       |
      | 'POST'   | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   | 'paramBigInteger: is not a valid big-integer'         |
      | 'PUT'    | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   | 'paramBigInteger: is not a valid big-integer'         |
      | 'PATCH'  | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   | 'paramBigInteger: is not a valid big-integer'         |
      | 'DELETE' | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   | 'paramBigInteger: is not a valid big-integer'         |
      | 'GET'    | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'HEAD'   | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   |                                                       |
      | 'POST'   | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'PUT'    | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'PATCH'  | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'DELETE' | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'GET'    | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'HEAD'   | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' |                                                       |
      | 'POST'   | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PUT'    | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PATCH'  | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'DELETE' | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'GET'    | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'HEAD'   | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' |                                                       |
      | 'POST'   | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'PUT'    | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'PATCH'  | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'DELETE' | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'GET'    | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           | 'paramLocalDate: is not a valid date'                 |
      | 'HEAD'   | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           |                                                       |
      | 'POST'   | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           | 'paramLocalDate: is not a valid date'                 |
      | 'PUT'    | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           | 'paramLocalDate: is not a valid date'                 |
      | 'PATCH'  | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           | 'paramLocalDate: is not a valid date'                 |
      | 'DELETE' | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           | 'paramLocalDate: is not a valid date'                 |
      | 'GET'    | '/paramYear'           | 'paramYear'           | '2100'                 | 'paramYear: is not a valid year'                      |
      | 'HEAD'   | '/paramYear'           | 'paramYear'           | '2100'                 |                                                       |
      | 'POST'   | '/paramYear'           | 'paramYear'           | '2100'                 | 'paramYear: is not a valid year'                      |
      | 'PUT'    | '/paramYear'           | 'paramYear'           | '2100'                 | 'paramYear: is not a valid year'                      |
      | 'PATCH'  | '/paramYear'           | 'paramYear'           | '2100'                 | 'paramYear: is not a valid year'                      |
      | 'DELETE' | '/paramYear'           | 'paramYear'           | '2100'                 | 'paramYear: is not a valid year'                      |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/i18n-constrained/named-optional-method-parameter<path>?<param>=<value> endpoint (RequestParamI18nConstrainedRestApi.class)
    this endpoint receives the request parameter in an optional method parameter, both having the diferent names, a custom request-parameter name is defined by the endpoint 
    and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/request-parameter/i18n-constrained/named-optional-method-parameter'<path>
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | param                    | value                  | message                                               |
      | 'GET'    | '/paramString'         | 'param-string'           | 'ab'                   | 'paramString: is not a valid string'                  |
      | 'HEAD'   | '/paramString'         | 'param-string'           | 'ab'                   |                                                       |
      | 'POST'   | '/paramString'         | 'param-string'           | 'ab'                   | 'paramString: is not a valid string'                  |
      | 'PUT'    | '/paramString'         | 'param-string'           | 'ab'                   | 'paramString: is not a valid string'                  |
      | 'PATCH'  | '/paramString'         | 'param-string'           | 'ab'                   | 'paramString: is not a valid string'                  |
      | 'DELETE' | '/paramString'         | 'param-string'           | 'ab'                   | 'paramString: is not a valid string'                  |
      | 'GET'    | '/paramInteger'        | 'param-integer'          | '10'                   | 'paramInteger: is not a valid integer'                |
      | 'HEAD'   | '/paramInteger'        | 'param-integer'          | '10'                   |                                                       |
      | 'POST'   | '/paramInteger'        | 'param-integer'          | '10'                   | 'paramInteger: is not a valid integer'                |
      | 'PUT'    | '/paramInteger'        | 'param-integer'          | '10'                   | 'paramInteger: is not a valid integer'                |
      | 'PATCH'  | '/paramInteger'        | 'param-integer'          | '10'                   | 'paramInteger: is not a valid integer'                |
      | 'DELETE' | '/paramInteger'        | 'param-integer'          | '10'                   | 'paramInteger: is not a valid integer'                |
      | 'GET'    | '/paramLong'           | 'param-long'             | '10'                   | 'paramLong: is not a valid long'                      |
      | 'HEAD'   | '/paramLong'           | 'param-long'             | '10'                   |                                                       |
      | 'POST'   | '/paramLong'           | 'param-long'             | '10'                   | 'paramLong: is not a valid long'                      |
      | 'PUT'    | '/paramLong'           | 'param-long'             | '10'                   | 'paramLong: is not a valid long'                      |
      | 'PATCH'  | '/paramLong'           | 'param-long'             | '10'                   | 'paramLong: is not a valid long'                      |
      | 'DELETE' | '/paramLong'           | 'param-long'             | '10'                   | 'paramLong: is not a valid long'                      |
      | 'GET'    | '/paramBigInteger'     | 'param-big-integer'      | '10'                   | 'paramBigInteger: is not a valid big-integer'         |
      | 'HEAD'   | '/paramBigInteger'     | 'param-big-integer'      | '10'                   |                                                       |
      | 'POST'   | '/paramBigInteger'     | 'param-big-integer'      | '10'                   | 'paramBigInteger: is not a valid big-integer'         |
      | 'PUT'    | '/paramBigInteger'     | 'param-big-integer'      | '10'                   | 'paramBigInteger: is not a valid big-integer'         |
      | 'PATCH'  | '/paramBigInteger'     | 'param-big-integer'      | '10'                   | 'paramBigInteger: is not a valid big-integer'         |
      | 'DELETE' | '/paramBigInteger'     | 'param-big-integer'      | '10'                   | 'paramBigInteger: is not a valid big-integer'         |
      | 'GET'    | '/paramBigDecimal'     | 'param-big-decimal'      | '10'                   | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'HEAD'   | '/paramBigDecimal'     | 'param-big-decimal'      | '10'                   |                                                       |
      | 'POST'   | '/paramBigDecimal'     | 'param-big-decimal'      | '10'                   | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'PUT'    | '/paramBigDecimal'     | 'param-big-decimal'      | '10'                   | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'PATCH'  | '/paramBigDecimal'     | 'param-big-decimal'      | '10'                   | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'DELETE' | '/paramBigDecimal'     | 'param-big-decimal'      | '10'                   | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'GET'    | '/paramOffsetDateTime' | 'param-offset-date-time' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'HEAD'   | '/paramOffsetDateTime' | 'param-offset-date-time' | '2100-01-01T00:00:00Z' |                                                       |
      | 'POST'   | '/paramOffsetDateTime' | 'param-offset-date-time' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PUT'    | '/paramOffsetDateTime' | 'param-offset-date-time' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PATCH'  | '/paramOffsetDateTime' | 'param-offset-date-time' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'DELETE' | '/paramOffsetDateTime' | 'param-offset-date-time' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'GET'    | '/paramLocalDateTime'  | 'param-local-date-time'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'HEAD'   | '/paramLocalDateTime'  | 'param-local-date-time'  | '2100-01-01T00:00:00Z' |                                                       |
      | 'POST'   | '/paramLocalDateTime'  | 'param-local-date-time'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'PUT'    | '/paramLocalDateTime'  | 'param-local-date-time'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'PATCH'  | '/paramLocalDateTime'  | 'param-local-date-time'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'DELETE' | '/paramLocalDateTime'  | 'param-local-date-time'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'GET'    | '/paramLocalDate'      | 'param-local-date'       | '2100-01-01'           | 'paramLocalDate: is not a valid date'                 |
      | 'HEAD'   | '/paramLocalDate'      | 'param-local-date'       | '2100-01-01'           |                                                       |
      | 'POST'   | '/paramLocalDate'      | 'param-local-date'       | '2100-01-01'           | 'paramLocalDate: is not a valid date'                 |
      | 'PUT'    | '/paramLocalDate'      | 'param-local-date'       | '2100-01-01'           | 'paramLocalDate: is not a valid date'                 |
      | 'PATCH'  | '/paramLocalDate'      | 'param-local-date'       | '2100-01-01'           | 'paramLocalDate: is not a valid date'                 |
      | 'DELETE' | '/paramLocalDate'      | 'param-local-date'       | '2100-01-01'           | 'paramLocalDate: is not a valid date'                 |
      | 'GET'    | '/paramYear'           | 'param-year'             | '2100'                 | 'paramYear: is not a valid year'                      |
      | 'HEAD'   | '/paramYear'           | 'param-year'             | '2100'                 |                                                       |
      | 'POST'   | '/paramYear'           | 'param-year'             | '2100'                 | 'paramYear: is not a valid year'                      |
      | 'PUT'    | '/paramYear'           | 'param-year'             | '2100'                 | 'paramYear: is not a valid year'                      |
      | 'PATCH'  | '/paramYear'           | 'param-year'             | '2100'                 | 'paramYear: is not a valid year'                      |
      | 'DELETE' | '/paramYear'           | 'param-year'             | '2100'                 | 'paramYear: is not a valid year'                      |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/i18n-constrained/dto-attribute<path>?<param>=<value> endpoint (RequestParamI18nConstrainedRestApi.class)
    this endpoint receives the request parameter in a dto attribute, both having the same name
    and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/request-parameter/i18n-constrained/dto-attribute'<path>
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | param                 | value                  | message                                               |
      | 'GET'    | '/paramString'         | 'paramString'         | 'ab'                   | 'paramString: is not a valid string'                  |
      | 'HEAD'   | '/paramString'         | 'paramString'         | 'ab'                   |                                                       |
      | 'POST'   | '/paramString'         | 'paramString'         | 'ab'                   | 'paramString: is not a valid string'                  |
      | 'PUT'    | '/paramString'         | 'paramString'         | 'ab'                   | 'paramString: is not a valid string'                  |
      | 'PATCH'  | '/paramString'         | 'paramString'         | 'ab'                   | 'paramString: is not a valid string'                  |
      | 'DELETE' | '/paramString'         | 'paramString'         | 'ab'                   | 'paramString: is not a valid string'                  |
      | 'GET'    | '/paramInteger'        | 'paramInteger'        | '10'                   | 'paramInteger: is not a valid integer'                |
      | 'HEAD'   | '/paramInteger'        | 'paramInteger'        | '10'                   |                                                       |
      | 'POST'   | '/paramInteger'        | 'paramInteger'        | '10'                   | 'paramInteger: is not a valid integer'                |
      | 'PUT'    | '/paramInteger'        | 'paramInteger'        | '10'                   | 'paramInteger: is not a valid integer'                |
      | 'PATCH'  | '/paramInteger'        | 'paramInteger'        | '10'                   | 'paramInteger: is not a valid integer'                |
      | 'DELETE' | '/paramInteger'        | 'paramInteger'        | '10'                   | 'paramInteger: is not a valid integer'                |
      | 'GET'    | '/paramLong'           | 'paramLong'           | '10'                   | 'paramLong: is not a valid long'                      |
      | 'HEAD'   | '/paramLong'           | 'paramLong'           | '10'                   |                                                       |
      | 'POST'   | '/paramLong'           | 'paramLong'           | '10'                   | 'paramLong: is not a valid long'                      |
      | 'PUT'    | '/paramLong'           | 'paramLong'           | '10'                   | 'paramLong: is not a valid long'                      |
      | 'PATCH'  | '/paramLong'           | 'paramLong'           | '10'                   | 'paramLong: is not a valid long'                      |
      | 'DELETE' | '/paramLong'           | 'paramLong'           | '10'                   | 'paramLong: is not a valid long'                      |
      | 'GET'    | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   | 'paramBigInteger: is not a valid big-integer'         |
      | 'HEAD'   | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   |                                                       |
      | 'POST'   | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   | 'paramBigInteger: is not a valid big-integer'         |
      | 'PUT'    | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   | 'paramBigInteger: is not a valid big-integer'         |
      | 'PATCH'  | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   | 'paramBigInteger: is not a valid big-integer'         |
      | 'DELETE' | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   | 'paramBigInteger: is not a valid big-integer'         |
      | 'GET'    | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'HEAD'   | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   |                                                       |
      | 'POST'   | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'PUT'    | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'PATCH'  | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'DELETE' | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'GET'    | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'HEAD'   | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' |                                                       |
      | 'POST'   | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PUT'    | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PATCH'  | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'DELETE' | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'GET'    | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'HEAD'   | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' |                                                       |
      | 'POST'   | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'PUT'    | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'PATCH'  | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'DELETE' | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'GET'    | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           | 'paramLocalDate: is not a valid date'                 |
      | 'HEAD'   | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           |                                                       |
      | 'POST'   | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           | 'paramLocalDate: is not a valid date'                 |
      | 'PUT'    | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           | 'paramLocalDate: is not a valid date'                 |
      | 'PATCH'  | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           | 'paramLocalDate: is not a valid date'                 |
      | 'DELETE' | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           | 'paramLocalDate: is not a valid date'                 |
      | 'GET'    | '/paramYear'           | 'paramYear'           | '2100'                 | 'paramYear: is not a valid year'                      |
      | 'HEAD'   | '/paramYear'           | 'paramYear'           | '2100'                 |                                                       |
      | 'POST'   | '/paramYear'           | 'paramYear'           | '2100'                 | 'paramYear: is not a valid year'                      |
      | 'PUT'    | '/paramYear'           | 'paramYear'           | '2100'                 | 'paramYear: is not a valid year'                      |
      | 'PATCH'  | '/paramYear'           | 'paramYear'           | '2100'                 | 'paramYear: is not a valid year'                      |
      | 'DELETE' | '/paramYear'           | 'paramYear'           | '2100'                 | 'paramYear: is not a valid year'                      |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/i18n-constrained/optional-dto-attribute<path>?<param>=<value> endpoint (RequestParamI18nConstrainedRestApi.class)
    this endpoint receives the request parameter in an optional dto attribute, both having the same name
    and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/request-parameter/i18n-constrained/optional-dto-attribute'<path>
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | param                 | value                  | message                                               |
      | 'GET'    | '/paramString'         | 'paramString'         | 'ab'                   | 'paramString: is not a valid string'                  |
      | 'HEAD'   | '/paramString'         | 'paramString'         | 'ab'                   |                                                       |
      | 'POST'   | '/paramString'         | 'paramString'         | 'ab'                   | 'paramString: is not a valid string'                  |
      | 'PUT'    | '/paramString'         | 'paramString'         | 'ab'                   | 'paramString: is not a valid string'                  |
      | 'PATCH'  | '/paramString'         | 'paramString'         | 'ab'                   | 'paramString: is not a valid string'                  |
      | 'DELETE' | '/paramString'         | 'paramString'         | 'ab'                   | 'paramString: is not a valid string'                  |
      | 'GET'    | '/paramInteger'        | 'paramInteger'        | '10'                   | 'paramInteger: is not a valid integer'                |
      | 'HEAD'   | '/paramInteger'        | 'paramInteger'        | '10'                   |                                                       |
      | 'POST'   | '/paramInteger'        | 'paramInteger'        | '10'                   | 'paramInteger: is not a valid integer'                |
      | 'PUT'    | '/paramInteger'        | 'paramInteger'        | '10'                   | 'paramInteger: is not a valid integer'                |
      | 'PATCH'  | '/paramInteger'        | 'paramInteger'        | '10'                   | 'paramInteger: is not a valid integer'                |
      | 'DELETE' | '/paramInteger'        | 'paramInteger'        | '10'                   | 'paramInteger: is not a valid integer'                |
      | 'GET'    | '/paramLong'           | 'paramLong'           | '10'                   | 'paramLong: is not a valid long'                      |
      | 'HEAD'   | '/paramLong'           | 'paramLong'           | '10'                   |                                                       |
      | 'POST'   | '/paramLong'           | 'paramLong'           | '10'                   | 'paramLong: is not a valid long'                      |
      | 'PUT'    | '/paramLong'           | 'paramLong'           | '10'                   | 'paramLong: is not a valid long'                      |
      | 'PATCH'  | '/paramLong'           | 'paramLong'           | '10'                   | 'paramLong: is not a valid long'                      |
      | 'DELETE' | '/paramLong'           | 'paramLong'           | '10'                   | 'paramLong: is not a valid long'                      |
      | 'GET'    | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   | 'paramBigInteger: is not a valid big-integer'         |
      | 'HEAD'   | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   |                                                       |
      | 'POST'   | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   | 'paramBigInteger: is not a valid big-integer'         |
      | 'PUT'    | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   | 'paramBigInteger: is not a valid big-integer'         |
      | 'PATCH'  | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   | 'paramBigInteger: is not a valid big-integer'         |
      | 'DELETE' | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   | 'paramBigInteger: is not a valid big-integer'         |
      | 'GET'    | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'HEAD'   | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   |                                                       |
      | 'POST'   | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'PUT'    | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'PATCH'  | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'DELETE' | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   | 'paramBigDecimal: is not a valid big-decimal'         |
      | 'GET'    | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'HEAD'   | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' |                                                       |
      | 'POST'   | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PUT'    | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PATCH'  | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'DELETE' | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: is not a valid offset-datetime' |
      | 'GET'    | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'HEAD'   | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' |                                                       |
      | 'POST'   | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'PUT'    | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'PATCH'  | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'DELETE' | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: is not a valid local-datetime'   |
      | 'GET'    | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           | 'paramLocalDate: is not a valid date'                 |
      | 'HEAD'   | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           |                                                       |
      | 'POST'   | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           | 'paramLocalDate: is not a valid date'                 |
      | 'PUT'    | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           | 'paramLocalDate: is not a valid date'                 |
      | 'PATCH'  | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           | 'paramLocalDate: is not a valid date'                 |
      | 'DELETE' | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           | 'paramLocalDate: is not a valid date'                 |
      | 'GET'    | '/paramYear'           | 'paramYear'           | '2100'                 | 'paramYear: is not a valid year'                      |
      | 'HEAD'   | '/paramYear'           | 'paramYear'           | '2100'                 |                                                       |
      | 'POST'   | '/paramYear'           | 'paramYear'           | '2100'                 | 'paramYear: is not a valid year'                      |
      | 'PUT'    | '/paramYear'           | 'paramYear'           | '2100'                 | 'paramYear: is not a valid year'                      |
      | 'PATCH'  | '/paramYear'           | 'paramYear'           | '2100'                 | 'paramYear: is not a valid year'                      |
      | 'DELETE' | '/paramYear'           | 'paramYear'           | '2100'                 | 'paramYear: is not a valid year'                      |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/i18n-constrained/nested-dto-attribute<path>?<param>=<value> endpoint (RequestParamI18nConstrainedRestApi.class)
    this endpoint receives the request parameter in a nested dto attribute, both having the same name
    and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/request-parameter/i18n-constrained/nested-dto-attribute'<path>
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | param                        | value                  | message                                                      |
      | 'GET'    | '/paramString'         | 'nested.paramString'         | 'ab'                   | 'nested.paramString: is not a valid string'                  |
      | 'HEAD'   | '/paramString'         | 'nested.paramString'         | 'ab'                   |                                                              |
      | 'POST'   | '/paramString'         | 'nested.paramString'         | 'ab'                   | 'nested.paramString: is not a valid string'                  |
      | 'PUT'    | '/paramString'         | 'nested.paramString'         | 'ab'                   | 'nested.paramString: is not a valid string'                  |
      | 'PATCH'  | '/paramString'         | 'nested.paramString'         | 'ab'                   | 'nested.paramString: is not a valid string'                  |
      | 'DELETE' | '/paramString'         | 'nested.paramString'         | 'ab'                   | 'nested.paramString: is not a valid string'                  |
      | 'GET'    | '/paramInteger'        | 'nested.paramInteger'        | '10'                   | 'nested.paramInteger: is not a valid integer'                |
      | 'HEAD'   | '/paramInteger'        | 'nested.paramInteger'        | '10'                   |                                                              |
      | 'POST'   | '/paramInteger'        | 'nested.paramInteger'        | '10'                   | 'nested.paramInteger: is not a valid integer'                |
      | 'PUT'    | '/paramInteger'        | 'nested.paramInteger'        | '10'                   | 'nested.paramInteger: is not a valid integer'                |
      | 'PATCH'  | '/paramInteger'        | 'nested.paramInteger'        | '10'                   | 'nested.paramInteger: is not a valid integer'                |
      | 'DELETE' | '/paramInteger'        | 'nested.paramInteger'        | '10'                   | 'nested.paramInteger: is not a valid integer'                |
      | 'GET'    | '/paramLong'           | 'nested.paramLong'           | '10'                   | 'nested.paramLong: is not a valid long'                      |
      | 'HEAD'   | '/paramLong'           | 'nested.paramLong'           | '10'                   |                                                              |
      | 'POST'   | '/paramLong'           | 'nested.paramLong'           | '10'                   | 'nested.paramLong: is not a valid long'                      |
      | 'PUT'    | '/paramLong'           | 'nested.paramLong'           | '10'                   | 'nested.paramLong: is not a valid long'                      |
      | 'PATCH'  | '/paramLong'           | 'nested.paramLong'           | '10'                   | 'nested.paramLong: is not a valid long'                      |
      | 'DELETE' | '/paramLong'           | 'nested.paramLong'           | '10'                   | 'nested.paramLong: is not a valid long'                      |
      | 'GET'    | '/paramBigInteger'     | 'nested.paramBigInteger'     | '10'                   | 'nested.paramBigInteger: is not a valid big-integer'         |
      | 'HEAD'   | '/paramBigInteger'     | 'nested.paramBigInteger'     | '10'                   |                                                              |
      | 'POST'   | '/paramBigInteger'     | 'nested.paramBigInteger'     | '10'                   | 'nested.paramBigInteger: is not a valid big-integer'         |
      | 'PUT'    | '/paramBigInteger'     | 'nested.paramBigInteger'     | '10'                   | 'nested.paramBigInteger: is not a valid big-integer'         |
      | 'PATCH'  | '/paramBigInteger'     | 'nested.paramBigInteger'     | '10'                   | 'nested.paramBigInteger: is not a valid big-integer'         |
      | 'DELETE' | '/paramBigInteger'     | 'nested.paramBigInteger'     | '10'                   | 'nested.paramBigInteger: is not a valid big-integer'         |
      | 'GET'    | '/paramBigDecimal'     | 'nested.paramBigDecimal'     | '10'                   | 'nested.paramBigDecimal: is not a valid big-decimal'         |
      | 'HEAD'   | '/paramBigDecimal'     | 'nested.paramBigDecimal'     | '10'                   |                                                              |
      | 'POST'   | '/paramBigDecimal'     | 'nested.paramBigDecimal'     | '10'                   | 'nested.paramBigDecimal: is not a valid big-decimal'         |
      | 'PUT'    | '/paramBigDecimal'     | 'nested.paramBigDecimal'     | '10'                   | 'nested.paramBigDecimal: is not a valid big-decimal'         |
      | 'PATCH'  | '/paramBigDecimal'     | 'nested.paramBigDecimal'     | '10'                   | 'nested.paramBigDecimal: is not a valid big-decimal'         |
      | 'DELETE' | '/paramBigDecimal'     | 'nested.paramBigDecimal'     | '10'                   | 'nested.paramBigDecimal: is not a valid big-decimal'         |
      | 'GET'    | '/paramOffsetDateTime' | 'nested.paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'nested.paramOffsetDateTime: is not a valid offset-datetime' |
      | 'HEAD'   | '/paramOffsetDateTime' | 'nested.paramOffsetDateTime' | '2100-01-01T00:00:00Z' |                                                              |
      | 'POST'   | '/paramOffsetDateTime' | 'nested.paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'nested.paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PUT'    | '/paramOffsetDateTime' | 'nested.paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'nested.paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PATCH'  | '/paramOffsetDateTime' | 'nested.paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'nested.paramOffsetDateTime: is not a valid offset-datetime' |
      | 'DELETE' | '/paramOffsetDateTime' | 'nested.paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'nested.paramOffsetDateTime: is not a valid offset-datetime' |
      | 'GET'    | '/paramLocalDateTime'  | 'nested.paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'nested.paramLocalDateTime: is not a valid local-datetime'   |
      | 'HEAD'   | '/paramLocalDateTime'  | 'nested.paramLocalDateTime'  | '2100-01-01T00:00:00Z' |                                                              |
      | 'POST'   | '/paramLocalDateTime'  | 'nested.paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'nested.paramLocalDateTime: is not a valid local-datetime'   |
      | 'PUT'    | '/paramLocalDateTime'  | 'nested.paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'nested.paramLocalDateTime: is not a valid local-datetime'   |
      | 'PATCH'  | '/paramLocalDateTime'  | 'nested.paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'nested.paramLocalDateTime: is not a valid local-datetime'   |
      | 'DELETE' | '/paramLocalDateTime'  | 'nested.paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'nested.paramLocalDateTime: is not a valid local-datetime'   |
      | 'GET'    | '/paramLocalDate'      | 'nested.paramLocalDate'      | '2100-01-01'           | 'nested.paramLocalDate: is not a valid date'                 |
      | 'HEAD'   | '/paramLocalDate'      | 'nested.paramLocalDate'      | '2100-01-01'           |                                                              |
      | 'POST'   | '/paramLocalDate'      | 'nested.paramLocalDate'      | '2100-01-01'           | 'nested.paramLocalDate: is not a valid date'                 |
      | 'PUT'    | '/paramLocalDate'      | 'nested.paramLocalDate'      | '2100-01-01'           | 'nested.paramLocalDate: is not a valid date'                 |
      | 'PATCH'  | '/paramLocalDate'      | 'nested.paramLocalDate'      | '2100-01-01'           | 'nested.paramLocalDate: is not a valid date'                 |
      | 'DELETE' | '/paramLocalDate'      | 'nested.paramLocalDate'      | '2100-01-01'           | 'nested.paramLocalDate: is not a valid date'                 |
      | 'GET'    | '/paramYear'           | 'nested.paramYear'           | '2100'                 | 'nested.paramYear: is not a valid year'                      |
      | 'HEAD'   | '/paramYear'           | 'nested.paramYear'           | '2100'                 |                                                              |
      | 'POST'   | '/paramYear'           | 'nested.paramYear'           | '2100'                 | 'nested.paramYear: is not a valid year'                      |
      | 'PUT'    | '/paramYear'           | 'nested.paramYear'           | '2100'                 | 'nested.paramYear: is not a valid year'                      |
      | 'PATCH'  | '/paramYear'           | 'nested.paramYear'           | '2100'                 | 'nested.paramYear: is not a valid year'                      |
      | 'DELETE' | '/paramYear'           | 'nested.paramYear'           | '2100'                 | 'nested.paramYear: is not a valid year'                      |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/i18n-constrained/nested-optional-dto-attribute<path>?<param>=<value> endpoint (RequestParamI18nConstrainedRestApi.class)
    this endpoint receives the request parameter in a nested optional-dto attribute, both having the same name
    and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/request-parameter/i18n-constrained/nested-optional-dto-attribute'<path>
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | param                        | value                  | message                                                      |
      | 'GET'    | '/paramString'         | 'nested.paramString'         | 'ab'                   | 'nested.paramString: is not a valid string'                  |
      | 'HEAD'   | '/paramString'         | 'nested.paramString'         | 'ab'                   |                                                              |
      | 'POST'   | '/paramString'         | 'nested.paramString'         | 'ab'                   | 'nested.paramString: is not a valid string'                  |
      | 'PUT'    | '/paramString'         | 'nested.paramString'         | 'ab'                   | 'nested.paramString: is not a valid string'                  |
      | 'PATCH'  | '/paramString'         | 'nested.paramString'         | 'ab'                   | 'nested.paramString: is not a valid string'                  |
      | 'DELETE' | '/paramString'         | 'nested.paramString'         | 'ab'                   | 'nested.paramString: is not a valid string'                  |
      | 'GET'    | '/paramInteger'        | 'nested.paramInteger'        | '10'                   | 'nested.paramInteger: is not a valid integer'                |
      | 'HEAD'   | '/paramInteger'        | 'nested.paramInteger'        | '10'                   |                                                              |
      | 'POST'   | '/paramInteger'        | 'nested.paramInteger'        | '10'                   | 'nested.paramInteger: is not a valid integer'                |
      | 'PUT'    | '/paramInteger'        | 'nested.paramInteger'        | '10'                   | 'nested.paramInteger: is not a valid integer'                |
      | 'PATCH'  | '/paramInteger'        | 'nested.paramInteger'        | '10'                   | 'nested.paramInteger: is not a valid integer'                |
      | 'DELETE' | '/paramInteger'        | 'nested.paramInteger'        | '10'                   | 'nested.paramInteger: is not a valid integer'                |
      | 'GET'    | '/paramLong'           | 'nested.paramLong'           | '10'                   | 'nested.paramLong: is not a valid long'                      |
      | 'HEAD'   | '/paramLong'           | 'nested.paramLong'           | '10'                   |                                                              |
      | 'POST'   | '/paramLong'           | 'nested.paramLong'           | '10'                   | 'nested.paramLong: is not a valid long'                      |
      | 'PUT'    | '/paramLong'           | 'nested.paramLong'           | '10'                   | 'nested.paramLong: is not a valid long'                      |
      | 'PATCH'  | '/paramLong'           | 'nested.paramLong'           | '10'                   | 'nested.paramLong: is not a valid long'                      |
      | 'DELETE' | '/paramLong'           | 'nested.paramLong'           | '10'                   | 'nested.paramLong: is not a valid long'                      |
      | 'GET'    | '/paramBigInteger'     | 'nested.paramBigInteger'     | '10'                   | 'nested.paramBigInteger: is not a valid big-integer'         |
      | 'HEAD'   | '/paramBigInteger'     | 'nested.paramBigInteger'     | '10'                   |                                                              |
      | 'POST'   | '/paramBigInteger'     | 'nested.paramBigInteger'     | '10'                   | 'nested.paramBigInteger: is not a valid big-integer'         |
      | 'PUT'    | '/paramBigInteger'     | 'nested.paramBigInteger'     | '10'                   | 'nested.paramBigInteger: is not a valid big-integer'         |
      | 'PATCH'  | '/paramBigInteger'     | 'nested.paramBigInteger'     | '10'                   | 'nested.paramBigInteger: is not a valid big-integer'         |
      | 'DELETE' | '/paramBigInteger'     | 'nested.paramBigInteger'     | '10'                   | 'nested.paramBigInteger: is not a valid big-integer'         |
      | 'GET'    | '/paramBigDecimal'     | 'nested.paramBigDecimal'     | '10'                   | 'nested.paramBigDecimal: is not a valid big-decimal'         |
      | 'HEAD'   | '/paramBigDecimal'     | 'nested.paramBigDecimal'     | '10'                   |                                                              |
      | 'POST'   | '/paramBigDecimal'     | 'nested.paramBigDecimal'     | '10'                   | 'nested.paramBigDecimal: is not a valid big-decimal'         |
      | 'PUT'    | '/paramBigDecimal'     | 'nested.paramBigDecimal'     | '10'                   | 'nested.paramBigDecimal: is not a valid big-decimal'         |
      | 'PATCH'  | '/paramBigDecimal'     | 'nested.paramBigDecimal'     | '10'                   | 'nested.paramBigDecimal: is not a valid big-decimal'         |
      | 'DELETE' | '/paramBigDecimal'     | 'nested.paramBigDecimal'     | '10'                   | 'nested.paramBigDecimal: is not a valid big-decimal'         |
      | 'GET'    | '/paramOffsetDateTime' | 'nested.paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'nested.paramOffsetDateTime: is not a valid offset-datetime' |
      | 'HEAD'   | '/paramOffsetDateTime' | 'nested.paramOffsetDateTime' | '2100-01-01T00:00:00Z' |                                                              |
      | 'POST'   | '/paramOffsetDateTime' | 'nested.paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'nested.paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PUT'    | '/paramOffsetDateTime' | 'nested.paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'nested.paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PATCH'  | '/paramOffsetDateTime' | 'nested.paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'nested.paramOffsetDateTime: is not a valid offset-datetime' |
      | 'DELETE' | '/paramOffsetDateTime' | 'nested.paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'nested.paramOffsetDateTime: is not a valid offset-datetime' |
      | 'GET'    | '/paramLocalDateTime'  | 'nested.paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'nested.paramLocalDateTime: is not a valid local-datetime'   |
      | 'HEAD'   | '/paramLocalDateTime'  | 'nested.paramLocalDateTime'  | '2100-01-01T00:00:00Z' |                                                              |
      | 'POST'   | '/paramLocalDateTime'  | 'nested.paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'nested.paramLocalDateTime: is not a valid local-datetime'   |
      | 'PUT'    | '/paramLocalDateTime'  | 'nested.paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'nested.paramLocalDateTime: is not a valid local-datetime'   |
      | 'PATCH'  | '/paramLocalDateTime'  | 'nested.paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'nested.paramLocalDateTime: is not a valid local-datetime'   |
      | 'DELETE' | '/paramLocalDateTime'  | 'nested.paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'nested.paramLocalDateTime: is not a valid local-datetime'   |
      | 'GET'    | '/paramLocalDate'      | 'nested.paramLocalDate'      | '2100-01-01'           | 'nested.paramLocalDate: is not a valid date'                 |
      | 'HEAD'   | '/paramLocalDate'      | 'nested.paramLocalDate'      | '2100-01-01'           |                                                              |
      | 'POST'   | '/paramLocalDate'      | 'nested.paramLocalDate'      | '2100-01-01'           | 'nested.paramLocalDate: is not a valid date'                 |
      | 'PUT'    | '/paramLocalDate'      | 'nested.paramLocalDate'      | '2100-01-01'           | 'nested.paramLocalDate: is not a valid date'                 |
      | 'PATCH'  | '/paramLocalDate'      | 'nested.paramLocalDate'      | '2100-01-01'           | 'nested.paramLocalDate: is not a valid date'                 |
      | 'DELETE' | '/paramLocalDate'      | 'nested.paramLocalDate'      | '2100-01-01'           | 'nested.paramLocalDate: is not a valid date'                 |
      | 'GET'    | '/paramYear'           | 'nested.paramYear'           | '2100'                 | 'nested.paramYear: is not a valid year'                      |
      | 'HEAD'   | '/paramYear'           | 'nested.paramYear'           | '2100'                 |                                                              |
      | 'POST'   | '/paramYear'           | 'nested.paramYear'           | '2100'                 | 'nested.paramYear: is not a valid year'                      |
      | 'PUT'    | '/paramYear'           | 'nested.paramYear'           | '2100'                 | 'nested.paramYear: is not a valid year'                      |
      | 'PATCH'  | '/paramYear'           | 'nested.paramYear'           | '2100'                 | 'nested.paramYear: is not a valid year'                      |
      | 'DELETE' | '/paramYear'           | 'nested.paramYear'           | '2100'                 | 'nested.paramYear: is not a valid year'                      |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/i18n-constrained/nested-dto-attribute<path>?<param>=<value> endpoint (RequestParamI18nConstrainedRestApi.class)
    this endpoint receives the request parameter in a nested dto attribute from an array, both having the same name
    and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/request-parameter/i18n-constrained/nested-dto-attribute'<path>
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | param                          | value                  | message                                                        |
      | 'GET'    | '/paramString'         | 'array[0].paramString'         | 'ab'                   | 'array[0].paramString: is not a valid string'                  |
      | 'HEAD'   | '/paramString'         | 'array[0].paramString'         | 'ab'                   |                                                                |
      | 'POST'   | '/paramString'         | 'array[0].paramString'         | 'ab'                   | 'array[0].paramString: is not a valid string'                  |
      | 'PUT'    | '/paramString'         | 'array[0].paramString'         | 'ab'                   | 'array[0].paramString: is not a valid string'                  |
      | 'PATCH'  | '/paramString'         | 'array[0].paramString'         | 'ab'                   | 'array[0].paramString: is not a valid string'                  |
      | 'DELETE' | '/paramString'         | 'array[0].paramString'         | 'ab'                   | 'array[0].paramString: is not a valid string'                  |
      | 'GET'    | '/paramInteger'        | 'array[0].paramInteger'        | '10'                   | 'array[0].paramInteger: is not a valid integer'                |
      | 'HEAD'   | '/paramInteger'        | 'array[0].paramInteger'        | '10'                   |                                                                |
      | 'POST'   | '/paramInteger'        | 'array[0].paramInteger'        | '10'                   | 'array[0].paramInteger: is not a valid integer'                |
      | 'PUT'    | '/paramInteger'        | 'array[0].paramInteger'        | '10'                   | 'array[0].paramInteger: is not a valid integer'                |
      | 'PATCH'  | '/paramInteger'        | 'array[0].paramInteger'        | '10'                   | 'array[0].paramInteger: is not a valid integer'                |
      | 'DELETE' | '/paramInteger'        | 'array[0].paramInteger'        | '10'                   | 'array[0].paramInteger: is not a valid integer'                |
      | 'GET'    | '/paramLong'           | 'array[0].paramLong'           | '10'                   | 'array[0].paramLong: is not a valid long'                      |
      | 'HEAD'   | '/paramLong'           | 'array[0].paramLong'           | '10'                   |                                                                |
      | 'POST'   | '/paramLong'           | 'array[0].paramLong'           | '10'                   | 'array[0].paramLong: is not a valid long'                      |
      | 'PUT'    | '/paramLong'           | 'array[0].paramLong'           | '10'                   | 'array[0].paramLong: is not a valid long'                      |
      | 'PATCH'  | '/paramLong'           | 'array[0].paramLong'           | '10'                   | 'array[0].paramLong: is not a valid long'                      |
      | 'DELETE' | '/paramLong'           | 'array[0].paramLong'           | '10'                   | 'array[0].paramLong: is not a valid long'                      |
      | 'GET'    | '/paramBigInteger'     | 'array[0].paramBigInteger'     | '10'                   | 'array[0].paramBigInteger: is not a valid big-integer'         |
      | 'HEAD'   | '/paramBigInteger'     | 'array[0].paramBigInteger'     | '10'                   |                                                                |
      | 'POST'   | '/paramBigInteger'     | 'array[0].paramBigInteger'     | '10'                   | 'array[0].paramBigInteger: is not a valid big-integer'         |
      | 'PUT'    | '/paramBigInteger'     | 'array[0].paramBigInteger'     | '10'                   | 'array[0].paramBigInteger: is not a valid big-integer'         |
      | 'PATCH'  | '/paramBigInteger'     | 'array[0].paramBigInteger'     | '10'                   | 'array[0].paramBigInteger: is not a valid big-integer'         |
      | 'DELETE' | '/paramBigInteger'     | 'array[0].paramBigInteger'     | '10'                   | 'array[0].paramBigInteger: is not a valid big-integer'         |
      | 'GET'    | '/paramBigDecimal'     | 'array[0].paramBigDecimal'     | '10'                   | 'array[0].paramBigDecimal: is not a valid big-decimal'         |
      | 'HEAD'   | '/paramBigDecimal'     | 'array[0].paramBigDecimal'     | '10'                   |                                                                |
      | 'POST'   | '/paramBigDecimal'     | 'array[0].paramBigDecimal'     | '10'                   | 'array[0].paramBigDecimal: is not a valid big-decimal'         |
      | 'PUT'    | '/paramBigDecimal'     | 'array[0].paramBigDecimal'     | '10'                   | 'array[0].paramBigDecimal: is not a valid big-decimal'         |
      | 'PATCH'  | '/paramBigDecimal'     | 'array[0].paramBigDecimal'     | '10'                   | 'array[0].paramBigDecimal: is not a valid big-decimal'         |
      | 'DELETE' | '/paramBigDecimal'     | 'array[0].paramBigDecimal'     | '10'                   | 'array[0].paramBigDecimal: is not a valid big-decimal'         |
      | 'GET'    | '/paramOffsetDateTime' | 'array[0].paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'array[0].paramOffsetDateTime: is not a valid offset-datetime' |
      | 'HEAD'   | '/paramOffsetDateTime' | 'array[0].paramOffsetDateTime' | '2100-01-01T00:00:00Z' |                                                                |
      | 'POST'   | '/paramOffsetDateTime' | 'array[0].paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'array[0].paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PUT'    | '/paramOffsetDateTime' | 'array[0].paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'array[0].paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PATCH'  | '/paramOffsetDateTime' | 'array[0].paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'array[0].paramOffsetDateTime: is not a valid offset-datetime' |
      | 'DELETE' | '/paramOffsetDateTime' | 'array[0].paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'array[0].paramOffsetDateTime: is not a valid offset-datetime' |
      | 'GET'    | '/paramLocalDateTime'  | 'array[0].paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'array[0].paramLocalDateTime: is not a valid local-datetime'   |
      | 'HEAD'   | '/paramLocalDateTime'  | 'array[0].paramLocalDateTime'  | '2100-01-01T00:00:00Z' |                                                                |
      | 'POST'   | '/paramLocalDateTime'  | 'array[0].paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'array[0].paramLocalDateTime: is not a valid local-datetime'   |
      | 'PUT'    | '/paramLocalDateTime'  | 'array[0].paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'array[0].paramLocalDateTime: is not a valid local-datetime'   |
      | 'PATCH'  | '/paramLocalDateTime'  | 'array[0].paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'array[0].paramLocalDateTime: is not a valid local-datetime'   |
      | 'DELETE' | '/paramLocalDateTime'  | 'array[0].paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'array[0].paramLocalDateTime: is not a valid local-datetime'   |
      | 'GET'    | '/paramLocalDate'      | 'array[0].paramLocalDate'      | '2100-01-01'           | 'array[0].paramLocalDate: is not a valid date'                 |
      | 'HEAD'   | '/paramLocalDate'      | 'array[0].paramLocalDate'      | '2100-01-01'           |                                                                |
      | 'POST'   | '/paramLocalDate'      | 'array[0].paramLocalDate'      | '2100-01-01'           | 'array[0].paramLocalDate: is not a valid date'                 |
      | 'PUT'    | '/paramLocalDate'      | 'array[0].paramLocalDate'      | '2100-01-01'           | 'array[0].paramLocalDate: is not a valid date'                 |
      | 'PATCH'  | '/paramLocalDate'      | 'array[0].paramLocalDate'      | '2100-01-01'           | 'array[0].paramLocalDate: is not a valid date'                 |
      | 'DELETE' | '/paramLocalDate'      | 'array[0].paramLocalDate'      | '2100-01-01'           | 'array[0].paramLocalDate: is not a valid date'                 |
      | 'GET'    | '/paramYear'           | 'array[0].paramYear'           | '2100'                 | 'array[0].paramYear: is not a valid year'                      |
      | 'HEAD'   | '/paramYear'           | 'array[0].paramYear'           | '2100'                 |                                                                |
      | 'POST'   | '/paramYear'           | 'array[0].paramYear'           | '2100'                 | 'array[0].paramYear: is not a valid year'                      |
      | 'PUT'    | '/paramYear'           | 'array[0].paramYear'           | '2100'                 | 'array[0].paramYear: is not a valid year'                      |
      | 'PATCH'  | '/paramYear'           | 'array[0].paramYear'           | '2100'                 | 'array[0].paramYear: is not a valid year'                      |
      | 'DELETE' | '/paramYear'           | 'array[0].paramYear'           | '2100'                 | 'array[0].paramYear: is not a valid year'                      |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/i18n-constrained/nested-optional-dto-attribute<path>?<param>=<value> endpoint (RequestParamI18nConstrainedRestApi.class)
    this endpoint receives the request parameter in a nested optional dto attribute from an array, both having the same name
    and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/request-parameter/i18n-constrained/nested-optional-dto-attribute'<path>
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | param                          | value                  | message                                                        |
      | 'GET'    | '/paramString'         | 'array[0].paramString'         | 'ab'                   | 'array[0].paramString: is not a valid string'                  |
      | 'HEAD'   | '/paramString'         | 'array[0].paramString'         | 'ab'                   |                                                                |
      | 'POST'   | '/paramString'         | 'array[0].paramString'         | 'ab'                   | 'array[0].paramString: is not a valid string'                  |
      | 'PUT'    | '/paramString'         | 'array[0].paramString'         | 'ab'                   | 'array[0].paramString: is not a valid string'                  |
      | 'PATCH'  | '/paramString'         | 'array[0].paramString'         | 'ab'                   | 'array[0].paramString: is not a valid string'                  |
      | 'DELETE' | '/paramString'         | 'array[0].paramString'         | 'ab'                   | 'array[0].paramString: is not a valid string'                  |
      | 'GET'    | '/paramInteger'        | 'array[0].paramInteger'        | '10'                   | 'array[0].paramInteger: is not a valid integer'                |
      | 'HEAD'   | '/paramInteger'        | 'array[0].paramInteger'        | '10'                   |                                                                |
      | 'POST'   | '/paramInteger'        | 'array[0].paramInteger'        | '10'                   | 'array[0].paramInteger: is not a valid integer'                |
      | 'PUT'    | '/paramInteger'        | 'array[0].paramInteger'        | '10'                   | 'array[0].paramInteger: is not a valid integer'                |
      | 'PATCH'  | '/paramInteger'        | 'array[0].paramInteger'        | '10'                   | 'array[0].paramInteger: is not a valid integer'                |
      | 'DELETE' | '/paramInteger'        | 'array[0].paramInteger'        | '10'                   | 'array[0].paramInteger: is not a valid integer'                |
      | 'GET'    | '/paramLong'           | 'array[0].paramLong'           | '10'                   | 'array[0].paramLong: is not a valid long'                      |
      | 'HEAD'   | '/paramLong'           | 'array[0].paramLong'           | '10'                   |                                                                |
      | 'POST'   | '/paramLong'           | 'array[0].paramLong'           | '10'                   | 'array[0].paramLong: is not a valid long'                      |
      | 'PUT'    | '/paramLong'           | 'array[0].paramLong'           | '10'                   | 'array[0].paramLong: is not a valid long'                      |
      | 'PATCH'  | '/paramLong'           | 'array[0].paramLong'           | '10'                   | 'array[0].paramLong: is not a valid long'                      |
      | 'DELETE' | '/paramLong'           | 'array[0].paramLong'           | '10'                   | 'array[0].paramLong: is not a valid long'                      |
      | 'GET'    | '/paramBigInteger'     | 'array[0].paramBigInteger'     | '10'                   | 'array[0].paramBigInteger: is not a valid big-integer'         |
      | 'HEAD'   | '/paramBigInteger'     | 'array[0].paramBigInteger'     | '10'                   |                                                                |
      | 'POST'   | '/paramBigInteger'     | 'array[0].paramBigInteger'     | '10'                   | 'array[0].paramBigInteger: is not a valid big-integer'         |
      | 'PUT'    | '/paramBigInteger'     | 'array[0].paramBigInteger'     | '10'                   | 'array[0].paramBigInteger: is not a valid big-integer'         |
      | 'PATCH'  | '/paramBigInteger'     | 'array[0].paramBigInteger'     | '10'                   | 'array[0].paramBigInteger: is not a valid big-integer'         |
      | 'DELETE' | '/paramBigInteger'     | 'array[0].paramBigInteger'     | '10'                   | 'array[0].paramBigInteger: is not a valid big-integer'         |
      | 'GET'    | '/paramBigDecimal'     | 'array[0].paramBigDecimal'     | '10'                   | 'array[0].paramBigDecimal: is not a valid big-decimal'         |
      | 'HEAD'   | '/paramBigDecimal'     | 'array[0].paramBigDecimal'     | '10'                   |                                                                |
      | 'POST'   | '/paramBigDecimal'     | 'array[0].paramBigDecimal'     | '10'                   | 'array[0].paramBigDecimal: is not a valid big-decimal'         |
      | 'PUT'    | '/paramBigDecimal'     | 'array[0].paramBigDecimal'     | '10'                   | 'array[0].paramBigDecimal: is not a valid big-decimal'         |
      | 'PATCH'  | '/paramBigDecimal'     | 'array[0].paramBigDecimal'     | '10'                   | 'array[0].paramBigDecimal: is not a valid big-decimal'         |
      | 'DELETE' | '/paramBigDecimal'     | 'array[0].paramBigDecimal'     | '10'                   | 'array[0].paramBigDecimal: is not a valid big-decimal'         |
      | 'GET'    | '/paramOffsetDateTime' | 'array[0].paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'array[0].paramOffsetDateTime: is not a valid offset-datetime' |
      | 'HEAD'   | '/paramOffsetDateTime' | 'array[0].paramOffsetDateTime' | '2100-01-01T00:00:00Z' |                                                                |
      | 'POST'   | '/paramOffsetDateTime' | 'array[0].paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'array[0].paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PUT'    | '/paramOffsetDateTime' | 'array[0].paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'array[0].paramOffsetDateTime: is not a valid offset-datetime' |
      | 'PATCH'  | '/paramOffsetDateTime' | 'array[0].paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'array[0].paramOffsetDateTime: is not a valid offset-datetime' |
      | 'DELETE' | '/paramOffsetDateTime' | 'array[0].paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'array[0].paramOffsetDateTime: is not a valid offset-datetime' |
      | 'GET'    | '/paramLocalDateTime'  | 'array[0].paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'array[0].paramLocalDateTime: is not a valid local-datetime'   |
      | 'HEAD'   | '/paramLocalDateTime'  | 'array[0].paramLocalDateTime'  | '2100-01-01T00:00:00Z' |                                                                |
      | 'POST'   | '/paramLocalDateTime'  | 'array[0].paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'array[0].paramLocalDateTime: is not a valid local-datetime'   |
      | 'PUT'    | '/paramLocalDateTime'  | 'array[0].paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'array[0].paramLocalDateTime: is not a valid local-datetime'   |
      | 'PATCH'  | '/paramLocalDateTime'  | 'array[0].paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'array[0].paramLocalDateTime: is not a valid local-datetime'   |
      | 'DELETE' | '/paramLocalDateTime'  | 'array[0].paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'array[0].paramLocalDateTime: is not a valid local-datetime'   |
      | 'GET'    | '/paramLocalDate'      | 'array[0].paramLocalDate'      | '2100-01-01'           | 'array[0].paramLocalDate: is not a valid date'                 |
      | 'HEAD'   | '/paramLocalDate'      | 'array[0].paramLocalDate'      | '2100-01-01'           |                                                                |
      | 'POST'   | '/paramLocalDate'      | 'array[0].paramLocalDate'      | '2100-01-01'           | 'array[0].paramLocalDate: is not a valid date'                 |
      | 'PUT'    | '/paramLocalDate'      | 'array[0].paramLocalDate'      | '2100-01-01'           | 'array[0].paramLocalDate: is not a valid date'                 |
      | 'PATCH'  | '/paramLocalDate'      | 'array[0].paramLocalDate'      | '2100-01-01'           | 'array[0].paramLocalDate: is not a valid date'                 |
      | 'DELETE' | '/paramLocalDate'      | 'array[0].paramLocalDate'      | '2100-01-01'           | 'array[0].paramLocalDate: is not a valid date'                 |
      | 'GET'    | '/paramYear'           | 'array[0].paramYear'           | '2100'                 | 'array[0].paramYear: is not a valid year'                      |
      | 'HEAD'   | '/paramYear'           | 'array[0].paramYear'           | '2100'                 |                                                                |
      | 'POST'   | '/paramYear'           | 'array[0].paramYear'           | '2100'                 | 'array[0].paramYear: is not a valid year'                      |
      | 'PUT'    | '/paramYear'           | 'array[0].paramYear'           | '2100'                 | 'array[0].paramYear: is not a valid year'                      |
      | 'PATCH'  | '/paramYear'           | 'array[0].paramYear'           | '2100'                 | 'array[0].paramYear: is not a valid year'                      |
      | 'DELETE' | '/paramYear'           | 'array[0].paramYear'           | '2100'                 | 'array[0].paramYear: is not a valid year'                      |
