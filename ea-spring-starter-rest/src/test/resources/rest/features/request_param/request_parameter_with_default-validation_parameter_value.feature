Feature: Request parameter with default-validation for parameter value
  
  The REST service receives data via the request parameter and it does not accomplish with some constraint. The service responds with an error message.
  
   Rule: The conversion between data types is given by the following table
  
  | parameter type | java type      | validation              | validation                                   |
  |----------------|----------------|-------------------------|----------------------------------------------|
  | string         | String         | @Size                   | @Size(max = 1)                               |
  | number         | Integer        | @Min @Max               | @Max(1)                                      |
  | number         | Long           | @Min @Max               | @Max(1)                                      |
  | number         | BigInteger     | @DecimalMin @DecimalMax | @DecimalMax("1")                             |
  | number         | Float          |                         |                                              |
  | number         | Double         |                         |                                              |
  | number         | BigDecimal     | @DecimalMin @DecimalMax | @DecimalMax("9.9")                           |
  | boolean        | Boolean        |                         |                                              |
  | ISO-8601       | OffsetDateTime | @DatetimeRange          | @DatetimeRange(max = "2000-01-01T00:00:00Z") |
  | ISO-8601       | LocalDateTime  | @DatetimeRange          | @DatetimeRange(max = "2000-01-01T00:00:00Z") |
  | ISO-8601       | LocalDate      | @DateRange              | @DateRange(max = "2000-01-01")               |
  | ISO-8601       | LocalTime      |                         |                                              |
  | ISO-8601       | Year           | @YearRange              | @YearRange(max = "2000")                     |
  | string         | ENUM           |                         |                                              |

  Background: 
    Given the default timezone 'America/Puerto_Rico'

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/constrained/method-parameter<path>?<param>=<value> endpoint (RequestParamConstrainedRestApi.class)
    this endpoint receives the request parameter in a method parameter, both having the same name
    and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/request-parameter/constrained/method-parameter'<path>
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | param                 | value                  | message                                                                                    |
      | 'GET'    | '/paramString'         | 'paramString'         | 'ab'                   | 'paramString: size must be between 0 and 1'                                                |
      | 'HEAD'   | '/paramString'         | 'paramString'         | 'ab'                   |                                                                                            |
      | 'POST'   | '/paramString'         | 'paramString'         | 'ab'                   | 'paramString: size must be between 0 and 1'                                                |
      | 'PUT'    | '/paramString'         | 'paramString'         | 'ab'                   | 'paramString: size must be between 0 and 1'                                                |
      | 'PATCH'  | '/paramString'         | 'paramString'         | 'ab'                   | 'paramString: size must be between 0 and 1'                                                |
      | 'DELETE' | '/paramString'         | 'paramString'         | 'ab'                   | 'paramString: size must be between 0 and 1'                                                |
      | 'GET'    | '/paramInteger'        | 'paramInteger'        | '10'                   | 'paramInteger: must be less than or equal to 1'                                            |
      | 'HEAD'   | '/paramInteger'        | 'paramInteger'        | '10'                   |                                                                                            |
      | 'POST'   | '/paramInteger'        | 'paramInteger'        | '10'                   | 'paramInteger: must be less than or equal to 1'                                            |
      | 'PUT'    | '/paramInteger'        | 'paramInteger'        | '10'                   | 'paramInteger: must be less than or equal to 1'                                            |
      | 'PATCH'  | '/paramInteger'        | 'paramInteger'        | '10'                   | 'paramInteger: must be less than or equal to 1'                                            |
      | 'DELETE' | '/paramInteger'        | 'paramInteger'        | '10'                   | 'paramInteger: must be less than or equal to 1'                                            |
      | 'GET'    | '/paramLong'           | 'paramLong'           | '10'                   | 'paramLong: must be less than or equal to 1'                                               |
      | 'HEAD'   | '/paramLong'           | 'paramLong'           | '10'                   |                                                                                            |
      | 'POST'   | '/paramLong'           | 'paramLong'           | '10'                   | 'paramLong: must be less than or equal to 1'                                               |
      | 'PUT'    | '/paramLong'           | 'paramLong'           | '10'                   | 'paramLong: must be less than or equal to 1'                                               |
      | 'PATCH'  | '/paramLong'           | 'paramLong'           | '10'                   | 'paramLong: must be less than or equal to 1'                                               |
      | 'DELETE' | '/paramLong'           | 'paramLong'           | '10'                   | 'paramLong: must be less than or equal to 1'                                               |
      | 'GET'    | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'HEAD'   | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   |                                                                                            |
      | 'POST'   | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'PUT'    | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'PATCH'  | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'DELETE' | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'GET'    | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'HEAD'   | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   |                                                                                            |
      | 'POST'   | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PUT'    | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PATCH'  | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'DELETE' | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'GET'    | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'HEAD'   | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' |                                                                                            |
      | 'POST'   | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PUT'    | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PATCH'  | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'DELETE' | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'GET'    | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'HEAD'   | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' |                                                                                            |
      | 'POST'   | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PUT'    | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PATCH'  | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'DELETE' | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'GET'    | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'HEAD'   | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           |                                                                                            |
      | 'POST'   | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PUT'    | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PATCH'  | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'DELETE' | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'GET'    | '/paramYear'           | 'paramYear'           | '2100'                 | 'paramYear: must be between -999999999 and 2000'                                           |
      | 'HEAD'   | '/paramYear'           | 'paramYear'           | '2100'                 |                                                                                            |
      | 'POST'   | '/paramYear'           | 'paramYear'           | '2100'                 | 'paramYear: must be between -999999999 and 2000'                                           |
      | 'PUT'    | '/paramYear'           | 'paramYear'           | '2100'                 | 'paramYear: must be between -999999999 and 2000'                                           |
      | 'PATCH'  | '/paramYear'           | 'paramYear'           | '2100'                 | 'paramYear: must be between -999999999 and 2000'                                           |
      | 'DELETE' | '/paramYear'           | 'paramYear'           | '2100'                 | 'paramYear: must be between -999999999 and 2000'                                           |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/constrained/named-method-parameter<path>?<param>=<value> endpoint (RequestParamConstrainedRestApi.class)
    this endpoint receives the request parameter in a method parameter, both having the diferent names, a custom request-parameter name is defined by the endpoint 
    and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/request-parameter/constrained/named-method-parameter'<path>
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | param                    | value                  | message                                                                                    |
      | 'GET'    | '/paramString'         | 'param-string'           | 'ab'                   | 'paramString: size must be between 0 and 1'                                                |
      | 'HEAD'   | '/paramString'         | 'param-string'           | 'ab'                   |                                                                                            |
      | 'POST'   | '/paramString'         | 'param-string'           | 'ab'                   | 'paramString: size must be between 0 and 1'                                                |
      | 'PUT'    | '/paramString'         | 'param-string'           | 'ab'                   | 'paramString: size must be between 0 and 1'                                                |
      | 'PATCH'  | '/paramString'         | 'param-string'           | 'ab'                   | 'paramString: size must be between 0 and 1'                                                |
      | 'DELETE' | '/paramString'         | 'param-string'           | 'ab'                   | 'paramString: size must be between 0 and 1'                                                |
      | 'GET'    | '/paramInteger'        | 'param-integer'          | '10'                   | 'paramInteger: must be less than or equal to 1'                                            |
      | 'HEAD'   | '/paramInteger'        | 'param-integer'          | '10'                   |                                                                                            |
      | 'POST'   | '/paramInteger'        | 'param-integer'          | '10'                   | 'paramInteger: must be less than or equal to 1'                                            |
      | 'PUT'    | '/paramInteger'        | 'param-integer'          | '10'                   | 'paramInteger: must be less than or equal to 1'                                            |
      | 'PATCH'  | '/paramInteger'        | 'param-integer'          | '10'                   | 'paramInteger: must be less than or equal to 1'                                            |
      | 'DELETE' | '/paramInteger'        | 'param-integer'          | '10'                   | 'paramInteger: must be less than or equal to 1'                                            |
      | 'GET'    | '/paramLong'           | 'param-long'             | '10'                   | 'paramLong: must be less than or equal to 1'                                               |
      | 'HEAD'   | '/paramLong'           | 'param-long'             | '10'                   |                                                                                            |
      | 'POST'   | '/paramLong'           | 'param-long'             | '10'                   | 'paramLong: must be less than or equal to 1'                                               |
      | 'PUT'    | '/paramLong'           | 'param-long'             | '10'                   | 'paramLong: must be less than or equal to 1'                                               |
      | 'PATCH'  | '/paramLong'           | 'param-long'             | '10'                   | 'paramLong: must be less than or equal to 1'                                               |
      | 'DELETE' | '/paramLong'           | 'param-long'             | '10'                   | 'paramLong: must be less than or equal to 1'                                               |
      | 'GET'    | '/paramBigInteger'     | 'param-big-integer'      | '10'                   | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'HEAD'   | '/paramBigInteger'     | 'param-big-integer'      | '10'                   |                                                                                            |
      | 'POST'   | '/paramBigInteger'     | 'param-big-integer'      | '10'                   | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'PUT'    | '/paramBigInteger'     | 'param-big-integer'      | '10'                   | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'PATCH'  | '/paramBigInteger'     | 'param-big-integer'      | '10'                   | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'DELETE' | '/paramBigInteger'     | 'param-big-integer'      | '10'                   | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'GET'    | '/paramBigDecimal'     | 'param-big-decimal'      | '10'                   | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'HEAD'   | '/paramBigDecimal'     | 'param-big-decimal'      | '10'                   |                                                                                            |
      | 'POST'   | '/paramBigDecimal'     | 'param-big-decimal'      | '10'                   | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PUT'    | '/paramBigDecimal'     | 'param-big-decimal'      | '10'                   | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PATCH'  | '/paramBigDecimal'     | 'param-big-decimal'      | '10'                   | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'DELETE' | '/paramBigDecimal'     | 'param-big-decimal'      | '10'                   | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'GET'    | '/paramOffsetDateTime' | 'param-offset-date-time' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'HEAD'   | '/paramOffsetDateTime' | 'param-offset-date-time' | '2100-01-01T00:00:00Z' |                                                                                            |
      | 'POST'   | '/paramOffsetDateTime' | 'param-offset-date-time' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PUT'    | '/paramOffsetDateTime' | 'param-offset-date-time' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PATCH'  | '/paramOffsetDateTime' | 'param-offset-date-time' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'DELETE' | '/paramOffsetDateTime' | 'param-offset-date-time' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'GET'    | '/paramLocalDateTime'  | 'param-local-date-time'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'HEAD'   | '/paramLocalDateTime'  | 'param-local-date-time'  | '2100-01-01T00:00:00Z' |                                                                                            |
      | 'POST'   | '/paramLocalDateTime'  | 'param-local-date-time'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PUT'    | '/paramLocalDateTime'  | 'param-local-date-time'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PATCH'  | '/paramLocalDateTime'  | 'param-local-date-time'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'DELETE' | '/paramLocalDateTime'  | 'param-local-date-time'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'GET'    | '/paramLocalDate'      | 'param-local-date'       | '2100-01-01'           | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'HEAD'   | '/paramLocalDate'      | 'param-local-date'       | '2100-01-01'           |                                                                                            |
      | 'POST'   | '/paramLocalDate'      | 'param-local-date'       | '2100-01-01'           | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PUT'    | '/paramLocalDate'      | 'param-local-date'       | '2100-01-01'           | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PATCH'  | '/paramLocalDate'      | 'param-local-date'       | '2100-01-01'           | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'DELETE' | '/paramLocalDate'      | 'param-local-date'       | '2100-01-01'           | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'GET'    | '/paramYear'           | 'param-year'             | '2100'                 | 'paramYear: must be between -999999999 and 2000'                                           |
      | 'HEAD'   | '/paramYear'           | 'param-year'             | '2100'                 |                                                                                            |
      | 'POST'   | '/paramYear'           | 'param-year'             | '2100'                 | 'paramYear: must be between -999999999 and 2000'                                           |
      | 'PUT'    | '/paramYear'           | 'param-year'             | '2100'                 | 'paramYear: must be between -999999999 and 2000'                                           |
      | 'PATCH'  | '/paramYear'           | 'param-year'             | '2100'                 | 'paramYear: must be between -999999999 and 2000'                                           |
      | 'DELETE' | '/paramYear'           | 'param-year'             | '2100'                 | 'paramYear: must be between -999999999 and 2000'                                           |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/constrained/optional-method-parameter<path>?<param>=<value> endpoint (RequestParamConstrainedRestApi.class)
    this endpoint receives the request parameter in an optional method parameter, both having the same name
    and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/request-parameter/constrained/optional-method-parameter'<path>
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | param                 | value                  | message                                                                                    |
      | 'GET'    | '/paramString'         | 'paramString'         | 'ab'                   | 'paramString: size must be between 0 and 1'                                                |
      | 'HEAD'   | '/paramString'         | 'paramString'         | 'ab'                   |                                                                                            |
      | 'POST'   | '/paramString'         | 'paramString'         | 'ab'                   | 'paramString: size must be between 0 and 1'                                                |
      | 'PUT'    | '/paramString'         | 'paramString'         | 'ab'                   | 'paramString: size must be between 0 and 1'                                                |
      | 'PATCH'  | '/paramString'         | 'paramString'         | 'ab'                   | 'paramString: size must be between 0 and 1'                                                |
      | 'DELETE' | '/paramString'         | 'paramString'         | 'ab'                   | 'paramString: size must be between 0 and 1'                                                |
      | 'GET'    | '/paramInteger'        | 'paramInteger'        | '10'                   | 'paramInteger: must be less than or equal to 1'                                            |
      | 'HEAD'   | '/paramInteger'        | 'paramInteger'        | '10'                   |                                                                                            |
      | 'POST'   | '/paramInteger'        | 'paramInteger'        | '10'                   | 'paramInteger: must be less than or equal to 1'                                            |
      | 'PUT'    | '/paramInteger'        | 'paramInteger'        | '10'                   | 'paramInteger: must be less than or equal to 1'                                            |
      | 'PATCH'  | '/paramInteger'        | 'paramInteger'        | '10'                   | 'paramInteger: must be less than or equal to 1'                                            |
      | 'DELETE' | '/paramInteger'        | 'paramInteger'        | '10'                   | 'paramInteger: must be less than or equal to 1'                                            |
      | 'GET'    | '/paramLong'           | 'paramLong'           | '10'                   | 'paramLong: must be less than or equal to 1'                                               |
      | 'HEAD'   | '/paramLong'           | 'paramLong'           | '10'                   |                                                                                            |
      | 'POST'   | '/paramLong'           | 'paramLong'           | '10'                   | 'paramLong: must be less than or equal to 1'                                               |
      | 'PUT'    | '/paramLong'           | 'paramLong'           | '10'                   | 'paramLong: must be less than or equal to 1'                                               |
      | 'PATCH'  | '/paramLong'           | 'paramLong'           | '10'                   | 'paramLong: must be less than or equal to 1'                                               |
      | 'DELETE' | '/paramLong'           | 'paramLong'           | '10'                   | 'paramLong: must be less than or equal to 1'                                               |
      | 'GET'    | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'HEAD'   | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   |                                                                                            |
      | 'POST'   | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'PUT'    | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'PATCH'  | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'DELETE' | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'GET'    | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'HEAD'   | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   |                                                                                            |
      | 'POST'   | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PUT'    | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PATCH'  | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'DELETE' | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'GET'    | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'HEAD'   | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' |                                                                                            |
      | 'POST'   | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PUT'    | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PATCH'  | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'DELETE' | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'GET'    | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'HEAD'   | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' |                                                                                            |
      | 'POST'   | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PUT'    | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PATCH'  | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'DELETE' | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'GET'    | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'HEAD'   | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           |                                                                                            |
      | 'POST'   | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PUT'    | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PATCH'  | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'DELETE' | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'GET'    | '/paramYear'           | 'paramYear'           | '2100'                 | 'paramYear: must be between -999999999 and 2000'                                           |
      | 'HEAD'   | '/paramYear'           | 'paramYear'           | '2100'                 |                                                                                            |
      | 'POST'   | '/paramYear'           | 'paramYear'           | '2100'                 | 'paramYear: must be between -999999999 and 2000'                                           |
      | 'PUT'    | '/paramYear'           | 'paramYear'           | '2100'                 | 'paramYear: must be between -999999999 and 2000'                                           |
      | 'PATCH'  | '/paramYear'           | 'paramYear'           | '2100'                 | 'paramYear: must be between -999999999 and 2000'                                           |
      | 'DELETE' | '/paramYear'           | 'paramYear'           | '2100'                 | 'paramYear: must be between -999999999 and 2000'                                           |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/constrained/named-optional-method-parameter<path>?<param>=<value> endpoint (RequestParamConstrainedRestApi.class)
    this endpoint receives the request parameter in an optional method parameter, both having the diferent names, a custom request-parameter name is defined by the endpoint 
    and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/request-parameter/constrained/named-optional-method-parameter'<path>
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | param                    | value                  | message                                                                                    |
      | 'GET'    | '/paramString'         | 'param-string'           | 'ab'                   | 'paramString: size must be between 0 and 1'                                                |
      | 'HEAD'   | '/paramString'         | 'param-string'           | 'ab'                   |                                                                                            |
      | 'POST'   | '/paramString'         | 'param-string'           | 'ab'                   | 'paramString: size must be between 0 and 1'                                                |
      | 'PUT'    | '/paramString'         | 'param-string'           | 'ab'                   | 'paramString: size must be between 0 and 1'                                                |
      | 'PATCH'  | '/paramString'         | 'param-string'           | 'ab'                   | 'paramString: size must be between 0 and 1'                                                |
      | 'DELETE' | '/paramString'         | 'param-string'           | 'ab'                   | 'paramString: size must be between 0 and 1'                                                |
      | 'GET'    | '/paramInteger'        | 'param-integer'          | '10'                   | 'paramInteger: must be less than or equal to 1'                                            |
      | 'HEAD'   | '/paramInteger'        | 'param-integer'          | '10'                   |                                                                                            |
      | 'POST'   | '/paramInteger'        | 'param-integer'          | '10'                   | 'paramInteger: must be less than or equal to 1'                                            |
      | 'PUT'    | '/paramInteger'        | 'param-integer'          | '10'                   | 'paramInteger: must be less than or equal to 1'                                            |
      | 'PATCH'  | '/paramInteger'        | 'param-integer'          | '10'                   | 'paramInteger: must be less than or equal to 1'                                            |
      | 'DELETE' | '/paramInteger'        | 'param-integer'          | '10'                   | 'paramInteger: must be less than or equal to 1'                                            |
      | 'GET'    | '/paramLong'           | 'param-long'             | '10'                   | 'paramLong: must be less than or equal to 1'                                               |
      | 'HEAD'   | '/paramLong'           | 'param-long'             | '10'                   |                                                                                            |
      | 'POST'   | '/paramLong'           | 'param-long'             | '10'                   | 'paramLong: must be less than or equal to 1'                                               |
      | 'PUT'    | '/paramLong'           | 'param-long'             | '10'                   | 'paramLong: must be less than or equal to 1'                                               |
      | 'PATCH'  | '/paramLong'           | 'param-long'             | '10'                   | 'paramLong: must be less than or equal to 1'                                               |
      | 'DELETE' | '/paramLong'           | 'param-long'             | '10'                   | 'paramLong: must be less than or equal to 1'                                               |
      | 'GET'    | '/paramBigInteger'     | 'param-big-integer'      | '10'                   | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'HEAD'   | '/paramBigInteger'     | 'param-big-integer'      | '10'                   |                                                                                            |
      | 'POST'   | '/paramBigInteger'     | 'param-big-integer'      | '10'                   | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'PUT'    | '/paramBigInteger'     | 'param-big-integer'      | '10'                   | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'PATCH'  | '/paramBigInteger'     | 'param-big-integer'      | '10'                   | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'DELETE' | '/paramBigInteger'     | 'param-big-integer'      | '10'                   | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'GET'    | '/paramBigDecimal'     | 'param-big-decimal'      | '10'                   | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'HEAD'   | '/paramBigDecimal'     | 'param-big-decimal'      | '10'                   |                                                                                            |
      | 'POST'   | '/paramBigDecimal'     | 'param-big-decimal'      | '10'                   | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PUT'    | '/paramBigDecimal'     | 'param-big-decimal'      | '10'                   | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PATCH'  | '/paramBigDecimal'     | 'param-big-decimal'      | '10'                   | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'DELETE' | '/paramBigDecimal'     | 'param-big-decimal'      | '10'                   | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'GET'    | '/paramOffsetDateTime' | 'param-offset-date-time' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'HEAD'   | '/paramOffsetDateTime' | 'param-offset-date-time' | '2100-01-01T00:00:00Z' |                                                                                            |
      | 'POST'   | '/paramOffsetDateTime' | 'param-offset-date-time' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PUT'    | '/paramOffsetDateTime' | 'param-offset-date-time' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PATCH'  | '/paramOffsetDateTime' | 'param-offset-date-time' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'DELETE' | '/paramOffsetDateTime' | 'param-offset-date-time' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'GET'    | '/paramLocalDateTime'  | 'param-local-date-time'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'HEAD'   | '/paramLocalDateTime'  | 'param-local-date-time'  | '2100-01-01T00:00:00Z' |                                                                                            |
      | 'POST'   | '/paramLocalDateTime'  | 'param-local-date-time'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PUT'    | '/paramLocalDateTime'  | 'param-local-date-time'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PATCH'  | '/paramLocalDateTime'  | 'param-local-date-time'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'DELETE' | '/paramLocalDateTime'  | 'param-local-date-time'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'GET'    | '/paramLocalDate'      | 'param-local-date'       | '2100-01-01'           | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'HEAD'   | '/paramLocalDate'      | 'param-local-date'       | '2100-01-01'           |                                                                                            |
      | 'POST'   | '/paramLocalDate'      | 'param-local-date'       | '2100-01-01'           | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PUT'    | '/paramLocalDate'      | 'param-local-date'       | '2100-01-01'           | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PATCH'  | '/paramLocalDate'      | 'param-local-date'       | '2100-01-01'           | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'DELETE' | '/paramLocalDate'      | 'param-local-date'       | '2100-01-01'           | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'GET'    | '/paramYear'           | 'param-year'             | '2100'                 | 'paramYear: must be between -999999999 and 2000'                                           |
      | 'HEAD'   | '/paramYear'           | 'param-year'             | '2100'                 |                                                                                            |
      | 'POST'   | '/paramYear'           | 'param-year'             | '2100'                 | 'paramYear: must be between -999999999 and 2000'                                           |
      | 'PUT'    | '/paramYear'           | 'param-year'             | '2100'                 | 'paramYear: must be between -999999999 and 2000'                                           |
      | 'PATCH'  | '/paramYear'           | 'param-year'             | '2100'                 | 'paramYear: must be between -999999999 and 2000'                                           |
      | 'DELETE' | '/paramYear'           | 'param-year'             | '2100'                 | 'paramYear: must be between -999999999 and 2000'                                           |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/constrained/dto-attribute<path>?<param>=<value> endpoint (RequestParamConstrainedRestApi.class)
    this endpoint receives the request parameter in a dto attribute, both having the same name
    and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/request-parameter/constrained/dto-attribute'<path>
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | param                 | value                  | message                                                                                    |
      | 'GET'    | '/paramString'         | 'paramString'         | 'ab'                   | 'paramString: size must be between 0 and 1'                                                |
      | 'HEAD'   | '/paramString'         | 'paramString'         | 'ab'                   |                                                                                            |
      | 'POST'   | '/paramString'         | 'paramString'         | 'ab'                   | 'paramString: size must be between 0 and 1'                                                |
      | 'PUT'    | '/paramString'         | 'paramString'         | 'ab'                   | 'paramString: size must be between 0 and 1'                                                |
      | 'PATCH'  | '/paramString'         | 'paramString'         | 'ab'                   | 'paramString: size must be between 0 and 1'                                                |
      | 'DELETE' | '/paramString'         | 'paramString'         | 'ab'                   | 'paramString: size must be between 0 and 1'                                                |
      | 'GET'    | '/paramInteger'        | 'paramInteger'        | '10'                   | 'paramInteger: must be less than or equal to 1'                                            |
      | 'HEAD'   | '/paramInteger'        | 'paramInteger'        | '10'                   |                                                                                            |
      | 'POST'   | '/paramInteger'        | 'paramInteger'        | '10'                   | 'paramInteger: must be less than or equal to 1'                                            |
      | 'PUT'    | '/paramInteger'        | 'paramInteger'        | '10'                   | 'paramInteger: must be less than or equal to 1'                                            |
      | 'PATCH'  | '/paramInteger'        | 'paramInteger'        | '10'                   | 'paramInteger: must be less than or equal to 1'                                            |
      | 'DELETE' | '/paramInteger'        | 'paramInteger'        | '10'                   | 'paramInteger: must be less than or equal to 1'                                            |
      | 'GET'    | '/paramLong'           | 'paramLong'           | '10'                   | 'paramLong: must be less than or equal to 1'                                               |
      | 'HEAD'   | '/paramLong'           | 'paramLong'           | '10'                   |                                                                                            |
      | 'POST'   | '/paramLong'           | 'paramLong'           | '10'                   | 'paramLong: must be less than or equal to 1'                                               |
      | 'PUT'    | '/paramLong'           | 'paramLong'           | '10'                   | 'paramLong: must be less than or equal to 1'                                               |
      | 'PATCH'  | '/paramLong'           | 'paramLong'           | '10'                   | 'paramLong: must be less than or equal to 1'                                               |
      | 'DELETE' | '/paramLong'           | 'paramLong'           | '10'                   | 'paramLong: must be less than or equal to 1'                                               |
      | 'GET'    | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'HEAD'   | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   |                                                                                            |
      | 'POST'   | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'PUT'    | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'PATCH'  | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'DELETE' | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'GET'    | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'HEAD'   | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   |                                                                                            |
      | 'POST'   | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PUT'    | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PATCH'  | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'DELETE' | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'GET'    | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'HEAD'   | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' |                                                                                            |
      | 'POST'   | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PUT'    | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PATCH'  | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'DELETE' | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'GET'    | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'HEAD'   | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' |                                                                                            |
      | 'POST'   | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PUT'    | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PATCH'  | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'DELETE' | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'GET'    | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'HEAD'   | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           |                                                                                            |
      | 'POST'   | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PUT'    | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PATCH'  | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'DELETE' | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'GET'    | '/paramYear'           | 'paramYear'           | '2100'                 | 'paramYear: must be between -999999999 and 2000'                                           |
      | 'HEAD'   | '/paramYear'           | 'paramYear'           | '2100'                 |                                                                                            |
      | 'POST'   | '/paramYear'           | 'paramYear'           | '2100'                 | 'paramYear: must be between -999999999 and 2000'                                           |
      | 'PUT'    | '/paramYear'           | 'paramYear'           | '2100'                 | 'paramYear: must be between -999999999 and 2000'                                           |
      | 'PATCH'  | '/paramYear'           | 'paramYear'           | '2100'                 | 'paramYear: must be between -999999999 and 2000'                                           |
      | 'DELETE' | '/paramYear'           | 'paramYear'           | '2100'                 | 'paramYear: must be between -999999999 and 2000'                                           |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/constrained/optional-dto-attribute<path>?<param>=<value> endpoint (RequestParamConstrainedRestApi.class)
    this endpoint receives the request parameter in an optional dto attribute, both having the same name
    and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/request-parameter/constrained/optional-dto-attribute'<path>
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | param                 | value                  | message                                                                                    |
      | 'GET'    | '/paramString'         | 'paramString'         | 'ab'                   | 'paramString: size must be between 0 and 1'                                                |
      | 'HEAD'   | '/paramString'         | 'paramString'         | 'ab'                   |                                                                                            |
      | 'POST'   | '/paramString'         | 'paramString'         | 'ab'                   | 'paramString: size must be between 0 and 1'                                                |
      | 'PUT'    | '/paramString'         | 'paramString'         | 'ab'                   | 'paramString: size must be between 0 and 1'                                                |
      | 'PATCH'  | '/paramString'         | 'paramString'         | 'ab'                   | 'paramString: size must be between 0 and 1'                                                |
      | 'DELETE' | '/paramString'         | 'paramString'         | 'ab'                   | 'paramString: size must be between 0 and 1'                                                |
      | 'GET'    | '/paramInteger'        | 'paramInteger'        | '10'                   | 'paramInteger: must be less than or equal to 1'                                            |
      | 'HEAD'   | '/paramInteger'        | 'paramInteger'        | '10'                   |                                                                                            |
      | 'POST'   | '/paramInteger'        | 'paramInteger'        | '10'                   | 'paramInteger: must be less than or equal to 1'                                            |
      | 'PUT'    | '/paramInteger'        | 'paramInteger'        | '10'                   | 'paramInteger: must be less than or equal to 1'                                            |
      | 'PATCH'  | '/paramInteger'        | 'paramInteger'        | '10'                   | 'paramInteger: must be less than or equal to 1'                                            |
      | 'DELETE' | '/paramInteger'        | 'paramInteger'        | '10'                   | 'paramInteger: must be less than or equal to 1'                                            |
      | 'GET'    | '/paramLong'           | 'paramLong'           | '10'                   | 'paramLong: must be less than or equal to 1'                                               |
      | 'HEAD'   | '/paramLong'           | 'paramLong'           | '10'                   |                                                                                            |
      | 'POST'   | '/paramLong'           | 'paramLong'           | '10'                   | 'paramLong: must be less than or equal to 1'                                               |
      | 'PUT'    | '/paramLong'           | 'paramLong'           | '10'                   | 'paramLong: must be less than or equal to 1'                                               |
      | 'PATCH'  | '/paramLong'           | 'paramLong'           | '10'                   | 'paramLong: must be less than or equal to 1'                                               |
      | 'DELETE' | '/paramLong'           | 'paramLong'           | '10'                   | 'paramLong: must be less than or equal to 1'                                               |
      | 'GET'    | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'HEAD'   | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   |                                                                                            |
      | 'POST'   | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'PUT'    | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'PATCH'  | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'DELETE' | '/paramBigInteger'     | 'paramBigInteger'     | '10'                   | 'paramBigInteger: must be less than or equal to 1'                                         |
      | 'GET'    | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'HEAD'   | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   |                                                                                            |
      | 'POST'   | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PUT'    | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PATCH'  | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'DELETE' | '/paramBigDecimal'     | 'paramBigDecimal'     | '10'                   | 'paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'GET'    | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'HEAD'   | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' |                                                                                            |
      | 'POST'   | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PUT'    | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PATCH'  | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'DELETE' | '/paramOffsetDateTime' | 'paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'GET'    | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'HEAD'   | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' |                                                                                            |
      | 'POST'   | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PUT'    | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PATCH'  | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'DELETE' | '/paramLocalDateTime'  | 'paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'GET'    | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'HEAD'   | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           |                                                                                            |
      | 'POST'   | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PUT'    | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PATCH'  | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'DELETE' | '/paramLocalDate'      | 'paramLocalDate'      | '2100-01-01'           | 'paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'GET'    | '/paramYear'           | 'paramYear'           | '2100'                 | 'paramYear: must be between -999999999 and 2000'                                           |
      | 'HEAD'   | '/paramYear'           | 'paramYear'           | '2100'                 |                                                                                            |
      | 'POST'   | '/paramYear'           | 'paramYear'           | '2100'                 | 'paramYear: must be between -999999999 and 2000'                                           |
      | 'PUT'    | '/paramYear'           | 'paramYear'           | '2100'                 | 'paramYear: must be between -999999999 and 2000'                                           |
      | 'PATCH'  | '/paramYear'           | 'paramYear'           | '2100'                 | 'paramYear: must be between -999999999 and 2000'                                           |
      | 'DELETE' | '/paramYear'           | 'paramYear'           | '2100'                 | 'paramYear: must be between -999999999 and 2000'                                           |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/constrained/nested-dto-attribute<path>?<param>=<value> endpoint (RequestParamConstrainedRestApi.class)
    this endpoint receives the request parameter in a nested dto attribute, both having the same name
    and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/request-parameter/constrained/nested-dto-attribute'<path>
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | param                        | value                  | message                                                                                           |
      | 'GET'    | '/paramString'         | 'nested.paramString'         | 'ab'                   | 'nested.paramString: size must be between 0 and 1'                                                |
      | 'HEAD'   | '/paramString'         | 'nested.paramString'         | 'ab'                   |                                                                                                   |
      | 'POST'   | '/paramString'         | 'nested.paramString'         | 'ab'                   | 'nested.paramString: size must be between 0 and 1'                                                |
      | 'PUT'    | '/paramString'         | 'nested.paramString'         | 'ab'                   | 'nested.paramString: size must be between 0 and 1'                                                |
      | 'PATCH'  | '/paramString'         | 'nested.paramString'         | 'ab'                   | 'nested.paramString: size must be between 0 and 1'                                                |
      | 'DELETE' | '/paramString'         | 'nested.paramString'         | 'ab'                   | 'nested.paramString: size must be between 0 and 1'                                                |
      | 'GET'    | '/paramInteger'        | 'nested.paramInteger'        | '10'                   | 'nested.paramInteger: must be less than or equal to 1'                                            |
      | 'HEAD'   | '/paramInteger'        | 'nested.paramInteger'        | '10'                   |                                                                                                   |
      | 'POST'   | '/paramInteger'        | 'nested.paramInteger'        | '10'                   | 'nested.paramInteger: must be less than or equal to 1'                                            |
      | 'PUT'    | '/paramInteger'        | 'nested.paramInteger'        | '10'                   | 'nested.paramInteger: must be less than or equal to 1'                                            |
      | 'PATCH'  | '/paramInteger'        | 'nested.paramInteger'        | '10'                   | 'nested.paramInteger: must be less than or equal to 1'                                            |
      | 'DELETE' | '/paramInteger'        | 'nested.paramInteger'        | '10'                   | 'nested.paramInteger: must be less than or equal to 1'                                            |
      | 'GET'    | '/paramLong'           | 'nested.paramLong'           | '10'                   | 'nested.paramLong: must be less than or equal to 1'                                               |
      | 'HEAD'   | '/paramLong'           | 'nested.paramLong'           | '10'                   |                                                                                                   |
      | 'POST'   | '/paramLong'           | 'nested.paramLong'           | '10'                   | 'nested.paramLong: must be less than or equal to 1'                                               |
      | 'PUT'    | '/paramLong'           | 'nested.paramLong'           | '10'                   | 'nested.paramLong: must be less than or equal to 1'                                               |
      | 'PATCH'  | '/paramLong'           | 'nested.paramLong'           | '10'                   | 'nested.paramLong: must be less than or equal to 1'                                               |
      | 'DELETE' | '/paramLong'           | 'nested.paramLong'           | '10'                   | 'nested.paramLong: must be less than or equal to 1'                                               |
      | 'GET'    | '/paramBigInteger'     | 'nested.paramBigInteger'     | '10'                   | 'nested.paramBigInteger: must be less than or equal to 1'                                         |
      | 'HEAD'   | '/paramBigInteger'     | 'nested.paramBigInteger'     | '10'                   |                                                                                                   |
      | 'POST'   | '/paramBigInteger'     | 'nested.paramBigInteger'     | '10'                   | 'nested.paramBigInteger: must be less than or equal to 1'                                         |
      | 'PUT'    | '/paramBigInteger'     | 'nested.paramBigInteger'     | '10'                   | 'nested.paramBigInteger: must be less than or equal to 1'                                         |
      | 'PATCH'  | '/paramBigInteger'     | 'nested.paramBigInteger'     | '10'                   | 'nested.paramBigInteger: must be less than or equal to 1'                                         |
      | 'DELETE' | '/paramBigInteger'     | 'nested.paramBigInteger'     | '10'                   | 'nested.paramBigInteger: must be less than or equal to 1'                                         |
      | 'GET'    | '/paramBigDecimal'     | 'nested.paramBigDecimal'     | '10'                   | 'nested.paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'HEAD'   | '/paramBigDecimal'     | 'nested.paramBigDecimal'     | '10'                   |                                                                                                   |
      | 'POST'   | '/paramBigDecimal'     | 'nested.paramBigDecimal'     | '10'                   | 'nested.paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PUT'    | '/paramBigDecimal'     | 'nested.paramBigDecimal'     | '10'                   | 'nested.paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PATCH'  | '/paramBigDecimal'     | 'nested.paramBigDecimal'     | '10'                   | 'nested.paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'DELETE' | '/paramBigDecimal'     | 'nested.paramBigDecimal'     | '10'                   | 'nested.paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'GET'    | '/paramOffsetDateTime' | 'nested.paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'nested.paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'HEAD'   | '/paramOffsetDateTime' | 'nested.paramOffsetDateTime' | '2100-01-01T00:00:00Z' |                                                                                                   |
      | 'POST'   | '/paramOffsetDateTime' | 'nested.paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'nested.paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PUT'    | '/paramOffsetDateTime' | 'nested.paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'nested.paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PATCH'  | '/paramOffsetDateTime' | 'nested.paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'nested.paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'DELETE' | '/paramOffsetDateTime' | 'nested.paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'nested.paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'GET'    | '/paramLocalDateTime'  | 'nested.paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'nested.paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'HEAD'   | '/paramLocalDateTime'  | 'nested.paramLocalDateTime'  | '2100-01-01T00:00:00Z' |                                                                                                   |
      | 'POST'   | '/paramLocalDateTime'  | 'nested.paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'nested.paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PUT'    | '/paramLocalDateTime'  | 'nested.paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'nested.paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PATCH'  | '/paramLocalDateTime'  | 'nested.paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'nested.paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'DELETE' | '/paramLocalDateTime'  | 'nested.paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'nested.paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'GET'    | '/paramLocalDate'      | 'nested.paramLocalDate'      | '2100-01-01'           | 'nested.paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'HEAD'   | '/paramLocalDate'      | 'nested.paramLocalDate'      | '2100-01-01'           |                                                                                                   |
      | 'POST'   | '/paramLocalDate'      | 'nested.paramLocalDate'      | '2100-01-01'           | 'nested.paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PUT'    | '/paramLocalDate'      | 'nested.paramLocalDate'      | '2100-01-01'           | 'nested.paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PATCH'  | '/paramLocalDate'      | 'nested.paramLocalDate'      | '2100-01-01'           | 'nested.paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'DELETE' | '/paramLocalDate'      | 'nested.paramLocalDate'      | '2100-01-01'           | 'nested.paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'GET'    | '/paramYear'           | 'nested.paramYear'           | '2100'                 | 'nested.paramYear: must be between -999999999 and 2000'                                           |
      | 'HEAD'   | '/paramYear'           | 'nested.paramYear'           | '2100'                 |                                                                                                   |
      | 'POST'   | '/paramYear'           | 'nested.paramYear'           | '2100'                 | 'nested.paramYear: must be between -999999999 and 2000'                                           |
      | 'PUT'    | '/paramYear'           | 'nested.paramYear'           | '2100'                 | 'nested.paramYear: must be between -999999999 and 2000'                                           |
      | 'PATCH'  | '/paramYear'           | 'nested.paramYear'           | '2100'                 | 'nested.paramYear: must be between -999999999 and 2000'                                           |
      | 'DELETE' | '/paramYear'           | 'nested.paramYear'           | '2100'                 | 'nested.paramYear: must be between -999999999 and 2000'                                           |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/constrained/nested-optional-dto-attribute<path>?<param>=<value> endpoint (RequestParamConstrainedRestApi.class)
    this endpoint receives the request parameter in a nested optional dto attribute, both having the same name
    and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/request-parameter/constrained/nested-optional-dto-attribute'<path>
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | param                        | value                  | message                                                                                           |
      | 'GET'    | '/paramString'         | 'nested.paramString'         | 'ab'                   | 'nested.paramString: size must be between 0 and 1'                                                |
      | 'HEAD'   | '/paramString'         | 'nested.paramString'         | 'ab'                   |                                                                                                   |
      | 'POST'   | '/paramString'         | 'nested.paramString'         | 'ab'                   | 'nested.paramString: size must be between 0 and 1'                                                |
      | 'PUT'    | '/paramString'         | 'nested.paramString'         | 'ab'                   | 'nested.paramString: size must be between 0 and 1'                                                |
      | 'PATCH'  | '/paramString'         | 'nested.paramString'         | 'ab'                   | 'nested.paramString: size must be between 0 and 1'                                                |
      | 'DELETE' | '/paramString'         | 'nested.paramString'         | 'ab'                   | 'nested.paramString: size must be between 0 and 1'                                                |
      | 'GET'    | '/paramInteger'        | 'nested.paramInteger'        | '10'                   | 'nested.paramInteger: must be less than or equal to 1'                                            |
      | 'HEAD'   | '/paramInteger'        | 'nested.paramInteger'        | '10'                   |                                                                                                   |
      | 'POST'   | '/paramInteger'        | 'nested.paramInteger'        | '10'                   | 'nested.paramInteger: must be less than or equal to 1'                                            |
      | 'PUT'    | '/paramInteger'        | 'nested.paramInteger'        | '10'                   | 'nested.paramInteger: must be less than or equal to 1'                                            |
      | 'PATCH'  | '/paramInteger'        | 'nested.paramInteger'        | '10'                   | 'nested.paramInteger: must be less than or equal to 1'                                            |
      | 'DELETE' | '/paramInteger'        | 'nested.paramInteger'        | '10'                   | 'nested.paramInteger: must be less than or equal to 1'                                            |
      | 'GET'    | '/paramLong'           | 'nested.paramLong'           | '10'                   | 'nested.paramLong: must be less than or equal to 1'                                               |
      | 'HEAD'   | '/paramLong'           | 'nested.paramLong'           | '10'                   |                                                                                                   |
      | 'POST'   | '/paramLong'           | 'nested.paramLong'           | '10'                   | 'nested.paramLong: must be less than or equal to 1'                                               |
      | 'PUT'    | '/paramLong'           | 'nested.paramLong'           | '10'                   | 'nested.paramLong: must be less than or equal to 1'                                               |
      | 'PATCH'  | '/paramLong'           | 'nested.paramLong'           | '10'                   | 'nested.paramLong: must be less than or equal to 1'                                               |
      | 'DELETE' | '/paramLong'           | 'nested.paramLong'           | '10'                   | 'nested.paramLong: must be less than or equal to 1'                                               |
      | 'GET'    | '/paramBigInteger'     | 'nested.paramBigInteger'     | '10'                   | 'nested.paramBigInteger: must be less than or equal to 1'                                         |
      | 'HEAD'   | '/paramBigInteger'     | 'nested.paramBigInteger'     | '10'                   |                                                                                                   |
      | 'POST'   | '/paramBigInteger'     | 'nested.paramBigInteger'     | '10'                   | 'nested.paramBigInteger: must be less than or equal to 1'                                         |
      | 'PUT'    | '/paramBigInteger'     | 'nested.paramBigInteger'     | '10'                   | 'nested.paramBigInteger: must be less than or equal to 1'                                         |
      | 'PATCH'  | '/paramBigInteger'     | 'nested.paramBigInteger'     | '10'                   | 'nested.paramBigInteger: must be less than or equal to 1'                                         |
      | 'DELETE' | '/paramBigInteger'     | 'nested.paramBigInteger'     | '10'                   | 'nested.paramBigInteger: must be less than or equal to 1'                                         |
      | 'GET'    | '/paramBigDecimal'     | 'nested.paramBigDecimal'     | '10'                   | 'nested.paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'HEAD'   | '/paramBigDecimal'     | 'nested.paramBigDecimal'     | '10'                   |                                                                                                   |
      | 'POST'   | '/paramBigDecimal'     | 'nested.paramBigDecimal'     | '10'                   | 'nested.paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PUT'    | '/paramBigDecimal'     | 'nested.paramBigDecimal'     | '10'                   | 'nested.paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PATCH'  | '/paramBigDecimal'     | 'nested.paramBigDecimal'     | '10'                   | 'nested.paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'DELETE' | '/paramBigDecimal'     | 'nested.paramBigDecimal'     | '10'                   | 'nested.paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'GET'    | '/paramOffsetDateTime' | 'nested.paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'nested.paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'HEAD'   | '/paramOffsetDateTime' | 'nested.paramOffsetDateTime' | '2100-01-01T00:00:00Z' |                                                                                                   |
      | 'POST'   | '/paramOffsetDateTime' | 'nested.paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'nested.paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PUT'    | '/paramOffsetDateTime' | 'nested.paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'nested.paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PATCH'  | '/paramOffsetDateTime' | 'nested.paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'nested.paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'DELETE' | '/paramOffsetDateTime' | 'nested.paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'nested.paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'GET'    | '/paramLocalDateTime'  | 'nested.paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'nested.paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'HEAD'   | '/paramLocalDateTime'  | 'nested.paramLocalDateTime'  | '2100-01-01T00:00:00Z' |                                                                                                   |
      | 'POST'   | '/paramLocalDateTime'  | 'nested.paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'nested.paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PUT'    | '/paramLocalDateTime'  | 'nested.paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'nested.paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PATCH'  | '/paramLocalDateTime'  | 'nested.paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'nested.paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'DELETE' | '/paramLocalDateTime'  | 'nested.paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'nested.paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'GET'    | '/paramLocalDate'      | 'nested.paramLocalDate'      | '2100-01-01'           | 'nested.paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'HEAD'   | '/paramLocalDate'      | 'nested.paramLocalDate'      | '2100-01-01'           |                                                                                                   |
      | 'POST'   | '/paramLocalDate'      | 'nested.paramLocalDate'      | '2100-01-01'           | 'nested.paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PUT'    | '/paramLocalDate'      | 'nested.paramLocalDate'      | '2100-01-01'           | 'nested.paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PATCH'  | '/paramLocalDate'      | 'nested.paramLocalDate'      | '2100-01-01'           | 'nested.paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'DELETE' | '/paramLocalDate'      | 'nested.paramLocalDate'      | '2100-01-01'           | 'nested.paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'GET'    | '/paramYear'           | 'nested.paramYear'           | '2100'                 | 'nested.paramYear: must be between -999999999 and 2000'                                           |
      | 'HEAD'   | '/paramYear'           | 'nested.paramYear'           | '2100'                 |                                                                                                   |
      | 'POST'   | '/paramYear'           | 'nested.paramYear'           | '2100'                 | 'nested.paramYear: must be between -999999999 and 2000'                                           |
      | 'PUT'    | '/paramYear'           | 'nested.paramYear'           | '2100'                 | 'nested.paramYear: must be between -999999999 and 2000'                                           |
      | 'PATCH'  | '/paramYear'           | 'nested.paramYear'           | '2100'                 | 'nested.paramYear: must be between -999999999 and 2000'                                           |
      | 'DELETE' | '/paramYear'           | 'nested.paramYear'           | '2100'                 | 'nested.paramYear: must be between -999999999 and 2000'                                           |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/constrained/nested-dto-attribute<path>?<param>=<value> endpoint (RequestParamConstrainedRestApi.class)
    this endpoint receives the request parameter in a nested dto attribute from an array, both having the same name
    and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/request-parameter/constrained/nested-dto-attribute'<path>
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | param                          | value                  | message                                                                                             |
      | 'GET'    | '/paramString'         | 'array[0].paramString'         | 'ab'                   | 'array[0].paramString: size must be between 0 and 1'                                                |
      | 'HEAD'   | '/paramString'         | 'array[0].paramString'         | 'ab'                   |                                                                                                     |
      | 'POST'   | '/paramString'         | 'array[0].paramString'         | 'ab'                   | 'array[0].paramString: size must be between 0 and 1'                                                |
      | 'PUT'    | '/paramString'         | 'array[0].paramString'         | 'ab'                   | 'array[0].paramString: size must be between 0 and 1'                                                |
      | 'PATCH'  | '/paramString'         | 'array[0].paramString'         | 'ab'                   | 'array[0].paramString: size must be between 0 and 1'                                                |
      | 'DELETE' | '/paramString'         | 'array[0].paramString'         | 'ab'                   | 'array[0].paramString: size must be between 0 and 1'                                                |
      | 'GET'    | '/paramInteger'        | 'array[0].paramInteger'        | '10'                   | 'array[0].paramInteger: must be less than or equal to 1'                                            |
      | 'HEAD'   | '/paramInteger'        | 'array[0].paramInteger'        | '10'                   |                                                                                                     |
      | 'POST'   | '/paramInteger'        | 'array[0].paramInteger'        | '10'                   | 'array[0].paramInteger: must be less than or equal to 1'                                            |
      | 'PUT'    | '/paramInteger'        | 'array[0].paramInteger'        | '10'                   | 'array[0].paramInteger: must be less than or equal to 1'                                            |
      | 'PATCH'  | '/paramInteger'        | 'array[0].paramInteger'        | '10'                   | 'array[0].paramInteger: must be less than or equal to 1'                                            |
      | 'DELETE' | '/paramInteger'        | 'array[0].paramInteger'        | '10'                   | 'array[0].paramInteger: must be less than or equal to 1'                                            |
      | 'GET'    | '/paramLong'           | 'array[0].paramLong'           | '10'                   | 'array[0].paramLong: must be less than or equal to 1'                                               |
      | 'HEAD'   | '/paramLong'           | 'array[0].paramLong'           | '10'                   |                                                                                                     |
      | 'POST'   | '/paramLong'           | 'array[0].paramLong'           | '10'                   | 'array[0].paramLong: must be less than or equal to 1'                                               |
      | 'PUT'    | '/paramLong'           | 'array[0].paramLong'           | '10'                   | 'array[0].paramLong: must be less than or equal to 1'                                               |
      | 'PATCH'  | '/paramLong'           | 'array[0].paramLong'           | '10'                   | 'array[0].paramLong: must be less than or equal to 1'                                               |
      | 'DELETE' | '/paramLong'           | 'array[0].paramLong'           | '10'                   | 'array[0].paramLong: must be less than or equal to 1'                                               |
      | 'GET'    | '/paramBigInteger'     | 'array[0].paramBigInteger'     | '10'                   | 'array[0].paramBigInteger: must be less than or equal to 1'                                         |
      | 'HEAD'   | '/paramBigInteger'     | 'array[0].paramBigInteger'     | '10'                   |                                                                                                     |
      | 'POST'   | '/paramBigInteger'     | 'array[0].paramBigInteger'     | '10'                   | 'array[0].paramBigInteger: must be less than or equal to 1'                                         |
      | 'PUT'    | '/paramBigInteger'     | 'array[0].paramBigInteger'     | '10'                   | 'array[0].paramBigInteger: must be less than or equal to 1'                                         |
      | 'PATCH'  | '/paramBigInteger'     | 'array[0].paramBigInteger'     | '10'                   | 'array[0].paramBigInteger: must be less than or equal to 1'                                         |
      | 'DELETE' | '/paramBigInteger'     | 'array[0].paramBigInteger'     | '10'                   | 'array[0].paramBigInteger: must be less than or equal to 1'                                         |
      | 'GET'    | '/paramBigDecimal'     | 'array[0].paramBigDecimal'     | '10'                   | 'array[0].paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'HEAD'   | '/paramBigDecimal'     | 'array[0].paramBigDecimal'     | '10'                   |                                                                                                     |
      | 'POST'   | '/paramBigDecimal'     | 'array[0].paramBigDecimal'     | '10'                   | 'array[0].paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PUT'    | '/paramBigDecimal'     | 'array[0].paramBigDecimal'     | '10'                   | 'array[0].paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PATCH'  | '/paramBigDecimal'     | 'array[0].paramBigDecimal'     | '10'                   | 'array[0].paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'DELETE' | '/paramBigDecimal'     | 'array[0].paramBigDecimal'     | '10'                   | 'array[0].paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'GET'    | '/paramOffsetDateTime' | 'array[0].paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'array[0].paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'HEAD'   | '/paramOffsetDateTime' | 'array[0].paramOffsetDateTime' | '2100-01-01T00:00:00Z' |                                                                                                     |
      | 'POST'   | '/paramOffsetDateTime' | 'array[0].paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'array[0].paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PUT'    | '/paramOffsetDateTime' | 'array[0].paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'array[0].paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PATCH'  | '/paramOffsetDateTime' | 'array[0].paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'array[0].paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'DELETE' | '/paramOffsetDateTime' | 'array[0].paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'array[0].paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'GET'    | '/paramLocalDateTime'  | 'array[0].paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'array[0].paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'HEAD'   | '/paramLocalDateTime'  | 'array[0].paramLocalDateTime'  | '2100-01-01T00:00:00Z' |                                                                                                     |
      | 'POST'   | '/paramLocalDateTime'  | 'array[0].paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'array[0].paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PUT'    | '/paramLocalDateTime'  | 'array[0].paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'array[0].paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PATCH'  | '/paramLocalDateTime'  | 'array[0].paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'array[0].paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'DELETE' | '/paramLocalDateTime'  | 'array[0].paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'array[0].paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'GET'    | '/paramLocalDate'      | 'array[0].paramLocalDate'      | '2100-01-01'           | 'array[0].paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'HEAD'   | '/paramLocalDate'      | 'array[0].paramLocalDate'      | '2100-01-01'           |                                                                                                     |
      | 'POST'   | '/paramLocalDate'      | 'array[0].paramLocalDate'      | '2100-01-01'           | 'array[0].paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PUT'    | '/paramLocalDate'      | 'array[0].paramLocalDate'      | '2100-01-01'           | 'array[0].paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PATCH'  | '/paramLocalDate'      | 'array[0].paramLocalDate'      | '2100-01-01'           | 'array[0].paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'DELETE' | '/paramLocalDate'      | 'array[0].paramLocalDate'      | '2100-01-01'           | 'array[0].paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'GET'    | '/paramYear'           | 'array[0].paramYear'           | '2100'                 | 'array[0].paramYear: must be between -999999999 and 2000'                                           |
      | 'HEAD'   | '/paramYear'           | 'array[0].paramYear'           | '2100'                 |                                                                                                     |
      | 'POST'   | '/paramYear'           | 'array[0].paramYear'           | '2100'                 | 'array[0].paramYear: must be between -999999999 and 2000'                                           |
      | 'PUT'    | '/paramYear'           | 'array[0].paramYear'           | '2100'                 | 'array[0].paramYear: must be between -999999999 and 2000'                                           |
      | 'PATCH'  | '/paramYear'           | 'array[0].paramYear'           | '2100'                 | 'array[0].paramYear: must be between -999999999 and 2000'                                           |
      | 'DELETE' | '/paramYear'           | 'array[0].paramYear'           | '2100'                 | 'array[0].paramYear: must be between -999999999 and 2000'                                           |

  Scenario Outline: The <method> call is made to the /api/v1/request-parameter/constrained/nested-optional-dto-attribute<path>?<param>=<value> endpoint (RequestParamConstrainedRestApi.class)
    this endpoint receives the request parameter in a nested optional dto attribute from an array, both having the same name
    and the service correctly maps the sent value with the declared data type, but service responding with an error message since data does not accomplish with some constraint.

    Given the endpoint '/api/v1/request-parameter/constrained/nested-optional-dto-attribute'<path>
    And the request with the <method> method
    And the request parameter <param> has the value <value>
    When I execute the request
    Then I get the status code 400
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value <message>

    Examples: 
      | method   | path                   | param                          | value                  | message                                                                                             |
      | 'GET'    | '/paramString'         | 'array[0].paramString'         | 'ab'                   | 'array[0].paramString: size must be between 0 and 1'                                                |
      | 'HEAD'   | '/paramString'         | 'array[0].paramString'         | 'ab'                   |                                                                                                     |
      | 'POST'   | '/paramString'         | 'array[0].paramString'         | 'ab'                   | 'array[0].paramString: size must be between 0 and 1'                                                |
      | 'PUT'    | '/paramString'         | 'array[0].paramString'         | 'ab'                   | 'array[0].paramString: size must be between 0 and 1'                                                |
      | 'PATCH'  | '/paramString'         | 'array[0].paramString'         | 'ab'                   | 'array[0].paramString: size must be between 0 and 1'                                                |
      | 'DELETE' | '/paramString'         | 'array[0].paramString'         | 'ab'                   | 'array[0].paramString: size must be between 0 and 1'                                                |
      | 'GET'    | '/paramInteger'        | 'array[0].paramInteger'        | '10'                   | 'array[0].paramInteger: must be less than or equal to 1'                                            |
      | 'HEAD'   | '/paramInteger'        | 'array[0].paramInteger'        | '10'                   |                                                                                                     |
      | 'POST'   | '/paramInteger'        | 'array[0].paramInteger'        | '10'                   | 'array[0].paramInteger: must be less than or equal to 1'                                            |
      | 'PUT'    | '/paramInteger'        | 'array[0].paramInteger'        | '10'                   | 'array[0].paramInteger: must be less than or equal to 1'                                            |
      | 'PATCH'  | '/paramInteger'        | 'array[0].paramInteger'        | '10'                   | 'array[0].paramInteger: must be less than or equal to 1'                                            |
      | 'DELETE' | '/paramInteger'        | 'array[0].paramInteger'        | '10'                   | 'array[0].paramInteger: must be less than or equal to 1'                                            |
      | 'GET'    | '/paramLong'           | 'array[0].paramLong'           | '10'                   | 'array[0].paramLong: must be less than or equal to 1'                                               |
      | 'HEAD'   | '/paramLong'           | 'array[0].paramLong'           | '10'                   |                                                                                                     |
      | 'POST'   | '/paramLong'           | 'array[0].paramLong'           | '10'                   | 'array[0].paramLong: must be less than or equal to 1'                                               |
      | 'PUT'    | '/paramLong'           | 'array[0].paramLong'           | '10'                   | 'array[0].paramLong: must be less than or equal to 1'                                               |
      | 'PATCH'  | '/paramLong'           | 'array[0].paramLong'           | '10'                   | 'array[0].paramLong: must be less than or equal to 1'                                               |
      | 'DELETE' | '/paramLong'           | 'array[0].paramLong'           | '10'                   | 'array[0].paramLong: must be less than or equal to 1'                                               |
      | 'GET'    | '/paramBigInteger'     | 'array[0].paramBigInteger'     | '10'                   | 'array[0].paramBigInteger: must be less than or equal to 1'                                         |
      | 'HEAD'   | '/paramBigInteger'     | 'array[0].paramBigInteger'     | '10'                   |                                                                                                     |
      | 'POST'   | '/paramBigInteger'     | 'array[0].paramBigInteger'     | '10'                   | 'array[0].paramBigInteger: must be less than or equal to 1'                                         |
      | 'PUT'    | '/paramBigInteger'     | 'array[0].paramBigInteger'     | '10'                   | 'array[0].paramBigInteger: must be less than or equal to 1'                                         |
      | 'PATCH'  | '/paramBigInteger'     | 'array[0].paramBigInteger'     | '10'                   | 'array[0].paramBigInteger: must be less than or equal to 1'                                         |
      | 'DELETE' | '/paramBigInteger'     | 'array[0].paramBigInteger'     | '10'                   | 'array[0].paramBigInteger: must be less than or equal to 1'                                         |
      | 'GET'    | '/paramBigDecimal'     | 'array[0].paramBigDecimal'     | '10'                   | 'array[0].paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'HEAD'   | '/paramBigDecimal'     | 'array[0].paramBigDecimal'     | '10'                   |                                                                                                     |
      | 'POST'   | '/paramBigDecimal'     | 'array[0].paramBigDecimal'     | '10'                   | 'array[0].paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PUT'    | '/paramBigDecimal'     | 'array[0].paramBigDecimal'     | '10'                   | 'array[0].paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'PATCH'  | '/paramBigDecimal'     | 'array[0].paramBigDecimal'     | '10'                   | 'array[0].paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'DELETE' | '/paramBigDecimal'     | 'array[0].paramBigDecimal'     | '10'                   | 'array[0].paramBigDecimal: must be less than or equal to 9.9'                                       |
      | 'GET'    | '/paramOffsetDateTime' | 'array[0].paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'array[0].paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'HEAD'   | '/paramOffsetDateTime' | 'array[0].paramOffsetDateTime' | '2100-01-01T00:00:00Z' |                                                                                                     |
      | 'POST'   | '/paramOffsetDateTime' | 'array[0].paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'array[0].paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PUT'    | '/paramOffsetDateTime' | 'array[0].paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'array[0].paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'PATCH'  | '/paramOffsetDateTime' | 'array[0].paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'array[0].paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'DELETE' | '/paramOffsetDateTime' | 'array[0].paramOffsetDateTime' | '2100-01-01T00:00:00Z' | 'array[0].paramOffsetDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z' |
      | 'GET'    | '/paramLocalDateTime'  | 'array[0].paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'array[0].paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'HEAD'   | '/paramLocalDateTime'  | 'array[0].paramLocalDateTime'  | '2100-01-01T00:00:00Z' |                                                                                                     |
      | 'POST'   | '/paramLocalDateTime'  | 'array[0].paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'array[0].paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PUT'    | '/paramLocalDateTime'  | 'array[0].paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'array[0].paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'PATCH'  | '/paramLocalDateTime'  | 'array[0].paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'array[0].paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'DELETE' | '/paramLocalDateTime'  | 'array[0].paramLocalDateTime'  | '2100-01-01T00:00:00Z' | 'array[0].paramLocalDateTime: must be between -999999999-01-01T00:00:00Z and 2000-01-01T00:00:00Z'  |
      | 'GET'    | '/paramLocalDate'      | 'array[0].paramLocalDate'      | '2100-01-01'           | 'array[0].paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'HEAD'   | '/paramLocalDate'      | 'array[0].paramLocalDate'      | '2100-01-01'           |                                                                                                     |
      | 'POST'   | '/paramLocalDate'      | 'array[0].paramLocalDate'      | '2100-01-01'           | 'array[0].paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PUT'    | '/paramLocalDate'      | 'array[0].paramLocalDate'      | '2100-01-01'           | 'array[0].paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'PATCH'  | '/paramLocalDate'      | 'array[0].paramLocalDate'      | '2100-01-01'           | 'array[0].paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'DELETE' | '/paramLocalDate'      | 'array[0].paramLocalDate'      | '2100-01-01'           | 'array[0].paramLocalDate: must be between -999999999-01-01 and 2000-01-01'                          |
      | 'GET'    | '/paramYear'           | 'array[0].paramYear'           | '2100'                 | 'array[0].paramYear: must be between -999999999 and 2000'                                           |
      | 'HEAD'   | '/paramYear'           | 'array[0].paramYear'           | '2100'                 |                                                                                                     |
      | 'POST'   | '/paramYear'           | 'array[0].paramYear'           | '2100'                 | 'array[0].paramYear: must be between -999999999 and 2000'                                           |
      | 'PUT'    | '/paramYear'           | 'array[0].paramYear'           | '2100'                 | 'array[0].paramYear: must be between -999999999 and 2000'                                           |
      | 'PATCH'  | '/paramYear'           | 'array[0].paramYear'           | '2100'                 | 'array[0].paramYear: must be between -999999999 and 2000'                                           |
      | 'DELETE' | '/paramYear'           | 'array[0].paramYear'           | '2100'                 | 'array[0].paramYear: must be between -999999999 and 2000'                                           |
