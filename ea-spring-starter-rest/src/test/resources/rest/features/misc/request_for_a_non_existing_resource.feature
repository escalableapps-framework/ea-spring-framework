Feature: Request for a non-existing resource
  
  The REST service receives data via the request body and interprets it as null.  The service responds with an error message.
  
   Rule: The conversion between data types is given by the following table
  
  | parameter type | java type      |
  |----------------|----------------|
  | string         | String         |
  | number         | Integer        |
  | number         | Long           |
  | number         | BigInteger     |
  | number         | Float          |
  | number         | Double         |
  | number         | BigDecimal     |
  | boolean        | Boolean        |
  | ISO-8601       | OffsetDateTime |
  | ISO-8601       | LocalDateTime  |
  | ISO-8601       | LocalDate      |
  | ISO-8601       | LocalTime      |
  | ISO-8601       | Year           |
  | string         | ENUM           |

  Background: 
    Given the default timezone 'America/Puerto_Rico'

  Scenario Outline: Resource not found in <method> operation.
    Given the endpoint '/api/v2'
    And the request with the <method> method
    When I execute the request
    Then I get the status code 404
    And the response body conforms to the format of the "error_template.json" template
    And the response body contains a "message" property with the value "Not Found"

    Examples: 
      | method   |
      | 'GET'    |
      | 'HEAD'   |
      | 'POST'   |
      | 'PUT'    |
      | 'PATCH'  |
      | 'DELETE' |
