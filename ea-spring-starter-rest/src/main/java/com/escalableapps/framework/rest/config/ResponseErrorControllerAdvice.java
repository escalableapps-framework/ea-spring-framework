package com.escalableapps.framework.rest.config;

import static java.time.OffsetDateTime.now;
import static java.time.ZoneOffset.UTC;
import static java.time.format.DateTimeFormatter.ISO_INSTANT;
import static org.apache.commons.lang3.StringUtils.ordinalIndexOf;
import static org.apache.commons.lang3.StringUtils.substring;
import static org.apache.commons.lang3.StringUtils.substringBefore;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.RequestDispatcher;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.boot.autoconfigure.web.ErrorProperties;
import org.springframework.boot.autoconfigure.web.servlet.error.BasicErrorController;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import com.escalableapps.framework.core.model.exception.ConflictException;
import com.escalableapps.framework.core.model.exception.EaFrameworkException;
import com.escalableapps.framework.core.model.exception.NotFoundException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@ControllerAdvice
@Order(0)
@Slf4j
public class ResponseErrorControllerAdvice extends BasicErrorController {

  private static class EaErrorAttributes extends DefaultErrorAttributes {

    private static final String ATTRIBUTE_MESSAGE = "message";
    private static final String ATTRIBUTE_TIMESTAMP = "timestamp";

    @Override
    public Map<String, Object> getErrorAttributes(final WebRequest webRequest, final ErrorAttributeOptions options) {
      Map<String, Object> errorAttributes = new LinkedHashMap<>();
      errorAttributes.put(ATTRIBUTE_TIMESTAMP, new Date());
      int status = (int) webRequest.getAttribute(RequestDispatcher.ERROR_STATUS_CODE, RequestAttributes.SCOPE_REQUEST);
      errorAttributes.put(ATTRIBUTE_MESSAGE, HttpStatus.valueOf(status).getReasonPhrase());
      return errorAttributes;
    }
  }

  private static class EaErrorProperties extends ErrorProperties {

    EaErrorProperties() {
      setIncludeException(false);
      setIncludeBindingErrors(IncludeAttribute.NEVER);
      setIncludeMessage(IncludeAttribute.ALWAYS);
    }
  }

  @Data
  private static class ErrorMessage {

    private String timestamp;
    private String message;

    ErrorMessage(final String message) {
      this.timestamp = now(UTC).format(ISO_INSTANT);
      this.message = message;
    }
  }

  private static class ResponseError extends ResponseEntity<ErrorMessage> {

    ResponseError(final ErrorMessage body, final HttpStatus status) {
      super(body, status);
    }

    ResponseError(final String message, final HttpStatus status) {
      this(new ErrorMessage(message), status);
    }
  }

  private static final String MESSAGE_FIELD_MSG = "%s: %s";

  private static final String MESSAGE_MUST_BE_AN_VALID = "%s: is not a valid type";

  private static final String REQUIRED_NOT_PRESENT = "%s: must not be null";

  public ResponseErrorControllerAdvice() {
    super(new EaErrorAttributes(), new EaErrorProperties());
  }

  @ExceptionHandler({BindException.class})
  public ResponseEntity<ErrorMessage> handleBindException(final BindException e) {
    FieldError fieldError = e.getFieldError();
    String message = String.format(MESSAGE_MUST_BE_AN_VALID, fieldError == null ? "" : fieldError.getField());
    return new ResponseError(message, BAD_REQUEST);
  }

  @ExceptionHandler({ConstraintViolationException.class})
  public ResponseEntity<ErrorMessage> handleConstraintViolationException(final ConstraintViolationException e) {
    ConstraintViolation<?> constraintViolation = e.getConstraintViolations().stream().findFirst().orElse(null);
    if (constraintViolation == null) {
      throw new EaFrameworkException("constraintViolation is null", e);
    }
    if (isConstraintViolationFromController(constraintViolation)) {
      String message;
      int offset = isConstraintViolationFromDto(constraintViolation) ? 2 : 1;
      String code = constraintViolation.getPropertyPath().toString();
      String name = substring(code, ordinalIndexOf(code, ".", offset) + 1);
      message = String.format(MESSAGE_FIELD_MSG, name, constraintViolation.getMessage());
      return new ResponseError(message, BAD_REQUEST);
    } else {
      log.error(e.getMessage(), e);
      String message = INTERNAL_SERVER_ERROR.getReasonPhrase();
      return new ResponseError(message, INTERNAL_SERVER_ERROR);
    }
  }

  private boolean isConstraintViolationFromController(final ConstraintViolation<?> constraintViolation) {
    Class<?> rootBeanClass = constraintViolation.getRootBean().getClass();
    return rootBeanClass.isAnnotationPresent(RestController.class)
        || rootBeanClass.isAnnotationPresent(Controller.class);
  }

  private boolean isConstraintViolationFromDto(final ConstraintViolation<?> constraintViolation) {
    Class<?> rootBeanClass = constraintViolation.getRootBean().getClass();
    Class<?> leafBeanClass = constraintViolation.getLeafBean().getClass();
    return !rootBeanClass.equals(leafBeanClass);
  }

  @ExceptionHandler({HttpMessageNotReadableException.class})
  public ResponseEntity<ErrorMessage> handleHttpMessageNotReadableException(final HttpMessageNotReadableException e) {
    String message;
    Throwable cause = e.getCause();
    if (cause instanceof InvalidFormatException || cause instanceof JsonMappingException) {
      String fieldName = ((JsonMappingException) cause).getPath().stream()
          .map(reference -> reference.getIndex() < 0 ? String.format(".%s", reference.getFieldName())
              : String.format("[%s]", reference.getIndex()))
          .collect(Collectors.joining(""));
      message = String.format(MESSAGE_MUST_BE_AN_VALID, fieldName.substring(1));
    } else {
      message = substringBefore(e.getMessage(), ":");
    }
    return new ResponseError(message, BAD_REQUEST);
  }

  @ExceptionHandler({MethodArgumentTypeMismatchException.class})
  public ResponseEntity<ErrorMessage> handleMethodArgumentTypeMismatchException(
      final MethodArgumentTypeMismatchException e) {
    String message = String.format(MESSAGE_MUST_BE_AN_VALID, e.getName());
    return new ResponseError(message, BAD_REQUEST);
  }

  @ExceptionHandler({MissingServletRequestParameterException.class})
  public ResponseEntity<ErrorMessage> handleMissingServletRequestParameterException(
      final MissingServletRequestParameterException e) {
    String message = String.format(REQUIRED_NOT_PRESENT, e.getParameterName());
    return new ResponseError(message, BAD_REQUEST);
  }

  @ExceptionHandler({ConflictException.class})
  public ResponseEntity<ErrorMessage> handleConflictException(final ConflictException e) {
    String message = e.getMessage();
    return new ResponseError(message, CONFLICT);
  }

  @ExceptionHandler({NotFoundException.class})
  public ResponseEntity<ErrorMessage> handleNotFoundException(final NotFoundException e) {
    String message = e.getMessage();
    return new ResponseError(message, NOT_FOUND);
  }

  @ExceptionHandler({Exception.class})
  public ResponseEntity<ErrorMessage> handleException(final Exception e) {
    log.error(e.getMessage(), e);
    String message = INTERNAL_SERVER_ERROR.getReasonPhrase();
    return new ResponseError(message, INTERNAL_SERVER_ERROR);
  }
}
