package com.escalableapps.framework.rest;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses = EaRestAutoConfiguration.class)
public class EaRestAutoConfiguration {
}
