package com.escalableapps.framework.rest.model;

import org.springframework.core.convert.converter.Converter;

public interface RequestParamConverter<T> extends Converter<String, T> {
}
