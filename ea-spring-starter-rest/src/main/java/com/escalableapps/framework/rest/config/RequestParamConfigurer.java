package com.escalableapps.framework.rest.config;

import static java.time.ZoneId.systemDefault;
import static org.apache.commons.lang3.StringUtils.isEmpty;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.Year;
import java.util.List;

import org.springframework.format.FormatterRegistry;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.escalableapps.framework.rest.model.RequestParamConverter;

@Component
public class RequestParamConfigurer implements WebMvcConfigurer {

  private static class StringRequestParamConverter implements RequestParamConverter<String> {

    @Override
    public String convert(final String source) {
      return isEmpty(source) ? null : source;
    }
  }

  private static class OffsetDateTimeRequestParamConverter implements RequestParamConverter<OffsetDateTime> {

    @Override
    public OffsetDateTime convert(final String iso) {
      return isEmpty(iso) ? null : OffsetDateTime.parse(iso);
    }
  }

  private static class LocalDateTimeRequestParamConverter implements RequestParamConverter<LocalDateTime> {

    @Override
    public LocalDateTime convert(final String iso) {
      return isEmpty(iso) ? null : LocalDateTime.ofInstant(OffsetDateTime.parse(iso).toInstant(), systemDefault());
    }
  }

  private static class LocalDateRequestParamConverter implements RequestParamConverter<LocalDate> {

    @Override
    public LocalDate convert(final String iso) {
      return isEmpty(iso) ? null : LocalDate.parse(iso);
    }
  }

  private static class LocalTimeRequestParamConverter implements RequestParamConverter<LocalTime> {

    @Override
    public LocalTime convert(final String iso) {
      return isEmpty(iso) ? null : LocalTime.parse(iso);
    }
  }

  private static class YearRequestParamConverter implements RequestParamConverter<Year> {

    @Override
    public Year convert(final String iso) {
      return isEmpty(iso) ? null : Year.parse(iso);
    }
  }

  private final List<RequestParamConverter<?>> requestParamConverters;

  public RequestParamConfigurer(final List<RequestParamConverter<?>> requestParamConverters) {
    this.requestParamConverters = requestParamConverters;
    this.requestParamConverters.add(new StringRequestParamConverter());
    this.requestParamConverters.add(new OffsetDateTimeRequestParamConverter());
    this.requestParamConverters.add(new LocalDateTimeRequestParamConverter());
    this.requestParamConverters.add(new LocalDateRequestParamConverter());
    this.requestParamConverters.add(new LocalTimeRequestParamConverter());
    this.requestParamConverters.add(new YearRequestParamConverter());
  }

  @Override
  public void addFormatters(final FormatterRegistry registry) {
    requestParamConverters.forEach(registry::addConverter);
  }
}
