# EA-SPRING-STARTER-CUCUMBER

Provides a stable configuration to work with JUnit4, Mockito, Spring Test and Cucumber.

## 1. Licence
[The MIT License (MIT)](LICENSE)

## 2. Auto configure
* [Automatic configuration of the ea-spring-starter-cucumber module](src/test/resources/cucumber/config/eastartercucumberconfigtest/auto_configuration.feature)

### 2.1 Beans
**TestContext:** Stores the test context information

## 3. Model

**TestContext:** *Bean* that stores the test context information
* [Create a test context for each test scenario](src/test/resources/cucumber/model/testcontexttest/create_context.feature)
