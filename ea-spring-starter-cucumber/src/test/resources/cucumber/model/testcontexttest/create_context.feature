Feature: Create a test context for each test scenario
  In order to reuse step definitions from different objects
  As a backend developer or test developer
  I want that in each test scenario, a new test context is created and this can be read from different objects with step definitions

  Scenario Outline: At the beginning of the <scenario number>, a new test context is created
    Given the test scenario has begun
    When you get a test context
    Then the test context is empty

    Examples: 
      | scenario number |
      | first           |
      | second          |

  Scenario: The test context can be read by different step definitions
    Given two step definition classes
    When the propertyName property with the 'propertyValue' value is saved in the test context
    Then a secondary step-definition object can get the propertyName property
    And a secondary step-definition object can get the 'propertyValue' value from the propertyName property
