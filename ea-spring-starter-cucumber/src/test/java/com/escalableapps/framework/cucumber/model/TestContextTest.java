package com.escalableapps.framework.cucumber.model;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions( //
    features = "classpath:cucumber/model/testcontexttest", //
    glue = "classpath:com.escalableapps.framework.cucumber.model.testcontexttest" //
)
public class TestContextTest {

  public static enum TestContextTestContext {
    PROPERTY_NAME
  }
}
