package com.escalableapps.framework.cucumber.model.testcontexttest;

import java.util.HashMap;

import org.junit.Assert;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.util.ReflectionTestUtils;

import com.escalableapps.framework.cucumber.model.TestContext;
import com.escalableapps.framework.cucumber.model.TestContextTest.TestContextTestContext;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.spring.CucumberContextConfiguration;

@CucumberContextConfiguration
@ContextConfiguration
@EnableAutoConfiguration
public class TestContextStepDef {

  private static final String STEPDEF_PACKAGE = "com.escalableapps.framework.cucumber.model.testcontexttest";

  private final TestContext<TestContextTestContext> testContext;

  private Boolean staging;

  public TestContextStepDef(TestContext<TestContextTestContext> testContext) {
    this.testContext = testContext;
  }

  @Before
  public void preScenario() {
    staging = true;
  }

  @After
  public void postScenario() {
    staging = false;
  }

  @Given("the test scenario has begun")
  public void theTestScenarioHasBegun() {
    Assert.assertTrue(staging);
  }

  @Given("two step definition classes")
  public void twoStepDefinitionClasses() {
    Assert.assertEquals(STEPDEF_PACKAGE, TestContextStepDef.class.getPackage().getName());
    Assert.assertEquals(STEPDEF_PACKAGE, SecondaryStepDef.class.getPackage().getName());
  }

  @When("you get a test context")
  public void youGetATestContext() {
    Assert.assertNotNull(testContext);
  }

  @When("the propertyName property with the {string} value is saved in the test context")
  public void thePropertyNamePropertyWithTheValueIsSavedInTheTestContext(String propertyValue) {
    testContext.put(TestContextTestContext.PROPERTY_NAME, propertyValue);
  }

  @Then("the test context is empty")
  public void theTestContextIsEmpty() {
    HashMap<?, ?> innerContext = (HashMap<?, ?>) ReflectionTestUtils.getField(testContext, "context");
    Assert.assertTrue(innerContext.isEmpty());
  }
}
