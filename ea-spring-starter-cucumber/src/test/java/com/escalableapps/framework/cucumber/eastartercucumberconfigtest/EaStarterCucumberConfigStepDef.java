package com.escalableapps.framework.cucumber.eastartercucumberconfigtest;

import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.Optional;

import org.junit.Assert;
import org.mockito.internal.util.io.IOUtil;
import org.springframework.beans.BeansException;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.test.context.ContextConfiguration;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.spring.CucumberContextConfiguration;

@CucumberContextConfiguration
@ContextConfiguration
@EnableAutoConfiguration
public class EaStarterCucumberConfigStepDef implements ApplicationContextAware {

  private static final String ENABLE_AUTO_CONFIGURATION = "org.springframework.boot.autoconfigure.EnableAutoConfiguration";

  private ApplicationContext applicationContext;

  @Given("Spring AutoConfiguration is enabled")
  public void springAutoConfigurationIsEnabled() {
    Optional<Annotation> optAnnotation = Arrays.asList(this.getClass().getAnnotations()).stream() //
        .filter(annotation -> ENABLE_AUTO_CONFIGURATION.equals(annotation.annotationType().getCanonicalName())) //
        .findFirst();

    Assert.assertTrue(optAnnotation.isPresent());
  }

  @When("Spring context has been loaded")
  public void springContextHasBeenLoaded() {
    Assert.assertNotNull(applicationContext);
  }

  @Then("the class defined for auto configuration is loaded from the {string} file")
  public void theClassDefinedForAutoConfigurationIsLoadedFromTheFile(String configFileName) throws Exception {
    InputStream is = this.getClass().getResourceAsStream("/" + configFileName);
    String autoConfigurationClassName = IOUtil.readLines(is).stream() //
        .filter(line -> line.startsWith(ENABLE_AUTO_CONFIGURATION)) //
        .findFirst().get().split("=")[1];

    Class<?> beanClass = Class.forName(autoConfigurationClassName);
    Object bean = applicationContext.getBean(beanClass);

    Assert.assertNotNull(bean);
  }

  @Override
  public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
    this.applicationContext = applicationContext;
  }
}
