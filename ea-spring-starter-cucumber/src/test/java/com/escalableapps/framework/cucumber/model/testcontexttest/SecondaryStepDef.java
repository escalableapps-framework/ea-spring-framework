package com.escalableapps.framework.cucumber.model.testcontexttest;

import org.junit.Assert;

import com.escalableapps.framework.cucumber.model.TestContext;
import com.escalableapps.framework.cucumber.model.TestContextTest.TestContextTestContext;

import io.cucumber.java.en.Then;

public class SecondaryStepDef {

  private final TestContext<TestContextTestContext> testContext;

  public SecondaryStepDef(TestContext<TestContextTestContext> testContext) {
    this.testContext = testContext;
  }

  @Then("a secondary step-definition object can get the propertyName property")
  public void aSecondaryStepDefinitionObjectCanGetThePropertyNameProperty() {
    Assert.assertTrue(testContext.contains(TestContextTestContext.PROPERTY_NAME));
  }

  @Then("a secondary step-definition object can get the {string} value from the propertyName property")
  public void aSecondaryStepDefinitionObjectCanGetTheValueFromThePropertyNameProperty(String expectedPropertyValue) {
    String propertyValue = testContext.get(TestContextTestContext.PROPERTY_NAME);
    Assert.assertEquals(expectedPropertyValue, propertyValue);
  }
}
