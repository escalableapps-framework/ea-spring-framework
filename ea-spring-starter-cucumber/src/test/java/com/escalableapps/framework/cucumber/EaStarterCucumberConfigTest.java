package com.escalableapps.framework.cucumber;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions( //
    features = "classpath:cucumber/eastartercucumberconfigtest", //
    glue = "classpath:com.escalableapps.framework.cucumber.eastartercucumberconfigtest" //
)
public class EaStarterCucumberConfigTest {
}
