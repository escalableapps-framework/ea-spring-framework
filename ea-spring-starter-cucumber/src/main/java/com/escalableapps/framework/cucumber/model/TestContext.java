package com.escalableapps.framework.cucumber.model;

import java.util.HashMap;

public class TestContext<E extends Enum<E>> {

  private final HashMap<Enum<E>, Object> context;

  public TestContext() {
    context = new HashMap<>();
  }

  public boolean contains(final E key) {
    return context.containsKey(key);
  }

  @SuppressWarnings("unchecked")
  public <T> T get(final E key) {
    return (T) context.get(key);
  }

  public void put(final E key, final Object value) {
    context.put(key, value);
  }

}
