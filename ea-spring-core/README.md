# EA-SPRING-CORE

Provides classes form the domain model also utilities that are generally used for application.

## 1. Licence
[The MIT License (MIT)](LICENSE)

## 2. Auto configure
* [Automatic configuration of the ea-spring-core module](src/test/resources/core/eacoreautoconfigurationtest/auto_configuration.feature)

### 2.1 Beans
**objectMapper:** Fully configured Jackson ObjectMapper

## 3. Model

**PageRequest:** Indicate the number of the results page and the desired page size, optionally I could indicate some sorting criteria
* [Create a page request object](src/test/resources/core/model/vo/pagerequesttest/create_pagerequest.feature)

**PageRequest.SortRequest:** Indicate the ordering property and if it is ascending or descendant
* [Create a sort request object](src/test/resources/core/model/vo/sortrequesttest/create_sortrequest.feature)

**PageResponse:** Create a page response object to get the results and information from the pagination
* [Create a page response object](src/test/resources/core/model/vo/pageresponsetest/create_pageresponse.feature)

**ConflictException:** Thrown when a process reaches a state of conflict with business logic
* [Create a ConflictException with a customized message](src/test/resources/core/model/exception/conflictexceptiontest/create_conflictexception.feature)

**NotFoundException:** Thrown when a process is not able to find a some resource to complete the operation
* [Create a NotFoundException with a customized message](src/test/resources/core/model/exception/notfoundexceptiontest/create_notfoundexception.feature)

**EaFrameworkException:** Thrown by the framework when an inconsistency state occurs
* [Create a EaFrameworkException with a customized message](src/test/resources/core/model/exception/eaframerowkexceptiontest/create_eaframeworkexception.feature)

## 4. Utils

**JsonUtils:** Provides useful methods for handling json
* [Convert objects to json](src/test/resources/core/util/jsonutilstest/as_json.feature)
* [Convert objects to pretty json](src/test/resources/core/util/jsonutilstest/as_pretty_json.feature)
* [Convert objects to json including nulls](src/test/resources/core/util/jsonutilstest/as_json_include_null.feature)
* [Convert objects to pretty json including nulls](src/test/resources/core/util/jsonutilstest/as_pretty_json_include_null.feature)
* [Parse a json to type](src/test/resources/core/util/jsonutilstest/parse_as_type.feature)
* [Parse a json to map](src/test/resources/core/util/jsonutilstest/parse_as_map.feature)
* [Parse a json to list](src/test/resources/core/util/jsonutilstest/parse_as_list.feature)
* [Parse a json to type only calls the set methods of the properties that are defined in the json](src/test/resources/core/util/jsonutilstest/set_property.feature)

**ResourceUtils:** Provides useful methods for read files in the classpath
* [Read the content of a file in the classpath](src/test/resources/core/util/resourceutilstest/read_resource.feature)

## 5. Validation

## 5.1 Constraints

[**@DateRange:** Validation for a date to be within a range](src/test/resources/core/validation/constraints/daterangetest/date_range.feature)

[**@DatetimeRange:** Validation for a OffsetDateTime to be within a range](src/test/resources/core/validation/constraints/datetimerangetest/datetime_range_OffsetDateTime.feature), [Validation for a LocalDateTime to be within a range](src/test/resources/core/validation/constraints/datetimerangetest/datetime_range_LocalDateTime.feature)

[**@YearRange:** Validation for a year to be within a range](src/test/resources/core/validation/constraints/yearrangetest/year_range.feature)

[**@TextHtml:** Secure html content validation](src/test/resources/core/validation/constraints/texthtmltest/text_html.feature)

[**@TextPlain:** Plain text content validation](src/test/resources/core/validation/constraints/textplaintest/text_plain.feature)
