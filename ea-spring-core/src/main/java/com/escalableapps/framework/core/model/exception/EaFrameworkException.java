package com.escalableapps.framework.core.model.exception;

import static java.lang.String.format;

public class EaFrameworkException extends RuntimeException {

  private static final long serialVersionUID = -3735100850377391628L;

  public EaFrameworkException(final Exception cause) {
    super(cause.getMessage(), cause);
  }

  public EaFrameworkException(final String message, final Exception cause) {
    super(message, cause);
  }

  public EaFrameworkException(final String message, final Object... values) {
    super(message == null ? null : format(message, values));
  }
}
