package com.escalableapps.framework.core.validation.constraints;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE_USE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import static org.apache.commons.lang3.StringUtils.trimToNull;
import static org.jsoup.safety.Safelist.none;

import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;

import org.jsoup.Jsoup;

import com.escalableapps.framework.core.validation.constraints.TextPlain.List;
import com.escalableapps.framework.core.validation.constraints.TextPlain.TextPlainValidator;

@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
@Retention(RUNTIME)
@Repeatable(List.class)
@Documented
@Constraint(validatedBy = TextPlainValidator.class)
public @interface TextPlain {

  @Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
  @Retention(RUNTIME)
  @Documented
  @interface List {
    TextPlain[] value();
  }

  class TextPlainValidator implements ConstraintValidator<TextPlain, String> {

    @Override
    public boolean isValid(final String value, final ConstraintValidatorContext context) {
      if (trimToNull(value) == null) {
        return true;
      }
      return Jsoup.isValid(value, none());
    }
  }

  Class<?>[] groups() default {};

  String message() default "{com.escalableapps.framework.core.validation.constraints.TextPlain.fail}";

  Class<? extends Payload>[] payload() default {};
}
