package com.escalableapps.framework.core;

import static java.time.ZoneOffset.UTC;
import static java.time.format.DateTimeFormatter.ISO_OFFSET_DATE_TIME;
import static org.apache.commons.lang3.StringUtils.isEmpty;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import com.escalableapps.framework.core.model.jackson.JacksonDeserializer;
import com.escalableapps.framework.core.model.jackson.JacksonSerializer;
import com.escalableapps.framework.core.util.JsonUtils;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;

@ComponentScan(basePackageClasses = EaCoreAutoConfiguration.class)
public class EaCoreAutoConfiguration {

  private static class LocalDateTimeJacksonDeserializer extends JacksonDeserializer<LocalDateTime> {

    LocalDateTimeJacksonDeserializer() {
      super(LocalDateTime.class);
    }

    @Override
    protected LocalDateTime deserialize(final String iso) {
      if (isEmpty(iso)) {
        return null;
      }
      Instant instant = OffsetDateTime.parse(iso).toInstant();
      return LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
    }
  }

  private static class LocalDateTimeJacksonSerializer extends JacksonSerializer<LocalDateTime> {

    private static final long serialVersionUID = 2770763166315452459L;

    LocalDateTimeJacksonSerializer() {
      super(LocalDateTime.class);
    }

    @Override
    protected String serialize(final LocalDateTime object) {
      Instant instant = ZonedDateTime.of(object, ZoneId.systemDefault()).toInstant();
      OffsetDateTime offsetDateTime = OffsetDateTime.ofInstant(instant, UTC);
      return offsetDateTime.format(ISO_OFFSET_DATE_TIME);
    }
  }

  private static class JacksonModule extends SimpleModule {

    private static final long serialVersionUID = 5866789388501845129L;

    @SuppressWarnings("rawtypes")
    JacksonModule(final List<JacksonDeserializer> deserializers, final List<JacksonSerializer> serializers) {
      deserializers.add(new LocalDateTimeJacksonDeserializer());
      serializers.add(new LocalDateTimeJacksonSerializer());

      DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME.withZone(UTC);
      new LocalDateTimeDeserializer(formatter);

      deserializers.stream().forEach(this::addDeserializer);
      serializers.stream().forEach(this::addSerializer);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    private void addDeserializer(final JacksonDeserializer deserializer) {
      addDeserializer(deserializer.getType(), deserializer);
    }
  }

  private static class JsonUtilsConfigurer extends JsonUtils {

    protected static void configureObjectMapper(final ObjectMapper objectMapper) {
      JsonUtils.setObjectMapper(objectMapper);
    }
  }

  private final ObjectMapper objectMapper;

  @SuppressWarnings("rawtypes")
  public EaCoreAutoConfiguration(final List<JacksonDeserializer> deserializers,
      final List<JacksonSerializer> serializers) {
    objectMapper = new ObjectMapper() //
        .registerModule(new JavaTimeModule()) //
        .registerModule(new Jdk8Module()) //
        .registerModule(new JacksonModule(deserializers, serializers)) //
        .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES) //
        .disable(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE) //
        .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS) //
        .setSerializationInclusion(Include.NON_NULL);
    JsonUtilsConfigurer.configureObjectMapper(objectMapper);
  }

  @Bean
  public ObjectMapper objectMapper() {
    return objectMapper;
  }
}
