package com.escalableapps.framework.core.model.vo;

import static com.escalableapps.framework.core.util.ValidationUtils.validate;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode
public class PageRequest implements Serializable {

  public enum Direction {
    ASC, DESC
  }

  @Getter
  @ToString
  @EqualsAndHashCode
  public static class SortRequest<E extends Enum<E>> implements Serializable {

    private static final long serialVersionUID = 5354985423477658804L;

    @NotNull
    private final E property;

    @NotNull
    private final Direction direction;

    public SortRequest(final E property) {
      this(property, Direction.ASC);
    }

    public SortRequest(final E property, final Direction direction) {
      this.property = property;
      this.direction = direction;
      validate(this);
    }
  }

  private static final long serialVersionUID = -7847090259423573279L;

  @Min(0)
  private final int pageNumber;

  @Min(1)
  private final int pageSize;

  @NotNull
  @SuppressWarnings("rawtypes")
  private final List<@NotNull SortRequest> sortRequests;

  public PageRequest() {
    this(0, Integer.MAX_VALUE);
  }

  @SuppressWarnings("rawtypes")
  public PageRequest(final int pageNumber, final int pageSize, final SortRequest... sortRequests) {
    this.pageNumber = pageNumber;
    this.pageSize = pageSize;
    this.sortRequests = Arrays.asList(sortRequests);
    validate(this);
  }

  public int getOffset() {
    return pageNumber * pageSize;
  }
}
