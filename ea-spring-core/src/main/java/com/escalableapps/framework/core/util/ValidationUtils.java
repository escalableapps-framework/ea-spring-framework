package com.escalableapps.framework.core.util;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.Validator;

public final class ValidationUtils {

  private static Validator validator;

  public static void validate(final Object object, final Class<?>... groups) {
    if (validator == null) {
      createValidator();
    }
    Set<ConstraintViolation<Object>> constraintViolations = validator.validate(object, groups);
    if (!constraintViolations.isEmpty()) {
      throw new ConstraintViolationException(constraintViolations);
    }
  }

  private static synchronized void createValidator() {
    if (validator == null) {
      validator = Validation.buildDefaultValidatorFactory().getValidator();
    }
  }

  private ValidationUtils() {
  }
}
