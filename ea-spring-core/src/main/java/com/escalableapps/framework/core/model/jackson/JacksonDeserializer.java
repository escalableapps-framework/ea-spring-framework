package com.escalableapps.framework.core.model.jackson;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.fasterxml.jackson.databind.util.ClassUtil;

public abstract class JacksonDeserializer<T> extends JsonDeserializer<T> {

  private static final int MAX_ERROR_STR_LEN = 500;

  private Class<T> type;

  protected JacksonDeserializer(final Class<T> type) {
    this.type = type;
  }

  @Override
  public T deserialize(final JsonParser jsonParser, final DeserializationContext deserializationContext)
      throws InvalidFormatException {
    String value = null;
    try {
      JsonNode node = jsonParser.getCodec().readTree(jsonParser);
      value = node.asText();
      return deserialize(value);
    } catch (Exception e) {
      String message = String.format("Cannot deserialize value of type %s from String %s: %s", ClassUtil.nameOf(type),
          quotedString(value), e.getMessage());
      throw InvalidFormatException.from(jsonParser, message, value, type);
    }
  }

  protected abstract T deserialize(String value);

  public Class<T> getType() {
    return type;
  }

  private String quotedString(final String desc) {
    if (desc == null) {
      return "[N/A]";
    }
    return String.format("\"%s\"", truncate(desc));
  }

  private String truncate(final String desc) {
    if (desc == null) {
      return "";
    }
    if (desc.length() <= MAX_ERROR_STR_LEN) {
      return desc;
    }
    return desc.substring(0, MAX_ERROR_STR_LEN) + "]...[" + desc.substring(desc.length() - MAX_ERROR_STR_LEN);
  }
}
