package com.escalableapps.framework.core.util;

import static com.escalableapps.framework.core.util.ValidationUtils.validate;

import java.nio.charset.StandardCharsets;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

import com.escalableapps.framework.core.model.exception.EaFrameworkException;

import lombok.AllArgsConstructor;

public final class ResourceUtils {

  @AllArgsConstructor
  private static class ReadResourceByResourceLocation {

    @NotBlank
    @Pattern(regexp = CLASSPATH_RESOURCE_PREFIX, //
        message = "{com.escalableapps.framework.core.util.ResourceUtils.resourceLocation.pattern}")
    private String resourceLocation;
  }

  @AllArgsConstructor
  private static class ReadResourceByResourcePathAndFilename {

    @NotBlank
    @Pattern(regexp = CLASSPATH_RESOURCE_PREFIX, //
        message = "{com.escalableapps.framework.core.util.ResourceUtils.resourcePath.pattern}")
    private String resourcePath;

    @NotBlank
    private String filename;
  }

  private static final String CLASSPATH_RESOURCE_PREFIX = "^(classpath(\\*)?:.*)$";
  private static final ResourceLoader RESOURCE_LOADER = new DefaultResourceLoader();
  private static final ResourcePatternResolver RESOURCE_PATTERN_RESOLVER = new PathMatchingResourcePatternResolver();

  public static String readResource(final String resourceLocation) {
    validate(new ReadResourceByResourceLocation(resourceLocation));

    try {
      String resourceUrl = RESOURCE_PATTERN_RESOLVER.getResources(resourceLocation)[0].getURL().toString();
      Resource resource = RESOURCE_LOADER.getResource(resourceUrl);
      return new String(resource.getInputStream().readAllBytes(), StandardCharsets.UTF_8);
    } catch (Exception e) {
      throw new EaFrameworkException(e);
    }
  }

  public static String readResource(final String resourcePath, final String filename) {
    validate(new ReadResourceByResourcePathAndFilename(resourcePath, filename));

    StringBuilder resourceLocation = new StringBuilder();
    resourceLocation.append(resourcePath);
    if (!resourcePath.endsWith("/")) {
      resourceLocation.append("/");
    }
    if (filename.startsWith("/")) {
      resourceLocation.append(filename.substring(1));
    } else {
      resourceLocation.append(filename);
    }
    return readResource(resourceLocation.toString());
  }

  private ResourceUtils() {
  }
}
