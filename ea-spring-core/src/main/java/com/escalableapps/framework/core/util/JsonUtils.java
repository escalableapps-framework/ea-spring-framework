package com.escalableapps.framework.core.util;

import static com.escalableapps.framework.core.util.ValidationUtils.validate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.escalableapps.framework.core.model.exception.EaFrameworkException;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.AllArgsConstructor;

public class JsonUtils {

  @AllArgsConstructor
  private static class ParseAsMapByJsonAndKeyClassAndValueClass {

    @NotBlank
    private String json;

    @NotNull
    private Class<?> keyClass;

    @NotNull
    private Class<?> valueClass;
  }

  @AllArgsConstructor
  private static class ParseAsTypeByJsonAndType {

    @NotBlank
    private String json;

    @NotNull
    private Class<?> type;
  }

  private static ObjectMapper objectMapper;

  private static ObjectMapper objectMapperIncludeNull;

  public static String asJson(final Object source) {
    try {
      return objectMapper.writeValueAsString(source);
    } catch (JsonProcessingException e) {
      throw new EaFrameworkException(e);
    }
  }

  public static String asJsonIncludeNull(final Object source) {
    try {
      return objectMapperIncludeNull.writeValueAsString(source);
    } catch (JsonProcessingException e) {
      throw new EaFrameworkException(e);
    }
  }

  public static String asPrettyJson(final Object source) {
    try {
      return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(source);
    } catch (JsonProcessingException e) {
      throw new EaFrameworkException(e);
    }
  }

  public static String asPrettyJsonIncludeNull(final Object source) {
    try {
      return objectMapperIncludeNull.writerWithDefaultPrettyPrinter().writeValueAsString(source);
    } catch (JsonProcessingException e) {
      throw new EaFrameworkException(e);
    }
  }

  public static <T> T parseAsType(final String json, final Class<T> type) {
    validate(new ParseAsTypeByJsonAndType(json, type));
    try {
      return objectMapper.readValue(json, type);
    } catch (Exception e) {
      throw new EaFrameworkException(e);
    }
  }

  public static <T> List<T> parseAsList(final String json, final Class<T> type) {
    validate(new ParseAsTypeByJsonAndType(json, type));
    try {
      JavaType javaType = objectMapper.getTypeFactory().constructCollectionType(List.class, type);
      return objectMapper.readValue(json, javaType);
    } catch (Exception e) {
      throw new EaFrameworkException(e);
    }
  }

  public static <K, V> Map<K, V> parseAsMap(final String json, final Class<K> keyClass, final Class<V> valueClass) {
    validate(new ParseAsMapByJsonAndKeyClassAndValueClass(json, keyClass, valueClass));
    try {
      JavaType javaType = objectMapper.getTypeFactory().constructMapLikeType(HashMap.class, keyClass, valueClass);
      return objectMapper.readValue(json, javaType);
    } catch (Exception e) {
      throw new EaFrameworkException(e);
    }
  }

  protected static void setObjectMapper(final ObjectMapper mapper) {
    JsonUtils.objectMapper = mapper;
    objectMapperIncludeNull = mapper.copy().setSerializationInclusion(Include.USE_DEFAULTS);
  }

  protected JsonUtils() {
  }
}
