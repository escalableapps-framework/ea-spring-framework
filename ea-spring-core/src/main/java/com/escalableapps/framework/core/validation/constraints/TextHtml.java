package com.escalableapps.framework.core.validation.constraints;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE_USE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import static org.apache.commons.lang3.StringUtils.trimToNull;
import static org.jsoup.safety.Safelist.basic;
import static org.jsoup.safety.Safelist.basicWithImages;
import static org.jsoup.safety.Safelist.none;
import static org.jsoup.safety.Safelist.relaxed;
import static org.jsoup.safety.Safelist.simpleText;

import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;

import org.jsoup.Jsoup;

import com.escalableapps.framework.core.validation.constraints.TextHtml.List;
import com.escalableapps.framework.core.validation.constraints.TextHtml.TextHtmlValidator;

@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
@Retention(RUNTIME)
@Repeatable(List.class)
@Documented
@Constraint(validatedBy = TextHtmlValidator.class)
public @interface TextHtml {

  @Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
  @Retention(RUNTIME)
  @Documented
  @interface List {
    TextHtml[] value();
  }

  class TextHtmlValidator implements ConstraintValidator<TextHtml, String> {

    private TextType htmlType;

    @Override
    public void initialize(final TextHtml constraintAnnotation) {
      this.htmlType = constraintAnnotation.value();
    }

    @Override
    public boolean isValid(final String value, final ConstraintValidatorContext context) {
      if (trimToNull(value) == null) {
        return true;
      }
      switch (htmlType) {
      case HTML_FULL:
        return validateHtmlFull(value);
      case HTML_DATA_IMAGES:
        return validateHtmlDataImages(value);
      case HTML_IMAGES:
        return validateHtmlImages(value);
      case HTML_BASIC:
        return validateHtmlBasic(value);
      case HTML_SIMPLE:
        return validateHtmlSimple(value);
      default:
        return validateTextPlain(value);
      }
    }

    private boolean validateHtmlBasic(final String value) {
      return Jsoup.isValid(value, basic());
    }

    private boolean validateHtmlDataImages(final String value) {
      return Jsoup.isValid(value, basicWithImages() //
          .addProtocols("img", "src", "data") //
          .addAttributes("img", "style") //
      );
    }

    private boolean validateHtmlFull(final String value) {
      return Jsoup.isValid(value, relaxed());
    }

    private boolean validateHtmlImages(final String value) {
      return Jsoup.isValid(value, basicWithImages());
    }

    private boolean validateHtmlSimple(final String value) {
      return Jsoup.isValid(value, simpleText());
    }

    private boolean validateTextPlain(final String value) {
      return Jsoup.isValid(value, none());
    }
  }

  enum TextType {

    /**
     * This whitelist allows only text nodes: all HTML will be prevent.
     */
    TEXT_PLAIN,

    /**
     * This whitelist allows only simple text formatting: b, em, i, strong, u. All
     * other HTML (tags andattributes) will be prevent.
     */
    HTML_SIMPLE,

    /**
     * This whitelist allows a fuller range of text nodes: a, b, blockquote, br,
     * cite, code, dd, dl, dt, em, i, li,ol, p, pre, q, small, span, strike, strong,
     * sub, sup, u, ul, and appropriate attributes. Links (a elements) can point to
     * http, https, ftp, mailto, and have an enforced rel=nofollow attribute. Does
     * not allow images.This whitelist allows a fuller range of text nodes: a, b,
     * blockquote, br, cite, code, dd, dl, dt, em, i, li,ol, p, pre, q, small, span,
     * strike, strong, sub, sup, u, ul, and appropriate attributes. Links (a
     * elements) can point to http, https, ftp, mailto, and have an enforced
     * rel=nofollow attribute. Does not allow images.
     */
    HTML_BASIC,

    /**
     * This whitelist allows the same text tags as basic, and also allows img tags,
     * with appropriateattributes, with src pointing to http or https.
     */
    HTML_IMAGES,

    /**
     * This whitelist allows the same text tags as basic, and also allows img tags,
     * with appropriateattributes, with src pointing to http, https or data.
     */
    HTML_DATA_IMAGES,

    /**
     * This whitelist allows a full range of text and structural body HTML: a, b,
     * blockquote, br, caption, cite,code, col, colgroup, dd, div, dl, dt, em, h1,
     * h2, h3, h4, h5, h6, i, img, li, ol, p, pre, q, small, span, strike, strong,
     * sub,sup, table, tbody, td, tfoot, th, thead, tr, u, ul. Links do not have an
     * enforced rel=nofollow attribute, but you can add that if desired.
     */
    HTML_FULL;
  }

  Class<?>[] groups() default {};

  String message() default "{com.escalableapps.framework.core.validation.constraints.TextHtml.fail}";

  Class<? extends Payload>[] payload() default {};

  TextType value();
}
