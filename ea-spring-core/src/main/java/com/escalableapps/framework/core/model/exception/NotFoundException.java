package com.escalableapps.framework.core.model.exception;

import static java.lang.String.format;

public class NotFoundException extends RuntimeException {

  private static final long serialVersionUID = 9015492577010064962L;

  public NotFoundException() {
    this((String) null);
  }

  public NotFoundException(final Exception cause) {
    super(cause.getMessage(), cause);
  }

  public NotFoundException(final String message, final Object... values) {
    super(message == null ? null : format(message, values));
  }
}
