package com.escalableapps.framework.core.model.vo;

import static com.escalableapps.framework.core.util.ValidationUtils.validate;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode
public class PageResponse<T extends Serializable> implements Serializable {

  private static final long serialVersionUID = 8305640834041424258L;

  @NotNull
  private final List<T> results;

  @Min(0)
  private final long totalElements;

  @Min(0)
  private final int pageNumber;

  @Min(1)
  private final int pageSize;

  private final int totalPages;

  private final boolean first;

  private final boolean last;

  public PageResponse(final List<T> results, final long totalElements, final int pageNumber, final int pageSize) {
    this.results = results;
    this.totalElements = totalElements;
    this.pageNumber = pageNumber;
    this.pageSize = pageSize;
    validate(this);

    this.totalPages = this.totalElements == 0 ? 0
        : (int) Math.ceil((double) this.totalElements / (double) this.pageSize);
    this.first = this.totalElements == 0 || this.pageNumber == 0;
    this.last = this.totalElements == 0 || this.pageNumber >= (totalPages - 1);
  }
}
