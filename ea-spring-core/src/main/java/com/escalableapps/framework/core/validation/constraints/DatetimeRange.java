package com.escalableapps.framework.core.validation.constraints;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE_USE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;

import com.escalableapps.framework.core.validation.constraints.DatetimeRange.DatetimeRangeValidator;
import com.escalableapps.framework.core.validation.constraints.DatetimeRange.List;

@Documented
@Constraint(validatedBy = DatetimeRangeValidator.class)
@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
@Retention(RUNTIME)
@Repeatable(List.class)
public @interface DatetimeRange {

  class DatetimeRangeValidator implements ConstraintValidator<DatetimeRange, Object> {

    private Instant min;
    private Instant max;

    @Override
    public void initialize(final DatetimeRange constraintAnnotation) {
      min = OffsetDateTime.parse(constraintAnnotation.min()).toInstant();
      max = OffsetDateTime.parse(constraintAnnotation.max()).toInstant();
    }

    @Override
    public boolean isValid(final Object value, final ConstraintValidatorContext context) {
      if (value == null) {
        return true;
      }
      Instant instant;
      if (value instanceof OffsetDateTime) {
        instant = ((OffsetDateTime) value).toInstant();
      } else if (value instanceof LocalDateTime) {
        instant = ZonedDateTime.of((LocalDateTime) value, ZoneId.systemDefault()).toInstant();
      } else {
        return false;
      }
      return (min.isBefore(instant) || min.equals(instant)) && (instant.isBefore(max) || instant.equals(max));
    }
  }

  @Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
  @Retention(RUNTIME)
  @Documented
  @interface List {
    DatetimeRange[] value();
  }

  Class<?>[] groups() default {};

  String max() default "+999999999-12-31T23:59:59.999Z";

  String message() default "{com.escalableapps.framework.core.validation.constraints.DatetimeRange.fail}";

  String min() default "-999999999-01-01T00:00:00Z";

  Class<? extends Payload>[] payload() default {};
}
