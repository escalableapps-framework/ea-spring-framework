package com.escalableapps.framework.core.model.exception;

import static java.lang.String.format;

public class ConflictException extends RuntimeException {

  private static final long serialVersionUID = -1896788574972355211L;

  public ConflictException() {
    this((String) null);
  }

  public ConflictException(final Exception cause) {
    super(cause.getMessage(), cause);
  }

  public ConflictException(final String message, final Object... values) {
    super(message == null ? null : format(message, values));
  }
}
