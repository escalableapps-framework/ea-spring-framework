package com.escalableapps.framework.core.model.jackson;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

public abstract class JacksonSerializer<T> extends StdSerializer<T> {

  private static final long serialVersionUID = 2770763166315452459L;

  protected JacksonSerializer(final Class<T> type) {
    super(type);
  }

  @Override
  public void serialize(final T object, final JsonGenerator jsonGenerator, final SerializerProvider provider)
      throws IOException {
    jsonGenerator.writeString(serialize(object));
  }

  protected abstract String serialize(T object);
}
