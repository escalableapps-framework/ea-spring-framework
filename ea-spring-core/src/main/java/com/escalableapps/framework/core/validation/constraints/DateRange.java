package com.escalableapps.framework.core.validation.constraints;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE_USE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.time.LocalDate;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;

import com.escalableapps.framework.core.validation.constraints.DateRange.DateRangeValidator;
import com.escalableapps.framework.core.validation.constraints.DateRange.List;

@Documented
@Constraint(validatedBy = DateRangeValidator.class)
@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
@Retention(RUNTIME)
@Repeatable(List.class)
public @interface DateRange {

  class DateRangeValidator implements ConstraintValidator<DateRange, LocalDate> {

    private LocalDate min;
    private LocalDate max;

    @Override
    public void initialize(final DateRange constraintAnnotation) {
      min = LocalDate.parse(constraintAnnotation.min());
      max = LocalDate.parse(constraintAnnotation.max());
    }

    @Override
    public boolean isValid(final LocalDate current, final ConstraintValidatorContext context) {
      if (current == null) {
        return true;
      }
      return (min.isBefore(current) || min.equals(current)) && (current.isBefore(max) || current.equals(max));
    }
  }

  @Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
  @Retention(RUNTIME)
  @Documented
  @interface List {
    DateRange[] value();
  }

  Class<?>[] groups() default {};

  String max() default "+999999999-12-31";

  String message() default "{com.escalableapps.framework.core.validation.constraints.DateRange.fail}";

  String min() default "-999999999-01-01";

  Class<? extends Payload>[] payload() default {};
}
