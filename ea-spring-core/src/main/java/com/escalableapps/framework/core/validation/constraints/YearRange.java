package com.escalableapps.framework.core.validation.constraints;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE_USE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.time.Year;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;

import com.escalableapps.framework.core.validation.constraints.YearRange.List;
import com.escalableapps.framework.core.validation.constraints.YearRange.YearRangeValidator;

@Documented
@Constraint(validatedBy = YearRangeValidator.class)
@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
@Retention(RUNTIME)
@Repeatable(List.class)
public @interface YearRange {

  @Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
  @Retention(RUNTIME)
  @Documented
  @interface List {
    YearRange[] value();
  }

  class YearRangeValidator implements ConstraintValidator<YearRange, Year> {

    private Year min;
    private Year max;

    @Override
    public void initialize(final YearRange constraintAnnotation) {
      min = Year.parse(constraintAnnotation.min());
      max = Year.parse(constraintAnnotation.max());
    }

    @Override
    public boolean isValid(final Year current, final ConstraintValidatorContext context) {
      if (current == null) {
        return true;
      }
      return (min.isBefore(current) || min.equals(current)) && (current.isBefore(max) || current.equals(max));
    }
  }

  Class<?>[] groups() default {};

  String max() default "+999999999";

  String message() default "{com.escalableapps.framework.core.validation.constraints.YearRange.fail}";

  String min() default "-999999999";

  Class<? extends Payload>[] payload() default {};
}
