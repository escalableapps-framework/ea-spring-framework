package com.escalableapps.framework.core.validation.constraints.datetimerangetest;

import static com.escalableapps.framework.core.validation.constraints.DatetimeRangeTest.DatetimeRangeTestContext.LOCALDATETIME;
import static com.escalableapps.framework.core.validation.constraints.DatetimeRangeTest.DatetimeRangeTestContext.MESSAGE;
import static com.escalableapps.framework.core.validation.constraints.DatetimeRangeTest.DatetimeRangeTestContext.OFFSETDATETIME;
import static com.escalableapps.framework.core.validation.constraints.DatetimeRangeTest.DatetimeRangeTestContext.RESULT;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.Set;
import java.util.TimeZone;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.junit.Assert;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.test.context.ContextConfiguration;

import com.escalableapps.framework.core.validation.constraints.DatetimeRangeTest.DatetimeRangeTestContext;
import com.escalableapps.framework.core.validation.constraints.DatetimeRangeTest.LocalDateTimeDatetimeRangeDto;
import com.escalableapps.framework.core.validation.constraints.DatetimeRangeTest.OffsetDateTimeDatetimeRangeDto;
import com.escalableapps.framework.cucumber.model.TestContext;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.spring.CucumberContextConfiguration;

@CucumberContextConfiguration
@ContextConfiguration
@EnableAutoConfiguration
public class DatetimeRangeStepDef {

  private final TestContext<DatetimeRangeTestContext> testContext;

  private final Validator validator;

  public DatetimeRangeStepDef(TestContext<DatetimeRangeTestContext> testContext) {
    this.testContext = testContext;
    validator = Validation.buildDefaultValidatorFactory().getValidator();
  }

  @Given("the default timezone {string}")
  public void theDefaultTimezone(String timezone) {
    System.setProperty("user.timezone", timezone);
    TimeZone.setDefault(TimeZone.getTimeZone(timezone));
  }

  @Given("the OffsetDateTime {string}")
  public void theOffsetDateTime(String iso) {
    testContext.put(OFFSETDATETIME, OffsetDateTime.parse(iso));
  }

  @Given("the LocalDateTime {string}")
  public void theLocalDateTime(String iso) {
    Instant instant = OffsetDateTime.parse(iso).toInstant();
    LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
    testContext.put(LOCALDATETIME, localDateTime);
  }

  @When("compared the OffsetDateTime against the minimum value")
  public void comparedTheOffsetDateTimeAgainstTheMinimumValue() {
    OffsetDateTime offsetdatetime = testContext.get(OFFSETDATETIME);
    comparedAgainst(offsetdatetime, null, null);
  }

  @When("compared the OffsetDateTime against the minimum and maximum value")
  public void comparedTheOffsetDateTimeAgainstTheMinimumAndMaximumValue() {
    OffsetDateTime offsetdatetime = testContext.get(OFFSETDATETIME);
    comparedAgainst(null, offsetdatetime, null);
  }

  @When("compared the OffsetDateTime against the maximum value")
  public void comparedTheOffsetDateTimeAgainstTheMaximumValue() {
    OffsetDateTime offsetdatetime = testContext.get(OFFSETDATETIME);
    comparedAgainst(null, null, offsetdatetime);
  }

  @When("compared the LocalDateTime against the minimum value")
  public void comparedTheLocalDateTimeAgainstTheMinimumValue() {
    LocalDateTime offsetdatetime = testContext.get(LOCALDATETIME);
    comparedAgainst(offsetdatetime, null, null);
  }

  @When("compared the LocalDateTime against the minimum and maximum value")
  public void comparedTheLocalDateTimeAgainstTheMinimumAndMaximumValue() {
    LocalDateTime offsetdatetime = testContext.get(LOCALDATETIME);
    comparedAgainst(null, offsetdatetime, null);
  }

  @When("compared the LocalDateTime against the maximum value")
  public void comparedTheLocalDateTimeAgainstTheMaximumValue() {
    LocalDateTime offsetdatetime = testContext.get(LOCALDATETIME);
    comparedAgainst(null, null, offsetdatetime);
  }

  public void comparedAgainst(OffsetDateTime min, OffsetDateTime minmax, OffsetDateTime max) {
    OffsetDateTimeDatetimeRangeDto dto = OffsetDateTimeDatetimeRangeDto.builder().min(min).minmax(minmax).max(max)
        .build();

    Set<ConstraintViolation<OffsetDateTimeDatetimeRangeDto>> result = validator.validate(dto);

    if (result.isEmpty()) {
      testContext.put(RESULT, true);
    } else {
      testContext.put(RESULT, false);
      testContext.put(MESSAGE, result.stream().findFirst().get().getMessage());
    }
  }

  public void comparedAgainst(LocalDateTime min, LocalDateTime minmax, LocalDateTime max) {
    LocalDateTimeDatetimeRangeDto dto = LocalDateTimeDatetimeRangeDto.builder().min(min).minmax(minmax).max(max)
        .build();

    Set<ConstraintViolation<LocalDateTimeDatetimeRangeDto>> result = validator.validate(dto);

    if (result.isEmpty()) {
      testContext.put(RESULT, true);
    } else {
      testContext.put(RESULT, false);
      testContext.put(MESSAGE, result.stream().findFirst().get().getMessage());
    }
  }

  @Then("I get {string} as a result")
  public void iGetAsAResult(String expectedResult) {
    Boolean result = testContext.get(RESULT);

    Assert.assertEquals(Boolean.valueOf(expectedResult), result);
  }

  @Then("the message in the validation is {string}")
  public void theMessageInTheValidationIs(String expectedMessage) {
    String message = testContext.get(MESSAGE);

    Assert.assertEquals(expectedMessage, message);
  }

  @Then("the message in the validation is null")
  public void theMessageInTheValidationIsNull() {
    String message = testContext.get(MESSAGE);

    Assert.assertNull(message);
  }
}
