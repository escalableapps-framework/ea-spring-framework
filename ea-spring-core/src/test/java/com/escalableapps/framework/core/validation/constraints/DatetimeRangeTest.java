package com.escalableapps.framework.core.validation.constraints;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import lombok.Builder;
import lombok.Data;

@RunWith(Cucumber.class)
@CucumberOptions( //
    features = "classpath:core/validation/constraints/datetimerangetest", //
    glue = "classpath:com.escalableapps.framework.core.validation.constraints.datetimerangetest" //
)
public class DatetimeRangeTest {

  public static enum DatetimeRangeTestContext {
    OFFSETDATETIME, LOCALDATETIME, RESULT, MESSAGE
  }

  @Builder
  @Data
  public static class LocalDateTimeDatetimeRangeDto {

    @DatetimeRange(min = "2000-01-01T00:00:00Z")
    private LocalDateTime min;

    @DatetimeRange(min = "2000-01-01T00:00:00Z", max = "2010-01-01T00:00:00Z")
    private LocalDateTime minmax;

    @DatetimeRange(max = "2010-01-01T00:00:00Z")
    private LocalDateTime max;

  }

  @Builder
  @Data
  public static class OffsetDateTimeDatetimeRangeDto {

    @DatetimeRange(min = "2000-01-01T00:00:00Z")
    private OffsetDateTime min;

    @DatetimeRange(min = "2000-01-01T00:00:00Z", max = "2010-01-01T00:00:00Z")
    private OffsetDateTime minmax;

    @DatetimeRange(max = "2010-01-01T00:00:00Z")
    private OffsetDateTime max;

  }
}
