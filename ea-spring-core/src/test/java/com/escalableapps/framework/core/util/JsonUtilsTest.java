package com.escalableapps.framework.core.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.Year;
import java.util.Optional;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import lombok.Data;

@RunWith(Cucumber.class)
@CucumberOptions( //
    features = "classpath:core/util/jsonutilstest", //
    glue = "classpath:com.escalableapps.framework.core.util.jsonutilstest" //
)
public class JsonUtilsTest {

  public static enum JsonUtilsTestContext {
    JSON, OBJECT, LIST, EXCEPTION
  }

  @Data
  public static class CircularDto {
    CircularDto reference;
  }

  @Data
  public static class JsonUtilsDto {

    private String typeString;
    private Integer typeInteger;
    private Long typeLong;
    private BigInteger typeBigInteger;
    private Float typeFloat;
    private Double typeDouble;
    private BigDecimal typeBigDecimal;
    private Boolean typeBoolean;
    private LocalDate typeLocalDate;
    private OffsetDateTime typeOffsetDateTime;
    private LocalDateTime typeLocalDateTime;
    private LocalTime typeLocalTime;
    private Year typeYear;
    private EnumAB typeEnumAB;
    private EnumLowerAB typeEnumLowerAB;

  }

  public static enum EnumAB {
    A, B
  }

  public static enum EnumLowerAB {
    A, B;
  }

  @Data
  public static class OptionalWitness {
    private Optional<String> param;
  }

  @Data
  public static class Witness {

    private String param;
    private boolean paramDefined;

    public void setParam(String param) {
      this.param = param;
      paramDefined = true;
    }
  }
}
