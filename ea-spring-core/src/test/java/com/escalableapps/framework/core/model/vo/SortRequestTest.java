package com.escalableapps.framework.core.model.vo;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions( //
    features = "classpath:core/model/vo/sortrequesttest", //
    glue = "classpath:com.escalableapps.framework.core.model.vo.sortrequesttest" //
)
public class SortRequestTest {

  public static enum SortRequestTestContext {
    PROPERTY, DIRECTION, SORT_REQUEST, EXCEPTION;
  }

  public static enum SortRequestSortProperties {
    PROPERTY_NAME
  }
}
