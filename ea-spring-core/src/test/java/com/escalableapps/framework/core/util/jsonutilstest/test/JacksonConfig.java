package com.escalableapps.framework.core.util.jsonutilstest.test;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.escalableapps.framework.core.model.jackson.JacksonDeserializer;
import com.escalableapps.framework.core.model.jackson.JacksonSerializer;
import com.escalableapps.framework.core.util.JsonUtilsTest.EnumLowerAB;

@Configuration
public class JacksonConfig {

  private static List<Pair<String, EnumLowerAB>> listEnumLowerAB = Stream.of( //
      Pair.of("a", EnumLowerAB.A), //
      Pair.of("b", EnumLowerAB.B) //
  ).collect(Collectors.toList());

  private static class EnumLowerABJacksonDeserializer extends JacksonDeserializer<EnumLowerAB> {

    public EnumLowerABJacksonDeserializer() {
      super(EnumLowerAB.class);
    }

    @Override
    protected EnumLowerAB deserialize(String value) {
      return listEnumLowerAB.stream().filter(p -> value.equals(p.getLeft())).map(p -> p.getRight()).findFirst().get();
    }
  }

  private static class EnumLowerABJacksonSerializer extends JacksonSerializer<EnumLowerAB> {

    private static final long serialVersionUID = -5560689857010970586L;

    EnumLowerABJacksonSerializer() {
      super(EnumLowerAB.class);
    }

    @Override
    protected String serialize(EnumLowerAB object) {
      return listEnumLowerAB.stream().filter(p -> object.equals(p.getRight())).map(p -> p.getLeft()).findFirst().get();
    }
  }

  @Bean
  public EnumLowerABJacksonDeserializer enumLowerABJacksonDeserializer() {
    return new EnumLowerABJacksonDeserializer();
  }

  @Bean
  public EnumLowerABJacksonSerializer enumLowerABJacksonSerializer() {
    return new EnumLowerABJacksonSerializer();
  }
}
