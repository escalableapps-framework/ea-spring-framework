package com.escalableapps.framework.core.validation.constraints;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import lombok.Builder;
import lombok.Data;

@RunWith(Cucumber.class)
@CucumberOptions( //
    features = "classpath:core/validation/constraints/textplaintest", //
    glue = "classpath:com.escalableapps.framework.core.validation.constraints.textplaintest" //
)
public class TextPlainTest {

  public static enum TextPlainTestContext {
    TEXT, RESULT, MESSAGE
  }

  @Builder
  @Data
  public static class TextPlainDto {

    @TextPlain
    private String text;

  }
}
