package com.escalableapps.framework.core;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions( //
    features = "classpath:core/eacoreautoconfigurationtest", //
    glue = "classpath:com.escalableapps.framework.core.eacoreautoconfigurationtest" //
)
public class EaCoreAutoConfigurationTest {
}
