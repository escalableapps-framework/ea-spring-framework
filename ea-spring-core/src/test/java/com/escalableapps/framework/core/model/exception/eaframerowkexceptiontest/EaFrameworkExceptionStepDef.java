package com.escalableapps.framework.core.model.exception.eaframerowkexceptiontest;

import static com.escalableapps.framework.core.model.exception.EaFrameworkExceptionTest.EaFrameworkExceptionTestContext.CAUSE;
import static com.escalableapps.framework.core.model.exception.EaFrameworkExceptionTest.EaFrameworkExceptionTestContext.EXCEPTION;
import static com.escalableapps.framework.core.model.exception.EaFrameworkExceptionTest.EaFrameworkExceptionTestContext.FIRST_ARGUMENT;
import static com.escalableapps.framework.core.model.exception.EaFrameworkExceptionTest.EaFrameworkExceptionTestContext.MESSAGE;
import static com.escalableapps.framework.core.model.exception.EaFrameworkExceptionTest.EaFrameworkExceptionTestContext.PATTERN;
import static com.escalableapps.framework.core.model.exception.EaFrameworkExceptionTest.EaFrameworkExceptionTestContext.SECOND_ARGUMENT;

import org.junit.Assert;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.test.context.ContextConfiguration;

import com.escalableapps.framework.core.model.exception.EaFrameworkException;
import com.escalableapps.framework.core.model.exception.EaFrameworkExceptionTest.EaFrameworkExceptionTestContext;
import com.escalableapps.framework.cucumber.model.TestContext;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.spring.CucumberContextConfiguration;

@CucumberContextConfiguration
@ContextConfiguration
@EnableAutoConfiguration
public class EaFrameworkExceptionStepDef {

  private final TestContext<EaFrameworkExceptionTestContext> testContext;

  public EaFrameworkExceptionStepDef(TestContext<EaFrameworkExceptionTestContext> testContext) {
    this.testContext = testContext;
  }

  @Given("the pattern-message {string}")
  public void thePatternMessage(String patternMessage) {
    testContext.put(PATTERN, patternMessage);
  }

  @Given("argument is {string}")
  public void argumentIs(String argumentValue) {
    firstArgumentIs(argumentValue);
  }

  @Given("first argument is {string}")
  public void firstArgumentIs(String firstArgumentValue) {
    testContext.put(FIRST_ARGUMENT, firstArgumentValue);
  }

  @Given("second argument is {string}")
  public void secondArgumentIs(String secondArgumentValue) {
    testContext.put(SECOND_ARGUMENT, secondArgumentValue);
  }

  @Given("a Exception with the message {string}")
  public void aExceptionWithTheMessage(String message) {
    testContext.put(CAUSE, new Exception(message));
  }

  @Given("the message {string}")
  public void theMessage(String message) {
    testContext.put(MESSAGE, message);
  }

  @When("create a EaFrameworkException")
  public void createAEaFrameworkException() {
    String patternMessage = testContext.get(PATTERN);
    String firstArgumentValue = testContext.get(FIRST_ARGUMENT);
    String secondArgumentValue = testContext.get(SECOND_ARGUMENT);
    String message = testContext.get(MESSAGE);
    Exception exception = testContext.get(CAUSE);

    EaFrameworkException eaFrameworkException = null;
    if (exception != null && message == null) {
      eaFrameworkException = new EaFrameworkException(exception);
    } else if (exception != null && message != null) {
      eaFrameworkException = new EaFrameworkException(message, exception);
    } else if (patternMessage != null && firstArgumentValue == null) {
      eaFrameworkException = new EaFrameworkException(patternMessage);
    } else if (patternMessage != null && firstArgumentValue != null && secondArgumentValue == null) {
      eaFrameworkException = new EaFrameworkException(patternMessage, firstArgumentValue);
    } else if (patternMessage != null && firstArgumentValue != null && secondArgumentValue != null) {
      eaFrameworkException = new EaFrameworkException(patternMessage, firstArgumentValue, secondArgumentValue);
    }
    testContext.put(EXCEPTION, eaFrameworkException);

  }

  @Then("the message is {string}")
  public void theMessageIs(String expectedMessage) {
    EaFrameworkException eaFrameworkException = testContext.get(EXCEPTION);

    Assert.assertEquals(expectedMessage, eaFrameworkException.getMessage());
  }

  @Then("the exception cause is Exception")
  public void theExceptionCauseIsException() {
    EaFrameworkException eaFrameworkException = testContext.get(EXCEPTION);

    Assert.assertEquals(Exception.class, eaFrameworkException.getCause().getClass());
  }
}
