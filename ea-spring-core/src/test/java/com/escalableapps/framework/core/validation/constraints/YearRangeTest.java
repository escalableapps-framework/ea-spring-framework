package com.escalableapps.framework.core.validation.constraints;

import java.time.Year;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import lombok.Builder;
import lombok.Data;

@RunWith(Cucumber.class)
@CucumberOptions( //
    features = "classpath:core/validation/constraints/yearrangetest", //
    glue = "classpath:com.escalableapps.framework.core.validation.constraints.yearrangetest" //
)
public class YearRangeTest {

  public static enum YearRangeTestContext {
    YEAR, RESULT, MESSAGE
  }

  @Builder
  @Data
  public static class YearRangeDto {

    @YearRange(min = "2000")
    private Year min;

    @YearRange(min = "2000", max = "2010")
    private Year minmax;

    @YearRange(max = "2010")
    private Year max;

  }
}
