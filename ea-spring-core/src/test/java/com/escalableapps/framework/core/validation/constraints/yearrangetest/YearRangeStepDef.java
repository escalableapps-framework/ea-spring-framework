package com.escalableapps.framework.core.validation.constraints.yearrangetest;

import static com.escalableapps.framework.core.validation.constraints.YearRangeTest.YearRangeTestContext.MESSAGE;
import static com.escalableapps.framework.core.validation.constraints.YearRangeTest.YearRangeTestContext.RESULT;
import static com.escalableapps.framework.core.validation.constraints.YearRangeTest.YearRangeTestContext.YEAR;

import java.time.Year;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.junit.Assert;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.test.context.ContextConfiguration;

import com.escalableapps.framework.core.validation.constraints.YearRangeTest.YearRangeDto;
import com.escalableapps.framework.core.validation.constraints.YearRangeTest.YearRangeTestContext;
import com.escalableapps.framework.cucumber.model.TestContext;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.spring.CucumberContextConfiguration;

@CucumberContextConfiguration
@ContextConfiguration
@EnableAutoConfiguration
public class YearRangeStepDef {

  private final TestContext<YearRangeTestContext> testContext;

  private final Validator validator;

  public YearRangeStepDef(TestContext<YearRangeTestContext> testContext) {
    this.testContext = testContext;
    validator = Validation.buildDefaultValidatorFactory().getValidator();
  }

  @Given("the year {string}")
  public void theYear(String iso) {
    testContext.put(YEAR, Year.parse(iso));
  }

  @When("compared against the minimum value")
  public void comparedAgainstTheMinimumValue() {
    Year year = testContext.get(YEAR);
    comparedAgainst(year, null, null);
  }

  @When("compared against the minimum and maximum value")
  public void comparedAgainstTheMinimumAndMaximumValue() {
    Year year = testContext.get(YEAR);
    comparedAgainst(null, year, null);
  }

  @When("compared against the maximum value")
  public void comparedAgainstTheMaximumValue() {
    Year year = testContext.get(YEAR);
    comparedAgainst(null, null, year);
  }

  public void comparedAgainst(Year min, Year minmax, Year max) {
    YearRangeDto dto = YearRangeDto.builder().min(min).minmax(minmax).max(max).build();

    Set<ConstraintViolation<YearRangeDto>> result = validator.validate(dto);

    if (result.isEmpty()) {
      testContext.put(RESULT, true);
    } else {
      testContext.put(RESULT, false);
      testContext.put(MESSAGE, result.stream().findFirst().get().getMessage());
    }
  }

  @Then("I get {string} as a result")
  public void iGetAsAResult(String expectedResult) {
    Boolean result = testContext.get(RESULT);

    Assert.assertEquals(Boolean.valueOf(expectedResult), result);
  }

  @Then("the message in the validation is {string}")
  public void theMessageInTheValidationIs(String expectedMessage) {
    String message = testContext.get(MESSAGE);

    Assert.assertEquals(expectedMessage, message);
  }

  @Then("the message in the validation is null")
  public void theMessageInTheValidationIsNull() {
    String message = testContext.get(MESSAGE);

    Assert.assertNull(message);
  }
}
