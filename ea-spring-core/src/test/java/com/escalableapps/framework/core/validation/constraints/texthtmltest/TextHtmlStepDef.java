package com.escalableapps.framework.core.validation.constraints.texthtmltest;

import static com.escalableapps.framework.core.validation.constraints.TextHtmlTest.TextHtmlTestContext.MESSAGE;
import static com.escalableapps.framework.core.validation.constraints.TextHtmlTest.TextHtmlTestContext.RESULT;
import static com.escalableapps.framework.core.validation.constraints.TextHtmlTest.TextHtmlTestContext.TEXT;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.groups.Default;

import org.junit.Assert;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.test.context.ContextConfiguration;

import com.escalableapps.framework.core.validation.constraints.TextHtmlTest.HtmlBasicGroup;
import com.escalableapps.framework.core.validation.constraints.TextHtmlTest.HtmlFullGroup;
import com.escalableapps.framework.core.validation.constraints.TextHtmlTest.HtmlImagesDataGroup;
import com.escalableapps.framework.core.validation.constraints.TextHtmlTest.HtmlImagesGroup;
import com.escalableapps.framework.core.validation.constraints.TextHtmlTest.HtmlSimpleGroup;
import com.escalableapps.framework.core.validation.constraints.TextHtmlTest.TextHtmlDto;
import com.escalableapps.framework.core.validation.constraints.TextHtmlTest.TextHtmlTestContext;
import com.escalableapps.framework.core.validation.constraints.TextHtmlTest.TextPlainGroup;
import com.escalableapps.framework.cucumber.model.TestContext;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.spring.CucumberContextConfiguration;

@CucumberContextConfiguration
@ContextConfiguration
@EnableAutoConfiguration
public class TextHtmlStepDef {

  private final TestContext<TextHtmlTestContext> testContext;

  private final Validator validator;

  public TextHtmlStepDef(TestContext<TextHtmlTestContext> testContext) {
    this.testContext = testContext;
    validator = Validation.buildDefaultValidatorFactory().getValidator();
  }

  @Given("the text null")
  public void theTextNull() {
    testContext.put(TEXT, null);
  }

  @Given("the text {string}")
  public void theText(String text) {
    testContext.put(TEXT, text);
  }

  @When("the TextPlain validation is executed with the {string} constraint")
  public void theTextPlainValidationIsExecutedWithTheConstraint(String constraint) {
    String text = testContext.get(TEXT);
    TextHtmlDto dto = TextHtmlDto.builder().text(text).build();

    Set<ConstraintViolation<TextHtmlDto>> result = validator.validate(dto, getGroup(constraint));

    if (result.isEmpty()) {
      testContext.put(RESULT, true);
    } else {
      testContext.put(RESULT, false);
      testContext.put(MESSAGE, result.stream().findFirst().get().getMessage());
    }
  }

  private Class<?> getGroup(String constraint) {
    switch (constraint) {
    case "TEXT_PLAIN":
      return TextPlainGroup.class;
    case "HTML_SIMPLE":
      return HtmlSimpleGroup.class;
    case "HTML_BASIC":
      return HtmlBasicGroup.class;
    case "HTML_IMAGES":
      return HtmlImagesGroup.class;
    case "HTML_DATA_IMAGES":
      return HtmlImagesDataGroup.class;
    case "HTML_FULL":
      return HtmlFullGroup.class;
    default:
      return Default.class;
    }
  }

  @Then("the {string} result is obtained in the validation")
  public void theResultIsObtainedInTheValidation(String expectedResult) {
    Boolean result = testContext.get(RESULT);

    Assert.assertEquals(Boolean.valueOf(expectedResult), result);
  }

  @Then("the message in validation is null")
  public void theMessageInValidationIsNull() {
    String message = testContext.get(MESSAGE);

    Assert.assertNull(message);
  }

  @Then("the message in validation is {string}")
  public void theMessageInValidationIs(String expectedMessage) {
    String message = testContext.get(MESSAGE);

    Assert.assertEquals(expectedMessage, message);
  }
}
