package com.escalableapps.framework.core.util.resourceutilstest;

import static com.escalableapps.framework.core.util.ResourceUtilsTest.ResourceUtilsTestContext.CONTENT;
import static com.escalableapps.framework.core.util.ResourceUtilsTest.ResourceUtilsTestContext.EXCEPTION;
import static com.escalableapps.framework.core.util.ResourceUtilsTest.ResourceUtilsTestContext.FILENAME;
import static com.escalableapps.framework.core.util.ResourceUtilsTest.ResourceUtilsTestContext.PATH;
import static com.escalableapps.framework.core.util.ResourceUtilsTest.ResourceUtilsTestContext.RESOURCE;
import static org.apache.commons.lang3.StringUtils.contains;

import java.util.Locale;

import org.junit.Assert;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.test.context.ContextConfiguration;

import com.escalableapps.framework.core.util.ResourceUtils;
import com.escalableapps.framework.core.util.ResourceUtilsTest.ResourceUtilsTestContext;
import com.escalableapps.framework.cucumber.model.TestContext;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.spring.CucumberContextConfiguration;

@CucumberContextConfiguration
@ContextConfiguration
@EnableAutoConfiguration
public class ResourceUtilsStepDef {

  private final TestContext<ResourceUtilsTestContext> testContext;

  public ResourceUtilsStepDef(TestContext<ResourceUtilsTestContext> testContext) {
    this.testContext = testContext;
  }

  @Given("the default language {string}")
  public void theDefaultLanguage(String language) {
    Locale.setDefault(new Locale(language));
  }

  @Given("the following path {string}")
  public void theFollowingPath(String path) {
    testContext.put(PATH, path);
  }

  @Given("the file name {string}")
  public void theFileName(String filename) {
    testContext.put(FILENAME, filename);
  }

  @Given("the following file {string}")
  public void theFollowingFile(String resource) {
    testContext.put(RESOURCE, resource);
  }

  @When("I read the file")
  public void iReadTheFile() {
    String path = testContext.get(PATH);
    String filename = testContext.get(FILENAME);
    String resource = testContext.get(RESOURCE);

    try {
      String content;
      if (testContext.contains(PATH)) {
        content = ResourceUtils.readResource(path, filename);
      } else {
        content = ResourceUtils.readResource(resource);
      }
      testContext.put(CONTENT, content);
    } catch (Exception exception) {
      testContext.put(EXCEPTION, exception);
    }
  }

  @Then("I get the content")
  public void iGetTheContent(String expectedContent) {
    String content = testContext.get(CONTENT);

    Assert.assertEquals(expectedContent, content);
  }

  @Then("I get an {string} with the message {string}")
  public void iGetAnWithTheMessage(String expectedExceptionClass, String expectedMessage) {
    Exception exception = testContext.get(EXCEPTION);

    Assert.assertNotNull(exception);
    Assert.assertEquals(expectedExceptionClass, exception.getClass().getSimpleName());
    Assert.assertEquals(expectedMessage, exception.getMessage());
  }

  @Then("I get an {string} contains the message {string}")
  public void iGetAnContainsTheMessage(String expectedExceptionClass, String expectedMessage) {
    Exception exception = testContext.get(EXCEPTION);

    Assert.assertNotNull(exception);
    Assert.assertEquals(expectedExceptionClass, exception.getClass().getSimpleName());
    Assert.assertTrue(contains(exception.getMessage(), expectedMessage));
  }
}
