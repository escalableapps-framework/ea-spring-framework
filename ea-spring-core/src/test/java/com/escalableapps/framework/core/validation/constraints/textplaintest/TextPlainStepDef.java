package com.escalableapps.framework.core.validation.constraints.textplaintest;

import static com.escalableapps.framework.core.validation.constraints.TextPlainTest.TextPlainTestContext.MESSAGE;
import static com.escalableapps.framework.core.validation.constraints.TextPlainTest.TextPlainTestContext.RESULT;
import static com.escalableapps.framework.core.validation.constraints.TextPlainTest.TextPlainTestContext.TEXT;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.junit.Assert;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.test.context.ContextConfiguration;

import com.escalableapps.framework.core.validation.constraints.TextPlainTest.TextPlainDto;
import com.escalableapps.framework.core.validation.constraints.TextPlainTest.TextPlainTestContext;
import com.escalableapps.framework.cucumber.model.TestContext;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.spring.CucumberContextConfiguration;

@CucumberContextConfiguration
@ContextConfiguration
@EnableAutoConfiguration
public class TextPlainStepDef {

  private final TestContext<TextPlainTestContext> testContext;

  private final Validator validator;

  public TextPlainStepDef(TestContext<TextPlainTestContext> testContext) {
    this.testContext = testContext;
    validator = Validation.buildDefaultValidatorFactory().getValidator();
  }

  @Given("the text null")
  public void theTextNull() {
    testContext.put(TEXT, null);
  }

  @Given("the text {string}")
  public void theText(String text) {
    testContext.put(TEXT, text);
  }

  @When("the TextPlain validation is executed")
  public void theTextPlainValidationIsExecuted() {
    String text = testContext.get(TEXT);
    TextPlainDto dto = TextPlainDto.builder().text(text).build();

    Set<ConstraintViolation<TextPlainDto>> result = validator.validate(dto);

    if (result.isEmpty()) {
      testContext.put(RESULT, true);
    } else {
      testContext.put(RESULT, false);
      testContext.put(MESSAGE, result.stream().findFirst().get().getMessage());
    }
  }

  @Then("the {string} result is obtained in the validation")
  public void theResultIsObtainedInTheValidation(String expectedResult) {
    Boolean result = testContext.get(RESULT);

    Assert.assertEquals(Boolean.valueOf(expectedResult), result);
  }

  @Then("the message in validation is null")
  public void theMessageInValidationIsNull() {
    String message = testContext.get(MESSAGE);

    Assert.assertNull(message);
  }

  @Then("the message in validation is {string}")
  public void theMessageInValidationIs(String expectedMessage) {
    String message = testContext.get(MESSAGE);

    Assert.assertEquals(expectedMessage, message);
  }
}
