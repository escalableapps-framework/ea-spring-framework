package com.escalableapps.framework.core.model.exception;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions( //
    features = "classpath:core/model/exception/conflictexceptiontest", //
    glue = {"classpath:com.escalableapps.framework.core.model.exception.conflictexceptiontest"} //
)
public class ConflictExceptionTest {

  public static enum ConflictExceptionTestContext {
    PATTERN, MESSAGE, FIRST_ARGUMENT, SECOND_ARGUMENT, CAUSE, EXCEPTION
  }
}
