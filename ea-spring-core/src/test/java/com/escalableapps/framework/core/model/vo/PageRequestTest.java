package com.escalableapps.framework.core.model.vo;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions( //
    features = "classpath:core/model/vo/pagerequesttest", //
    glue = "classpath:com.escalableapps.framework.core.model.vo.pagerequesttest" //
)
public class PageRequestTest {

  public static enum PageRequestTestContext {
    PAGE_NUMBER, PAGE_SIZE, OFFSET, SORT_CRITERIA, PAGE_REQUEST, EXCEPTION
  }

  public static enum PageRequestSortProperties {
    PROPERTY_NAME_1, PROPERTY_NAME_2
  }
}
