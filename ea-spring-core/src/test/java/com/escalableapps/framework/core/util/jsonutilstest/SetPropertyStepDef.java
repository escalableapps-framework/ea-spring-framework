package com.escalableapps.framework.core.util.jsonutilstest;

import static com.escalableapps.framework.core.util.JsonUtilsTest.JsonUtilsTestContext.JSON;
import static com.escalableapps.framework.core.util.JsonUtilsTest.JsonUtilsTestContext.OBJECT;

import org.junit.Assert;

import com.escalableapps.framework.core.util.JsonUtils;
import com.escalableapps.framework.core.util.JsonUtilsTest.JsonUtilsTestContext;
import com.escalableapps.framework.core.util.JsonUtilsTest.OptionalWitness;
import com.escalableapps.framework.core.util.JsonUtilsTest.Witness;
import com.escalableapps.framework.cucumber.model.TestContext;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class SetPropertyStepDef {

  private final TestContext<JsonUtilsTestContext> testContext;

  public SetPropertyStepDef(TestContext<JsonUtilsTestContext> testContext) {
    this.testContext = testContext;
  }

  @Given("a json {string}")
  public void aJson(String json) {
    testContext.put(JSON, json);
  }

  @When("parse the json as a type Witness")
  public void parseTheJsonAsATypeWitness() {
    String json = testContext.get(JSON);

    Witness object = JsonUtils.parseAsType(json, Witness.class);

    testContext.put(OBJECT, object);
  }

  @When("parse the json as a type OptionalWitness")
  public void parseTheJsonAsATypeOptionalWitness() {
    String json = testContext.get(JSON);

    OptionalWitness object = JsonUtils.parseAsType(json, OptionalWitness.class);

    testContext.put(OBJECT, object);
  }

  @Then("the object has the paramDefined value {string}")
  public void theObjectHasTheParamDefinedValue(String paramDefined) {
    Witness object = testContext.get(OBJECT);
    boolean expected = Boolean.valueOf(paramDefined);

    Assert.assertEquals(expected, object.isParamDefined());
  }

  @Then("the object has the param value null")
  public void theObjectHasTheParamValueNull() {
    Witness object = testContext.get(OBJECT);

    Assert.assertNull(object.getParam());
  }

  @Then("the object has the param value {string}")
  public void theObjectHasTheParamValue(String expectedParamValue) {
    Witness object = testContext.get(OBJECT);

    Assert.assertEquals(expectedParamValue, object.getParam());
  }

  @Then("the object has the optional param set as null")
  public void theObjectHasTheOptionalParamSetAsNull() {
    OptionalWitness object = testContext.get(OBJECT);

    Assert.assertNull(object.getParam());
  }

  @Then("the object has the optional param set as not null")
  public void theObjectHasTheOptionalParamSetAsNotNull() {
    OptionalWitness object = testContext.get(OBJECT);

    Assert.assertNotNull(object.getParam());
  }

  @Then("the object has the optional param value is ")
  public void theObjectHasTheOptionalParamValueIs() {
  }

  @Then("the object has the optional param value is present")
  public void theObjectHasTheOptionalParamValueIsPresent() {
    OptionalWitness object = testContext.get(OBJECT);

    Assert.assertTrue(object.getParam().isPresent());
  }

  @Then("the object has the optional param value is not present")
  public void theObjectHasTheOptionalParamValueIsNotPresent() {
    OptionalWitness object = testContext.get(OBJECT);

    Assert.assertFalse(object.getParam().isPresent());
  }

  @Then("the object has the optional param value set as ")
  public void theObjectHasTheOptionalParamValueSetAs() {
  }

  @Then("the object has the optional param value set as {string}")
  public void theObjectHasTheOptionalParamValueSetAs(String expectedOptionalParamValue) {
    OptionalWitness object = testContext.get(OBJECT);

    Assert.assertEquals(expectedOptionalParamValue, object.getParam().get());
  }
}
