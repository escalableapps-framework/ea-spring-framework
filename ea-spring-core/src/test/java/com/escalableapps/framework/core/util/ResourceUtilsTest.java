package com.escalableapps.framework.core.util;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions( //
    features = "classpath:core/util/resourceutilstest", //
    glue = "classpath:com.escalableapps.framework.core.util.resourceutilstest" //
)
public class ResourceUtilsTest {

  public static enum ResourceUtilsTestContext {
    PATH, FILENAME, RESOURCE, CONTENT, EXCEPTION
  }
}
