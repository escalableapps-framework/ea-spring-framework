package com.escalableapps.framework.core.model.exception.conflictexceptiontest;

import static com.escalableapps.framework.core.model.exception.ConflictExceptionTest.ConflictExceptionTestContext.CAUSE;
import static com.escalableapps.framework.core.model.exception.ConflictExceptionTest.ConflictExceptionTestContext.EXCEPTION;
import static com.escalableapps.framework.core.model.exception.ConflictExceptionTest.ConflictExceptionTestContext.FIRST_ARGUMENT;
import static com.escalableapps.framework.core.model.exception.ConflictExceptionTest.ConflictExceptionTestContext.PATTERN;
import static com.escalableapps.framework.core.model.exception.ConflictExceptionTest.ConflictExceptionTestContext.SECOND_ARGUMENT;

import org.junit.Assert;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.test.context.ContextConfiguration;

import com.escalableapps.framework.core.model.exception.ConflictException;
import com.escalableapps.framework.core.model.exception.ConflictExceptionTest.ConflictExceptionTestContext;
import com.escalableapps.framework.cucumber.model.TestContext;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.spring.CucumberContextConfiguration;

@CucumberContextConfiguration
@ContextConfiguration
@EnableAutoConfiguration
public class ConflictExceptionStepDef {

  private final TestContext<ConflictExceptionTestContext> testContext;

  public ConflictExceptionStepDef(TestContext<ConflictExceptionTestContext> testContext) {
    this.testContext = testContext;
  }

  @Given("the pattern-message {string}")
  public void thePatternMessage(String patternMessage) {
    testContext.put(PATTERN, patternMessage);
  }

  @Given("argument is {string}")
  public void argumentIs(String argumentValue) {
    firstArgumentIs(argumentValue);
  }

  @Given("first argument is {string}")
  public void firstArgumentIs(String firstArgumentValue) {
    testContext.put(FIRST_ARGUMENT, firstArgumentValue);
  }

  @Given("second argument is {string}")
  public void secondArgumentIs(String secondArgumentValue) {
    testContext.put(SECOND_ARGUMENT, secondArgumentValue);
  }

  @Given("a Exception with the message {string}")
  public void aExceptionWithTheMessage(String message) {
    testContext.put(CAUSE, new Exception(message));
  }

  @When("create a ConflictException")
  public void createAConflictException() {
    String patternMessage = testContext.get(PATTERN);
    String firstArgumentValue = testContext.get(FIRST_ARGUMENT);
    String secondArgumentValue = testContext.get(SECOND_ARGUMENT);
    Exception exception = testContext.get(CAUSE);

    ConflictException conflictException = null;
    if (exception != null) {
      conflictException = new ConflictException(exception);
    } else if (patternMessage == null) {
      conflictException = new ConflictException();
    } else if (patternMessage != null && firstArgumentValue == null) {
      conflictException = new ConflictException(patternMessage);
    } else if (patternMessage != null && firstArgumentValue != null && secondArgumentValue == null) {
      conflictException = new ConflictException(patternMessage, firstArgumentValue);
    } else if (patternMessage != null && firstArgumentValue != null && secondArgumentValue != null) {
      conflictException = new ConflictException(patternMessage, firstArgumentValue, secondArgumentValue);
    }

    testContext.put(EXCEPTION, conflictException);
  }

  @Then("the message is null")
  public void theMessageIsNull() {
    ConflictException conflictException = testContext.get(EXCEPTION);

    Assert.assertNull(conflictException.getMessage());
  }

  @Then("the message is {string}")
  public void theMessageIs(String expectedMessage) {
    ConflictException conflictException = testContext.get(EXCEPTION);

    Assert.assertEquals(expectedMessage, conflictException.getMessage());
  }

  @Then("the exception cause is Exception")
  public void theExceptionCauseIsException() {
    ConflictException conflictException = testContext.get(EXCEPTION);

    Assert.assertEquals(Exception.class, conflictException.getCause().getClass());
  }
}
