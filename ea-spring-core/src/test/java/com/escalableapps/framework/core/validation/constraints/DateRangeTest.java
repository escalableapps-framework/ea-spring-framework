package com.escalableapps.framework.core.validation.constraints;

import java.time.LocalDate;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import lombok.Builder;
import lombok.Data;

@RunWith(Cucumber.class)
@CucumberOptions( //
    features = "classpath:core/validation/constraints/daterangetest", //
    glue = "classpath:com.escalableapps.framework.core.validation.constraints.daterangetest" //
)
public class DateRangeTest {

  public static enum DateRangeTestContext {
    LOCALDATE, RESULT, MESSAGE
  }

  @Builder
  @Data
  public static class DateRangeDto {

    @DateRange(min = "2000-01-01")
    private LocalDate min;

    @DateRange(min = "2000-01-01", max = "2010-01-01")
    private LocalDate minmax;

    @DateRange(max = "2010-01-01")
    private LocalDate max;

  }
}
