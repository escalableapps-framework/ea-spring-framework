package com.escalableapps.framework.core.model.vo;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions( //
    features = "classpath:core/model/vo/pageresponsetest", //
    glue = "classpath:com.escalableapps.framework.core.model.vo.pageresponsetest" //
)
public class PageResponseTest {

  public static enum PageResponseTestContext {
    RESULTS, TOTAL_ELEMENTS, PAGE_NUMBER, PAGE_SIZE, PAGE_RESPONSE, EXCEPTION
  }
}
