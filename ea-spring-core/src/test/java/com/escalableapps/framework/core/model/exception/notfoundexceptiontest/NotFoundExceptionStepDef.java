package com.escalableapps.framework.core.model.exception.notfoundexceptiontest;

import static com.escalableapps.framework.core.model.exception.NotFoundExceptionTest.NotFoundExceptionTestContext.CAUSE;
import static com.escalableapps.framework.core.model.exception.NotFoundExceptionTest.NotFoundExceptionTestContext.EXCEPTION;
import static com.escalableapps.framework.core.model.exception.NotFoundExceptionTest.NotFoundExceptionTestContext.FIRST_ARGUMENT;
import static com.escalableapps.framework.core.model.exception.NotFoundExceptionTest.NotFoundExceptionTestContext.PATTERN;
import static com.escalableapps.framework.core.model.exception.NotFoundExceptionTest.NotFoundExceptionTestContext.SECOND_ARGUMENT;

import org.junit.Assert;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.test.context.ContextConfiguration;

import com.escalableapps.framework.core.model.exception.NotFoundException;
import com.escalableapps.framework.core.model.exception.NotFoundExceptionTest.NotFoundExceptionTestContext;
import com.escalableapps.framework.cucumber.model.TestContext;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.spring.CucumberContextConfiguration;

@CucumberContextConfiguration
@ContextConfiguration
@EnableAutoConfiguration
public class NotFoundExceptionStepDef {

  private final TestContext<NotFoundExceptionTestContext> testContext;

  public NotFoundExceptionStepDef(TestContext<NotFoundExceptionTestContext> testContext) {
    this.testContext = testContext;
  }

  @Given("the pattern-message {string}")
  public void thePatternMessage(String patternMessage) {
    testContext.put(PATTERN, patternMessage);
  }

  @Given("argument is {string}")
  public void argumentIs(String argumentValue) {
    firstArgumentIs(argumentValue);
  }

  @Given("first argument is {string}")
  public void firstArgumentIs(String firstArgumentValue) {
    testContext.put(FIRST_ARGUMENT, firstArgumentValue);
  }

  @Given("second argument is {string}")
  public void secondArgumentIs(String secondArgumentValue) {
    testContext.put(SECOND_ARGUMENT, secondArgumentValue);
  }

  @Given("a Exception with the message {string}")
  public void aExceptionWithTheMessage(String message) {
    testContext.put(CAUSE, new Exception(message));
  }

  @When("create a NotFoundException")
  public void createANotFoundException() {
    String patternMessage = testContext.get(PATTERN);
    String firstArgumentValue = testContext.get(FIRST_ARGUMENT);
    String secondArgumentValue = testContext.get(SECOND_ARGUMENT);
    Exception exception = testContext.get(CAUSE);

    NotFoundException notFoundException = null;
    if (exception != null) {
      notFoundException = new NotFoundException(exception);
    } else if (patternMessage == null) {
      notFoundException = new NotFoundException();
    } else if (patternMessage != null && firstArgumentValue == null) {
      notFoundException = new NotFoundException(patternMessage);
    } else if (patternMessage != null && firstArgumentValue != null && secondArgumentValue == null) {
      notFoundException = new NotFoundException(patternMessage, firstArgumentValue);
    } else if (patternMessage != null && firstArgumentValue != null && secondArgumentValue != null) {
      notFoundException = new NotFoundException(patternMessage, firstArgumentValue, secondArgumentValue);
    }

    testContext.put(EXCEPTION, notFoundException);
  }

  @Then("the message is null")
  public void theMessageIsNull() {
    NotFoundException notFoundException = testContext.get(EXCEPTION);

    Assert.assertNull(notFoundException.getMessage());
  }

  @Then("the message is {string}")
  public void theMessageIs(String expectedMessage) {
    NotFoundException notFoundException = testContext.get(EXCEPTION);

    Assert.assertEquals(expectedMessage, notFoundException.getMessage());
  }

  @Then("the exception cause is Exception")
  public void theExceptionCauseIsException() {
    NotFoundException notFoundException = testContext.get(EXCEPTION);

    Assert.assertEquals(Exception.class, notFoundException.getCause().getClass());
  }
}
