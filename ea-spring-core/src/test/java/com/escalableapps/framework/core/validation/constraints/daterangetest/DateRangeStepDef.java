package com.escalableapps.framework.core.validation.constraints.daterangetest;

import static com.escalableapps.framework.core.validation.constraints.DateRangeTest.DateRangeTestContext.LOCALDATE;
import static com.escalableapps.framework.core.validation.constraints.DateRangeTest.DateRangeTestContext.MESSAGE;
import static com.escalableapps.framework.core.validation.constraints.DateRangeTest.DateRangeTestContext.RESULT;

import java.time.LocalDate;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.junit.Assert;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.test.context.ContextConfiguration;

import com.escalableapps.framework.core.validation.constraints.DateRangeTest.DateRangeDto;
import com.escalableapps.framework.core.validation.constraints.DateRangeTest.DateRangeTestContext;
import com.escalableapps.framework.cucumber.model.TestContext;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.spring.CucumberContextConfiguration;

@CucumberContextConfiguration
@ContextConfiguration
@EnableAutoConfiguration
public class DateRangeStepDef {

  private final TestContext<DateRangeTestContext> testContext;

  private final Validator validator;

  public DateRangeStepDef(TestContext<DateRangeTestContext> testContext) {
    this.testContext = testContext;
    validator = Validation.buildDefaultValidatorFactory().getValidator();
  }

  @Given("the date {string}")
  public void theDate(String iso) {
    testContext.put(LOCALDATE, LocalDate.parse(iso));
  }

  @When("compared against the minimum value")
  public void comparedAgainstTheMinimumValue() {
    LocalDate localDate = testContext.get(LOCALDATE);
    comparedAgainst(localDate, null, null);
  }

  @When("compared against the minimum and maximum value")
  public void comparedAgainstTheMinimumAndMaximumValue() {
    LocalDate localDate = testContext.get(LOCALDATE);
    comparedAgainst(null, localDate, null);
  }

  @When("compared against the maximum value")
  public void comparedAgainstTheMaximumValue() {
    LocalDate localDate = testContext.get(LOCALDATE);
    comparedAgainst(null, null, localDate);
  }

  public void comparedAgainst(LocalDate min, LocalDate minmax, LocalDate max) {
    DateRangeDto dto = DateRangeDto.builder().min(min).minmax(minmax).max(max).build();

    Set<ConstraintViolation<DateRangeDto>> result = validator.validate(dto);

    if (result.isEmpty()) {
      testContext.put(RESULT, true);
    } else {
      testContext.put(RESULT, false);
      testContext.put(MESSAGE, result.stream().findFirst().get().getMessage());
    }
  }

  @Then("I get {string} as a result")
  public void iGetAsAResult(String expectedResult) {
    Boolean result = testContext.get(RESULT);

    Assert.assertEquals(Boolean.valueOf(expectedResult), result);
  }

  @Then("the message in the validation is {string}")
  public void theMessageInTheValidationIs(String expectedMessage) {
    String message = testContext.get(MESSAGE);

    Assert.assertEquals(expectedMessage, message);
  }

  @Then("the message in the validation is null")
  public void theMessageInTheValidationIsNull() {
    String message = testContext.get(MESSAGE);

    Assert.assertNull(message);
  }
}
