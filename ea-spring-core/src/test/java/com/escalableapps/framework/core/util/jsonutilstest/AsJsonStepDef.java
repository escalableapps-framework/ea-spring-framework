package com.escalableapps.framework.core.util.jsonutilstest;

import static com.escalableapps.framework.core.util.JsonUtilsTest.JsonUtilsTestContext.EXCEPTION;
import static com.escalableapps.framework.core.util.JsonUtilsTest.JsonUtilsTestContext.JSON;
import static com.escalableapps.framework.core.util.JsonUtilsTest.JsonUtilsTestContext.LIST;
import static com.escalableapps.framework.core.util.JsonUtilsTest.JsonUtilsTestContext.OBJECT;
import static com.escalableapps.framework.core.util.jsonutilstest.JsonUtilsStepDef.createEnumAB;
import static com.escalableapps.framework.core.util.jsonutilstest.JsonUtilsStepDef.createEnumLowerAB;
import static com.escalableapps.framework.core.util.jsonutilstest.JsonUtilsStepDef.createLocalDate;
import static com.escalableapps.framework.core.util.jsonutilstest.JsonUtilsStepDef.createLocalDateTime;
import static com.escalableapps.framework.core.util.jsonutilstest.JsonUtilsStepDef.createLocalTime;
import static com.escalableapps.framework.core.util.jsonutilstest.JsonUtilsStepDef.createOffsetDateTime;
import static com.escalableapps.framework.core.util.jsonutilstest.JsonUtilsStepDef.createYear;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.json.JSONException;
import org.junit.Assert;
import org.skyscreamer.jsonassert.JSONAssert;

import com.escalableapps.framework.core.util.JsonUtils;
import com.escalableapps.framework.core.util.JsonUtilsTest.CircularDto;
import com.escalableapps.framework.core.util.JsonUtilsTest.JsonUtilsDto;
import com.escalableapps.framework.core.util.JsonUtilsTest.JsonUtilsTestContext;
import com.escalableapps.framework.cucumber.model.TestContext;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class AsJsonStepDef {

  private final TestContext<JsonUtilsTestContext> testContext;

  public AsJsonStepDef(TestContext<JsonUtilsTestContext> testContext) {
    this.testContext = testContext;
  }

  @Given("an object with the values")
  public void anObjectWithTheValues(Map<String, String> values) {
    JsonUtilsDto object = buildDto(values);

    testContext.put(OBJECT, object);
  }

  @Given("a null object")
  public void aNullObject() {
    testContext.put(OBJECT, null);
  }

  @Given("a list of objects")
  public void aListOfObjects() {
    testContext.put(LIST, new ArrayList<JsonUtilsDto>());
  }

  @Given("a object at position {int} with the values")
  public void aObjectAtPositionWithTheValues(Integer position, Map<String, String> values) {
    JsonUtilsDto object = buildDto(values);

    List<JsonUtilsDto> list = testContext.get(LIST);
    list.add(position, object);
  }

  private JsonUtilsDto buildDto(Map<String, String> values) {
    JsonUtilsDto object = new JsonUtilsDto();
    object.setTypeString(values.get("typeString"));
    object.setTypeInteger(NumberUtils.createInteger(values.get("typeInteger")));
    object.setTypeLong(NumberUtils.createLong(values.get("typeLong")));
    object.setTypeBigInteger(NumberUtils.createBigInteger(values.get("typeBigInteger")));
    object.setTypeFloat(NumberUtils.createFloat(values.get("typeFloat")));
    object.setTypeDouble(NumberUtils.createDouble(values.get("typeDouble")));
    object.setTypeBigDecimal(NumberUtils.createBigDecimal(values.get("typeBigDecimal")));
    object.setTypeBoolean(BooleanUtils.toBooleanObject(values.get("typeBoolean")));
    object.setTypeOffsetDateTime(createOffsetDateTime(values.get("typeOffsetDateTime")));
    object.setTypeLocalDateTime(createLocalDateTime(values.get("typeLocalDateTime")));
    object.setTypeLocalDate(createLocalDate(values.get("typeLocalDate")));
    object.setTypeLocalTime(createLocalTime(values.get("typeLocalTime")));
    object.setTypeYear(createYear(values.get("typeYear")));
    object.setTypeEnumAB(createEnumAB(values.get("typeEnumAB")));
    object.setTypeEnumLowerAB(createEnumLowerAB(values.get("typeEnumLowerAB")));
    return object;
  }

  @Given("an empty list")
  public void anEmptyList() {
    testContext.put(LIST, new ArrayList<JsonUtilsDto>());
  }

  @Given("a null list")
  public void aNullList() {
    testContext.put(LIST, null);
  }

  @Given("given an object that one of its properties refers to itself")
  public void givenAnObjectThatOneOfItsPropertiesRefersToItself() {
    CircularDto object = new CircularDto();
    object.setReference(object);

    testContext.put(OBJECT, object);
  }

  @When("convert to json")
  public void convertToJson() {
    Object source = testContext.contains(OBJECT) ? testContext.get(OBJECT) : testContext.get(LIST);

    try {
      String json = JsonUtils.asJson(source);

      testContext.put(JSON, json);
    } catch (Exception exception) {
      testContext.put(EXCEPTION, exception);
    }
  }

  @When("convert to pretty json")
  public void convertToPrettyJson() {
    Object source = testContext.contains(OBJECT) ? testContext.get(OBJECT) : testContext.get(LIST);

    try {
      String json = JsonUtils.asPrettyJson(source);

      testContext.put(JSON, json);
    } catch (Exception exception) {
      testContext.put(EXCEPTION, exception);
    }
  }

  @When("convert to json including nulls")
  public void convertToJsonIncludingNulls() {
    Object source = testContext.contains(OBJECT) ? testContext.get(OBJECT) : testContext.get(LIST);

    try {
      String json = JsonUtils.asJsonIncludeNull(source);

      testContext.put(JSON, json);
    } catch (Exception exception) {
      testContext.put(EXCEPTION, exception);
    }
  }

  @When("convert to pretty json including nulls")
  public void convertToPrettyJsonIncludingNulls() {
    Object source = testContext.contains(OBJECT) ? testContext.get(OBJECT) : testContext.get(LIST);

    try {
      String json = JsonUtils.asPrettyJsonIncludeNull(source);

      testContext.put(JSON, json);
    } catch (Exception exception) {
      testContext.put(EXCEPTION, exception);
    }
  }

  @Then("I get a json")
  public void iGetAJson(String expectedJson) throws JSONException {
    String json = testContext.get(JSON);

    if ("null".equalsIgnoreCase(expectedJson.trim())) {
      Assert.assertEquals(expectedJson.trim(), json.trim());
    } else {
      JSONAssert.assertEquals(expectedJson, json, true);
    }
  }
}
