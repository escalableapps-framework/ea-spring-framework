package com.escalableapps.framework.core.model.vo.pagerequesttest;

import static com.escalableapps.framework.core.model.vo.PageRequestTest.PageRequestTestContext.EXCEPTION;
import static com.escalableapps.framework.core.model.vo.PageRequestTest.PageRequestTestContext.PAGE_NUMBER;
import static com.escalableapps.framework.core.model.vo.PageRequestTest.PageRequestTestContext.PAGE_REQUEST;
import static com.escalableapps.framework.core.model.vo.PageRequestTest.PageRequestTestContext.PAGE_SIZE;
import static com.escalableapps.framework.core.model.vo.PageRequestTest.PageRequestTestContext.SORT_CRITERIA;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.test.context.ContextConfiguration;

import com.escalableapps.framework.core.model.vo.PageRequest;
import com.escalableapps.framework.core.model.vo.PageRequest.SortRequest;
import com.escalableapps.framework.core.model.vo.PageRequestTest.PageRequestSortProperties;
import com.escalableapps.framework.core.model.vo.PageRequestTest.PageRequestTestContext;
import com.escalableapps.framework.core.util.JsonUtils;
import com.escalableapps.framework.cucumber.model.TestContext;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.spring.CucumberContextConfiguration;

@CucumberContextConfiguration
@ContextConfiguration
@EnableAutoConfiguration
public class PageRequestStepDef {

  private final TestContext<PageRequestTestContext> testContext;

  public PageRequestStepDef(TestContext<PageRequestTestContext> testContext) {
    this.testContext = testContext;
  }

  @Given("the default language {string}")
  public void theDefaultLanguage(String language) {
    Locale.setDefault(new Locale(language));
  }

  @Given("the page number {int}")
  public void thePageNumber(Integer pageNumber) {
    testContext.put(PAGE_NUMBER, pageNumber);
  }

  @Given("the page size {int}")
  public void thePageSize(Integer pageSize) {
    testContext.put(PAGE_SIZE, pageSize);
  }

  @Given("the sort criteria {string}")
  public void theSortCriteria(String properties) {
    List<String> propertyNames = JsonUtils.parseAsList(properties, String.class);
    List<SortRequest<PageRequestSortProperties>> sortCriteria = getSortCriteria(propertyNames);

    testContext.put(SORT_CRITERIA, sortCriteria);
  }

  private List<SortRequest<PageRequestSortProperties>> getSortCriteria(List<String> propertyNames) {

    return propertyNames.stream().map(property -> {
      if (property == null) {
        return null;
      }
      PageRequestSortProperties propertyEnum = PageRequestSortProperties.valueOf(property);
      return new SortRequest<>(propertyEnum);
    }).collect(Collectors.toList());
  }

  @SuppressWarnings("unchecked")
  @When("create the PageRequest")
  public void createThePageRequest() {
    Integer pageNumber = testContext.get(PAGE_NUMBER);
    Integer pageSize = testContext.get(PAGE_SIZE);
    List<SortRequest<PageRequestSortProperties>> sortCriteria = testContext.get(SORT_CRITERIA);

    try {
      PageRequest pageRequest;
      if (pageNumber == null || pageSize == null) {
        pageRequest = new PageRequest();
      } else if (sortCriteria == null) {
        pageRequest = new PageRequest(pageNumber, pageSize);
      } else {
        SortRequest<PageRequestSortProperties>[] array = sortCriteria.stream().toArray(SortRequest[]::new);
        pageRequest = new PageRequest(pageNumber, pageSize, array);
      }

      testContext.put(PAGE_REQUEST, pageRequest);
    } catch (Exception exception) {
      testContext.put(EXCEPTION, exception);
    }
  }

  @Then("the PageRequest has its offset with {int} value")
  public void thePageRequestHasItsOffsetWithValue(Integer expectedOffset) {
    PageRequest pageRequest = testContext.get(PAGE_REQUEST);

    Assert.assertEquals(expectedOffset.intValue(), pageRequest.getOffset());
  }

  @Then("the sortRequests contains this SortRequest objects {string}")
  public void theSortRequestsContainsThisSortRequestObjects(String expectedProperties) {
    List<String> expectedPropertyNames = JsonUtils.parseAsList(expectedProperties, String.class);

    PageRequest pageRequest = testContext.get(PAGE_REQUEST);
    List<String> propertyNames = pageRequest.getSortRequests().stream()
        .map(sortRequest -> sortRequest.getProperty().name()).collect(Collectors.toList());

    Assert.assertEquals(expectedPropertyNames, propertyNames);
  }

  @Then("I get an {string} with the message {string}")
  public void iGetAnWithTheMessage(String expectedExceptionClass, String expectedMessage) {
    Exception exception = testContext.get(EXCEPTION);

    Assert.assertNotNull(exception);
    Assert.assertEquals(expectedExceptionClass, exception.getClass().getSimpleName());
    Assert.assertEquals(expectedMessage, exception.getMessage());
  }
}
