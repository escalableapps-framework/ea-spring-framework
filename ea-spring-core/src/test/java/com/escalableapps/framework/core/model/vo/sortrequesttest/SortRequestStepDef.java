package com.escalableapps.framework.core.model.vo.sortrequesttest;

import static com.escalableapps.framework.core.model.vo.SortRequestTest.SortRequestTestContext.DIRECTION;
import static com.escalableapps.framework.core.model.vo.SortRequestTest.SortRequestTestContext.EXCEPTION;
import static com.escalableapps.framework.core.model.vo.SortRequestTest.SortRequestTestContext.PROPERTY;
import static com.escalableapps.framework.core.model.vo.SortRequestTest.SortRequestTestContext.SORT_REQUEST;

import java.util.Locale;

import org.junit.Assert;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.test.context.ContextConfiguration;

import com.escalableapps.framework.core.model.vo.PageRequest.Direction;
import com.escalableapps.framework.core.model.vo.PageRequest.SortRequest;
import com.escalableapps.framework.core.model.vo.SortRequestTest.SortRequestSortProperties;
import com.escalableapps.framework.core.model.vo.SortRequestTest.SortRequestTestContext;
import com.escalableapps.framework.cucumber.model.TestContext;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.spring.CucumberContextConfiguration;

@CucumberContextConfiguration
@ContextConfiguration
@EnableAutoConfiguration
public class SortRequestStepDef {

  private final TestContext<SortRequestTestContext> testContext;

  public SortRequestStepDef(TestContext<SortRequestTestContext> testContext) {
    this.testContext = testContext;
  }

  @Given("the default language {string}")
  public void theDefaultLanguage(String language) {
    Locale.setDefault(new Locale(language));
  }

  @Given("the a null sort property")
  public void theANullSortProperty() {
  }

  @Given("the sort property {string}")
  public void theSortProperty(String property) {
    testContext.put(PROPERTY, SortRequestSortProperties.valueOf(property));
  }

  @Given("the direction with {string} value")
  public void theDirectionWithValue(String direction) {
    testContext.put(DIRECTION, Direction.valueOf(direction));
  }

  @When("create the SortRequest")
  public void createTheSortRequest() {
    SortRequestSortProperties property = testContext.get(PROPERTY);
    Direction ascendant = testContext.get(DIRECTION);

    try {
      SortRequest<SortRequestSortProperties> sortRequest;
      if (ascendant == null) {
        sortRequest = new SortRequest<>(property);
      } else {
        sortRequest = new SortRequest<>(property, ascendant);
      }

      testContext.put(SORT_REQUEST, sortRequest);
    } catch (Exception exception) {
      testContext.put(EXCEPTION, exception);
    }
  }

  @Then("the SortRequest has its property with {string} value")
  public void theSortRequestHasItsPropertyWithValue(String expectedProperty) {
    SortRequest<SortRequestSortProperties> sortRequest = testContext.get(SORT_REQUEST);

    Assert.assertEquals(expectedProperty, sortRequest.getProperty().name());
  }

  @Then("the SortRequest has its direction with {string} value")
  public void theSortRequestHasItsDirectionWithValue(String expectedDirection) {
    SortRequest<SortRequestSortProperties> sortRequest = testContext.get(SORT_REQUEST);

    Assert.assertEquals(Direction.valueOf(expectedDirection), sortRequest.getDirection());
  }

  @Then("I get an {string} with the message {string}")
  public void iGetAnWithTheMessage(String expectedExceptionClass, String expectedMessage) {
    Exception exception = testContext.get(EXCEPTION);

    Assert.assertNotNull(exception);
    Assert.assertEquals(expectedExceptionClass, exception.getClass().getSimpleName());
    Assert.assertEquals(expectedMessage, exception.getMessage());
  }
}
