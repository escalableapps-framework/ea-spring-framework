package com.escalableapps.framework.core.model.vo.pageresponsetest;

import static com.escalableapps.framework.core.model.vo.PageResponseTest.PageResponseTestContext.EXCEPTION;
import static com.escalableapps.framework.core.model.vo.PageResponseTest.PageResponseTestContext.PAGE_NUMBER;
import static com.escalableapps.framework.core.model.vo.PageResponseTest.PageResponseTestContext.PAGE_RESPONSE;
import static com.escalableapps.framework.core.model.vo.PageResponseTest.PageResponseTestContext.PAGE_SIZE;
import static com.escalableapps.framework.core.model.vo.PageResponseTest.PageResponseTestContext.RESULTS;
import static com.escalableapps.framework.core.model.vo.PageResponseTest.PageResponseTestContext.TOTAL_ELEMENTS;

import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.test.context.ContextConfiguration;

import com.escalableapps.framework.core.model.vo.PageResponse;
import com.escalableapps.framework.core.model.vo.PageResponseTest.PageResponseTestContext;
import com.escalableapps.framework.core.util.JsonUtils;
import com.escalableapps.framework.cucumber.model.TestContext;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.spring.CucumberContextConfiguration;

@CucumberContextConfiguration
@ContextConfiguration
@EnableAutoConfiguration
public class PageResponseStepDef {

  private final TestContext<PageResponseTestContext> testContext;

  public PageResponseStepDef(TestContext<PageResponseTestContext> testContext) {
    this.testContext = testContext;
  }

  @Given("the default language {string}")
  public void theDefaultLanguage(String language) {
    Locale.setDefault(new Locale(language));
  }

  @Given("given a null list of results")
  public void givenANullListOfResults() {
  }

  @Given("given the list of results {string}")
  public void givenTheListOfResults(String jsonResults) {
    List<Integer> results = JsonUtils.parseAsList(jsonResults, Integer.class);
    testContext.put(RESULTS, results);
  }

  @Given("a total number of items of {int}")
  public void aTotalNumberOfItemsOf(Integer totalElements) {
    testContext.put(TOTAL_ELEMENTS, totalElements.longValue());
  }

  @Given("a null PageRequest")
  public void aNullPageRequest() {
  }

  @Given("a PageRequest with a pageNumber equal to {int} and a pageSize equal to {int}")
  public void aPageRequestWithAPageNumberEqualToAndAPageSizeEqualTo(Integer pageNumber, Integer pageSize) {
    testContext.put(PAGE_NUMBER, pageNumber);
    testContext.put(PAGE_SIZE, pageSize);
  }

  @When("create the PageResponse using pageNumber and pageSize")
  public void createThePageResponseUsingPageNumberAndPageSize() {
    List<Integer> results = testContext.get(RESULTS);
    Long totalElements = testContext.get(TOTAL_ELEMENTS);
    Integer pageNumber = testContext.get(PAGE_NUMBER);
    Integer pageSize = testContext.get(PAGE_SIZE);

    try {
      PageResponse<Integer> pageResponse = new PageResponse<>(results, totalElements, pageNumber, pageSize);

      testContext.put(PAGE_RESPONSE, pageResponse);
    } catch (Exception exception) {
      testContext.put(EXCEPTION, exception);
    }
  }

  @Then("the results property has {string}")
  public void theResultsPropertyHas(String expectedJsonResults) {
    List<Integer> expectedResults = JsonUtils.parseAsList(expectedJsonResults, Integer.class);
    PageResponse<Integer> pageResponse = testContext.get(PAGE_RESPONSE);

    Assert.assertEquals(expectedResults, pageResponse.getResults());
  }

  @Then("the totalElements property has {int}")
  public void theTotalElementsPropertyHas(Integer expectedTotalElements) {
    PageResponse<Integer> pageResponse = testContext.get(PAGE_RESPONSE);

    Assert.assertEquals(expectedTotalElements.longValue(), pageResponse.getTotalElements());
  }

  @Then("the pageNumber property has {int}")
  public void thePageNumberPropertyHas(Integer expectedPageNumber) {
    PageResponse<Integer> pageResponse = testContext.get(PAGE_RESPONSE);

    Assert.assertEquals(expectedPageNumber.intValue(), pageResponse.getPageNumber());
  }

  @Then("the pageSize property has {int}")
  public void thePageSizePropertyHas(Integer expectedPageSize) {
    PageResponse<Integer> pageResponse = testContext.get(PAGE_RESPONSE);

    Assert.assertEquals(expectedPageSize.intValue(), pageResponse.getPageSize());
  }

  @Then("the totalPages property has {int}")
  public void theTotalPagesPropertyHas(Integer expectedTotalPages) {
    PageResponse<Integer> pageResponse = testContext.get(PAGE_RESPONSE);

    Assert.assertEquals(expectedTotalPages.intValue(), pageResponse.getTotalPages());
  }

  @Then("the first property is {string}")
  public void theFirstPropertyIs(String expectedFirst) {
    PageResponse<Integer> pageResponse = testContext.get(PAGE_RESPONSE);

    Assert.assertEquals(Boolean.valueOf(expectedFirst), pageResponse.isFirst());
  }

  @Then("the last property is {string}")
  public void theLastPropertyIs(String expectedLast) {
    PageResponse<Integer> pageResponse = testContext.get(PAGE_RESPONSE);

    Assert.assertEquals(Boolean.valueOf(expectedLast), pageResponse.isLast());
  }

  @Then("I get an {string} with the message {string}")
  public void iGetAnWithTheMessage(String expectedExceptionClass, String expectedMessage) {
    Exception exception = testContext.get(EXCEPTION);

    Assert.assertNotNull(exception);
    Assert.assertEquals(expectedExceptionClass, exception.getClass().getSimpleName());
    Assert.assertEquals(expectedMessage, exception.getMessage());
  }
}
