package com.escalableapps.framework.core.model.exception;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions( //
    features = "classpath:core/model/exception/notfoundexceptiontest", //
    glue = {"classpath:com.escalableapps.framework.core.model.exception.notfoundexceptiontest"} //
)
public class NotFoundExceptionTest {

  public static enum NotFoundExceptionTestContext {
    PATTERN, MESSAGE, FIRST_ARGUMENT, SECOND_ARGUMENT, CAUSE, EXCEPTION
  }
}
