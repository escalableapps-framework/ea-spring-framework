package com.escalableapps.framework.core.util.jsonutilstest;

import static com.escalableapps.framework.core.util.JsonUtilsTest.JsonUtilsTestContext.EXCEPTION;
import static com.escalableapps.framework.core.util.JsonUtilsTest.JsonUtilsTestContext.JSON;
import static com.escalableapps.framework.core.util.JsonUtilsTest.JsonUtilsTestContext.LIST;
import static com.escalableapps.framework.core.util.JsonUtilsTest.JsonUtilsTestContext.OBJECT;
import static com.escalableapps.framework.core.util.jsonutilstest.JsonUtilsStepDef.createEnumAB;
import static com.escalableapps.framework.core.util.jsonutilstest.JsonUtilsStepDef.createEnumLowerAB;
import static com.escalableapps.framework.core.util.jsonutilstest.JsonUtilsStepDef.createLocalDate;
import static com.escalableapps.framework.core.util.jsonutilstest.JsonUtilsStepDef.createLocalDateTime;
import static com.escalableapps.framework.core.util.jsonutilstest.JsonUtilsStepDef.createLocalTime;
import static com.escalableapps.framework.core.util.jsonutilstest.JsonUtilsStepDef.createOffsetDateTime;
import static com.escalableapps.framework.core.util.jsonutilstest.JsonUtilsStepDef.createYear;
import static org.apache.commons.lang3.BooleanUtils.toBooleanObject;
import static org.apache.commons.lang3.math.NumberUtils.createBigDecimal;
import static org.apache.commons.lang3.math.NumberUtils.createBigInteger;
import static org.apache.commons.lang3.math.NumberUtils.createDouble;
import static org.apache.commons.lang3.math.NumberUtils.createFloat;
import static org.apache.commons.lang3.math.NumberUtils.createInteger;
import static org.apache.commons.lang3.math.NumberUtils.createLong;

import java.util.List;
import java.util.Map;

import org.junit.Assert;

import com.escalableapps.framework.core.util.JsonUtils;
import com.escalableapps.framework.core.util.JsonUtilsTest.JsonUtilsDto;
import com.escalableapps.framework.core.util.JsonUtilsTest.JsonUtilsTestContext;
import com.escalableapps.framework.cucumber.model.TestContext;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class ParseStepDef {

  private final TestContext<JsonUtilsTestContext> testContext;

  public ParseStepDef(TestContext<JsonUtilsTestContext> testContext) {
    this.testContext = testContext;
  }

  @Given("a null json")
  public void aNullJson() {
  }

  @Given("a json")
  public void aJson(String json) {
    testContext.put(JSON, json);
  }

  @When("parse the json as a type of null")
  public void parseTheJsonAsATypeOfNull() {
    parseTheJsonAsATypeOf(null);
  }

  @When("parse the json as a type of {string}")
  public void parseTheJsonAsATypeOf(String type) {
    String json = testContext.get(JSON);

    try {
      JsonUtilsDto object = JsonUtils.parseAsType(json, type == null ? null : JsonUtilsDto.class);

      testContext.put(OBJECT, object);
    } catch (Exception exception) {
      testContext.put(EXCEPTION, exception);
    }
  }

  @When("parse the json as a map with {string} key and null value")
  public void parseTheJsonAsAMapWithKeyAndNullValue(String keyClass) {
    parseTheJsonAsAMapWithKeyAndValue(keyClass, null);
  }

  @When("parse the json as a map with null key and {string} value")
  public void parseTheJsonAsAMapWithNullKeyAndValue(String valueClass) {
    parseTheJsonAsAMapWithKeyAndValue(null, valueClass);
  }

  @When("parse the json as a map with {string} key and {string} value")
  public void parseTheJsonAsAMapWithKeyAndValue(String keyClass, String valueClass) {
    String json = testContext.get(JSON);

    try {
      Map<String, Object> object = JsonUtils.parseAsMap(json, keyClass == null ? null : String.class,
          valueClass == null ? null : Object.class);

      testContext.put(OBJECT, object);
    } catch (Exception exception) {
      testContext.put(EXCEPTION, exception);
    }
  }

  @When("parse the json as a list of null")
  public void parseTheJsonAsAListOfNull() {
    parseTheJsonAsAListOf(null);
  }

  @When("parse the json as a list of {string}")
  public void parseTheJsonAsAListOf(String type) {
    String json = testContext.get(JSON);

    try {
      List<JsonUtilsDto> list = JsonUtils.parseAsList(json, type == null ? null : JsonUtilsDto.class);

      testContext.put(LIST, list);
    } catch (Exception exception) {
      testContext.put(EXCEPTION, exception);
    }
  }

  @Then("I get an object with the values")
  public void iGetAnObjectWithTheValues(Map<String, String> expectedValues) {
    JsonUtilsDto object = testContext.get(OBJECT);

    assertResult(expectedValues, object);
  }

  @Then("I get a map with the values")
  public void iGetAMapWithTheValues(Map<String, String> expectedValues) {
    Map<String, Object> object = testContext.get(OBJECT);

    Assert.assertEquals(expectedValues.get("typeString"), object.get("typeString"));
    Assert.assertEquals(createInteger(expectedValues.get("typeInteger")), object.get("typeInteger"));
    Assert.assertEquals(createLong(expectedValues.get("typeLong")), object.get("typeLong"));
    Assert.assertEquals(createBigInteger(expectedValues.get("typeBigInteger")), object.get("typeBigInteger"));
    Assert.assertEquals(createDouble(expectedValues.get("typeFloat")), object.get("typeFloat"));
    Assert.assertEquals(createDouble(expectedValues.get("typeDouble")), object.get("typeDouble"));
    Assert.assertEquals(createDouble(expectedValues.get("typeBigDecimal")), object.get("typeBigDecimal"));
    Assert.assertEquals(toBooleanObject(expectedValues.get("typeBoolean")), object.get("typeBoolean"));
    Assert.assertEquals(expectedValues.get("typeOffsetDateTime"), object.get("typeOffsetDateTime"));
    Assert.assertEquals(expectedValues.get("typeLocalDateTime"), object.get("typeLocalDateTime"));
    Assert.assertEquals(expectedValues.get("typeLocalDate"), object.get("typeLocalDate"));
    Assert.assertEquals(expectedValues.get("typeLocalTime"), object.get("typeLocalTime"));
    Assert.assertEquals(expectedValues.get("typeYear"), object.get("typeYear"));
    Assert.assertEquals(expectedValues.get("typeEnumAB"), object.get("typeEnumAB"));
    Assert.assertEquals(expectedValues.get("typeEnumLowerAB"), object.get("typeEnumLowerAB"));
  }

  @Then("I get a map with the classes")
  public void iGetAMapWithTheClasses(Map<String, String> expectedValues) {
    Map<String, Object> object = testContext.get(OBJECT);

    Assert.assertEquals(expectedValues.get("typeString"), object.get("typeString").getClass().getSimpleName());
    Assert.assertEquals(expectedValues.get("typeInteger"), object.get("typeInteger").getClass().getSimpleName());
    Assert.assertEquals(expectedValues.get("typeLong"), object.get("typeLong").getClass().getSimpleName());
    Assert.assertEquals(expectedValues.get("typeBigInteger"), object.get("typeBigInteger").getClass().getSimpleName());
    Assert.assertEquals(expectedValues.get("typeFloat"), object.get("typeFloat").getClass().getSimpleName());
    Assert.assertEquals(expectedValues.get("typeDouble"), object.get("typeDouble").getClass().getSimpleName());
    Assert.assertEquals(expectedValues.get("typeBigDecimal"), object.get("typeBigDecimal").getClass().getSimpleName());
    Assert.assertEquals(expectedValues.get("typeBoolean"), object.get("typeBoolean").getClass().getSimpleName());
    Assert.assertEquals(expectedValues.get("typeOffsetDateTime"),
        object.get("typeOffsetDateTime").getClass().getSimpleName());
    Assert.assertEquals(expectedValues.get("typeLocalDateTime"),
        object.get("typeLocalDateTime").getClass().getSimpleName());
    Assert.assertEquals(expectedValues.get("typeLocalDate"), object.get("typeLocalDate").getClass().getSimpleName());
    Assert.assertEquals(expectedValues.get("typeLocalTime"), object.get("typeLocalTime").getClass().getSimpleName());
    Assert.assertEquals(expectedValues.get("typeYear"), object.get("typeYear").getClass().getSimpleName());
    Assert.assertEquals(expectedValues.get("typeEnumAB"), object.get("typeEnumAB").getClass().getSimpleName());
    Assert.assertEquals(expectedValues.get("typeEnumLowerAB"),
        object.get("typeEnumLowerAB").getClass().getSimpleName());
  }

  @Then("I get a list with an object at position {int} with the values")
  public void iGetAListWithAnObjectAtPositionWithTheValues(Integer position, Map<String, String> expectedValues) {
    List<JsonUtilsDto> list = testContext.get(LIST);
    JsonUtilsDto object = list.get(position);

    assertResult(expectedValues, object);
  }

  private void assertResult(Map<String, String> expectedValues, JsonUtilsDto object) {
    Assert.assertEquals(expectedValues.get("typeString"), object.getTypeString());
    Assert.assertEquals(createInteger(expectedValues.get("typeInteger")), object.getTypeInteger());
    Assert.assertEquals(createLong(expectedValues.get("typeLong")), object.getTypeLong());
    Assert.assertEquals(createBigInteger(expectedValues.get("typeBigInteger")), object.getTypeBigInteger());
    Assert.assertEquals(createFloat(expectedValues.get("typeFloat")), object.getTypeFloat());
    Assert.assertEquals(createDouble(expectedValues.get("typeDouble")), object.getTypeDouble());
    Assert.assertEquals(createBigDecimal(expectedValues.get("typeBigDecimal")), object.getTypeBigDecimal());
    Assert.assertEquals(toBooleanObject(expectedValues.get("typeBoolean")), object.getTypeBoolean());
    Assert.assertEquals(createOffsetDateTime(expectedValues.get("typeOffsetDateTime")), object.getTypeOffsetDateTime());
    Assert.assertEquals(createLocalDateTime(expectedValues.get("typeLocalDateTime")), object.getTypeLocalDateTime());
    Assert.assertEquals(createLocalDate(expectedValues.get("typeLocalDate")), object.getTypeLocalDate());
    Assert.assertEquals(createLocalTime(expectedValues.get("typeLocalTime")), object.getTypeLocalTime());
    Assert.assertEquals(createYear(expectedValues.get("typeYear")), object.getTypeYear());
    Assert.assertEquals(createEnumAB(expectedValues.get("typeEnumAB")), object.getTypeEnumAB());
    Assert.assertEquals(createEnumLowerAB(expectedValues.get("typeEnumLowerAB")), object.getTypeEnumLowerAB());
  }

  @Then("I get a null value")
  public void iGetANullValue() {
    JsonUtilsDto object = testContext.get(OBJECT);

    Assert.assertNull(object);
  }

  @Then("I get a empty map")
  public void iGetAEmptyMap() {
    Map<String, Object> object = testContext.get(OBJECT);

    Assert.assertNotNull(object);
    Assert.assertTrue(object.isEmpty());
  }

  @Then("I get an empty list")
  public void iGetAnEmptyList() {
    List<JsonUtilsDto> list = testContext.get(LIST);

    Assert.assertNotNull(list);
    Assert.assertTrue(list.isEmpty());
  }
}
