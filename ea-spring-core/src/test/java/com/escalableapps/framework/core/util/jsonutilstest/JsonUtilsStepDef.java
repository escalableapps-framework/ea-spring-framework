package com.escalableapps.framework.core.util.jsonutilstest;

import static com.escalableapps.framework.core.util.JsonUtilsTest.JsonUtilsTestContext.EXCEPTION;
import static org.apache.commons.lang3.StringUtils.contains;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.Year;
import java.time.ZoneOffset;
import java.util.Locale;
import java.util.TimeZone;

import org.junit.Assert;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.test.context.ContextConfiguration;

import com.escalableapps.framework.core.util.JsonUtilsTest.EnumAB;
import com.escalableapps.framework.core.util.JsonUtilsTest.EnumLowerAB;
import com.escalableapps.framework.core.util.JsonUtilsTest.JsonUtilsTestContext;
import com.escalableapps.framework.cucumber.model.TestContext;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.spring.CucumberContextConfiguration;

@CucumberContextConfiguration
@ContextConfiguration
@EnableAutoConfiguration
public class JsonUtilsStepDef {

  private final TestContext<JsonUtilsTestContext> testContext;

  public JsonUtilsStepDef(TestContext<JsonUtilsTestContext> testContext) {
    this.testContext = testContext;
  }

  public static EnumAB createEnumAB(String value) {
    if (value == null) {
      return null;
    }
    return EnumAB.valueOf(value);
  }

  public static EnumLowerAB createEnumLowerAB(String value) {
    if (value == null) {
      return null;
    }
    return EnumLowerAB.valueOf(value);
  }

  public static Year createYear(String value) {
    if (value == null) {
      return null;
    }
    return Year.parse(value);
  }

  public static LocalTime createLocalTime(String value) {
    if (value == null) {
      return null;
    }
    LocalTime localTime;
    String[] isoArray = value.split(" ");
    localTime = LocalTime.of( //
        Integer.parseInt(isoArray[0]), //
        Integer.parseInt(isoArray[1]), //
        Integer.parseInt(isoArray[2]), //
        Integer.parseInt(isoArray[3]) * 1000000 //
    );
    return localTime;
  }

  public static LocalDate createLocalDate(String value) {
    if (value == null) {
      return null;
    }
    LocalDate localDate;
    String[] isoArray = value.split(" ");
    localDate = LocalDate.of( //
        Integer.parseInt(isoArray[0]), //
        Integer.parseInt(isoArray[1]), //
        Integer.parseInt(isoArray[2]) //
    );
    return localDate;
  }

  public static OffsetDateTime createOffsetDateTime(String value) {
    if (value == null) {
      return null;
    }
    OffsetDateTime offsetDateTime;
    String[] isoArray = value.split(" ");
    offsetDateTime = OffsetDateTime.of( //
        Integer.parseInt(isoArray[0]), //
        Integer.parseInt(isoArray[1]), //
        Integer.parseInt(isoArray[2]), //
        Integer.parseInt(isoArray[3]), //
        Integer.parseInt(isoArray[4]), //
        Integer.parseInt(isoArray[5]), //
        Integer.parseInt(isoArray[6]) * 1000000, //
        getTimeZone(isoArray[7]) //
    );
    return offsetDateTime;
  }

  public static LocalDateTime createLocalDateTime(String value) {
    if (value == null) {
      return null;
    }
    LocalDateTime offsetDateTime;
    String[] isoArray = value.split(" ");
    offsetDateTime = LocalDateTime.of( //
        Integer.parseInt(isoArray[0]), //
        Integer.parseInt(isoArray[1]), //
        Integer.parseInt(isoArray[2]), //
        Integer.parseInt(isoArray[3]), //
        Integer.parseInt(isoArray[4]), //
        Integer.parseInt(isoArray[5]), //
        Integer.parseInt(isoArray[6]) * 1000000 //
    );
    return offsetDateTime;
  }

  private static ZoneOffset getTimeZone(String offsetId) {
    return ZoneOffset.of(offsetId);
  }

  @Given("the default timezone {string}")
  public void theDefaultTimezone(String timezone) {
    System.setProperty("user.timezone", timezone);
    TimeZone.setDefault(TimeZone.getTimeZone(timezone));
  }

  @Given("the default language {string}")
  public void theDefaultLanguage(String language) {
    Locale.setDefault(new Locale(language));
  }

  @Then("I get an {string} with the message {string}")
  public void iGetAnWithTheMessage(String expectedExceptionClass, String expectedMessage) {
    Exception exception = testContext.get(EXCEPTION);

    Assert.assertNotNull(exception);
    Assert.assertEquals(expectedExceptionClass, exception.getClass().getSimpleName());
    Assert.assertEquals(expectedMessage, exception.getMessage());
  }

  @Then("I get an {string} contains the message {string}")
  public void iGetAnContainsTheMessage(String expectedExceptionClass, String expectedMessage) {
    Exception exception = testContext.get(EXCEPTION);

    Assert.assertNotNull(exception);
    Assert.assertEquals(expectedExceptionClass, exception.getClass().getSimpleName());
    Assert.assertTrue(contains(exception.getMessage(), expectedMessage));
  }
}
