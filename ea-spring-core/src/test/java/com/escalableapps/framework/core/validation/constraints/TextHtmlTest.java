package com.escalableapps.framework.core.validation.constraints;

import static com.escalableapps.framework.core.validation.constraints.TextHtml.TextType.HTML_BASIC;
import static com.escalableapps.framework.core.validation.constraints.TextHtml.TextType.HTML_DATA_IMAGES;
import static com.escalableapps.framework.core.validation.constraints.TextHtml.TextType.HTML_FULL;
import static com.escalableapps.framework.core.validation.constraints.TextHtml.TextType.HTML_IMAGES;
import static com.escalableapps.framework.core.validation.constraints.TextHtml.TextType.HTML_SIMPLE;
import static com.escalableapps.framework.core.validation.constraints.TextHtml.TextType.TEXT_PLAIN;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import lombok.Builder;
import lombok.Data;

@RunWith(Cucumber.class)
@CucumberOptions( //
    features = "classpath:core/validation/constraints/texthtmltest", //
    glue = "classpath:com.escalableapps.framework.core.validation.constraints.texthtmltest" //
)
public class TextHtmlTest {

  public static enum TextHtmlTestContext {
    TEXT, RESULT, MESSAGE
  }

  public interface HtmlBasicGroup {
  }

  public interface HtmlFullGroup {
  }

  public interface HtmlImagesDataGroup {
  }

  public interface HtmlImagesGroup {
  }

  public interface HtmlSimpleGroup {
  }

  public interface TextPlainGroup {
  }

  @Builder
  @Data
  public static class TextHtmlDto {

    @TextHtml(value = TEXT_PLAIN, groups = TextPlainGroup.class)
    @TextHtml(value = HTML_SIMPLE, groups = HtmlSimpleGroup.class)
    @TextHtml(value = HTML_BASIC, groups = HtmlBasicGroup.class)
    @TextHtml(value = HTML_IMAGES, groups = HtmlImagesGroup.class)
    @TextHtml(value = HTML_DATA_IMAGES, groups = HtmlImagesDataGroup.class)
    @TextHtml(value = HTML_FULL, groups = HtmlFullGroup.class)
    private String text;

  }
}
