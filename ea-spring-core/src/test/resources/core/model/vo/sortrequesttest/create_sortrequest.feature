Feature: Create an sort request object
  In order to obtain a result ordered by some criteria
  As a backend developer
  I want to create a sort request to indicate the ordering property and if it is ascending (ASC) or descendant (DESC)

  Background: 
    Given the default language 'en'

  Scenario: Create a sort request only with the sort property
    Given the sort property 'PROPERTY_NAME'
    When create the SortRequest
    Then the SortRequest has its property with 'PROPERTY_NAME' value
    And the SortRequest has its direction with 'ASC' value

  Scenario Outline: Create a sort request with sort property and the direction value is <direction>
    Given the sort property 'PROPERTY_NAME'
    And the direction with <direction> value
    When create the SortRequest
    Then the SortRequest has its property with 'PROPERTY_NAME' value
    And the SortRequest has its direction with <direction> value

    Examples: 
      | direction |
      | 'ASC'     |
      | 'DESC'    |

  Scenario: Create a sort request with null sort property, I get an error
    Given the a null sort property
    When create the SortRequest
    Then I get an 'ConstraintViolationException' with the message 'property: must not be null'
