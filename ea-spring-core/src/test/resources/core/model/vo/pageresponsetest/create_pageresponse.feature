Feature: Create a page response object
  In order to obtain a partial result
  As a backend developer
  I want to create a page result to get results by page and with the desired page size

  Background: 
    Given the default language 'en'

  Scenario Outline: Create a page response number <page number> of a query <scenario case> with <total elements> elements
    Given given the list of results <results>
    And a total number of items of <total elements>
    And a PageRequest with a pageNumber equal to <page number> and a pageSize equal to <page size>
    When create the PageResponse using pageNumber and pageSize
    Then the results property has <results>
    And the totalElements property has <total elements>
    And the pageNumber property has <page number>
    And the pageSize property has <page size>
    And the totalPages property has <total pages>
    And the first property is <first>
    And the last property is <last>

    Examples: 
      | scenario case       | results        | page number | total elements | page size | total pages | first   | last    |
      | without results     | '[]'           |           0 |              0 |         5 |           0 | 'true'  | 'true'  |
      | without results     | '[]'           |           1 |              0 |         5 |           0 | 'true'  | 'true'  |
      | with 1 results page | '[1,2,3,4]'    |           0 |              4 |         5 |           1 | 'true'  | 'true'  |
      | with 1 results page | '[]'           |           1 |              4 |         5 |           1 | 'false' | 'true'  |
      | with 1 results page | '[1,2,3,4,5]'  |           0 |              5 |         5 |           1 | 'true'  | 'true'  |
      | with 1 results page | '[]'           |           1 |              5 |         5 |           1 | 'false' | 'true'  |
      | with 2 results page | '[1,2,3,4,5]'  |           0 |              6 |         5 |           2 | 'true'  | 'false' |
      | with 2 results page | '[6]'          |           1 |              6 |         5 |           2 | 'false' | 'true'  |
      | with 2 results page | '[]'           |           2 |              6 |         5 |           2 | 'false' | 'true'  |
      | with 2 results page | '[1,2,3,4,5]'  |           0 |              9 |         5 |           2 | 'true'  | 'false' |
      | with 2 results page | '[6,7,8,9]'    |           1 |              9 |         5 |           2 | 'false' | 'true'  |
      | with 2 results page | '[]'           |           2 |              9 |         5 |           2 | 'false' | 'true'  |
      | with 2 results page | '[1,2,3,4,5]'  |           0 |             10 |         5 |           2 | 'true'  | 'false' |
      | with 2 results page | '[6,7,8,9,10]' |           1 |             10 |         5 |           2 | 'false' | 'true'  |
      | with 2 results page | '[]'           |           2 |             10 |         5 |           2 | 'false' | 'true'  |
      | with 3 results page | '[1,2,3,4,5]'  |           0 |             11 |         5 |           3 | 'true'  | 'false' |
      | with 3 results page | '[6,7,8,9,10]' |           1 |             11 |         5 |           3 | 'false' | 'false' |
      | with 3 results page | '[11]'         |           2 |             11 |         5 |           3 | 'false' | 'true'  |
      | with 3 results page | '[]'           |           3 |             11 |         5 |           3 | 'false' | 'true'  |

  Scenario: Create a page response with null results object, I get an error
    Given given a null list of results
    And a total number of items of 0
    And a PageRequest with a pageNumber equal to 0 and a pageSize equal to 10
    When create the PageResponse using pageNumber and pageSize
    Then I get an 'ConstraintViolationException' with the message 'results: must not be null'

  Scenario: Create a page response with a total number of items less than zero, I get an error
    Given given the list of results '[]'
    And a total number of items of -1
    And a PageRequest with a pageNumber equal to 0 and a pageSize equal to 10
    When create the PageResponse using pageNumber and pageSize
    Then I get an 'ConstraintViolationException' with the message 'totalElements: must be greater than or equal to 0'

  Scenario: Create a page response with a page number less than zero, I get an error
    Given given the list of results '[]'
    And a total number of items of 0
    And a PageRequest with a pageNumber equal to -1 and a pageSize equal to 10
    When create the PageResponse using pageNumber and pageSize
    Then I get an 'ConstraintViolationException' with the message 'pageNumber: must be greater than or equal to 0'

  Scenario: Create a page response with a page size less than one, I get an error
    Given given the list of results '[]'
    And a total number of items of 0
    And a PageRequest with a pageNumber equal to 0 and a pageSize equal to 0
    When create the PageResponse using pageNumber and pageSize
    Then I get an 'ConstraintViolationException' with the message 'pageSize: must be greater than or equal to 1'
