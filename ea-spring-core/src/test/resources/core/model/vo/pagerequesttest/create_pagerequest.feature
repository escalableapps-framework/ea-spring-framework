Feature: Create an page request object
  In order to obtain a partial result
  As a backend developer
  I want to create a page request to indicate the number of the results page and the desired page size, optionally I could indicate some sorting criteria

  Background: 
    Given the default language 'en'

  Scenario: Create a page request without arguments
    When create the PageRequest
    Then the PageRequest has its offset with 0 value

  Scenario Outline: Create a page request only with the page number <page number> and page size <page size>
    Given the page number <page number>
    And the page size <page size>
    When create the PageRequest
    Then the PageRequest has its offset with <offset> value

    Examples: 
      | page number | page size | offset |
      |           0 |        10 |      0 |
      |           1 |        10 |     10 |
      |           2 |        10 |     20 |

  Scenario Outline: Create a page request with with the page number <page number>, page size <page size> and several sort criteria <sort criteria>
    Given the page number <page number>
    And the page size <page size>
    And the sort criteria <sort criteria>
    When create the PageRequest
    Then the PageRequest has its offset with <offset> value
    And the sortRequests contains this SortRequest objects <sort criteria>

    Examples: 
      | page number | page size | offset | sort criteria                            |
      |           0 |        10 |      0 | '["PROPERTY_NAME_1"]'                    |
      |           1 |        10 |     10 | '["PROPERTY_NAME_1", "PROPERTY_NAME_2"]' |
      |           2 |        10 |     20 | '["PROPERTY_NAME_1", "PROPERTY_NAME_2"]' |

  Scenario: Create a page request with a page number less than zero,  I get an error
    Given the page number -1
    And the page size 10
    When create the PageRequest
    Then I get an 'ConstraintViolationException' with the message 'pageNumber: must be greater than or equal to 0'

  Scenario: Create a page request with a page size less than one,  I get an error
    Given the page number 0
    And the page size 0
    When create the PageRequest
    Then I get an 'ConstraintViolationException' with the message 'pageSize: must be greater than or equal to 1'

  Scenario: Create a page request with a sort criteria contains a null value,  I get an error
    Given the page number 0
    And the page size 10
    And the sort criteria '[null]'
    When create the PageRequest
    Then I get an 'ConstraintViolationException' with the message 'sortRequests[0].<list element>: must not be null'
