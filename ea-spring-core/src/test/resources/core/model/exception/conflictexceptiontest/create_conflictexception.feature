Feature: Create a ConflictException with a customized message
  In order to throw an exception with a customized message
  As a backend developer
  I want to have the facility to create an exception with a pattern and substitution arguments

  Scenario: Exception is created without a message and without arguments
    When create a ConflictException
    Then the message is null

  Scenario: Exception is created with a message and without arguments
    Given the pattern-message 'This is a message'
    When create a ConflictException
    Then the message is 'This is a message'

  Scenario: Exception is created with a message and with an argument
    Given the pattern-message 'This is a message with: argument=%s'
    And argument is 'argumentValue'
    When create a ConflictException
    Then the message is 'This is a message with: argument=argumentValue'

  Scenario: Exception is created with a message and with two arguments
    Given the pattern-message 'This is a message with firstArgument=%s and secondArgument=%s'
    And first argument is 'firstArgumentValue'
    And second argument is 'secondArgumentValue'
    When create a ConflictException
    Then the message is 'This is a message with firstArgument=firstArgumentValue and secondArgument=secondArgumentValue'

  Scenario: Exception is created with an exception cause
    Given a Exception with the message 'This is a message'
    When create a ConflictException
    Then the message is 'This is a message'
    And the exception cause is Exception
