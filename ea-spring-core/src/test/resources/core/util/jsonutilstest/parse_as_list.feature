Feature: Parse a json to list
  In order to retrieve the information contained in a json
  As a backend developer
  I want a component that creates a list of objects from a json
  
  Rule: The conversion between data types is given by the following table
  
  | json     | java           |
  |----------|----------------|
  | string   | String         |
  | number   | Integer        |
  | number   | Long           |
  | number   | BigInteger     |
  | number   | Float          |
  | number   | Double         |
  | number   | BigDecimal     |
  | boolean  | Boolean        |
  | ISO-8601 | OffsetDateTime |
  | ISO-8601 | LocalDateTime  |
  | ISO-8601 | LocalDate      |
  | ISO-8601 | LocalTime      |
  | ISO-8601 | Year           |
  | string   | ENUM           |

  Background: 
    Given the default timezone 'America/Puerto_Rico'
    And the default language 'en'

  Scenario: From a json compatible with a target class I get an object
    Given a json
      """
      [
       {
        "typeString": "Hello world",
        "typeInteger": -2147483648,
        "typeLong": -9223372036854775808,
        "typeBigInteger": -9223372036854775809,
        "typeFloat": -1.2345678,
        "typeDouble": -1.2345678901234567,
        "typeBigDecimal": -1.2345678901234567890123456789,
        "typeBoolean": true,
        "typeOffsetDateTime": "2019-04-24T15:43:00.003Z",
        "typeLocalDateTime": "2019-01-24T15:43:00.003Z",
        "typeLocalDate": "2019-04-24",
        "typeLocalTime": "15:43:00.003",
        "typeYear": "2019",
        "typeEnumAB": "A",
        "typeEnumLowerAB": "a"
       },
       {
        "typeString": "Hello world",
        "typeInteger": 2147483647,
        "typeLong": 9223372036854775807,
        "typeBigInteger": 9223372036854775808,
        "typeFloat": 1.2345678,
        "typeDouble": 1.2345678901234567,
        "typeBigDecimal": 1.2345678901234567890123456789,
        "typeBoolean": true,
        "typeOffsetDateTime": "2019-04-24T15:43:00.003+05:00",
        "typeLocalDateTime": "2019-01-24T15:43:00.003-05:00",
        "typeLocalDate": "2019-04-24",
        "typeLocalTime": null,
        "typeYear": "2021",
        "typeEnumAB": "B",
        "typeEnumLowerAB": "b"
       }
      ]
      """
    When parse the json as a list of 'JsonUtilsDto'
    Then I get a list with an object at position 0 with the values
      | typeString         | Hello world                     |
      | typeInteger        |                     -2147483648 |
      | typeLong           |            -9223372036854775808 |
      | typeBigInteger     |            -9223372036854775809 |
      | typeFloat          |                      -1.2345678 |
      | typeDouble         |             -1.2345678901234567 |
      | typeBigDecimal     | -1.2345678901234567890123456789 |
      | typeBoolean        | true                            |
      | typeOffsetDateTime | 2019 04 24 15 43 00 003 Z       |
      | typeLocalDateTime  | 2019 01 24 11 43 00 003         |
      | typeLocalDate      | 2019 04 24                      |
      | typeLocalTime      | 15 43 00 003                    |
      | typeYear           |                            2019 |
      | typeEnumAB         | A                               |
      | typeEnumLowerAB    | A                               |
    And I get a list with an object at position 1 with the values
      | typeString         | Hello world                    |
      | typeInteger        |                     2147483647 |
      | typeLong           |            9223372036854775807 |
      | typeBigInteger     |            9223372036854775808 |
      | typeFloat          |                      1.2345678 |
      | typeDouble         |             1.2345678901234567 |
      | typeBigDecimal     | 1.2345678901234567890123456789 |
      | typeBoolean        | true                           |
      | typeOffsetDateTime | 2019 04 24 15 43 00 003 +05    |
      | typeLocalDateTime  | 2019 01 24 16 43 00 003        |
      | typeLocalDate      | 2019 04 24                     |
      | typeLocalTime      |                                |
      | typeYear           |                           2021 |
      | typeEnumAB         | B                              |
      | typeEnumLowerAB    | B                              |

  Scenario: From a json without properties I get an empty object
    Given a json
      """
      []
      """
    When parse the json as a list of 'JsonUtilsDto'
    Then I get an empty list

  Scenario: From a json representing a null I get a null
    Given a json
      """
      null
      """
    When parse the json as a list of 'JsonUtilsDto'
    Then I get a null value

  Scenario: From a json with some property not compatible with a target class I get an error
    Given a json
      """
      [
       {
        "typeLocalDate": "Hello world"
       }
      ]
      """
    When parse the json as a list of 'JsonUtilsDto'
    Then I get an 'EaFrameworkException' contains the message 'Cannot deserialize value of type'

  Scenario: From a malformed json I get an error
    Given a json
      """
      {
       "typeString": "Hello world
      }
      """
    When parse the json as a list of 'JsonUtilsDto'
    Then I get an 'EaFrameworkException' contains the message 'Cannot deserialize value of type'

  Scenario: From a null json I get an error
    Given a null json
    When parse the json as a list of 'JsonUtilsDto'
    Then I get an 'ConstraintViolationException' contains the message 'json: must not be blank'

  Scenario: From a json and a null target type I get an error
    Given a json
      """
      {}
      """
    When parse the json as a list of null
    Then I get an 'ConstraintViolationException' contains the message 'type: must not be null'
