Feature: Parse a json to type
  In order to retrieve the information contained in a json
  As a backend developer
  I want a component that creates an object from a json
  
   Rule: The conversion between data types is given by the following table
  
  | json     | java           |
  |----------|----------------|
  | string   | String         |
  | number   | Integer        |
  | number   | Long           |
  | number   | BigInteger     |
  | number   | Float          |
  | number   | Double         |
  | number   | BigDecimal     |
  | boolean  | Boolean        |
  | ISO-8601 | OffsetDateTime |
  | ISO-8601 | LocalDateTime  |
  | ISO-8601 | LocalDate      |
  | ISO-8601 | LocalTime      |
  | ISO-8601 | Year           |
  | string   | ENUM           |

  Background: 
    Given the default timezone 'America/Puerto_Rico'
    And the default language 'en'

  Scenario: From a json compatible with a target class I get an object
    Given a json
      """
      {
       "typeString": "Hello world",
       "typeInteger": 2147483647,
       "typeLong": 9223372036854775807,
       "typeBigInteger": 9223372036854775808,
       "typeFloat": 1.2345678,
       "typeDouble": 1.2345678901234567,
       "typeBigDecimal": 1.2345678901234567890123456789,
       "typeBoolean": true,
       "typeOffsetDateTime": "2019-04-24T15:43:00.003Z",
       "typeLocalDateTime": "2019-01-24T15:43:00.003Z",
       "typeLocalDate": "2019-04-24",
       "typeLocalTime": "15:43:00.003",
       "typeYear": "2019",
       "typeEnumAB": "A",
       "typeEnumLowerAB": "a"
      }
      """
    When parse the json as a type of 'JsonUtilsDto'
    Then I get an object with the values
      | typeString         | Hello world                    |
      | typeInteger        |                     2147483647 |
      | typeLong           |            9223372036854775807 |
      | typeBigInteger     |            9223372036854775808 |
      | typeFloat          |                      1.2345678 |
      | typeDouble         |             1.2345678901234567 |
      | typeBigDecimal     | 1.2345678901234567890123456789 |
      | typeBoolean        | true                           |
      | typeOffsetDateTime | 2019 04 24 15 43 00 003 Z      |
      | typeLocalDateTime  | 2019 01 24 11 43 00 003        |
      | typeLocalDate      | 2019 04 24                     |
      | typeLocalTime      | 15 43 00 003                   |
      | typeYear           |                           2019 |
      | typeEnumAB         | A                              |
      | typeEnumLowerAB    | A                              |

  Scenario: From a json with fewer properties than a target class I get an object
    Given a json
      """
      {
       "typeString": "Hello world",
       "typeInteger": 2147483647
      }
      """
    When parse the json as a type of 'JsonUtilsDto'
    Then I get an object with the values
      | typeString         | Hello world |
      | typeInteger        |  2147483647 |
      | typeLong           |             |
      | typeBigInteger     |             |
      | typeFloat          |             |
      | typeDouble         |             |
      | typeBigDecimal     |             |
      | typeBoolean        |             |
      | typeOffsetDateTime |             |
      | typeLocalDateTime  |             |
      | typeLocalDate      |             |
      | typeLocalTime      |             |
      | typeYear           |             |
      | typeEnumAB         |             |
      | typeEnumLowerAB    |             |

  Scenario: From a json with some properties not included in a target class I get an object
    Given a json
      """
      {
       "typeString": "Hello world",
       "typeInteger": 2147483647,
       "typeLong": 9223372036854775807,
       "typeBigInteger": 9223372036854775808,
       "typeFloat": 1.2345678,
       "typeDouble": 1.2345678901234567,
       "typeBigDecimal": 1.2345678901234567890123456789,
       "typeBoolean": true,
       "typeOffsetDateTime": "2019-04-24T15:43:00.003Z",
       "typeLocalDateTime": "2019-01-24T15:43:00.003Z",
       "typeLocalDate": "2019-04-24",
       "typeLocalTime": "15:43:00.003",
       "typeYear": "2019",
       "typeEnumAB": "A",
       "typeEnumLowerAB": "a",
       "nonTargetValue": "nonBarValue"
      }
      """
    When parse the json as a type of 'JsonUtilsDto'
    Then I get an object with the values
      | typeString         | Hello world                    |
      | typeInteger        |                     2147483647 |
      | typeLong           |            9223372036854775807 |
      | typeBigInteger     |            9223372036854775808 |
      | typeFloat          |                      1.2345678 |
      | typeDouble         |             1.2345678901234567 |
      | typeBigDecimal     | 1.2345678901234567890123456789 |
      | typeBoolean        | true                           |
      | typeOffsetDateTime | 2019 04 24 15 43 00 003 Z      |
      | typeLocalDateTime  | 2019 01 24 11 43 00 003        |
      | typeLocalDate      | 2019 04 24                     |
      | typeLocalTime      | 15 43 00 003                   |
      | typeYear           |                           2019 |
      | typeEnumAB         | A                              |
      | typeEnumLowerAB    | A                              |

  Scenario: From a json without properties I get an empty object
    Given a json
      """
      {}
      """
    When parse the json as a type of 'JsonUtilsDto'
    Then I get an object with the values
      | typeString         |  |
      | typeInteger        |  |
      | typeLong           |  |
      | typeBigInteger     |  |
      | typeFloat          |  |
      | typeDouble         |  |
      | typeBigDecimal     |  |
      | typeBoolean        |  |
      | typeOffsetDateTime |  |
      | typeLocalDateTime  |  |
      | typeLocalDate      |  |
      | typeLocalTime      |  |
      | typeYear           |  |
      | typeEnumAB         |  |
      | typeEnumLowerAB    |  |

  Scenario: From a json representing a null I get a null
    Given a json
      """
      null
      """
    When parse the json as a type of 'JsonUtilsDto'
    Then I get a null value

  Scenario: From a json with the property 'typeInteger' not compatible with a target class I get an error
    Given a json
      """
      {
       "typeInteger": "Hello world"
      }
      """
    When parse the json as a type of 'JsonUtilsDto'
    Then I get an 'EaFrameworkException' contains the message 'Cannot deserialize value of type'

  Scenario: From a json with the property 'typeLong' not compatible with a target class I get an error
    Given a json
      """
      {
       "typeLong": "Hello world"
      }
      """
    When parse the json as a type of 'JsonUtilsDto'
    Then I get an 'EaFrameworkException' contains the message 'Cannot deserialize value of type'

  Scenario: From a json with the property 'typeBigInteger' not compatible with a target class I get an error
    Given a json
      """
      {
       "typeBigInteger": "Hello world"
      }
      """
    When parse the json as a type of 'JsonUtilsDto'
    Then I get an 'EaFrameworkException' contains the message 'Cannot deserialize value of type'

  Scenario: From a json with the property 'typeFloat' not compatible with a target class I get an error
    Given a json
      """
      {
       "typeFloat": "Hello world"
      }
      """
    When parse the json as a type of 'JsonUtilsDto'
    Then I get an 'EaFrameworkException' contains the message 'Cannot deserialize value of type'

  Scenario: From a json with the property 'typeDouble' not compatible with a target class I get an error
    Given a json
      """
      {
       "typeDouble": "Hello world"
      }
      """
    When parse the json as a type of 'JsonUtilsDto'
    Then I get an 'EaFrameworkException' contains the message 'Cannot deserialize value of type'

  Scenario: From a json with the property 'typeBigDecimal' not compatible with a target class I get an error
    Given a json
      """
      {
       "typeBigDecimal": "Hello world"
      }
      """
    When parse the json as a type of 'JsonUtilsDto'
    Then I get an 'EaFrameworkException' contains the message 'Cannot deserialize value of type'

  Scenario: From a json with the property 'typeBoolean' not compatible with a target class I get an error
    Given a json
      """
      {
       "typeBoolean": "Hello world"
      }
      """
    When parse the json as a type of 'JsonUtilsDto'
    Then I get an 'EaFrameworkException' contains the message 'Cannot deserialize value of type'

  Scenario: From a json with the property 'typeOffsetDateTime' not compatible with a target class I get an error
    Given a json
      """
      {
       "typeOffsetDateTime": "Hello world"
      }
      """
    When parse the json as a type of 'JsonUtilsDto'
    Then I get an 'EaFrameworkException' contains the message 'Cannot deserialize value of type'

  Scenario: From a json with the property 'typeLocalDateTime' not compatible with a target class I get an error
    Given a json
      """
      {
       "typeLocalDateTime": "Hello world"
      }
      """
    When parse the json as a type of 'JsonUtilsDto'
    Then I get an 'EaFrameworkException' contains the message 'Cannot deserialize value of type'

  Scenario: From a json with the property 'typeLocalDate' not compatible with a target class I get an error
    Given a json
      """
      {
       "typeLocalDate": "Hello world"
      }
      """
    When parse the json as a type of 'JsonUtilsDto'
    Then I get an 'EaFrameworkException' contains the message 'Cannot deserialize value of type'

  Scenario: From a json with the property 'typeLocalTime' not compatible with a target class I get an error
    Given a json
      """
      {
       "typeLocalTime": "Hello world"
      }
      """
    When parse the json as a type of 'JsonUtilsDto'
    Then I get an 'EaFrameworkException' contains the message 'Cannot deserialize value of type'

  Scenario: From a json with the property 'typeYear' not compatible with a target class I get an error
    Given a json
      """
      {
       "typeYear": "Hello world"
      }
      """
    When parse the json as a type of 'JsonUtilsDto'
    Then I get an 'EaFrameworkException' contains the message 'Cannot deserialize value of type'

  Scenario: From a json with the property 'typeEnumAB' not compatible with a target class I get an error
    Given a json
      """
      {
       "typeEnumAB": "X"
      }
      """
    When parse the json as a type of 'JsonUtilsDto'
    Then I get an 'EaFrameworkException' contains the message 'Cannot deserialize value of type'

  Scenario: From a json with the property 'typeOffsetDateTime' not compatible with a target class I get an error
    Given a json
      """
      {
       "typeOffsetDateTime": ""
      }
      """
    When parse the json as a type of 'JsonUtilsDto'
    Then I get an object with the values
      | typeString         |  |
      | typeInteger        |  |
      | typeLong           |  |
      | typeBigInteger     |  |
      | typeFloat          |  |
      | typeDouble         |  |
      | typeBigDecimal     |  |
      | typeBoolean        |  |
      | typeOffsetDateTime |  |
      | typeLocalDateTime  |  |
      | typeLocalDate      |  |
      | typeLocalTime      |  |
      | typeYear           |  |
      | typeEnumAB         |  |
      | typeEnumLowerAB    |  |

  Scenario: From a json with the property 'typeLocalDateTime' not compatible with a target class I get an error
    Given a json
      """
      {
       "typeLocalDateTime": ""
      }
      """
    When parse the json as a type of 'JsonUtilsDto'
    Then I get an object with the values
      | typeString         |  |
      | typeInteger        |  |
      | typeLong           |  |
      | typeBigInteger     |  |
      | typeFloat          |  |
      | typeDouble         |  |
      | typeBigDecimal     |  |
      | typeBoolean        |  |
      | typeOffsetDateTime |  |
      | typeLocalDateTime  |  |
      | typeLocalDate      |  |
      | typeLocalTime      |  |
      | typeYear           |  |
      | typeEnumAB         |  |
      | typeEnumLowerAB    |  |

  Scenario: From a json with the property 'typeLocalDate' not compatible with a target class I get an error
    Given a json
      """
      {
       "typeLocalDate": ""
      }
      """
    When parse the json as a type of 'JsonUtilsDto'
    Then I get an object with the values
      | typeString         |  |
      | typeInteger        |  |
      | typeLong           |  |
      | typeBigInteger     |  |
      | typeFloat          |  |
      | typeDouble         |  |
      | typeBigDecimal     |  |
      | typeBoolean        |  |
      | typeOffsetDateTime |  |
      | typeLocalDateTime  |  |
      | typeLocalDate      |  |
      | typeLocalTime      |  |
      | typeYear           |  |
      | typeEnumAB         |  |
      | typeEnumLowerAB    |  |

  Scenario: From a json with the property 'typeLocalTime' not compatible with a target class I get an error
    Given a json
      """
      {
       "typeLocalTime": ""
      }
      """
    When parse the json as a type of 'JsonUtilsDto'
    Then I get an object with the values
      | typeString         |  |
      | typeInteger        |  |
      | typeLong           |  |
      | typeBigInteger     |  |
      | typeFloat          |  |
      | typeDouble         |  |
      | typeBigDecimal     |  |
      | typeBoolean        |  |
      | typeOffsetDateTime |  |
      | typeLocalDateTime  |  |
      | typeLocalDate      |  |
      | typeLocalTime      |  |
      | typeYear           |  |
      | typeEnumAB         |  |
      | typeEnumLowerAB    |  |

  Scenario: From a json with the property 'typeYear' not compatible with a target class I get an error
    Given a json
      """
      {
       "typeYear": ""
      }
      """
    When parse the json as a type of 'JsonUtilsDto'
    Then I get an object with the values
      | typeString         |  |
      | typeInteger        |  |
      | typeLong           |  |
      | typeBigInteger     |  |
      | typeFloat          |  |
      | typeDouble         |  |
      | typeBigDecimal     |  |
      | typeBoolean        |  |
      | typeOffsetDateTime |  |
      | typeLocalDateTime  |  |
      | typeLocalDate      |  |
      | typeLocalTime      |  |
      | typeYear           |  |
      | typeEnumAB         |  |
      | typeEnumLowerAB    |  |

  Scenario: From a malformed json I get an error
    Given a json
      """
      {
       "typeString": "Hello world
      }
      """
    When parse the json as a type of 'JsonUtilsDto'
    Then I get an 'EaFrameworkException' contains the message 'Illegal unquoted character'

  Scenario: From a null json I get an error
    Given a null json
    When parse the json as a type of 'JsonUtilsDto'
    Then I get an 'ConstraintViolationException' contains the message 'json: must not be blank'

  Scenario: From a json and a null target type I get an error
    Given a json
      """
      {}
      """
    When parse the json as a type of null
    Then I get an 'ConstraintViolationException' contains the message 'type: must not be null'
