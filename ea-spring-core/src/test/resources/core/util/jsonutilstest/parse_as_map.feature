Feature: Parse a json to map
  In order to retrieve the information contained in a json
  As a backend developer
  I want a component that creates a map from a json
  
  Rule: The conversion between data types is given by the following table
  
  | json     | java           |
  |----------|----------------|
  | string   | String         |
  | number   | Integer        |
  | number   | Long           |
  | number   | BigInteger     |
  | number   | Float          |
  | number   | Double         |
  | number   | BigDecimal     |
  | boolean  | Boolean        |
  | ISO-8601 | OffsetDateTime |
  | ISO-8601 | LocalDateTime  |
  | ISO-8601 | LocalDate      |
  | ISO-8601 | LocalTime      |
  | ISO-8601 | Year           |
  | string   | ENUM           |

  Background: 
    Given the default timezone 'America/Puerto_Rico'
    And the default language 'en'

  Scenario: From a json I get a map
    Given a json
      """
      {
       "typeString": "Hello world",
       "typeInteger": 2147483647,
       "typeLong": 9223372036854775807,
       "typeBigInteger": 9223372036854775808,
       "typeFloat": 1.2345678,
       "typeDouble": 1.2345678901234567,
       "typeBigDecimal": 1.2345678901234567890123456789,
       "typeBoolean": true,
       "typeOffsetDateTime": "2019-04-24T15:43:00.003Z",
       "typeLocalDateTime": "2019-01-24T15:43:00.003-05:00",
       "typeLocalDate": "2019-04-24",
       "typeLocalTime": "15:43:00.003",
       "typeYear": "2019",
       "typeEnumAB": "A",
       "typeEnumLowerAB": "a"
      }
      """
    When parse the json as a map with 'String' key and 'Object' value
    Then I get a map with the values
      | typeString         | Hello world                   |
      | typeInteger        |                    2147483647 |
      | typeLong           |           9223372036854775807 |
      | typeBigInteger     |           9223372036854775808 |
      | typeFloat          |                     1.2345678 |
      | typeDouble         |            1.2345678901234567 |
      | typeBigDecimal     |            1.2345678901234567 |
      | typeBoolean        | true                          |
      | typeOffsetDateTime | 2019-04-24T15:43:00.003Z      |
      | typeLocalDateTime  | 2019-01-24T15:43:00.003-05:00 |
      | typeLocalDate      | 2019-04-24                    |
      | typeLocalTime      | 15:43:00.003                  |
      | typeYear           |                          2019 |
      | typeEnumAB         | A                             |
      | typeEnumLowerAB    | a                             |
    Then I get a map with the classes
      | typeString         | String     |
      | typeInteger        | Integer    |
      | typeLong           | Long       |
      | typeBigInteger     | BigInteger |
      | typeFloat          | Double     |
      | typeDouble         | Double     |
      | typeBigDecimal     | Double     |
      | typeBoolean        | Boolean    |
      | typeOffsetDateTime | String     |
      | typeLocalDateTime  | String     |
      | typeLocalDate      | String     |
      | typeLocalTime      | String     |
      | typeYear           | String     |
      | typeEnumAB         | String     |
      | typeEnumLowerAB    | String     |

  Scenario: From a json without properties I get an empty object
    Given a json
      """
      {}
      """
    When parse the json as a map with 'String' key and 'Object' value
    Then I get a empty map

  Scenario: From a json representing a null I get a null
    Given a json
      """
      null
      """
    When parse the json as a map with 'String' key and 'Object' value
    Then I get a null value

  Scenario: From a malformed json I get an error
    Given a json
      """
      {
       "typeString": "Hello world
      }
      """
    When parse the json as a map with 'String' key and 'Object' value
    Then I get an 'EaFrameworkException' contains the message 'Illegal unquoted character'

  Scenario: From a null json I get an error
    Given a null json
    When parse the json as a map with 'String' key and 'Object' value
    Then I get an 'ConstraintViolationException' contains the message 'json: must not be blank'

  Scenario: From a json and a null key type of map I get an error
    Given a json
      """
      {}
      """
    When parse the json as a map with null key and 'Object' value
    Then I get an 'ConstraintViolationException' contains the message 'keyClass: must not be null'

  Scenario: From a json and a null value type of map I get an error
    Given a json
      """
      {}
      """
    When parse the json as a map with 'String' key and null value
    Then I get an 'ConstraintViolationException' contains the message 'valueClass: must not be null'
