Feature: Convert objects to pretty json
  In order to convert objects to json
  As a backend developer
  I want a component that creates a json from an object
  
  Rule: The conversion between data types is given by the following table
  
  | java           | json     |
  |----------------|----------|
  | String         | string   |
  | Integer        | number   |
  | Long           | number   |
  | BigInteger     | number   |
  | Float          | number   |
  | Double         | number   |
  | BigDecimal     | number   |
  | Boolean        | boolean  |
  | OffsetDateTime | ISO-8601 |
  | LocalDateTime  | ISO-8601 |
  | LocalDate      | ISO-8601 |
  | LocalTime      | ISO-8601 |
  | Year           | ISO-8601 |
  | ENUM           | string   |

  Background: 
    Given the default timezone 'America/Puerto_Rico'
    And the default language 'en'

  Scenario: From an object I get a json
    Given an object with the values
      | typeString         | Hello world                    |
      | typeInteger        |                     2147483647 |
      | typeLong           |            9223372036854775807 |
      | typeBigInteger     |            9223372036854775808 |
      | typeFloat          |                      1.2345678 |
      | typeDouble         |             1.2345678901234567 |
      | typeBigDecimal     | 1.2345678901234567890123456789 |
      | typeBoolean        | true                           |
      | typeOffsetDateTime | 2019 04 24 15 43 00 003 Z      |
      | typeLocalDateTime  | 2019 01 24 15 43 00 003        |
      | typeLocalDate      | 2019 04 24                     |
      | typeLocalTime      | 15 43 00 003                   |
      | typeYear           |                           2019 |
      | typeEnumAB         | A                              |
      | typeEnumLowerAB    | A                              |
    When convert to pretty json
    Then I get a json
      """
      {
       "typeString": "Hello world",
       "typeInteger": 2147483647,
       "typeLong": 9223372036854775807,
       "typeBigInteger": 9223372036854775808,
       "typeFloat": 1.2345678,
       "typeDouble": 1.2345678901234567,
       "typeBigDecimal": 1.2345678901234567890123456789,
       "typeBoolean": true,
       "typeOffsetDateTime": "2019-04-24T15:43:00.003Z",
       "typeLocalDateTime": "2019-01-24T19:43:00.003Z",
       "typeLocalDate": "2019-04-24",
       "typeLocalTime": "15:43:00.003",
       "typeYear": "2019",
       "typeEnumAB": "A",
       "typeEnumLowerAB": "a"
      }
      """

  Scenario: From an object with some null properties I get a json
    Given an object with the values
      | typeString         | Hello world |
      | typeInteger        |  2147483647 |
      | typeLong           |             |
      | typeBigInteger     |             |
      | typeFloat          |             |
      | typeDouble         |             |
      | typeBigDecimal     |             |
      | typeBoolean        |             |
      | typeOffsetDateTime |             |
      | typeLocalDateTime  |             |
      | typeLocalDate      |             |
      | typeLocalTime      |             |
      | typeYear           |             |
      | typeEnumAB         |             |
      | typeEnumLowerAB    |             |
    When convert to pretty json
    Then I get a json
      """
      {
       "typeString": "Hello world",
       "typeInteger": 2147483647
      }
      """

  Scenario: From an object with all null properties I get an empty json
    Given an object with the values
      | typeString         |  |
      | typeInteger        |  |
      | typeLong           |  |
      | typeBigInteger     |  |
      | typeFloat          |  |
      | typeDouble         |  |
      | typeBigDecimal     |  |
      | typeBoolean        |  |
      | typeOffsetDateTime |  |
      | typeLocalDateTime  |  |
      | typeLocalDate      |  |
      | typeLocalTime      |  |
      | typeYear           |  |
      | typeEnumAB         |  |
      | typeEnumLowerAB    |  |
    When convert to pretty json
    Then I get a json
      """
      {}
      """

  Scenario: From a null object I get a json representing a null
    Given a null object
    When convert to pretty json
    Then I get a json
      """
      null
      """

  Scenario: From a list of objects I get a json
    Given a list of objects
    And a object at position 0 with the values
      | typeString         | Hello world                     |
      | typeInteger        |                     -2147483648 |
      | typeLong           |            -9223372036854775808 |
      | typeBigInteger     |            -9223372036854775809 |
      | typeFloat          |                      -1.2345678 |
      | typeDouble         |             -1.2345678901234567 |
      | typeBigDecimal     | -1.2345678901234567890123456789 |
      | typeBoolean        | true                            |
      | typeOffsetDateTime | 2019 04 24 15 43 00 003 Z       |
      | typeLocalDateTime  | 2019 01 24 15 43 00 003         |
      | typeLocalDate      | 2019 04 24                      |
      | typeLocalTime      | 15 43 00 003                    |
      | typeYear           |                            2019 |
      | typeEnumAB         | A                               |
      | typeEnumLowerAB    | A                               |
    And a object at position 1 with the values
      | typeString         | Hello world                    |
      | typeInteger        |                     2147483647 |
      | typeLong           |            9223372036854775807 |
      | typeBigInteger     |            9223372036854775808 |
      | typeFloat          |                      1.2345678 |
      | typeDouble         |             1.2345678901234567 |
      | typeBigDecimal     | 1.2345678901234567890123456789 |
      | typeBoolean        | true                           |
      | typeOffsetDateTime | 2019 04 24 15 43 00 003 +05    |
      | typeLocalDateTime  | 2019 01 24 15 43 00 003        |
      | typeLocalDate      | 2019 04 24                     |
      | typeLocalTime      |                                |
      | typeYear           |                           2021 |
      | typeEnumAB         | B                              |
      | typeEnumLowerAB    | B                              |
    When convert to pretty json
    Then I get a json
      """
      [
       {
        "typeString": "Hello world",
        "typeInteger": -2147483648,
        "typeLong": -9223372036854775808,
        "typeBigInteger": -9223372036854775809,
        "typeFloat": -1.2345678,
        "typeDouble": -1.2345678901234567,
        "typeBigDecimal": -1.2345678901234567890123456789,
        "typeBoolean": true,
        "typeOffsetDateTime": "2019-04-24T15:43:00.003Z",
        "typeLocalDateTime": "2019-01-24T19:43:00.003Z",
        "typeLocalDate": "2019-04-24",
        "typeLocalTime": "15:43:00.003",
        "typeYear": "2019",
        "typeEnumAB": "A",
       "typeEnumLowerAB": "a"
       },
       {
        "typeString": "Hello world",
        "typeInteger": 2147483647,
        "typeLong": 9223372036854775807,
        "typeBigInteger": 9223372036854775808,
        "typeFloat": 1.2345678,
        "typeDouble": 1.2345678901234567,
        "typeBigDecimal": 1.2345678901234567890123456789,
        "typeBoolean": true,
        "typeOffsetDateTime": "2019-04-24T15:43:00.003+05:00",
        "typeLocalDateTime": "2019-01-24T19:43:00.003Z",
        "typeLocalDate": "2019-04-24",
        "typeYear": "2021",
        "typeEnumAB": "B",
       "typeEnumLowerAB": "b"
       }
      ]
      """

  Scenario: From an empty list of objects I get a json
    Given an empty list
    When convert to pretty json
    Then I get a json
      """
      []
      """

  Scenario: From an null list I get a json
    Given a null list
    When convert to pretty json
    Then I get a json
      """
      null
      """

  Scenario: From an object with circular reference I get an error
    Given given an object that one of its properties refers to itself
    When convert to pretty json
    Then I get an 'EaFrameworkException' contains the message 'Direct self-reference leading to cycle'
