Feature: Parse a json to type only calls the set methods of the properties that are defined in the json
  In order to determine the properties defined in the json
  As a backend developer
  I want a component that only calls the set methods of the properties that are defined in the json

  Scenario Outline: Parse the json <json> as a type Witness.class with a param properties and having a paramDefined property which is set to true when the set method is invoked
    Given a json <json>
    When parse the json as a type Witness
    Then the object has the paramDefined value <paramDefined>
    And the object has the param value <param value>

    Examples: 
      | json                | paramDefined | param value |
      | '{}'                | 'false'      | null        |
      | '{"param":null}'    | 'true'       | null        |
      | '{"param":"value"}' | 'true'       | 'value'     |

  Scenario Outline: Parse the json <json> as a type OptionalWitness.class with an Optional param properties
    Given a json <json>
    When parse the json as a type OptionalWitness
    Then the object has the optional param set as <param is null>
    And the object has the optional param value is <param is present>
    And the object has the optional param value set as <param value>

    Examples: 
      | json                | param is null | param is present | param value |
      | '{}'                | null          |                  |             |
      | '{"param":null}'    | not null      | not present      |             |
      | '{"param":"value"}' | not null      | present          | 'value'     |
