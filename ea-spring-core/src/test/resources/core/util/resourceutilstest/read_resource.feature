Feature: Read the content of a file in the classpath
  In order to save information relevant to the application in text files
  As a backend developer
  I want a component that reads the content of a file from the classpath in UTF8

  Background: 
    Given the default language 'en'

  Scenario Outline: The content of a file is read by path <file path> and file name <file name>
    Given the following path <file path>
    And the file name <file name>
    When I read the file
    Then I get the content
      """
      Hello
      World
      """

    Examples: 
      | file path                                      | file name            |
      | 'classpath:core/util/resourceutilstest/tests'  | 'resource_file.txt'  |
      | 'classpath:core/util/resourceutilstest/tests/' | 'resource_file.txt'  |
      | 'classpath:core/util/resourceutilstest/tests'  | '/resource_file.txt' |
      | 'classpath:core/util/resourceutilstest/tests/' | '/resource_file.txt' |

  Scenario: The content of a file is read by resource url
    Given the following file 'classpath:core/util/resourceutilstest/tests/resource_file.txt'
    When I read the file
    Then I get the content
      """
      Hello
      World
      """

  Scenario: The content of a file with emojis is read
    Given the following file 'classpath:core/util/resourceutilstest/tests/emoji.txt'
    When I read the file
    Then I get the content
      """
      😀❤️😂😍👍
      """

  Scenario: Trying to read a file that does not exist, I get an error
    Given the following file 'classpath:core/util/resourceutilstest/resource_file.txt'
    When I read the file
    Then I get an 'EaFrameworkException' with the message 'class path resource [core/util/resourceutilstest/resource_file.txt] cannot be resolved to URL because it does not exist'

  Scenario: Trying to read a blank path, I get an error
    Given the following path ''
    And the file name 'resource_file.txt'
    When I read the file
    Then I get an 'ConstraintViolationException' contains the message 'resourcePath: must not be blank'

  Scenario: Trying to read a path from file-system, I get an error
    Given the following path 'file:core/util/resourceutilstest/tests/resource_file.txt'
    And the file name 'resource_file.txt'
    When I read the file
    Then I get an 'ConstraintViolationException' contains the message 'resourcePath: must belong to the classpath protocol'

  Scenario: Trying to read a blank file-name, I get an error
    Given the following path 'classpath:core/util/resourceutilstest/tests'
    And the file name ''
    When I read the file
    Then I get an 'ConstraintViolationException' contains the message 'filename: must not be blank'

  Scenario: Trying to read a blank file, I get an error
    Given the following file ''
    When I read the file
    Then I get an 'ConstraintViolationException' contains the message 'resourceLocation: must not be blank'

  Scenario: Trying to read a file from file-system, I get an error
    Given the following file 'file:core/util/resourceutilstest/tests/resource_file.txt'
    When I read the file
    Then I get an 'ConstraintViolationException' contains the message 'resourceLocation: must belong to the classpath protocol'
