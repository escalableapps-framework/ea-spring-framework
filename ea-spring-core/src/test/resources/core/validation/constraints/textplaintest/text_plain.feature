Feature: Plain text content validation
  In order to avoid the injection of html or javascript code
  As a backend developer
  I want a component that only allows entering plain text

  Scenario Outline: Text <text> is expected to be plain text
    Given the text <text>
    When the TextPlain validation is executed
    Then the <result> result is obtained in the validation
    And the message in validation is <message>

    Examples: 
      | text                                                                                              | result  | message            |
      | null                                                                                              | 'true'  | null               |
      | ''                                                                                                | 'true'  | null               |
      | '  '                                                                                              | 'true'  | null               |
      | 'hola'                                                                                            | 'true'  | null               |
      | '<b>hola</b>'                                                                                     | 'false' | "isn't plain/text" |
      | '<em>hola</em>'                                                                                   | 'false' | "isn't plain/text" |
      | '<i>hola</i>'                                                                                     | 'false' | "isn't plain/text" |
      | '<strong>hola</strong>'                                                                           | 'false' | "isn't plain/text" |
      | '<u>hola</u>'                                                                                     | 'false' | "isn't plain/text" |
      | '<a>hola</a>'                                                                                     | 'false' | "isn't plain/text" |
      | '<blockquote>hola</blockquote>'                                                                   | 'false' | "isn't plain/text" |
      | '<br><br/><br />'                                                                                 | 'false' | "isn't plain/text" |
      | '<cite>hola</cite>'                                                                               | 'false' | "isn't plain/text" |
      | '<code>hola</code>'                                                                               | 'false' | "isn't plain/text" |
      | '<dd>hola</dd>'                                                                                   | 'false' | "isn't plain/text" |
      | '<dl>hola</dl>'                                                                                   | 'false' | "isn't plain/text" |
      | '<dt>hola</dt>'                                                                                   | 'false' | "isn't plain/text" |
      | '<li>hola</li>'                                                                                   | 'false' | "isn't plain/text" |
      | '<ol>hola</ol>'                                                                                   | 'false' | "isn't plain/text" |
      | '<p>hola</p>'                                                                                     | 'false' | "isn't plain/text" |
      | '<pre>hola</pre>'                                                                                 | 'false' | "isn't plain/text" |
      | '<q>hola</q>'                                                                                     | 'false' | "isn't plain/text" |
      | '<small>hola</small>'                                                                             | 'false' | "isn't plain/text" |
      | '<span>hola</span>'                                                                               | 'false' | "isn't plain/text" |
      | '<strike>hola</strike>'                                                                           | 'false' | "isn't plain/text" |
      | '<sub>hola</sub>'                                                                                 | 'false' | "isn't plain/text" |
      | '<sup>hola</sup>'                                                                                 | 'false' | "isn't plain/text" |
      | '<ul>hola</ul>'                                                                                   | 'false' | "isn't plain/text" |
      | '<img src="http://">'                                                                             | 'false' | "isn't plain/text" |
      | '<img src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" >' | 'false' | "isn't plain/text" |
      | '<table><caption>hola</caption></table>'                                                          | 'false' | "isn't plain/text" |
      | '<table><col></table>'                                                                            | 'false' | "isn't plain/text" |
      | '<table><colgroup></colgroup></table>'                                                            | 'false' | "isn't plain/text" |
      | '<div>hola</div>'                                                                                 | 'false' | "isn't plain/text" |
      | '<h1>hola</h1>'                                                                                   | 'false' | "isn't plain/text" |
      | '<h2>hola</h2>'                                                                                   | 'false' | "isn't plain/text" |
      | '<h3>hola</h3>'                                                                                   | 'false' | "isn't plain/text" |
      | '<h4>hola</h4>'                                                                                   | 'false' | "isn't plain/text" |
      | '<h5>hola</h5>'                                                                                   | 'false' | "isn't plain/text" |
      | '<h6>hola</h6>'                                                                                   | 'false' | "isn't plain/text" |
      | '<table></table>'                                                                                 | 'false' | "isn't plain/text" |
      | '<table><tbody></tbody></table>'                                                                  | 'false' | "isn't plain/text" |
      | '<table><tr><td>hola</td></tr></table>'                                                           | 'false' | "isn't plain/text" |
      | '<table><tfoot></tfoot></table>'                                                                  | 'false' | "isn't plain/text" |
      | '<table><tr><th></th></tr></table>'                                                               | 'false' | "isn't plain/text" |
      | '<table><thead></thead></table>'                                                                  | 'false' | "isn't plain/text" |
      | '<table><tr></tr></table>'                                                                        | 'false' | "isn't plain/text" |
      | '<script>hola</script>'                                                                           | 'false' | "isn't plain/text" |
