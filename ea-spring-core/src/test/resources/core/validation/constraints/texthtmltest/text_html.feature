Feature: Secure html content validation
  In order to avoid the injection of html or javascript code 
  As a backend developer
  I want a component that only allows entering html text with some constraints
  
  Rule:
  - TEXT_PLAIN: This whitelist allows only text nodes: all HTML will be prevent
  - HTML_SIMPLE: This whitelist allows only simple text formatting: b, em, i, strong, u. All other HTML (tags andattributes) will be prevent
  - HTML_BASIC: This whitelist allows a fuller range of text nodes: a, b, blockquote, br, cite, code, dd, dl, dt, em, i, li,ol, p, pre, q, small, span, strike, strong, sub, sup, u, ul, and appropriate attributes. Links (a elements) can point to http, https, ftp, mailto, and have an enforced rel=nofollow attribute. Does not allow images.
  - HTML_IMAGES: This whitelist allows the same text tags as basic, and also allows img tags, with appropriateattributes, with src pointing to http or https.
  - HTML_DATA_IMAGES: This whitelist allows the same text tags as basic, and also allows img tags, with appropriateattributes, with src pointing to http, https or data
  - HTML_FULL: This whitelist allows a full range of text and structural body HTML: a, b, blockquote, br, caption, cite,code, col, colgroup, dd, div, dl, dt, em, h1, h2, h3, h4, h5, h6, i, img, li, ol, p, pre, q, small, span, strike, strong, sub,sup, table, tbody, td, tfoot, th, thead, tr, u, ul. Links do not have an enforced rel=nofollow attribute, but you can add that if desired

  Scenario Outline: Text <text> is expected to match the constraint <constraint>
    Given the text <text>
    When the TextPlain validation is executed with the <constraint> constraint
    Then the <result> result is obtained in the validation
    And the message in validation is <message>

    Examples: 
      | text                                                                                              | constraint         | result  | message                                                 |
      | null                                                                                              | 'TEXT_PLAIN'       | 'true'  | null                                                    |
      | ''                                                                                                | 'TEXT_PLAIN'       | 'true'  | null                                                    |
      | '  '                                                                                              | 'TEXT_PLAIN'       | 'true'  | null                                                    |
      | null                                                                                              | 'HTML_SIMPLE'      | 'true'  | null                                                    |
      | ''                                                                                                | 'HTML_SIMPLE'      | 'true'  | null                                                    |
      | '  '                                                                                              | 'HTML_SIMPLE'      | 'true'  | null                                                    |
      | null                                                                                              | 'HTML_BASIC'       | 'true'  | null                                                    |
      | ''                                                                                                | 'HTML_BASIC'       | 'true'  | null                                                    |
      | '  '                                                                                              | 'HTML_BASIC'       | 'true'  | null                                                    |
      | null                                                                                              | 'HTML_IMAGES'      | 'true'  | null                                                    |
      | ''                                                                                                | 'HTML_IMAGES'      | 'true'  | null                                                    |
      | '  '                                                                                              | 'HTML_IMAGES'      | 'true'  | null                                                    |
      | null                                                                                              | 'HTML_DATA_IMAGES' | 'true'  | null                                                    |
      | ''                                                                                                | 'HTML_DATA_IMAGES' | 'true'  | null                                                    |
      | '  '                                                                                              | 'HTML_DATA_IMAGES' | 'true'  | null                                                    |
      | null                                                                                              | 'HTML_FULL'        | 'true'  | null                                                    |
      | ''                                                                                                | 'HTML_FULL'        | 'true'  | null                                                    |
      | '  '                                                                                              | 'HTML_FULL'        | 'true'  | null                                                    |
      | 'hola'                                                                                            | 'TEXT_PLAIN'       | 'true'  | null                                                    |
      | '<b>hola</b>'                                                                                     | 'TEXT_PLAIN'       | 'false' | "isn't safe text/html with constraint TEXT_PLAIN"       |
      | '<em>hola</em>'                                                                                   | 'TEXT_PLAIN'       | 'false' | "isn't safe text/html with constraint TEXT_PLAIN"       |
      | '<i>hola</i>'                                                                                     | 'TEXT_PLAIN'       | 'false' | "isn't safe text/html with constraint TEXT_PLAIN"       |
      | '<strong>hola</strong>'                                                                           | 'TEXT_PLAIN'       | 'false' | "isn't safe text/html with constraint TEXT_PLAIN"       |
      | '<u>hola</u>'                                                                                     | 'TEXT_PLAIN'       | 'false' | "isn't safe text/html with constraint TEXT_PLAIN"       |
      | '<a>hola</a>'                                                                                     | 'TEXT_PLAIN'       | 'false' | "isn't safe text/html with constraint TEXT_PLAIN"       |
      | '<blockquote>hola</blockquote>'                                                                   | 'TEXT_PLAIN'       | 'false' | "isn't safe text/html with constraint TEXT_PLAIN"       |
      | '<br><br/><br />'                                                                                 | 'TEXT_PLAIN'       | 'false' | "isn't safe text/html with constraint TEXT_PLAIN"       |
      | '<cite>hola</cite>'                                                                               | 'TEXT_PLAIN'       | 'false' | "isn't safe text/html with constraint TEXT_PLAIN"       |
      | '<code>hola</code>'                                                                               | 'TEXT_PLAIN'       | 'false' | "isn't safe text/html with constraint TEXT_PLAIN"       |
      | '<dd>hola</dd>'                                                                                   | 'TEXT_PLAIN'       | 'false' | "isn't safe text/html with constraint TEXT_PLAIN"       |
      | '<dl>hola</dl>'                                                                                   | 'TEXT_PLAIN'       | 'false' | "isn't safe text/html with constraint TEXT_PLAIN"       |
      | '<dt>hola</dt>'                                                                                   | 'TEXT_PLAIN'       | 'false' | "isn't safe text/html with constraint TEXT_PLAIN"       |
      | '<li>hola</li>'                                                                                   | 'TEXT_PLAIN'       | 'false' | "isn't safe text/html with constraint TEXT_PLAIN"       |
      | '<ol>hola</ol>'                                                                                   | 'TEXT_PLAIN'       | 'false' | "isn't safe text/html with constraint TEXT_PLAIN"       |
      | '<p>hola</p>'                                                                                     | 'TEXT_PLAIN'       | 'false' | "isn't safe text/html with constraint TEXT_PLAIN"       |
      | '<pre>hola</pre>'                                                                                 | 'TEXT_PLAIN'       | 'false' | "isn't safe text/html with constraint TEXT_PLAIN"       |
      | '<q>hola</q>'                                                                                     | 'TEXT_PLAIN'       | 'false' | "isn't safe text/html with constraint TEXT_PLAIN"       |
      | '<small>hola</small>'                                                                             | 'TEXT_PLAIN'       | 'false' | "isn't safe text/html with constraint TEXT_PLAIN"       |
      | '<span>hola</span>'                                                                               | 'TEXT_PLAIN'       | 'false' | "isn't safe text/html with constraint TEXT_PLAIN"       |
      | '<strike>hola</strike>'                                                                           | 'TEXT_PLAIN'       | 'false' | "isn't safe text/html with constraint TEXT_PLAIN"       |
      | '<sub>hola</sub>'                                                                                 | 'TEXT_PLAIN'       | 'false' | "isn't safe text/html with constraint TEXT_PLAIN"       |
      | '<sup>hola</sup>'                                                                                 | 'TEXT_PLAIN'       | 'false' | "isn't safe text/html with constraint TEXT_PLAIN"       |
      | '<ul>hola</ul>'                                                                                   | 'TEXT_PLAIN'       | 'false' | "isn't safe text/html with constraint TEXT_PLAIN"       |
      | '<img src="http://">'                                                                             | 'TEXT_PLAIN'       | 'false' | "isn't safe text/html with constraint TEXT_PLAIN"       |
      | '<img src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" >' | 'TEXT_PLAIN'       | 'false' | "isn't safe text/html with constraint TEXT_PLAIN"       |
      | '<table><caption>hola</caption></table>'                                                          | 'TEXT_PLAIN'       | 'false' | "isn't safe text/html with constraint TEXT_PLAIN"       |
      | '<table><col></table>'                                                                            | 'TEXT_PLAIN'       | 'false' | "isn't safe text/html with constraint TEXT_PLAIN"       |
      | '<table><colgroup></colgroup></table>'                                                            | 'TEXT_PLAIN'       | 'false' | "isn't safe text/html with constraint TEXT_PLAIN"       |
      | '<div>hola</div>'                                                                                 | 'TEXT_PLAIN'       | 'false' | "isn't safe text/html with constraint TEXT_PLAIN"       |
      | '<h1>hola</h1>'                                                                                   | 'TEXT_PLAIN'       | 'false' | "isn't safe text/html with constraint TEXT_PLAIN"       |
      | '<h2>hola</h2>'                                                                                   | 'TEXT_PLAIN'       | 'false' | "isn't safe text/html with constraint TEXT_PLAIN"       |
      | '<h3>hola</h3>'                                                                                   | 'TEXT_PLAIN'       | 'false' | "isn't safe text/html with constraint TEXT_PLAIN"       |
      | '<h4>hola</h4>'                                                                                   | 'TEXT_PLAIN'       | 'false' | "isn't safe text/html with constraint TEXT_PLAIN"       |
      | '<h5>hola</h5>'                                                                                   | 'TEXT_PLAIN'       | 'false' | "isn't safe text/html with constraint TEXT_PLAIN"       |
      | '<h6>hola</h6>'                                                                                   | 'TEXT_PLAIN'       | 'false' | "isn't safe text/html with constraint TEXT_PLAIN"       |
      | '<table></table>'                                                                                 | 'TEXT_PLAIN'       | 'false' | "isn't safe text/html with constraint TEXT_PLAIN"       |
      | '<table><tbody></tbody></table>'                                                                  | 'TEXT_PLAIN'       | 'false' | "isn't safe text/html with constraint TEXT_PLAIN"       |
      | '<table><tr><td>hola</td></tr></table>'                                                           | 'TEXT_PLAIN'       | 'false' | "isn't safe text/html with constraint TEXT_PLAIN"       |
      | '<table><tfoot></tfoot></table>'                                                                  | 'TEXT_PLAIN'       | 'false' | "isn't safe text/html with constraint TEXT_PLAIN"       |
      | '<table><tr><th></th></tr></table>'                                                               | 'TEXT_PLAIN'       | 'false' | "isn't safe text/html with constraint TEXT_PLAIN"       |
      | '<table><thead></thead></table>'                                                                  | 'TEXT_PLAIN'       | 'false' | "isn't safe text/html with constraint TEXT_PLAIN"       |
      | '<table><tr></tr></table>'                                                                        | 'TEXT_PLAIN'       | 'false' | "isn't safe text/html with constraint TEXT_PLAIN"       |
      | '<script>hola</script>'                                                                           | 'TEXT_PLAIN'       | 'false' | "isn't safe text/html with constraint TEXT_PLAIN"       |
      | 'hola'                                                                                            | 'HTML_SIMPLE'      | 'true'  | null                                                    |
      | '<b>hola</b>'                                                                                     | 'HTML_SIMPLE'      | 'true'  | null                                                    |
      | '<em>hola</em>'                                                                                   | 'HTML_SIMPLE'      | 'true'  | null                                                    |
      | '<i>hola</i>'                                                                                     | 'HTML_SIMPLE'      | 'true'  | null                                                    |
      | '<strong>hola</strong>'                                                                           | 'HTML_SIMPLE'      | 'true'  | null                                                    |
      | '<u>hola</u>'                                                                                     | 'HTML_SIMPLE'      | 'true'  | null                                                    |
      | '<a>hola</a>'                                                                                     | 'HTML_SIMPLE'      | 'false' | "isn't safe text/html with constraint HTML_SIMPLE"      |
      | '<blockquote>hola</blockquote>'                                                                   | 'HTML_SIMPLE'      | 'false' | "isn't safe text/html with constraint HTML_SIMPLE"      |
      | '<br><br/><br />'                                                                                 | 'HTML_SIMPLE'      | 'false' | "isn't safe text/html with constraint HTML_SIMPLE"      |
      | '<cite>hola</cite>'                                                                               | 'HTML_SIMPLE'      | 'false' | "isn't safe text/html with constraint HTML_SIMPLE"      |
      | '<code>hola</code>'                                                                               | 'HTML_SIMPLE'      | 'false' | "isn't safe text/html with constraint HTML_SIMPLE"      |
      | '<dd>hola</dd>'                                                                                   | 'HTML_SIMPLE'      | 'false' | "isn't safe text/html with constraint HTML_SIMPLE"      |
      | '<dl>hola</dl>'                                                                                   | 'HTML_SIMPLE'      | 'false' | "isn't safe text/html with constraint HTML_SIMPLE"      |
      | '<dt>hola</dt>'                                                                                   | 'HTML_SIMPLE'      | 'false' | "isn't safe text/html with constraint HTML_SIMPLE"      |
      | '<li>hola</li>'                                                                                   | 'HTML_SIMPLE'      | 'false' | "isn't safe text/html with constraint HTML_SIMPLE"      |
      | '<ol>hola</ol>'                                                                                   | 'HTML_SIMPLE'      | 'false' | "isn't safe text/html with constraint HTML_SIMPLE"      |
      | '<p>hola</p>'                                                                                     | 'HTML_SIMPLE'      | 'false' | "isn't safe text/html with constraint HTML_SIMPLE"      |
      | '<pre>hola</pre>'                                                                                 | 'HTML_SIMPLE'      | 'false' | "isn't safe text/html with constraint HTML_SIMPLE"      |
      | '<q>hola</q>'                                                                                     | 'HTML_SIMPLE'      | 'false' | "isn't safe text/html with constraint HTML_SIMPLE"      |
      | '<small>hola</small>'                                                                             | 'HTML_SIMPLE'      | 'false' | "isn't safe text/html with constraint HTML_SIMPLE"      |
      | '<span>hola</span>'                                                                               | 'HTML_SIMPLE'      | 'false' | "isn't safe text/html with constraint HTML_SIMPLE"      |
      | '<strike>hola</strike>'                                                                           | 'HTML_SIMPLE'      | 'false' | "isn't safe text/html with constraint HTML_SIMPLE"      |
      | '<sub>hola</sub>'                                                                                 | 'HTML_SIMPLE'      | 'false' | "isn't safe text/html with constraint HTML_SIMPLE"      |
      | '<sup>hola</sup>'                                                                                 | 'HTML_SIMPLE'      | 'false' | "isn't safe text/html with constraint HTML_SIMPLE"      |
      | '<ul>hola</ul>'                                                                                   | 'HTML_SIMPLE'      | 'false' | "isn't safe text/html with constraint HTML_SIMPLE"      |
      | '<img src="http://">'                                                                             | 'HTML_SIMPLE'      | 'false' | "isn't safe text/html with constraint HTML_SIMPLE"      |
      | '<img src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" >' | 'HTML_SIMPLE'      | 'false' | "isn't safe text/html with constraint HTML_SIMPLE"      |
      | '<table><caption>hola</caption></table>'                                                          | 'HTML_SIMPLE'      | 'false' | "isn't safe text/html with constraint HTML_SIMPLE"      |
      | '<table><col></table>'                                                                            | 'HTML_SIMPLE'      | 'false' | "isn't safe text/html with constraint HTML_SIMPLE"      |
      | '<table><colgroup></colgroup></table>'                                                            | 'HTML_SIMPLE'      | 'false' | "isn't safe text/html with constraint HTML_SIMPLE"      |
      | '<div>hola</div>'                                                                                 | 'HTML_SIMPLE'      | 'false' | "isn't safe text/html with constraint HTML_SIMPLE"      |
      | '<h1>hola</h1>'                                                                                   | 'HTML_SIMPLE'      | 'false' | "isn't safe text/html with constraint HTML_SIMPLE"      |
      | '<h2>hola</h2>'                                                                                   | 'HTML_SIMPLE'      | 'false' | "isn't safe text/html with constraint HTML_SIMPLE"      |
      | '<h3>hola</h3>'                                                                                   | 'HTML_SIMPLE'      | 'false' | "isn't safe text/html with constraint HTML_SIMPLE"      |
      | '<h4>hola</h4>'                                                                                   | 'HTML_SIMPLE'      | 'false' | "isn't safe text/html with constraint HTML_SIMPLE"      |
      | '<h5>hola</h5>'                                                                                   | 'HTML_SIMPLE'      | 'false' | "isn't safe text/html with constraint HTML_SIMPLE"      |
      | '<h6>hola</h6>'                                                                                   | 'HTML_SIMPLE'      | 'false' | "isn't safe text/html with constraint HTML_SIMPLE"      |
      | '<table></table>'                                                                                 | 'HTML_SIMPLE'      | 'false' | "isn't safe text/html with constraint HTML_SIMPLE"      |
      | '<table><tbody></tbody></table>'                                                                  | 'HTML_SIMPLE'      | 'false' | "isn't safe text/html with constraint HTML_SIMPLE"      |
      | '<table><tr><td>hola</td></tr></table>'                                                           | 'HTML_SIMPLE'      | 'false' | "isn't safe text/html with constraint HTML_SIMPLE"      |
      | '<table><tfoot></tfoot></table>'                                                                  | 'HTML_SIMPLE'      | 'false' | "isn't safe text/html with constraint HTML_SIMPLE"      |
      | '<table><tr><th></th></tr></table>'                                                               | 'HTML_SIMPLE'      | 'false' | "isn't safe text/html with constraint HTML_SIMPLE"      |
      | '<table><thead></thead></table>'                                                                  | 'HTML_SIMPLE'      | 'false' | "isn't safe text/html with constraint HTML_SIMPLE"      |
      | '<table><tr></tr></table>'                                                                        | 'HTML_SIMPLE'      | 'false' | "isn't safe text/html with constraint HTML_SIMPLE"      |
      | '<script>hola</script>'                                                                           | 'HTML_SIMPLE'      | 'false' | "isn't safe text/html with constraint HTML_SIMPLE"      |
      | 'hola'                                                                                            | 'HTML_BASIC'       | 'true'  | null                                                    |
      | '<b>hola</b>'                                                                                     | 'HTML_BASIC'       | 'true'  | null                                                    |
      | '<em>hola</em>'                                                                                   | 'HTML_BASIC'       | 'true'  | null                                                    |
      | '<i>hola</i>'                                                                                     | 'HTML_BASIC'       | 'true'  | null                                                    |
      | '<strong>hola</strong>'                                                                           | 'HTML_BASIC'       | 'true'  | null                                                    |
      | '<u>hola</u>'                                                                                     | 'HTML_BASIC'       | 'true'  | null                                                    |
      | '<a>hola</a>'                                                                                     | 'HTML_BASIC'       | 'true'  | null                                                    |
      | '<blockquote>hola</blockquote>'                                                                   | 'HTML_BASIC'       | 'true'  | null                                                    |
      | '<br><br/><br />'                                                                                 | 'HTML_BASIC'       | 'true'  | null                                                    |
      | '<cite>hola</cite>'                                                                               | 'HTML_BASIC'       | 'true'  | null                                                    |
      | '<code>hola</code>'                                                                               | 'HTML_BASIC'       | 'true'  | null                                                    |
      | '<dd>hola</dd>'                                                                                   | 'HTML_BASIC'       | 'true'  | null                                                    |
      | '<dl>hola</dl>'                                                                                   | 'HTML_BASIC'       | 'true'  | null                                                    |
      | '<dt>hola</dt>'                                                                                   | 'HTML_BASIC'       | 'true'  | null                                                    |
      | '<li>hola</li>'                                                                                   | 'HTML_BASIC'       | 'true'  | null                                                    |
      | '<ol>hola</ol>'                                                                                   | 'HTML_BASIC'       | 'true'  | null                                                    |
      | '<p>hola</p>'                                                                                     | 'HTML_BASIC'       | 'true'  | null                                                    |
      | '<pre>hola</pre>'                                                                                 | 'HTML_BASIC'       | 'true'  | null                                                    |
      | '<q>hola</q>'                                                                                     | 'HTML_BASIC'       | 'true'  | null                                                    |
      | '<small>hola</small>'                                                                             | 'HTML_BASIC'       | 'true'  | null                                                    |
      | '<span>hola</span>'                                                                               | 'HTML_BASIC'       | 'true'  | null                                                    |
      | '<strike>hola</strike>'                                                                           | 'HTML_BASIC'       | 'true'  | null                                                    |
      | '<sub>hola</sub>'                                                                                 | 'HTML_BASIC'       | 'true'  | null                                                    |
      | '<sup>hola</sup>'                                                                                 | 'HTML_BASIC'       | 'true'  | null                                                    |
      | '<ul>hola</ul>'                                                                                   | 'HTML_BASIC'       | 'true'  | null                                                    |
      | '<img src="http://">'                                                                             | 'HTML_BASIC'       | 'false' | "isn't safe text/html with constraint HTML_BASIC"       |
      | '<img src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" >' | 'HTML_BASIC'       | 'false' | "isn't safe text/html with constraint HTML_BASIC"       |
      | '<table><caption>hola</caption></table>'                                                          | 'HTML_BASIC'       | 'false' | "isn't safe text/html with constraint HTML_BASIC"       |
      | '<table><col></table>'                                                                            | 'HTML_BASIC'       | 'false' | "isn't safe text/html with constraint HTML_BASIC"       |
      | '<table><colgroup></colgroup></table>'                                                            | 'HTML_BASIC'       | 'false' | "isn't safe text/html with constraint HTML_BASIC"       |
      | '<div>hola</div>'                                                                                 | 'HTML_BASIC'       | 'false' | "isn't safe text/html with constraint HTML_BASIC"       |
      | '<h1>hola</h1>'                                                                                   | 'HTML_BASIC'       | 'false' | "isn't safe text/html with constraint HTML_BASIC"       |
      | '<h2>hola</h2>'                                                                                   | 'HTML_BASIC'       | 'false' | "isn't safe text/html with constraint HTML_BASIC"       |
      | '<h3>hola</h3>'                                                                                   | 'HTML_BASIC'       | 'false' | "isn't safe text/html with constraint HTML_BASIC"       |
      | '<h4>hola</h4>'                                                                                   | 'HTML_BASIC'       | 'false' | "isn't safe text/html with constraint HTML_BASIC"       |
      | '<h5>hola</h5>'                                                                                   | 'HTML_BASIC'       | 'false' | "isn't safe text/html with constraint HTML_BASIC"       |
      | '<h6>hola</h6>'                                                                                   | 'HTML_BASIC'       | 'false' | "isn't safe text/html with constraint HTML_BASIC"       |
      | '<table></table>'                                                                                 | 'HTML_BASIC'       | 'false' | "isn't safe text/html with constraint HTML_BASIC"       |
      | '<table><tbody></tbody></table>'                                                                  | 'HTML_BASIC'       | 'false' | "isn't safe text/html with constraint HTML_BASIC"       |
      | '<table><tr><td>hola</td></tr></table>'                                                           | 'HTML_BASIC'       | 'false' | "isn't safe text/html with constraint HTML_BASIC"       |
      | '<table><tfoot></tfoot></table>'                                                                  | 'HTML_BASIC'       | 'false' | "isn't safe text/html with constraint HTML_BASIC"       |
      | '<table><tr><th></th></tr></table>'                                                               | 'HTML_BASIC'       | 'false' | "isn't safe text/html with constraint HTML_BASIC"       |
      | '<table><thead></thead></table>'                                                                  | 'HTML_BASIC'       | 'false' | "isn't safe text/html with constraint HTML_BASIC"       |
      | '<table><tr></tr></table>'                                                                        | 'HTML_BASIC'       | 'false' | "isn't safe text/html with constraint HTML_BASIC"       |
      | '<script>hola</script>'                                                                           | 'HTML_BASIC'       | 'false' | "isn't safe text/html with constraint HTML_BASIC"       |
      | 'hola'                                                                                            | 'HTML_IMAGES'      | 'true'  | null                                                    |
      | '<b>hola</b>'                                                                                     | 'HTML_IMAGES'      | 'true'  | null                                                    |
      | '<em>hola</em>'                                                                                   | 'HTML_IMAGES'      | 'true'  | null                                                    |
      | '<i>hola</i>'                                                                                     | 'HTML_IMAGES'      | 'true'  | null                                                    |
      | '<strong>hola</strong>'                                                                           | 'HTML_IMAGES'      | 'true'  | null                                                    |
      | '<u>hola</u>'                                                                                     | 'HTML_IMAGES'      | 'true'  | null                                                    |
      | '<a>hola</a>'                                                                                     | 'HTML_IMAGES'      | 'true'  | null                                                    |
      | '<blockquote>hola</blockquote>'                                                                   | 'HTML_IMAGES'      | 'true'  | null                                                    |
      | '<br><br/><br />'                                                                                 | 'HTML_IMAGES'      | 'true'  | null                                                    |
      | '<cite>hola</cite>'                                                                               | 'HTML_IMAGES'      | 'true'  | null                                                    |
      | '<code>hola</code>'                                                                               | 'HTML_IMAGES'      | 'true'  | null                                                    |
      | '<dd>hola</dd>'                                                                                   | 'HTML_IMAGES'      | 'true'  | null                                                    |
      | '<dl>hola</dl>'                                                                                   | 'HTML_IMAGES'      | 'true'  | null                                                    |
      | '<dt>hola</dt>'                                                                                   | 'HTML_IMAGES'      | 'true'  | null                                                    |
      | '<li>hola</li>'                                                                                   | 'HTML_IMAGES'      | 'true'  | null                                                    |
      | '<ol>hola</ol>'                                                                                   | 'HTML_IMAGES'      | 'true'  | null                                                    |
      | '<p>hola</p>'                                                                                     | 'HTML_IMAGES'      | 'true'  | null                                                    |
      | '<pre>hola</pre>'                                                                                 | 'HTML_IMAGES'      | 'true'  | null                                                    |
      | '<q>hola</q>'                                                                                     | 'HTML_IMAGES'      | 'true'  | null                                                    |
      | '<small>hola</small>'                                                                             | 'HTML_IMAGES'      | 'true'  | null                                                    |
      | '<span>hola</span>'                                                                               | 'HTML_IMAGES'      | 'true'  | null                                                    |
      | '<strike>hola</strike>'                                                                           | 'HTML_IMAGES'      | 'true'  | null                                                    |
      | '<sub>hola</sub>'                                                                                 | 'HTML_IMAGES'      | 'true'  | null                                                    |
      | '<sup>hola</sup>'                                                                                 | 'HTML_IMAGES'      | 'true'  | null                                                    |
      | '<ul>hola</ul>'                                                                                   | 'HTML_IMAGES'      | 'true'  | null                                                    |
      | '<img src="http://">'                                                                             | 'HTML_IMAGES'      | 'true'  | null                                                    |
      | '<img src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" >' | 'HTML_IMAGES'      | 'false' | "isn't safe text/html with constraint HTML_IMAGES"      |
      | '<table><caption>hola</caption></table>'                                                          | 'HTML_IMAGES'      | 'false' | "isn't safe text/html with constraint HTML_IMAGES"      |
      | '<table><col></table>'                                                                            | 'HTML_IMAGES'      | 'false' | "isn't safe text/html with constraint HTML_IMAGES"      |
      | '<table><colgroup></colgroup></table>'                                                            | 'HTML_IMAGES'      | 'false' | "isn't safe text/html with constraint HTML_IMAGES"      |
      | '<div>hola</div>'                                                                                 | 'HTML_IMAGES'      | 'false' | "isn't safe text/html with constraint HTML_IMAGES"      |
      | '<h1>hola</h1>'                                                                                   | 'HTML_IMAGES'      | 'false' | "isn't safe text/html with constraint HTML_IMAGES"      |
      | '<h2>hola</h2>'                                                                                   | 'HTML_IMAGES'      | 'false' | "isn't safe text/html with constraint HTML_IMAGES"      |
      | '<h3>hola</h3>'                                                                                   | 'HTML_IMAGES'      | 'false' | "isn't safe text/html with constraint HTML_IMAGES"      |
      | '<h4>hola</h4>'                                                                                   | 'HTML_IMAGES'      | 'false' | "isn't safe text/html with constraint HTML_IMAGES"      |
      | '<h5>hola</h5>'                                                                                   | 'HTML_IMAGES'      | 'false' | "isn't safe text/html with constraint HTML_IMAGES"      |
      | '<h6>hola</h6>'                                                                                   | 'HTML_IMAGES'      | 'false' | "isn't safe text/html with constraint HTML_IMAGES"      |
      | '<table></table>'                                                                                 | 'HTML_IMAGES'      | 'false' | "isn't safe text/html with constraint HTML_IMAGES"      |
      | '<table><tbody></tbody></table>'                                                                  | 'HTML_IMAGES'      | 'false' | "isn't safe text/html with constraint HTML_IMAGES"      |
      | '<table><tr><td>hola</td></tr></table>'                                                           | 'HTML_IMAGES'      | 'false' | "isn't safe text/html with constraint HTML_IMAGES"      |
      | '<table><tfoot></tfoot></table>'                                                                  | 'HTML_IMAGES'      | 'false' | "isn't safe text/html with constraint HTML_IMAGES"      |
      | '<table><tr><th></th></tr></table>'                                                               | 'HTML_IMAGES'      | 'false' | "isn't safe text/html with constraint HTML_IMAGES"      |
      | '<table><thead></thead></table>'                                                                  | 'HTML_IMAGES'      | 'false' | "isn't safe text/html with constraint HTML_IMAGES"      |
      | '<table><tr></tr></table>'                                                                        | 'HTML_IMAGES'      | 'false' | "isn't safe text/html with constraint HTML_IMAGES"      |
      | '<script>hola</script>'                                                                           | 'HTML_IMAGES'      | 'false' | "isn't safe text/html with constraint HTML_IMAGES"      |
      | 'hola'                                                                                            | 'HTML_DATA_IMAGES' | 'true'  | null                                                    |
      | '<b>hola</b>'                                                                                     | 'HTML_DATA_IMAGES' | 'true'  | null                                                    |
      | '<em>hola</em>'                                                                                   | 'HTML_DATA_IMAGES' | 'true'  | null                                                    |
      | '<i>hola</i>'                                                                                     | 'HTML_DATA_IMAGES' | 'true'  | null                                                    |
      | '<strong>hola</strong>'                                                                           | 'HTML_DATA_IMAGES' | 'true'  | null                                                    |
      | '<u>hola</u>'                                                                                     | 'HTML_DATA_IMAGES' | 'true'  | null                                                    |
      | '<a>hola</a>'                                                                                     | 'HTML_DATA_IMAGES' | 'true'  | null                                                    |
      | '<blockquote>hola</blockquote>'                                                                   | 'HTML_DATA_IMAGES' | 'true'  | null                                                    |
      | '<br><br/><br />'                                                                                 | 'HTML_DATA_IMAGES' | 'true'  | null                                                    |
      | '<cite>hola</cite>'                                                                               | 'HTML_DATA_IMAGES' | 'true'  | null                                                    |
      | '<code>hola</code>'                                                                               | 'HTML_DATA_IMAGES' | 'true'  | null                                                    |
      | '<dd>hola</dd>'                                                                                   | 'HTML_DATA_IMAGES' | 'true'  | null                                                    |
      | '<dl>hola</dl>'                                                                                   | 'HTML_DATA_IMAGES' | 'true'  | null                                                    |
      | '<dt>hola</dt>'                                                                                   | 'HTML_DATA_IMAGES' | 'true'  | null                                                    |
      | '<li>hola</li>'                                                                                   | 'HTML_DATA_IMAGES' | 'true'  | null                                                    |
      | '<ol>hola</ol>'                                                                                   | 'HTML_DATA_IMAGES' | 'true'  | null                                                    |
      | '<p>hola</p>'                                                                                     | 'HTML_DATA_IMAGES' | 'true'  | null                                                    |
      | '<pre>hola</pre>'                                                                                 | 'HTML_DATA_IMAGES' | 'true'  | null                                                    |
      | '<q>hola</q>'                                                                                     | 'HTML_DATA_IMAGES' | 'true'  | null                                                    |
      | '<small>hola</small>'                                                                             | 'HTML_DATA_IMAGES' | 'true'  | null                                                    |
      | '<span>hola</span>'                                                                               | 'HTML_DATA_IMAGES' | 'true'  | null                                                    |
      | '<strike>hola</strike>'                                                                           | 'HTML_DATA_IMAGES' | 'true'  | null                                                    |
      | '<sub>hola</sub>'                                                                                 | 'HTML_DATA_IMAGES' | 'true'  | null                                                    |
      | '<sup>hola</sup>'                                                                                 | 'HTML_DATA_IMAGES' | 'true'  | null                                                    |
      | '<ul>hola</ul>'                                                                                   | 'HTML_DATA_IMAGES' | 'true'  | null                                                    |
      | '<img src="http://">'                                                                             | 'HTML_DATA_IMAGES' | 'true'  | null                                                    |
      | '<img src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" >' | 'HTML_DATA_IMAGES' | 'true'  | null                                                    |
      | '<table><caption>hola</caption></table>'                                                          | 'HTML_DATA_IMAGES' | 'false' | "isn't safe text/html with constraint HTML_DATA_IMAGES" |
      | '<table><col></table>'                                                                            | 'HTML_DATA_IMAGES' | 'false' | "isn't safe text/html with constraint HTML_DATA_IMAGES" |
      | '<table><colgroup></colgroup></table>'                                                            | 'HTML_DATA_IMAGES' | 'false' | "isn't safe text/html with constraint HTML_DATA_IMAGES" |
      | '<div>hola</div>'                                                                                 | 'HTML_DATA_IMAGES' | 'false' | "isn't safe text/html with constraint HTML_DATA_IMAGES" |
      | '<h1>hola</h1>'                                                                                   | 'HTML_DATA_IMAGES' | 'false' | "isn't safe text/html with constraint HTML_DATA_IMAGES" |
      | '<h2>hola</h2>'                                                                                   | 'HTML_DATA_IMAGES' | 'false' | "isn't safe text/html with constraint HTML_DATA_IMAGES" |
      | '<h3>hola</h3>'                                                                                   | 'HTML_DATA_IMAGES' | 'false' | "isn't safe text/html with constraint HTML_DATA_IMAGES" |
      | '<h4>hola</h4>'                                                                                   | 'HTML_DATA_IMAGES' | 'false' | "isn't safe text/html with constraint HTML_DATA_IMAGES" |
      | '<h5>hola</h5>'                                                                                   | 'HTML_DATA_IMAGES' | 'false' | "isn't safe text/html with constraint HTML_DATA_IMAGES" |
      | '<h6>hola</h6>'                                                                                   | 'HTML_DATA_IMAGES' | 'false' | "isn't safe text/html with constraint HTML_DATA_IMAGES" |
      | '<table></table>'                                                                                 | 'HTML_DATA_IMAGES' | 'false' | "isn't safe text/html with constraint HTML_DATA_IMAGES" |
      | '<table><tbody></tbody></table>'                                                                  | 'HTML_DATA_IMAGES' | 'false' | "isn't safe text/html with constraint HTML_DATA_IMAGES" |
      | '<table><tr><td>hola</td></tr></table>'                                                           | 'HTML_DATA_IMAGES' | 'false' | "isn't safe text/html with constraint HTML_DATA_IMAGES" |
      | '<table><tfoot></tfoot></table>'                                                                  | 'HTML_DATA_IMAGES' | 'false' | "isn't safe text/html with constraint HTML_DATA_IMAGES" |
      | '<table><tr><th></th></tr></table>'                                                               | 'HTML_DATA_IMAGES' | 'false' | "isn't safe text/html with constraint HTML_DATA_IMAGES" |
      | '<table><thead></thead></table>'                                                                  | 'HTML_DATA_IMAGES' | 'false' | "isn't safe text/html with constraint HTML_DATA_IMAGES" |
      | '<table><tr></tr></table>'                                                                        | 'HTML_DATA_IMAGES' | 'false' | "isn't safe text/html with constraint HTML_DATA_IMAGES" |
      | '<script>hola</script>'                                                                           | 'HTML_DATA_IMAGES' | 'false' | "isn't safe text/html with constraint HTML_DATA_IMAGES" |
      | 'hola'                                                                                            | 'HTML_FULL'        | 'true'  | null                                                    |
      | '<b>hola</b>'                                                                                     | 'HTML_FULL'        | 'true'  | null                                                    |
      | '<em>hola</em>'                                                                                   | 'HTML_FULL'        | 'true'  | null                                                    |
      | '<i>hola</i>'                                                                                     | 'HTML_FULL'        | 'true'  | null                                                    |
      | '<strong>hola</strong>'                                                                           | 'HTML_FULL'        | 'true'  | null                                                    |
      | '<u>hola</u>'                                                                                     | 'HTML_FULL'        | 'true'  | null                                                    |
      | '<a>hola</a>'                                                                                     | 'HTML_FULL'        | 'true'  | null                                                    |
      | '<blockquote>hola</blockquote>'                                                                   | 'HTML_FULL'        | 'true'  | null                                                    |
      | '<br><br/><br />'                                                                                 | 'HTML_FULL'        | 'true'  | null                                                    |
      | '<cite>hola</cite>'                                                                               | 'HTML_FULL'        | 'true'  | null                                                    |
      | '<code>hola</code>'                                                                               | 'HTML_FULL'        | 'true'  | null                                                    |
      | '<dd>hola</dd>'                                                                                   | 'HTML_FULL'        | 'true'  | null                                                    |
      | '<dl>hola</dl>'                                                                                   | 'HTML_FULL'        | 'true'  | null                                                    |
      | '<dt>hola</dt>'                                                                                   | 'HTML_FULL'        | 'true'  | null                                                    |
      | '<li>hola</li>'                                                                                   | 'HTML_FULL'        | 'true'  | null                                                    |
      | '<ol>hola</ol>'                                                                                   | 'HTML_FULL'        | 'true'  | null                                                    |
      | '<p>hola</p>'                                                                                     | 'HTML_FULL'        | 'true'  | null                                                    |
      | '<pre>hola</pre>'                                                                                 | 'HTML_FULL'        | 'true'  | null                                                    |
      | '<q>hola</q>'                                                                                     | 'HTML_FULL'        | 'true'  | null                                                    |
      | '<small>hola</small>'                                                                             | 'HTML_FULL'        | 'true'  | null                                                    |
      | '<span>hola</span>'                                                                               | 'HTML_FULL'        | 'true'  | null                                                    |
      | '<strike>hola</strike>'                                                                           | 'HTML_FULL'        | 'true'  | null                                                    |
      | '<sub>hola</sub>'                                                                                 | 'HTML_FULL'        | 'true'  | null                                                    |
      | '<sup>hola</sup>'                                                                                 | 'HTML_FULL'        | 'true'  | null                                                    |
      | '<ul>hola</ul>'                                                                                   | 'HTML_FULL'        | 'true'  | null                                                    |
      | '<img src="http://">'                                                                             | 'HTML_FULL'        | 'true'  | null                                                    |
      | '<img src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" >' | 'HTML_FULL'        | 'false' | "isn't safe text/html with constraint HTML_FULL"        |
      | '<table><caption>hola</caption></table>'                                                          | 'HTML_FULL'        | 'true'  | null                                                    |
      | '<table><col></table>'                                                                            | 'HTML_FULL'        | 'true'  | null                                                    |
      | '<table><colgroup></colgroup></table>'                                                            | 'HTML_FULL'        | 'true'  | null                                                    |
      | '<div>hola</div>'                                                                                 | 'HTML_FULL'        | 'true'  | null                                                    |
      | '<h1>hola</h1>'                                                                                   | 'HTML_FULL'        | 'true'  | null                                                    |
      | '<h2>hola</h2>'                                                                                   | 'HTML_FULL'        | 'true'  | null                                                    |
      | '<h3>hola</h3>'                                                                                   | 'HTML_FULL'        | 'true'  | null                                                    |
      | '<h4>hola</h4>'                                                                                   | 'HTML_FULL'        | 'true'  | null                                                    |
      | '<h5>hola</h5>'                                                                                   | 'HTML_FULL'        | 'true'  | null                                                    |
      | '<h6>hola</h6>'                                                                                   | 'HTML_FULL'        | 'true'  | null                                                    |
      | '<table></table>'                                                                                 | 'HTML_FULL'        | 'true'  | null                                                    |
      | '<table><tbody></tbody></table>'                                                                  | 'HTML_FULL'        | 'true'  | null                                                    |
      | '<table><tr><td>hola</td></tr></table>'                                                           | 'HTML_FULL'        | 'true'  | null                                                    |
      | '<table><tfoot></tfoot></table>'                                                                  | 'HTML_FULL'        | 'true'  | null                                                    |
      | '<table><tr><th></th></tr></table>'                                                               | 'HTML_FULL'        | 'true'  | null                                                    |
      | '<table><thead></thead></table>'                                                                  | 'HTML_FULL'        | 'true'  | null                                                    |
      | '<table><tr></tr></table>'                                                                        | 'HTML_FULL'        | 'true'  | null                                                    |
      | '<script>hola</script>'                                                                           | 'HTML_FULL'        | 'false' | "isn't safe text/html with constraint HTML_FULL"        |
