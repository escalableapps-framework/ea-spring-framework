Feature: Validation for a year to be within a range
  In order to validate that a given year is within a certain valid range
  As a backend developer
  I want a component that validates that the year complies with said restriction

  Scenario Outline: year <year> is expected to be greater than or equal to '2000'
    Given the year <year>
    When compared against the minimum value
    Then I get <result> as a result
    And the message in the validation is <message>

    Examples: 
      | year         | result  | message                               |
      | '1999'       | 'false' | 'must be between 2000 and +999999999' |
      | '-999999999' | 'false' | 'must be between 2000 and +999999999' |
      | '2000'       | 'true'  | null                                  |
      | '+999999999' | 'true'  | null                                  |

  Scenario Outline: year <year> is equal to or greater than the minimum acceptable range of '2000' and is also equal to or less than the maximum acceptable range of '2010'
    Given the year <year>
    When compared against the minimum and maximum value
    Then I get <result> as a result
    And the message in the validation is <message>

    Examples: 
      | year   | result | message |
      | '2000' | 'true' | null    |
      | '2009' | 'true' | null    |
      | '2010' | 'true' | null    |

  Scenario Outline: year <year> is expected to be less than or equal to '2010'
    Given the year <year>
    When compared against the maximum value
    Then I get <result> as a result
    And the message in the validation is <message>

    Examples: 
      | year         | result  | message                               |
      | '-999999999' | 'true'  | null                                  |
      | '2009'       | 'true'  | null                                  |
      | '2010'       | 'true'  | null                                  |
      | '+999999999' | 'false' | 'must be between -999999999 and 2010' |
