Feature: Validation for a date to be within a range
  In order to validate that a given date is within a certain valid range
  As a backend developer
  I want a component that validates that the date complies with said restriction

  Scenario Outline: date <date> is expected to be greater than or equal to '2000-01-01'
    Given the date <date>
    When compared against the minimum value
    Then I get <result> as a result
    And the message in the validation is <message>

    Examples: 
      | date               | result  | message                                           |
      | '1999-12-31'       | 'false' | 'must be between 2000-01-01 and +999999999-12-31' |
      | '-999999999-01-01' | 'false' | 'must be between 2000-01-01 and +999999999-12-31' |
      | '2000-01-01'       | 'true'  | null                                              |
      | '2000-01-02'       | 'true'  | null                                              |
      | '+999999999-12-31' | 'true'  | null                                              |

  Scenario Outline: date <date> is equal to or greater than the minimum acceptable range of '2000-01-01' and is also equal to or less than the maximum acceptable range of '2010-01-01'
    Given the date <date>
    When compared against the minimum and maximum value
    Then I get <result> as a result
    And the message in the validation is <message>

    Examples: 
      | date         | result | message |
      | '2000-01-01' | 'true' | null    |
      | '2000-01-02' | 'true' | null    |
      | '2009-12-31' | 'true' | null    |
      | '2010-01-01' | 'true' | null    |

  Scenario Outline: date <date> is expected to be less than or equal to '2010-01-01'
    Given the date <date>
    When compared against the maximum value
    Then I get <result> as a result
    And the message in the validation is <message>

    Examples: 
      | date               | result  | message                                           |
      | '-999999999-01-01' | 'true'  | null                                              |
      | '2009-12-31'       | 'true'  | null                                              |
      | '2010-01-01'       | 'true'  | null                                              |
      | '2010-01-02'       | 'false' | 'must be between -999999999-01-01 and 2010-01-01' |
      | '+999999999-12-31' | 'false' | 'must be between -999999999-01-01 and 2010-01-01' |
