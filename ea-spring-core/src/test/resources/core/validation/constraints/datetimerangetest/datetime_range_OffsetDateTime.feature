Feature: Validation for a OffsetDateTime to be within a range
  In order to validate that a given OffsetDateTime is within a certain valid range
  As a backend developer
  I want a component that validates that the OffsetDateTime complies with said restriction

  Scenario Outline: OffsetDateTime <datetime> is expected to be greater than or equal to '2000-01-01T00:00:00Z'
    Given the OffsetDateTime <datetime>
    When compared the OffsetDateTime against the minimum value
    Then I get <result> as a result
    And the message in the validation is <message>

    Examples: 
      | datetime                          | result  | message                                                                   |
      | '-10000-01-01T00:00:00Z'          | 'false' | 'must be between 2000-01-01T00:00:00Z and +999999999-12-31T23:59:59.999Z' |
      | '1999-12-31T23:59:59.999Z'        | 'false' | 'must be between 2000-01-01T00:00:00Z and +999999999-12-31T23:59:59.999Z' |
      | '2000-01-01T00:00:00Z'            | 'true'  | null                                                                      |
      | '2000-01-01T00:00:00.001Z'        | 'true'  | null                                                                      |
      | '+10000-12-31T23:59:59.999Z'      | 'true'  | null                                                                      |
      | '1999-12-31T19:59:59.999-04:00'   | 'false' | 'must be between 2000-01-01T00:00:00Z and +999999999-12-31T23:59:59.999Z' |
      | '1999-12-31T20:00:00-04:00'       | 'true'  | null                                                                      |
      | '1999-12-31T20:00:00.001-04:00'   | 'true'  | null                                                                      |
      | '+10000-12-31T23:59:59.999-04:00' | 'true'  | null                                                                      |

  Scenario Outline: OffsetDateTime <datetime> is equal to or greater than the minimum acceptable range of '2000-01-01T00:00:00Z' and is also equal to or less than
    the maximum acceptable range of '2010-01-01T00:00:00Z'

    Given the OffsetDateTime <datetime>
    When compared the OffsetDateTime against the minimum and maximum value
    Then I get <result> as a result
    And the message in the validation is <message>

    Examples: 
      | datetime                     | result  | message                                                         |
      | '-10000-01-01T00:00:00Z'     | 'false' | 'must be between 2000-01-01T00:00:00Z and 2010-01-01T00:00:00Z' |
      | '1999-12-31T23:59:59.999Z'   | 'false' | 'must be between 2000-01-01T00:00:00Z and 2010-01-01T00:00:00Z' |
      | '2000-01-01T00:00:00Z'       | 'true'  | null                                                            |
      | '2000-01-01T00:00:00.001Z'   | 'true'  | null                                                            |
      | '2009-12-31T23:59:59.999Z'   | 'true'  | null                                                            |
      | '2010-01-01T00:00:00Z'       | 'true'  | null                                                            |
      | '2010-01-01T00:00:00.001Z'   | 'false' | 'must be between 2000-01-01T00:00:00Z and 2010-01-01T00:00:00Z' |
      | '+10000-12-31T23:59:59.999Z' | 'false' | 'must be between 2000-01-01T00:00:00Z and 2010-01-01T00:00:00Z' |

  Scenario Outline: OffsetDateTime <datetime> is expected to be less than or equal to '2010-01-01T00:00:00Z'
    Given the OffsetDateTime <datetime>
    When compared the OffsetDateTime against the maximum value
    Then I get <result> as a result
    And the message in the validation is <message>

    Examples: 
      | datetime                        | result  | message                                                               |
      | '-10000-01-01T00:00:00Z'        | 'true'  | null                                                                  |
      | '2009-12-31T23:59:59.999Z'      | 'true'  | null                                                                  |
      | '2010-01-01T00:00:00Z'          | 'true'  | null                                                                  |
      | '2010-01-01T00:00:00.001Z'      | 'false' | 'must be between -999999999-01-01T00:00:00Z and 2010-01-01T00:00:00Z' |
      | '+10000-12-31T23:59:59.999Z'    | 'false' | 'must be between -999999999-01-01T00:00:00Z and 2010-01-01T00:00:00Z' |
      | '-10000-01-01T00:00:00-04:00'   | 'true'  | null                                                                  |
      | '2009-12-31T19:59:59.999-04:00' | 'true'  | null                                                                  |
      | '2009-12-31T20:00:00-04:00'     | 'true'  | null                                                                  |
      | '2009-12-31T20:00:00.001-04:00' | 'false' | 'must be between -999999999-01-01T00:00:00Z and 2010-01-01T00:00:00Z' |
