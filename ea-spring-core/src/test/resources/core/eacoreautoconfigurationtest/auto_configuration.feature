Feature: Automatic configuration of the ea-spring-core module
  In order to start the ea-spring-core module
  As a backend developer
  I want the ea-spring-framework to auto configure when loading the Spring context

  Scenario: Automatic configuration loaded successfully
    Given Spring AutoConfiguration is enabled
    When Spring context has been loaded
    Then the class defined for auto configuration is loaded from the 'META-INF/spring.factories' file
    And the ObjectMapper bean is created
