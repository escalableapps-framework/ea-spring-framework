#!/bin/bash

git_repository="git@gitlab.com:escalableapps-framework/ea-spring-framework.git"
git_user_name="Escalable Apps"
git_user_email="contacto@escalableapps.com"

version(){
	new_version=$1
	if [ -z $new_version ]; then
		project_name=$(mvn -q -Dexec.executable=echo -Dexec.args='${project.name}' --non-recursive exec:exec) && \
		project_version=$(mvn -q -Dexec.executable=echo -Dexec.args='${project.version}' --non-recursive exec:exec)
		checkExitCode $?
		echo "---------------------------"
		echo ""
		echo "$project_name version $project_version"
		echo ""
		help
	fi
	case $1 in
	"nextRelease") mvn versions:set -DremoveSnapshot versions:commit
	;;
    "nextSnapshot") mvn versions:set -DnextSnapshot versions:commit
	;;
	*) mvn versions:set -DnewVersion=$new_version versions:commit
	;;
	esac

	new_version=$(mvn -q -Dexec.executable=echo -Dexec.args='${project.version}' --non-recursive exec:exec)
	checkExitCode $?

	sed -i 's/Version: .*/Version: '$new_version'**/g' README.md
}

sonar(){
	mvn clean verify sonar:sonar \
	  -Dsonar.projectKey=ea-spring-framework \
	  -Dsonar.host.url=http://localhost:9000 \
	  -Dsonar.login=0644d0233e59552c97e9aa5329864dafa51ae964
}

build(){
	if [ $CI ]; then
		build_version=$(mvn -q -Dexec.executable=echo -Dexec.args='${project.version}' --non-recursive exec:exec)
		if [ "$CI_COMMIT_BRANCH" = "develop" ] && [[ $build_version =~ ^([1-9]+\.[0-9]+\.[0-9]+)$ ]]; then
			version nextSnapshot && \
			build_version=$(mvn -q -Dexec.executable=echo -Dexec.args='${project.version}' --non-recursive exec:exec)
			checkExitCode $?
	
			chmod 600 $ESCALABLEAPPS_SSHKEY && \
			git remote remove origin && \
			git remote add origin $git_repository && \
			git config user.name "$git_user_name" && \
			git config user.email "$git_user_email" && \
			git commit -a -m "Snapshot v$build_version created by gitlab-ci Build" && \
			GIT_SSH_COMMAND='ssh -i $ESCALABLEAPPS_SSHKEY -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no' git push origin HEAD:$CI_COMMIT_BRANCH
			exit 1
		elif [ "$CI_COMMIT_BRANCH" = "master" ] && [[ $build_version =~ ^.+SNAPSHOT$ ]]; then
			version nextRelease && \
			build_version=$(mvn -q -Dexec.executable=echo -Dexec.args='${project.version}' --non-recursive exec:exec)
			checkExitCode $?
	
			chmod 600 $ESCALABLEAPPS_SSHKEY && \
			git remote remove origin && \
			git remote add origin $git_repository && \
			git config user.name "$git_user_name" && \
			git config user.email "$git_user_email" && \
			git commit -a -m "Release v$build_version created by gitlab-ci Build" && \
			GIT_SSH_COMMAND='ssh -i $ESCALABLEAPPS_SSHKEY -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no' git push origin HEAD:$CI_COMMIT_BRANCH
			exit 1
		fi
	fi
	mvn clean package -DskipTests
}

test(){
	profiles=$1
	if [ $CI ]; then
		mvn clean test -P $profiles
	else
		mvn clean test
	fi
}

tag(){
	if [ "$CI_COMMIT_BRANCH" = "master" ]; then
		build_version=$(mvn -q -Dexec.executable=echo -Dexec.args='${project.version}' --non-recursive exec:exec) && \
		tag_version=v$build_version
		checkExitCode $?

		chmod 600 $ESCALABLEAPPS_SSHKEY && \
		git remote remove origin && \
		git remote add origin $git_repository && \
		git config user.name "$git_user_name" && \
		git config user.email "$git_user_email" && \
		git tag -a $tag_version -m "Tag $tag_version created by gitlab-ci Build" && \
		GIT_SSH_COMMAND='ssh -i $ESCALABLEAPPS_SSHKEY -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no' git push origin $tag_version
	fi
}

deploy(){
	if [ $CI ]; then
		build_version=$(mvn -q -Dexec.executable=echo -Dexec.args='${project.version}' --non-recursive exec:exec)
		checkExitCode $?

		if [ "$CI_COMMIT_BRANCH" = "develop" ]; then
			mvn -s devops/settings-snapshot.xml clean deploy -DskipTests
		elif [ "$CI_COMMIT_BRANCH" = "master" ]; then
			mvn -s devops/settings-staging.xml clean deploy -DskipTests
		fi
	else
		mvn clean deploy -DskipTests
	fi
}

checkExitCode(){
	exit_response=$1
	if [ $exit_response != 0 ]; then
		exit $exit_response
	fi
}

help(){
	echo "Valid arguments for MAKE.sh"
	echo "------------------------------------------------------------------------"
	echo "MAKE version              : Get the current version of the project"
	echo "MAKE version x.x.x        : Set the project version as indicated"
	echo "MAKE version nextSnapshot : Set the project version to the next snapshot"
	echo "MAKE version nextReleaset : Set the project version to the next release"
	echo "MAKE sonar                : Analyze source code using Sonar"
	echo "MAKE build                : Compile the source code"
	echo "MAKE test                 : Run project tests"
	echo "MAKE tag                  : Create a tag in the code repository"
	echo "MAKE deploy               : Deploy the project to Maven central"
	exit 0
}

case $1 in
	"version") version $2
	;;
    "sonar") sonar
	;;
    "build") build
	;;
    "test") test $2
	;;
    "tag") tag
	;;
    "deploy") deploy
	;;
	*) help
	;;
esac
exit $?
