# EA-SPRING-FRAMEWORK

**Version: 1.0.16**

Working with SpringFramework may require a number of configurations. Ea-Spring-Framework provides a series of out-of-the-box beans and utilities without worrying about configurations.

## 1. License

### 1.1 MIT License
Projects referencing this document are released under the terms of the [MIT License](LICENSE).

### 1.2 License Summary

#### 1.2.1 You can

**Commercial Use**: You may use the work commercially.

**Modify**: You may make changes to the work.

**Distribute**: You may distribute the compiled code and/or source.

**Sublicense**: You may incorporate the work into something that has a more restrictive license.

**Private Use**: You may use the work for private use.

#### 1.2.2 You cannot

**Hold Liable**: The work is provided "as is". You may not hold the author liable.

#### 1.2.3 You must

**Include Copyright**: You must include the copyright notice in all copies or substantial uses of the work.

**Include License**: You must include the license notice in all copies or substantial uses of the work.

## 2. How to run

### 2.1 Prerequisites

**Linux**: You must have some version of Linux. In case you need to use Windows, it is recommended to use [Git Bash](https://git-scm.com/downloads) to be able to execute the commands indicated in this document.

**JDK**: You must have Java Development Kit version 11 installed. `javac --version`

**Maven**: You must have Maven version 3.6 or later installed. `mvn --version`

**IDE for Java**: You must have [Eclipse](https://www.eclipse.org/downloads/packages/), [IntelliJ IDEA](https://www.jetbrains.com/idea/download) or [NetBeans](https://netbeans.apache.org/download/index.html)

**Lombok**: You must have [Project Lombok](https://projectlombok.org/setup/eclipse) in order to be able to compile project within your IDE.

## 3. Modules

>>>
ea-spring-framework  
 +- [ea-spring-core](ea-spring-core)  
 +- [ea-spring-starter-cucumber](ea-spring-starter-cucumber)  
 +- [ea-spring-starter-rest](ea-spring-starter-rest)  
>>>

